#include "cinder/app/App.h"
#include "cinder/app/RendererGl.h"
#include "cinder/gl/gl.h"
#include "FlowField.h"
#include "ParticleSystem.h"
#include "cinder/Rand.h"

#include "cinder/Camera.h"
#include "cinder/CameraUi.h"

#include "Composer.h"
#include "PostProcess.h"

#include "Background.h"

using namespace ci;
using namespace ci::app;
using namespace std;


class ComputeParticlesApp : public App {
  public:
	void setup() override;
	void setupGeometry();
	void orbitCam();
	void mouseDown( MouseEvent event ) override;
	void mouseDrag(MouseEvent event) override;
	void update() override;
	void draw() override;
	void renderLayers();

	ci::CameraPersp mCam;
	ci::CameraUi mCamUi;

	FlowFieldRef field;
	ParticleSystem system;
	ci::CameraOrtho ortho;
	ci::vec3 bounds;

	gl::FboRef mRenderFbo, mSceneFbo;

	ci::gl::GlslProgRef mShader;
	ci::gl::BatchRef mBatch;

	ci::gl::BatchRef mEnv;
	ci::gl::GlslProgRef mEnvShader;

	Composer composer;
	PostProcess blurH;
	PostProcess blurW;

	Background bg;
};

void ComputeParticlesApp::setup()
{

	ci::vec3 eye = ci::vec3(0, 0, -40);
	ci::vec3 target = ci::vec3(0, 0, 0);

	mCam = CameraPersp(getWindowWidth(), getWindowHeight(), 60.0, 0.1, 10000.0);
	mCam.lookAt(eye, target);
	mCamUi = CameraUi(&mCam, getWindow(), -1);

	// generate flow field positions. 
	// w components hold a force to effect particles
	auto generator = [=]() -> vector<ci::vec4> {
		vector<ci::vec4> positions;

		float resolution = 10.0f;
		int cubeSize = 150;
		float cols = cubeSize / resolution;
		float rows = cubeSize / resolution;
		float zcols = cubeSize / resolution;

		auto getValue = [=](float x, float y)->float {

			//return (x + y) * 0.001 * M_PI * 2;
			return (sin(x * 0.01) + sin(y * 0.0001)) * M_PI * 2;
		};

		for (int i = 0; i < cols; ++i) {
			for (int j = 0; j < rows; ++j) {

				for (int a = 0; a < zcols; ++a) {
					auto x = i * resolution;
					auto y = j * resolution;
					auto z = a * resolution;


					x -= cubeSize / 2;
					y -= cubeSize / 2;
					z -= cubeSize / 2;



					float value = (x + y + z) * 0.01 * M_PI * 2;
					//float value = randFloat();
					//float value = getValue(x, y);

					positions.push_back(vec4(x, y, z, value));
				}

			}
		}

		// set the bounds
		bounds = vec3(cols, rows, zcols);


		return positions;

	};

	field = FlowField::create();

	// generate a flow field and set up it's positions and values. 
	field->generate(generator, true);

	// build a particle system, keep it under the size of the flow field. 
	//system = ParticleSystem(field->getSize());
	system = ParticleSystem(500);

	// set the flow field ref 
	system.setField(field);

	// =========== SETUP FBOS =============== //
	gl::Fbo::Format fmt;
	gl::Texture::Format tfmt;
	tfmt.setInternalFormat(GL_RGBA32F);
	tfmt.setWrap(GL_CLAMP_TO_EDGE, GL_CLAMP_TO_EDGE);
	fmt.setColorTextureFormat(tfmt);

	mSceneFbo = gl::Fbo::create(getWindowWidth(), getWindowHeight());
	mRenderFbo = gl::Fbo::create(getWindowWidth(), getWindowHeight());

	setupGeometry();



	// ========== SETUP POST =============== //
	blurW = PostProcess("shaders/blur.glsl");
	blurH = PostProcess("shaders/blur.glsl");

	//composer.addLayer(blurW);
	//composer.addLayer(blurH);
}

void ComputeParticlesApp::orbitCam() {
	auto target = vec3(0);
	float speed = 0.5;
	float deltaX = (target.x - speed) / (float)getWindowSize().x * mCam.getPivotDistance();
	float deltaY = (target.z - 0.0) / (float)getWindowSize().y * mCam.getPivotDistance();

	vec3 right, up;
	mCam.getBillboardVectors(&right, &up);
	mCam.setEyePoint(mCam.getEyePoint() - right * deltaX + up * deltaY);
	mCam.lookAt(target);

}

void ComputeParticlesApp::mouseDown(MouseEvent event) {
	mCamUi.mouseDown(event);
}
void ComputeParticlesApp::mouseDrag(MouseEvent event) {
	mCamUi.mouseDrag(event);
}



void ComputeParticlesApp::update()
{

	orbitCam();

	system.update();
	mSceneFbo->bindFramebuffer();
	gl::clear();
	gl::setMatrices(mCam);
	system.draw();
	
	bg.draw(mCam.getViewMatrix(),mCam.getInverseViewMatrix());
	mSceneFbo->unbindFramebuffer();
}


void ComputeParticlesApp::setupGeometry() {


	auto quad = gl::VboMesh::create(geom::Plane().size(vec2(getWindowWidth(),getWindowHeight())));
	
	mShader = gl::getStockShader(gl::ShaderDef().texture());

	mBatch = gl::Batch::create(quad, mShader);

}

void ComputeParticlesApp::renderLayers() {
	gl::ScopedFramebuffer fbo(mRenderFbo);
	gl::clear(Color(0, 0, 0));


	gl::ScopedTextureBind tex0(mSceneFbo->getColorTexture(), 0);
	gl::viewport(getWindowSize());
	gl::setMatricesWindow(getWindowSize());
	gl::translate(vec2(getWindowWidth() / 2, getWindowHeight() / 2));

	

	// draw main layer
	gl::pushMatrices();
	gl::rotate(toRadians(90.0f), vec3(1, 0, 0));
	mBatch->draw();
	gl::popMatrices();


	// enable blending. 
	gl::enableAdditiveBlending();

	// draw rotated layer
	gl::pushMatrices();
	gl::rotate(toRadians(-90.0f), vec3(0, 1, 0));
	mBatch->draw();
	gl::popMatrices();



	// disable blending
	gl::disableBlending();


}

void ComputeParticlesApp::draw()
{
	gl::clear(Color::gray(1));

	renderLayers();

	// ===== SETUP POST PROCESSING UNIFORMS ======= // 
	blurW.uniform("sample_offset", vec2(1.0f / app::getWindowWidth(), 0.0f));
	blurW.uniform("attenuation", 1.5f);
	blurH.uniform("sample_offset", vec2( 0.0f, 1.0f / app::getWindowHeight()));
	blurH.uniform("attenuation", 1.5f);


	// ===== RENDER SCENE ======= // 
	composer.render(mRenderFbo->getColorTexture());
	
}

CINDER_APP( ComputeParticlesApp, RendererGl, [](App::Settings *settings) {
	settings->setWindowSize(1024, 768);
});

