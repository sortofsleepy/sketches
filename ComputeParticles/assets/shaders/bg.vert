#version 420



uniform mat4 ciProjectionMatrix;
uniform mat4 ciModelMatrix;
uniform mat4 ciViewMatrix;
uniform mat3 ciNormalMatrix;
uniform mat4 viewInverse;

uniform float scale;

in vec3 ciPosition;
in vec3 ciNormal;

out vec3 vPosition;
out vec3 vWsPosition;
out vec3 vEyePosition;
out mat3 vNormalMatrix;
out mat4 vViewInverse;
out vec3 vGeomNormal;
void main(){
    vec3 pos = ciPosition;
  

    vec4 worldSpacePosition = ciModelMatrix * vec4(ciPosition,1.);
    vec4 viewSpacePosition = ciViewMatrix * worldSpacePosition;

    vec4 eyeDirViewSpace = viewSpacePosition - vec4(0.0,0.0,0.0,1.0);

    vPosition = ciPosition;
    vWsPosition = worldSpacePosition.xyz;
    vEyePosition = -vec3(ciViewMatrix * eyeDirViewSpace);
    vNormalMatrix = ciNormalMatrix;
    vViewInverse = viewInverse;
	vGeomNormal = ciNormal; 

    gl_Position = ciProjectionMatrix * ciViewMatrix * ciModelMatrix * vec4(pos,1);
}
