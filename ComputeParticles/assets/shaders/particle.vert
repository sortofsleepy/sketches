#version 420
#extension GL_ARB_shader_storage_buffer_object : require

in vec3 ciPosition;
in int particleId;
in float rotationOffset;
in float time;



#include "rotations.glsl"

out vec3 e;
out vec3 n;

struct Particle
{
	// current position 
	vec4 pos;

	// previous position 
	vec4 ppos;

	// home position 
	vec4 start;

	// velocity 
	vec4 vel;

	// acceleration 
	vec4 accel;

	// x = speed
	// y = mass
	vec4 meta;
};

layout( std140, binding = 0 ) buffer Part
{
    Particle particles[];
};

uniform mat4 ciModelViewProjection;
uniform mat4 ciModelView;
uniform mat3 ciNormalMatrix;

in vec3 ciNormal;

out vec3 vVel;
out vec3 vPos;

void main(){	
	vec3 pos = ciPosition;

	// scale instance 
	pos.x *= particles[particleId].meta.z;
	pos.y *= particles[particleId].meta.z;
	pos.z *= particles[particleId].meta.z;


	vec3 particlePos = particles[particleId].pos.xyz;


		// rotate
	pos = rotateX(pos,particlePos.x * rotationOffset * 0.5);
	pos = rotateY(pos,particlePos.y * rotationOffset * 0.5);
	pos = rotateZ(pos,particlePos.z * rotationOffset * 0.5);
	

	vec3 finalPos = pos + particles[particleId].pos.xyz;

	// SEM shading 
	e = normalize(vec3(ciModelView * vec4(ciPosition,1.)));
	n = normalize(ciNormalMatrix * ciNormal);

	gl_Position = ciModelViewProjection * vec4( finalPos, 1 );
	vVel = particles[particleId].vel.xyz;
	vPos = particles[particleId].pos.xyz;
}