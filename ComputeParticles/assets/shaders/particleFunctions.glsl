
// applies a force to a particle
Particle addForce(Particle p, vec3 _force){

	float mass = p.meta.y;
	_force /= vec3(mass);

	p.accel += vec4(_force,1.);

	return p;
}

Particle checkBounds(Particle p, float maxDistance){
// compare distance from the center, if value is greater than bounds, 
	// reverse velocity

	float dist = distance(p.pos,vec4(100.));

	if(dist > maxDistance){
		p.vel *= -1.0;
	}


	return p;
}


Particle buildAttractiveForce(Particle p){

	float mass = p.meta.y;
	float G = 1.0;
	float centerMass = 20.0;

	// build attractive force
	vec4 force = vec4(0.) - p.pos;
	float dist = length(force);
	dist = clamp(dist,5.0,100.0);
	float strength = (G * centerMass * mass) / (dist * dist);

	force *= vec4(strength,strength,strength,1.);
	

	
	p = addForce(p,force.xyz);
	

	return p;
}

Particle buildSphericalForce(Particle p){
	
	float x = cos(p.spherical.y) * sin(p.spherical.x) * 30.0;
	float y = sin(p.spherical.y) * sin(p.spherical.x) * 30.0;
	float z = cos(p.spherical.x) * 30.0;

	p.spherical.x += p.spherical.z;
	p.spherical.y += p.spherical.w;

	p.pos = vec4(x,y,z,1.);

	return p;
}


Particle update(Particle p, Effector e){


	p = buildSphericalForce(p);

	
	p.vel += p.accel;

	p.pos += p.vel;

	p.accel *= 0.0;
	
	
	p = buildAttractiveForce(p);

	// check distance to effector 
	if(distance(p.pos.xyz,e.pos.xyz) < 10.0){
		p = addForce(p,vec3(e.pos.w));
	}

	
	
	return p;

}