precision highp float;




uniform samplerCube uIrradianceCubeMap;
uniform samplerCube uRadianceCubeMap;
uniform float uMetallic;
uniform float uSpecular;
uniform float uExposure;
uniform float uRoughness;
uniform float uGamma;

in vec3 vPosition;
in vec3 vWsPosition;
in vec3 vEyePosition;
in mat3 vNormalMatrix;
in mat4 vViewInverse;
in vec3 vGeomNormal;

const vec3 uBaseColor = vec3(1.0,1.0,1.0);
out vec4 glFragColor;

#include "pbr.glsl"

void main(){
    // ====== calculate normals ========== //
   vec3 x = dFdx(vPosition);
   vec3 y = dFdy(vPosition);
   vec3 normal = vGeomNormal;
   vec3 vNormal = normalize( vNormalMatrix * normal);
   vec3 vWsNormal = vec3(vViewInverse * vec4(vNormal,0.0));

   // ====== PBR STUFF ======== //
   vec3 N = normalize(vWsNormal);
   vec3 V = normalize(vEyePosition);


   vec3 color = getPbr(N,V,uRadianceCubeMap,uIrradianceCubeMap,uBaseColor,uRoughness,uMetallic,uSpecular);
   color = toneMap(color * uExposure);
   color = color * (1.0 / toneMap(vec3(10.0)));
   color = pow(color,vec3(1.0 / uGamma));
   float g = (color.r + color.g + color.b);

   vec4 finalColor = vec4(color*color,g);

   glFragColor = finalColor;
}


/*
 color = toneMap(color * uExposure);
   color = color * (1.0 / toneMap(vec3(10.0)));
   color = pow(color,vec3(1.0 / uGamma));
   float g = (color.r + color.g + color.b);

   vec4 finalColor = vec4(color*color,g);
   */
