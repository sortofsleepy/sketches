struct Effector
{
	vec4 pos;
};

layout( std140, binding = 1 ) buffer Effect
{
    Effector effectors[];
};
