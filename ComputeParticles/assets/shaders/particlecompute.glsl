#version 420 core
#extension GL_ARB_compute_shader : enable
#extension GL_ARB_shader_storage_buffer_object : enable
#extension GL_ARB_compute_variable_group_size : enable

// number of effectors so we can loop
uniform int numEffectors;

// value representing the max distance a particle can be from the center. 
uniform float maxDistance;

// minimum distance a particle needs to be before it is affected by an effector. 
const float minDistance = 0.2;

uniform float OffsetValue;

struct Particle
{
	// current position 
	vec4 pos;

	// previous position 
	vec4 ppos;

	// velocity 
	vec4 vel;

	// acceleration 
	vec4 accel;

	vec4 spherical;

	// x = speed
	// y = mass
	vec4 meta;
};

layout( std140, binding = 0 ) buffer Part
{
    Particle particles[];
};


//layout( local_size_variable ) in;
layout( local_size_x = 228, local_size_y = 1, local_size_z = 1 ) in;


// ====================== INCLUDES ===========================
#include "noise.glsl"

// effectors make up the flow field that manipulate the particles. 
#include "effectors.glsl"

// split particle related functions to cut down on the code a little bit. 
#include "particleFunctions.glsl"

void main()
{
	uint gid = gl_GlobalInvocationID.x;	// The .y and .z are both 1 in this case.



	Particle p = particles[gid];
	Effector e = effectors[gid];

	p = update(p,e);

	//p *= OffsetValue;

	particles[gid] = p;
 
}
