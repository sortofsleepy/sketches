

#version 420
in vec3 vVel;
in vec3 vPos;

in vec3 e;
in vec3 n;

uniform float time;
uniform sampler2D matCap;

out vec4 glFragColor;

void main(){

	vec3 r = reflect( e, n );
	float m = 2. * sqrt( pow( r.x, 2. ) + pow( r.y, 2. ) + pow( r.z + 1., 2. ) );
	vec2 vN = r.xy / m + .5;


	vec3 base = vec3(1.);
	vec4 tex = texture(matCap,vN);
	vec4 color = vec4(mix(base,vPos * vVel,0.8),1.);

	for(int i = 0; i < 6; ++i){
		color += color;
	}
	glFragColor = tex * (color * 0.2);

}