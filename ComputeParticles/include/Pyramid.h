#pragma once
//
// Created by sortofsleepy on 9/11/2018.
//

#ifndef PARTICLES_Pyramid_H
#define PARTICLES_Pyramid_H

#include "cinder/gl/VboMesh.h"
#include "cinder/gl/Batch.h"
#include "cinder/gl/GlslProg.h"
#include "cinder/gl/gl.h"
#include "cinder/GeomIo.h"
#include "Object.h"
using namespace ci;
using namespace std;

class Pyramid {

	gl::VboMeshRef mMesh;
	gl::BatchRef mBatch;
	gl::GlslProgRef mShader;
	float radius = 5.0f;

	ci::vec3 position;
public:
	Pyramid() {


		std::vector<float> vertices = {
				0.0,  1.0,  0.0,
				-1.0, -1.0,  1.0,
				1.0, -1.0,  1.0,

				// Right face
				0.0,  1.0,  0.0,
				1.0, -1.0,  1.0,
				1.0, -1.0, -1.0,

				// Back face
				0.0,  1.0,  0.0,
				1.0, -1.0, -1.0,
				-1.0, -1.0, -1.0,

				// Left face
				0.0,  1.0,  0.0,
				-1.0, -1.0, -1.0,
				-1.0, -1.0,  1.0
		};

		gl::VboMesh::Layout layout;
		layout.attrib(geom::POSITION, 3);

		mMesh = gl::VboMesh::create(vertices.size(), GL_TRIANGLES, { layout });
		mMesh->bufferAttrib(geom::POSITION, sizeof(float) * vertices.size(), vertices.data());

		mShader = gl::getStockShader(gl::ShaderDef().color());
		mBatch = gl::Batch::create(mMesh, mShader);
	}

	void setPosition(ci::vec3 pos) {
		position = pos;
	}

	void draw() {
		gl::pushMatrices();
		gl::translate(position);
		gl::scale(vec3(10));
		mBatch->draw();
		gl::popMatrices();
	}



};

#endif //PARTICLES_Pyramid_H
