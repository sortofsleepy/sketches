#pragma once


#include <vector>
#include "cinder/Vector.h"
#include "cinder/gl/Ssbo.h"
#include "cinder/gl/GlslProg.h"
#include "cinder/Log.h"

using namespace ci;
using namespace std;


typedef struct {
	ci::vec4 pos;
}Effector;

typedef shared_ptr<class FlowField> FlowFieldRef;

class FlowField {

	vector<ci::vec4> positions;

	gl::SsboRef mSsbo;

	// debug stuff 
	gl::GlslProgRef mDebugShader;
	gl::VboRef mVbo;
	gl::VaoRef mVao;

	bool debugMode = false;

public:
	FlowField() = default;

	static FlowFieldRef create() {
		return FlowFieldRef(new FlowField());
	}

	//! runs the generator function to create the field and parses it into 
	//! the ssbo to pass to the compute shader. 
	void generate(std::function<vector<ci::vec4>()> generator,bool debugMode=false) {
		positions = generator();

		vector<Effector> effectors;
		for (int i = 0; i < positions.size(); ++i) {
			auto x = positions[i].x;
			auto y = positions[i].y;
			auto z = positions[i].z;
			auto force = positions[i].w;


			Effector effector;
			effector.pos = vec4(x, y, z,force);
			//effector.force = force;
			effectors.push_back(effector);
		}

		// don't forget to add
		mSsbo = gl::Ssbo::create(effectors.size() * sizeof(Effector), effectors.data(), GL_STATIC_DRAW);
		
		this->debugMode = debugMode;

		if (debugMode) {
			//buildDebug();
		}
		
	}

	gl::SsboRef getFieldData() {
		return mSsbo;
	}

	int getSize() {
		return positions.size();
	}

	void drawDebug(mat4 projectionMatrix, mat4 viewMatrix) {
		gl::ScopedVao scopeVao(mVao);
		gl::ScopedGlslProg scopeGlsl(mDebugShader);

		gl::setDefaultShaderVars();

		mDebugShader->uniform("projectionMatrix", projectionMatrix);
		mDebugShader->uniform("viewMatrix",viewMatrix);
		gl::pointSize(5.0);
		gl::drawArrays(GL_POINTS, 0, positions.size());
	}
	void bind() {
	}

	void unbind() {
		

	}

	ci::gl::VboRef getRawFieldData() {
		return mVbo;
	}

	/*
	void buildDebug() {
	
	
		mVao = gl::Vao::create();

		
		gl::ScopedVao scopeVao(mVao);
		{
			// buffer the positions
			mVbo = gl::Vbo::create(GL_ARRAY_BUFFER, positions.size() * sizeof(vec4), positions.data(), GL_STATIC_DRAW);
			{
				// bind and explain the vbo to your vao so that it knows how to distribute vertices to your shaders.
				gl::ScopedBuffer sccopeBuffer(mVbo);
				gl::vertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 0, (const GLvoid*)0);
				gl::enableVertexAttribArray(0);
			}
		}
		gl::GlslProg::Format fmt;
		fmt.vertex(app::loadAsset("gridDebug.glsl"));
		fmt.fragment(app::loadAsset("gridDebugFrag.glsl"));
		fmt.attribLocation("position", 0);

		mDebugShader = gl::GlslProg::create(fmt);
	}
	*/


};