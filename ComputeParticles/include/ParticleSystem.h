//
// Created by sortofsleepy on 9/23/2018.
//

#ifndef PARTICLES_PARTICLESYSTEM_H
#define PARTICLES_PARTICLESYSTEM_H

#include "cinder/Vector.h"
#include "cinder/gl/gl.h"
#include "cinder/Utilities.h"
#include "cinder/gl/Ssbo.h"
#include "cinder/gl/Vbo.h"
#include "cinder/app/App.h"
#include "cinder/Log.h"
#include "FlowField.h"
#include "cinder/Rand.h"
#include "cinder/gl/Texture.h"

using namespace ci;
using namespace ci::app;
using namespace std;

// note - to keep memory in alignment, each item needs to be at least
// 4 bytes, so make everything vec4 instead of vec3
#pragma pack( push, 1 )
struct Particle {

	// current position 
	ci::vec4 pos;

	// previous position 
	ci::vec4 ppos;

	// velocity 
	ci::vec4 vel;

	// acceleration 
	ci::vec4 accel;

	// spherical components
	// x - phi
	// y - theta
	// z - phiSpeed
	// w - thetaSpeed
	ci::vec4 spherical;

	// x = speed
	// y = mass
	// z = scale
	ci::vec4 meta;
};
#pragma pack( pop )

class ParticleSystem {
	gl::SsboRef mParticleBuffer;
	gl::VboRef mIdsVbo;
	gl::VaoRef mAttributes;

	ci::gl::VaoRef vao;

	enum { WORK_GROUP_SIZE = 228 };

	gl::GlslProgRef mUpdateShader, mRenderShader;
	vector<Particle> particles;

	ci::gl::VboMeshRef mMesh;
	ci::gl::BatchRef mBatch;

	ci::vec2 resolution;

	FlowFieldRef field;

	int mNumParticles;

	ci::vec3 mBounds;

	ci::gl::TextureRef mMatCap;
public:
	explicit ParticleSystem(int numParticles = 100) {
		vao = gl::Vao::create();
		resolution = app::getWindowSize();
		mNumParticles = numParticles;

		for (int i = 0; i < numParticles; ++i) {
			Particle p;

			p.vel = vec4(randVec3(), 1.);
			p.accel = vec4(randVec3(), 1.);

			// set speed
			p.meta.x = randFloat();

			// set mass
			p.meta.y = randFloat();

			// set scale 
			p.meta.z = randFloat(0.50,1.8);

			// set spherical components
			p.spherical.x = randFloat(M_PI * M_PI);
			p.spherical.y = randFloat(M_PI * M_PI);
			p.spherical.z = randFloat(-0.01, 0.01);
			p.spherical.w = randFloat(-0.01, 0.01);

			particles.emplace_back(p);
		}

		// init shaders
		gl::GlslProg::Format uFmt, rFmt;
		uFmt.compute(loadAsset("shaders/particlecompute.glsl"));
		rFmt.vertex(loadAsset("shaders/particle.vert"));
		rFmt.fragment(loadAsset("shaders/particle.frag"));

		try {
			mUpdateShader = gl::GlslProg::
				create(gl::GlslProg::Format().compute(loadAsset("shaders/particlecompute.glsl")).attribLocation("OffsetValue",0));
		}
		catch (ci::Exception e) {
			CI_LOG_E(e.what());
		}

		try {
			// build rendering shader. 
			mRenderShader = gl::GlslProg::create(rFmt);
		}
		catch (ci::Exception e) {
			CI_LOG_E(e.what());
		}
	

		// load particles into ssbo
		mParticleBuffer = gl::Ssbo::create(particles.size() * sizeof(Particle), particles.data(), GL_STATIC_DRAW);

		// generate geometry
		generateGeometry();

		mMatCap = gl::Texture::create(loadImage(app::loadAsset("imgs/matcap.jpg")));
	}

	void setField(FlowFieldRef field) {
		this->field = field;
	}

	void setBounds(ci::vec3 mBounds) {
		this->mBounds = mBounds;
	}

	void generateGeometry() {
		//auto geo = geom::Icosphere().subdivisions(0.5);
		auto geo = geom::Teapot();

		mMesh = gl::VboMesh::create(geo);

		int numParticles = particles.size();

		// generate ids so we can look up particles easily when rendering.
		std::vector<GLuint> ids(numParticles);
		GLuint currId = 0;
		ids.push_back(0);
		std::generate(ids.begin(), ids.end(), [&currId]() -> GLuint { return currId++; });

		mIdsVbo = gl::Vbo::create<GLuint>(GL_ARRAY_BUFFER, ids, GL_STATIC_DRAW);

		// generate random rotation offsets
		std::vector<float> rotationOffsets(numParticles);
		std::generate(rotationOffsets.begin(), rotationOffsets.end(), []() -> GLuint {
			return randFloat() + 1.0;
		});

		gl::VboRef rotations = gl::Vbo::create<float>(GL_ARRAY_BUFFER, rotationOffsets, GL_STATIC_DRAW);

		vao->bind();
		rotations->bind();
		gl::vertexAttribPointer(0, 1, GL_FLOAT, GL_FALSE, sizeof(float) * rotationOffsets.size(),0);
		gl::enableVertexAttribArray(0);
		rotations->unbind();
		vao->unbind();

		geom::BufferLayout instanceDataLayout;
		instanceDataLayout.append(geom::Attrib::CUSTOM_0, 1, 0, 0, 1 /* per instance */);
		mMesh->appendVbo(instanceDataLayout, mIdsVbo);

		geom::BufferLayout rotationLayout;
		rotationLayout.append(geom::Attrib::CUSTOM_1, 1, 0, 0, 1 /* per instance */);
		mMesh->appendVbo(rotationLayout, rotations);


		mBatch = gl::Batch::create(mMesh, mRenderShader, {
			{geom::CUSTOM_0,"particleId"},
			{geom::CUSTOM_1,"rotationOffset"}
		});

	
	}

	void update() {
	
		gl::ScopedVao v(vao);
		gl::ScopedGlslProg updateS(mUpdateShader);

		mParticleBuffer->bindBase(0);
		field->getFieldData()->bindBase(1);

		
		mUpdateShader->uniform("numEffectors", field->getSize());
		mUpdateShader->uniform("maxDistance", 130.0f);
		mUpdateShader->uniform("time", (float)app::getElapsedSeconds());

		// may need to adjust work group size, just dispatch everything for now. 
		gl::dispatchCompute(mNumParticles, 1, 1);
		gl::memoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);
	

	}


	void draw() {

		gl::enableDepthRead();
		gl::enableDepthWrite();


		gl::ScopedBuffer scopedParticleSsbo(mParticleBuffer);
		gl::ScopedTextureBind tex(mMatCap, 0);

		mBatch->getGlslProg()->uniform("time", (float)app::getElapsedSeconds());
		mBatch->getGlslProg()->uniform("matCap", 0);

		mBatch->drawInstanced(particles.size());
	}
};

#endif //PARTICLES_PARTICLESYSTEM_H
