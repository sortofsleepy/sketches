#pragma once

#include <vector>
#include "PostProcess.h"
#include "cinder/app/App.h"
#include "cinder/gl/Fbo.h"
#include "cinder/gl/Texture.h"
using namespace std;

class Composer {

	std::vector<PostProcess> effects;

	ci::gl::FboRef mFbo;

	
	
public:

	Composer(int width=ci::app::getWindowWidth(),int height=ci::app::getWindowHeight()) {

		ci::gl::Fbo::Format fmt;
		ci::gl::Texture::Format tfmt;

		tfmt.setWrap(GL_CLAMP_TO_EDGE, GL_CLAMP_TO_EDGE);
		tfmt.setMagFilter(GL_NEAREST);
		tfmt.setMinFilter(GL_NEAREST);

		fmt.setColorTextureFormat(tfmt);

		mFbo = gl::Fbo::create(width,height, fmt); {
			mFbo->bindFramebuffer();
			gl::clear(ColorA(0, 0, 0, 0));
			mFbo->unbindFramebuffer();
		}
	}
	
	//! Adds a post processing layer to utilize. Can be chained. 
	Composer& addLayer(PostProcess layer) {
		layer.setFBO(mFbo);
		effects.push_back(layer);

		return *this;
	}

	//! Renders the scene while running all passes.
	void render(ci::gl::TextureRef input) {

		if (effects.size() > 0) {
			for (int i = 0; i < effects.size(); ++i) {

				// if first item, render input into composer, 
				// otherwise render previous input into current layer. 
				if (i == 0) {
					effects[i].render(input);

				}
				else {
					int idx = i - 1;
					effects[i].render(effects[idx].getTexture());

				}


				gl::viewport(getWindowSize());
				gl::setMatricesWindow(getWindowSize());
				gl::draw(effects[i].getTexture(), Rectf(0, 0, mFbo->getWidth(), mFbo->getHeight()));

			}
		}
		else {
			gl::viewport(getWindowSize());
			gl::setMatricesWindow(getWindowSize());
			gl::draw(input, Rectf(0, 0, mFbo->getWidth(), mFbo->getHeight()));

		}
		
	}

	
};
