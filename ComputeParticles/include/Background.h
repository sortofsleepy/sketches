#pragma once

#include "cinder/gl/GlslProg.h"
#include "cinder/gl/Batch.h"
#include "cinder/gl/VboMesh.h"
#include "cinder/app/App.h"
#include "cinder/gl/gl.h"

using namespace std;
using namespace ci;

class Background {

	gl::GlslProgRef mShader;
	gl::BatchRef mBatch;

	gl::TextureCubeMapRef mIrr,mRad;
	
	float metallic, specular, exposure, roughness, gamma;


public:
	Background(float metallic=1.0f,float specular=1.0f, float exposure=5.0f, float roughness=0.5f, float gamma=2.2f):
	metallic(metallic),
	specular(specular),
	exposure(exposure),
	roughness(roughness),
	gamma(gamma){
	
		gl::GlslProg::Format fmt;
		fmt.vertex(app::loadAsset("shaders/bg.vert"));
		fmt.fragment(app::loadAsset("shaders/bg.frag"));
		fmt.version(420);

		try {
			mShader = gl::GlslProg::create(fmt);

		}
		catch (gl::GlslProgCompileExc &e) {

			CI_LOG_E(e.what());
		}
		auto mesh = gl::VboMesh::create(geom::Icosphere().subdivisions(0.2));
		mBatch = gl::Batch::create(mesh, mShader);


		// build cube map 
		ImageSourceRef imgs[6] = {
			loadImage(app::loadAsset("imgs/irr_negx.hdr")),
			loadImage(app::loadAsset("imgs/irr_negy.hdr")),
			loadImage(app::loadAsset("imgs/irr_negz.hdr")),
			loadImage(app::loadAsset("imgs/irr_posx.hdr")),
			loadImage(app::loadAsset("imgs/irr_posy.hdr")),
			loadImage(app::loadAsset("imgs/irr_negz.hdr")),
		};

		mIrr = gl::TextureCubeMap::create(imgs);
		mRad = gl::TextureCubeMap::createFromDds(loadAsset("imgs/studio_radiance.dds"));
	}

	void draw(mat4 viewMatrix,mat4 inverseView) {
		auto prog = mBatch->getGlslProg();

		gl::ScopedTextureBind irr(mIrr, 0);
		gl::ScopedTextureBind rad(mRad, 1);

		// calculate normal matrix ciNormalMatrix isn't getting populated for some reason
		mat4 modelMatrix;
		modelMatrix *= vec4(150);

		mat4 normalMatrix = glm::transpose(glm::inverse(modelMatrix * viewMatrix));
		
		prog->uniform("uIrradianceCubeMap", 0);
		prog->uniform("uRadianceCubeMap", 1);
		prog->uniform("uMetallic", metallic);
		prog->uniform("uSpecular", specular);
		prog->uniform("uExposure", exposure);
		prog->uniform("uRoughness", roughness);
		prog->uniform("uGamma", gamma);
		prog->uniform("viewInverse", inverseView);
		prog->uniform("normalMatrix", normalMatrix);

		gl::pushMatrices();
		gl::scale(vec3(150));
		mBatch->draw();

		gl::popMatrices();
	}


};