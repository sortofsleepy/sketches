#pragma once

#include "cinder/app/App.h"
#include "cinder/gl/Fbo.h"
#include "cinder/gl/GlslProg.h"
#include "cinder/gl/gl.h"

using namespace ci;
using namespace std;

#define STRINGIFY(A) #A

class PostProcess {
	ci::gl::GlslProgRef mShader;
	ci::gl::FboRef mFbo;
	ci::gl::TextureRef mFrame;
public:
	PostProcess() {}
	PostProcess(std::string shaderPath) {
		gl::GlslProg::Format fmt;

		fmt.vertex(app::loadAsset("shaders/quad.vert"));
		fmt.fragment(app::loadAsset(shaderPath));
		fmt.version(420);


		mShader = gl::GlslProg::create(fmt);
		//mShader = gl::getStockShader(gl::ShaderDef().texture());
	
		//mFbo = gl::Fbo::create(app::getWindowWidth(), app::getWindowHeight());
	}

	void setFBO(ci::gl::FboRef mFbo) {
		this->mFbo = mFbo;
	}

	gl::TextureRef render(ci::gl::TextureRef mInput) {

		gl::ScopedFramebuffer fbo(mFbo); {
			gl::clear(ColorA(0, 0, 0, 0));

			gl::ScopedGlslProg shader(mShader);
			gl::ScopedViewport view(app::getWindowSize());

			gl::setMatricesWindow(getWindowSize());

			gl::ScopedTextureBind tex0(mInput);

			mShader->uniform("uTex0", 0);

			gl::drawSolidRect(app::getWindowBounds());


		}

		mFrame = mFbo->getColorTexture();

		return mFbo->getColorTexture();
	}


	template<typename T>
	void uniform(std::string name, T value) {
		mShader->uniform(name, value);
	}

	ci::gl::TextureRef getTexture() {
		//return mFbo->getColorTexture();
		return mFrame;
	}
};