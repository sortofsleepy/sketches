#pragma once
//
// Created by sortofsleepy on 9/14/2018.
//
#pragma once
#ifndef PARTICLES_OBJECT_H
#define PARTICLES_OBJECT_H

#include "cinder/Vector.h"
using namespace ci;

class Object {

public:
	ci::vec3 acceleration;
	float mass = 1.0f;
	ci::vec3 velocity;
	ci::vec3 position;

	void addForce(ci::vec3 force) {
		force /= vec3(mass);
		acceleration += force;
	}

	void updatePhysics() {
		velocity += acceleration;
		position += velocity;

		acceleration *= 0.0;

	}
};

#endif //PARTICLES_OBJECT_H
