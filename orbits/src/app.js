import {createRenderer} from './jirachi/core/gl'
import {PerspectiveCamera,orbitTarget,updateProjection,fullscreenAspectRatio,setZoom} from './jirachi/framework/Camera'
import {createTexture2d} from './jirachi/core/texture'
import Icohedron from './objects/Icohedron'
import OuterShell from './objects/OuterShell'
import RoomEnv from './objects/RoomEnv'
import MeshRibbonSystem from './objects/MeshRibbonSystem'

// ========== GL SETUP ============= //
window.appTime = 0.0;

// build renderer
let gl = createRenderer()
    .setFullscreen()
    .attachToScreen();

// build camera
var camera = PerspectiveCamera(Math.PI / 4, fullscreenAspectRatio(), 0.1, 10000);
camera = setZoom(camera,-1400.0);

// ========== OBJECT SETUP ============= //
let ico = new Icohedron(gl,1);
ico.scale = 80.0;

var img = new Image();
img.src = "/img/matcap.jpg";
let drawInner = false;
img.onload = function(){
    ico.texture = createTexture2d(gl,{
        data:img
    });
    drawInner = true;
}

// outer pink shell
var outer = new OuterShell(gl);

// room
let room = new RoomEnv(gl)

// lines
let system = new MeshRibbonSystem(gl);

window.addEventListener('resize',()=>{
    camera = updateProjection(camera);
})

animate();
function animate(){
    requestAnimationFrame(animate);
    gl.clearScreen();

    appTime += 0.01;

    // make sure camera orbits the center
    orbitTarget(camera,0.2);


    room.updateTime();
    room.draw(camera);


    gl.enable(gl.DEPTH_TEST);
    system.draw(camera);

    ico.draw(camera,shader => {
        shader.uniform('scale',ico.scale);
        if(ico.hasOwnProperty('texture')){
            ico.texture.bind();
            shader.uniform('uTex',0)
        }
    });
    ico.rotateY(1.0);
    ico.rotateZ(1.0);

    gl.enable(gl.BLEND);
    gl.enableAlphaBlending();


    outer.draw(camera);

    gl.disableBlending();




}

/*
 room.updateTime();
 room.draw(camera);



 gl.enable(gl.DEPTH_TEST);
 ico.draw(camera,shader => {
 shader.uniform('scale',ico.scale);
 if(ico.hasOwnProperty('texture')){
 ico.texture.bind();
 shader.uniform('uTex',0)
 }
 });
 ico.rotateY(2.0);
 ico.rotateZ(2.0);
 gl.enable(gl.BLEND);
 gl.enableAlphaBlending();


 outer.draw(camera);
 gl.disableBlending();


 */