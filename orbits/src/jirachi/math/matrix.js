/**
 * Mimics glm's row function. Sets the specified row in the specified matrix
 * @param matrix
 * @param rowNum
 */
export function row(matrixTarget,matrixIn,rowNum){
    switch(rowNum){
        case 0:

            break;
        case 1:

            break;

        case 2:

            break;

        case 3:

            break;
    }
}

/*
 out[0] = m00;
 out[1] = m01;
 out[2] = m02;
 out[3] = m03;
 out[4] = m10;
 out[5] = m11;
 out[6] = m12;
 out[7] = m13;
 out[8] = m20;
 out[9] = m21;
 out[10] = m22;
 out[11] = m23;
 out[12] = m30;
 out[13] = m31;
 out[14] = m32;
 out[15] = m33;
 */