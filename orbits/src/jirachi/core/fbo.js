import {createTexture2d,createTextureFormat,createDepthTexture} from "./texture"
import {logError,logWarn} from '../utils'

/**
 * A helper function for checking whether or not building a framebuffer was successful
 * @param gl {WebGLContext} a webgl rendering context.
 * @param status {Number} the value obtained from calling gl.checkFramebufferStatus
 * @returns {boolean} returns true or throws an error.
 */
function throwFBOError(gl,status) {
    switch(status){
        case gl.FRAMEBUFFER_UNSUPPORTED:
            throw new Error('gl-fbo: Framebuffer unsupported')
        case gl.FRAMEBUFFER_INCOMPLETE_ATTACHMENT:
            throw new Error('gl-fbo: Framebuffer incomplete attachment')
        case gl.FRAMEBUFFER_INCOMPLETE_DIMENSIONS:
            throw new Error('gl-fbo: Framebuffer incomplete dimensions')
        case gl.FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT:
            throw new Error('gl-fbo: Framebuffer incomplete missing attachment')

        case gl.FRAMEBUFFER_COMPLETE:
            return true;
        default:
            console.error("unknown error creating framebuffer")
            return false;

    }
}

//================== MAIN FUNCTION ======================== //

/**
 * A Function to help with setting up the parameters for creating an FBO.
 * @param gl {WebGLContext} a WebGL rendering context
 * @param attachments {Number} an optional number of color attachments to append onto the FBO. By default, it's 0
 * @returns {*}
 */
export function createFBOFormat(gl,{
    attachments=0,
    textures=[]
}={}){

    // setup attachment constants
    let attachPts = [];
    let maxAttachments;
    let ext;
    if(!gl.isWebGL2){
        ext = gl.getExtension('WEBGL_draw_buffers');
        maxAttachments = gl.getParameter(ext.MAX_COLOR_ATTACHMENTS_WEBGL);
    }else{
        maxAttachments = gl.getParameter(gl.MAX_COLOR_ATTACHMENTS)
    }

    // ensure attachments is less than max and ensure texture length is the same too
    // before storing necessary constants.
    // store associations with any texture and attachment points if necessary.
    if(attachments > 0 && attachments < maxAttachments){
        for(var i = 0; i < attachments;++i){
            if(gl.isWebGL2){
                attachPts.push({
                    texture:textures[i] || null,
                    attachPoint:gl.COLOR_ATTACHMENT0 + i
                });
            }else{
                attachPts.push({
                    texture:textures[i] || null,
                    attachPoint:gl.COLOR_ATTACHMENT0_WEBGL + i
                });
            }
        }
    }


    return {
        // the format for the fbo's texture. By default is null, will get replaced with default as texture is built or
        // use the passed in settings
        textureFormat:null,

        // array of attachment points and the texture's they're associated with
        attachments:attachPts,

        /**
         * Sets a texture format for the fbo
         * @param textureFormat {Object} an object defining the settings used for creating the framebuffer texture.
         * @returns {setTextureFormat}
         */
        setTextureFormat(textureFormat={
            format:RGBA,
            internalFormat:RGBA,
            type:UNSIGNED_BYTE,
            wrapS:CLAMP_TO_EDGE,
            wrapT:CLAMP_TO_EDGE,
            minFilter:LINEAR,
            magFilter:LINEAR,
            depth:false,
            generateMipMaps:false
        }){
            // some settings need to be changed for WebGl 2
            if(gl.isWebGL2){
                //textureFormat.internalFormat = gl.RGBA32F
            }

            this.textureFormat = createTextureFormat(textureFormat);

            for(var i in this.textureFormat){
                this[i] = this.textureFormat[i];
            }
            return this;
        }
    }
}
/**
 * Creates a WebGL framebuffer object(aka FBO)
 * @param gl {WebGLContext} a WebGLRendering context
 * @param width {Number} the width for the FBO
 * @param height {Number} the height for the FBO
 * @param format {Object} an optional format object which can be created with createFBOFormat
 */
export function createFBO(gl,width=512,height=512, {
    format = null,
    initTexture = false,
    depthTexture=false
}={}){
    let framebuffer = gl.createFramebuffer();
    let attachmentTextures = [];

    // if we haven't specified a format, create one using defaults.
    if(format === null){
        format = createFBOFormat(gl);
    }

    // =========== BUILD FBO TEXTURE =========== //
    // check to see if there's a specific texture format setup, otherwise, use the default.
    if(format.textureFormat === null){
        format.setTextureFormat();
    }

    let texture = null;
    if(initTexture !== false){
        texture = initTexture;
    }else{
        texture = createTexture2d(gl,{
            textureFormat:format.textureFormat,
            width:width,
            height:height
        });
    }


    // =========== BUILD FBO =========== //
    gl.bindFramebuffer(gl.FRAMEBUFFER,framebuffer);

    // attach primary drawing texture
    gl.framebufferTexture2D(gl.FRAMEBUFFER, gl.COLOR_ATTACHMENT0, gl.TEXTURE_2D, texture.raw, 0);


    // append texture attachments if necessary
    if(format.attachments.length > 0){

        let attLen = format.attachments.length;

        // loop through attachments and append
        for(var i = 0; i < attLen; ++i){
            let attachment = format.attachments[i];
            let aTex = attachment.texture;

            // if a texture wasn't specified for this color attachment,
            // create a new texture using default settings based on FBO
            if(aTex === null ){
                aTex = createTexture2d(gl,{
                    textureFormat:format.textureFormat,
                    width:width,
                    height:height
                });
            }else{
                gl.framebufferTexture2D(FRAMEBUFFER, attachment.attachPoint,TEXTURE_2D, aTex.raw, 0);
            }

            attachmentTextures.push(aTex);
        }
    }


    /**
     * If we need a depth texture, build it and add.
     */
    if(depthTexture){
        let depth = createDepthTexture(gl,{
            width:width,
            height:height
        });
        gl.framebufferTexture2D(FRAMEBUFFER,gl.DEPTH_ATTACHMENT,TEXTURE_2D,depth,0);

    }

    // make sure FBO is complete
    var status = gl.checkFramebufferStatus(gl.FRAMEBUFFER);
    throwFBOError(gl,status);

    gl.bindFramebuffer(gl.FRAMEBUFFER, null);



    return {
        gl:gl,
        name:"FBO",
        drawTexture:texture,
        fbo:framebuffer,
        attachments:attachmentTextures,
        getTexture(){
            return texture;
        },

        /**
         * For binding the Fbo to draw onto it.
         */
        bind(){
            gl.bindFramebuffer(FRAMEBUFFER,framebuffer);
        },

        /**
         * Unbinds the previously bound FBO, returning drawing commands to
         * the main context Framebuffer.
         */
        unbind(){
            gl.bindFramebuffer(FRAMEBUFFER,null);
        },

        /**
         * Binds the texture of the framebuffer
         * @param index the index to bind the texture to
         */
        bindTexture(index=0){
            this.drawTexture.bind(index);
        },

        /**
         * Helper function for unbinding the currently bound
         * texture.
         */
        unbindTexture(){
            gl.bindTexture(gl.TEXTURE_2D,null);
        }
    }
}
