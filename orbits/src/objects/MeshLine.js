import Mesh from '../jirachi/framework/Mesh'
import {randInt} from '../jirachi/math/core'
import vert from '../shaders/meshline.vert'
import frag from '../shaders/meshline.frag'

// Nothing special here, pretty much a direct port from @spite's Three.js MeshLine
// https://github.com/spite/THREE.MeshLine/blob/master/src/THREE.MeshLine.js
class MeshLine extends Mesh{
    constructor(gl){
        super(gl,{
            vertex:vert,
            fragment:frag,
            uniforms:[
                'map',
                'alphaMap',
                'useMap',
                'useAlphaMap',
                'dashArray',
                'visibility',
                'alphaTest',
                'repeat',
                'resolution',
                'lineWidth',
                'color',
                'opacity',
                'near',
                'far',
                'time',
                'sizeAttenuation'
            ]
        })
        this.mode = gl.TRIANGLES;
        this.params = {
            map:[],
            alphaMap:[],
            useMap:0,
            useAlphaMap:0,
            color:[1,0,0],
            opacity:1,
            alphaTest:0.2,
            resolution:[window.innerWidth,window.innerHeight],
            sizeAttenuation:false,
            lineWidth:randInt(3,15),
            near:0.1,
            far:1000.0
        }

        window.addEventListener('resize',()=>{
            this.params.resolution = [window.innerWidth,window.innerHeight]
        });

    }

    draw(camera){
        let shader = this.shader;
        this.shader.bind();


        // update normal matrix
        this.calculateNormalMatrix(camera.view);

        shader.set4x4Uniform('projectionMatrix',camera.projection);
        shader.set4x4Uniform('modelMatrix',this.model);
        shader.set4x4Uniform('viewMatrix',camera.view)
        shader.uniform('time',appTime)

        for(var i in this.params){
            shader.uniform(i,this.params[i]);
        }

        // bind vao
        this.vao.bind();

        this.gl.drawElements(this.mode,this.numVertices,UNSIGNED_SHORT,0);
        // unbind vao
        this.vao.unbind();
    }

    buildMesh(startingVertices){
        let gl = this.gl;
        this.positions = [];
        this.next = [];
        this.side = [];
        this.width = [];
        this.indices = [];
        this.uvs = [];
        this.counters = [];

        let g = startingVertices;
        for( var j = 0; j < g.length; j += 3 ) {
            var c = j/g.length;
            this.positions.push( g[ j ], g[ j + 1 ], g[ j + 2 ] );
            this.positions.push( g[ j ], g[ j + 1 ], g[ j + 2 ] );
            this.counters.push(c);
            this.counters.push(c);
        }


        this._process();


        this.addAttribute('position',this.positions);
        this.addAttribute('previous',this.previous);
        this.addAttribute('next',this.next);
        this.addAttribute('side',this.side,1);
        this.addAttribute('width',this.width,1);
        this.addAttribute('uv',this.uvs,2);
        this.addAttribute('counters',this.counters,1);
        this.addIndices(this.indices);



    }

    _compareV3(a,b){
        var aa = a * 6;
        var ab = b * 6;
        return ( this.positions[ aa ] === this.positions[ ab ] ) && ( this.positions[ aa + 1 ] === this.positions[ ab + 1 ] ) && ( this.positions[ aa + 2 ] === this.positions[ ab + 2 ] );

    }

    _copyV3(a){
        var aa = a * 6;
        return [ this.positions[ aa ], this.positions[ aa + 1 ], this.positions[ aa + 2 ] ];
    }

    _process(){

        var l = this.positions.length / 6;

        this.previous = [];
        this.next = [];
        this.side = [];
        this.width = [];
        this.indices = [];
        this.uvs = [];

        for( var j = 0; j < l; j++ ) {
            this.side.push( 1 );
            this.side.push( -1 );
        }

        var w;
        for( var j = 0; j < l; j++ ) {
            if( this.widthCallback ) w = this.widthCallback( j / ( l -1 ) );
            else w = 1;
            this.width.push( w );
            this.width.push( w );
        }

        for( var j = 0; j < l; j++ ) {
            this.uvs.push( j / ( l - 1 ), 0 );
            this.uvs.push( j / ( l - 1 ), 1 );
        }

        var v;

        if( this._compareV3( 0, l - 1 ) ){
            v = this._copyV3( l - 2 );
        } else {
            v = this._copyV3( 0 );
        }
        this.previous.push( v[ 0 ], v[ 1 ], v[ 2 ] );
        this.previous.push( v[ 0 ], v[ 1 ], v[ 2 ] );
        for( var j = 0; j < l - 1; j++ ) {
            v = this._copyV3( j );
            this.previous.push( v[ 0 ], v[ 1 ], v[ 2 ] );
            this.previous.push( v[ 0 ], v[ 1 ], v[ 2 ] );
        }

        for( var j = 1; j < l; j++ ) {
            v = this._copyV3( j );
            this.next.push( v[ 0 ], v[ 1 ], v[ 2 ] );
            this.next.push( v[ 0 ], v[ 1 ], v[ 2 ] );
        }

        if( this._compareV3( l - 1, 0 ) ){
            v = this._copyV3( 1 );
        } else {
            v = this._copyV3( l - 1 );
        }
        this.next.push( v[ 0 ], v[ 1 ], v[ 2 ] );
        this.next.push( v[ 0 ], v[ 1 ], v[ 2 ] );

        for( var j = 0; j < l - 1; j++ ) {
            var n = j * 2;
            this.indices.push( n, n + 1, n + 2 );
            this.indices.push( n + 2, n + 1, n + 3 );
        }

    }

    _memcpy (src, srcOffset, dst, dstOffset, length) {
        var i

        src = src.subarray || src.slice ? src : src.buffer
        dst = dst.subarray || dst.slice ? dst : dst.buffer

        src = srcOffset ? src.subarray ?
                src.subarray(srcOffset, length && srcOffset + length) :
                src.slice(srcOffset, length && srcOffset + length) : src

        if (dst.set) {
            dst.set(src, dstOffset)
        } else {
            for (i=0; i<src.length; i++) {
                dst[i + dstOffset] = src[i]
            }
        }

        return dst
    }

    advance(position){
        var positions = this.positions;
        var previous = this.previous;
        var next = this.next;
        var l = positions.length;

        // PREVIOUS
        this._memcpy( positions, 0, previous, 0, l );

        // POSITIONS
        this._memcpy( positions, 6, positions, 0, l - 6 );

        positions[l - 6] = position[0];
        positions[l - 5] = position[1];
        positions[l - 4] = position[2];
        positions[l - 3] = position[0];
        positions[l - 2] = position[1];
        positions[l - 1] = position[2];

        // NEXT
        this._memcpy( positions, 6, next, 0, l - 6 );

        next[l - 6]  = position[0]
        next[l - 5]  = position[1];
        next[l - 4]  = position[2];
        next[l - 3]  = position[0];
        next[l - 2]  = position[1];
        next[l - 1]  = position[2];

        this.positions = positions;
        this.previous = previous;
        this.next = next;

        this.updateAttribute('position',this.positions);
        this.updateAttribute('previous',this.previous);
        this.updateAttribute('next',next);

    }

}

export default MeshLine;