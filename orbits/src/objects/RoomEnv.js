import Mesh from '../jirachi/framework/Mesh'
import {flattenArray} from '../jirachi/math/core'
import vert from '../shaders/room.vert'
import frag from '../shaders/room.frag'


// Largely based on Robert Hodgin's Eyeo 2012 code.
// roberthodgin.com/portfolio/work/eyeo-2012/
// Lotta small tweaks to get things looking close to right.
const MAX_TIMEULTI = 120.0;

class RoomEnv extends Mesh{
    constructor(gl){
        super(gl,{
            vertex:vert,
            fragment:frag,
            uniforms:[
                'eyePos',
                'power',
                'lightPower',
                'roomDimensions',
                'timePer',
                'uLightColor'
            ]
        })

        this.mBoxSize = [800,350,800];
        this.isPowerOn = false;
        this.mPower = 1.0;
        this.mTime = window.appTime;
        this.mTimeAdjusted= 0.0;
        this.mTimeMulti = 60.0;
        this.mTimeElapsed = 0.0;
        this.mTimer = 0.0;
        this.mTick = false;

        this.mPower = 1.0;

        // scale model matrix according to room size
        this.scale(this.mBoxSize);


        this._buildMesh();
    }

    updateTime(saveFrame=false){
        let isPowerOn = this.isPowerOn;
        if( isPowerOn ) this.mPower -= ( this.mPower - 1.0 ) * 0.2;
            else this.mPower -= ( this.mPower - 0.0 ) * 0.2;

        let prevTime = this.mTime;
        this.mTime = window.appTime;
        let dt = this.mTime - prevTime;

        if( saveFrame ) dt = 1.0/60.0;

        this.mTimeAdjusted = dt * this.mTimeMulti;
        this.mTimeElapsed += this.mTimeAdjusted;

        this.mTimer += this.mTimeAdjusted;
        this.mTick = false;

        if( this.mTimer > 1.0 ){
            this.mTick = true;
            this.mTimer = 0.0;
        }
    }

    draw(camera){
        let shader = this.shader;
        let gl = this.gl;
        let vao = this.vao;

        gl.enable(gl.CULL_FACE);
        gl.cullFace(gl.BACK);
        shader.bind();


        shader.set4x4Uniform('projectionMatrix',camera.projection);
        shader.uniform("viewMatrix",camera.view);
        shader.uniform("view",camera.view);
        shader.uniform("modelMatrix",this.model);

        shader.uniform('eyePos',camera.eye);
        shader.uniform('roomDimensions',this.mBoxSize);
        shader.uniform('lightPower',this.getLightPower());
        shader.uniform('power',this.mPower);
        shader.uniform('timePer',this.getTimePer() * 1.5 + 0.5);
        shader.uniform('time',appTime);


        vao.bind();
        gl.drawElements(this.mode,this.numVertices,UNSIGNED_SHORT,0);
        vao.unbind();
        gl.disable(gl.CULL_FACE);
    }

    getPower(){
        return this.mPower;
    }

    getLightPower(){
        let p = this.getPower() * 5.0 * Math.PI;
        let lightPower = Math.cos(p) * 0.5;
        return lightPower;
    }
    getTimePer(){
        return this.mTimeMulti / MAX_TIMEULTI;
    }
    _buildMesh(){
        let posCoords = [];
        let indices = [];
        let texCoords = [];
        let X = 1.0;
        let Y = 1.0;
        let Z = 1.0;
        let index = 0;

        let vVerts = [
            [-X,-Y,-Z],[-X,-Y,Z],
            [X,-Y,Z],[X,-Y,-Z],
            [-X,Y,-Z],[-X,Y,Z],
            [X,Y,Z],[X,Y,-Z]
        ];

        let vIndices = [
            [0,1,3],[1,2,3],
            [4,7,5],[7,6,5],
            [0,4,1],[4,5,1],
            [2,6,3],[6,7,3],
            [1,5,2],[5,6,2],
            [3,7,0],[7,4,0]
        ]


        // just writing out normals
        // by hand as the method used doesn't translate well from c++
        let normals = [

            [0,1,0],
            [0,1,0],
            [0,1,0],
            [0,1,0],
            [0,1,0],
            [0,1,0],


            [0,-1,0],
            [0,-1,0],
            [0,-1,0],
            [0,-1,0],
            [0,-1,0],
            [0,-1,0],


            [1,0,0],
            [1,0,0],
            [1,0,0],
            [1,0,0],
            [1,0,0],
            [1,0,0],

            [-1,0,0],
            [-1,0,0],
            [-1,0,0],
            [-1,0,0],
            [-1,0,0],
            [-1,0,0],

            [0,0,-1],
            [0,0,-1],
            [0,0,-1],
            [0,0,-1],
            [0,0,-1],
            [0,0,-1],

            [0,0,1],
            [0,0,1],
            [0,0,1],
            [0,0,1],
            [0,0,1],
            [0,0,1]
        ]

            let vTexCoords = [
            [0,0],
            [0,1],
            [1,1],
            [1,0]
        ]

        let tIndices = [
            [0,1,3],[1,2,3],
            [0,1,3],[1,2,3],
            [0,1,3],[1,2,3],
            [0,1,3],[1,2,3],
            [0,1,3],[1,2,3],
            [0,1,3],[1,2,3]
        ]


        for(let i = 0; i < 12; ++i){
            posCoords.push(vVerts[vIndices[i][0]])
            posCoords.push(vVerts[vIndices[i][1]])
            posCoords.push(vVerts[vIndices[i][2]])
            indices.push(index++,index++,index++);
            texCoords.push(vTexCoords[tIndices[i][0]])
            texCoords.push(vTexCoords[tIndices[i][1]])
            texCoords.push(vTexCoords[tIndices[i][2]])
        }


        this.addAttribute('position',flattenArray(posCoords));
        this.addAttribute('normal',flattenArray(normals));
        this.addAttribute('uv',flattenArray(texCoords));
        this.addIndices(indices);

    }
}

export default RoomEnv;