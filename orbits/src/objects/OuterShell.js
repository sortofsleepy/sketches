import Icohedron from './Icohedron'
import {loadDDS} from '../jirachi/framework/ImageLoader'
import {createCubemap,createTextureFormat,createCubemapFromDDS} from '../jirachi/core/texture'


import pbr from '../shaders/pbr.glsl'
import vert from '../shaders/outershell.vert'
import frag from '../shaders/outershell.frag'


class OuterShell {
    constructor(gl){

        this.shape = new Icohedron(gl,2,{
            vertex:vert,
            fragment:[pbr,frag],
            uniforms:[
                'scale',
                'loaded',
                'uIrradianceCubeMap',
                'uRadianceCubeMap',
                'viewInverse',
                'uMetalic',
                'uSpecular',
                'uGamma',
                'uExposure',
                'uRoughness'
            ]
        });

        this.irradianceMap = [
            'img/irr_posx.png',
            'img/irr_negx.png',
            'img/irr_posy.png',
            'img/irr_negy.png',
            'img/irr_posz.png',
            'img/irr_negz.png',
        ]
        this.loaded = false;

        this.shape.scale = 20.0;
        this.gl = gl;

        this.pbr = {
            uMetallic:1.0,
            uSpecular:1.0,
            uGamma:2.2,
            uExposure:3.0,
            uRoughness:0.05
        }



        this._loadMap();
    }


    draw(camera){
        let gl = this.gl;
        let shape = this.shape;

        // update normal matrix
        shape.calculateNormalMatrix(camera.view);
        shape.rotateY(1.0);
        shape.rotateZ(1.0);
        shape.rotateX(1.0);
        // only draw once all textures are loaded.
        if(this.loaded === true) {

            shape.draw(camera, shader => {
                shader.uniform('scale', 140.0);

                this.irradianceCubeMap.bind(0);
                this.radianceMap.bind(1)

                shader.setTextureUniform('uIrradianceMap', 0)
                shader.setTextureUniform('uRadianceCubeMap', 1);
                shader.set3x3Uniform('normalMatrix',shape.normalMatrix);
                shader.set4x4Uniform('viewInverse',camera.getInverseView());
                shader.set4x4Uniform('modelMatrix',shape.model);

                for(var i in this.pbr){
                    shader.uniform(i,this.pbr[i]);
                }

            })
        }

    }

    _loadMap(){
        let gl = this.gl;
        let irrMap = this.irradianceMap;
        let images = [];
        let count = 0;

        irrMap.forEach(itm => {
            let img = new Image();
            img.src = itm;
            img.onload = () => {
                images.push(img);
                count += 1;
            }
        });

        let timer = setInterval(()=>{
            if(count === 6){
                console.log("Outer shell irradiance map loaded")
                this.irradianceCubeMap = createCubemap(gl,images);

                loadDDS('img/studio_radiance.dds').then(dds => {
                    console.log("Outer shell radiance map loaded")
                    this.radianceMap = createCubemapFromDDS(gl,dds);
                    this.loaded = true;
                }).catch(err => {
                    console.log("error",err)
                });

                clearInterval(timer);

            }
        })
    }
}


export default OuterShell