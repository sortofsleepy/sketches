

class Light {
    constructor(){
        this.props = {
            mColorAmbient: [0,0,0],
            mColorDiffuse: [1,1,1],
            mColorSpecular: [1,1,1],
            mIntensity: 0.5,
            mRadius: 0.0,
            mVolume: 1.0,
            mPosition: [0,0,0],
            isOn: true
        }
    }

    /**
     * Sends all of the light's props to the passed in shader
     * @param shader
     */
    updateShader(shader){
        for(var i in this.props){
            shader.uniform(i,this.props[i]);
        }
    }
}

export default Light;