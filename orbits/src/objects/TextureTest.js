import {createPlane} from '../geometry/Plane'
import Mesh from '../jirachi/framework/Mesh'
import vert from '../shaders/texture.vert'
import frag from '../shaders/texture.frag'
import {flattenArray} from '../jirachi/math/core'
import {createTexture2d} from '../jirachi/core/texture'

// testing idea for GPU ribbons. Gonna be tricky :p
class TextureTest extends Mesh{
    constructor(gl){
        super(gl,{
            vertex:vert,
            fragment:frag,
            uniforms:[
                'meshTest'
            ]
        })

        let p = createPlane(400,400);

        this.addAttribute('position',flattenArray(p.positions));
        this.addAttribute('uv',flattenArray(p.uvs));
        this.addIndices(flattenArray(p.cells))

        this._buildTexture();
    }

    _buildTexture(){
        let width = 3;
        let height = 3;
        var data = [];


        for(let i = 0; i < width; i += 1){
            for(let j = 0; j < height; j += 1) {
                data.push([i,j,0])
            }
        }

        data = flattenArray(data);
        let fdata = new Float32Array(width * height * 4);

        let tex = createTexture2d(this.gl,{
            width:3,
            height:3,
            data:fdata
        })

    }
}

export default TextureTest