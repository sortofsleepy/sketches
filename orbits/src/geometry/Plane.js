
// direct port from here
// https://github.com/vorg/primitive-plane
// exported so it's easier to include in Rollup, also ignoring quad parameter

export function createPlane(sx,sy,nx,ny){
    sx = sx || 1
    sy = sy || 1
    nx = nx || 1
    ny = ny || 1

    var positions = []
    var uvs = []
    var normals = []
    var cells = []

    for (var iy = 0; iy <= ny; iy++) {
        for (var ix = 0; ix <= nx; ix++) {
            var u = ix / nx
            var v = iy / ny
            var x = -sx / 2 + u * sx // starts on the left
            var y = sy / 2 - v * sy // starts at the top
            positions.push([x, y, 0])
            uvs.push([u, v])
            normals.push([0, 0, 1])
            if (iy < ny && ix < nx) {
                cells.push([iy * (nx + 1) + ix, (iy + 1) * (nx + 1) + ix + 1, iy * (nx + 1) + ix + 1])
                cells.push([(iy + 1) * (nx + 1) + ix + 1, iy * (nx + 1) + ix, (iy + 1) * (nx + 1) + ix])
            }
        }
    }

    return {
        positions: positions,
        normals: normals,
        uvs: uvs,
        cells: cells
    }
}