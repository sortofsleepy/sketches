import FxPass from '../libs/jirachi/post/FxPass'
import abrasion from '../shaders/post/abrasion.glsl'



class AcsiPass extends FxPass{
    constructor(gl){
        super(gl,abrasion);
        this.time = 0.0;
    }

    runPass(){
        this.time += 0.01;
        this.fbo.bind();
        this.gl.clearScreen();
        if(this.input !== null){
            this.input.bind();
        }
        this.drawQuad.drawWithCallback(shader => {
            shader.uniform('resolution',this.resolution);
            shader.uniform('time',this.time);
            if(this.input !== null){
                shader.setTextureUniform('tex0',0);
            }
        })
        this.fbo.unbind();
    }
}

export default AcsiPass;