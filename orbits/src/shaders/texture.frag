precision highp float;

uniform bool meshTest;


out vec4 glFragColor;

void main(){
    if(meshTest){
        glFragColor = vec4(1.0,1.0,0.0,1.0);
    }else{
        glFragColor = vec4(1.);
    }
}
