
precision highp float;

uniform sampler2D uTex;

out vec4 glFragColor;

in mat3 vNormalMatrix;
in vec3 vE;
in float vId;
in vec3 vPosition;
void main(){

    vec3 x = dFdx(vPosition);
    vec3 y = dFdy(vPosition);
    vec3 normal = normalize(cross(x,y));

    vec3 n = normalize(vNormalMatrix * normal);
    vec3 r = reflect(vE,n);
    float m = 2. * sqrt(
        pow(r.x,2.) +
        pow(r.y,2.) +
        pow(r.z + 1.,2.)
    );

    vec2 vN = r.xy / m + .5;
    vec4 tex = texture(uTex,vN);

    glFragColor = tex;
}