


uniform mat4 projectionMatrix;
uniform mat4 modelMatrix;
uniform mat4 viewMatrix;
uniform mat3 normalMatrix;
uniform mat4 viewInverse;

uniform float scale;

in vec3 position;

out vec3 vPosition;
out vec3 vWsPosition;
out vec3 vEyePosition;
out mat3 vNormalMatrix;
out mat4 vViewInverse;
void main(){
    vec3 pos = position;
    pos.x *= scale;
    pos.y *= scale;
    pos.z *= scale;

    vec4 worldSpacePosition = modelMatrix * vec4(position,1.);
    vec4 viewSpacePosition = viewMatrix * worldSpacePosition;

    vec4 eyeDirViewSpace = viewSpacePosition - vec4(0.0,0.0,0.0,1.0);

    vPosition = position;
    vWsPosition = worldSpacePosition.xyz;
    vEyePosition = -vec3(viewMatrix * eyeDirViewSpace);
    vNormalMatrix = normalMatrix;
    vViewInverse = viewInverse;

    gl_Position = projectionMatrix * viewMatrix * modelMatrix * vec4(pos,1);
}