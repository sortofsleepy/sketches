
uniform mat4 projectionMatrix;
uniform mat4 modelMatrix;
uniform mat4 viewMatrix;
uniform mat3 normalMatrix;

uniform float scale;

in vec3 position;
in float ids;

out float vId;
out mat3 vNormalMatrix;
out vec3 vE;
out vec3 vPosition;

void main(){
    vec4 pos = vec4(position,1.);
    pos.x *= scale;
    pos.y *= scale;
    pos.z *= scale;

    vec4 mvMatrix = modelMatrix * viewMatrix * vec4(1.);
    vec3 e = normalize(vec3(mvMatrix * pos));

    vId = ids;
    vE = e;
    vPosition = pos.xyz;
    vNormalMatrix = normalMatrix;

    gl_Position = projectionMatrix * viewMatrix * modelMatrix * pos;
}