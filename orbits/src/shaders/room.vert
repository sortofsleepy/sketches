
uniform mat4 projectionMatrix;
uniform mat4 modelMatrix;
uniform mat4 viewMatrix;
uniform vec3 eyePos;
uniform vec3 roomDimensions;

in vec3 position;
in vec2 uv;
in vec3 normal;

out vec2 vUv;
out vec3 vEyeDir;
out vec3 vNormal;
out vec4 vVertex;

void main(){
    vec3 pos = position;

    vVertex = modelMatrix * vec4(pos,1.);
    vNormal = normal;
    vEyeDir = normalize(eyePos - vVertex.xyz);
    vUv = uv;

    gl_Position = projectionMatrix * viewMatrix * vVertex;
}