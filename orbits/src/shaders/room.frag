precision highp float;

uniform vec3 eyePos;
uniform float power;
uniform float lightPower;
uniform vec3 roomDimensions;
uniform float timePer;
uniform float time;
uniform vec4 uLightColor;

in vec3 eyeDir;
in vec4 vVertex;
in vec3 vNormal;

vec3 v3( float x, float y, float z ){
	return vec3( x, y, z );
}

vec3 h2rgb( float hue ){

  float h = abs(hue - floor(hue)) * 6.;
  vec3 c = vec3( 0., 0., 0. );

  int f = int(floor( h ));

  if(f==0)c=v3(1.,h,0.);else if(f==1)c=v3(2.-h,1.,0.);else if(f==2)c=v3(0.,1.,h-2.);else if(f==3)c=v3(0.,4.-h,1.);else if(f==4)c=v3(h-4.,0.,1.);else c=v3(1.,0.,6.-h);
  return c;
}

out vec4 glFragColor;

void main(){


vec4 pos = normalize(vVertex);
  float aoLight = 1.8 - length( vVertex.xyz ) * ( 0.0015 + ( power * 0.0015 ) );

  float ceiling = 0.0;
  if( vNormal.y < -1.0 || vNormal.y < -1.0 ){
    ceiling = 1.0;
  }

  float yPer = clamp( vVertex.y / roomDimensions.y, 0.0, 1.0 );
  float ceilingGlow = pow( yPer, 4.0 ) * .25;
  ceilingGlow += pow( yPer, 20.0 );
  ceilingGlow += pow( max( yPer - 4.7, 0.0 ), 5.0 );


  vec3 litRoomColor = vec3( aoLight + ( ceiling + ceilingGlow * timePer ) * lightPower);
  litRoomColor *= h2rgb(time / 10.0);

  glFragColor = vec4(litRoomColor,1.);
}
