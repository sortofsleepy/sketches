
uniform mat4 projectionMatrix;
uniform mat4 modelMatrix;
uniform mat4 viewMatrix;


in vec3 position;
in vec2 uv;
in vec3 normal;


out vec2 vUv;
void main(){
    vec3 pos = position;
    vUv = uv;
    gl_Position = projectionMatrix * viewMatrix * vec4(pos,1.);
}