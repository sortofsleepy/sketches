module.exports = {
    loaders:[
        {
            test:/\.worker.js$/,
            exclude:/(node_modules | bower_components)/,
            use:"worker-loader"
        }
    ]
}