import Cube from './objects/cube'
import Background from './objects/bg'
import createFramebuffer from './jirachi/core/fbo'
let cube,bg,fbo;

export function setupSketch(gl){

    cube = new Cube(gl);
    bg = new Background(gl);

    fbo = createFramebuffer(gl,{
        width:window.innerWidth,
        height:window.innerHeight
    })
}


/**
 * Main sketch - note everything in this function is already in an rAF
 * @param gl
 * @param camera
 */
export function startSketch(gl,camera,time){
    fbo.bind();
    gl.clearScreen();
    cube.draw(camera,time);
    fbo.unbind();


    gl.disableBlend();
    gl.enableDepth();
    gl.enableDepthWrite()
    cube.draw(camera,time);

    gl.disableDepth();
    //gl.enableAlphaBlending();
    gl.enableAdditiveBlending();
    bg.draw(time,fbo.getTexture());


}


/*
  gl.disableBlend();
    gl.enableDepth();
    gl.enableDepthWrite()
    cube.draw(camera,time);

    gl.disableDepth();
    gl.enableAdditiveBlending();
    bg.draw(time);
 */