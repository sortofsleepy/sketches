import {logError,isInt} from "../utils";

export default function(gl,spec={}){

    let shader = gl.createProgram();

    for(let i in Shader.prototype){
        shader[i] = Shader.prototype[i];
    }


    let glsl = new Shader(gl,spec);
    Object.assign(shader,glsl);


    shader.parseSpec();


    return shader;

}


// ========= DEFINITION ============== //

function Shader(gl,spec = null){
    this.gl = gl;
    this.uniforms = [];
    this.attributes = {};


    this.spec = spec !== null ? spec : {};

    // default precision for floating point values.
    // TODO incorperate precision specification for multiple types.
    this.precision = "highp float";

    if(this.spec.hasOwnProperty("precision")){
        this.precision = this.spec.precision;
    }



}

Shader.prototype = {

    bind(){

        this.gl.useProgram(this);
    },

    /**
     * A general catch-all uniform setting function so you don't have to use a specific uniform function
     * @param name {String} name of the uniform value.
     * @param value {*} the value to set for the uniform.
     */
    uniform(name,value){

        // first, separate whether or not we're dealing with a number vs a matrix / vector
        // if array it's either a vector or matrix, otherwise it's a float or int
        // We also need to distinguish and see if it's a typed array or a regular array.
        if(value instanceof Array || Object.prototype.toString.call(value.buffer) === "[object ArrayBuffer]"){


            switch(value.length){
                case 2:
                    this.setVec2(name,value);
                    break;
                case 3:
                    this.setVec3(name,value);
                    break;
                case 9:
                    this.setMat3(name,value);
                    break;
                case 16:

                    this.setMat4(name,value);
                    break;
            }


        } else {

            // determine if value is int or float
            // TODO this is the tricky part - what if we need to pass something like 1.00 ?
            if(isInt(value)){
                this.setInt(name,value);
            }else{
                this.setFloat(name,value);
            }

        }
    },

    setVec2(name,value){
        if(value instanceof Array && value.length === 2){
            this.gl.uniform2fv(this.uniforms[name].location,value);
        }

        return this
    },

    setVec3(name,value){

        this._uniformLookup(name);
        if(value instanceof Array && value.length === 3 && this.uniforms[name] !== undefined) {
            this.gl.uniform3fv(this.uniforms[name].location, value);
        }
    },
    /**
     * Sets a floating point value uniform
     * @param name {String} name of the uniform
     * @param value {Number} value of the uniform
     */
    setFloat(name,value){

        this._uniformLookup(name);
        if(this.uniforms[name] !== undefined){
            this.gl.uniform1f(this.uniforms[name].location,value);
        }

        return this;
    },

    /**
     * sets a integer uniform variable. note that this method only works
     * if the uniform is currently in use
     * @param name {String} name of the uniform
     * @param value {Number} value
     * @returns {Shader} returns shader for chaining.
     */
    setInt(name, value){

        this._uniformLookup(name);

        this.gl.uniform1i(this.uniforms[name].location,value);
        return this;
    },

    /**
     * sets a mat4 uniform variable. note that this method only works
     * if the uniform is currently in use
     * @param name {String} name of the uniform
     * @param value {Array} 16 item array of the value
     * @returns {Shader} returns shader for chaining.
     */
    setMat4(name,value){
        // check to make sure value is an array and has the right number of elements.
        if(value instanceof Array || value.length === 16){
            this.gl.uniformMatrix4fv(this.uniforms[name].location,false,value);
        }

        return this;
    },

    /**
     * sets a mat3 uniform variable. note that this method only works
     * if the uniform is currently in use
     * @param name {String} name of the uniform
     * @param value {Array} 9 item array of the value
     * @returns {Shader} returns shader for chaining.
     */
    setMat3(name,value){
        // check to make sure value is an array and has the right number of elements.
        if(value instanceof Array && value.length === 9){
            this.gl.uniformMatrix3fv(this.uniforms[name].location,false,value);
        }

        return this;
    },


    /**
     * Looks through shader and parses the attribute data of any currently in-use
     * attribute values
     */
    parseActiveAttributes(){
        let gl = this.gl;

        let attribs = gl.getProgramParameter(this, gl.ACTIVE_ATTRIBUTES);


        for(let i = 0; i < attribs; ++i){
            let attrib = gl.getActiveAttrib(this,i);
            let loc = gl.getAttribLocation(this,attrib.name) ;

            this.attributes[attrib.name] = {
                size:attrib.size,
                name:attrib.name,
                type:attrib.type,
                location:loc
            };
        }
    },

    /**
     * Looks through shader and parses the attribute data of any currently in-use
     * uniform values
     */
    parseActiveUniforms(){
        let gl = this.gl;

        // set uniforms and their locations (plus default values if specified)
        let numUniforms = gl.getProgramParameter(this,gl.ACTIVE_UNIFORMS);

        // first loop through and save all active uniforms.
        for(let i = 0; i < numUniforms; ++i){
            let info = gl.getActiveUniform(this,i);
            let location = gl.getUniformLocation(this,info.name);
            this.uniforms[info.name] = {
                location:location,
                name:info.name
            };
        }
    },

    delete(){
        this.gl.deleteProgram(this);
    },
    /**
     * Loads a shader
     * @param vertex {String} vertex source
     * @param fragment {String} fragment source.
     */
    loadShader(vertex,fragment){
        let gl = this.gl;
        let vShader = this.compileShader(gl.VERTEX_SHADER,vertex);
        let fShader = this.compileShader(gl.FRAGMENT_SHADER,fragment);

        if(vShader !== false && fShader !== false) {

            gl.attachShader(this,vShader);
            gl.attachShader(this,fShader);


            gl.linkProgram(this);

            // TODO is this really necessary?
            gl.deleteShader(vShader);
            gl.deleteShader(fShader);

            if(!gl.getProgramParameter(this,gl.LINK_STATUS)){
                logError("Could not initialize WebGLProgram");
                throw ("Couldn't link shader program - " + gl.getProgramInfoLog(this));
                return false;
            }else{
                return this;
            }


        }

    },

    /**
     * Compiles a shader.
     * @param type
     * @param source
     * @returns {*}
     */
    compileShader(type,source){
        let gl = this.gl;
        let shader = gl.createShader(type);
        let shaderTypeName = ""

        // get the string name of the type of shader we're trying to compile.
        if(type === gl.FRAGMENT_SHADER){
            shaderTypeName = "FRAGMENT"
        }else{
            shaderTypeName = "VERTEX";
        }

        gl.shaderSource(shader, source);
        gl.compileShader(shader);

        if (!gl.getShaderParameter(shader, gl.COMPILE_STATUS)) {
            logError(`Error in ${shaderTypeName} shader compilation - ` + gl.getShaderInfoLog(shader),true);
            return false;
        } else {
            return shader;
        }
    },
    /**
     * Parses the shader specification
     */
    parseSpec(){
        let spec = this.spec;

        if((spec.hasOwnProperty("vertex") || spec.hasOwnProperty("vertexShader")) &&
            spec.hasOwnProperty("fragment") || spec.hasOwnProperty("fragmentShader")){

            let vertex = ""
            let fragment = ""
            let version = "300 es";


            if(spec.hasOwnProperty("vertex")){
                vertex = spec.vertex
            }else{
                vertex = spec.vertexShader
            }

            if(spec.hasOwnProperty("fragment")){
                fragment = spec.fragment
            }else{
                fragment = spec.fragmentShader
            }

            if(spec.hasOwnProperty("version")){
                version = spec.version;
            }

            if(vertex instanceof Array){
                vertex = this._unrollChunks(vertex,version);
            }else{
                vertex = `#version ${version} \n` + vertex;
            }

            if(fragment instanceof Array){
                fragment = this._unrollChunks(fragment,version);
            }else{
                fragment = `#version ${version} \n precision ${this.precision}; \n` + fragment;
            }



            this.vertexSource = vertex;
            this.fragmentSource = fragment;

            this.loadShader(vertex,fragment);
            this.parseActiveAttributes();
            this.parseActiveUniforms();
        }
    },


    /**
     * If a shader was specified as an array, this unrolls all the components of that array into one string.
     * @param shader {string} the shader source to unroll.
     * @param version {string} the version declaration
     * @returns {string} the unrolled shader source.
     * @private
     */
    _unrollChunks(shader,version){

        let fin = `#version ${version} \n precision ${this.precision}; \n`;

        shader.forEach(chunk => {
            fin += chunk + "\n";
        });


        return fin;
    },



    _uniformLookup(name){
        if(this.uniforms[name] === undefined){
            if(window.debug){
                throw new Error("Unable to find uniform " + name);
            }
        }
        return;
    }


};

