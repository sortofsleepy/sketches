import createVAO from './vao'
import createVBO from './vbo'
import strings from '../strings'
import createShader from './shader'
/**
 * Attempt at making drawing a bit more like Vulkan
 * Code adapted from David Li's code.
 */
class DrawState {
    constructor(gl,{
        fullscreen=true
    }={}){

        this.framebuffer = null;
        this.shader = null;
        this.vao = createVAO(gl);
        this.gl = gl;
        this.blending = false;
        this.blendFunc = null;
        this.hasIndexBuffer = false;
        this.shouldClearFramebuffer = true;
        this.viewport = [0,0,window.innerWidth,window.innerHeight];
        this.clearColor = [0,0,0,0];

        this.shaderSet = false;

        this.isInstanced = false;

        // reference to all textures that need to be bound for the state.
        this.textures = {}

        // list of attributes
        this.attributes = {};

        // if we're drawing to the main viewport.
        if(fullscreen){
            window.addEventListener('resize',() => {
                this.viewport = [0,0,window.innerWidth,window.innerHeight];
            })
        }
    }

    /**
     * Adds a texture to the state to be bound
     * @param texture {WebGLTexture} the texture to bind
     * @param unit {Number} the texture unit to bind to.
     * @returns {DrawState}
     */
    addTexture(texture,unit=0){
        this.textures[unit] = texture;
        return this;
    }

    addMesh(){

    }

    /**
     * Sets the viewport for the state.
     * @param x {Number} the x coordinate.
     * @param y {Number} the y coordinate
     * @param width {Number} width of the viewport
     * @param height {Number} height of the viewport.
     * @returns {DrawState}
     */
    viewport(x=0,y=0,width=window.innerWidth,height=window.innerHeight){
        this.viewport = [x,y,width,height];
        return this;
    }

    setClearColor(r=0,g=0,b=0,a=1){
        this.clearColor[0] = r;
        this.clearColor[1] = g;
        this.clearColor[2] = b;
        this.clearColor[3] = a;
        return this;
    }

    /**
     * Enables blending
     * @param func {String} the blending function to use.
     * @returns {DrawState}
     */
    enableBlending(func=null){
        this.blending = true;
        if(func !== null){
            this.setBlendingFunction(func);
        }
        return this;
    }

    // set a blending function to run when blending is turned on.
    setBlendingFunction(func){
        this.blendFunc = func;
        return this;
    }

    disableBlending(){
        this.blending = false;
        return this;
    }

    setFramebuffer(fbo){
        this.framebuffer = fbo;
        return this;
    }

    /**
     * Sets the shader for the state
     * @param shader {*} Can be a WebGLProgram instance or a Object with the keys vertex and fragment.
     * @returns {DrawState}
     */
    setShader(shader={
        vertex:"",
        fragment:""
    }){

        if(shader instanceof WebGLProgram){
            this.shader = shader;
            this.shaderSet = true;
        }else if(shader instanceof Object && shader.vertex !== "" && shader.fragment !== ""){

            shader = createShader(this.gl,{
                vertex:shader.vertex,
                fragment:shader.fragment
            });
            this.shader = shader;
            this.shaderSet = true;
        }else{
            console.error("Drawstate::setShader - no vertex or fragment shaders set");
        }
        return this;
    }



    useProgram(shader=null){
        if(shader !== null){
            this.setShader(shader);
            this.shader.bind();
        }else if(this.shader){
            this.shader.bind();
        }
        return this;
    }

    uniform(name,value){
        this.shader.uniform(name,value);
        return this;
    }

    floatUniform(name,value){
        this.shader.setFloat(name,value);
        return this;
    }


    setViewport(x,y,width,height){
        this.viewport = [x,y,width,height];
        return this;
    }

    /**
     * Sets an index buffer to use
     * @param buffer
     */
    setIndex(buffer){

        if(buffer instanceof WebGLBuffer){
            this.hasIndexBuffer = true;
            this.indexBuffer = buffer;

            this.vao.bind();
            buffer.bind();
            buffer.bufferData(buffer.data);

            this.vao.unbind();

            this.numElements = buffer.numelements;
        }else{

            let vbo = createVBO(this.gl,{
                data:buffer,
                indexed:true
            });

            this.vao.bind();
            vbo.bind();
            vbo.bufferData(buffer);

            this.vao.unbind();

            this.numElements = vbo.numelements;

        }
        return this;
    }

    updateAttribute(name,data){
        let attr = this.attributes[name];
        let buffer = attr.buffer;
        buffer.bind();

        buffer.unbind();

    }

    setInstancedAttribute(name,buffer,{
        data=null,
        size=3,
        type=this.gl.FLOAT,
        normalized=false,
        stride=0,
        offset=0
    }={}){
        if(!this.isInstanced){
            this.isInstanced = true;
        }

        let attrib = this.shader.attributes[name];

        // if we pass in an actual WebGLBuffer
        if(buffer instanceof WebGLBuffer){


            if(this.shader){
                this.attributes[name] = {
                    buffer:buffer,
                    size:size,
                    dataOptions:{
                        type:type,
                        normalized:normalized,
                        stride:stride,
                        offset:offset
                    }
                };
            }else {
                console.warn(strings.drawstate.setAttribute.missingShader);
            }



            if(buffer.data !== null){
                this.vao.bind();

                buffer.bind();
                buffer.bufferData(buffer.data);

                this.vao.makeInstacedAttribute(attrib.location);
                buffer.unbind();
                this.vao.unbind();
            }


        }else if(buffer instanceof Array || ArrayBuffer.isView(buffer)){


            // otherwise we need to make a buffer
            let vbo = createVBO(this.gl,{
                data:buffer
            });

            if(this.shader){

                this.attributes[name] = {
                    buffer:vbo,
                    size:size,
                    dataOptions:{
                        type:type,
                        normalized:normalized,
                        stride:stride,
                        offset:offset
                    }
                };
            }else {
                console.warn(strings.drawstate.setAttribute.missingShader);
            }


            this.vao.bind();

            vbo.bind();
            vbo.bufferData(buffer);
            this.vao.makeInstacedAttribute(attrib.location);
            vbo.unbind();
            this.vao.unbind();
        }
        return this;
    }
    /**
     * Adds an attribute to the state;
     * @param buffer {WebGLBuffer} an Augmented WebGLBuffer created by Jirachi.
     * @param size {Number} size of each component
     * @param type {Number} constant describing the type of data
     * @param normalized {Boolean} whether or not the data is normalized
     * @param stride {Number} the number of steps between sets
     * @param offset {Number} the index of where the data buffer describes is.
     */
    setAttribute(name,buffer,{
        data=null,
        size=3,
        type=this.gl.FLOAT,
        normalized=false,
        stride=0,
        offset=0
    }={}){


        // if we pass in an actual WebGLBuffer
        if(buffer instanceof WebGLBuffer){


            if(this.shader){
                this.attributes[name] = {
                    buffer:buffer,
                    size:size,
                    dataOptions:{
                        type:type,
                        normalized:normalized,
                        stride:stride,
                        offset:offset
                    }
                };
            }else {
                console.warn(strings.drawstate.setAttribute.missingShader);
            }



            if(buffer.data !== null){
                this.vao.bind();

                buffer.bind();
                buffer.bufferData(buffer.data);
                buffer.unbind();
                this.vao.unbind();
            }


        }else if(buffer instanceof Array || ArrayBuffer.isView(buffer)){

            // otherwise we need to make a buffer
            let vbo = createVBO(this.gl,{
                data:buffer
            });

            if(this.shader){
                this.attributes[name] = {
                    buffer:vbo,
                    size:size,
                    dataOptions:{
                        type:type,
                        normalized:normalized,
                        stride:stride,
                        offset:offset
                    }
                };
            }else {
                console.warn(strings.drawstate.setAttribute.missingShader);
            }


            this.vao.bind();

            vbo.bind();
            vbo.bufferData(buffer);
            vbo.unbind();
            this.vao.unbind();
        }
        return this;

    }


    setFramebufferClear(shouldClear=true){
        this.shouldClearFramebuffer = shoudlClear;
        return this;
    }

    drawElementsInstanced(numInstances=1,
        mode = this.gl.TRIANGLES,
        count=this.numElements,
        type=this.gl.UNSIGNED_SHORT,
        offset=0
    ){

        let gl = this.gl;
        let attributes = this.attributes;

        this.vao.bind();

        // enable attributes
        for(let i in attributes){
            let attr = attributes[i];
            attr.buffer.bind();
            this.vao.addAttribute(this.shader,i,attr.size);
            attr.buffer.unbind();
        }



        gl.clearScreen()

        gl.drawElementsInstanced(mode,count,type,offset,numInstances);

        this.vao.unbind();
    }

    drawElements(
        mode = this.gl.TRIANGLES,
        count=this.numElements,
        type=this.gl.UNSIGNED_SHORT,
        offset=0
    ){

        let gl = this.gl;
        let attributes = this.attributes;

        this.vao.bind();

        // enable attributes
        for(let i in attributes){
            let attr = attributes[i];
            attr.buffer.bind();
            this.vao.addAttribute(this.shader,i,attr.size)
            attr.buffer.unbind();
        }



        gl.clearScreen()

        gl.drawElements(mode,count,type,offset);

        this.vao.unbind();
    }

    /**
     * Renders the draw state with the set options. Acts the same as gl.drawArrays
     * @param mode {GLuint} the primitive mode to use.
     * @param first {Number} the index of where to start drawing from
     * @param count {Number} how many vertices to draw
     * @param resetWidth {Number} optional - the width for the viewport when un-binding the fbo
     * @param resetHeight {Number} the height for the viewport when unbinding the fbo.
     */
    drawArrays(mode=this.gl.TRIANGLES,first=0,count=1,resetWidth=window.innerWidth,resetHeight=window.innerHeight) {
        let gl = this.gl;
        let attributes = this.attributes;


        this.vao.bind();

        // enable attributes
        for(let i in attributes){
            let attr = attributes[i];
            attr.buffer.bind();
            this.vao.addAttribute(this.shader,i,attr.size)
            attr.buffer.unbind();
        }


        if(this.framebuffer){
            this.framebuffer.bind();
        }

        gl.clearColor(0.0,0.0,0.0,0.0);
        gl.viewport(this.viewport[0],this.viewport[1],this.viewport[2],this.viewport[3]);
        gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

        if(this.blending){
            gl.enable(gl.BLEND);
            this.blendFunc();
        }


        for(let i in this.textures){
            this.textures[i].bind(parseInt(i));
        }

        // ============ CODE STARTS HERE ====================//


        gl.drawArrays(mode,first,count);

        // ============ CODE ENDS HERE ================== //

        for(let i in this.textures){
            this.textures[i].unbind(parseInt(i));
        }
        // unbind framebuffer
        if(this.framebuffer){
            this.framebuffer.unbind();
        }

        if(this.blending){
            gl.disableBlend();
        }

        this.vao.unbind();
        // reset viewport
        gl.viewport(0,0,resetWidth,resetHeight);
    }

}

export default DrawState;

