
import {flattenArray} from "../math/core";

/**
 * Creates a WebGlBuffer object but augmented with some additional functions and
 * properties
 * @param gl {WebGLRenderingContext}
 * @param indexed {Boolean}
 * @param size {Number}
 * @param usage {Number}
 * @param data {ArrayBuffer}
 * @returns {AudioBuffer | WebGLBuffer | *}
 */
export default function(gl,{
    indexed=false,
    size=3,
    usage="STATIC_DRAW",
    data=null
}={}){
    let buffer = null;

    // set the buffer type
    let bufferType = "ARRAY_BUFFER";
    if(indexed === true){
        bufferType = "ELEMENT_ARRAY_BUFFER";


    }


    let name = bufferType;
    let bufferTypeName = bufferType;
    bufferType = gl[bufferType];

    // set the usage
    let usageName = usage;
    usage = gl[usage];
    buffer = gl.createBuffer();

    // save data
    buffer.data = data;

    // if we want to immediately buffer VBO with data, do so
    //TODO this might require some rethinking
    if(data !== null){
        if(data instanceof Array || ArrayBuffer.isView(data)){

            gl.bindBuffer(bufferType,buffer);
            gl.bufferData(bufferType,data,usage);
            gl.bindBuffer(bufferType,null);

            // if data isn't null, store the number of elements we need to draw which should be
            // the same as the data length. If it's not set, set the number of elements to 1
            // to hopefully get something to show up on screen.
            buffer.numelements = data.length !== null ? data.length : 1;
        }
    }

    for(let i in VBO.prototype){
        buffer[i] = VBO.prototype[i];
    }

    Object.assign(buffer,new VBO(gl,[bufferType,bufferTypeName],[usage,usageName],size));

    return buffer;
}

// ========== DEFINITION ============= //
function VBO(gl,type,usage,size=3){
    this.gl = gl;
    this.type = type[0];
    this.vboTypeName = type[1];
    this.usage = usage[0]
    this.usageName= usage[1];
    this.size = size;

    this.isBound = false;

}

VBO.prototype = {

    bind(){
        this.gl.bindBuffer(this.type,this);
        this.isBound = true;
    },

    unbind(){
        this.gl.bindBuffer(this.type,null);
        this.isBound = false;
    },

    /**
     * Update the buffer data
     * @param data {Array} An array or Typed array corresponding to the correct buffer type
     * @param offset {Number} an optional offset value
     */
    updateBuffer(data=false,offset=0){

        let type = this.type;
        let gl = this.gl;

        if(!data){
            return;
        }

        if(data instanceof Array){
            switch (type){
                case gl.ARRAY_BUFFER:
                    data = new Float32Array(flattenArray(data));
                    break;

                case gl.ELEMENT_ARRAY_BUFFER:
                    data = new Uint16Array(flattenArray(data));
                    break;
            }
        }

        gl.bufferSubData(type,offset,data);
    },


    /**
     * Buffers the data onto the VBO. Can be called automatically without needing to bind the buffer first
     * @param data {Array} a array or typed array.
     * @param type {Number} optional - the type of VBO you're making. Assumes a regular floating point based buffer.
     */
    bufferData(data,type=this.type){

        data = data !== undefined ? data : this.data;

        if(data === null || data === undefined){
            console.warn("buffer data called on VBO without any vertex data");
            return;
        }

        if((!data instanceof Array) && (!data instanceof ArrayBuffer)){
            return;
        }

        // if we pass in a normal array and there are nested arrays, make sure to flatten data.
        if(data[0] instanceof Array){
            switch (type){
                case this.gl.ARRAY_BUFFER:
                    data = new Float32Array(flattenArray(data));
                    break;

                case this.gl.ELEMENT_ARRAY_BUFFER:
                    data = new Uint16Array(flattenArray(data));
                    break;
            }
        }


        // if data is a regular array, turn it into a TypedArray
        if(data instanceof Array){

            switch (type){
                case this.gl.ARRAY_BUFFER:
                    data = new Float32Array(data);
                    break;

                case this.gl.ELEMENT_ARRAY_BUFFER:
                    data = new Uint16Array(data);
                    break;
            }
        }


        // buffer the data, if already bound don't bother binding.
        if(!this.isBound){
            this.bind();
            this.gl.bufferData(this.type,data,this.usage);
            this.unbind();
        }else{

            this.gl.bufferData(this.type,data,this.usage);
        }

        this.data = data;

    }
};