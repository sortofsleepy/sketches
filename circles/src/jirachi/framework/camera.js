import mat4 from '../math/mat4'
import vec3 from '../math/vec3'


class Camera {
    constructor({
        position=vec3.create(0,0,0),
        direction=  vec3.create(0,0,-1),
        up=vec3.create(0,1,0)
    }={}){

        this.projection = mat4.create();
        this.view = mat4.create();
        this.fov = 0;
        this.aspect = 0;
        this.near = 0;
        this.far = 0;

        // inverse of the view matrix
        this.inverseView = mat4.create();

        // camera position
        this.position = position;

        // camera direction
        this.direction = direction;

        // orientation
        this.orientation = mat4.create();

        // up
        this.up =up;

        // center
        this.center = vec3.create();

        // eye
        this.eye = vec3.create();

        return this;
    }

    lookAt(eye,aCenter=null){
        this.eye = vec3.clone(eye);
        this.center = aCenter !== null ? vec3.clone(aCenter) : this.center;

        vec3.copy(this.position,eye);
        mat4.identity(this.view);
        mat4.lookAt(this.view,eye,this.center,this.up)

        return this;
    }

    getProjectionMatrix(){
        return this.projection;
    }

    getViewMatrix(){
        return this.view;
    }

    getInvertedViewMatrix(){
        return mat4.invert(this.inverseView,this.view);
    }

    /**
     * Turns the camera into a perspective camra
     * @param fov {Number} Field of view
     * @param aspect {Number} aspect ratio. If the keyword "fullscreen" is used - aspect is calculated to window.innerWidth / window.innerHeight
     * @param near {Number} camera near value
     * @param far {Number} camera far value.
     */
    setupPerspective(fov,aspect,near,far){

        aspect = aspect === "fullscreen" ? window.innerWidth / window.innerHeight : aspect;

        // generate projection matrix.
        this.projection = mat4.perspective(this.projection,fov,aspect,near,far);

        this.fov = fov;
        this.aspect = aspect;
        this.near = near;
        this.far = far;

        // initial translation, just so we can ensure something shows up and no one thinks something's weird.
        mat4.translate(this.view,this.view,[0,0,-10])

        return this;
    }

    /**
     * Basically the same as setupPerspective but all params are optional and by default,
     * draw from previously saved params.
     * @param fov
     * @param aspect
     * @param near
     * @param far
     * @returns {Camera}
     */
    updatePerspective({
        fov=this.fov,
        aspect=window.innerWidth / window.innerHeight,
        near=this.near,
        far=this.far
     }={}){

        // generate projection matrix.
        this.projection = mat4.perspective(this.projection,fov,aspect,near,far);

        this.fov = fov;
        this.aspect = aspect;
        this.near = near;
        this.far = far;


        return this;
    }
}

export default Camera;

// ==================== SUPPORT FUNCTIONS ======================= //

/**
 * Updates the aspect ratio of a camera
 * @param camera {Camera} a camera object
 * @param aspect {Number} the new aspect ratio.
 * @returns {Camera}
 */
export function updateAspectRatio(camera,aspect){

    if(camera instanceof Camera){
        camera.aspect = aspect;
        mat4.perspective(camera.projection,camera.fov,camera.aspect,camera.near,camera.far)
        return camera;
    }
}

export function setZoom(camera,zoom){
    camera.zoom = zoom;
    camera.position = [0,0,zoom];
    camera.lookAt([0,0,0])
    mat4.translate(camera.view,camera.view,[0,0,zoom]);
    return camera;
}

