/**
 Testing app for things

 */
import Quad from './framework/quad'
import createTexture from './core/texture'
import createRenderer from './core/gl'


let img = new Image();
let quad = null;
let gl = null;
let tex = null;

img.src = "/textures/apple-music.png"


img.onload = () => {

    gl = createRenderer()
        .attachToScreen()
        .setFullscreen();

    quad = new Quad(gl,{
        hasTexture:true
    });

    tex = createTexture(gl,{
        data:img
    })

    animate();

}


function animate(){
    requestAnimationFrame(animate);

    gl.clearScreen();

    quad.draw(tex);
}