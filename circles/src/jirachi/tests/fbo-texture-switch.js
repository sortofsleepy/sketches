/**
 Testing app for things

 */
import Quad from './framework/quad'
import createTexture from './core/texture'
import createRenderer from './core/gl'
import createFramebuffer from './core/fbo'

let img = new Image();
let img2 = new Image();
let quad = null;
let renderQuad = null;
let gl = null;
let fbo = null;
let tex = null;
let tex2 = null;

img.src = "/textures/apple-music.png"
img2.src = "/textures/test.png";

let imgcount = 0;

img.onload = () => {
    imgcount += 1;
}

img2.onload = () => {
    imgcount += 1;
}

let timer = setInterval(() => {
    if(imgcount === 2){
        clearInterval(timer);
        build();
    }
})


function build(){

    gl = createRenderer()
        .attachToScreen()
        .setFullscreen();

    quad = new Quad(gl,{
        hasTexture:true
    });

    renderQuad = new Quad(gl,{
        hasTexture:true
    });

    tex = createTexture(gl,{
        data:img
    });

    tex2 = createTexture(gl,{
        data:img2
    });


    fbo = createFramebuffer(gl,{
        width:window.innerWidth,
        height:window.innerHeight,
        hasDepthTexture:true
    });

    window.addEventListener('resize',() => {
        fbo.resize(window.innerWidth,window.innerHeight);
    })

    fbo.bind();
    fbo.setViewport();
    quad.draw(tex);
    fbo.unbind();

    window.addEventListener('keydown',() => {
        fbo.replaceTexture(tex2);
    })

    animate();
}


function animate(){
    requestAnimationFrame(animate);

    gl.clearScreen();



    renderQuad.draw(fbo.texture);
}