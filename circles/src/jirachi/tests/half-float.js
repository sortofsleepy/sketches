/**
 Testing app for things

 */
import Quad from './framework/quad'
import createTexture from './core/texture'
import createRenderer from './core/gl'
import createFramebuffer from './core/fbo'
import {toHalfFloat} from "./math/utils";

let img = new Image();
let quad = null;
let renderQuad = null;
let gl = null;
let fbo = null;
let tex = null;

img.src = "/textures/apple-music.png"


img.onload = () => {

    gl = createRenderer()
        .attachToScreen()
        .setFullscreen();

    quad = new Quad(gl,{
        hasTexture:true
    });



    let data = new Uint16Array(512 * 512 * 4);
    let len = data.length;
    for(let i = 0; i < len; i += 4){
        data[i] = toHalfFloat(0);
        data[i + 2] = toHalfFloat(0);
    }

    console.log(data);




    tex = createTexture(gl,{
        data:data,
        format:{
            format:gl.RGBA,
            internalFormat:gl.RGBA16F,
            minFilter:gl.NEAREST,
            magFilter:gl.NEAREST,
            texelType:gl.HALF_FLOAT
        }
    });





    animate();
}


function animate(){
    requestAnimationFrame(animate);

    gl.clearScreen();

    quad.draw(tex);
}