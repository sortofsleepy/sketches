/**
 Testing app for things

 */
import Quad from './framework/quad'
import createTexture from './core/texture'
import createRenderer from './core/gl'
import createFramebuffer from './core/fbo'

let img = new Image();
let quad = null;
let renderQuad = null;
let gl = null;
let fbo = null;
let tex = null;

img.src = "/textures/apple-music.png"


img.onload = () => {

    gl = createRenderer()
        .attachToScreen()
        .setFullscreen();

    quad = new Quad(gl,{
        hasTexture:true
    });

    renderQuad = new Quad(gl,{
        hasTexture:true
    });

    tex = createTexture(gl,{
        data:img
    });

    fbo = createFramebuffer(gl,{
        width:window.innerWidth,
        height:window.innerHeight,
        hasDepthTexture:true
    });

    window.addEventListener('resize',() => {
        fbo.resize(window.innerWidth,window.innerHeight);
    })


    animate();
}


function animate(){
    requestAnimationFrame(animate);

    gl.clearScreen();

    fbo.bind();
    fbo.setViewport();
    quad.draw(tex);
    fbo.unbind();

    renderQuad.draw(fbo.texture);
}