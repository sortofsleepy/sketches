
uniform mat4 projectionMatrix;
uniform mat4 modelMatrix;
uniform mat4 viewMatrix;
uniform vec2 resolution;
uniform float lineWidth;
uniform vec3 color;
uniform float opacity;
uniform float near;
uniform float far;
uniform float sizeAttenuation;


in vec3 position;
in vec3 previous;
in vec3 next;
in float side;
in float width;
in vec2 uv;
in float counters;

out vec4 vColor;
out vec2 vUv;
out float vCounters;

vec2 fix( vec4 i, float aspect ) {
    vec2 res = i.xy / i.w;
    res.x *= aspect;
	vCounters = counters;
    return res;
}

void main(){
    vec3 pos = position;


    float aspect = resolution.x / resolution.y;
    float pixelWidthRatio = 1. / (resolution.x * projectionMatrix[0][0]);


    vColor = vec4( color, opacity );
    vUv = uv;

    mat4 m = projectionMatrix * modelMatrix * viewMatrix;
    vec4 finalPosition = m * vec4( position, 1.0 );
    vec4 prevPos = m * vec4( previous, 1.0 );
    vec4 nextPos = m * vec4( next, 1.0 );

    vec2 currentP = fix( finalPosition, aspect );
    vec2 prevP = fix( prevPos, aspect );
    vec2 nextP = fix( nextPos, aspect );

    float pixelWidth = finalPosition.w * pixelWidthRatio;
    float w = 1.8 * pixelWidth * lineWidth * width;

    if( sizeAttenuation == 1. ) {
        w = 1.8 * lineWidth * width;
    }

    vec2 dir;
    if(nextP == currentP) dir = normalize(currentP - prevP);
    else if(prevP == currentP) dir = normalize(nextP - currentP);
    else {
         vec2 dir1 = normalize( currentP - prevP );
          vec2 dir2 = normalize( nextP - currentP );
          dir = normalize( dir1 + dir2 );

          vec2 perp = vec2( -dir1.y, dir1.x );
          vec2 miter = vec2( -dir.y, dir.x );
          //w = clamp( w / dot( miter, perp ), 0., 4. * lineWidth * width );',
    }

    vec2 normal = vec2( -dir.y, dir.x );
    normal.x /= aspect;
    normal *= .5 * w;

    vec4 offset = vec4( normal * side, 0.0, 1.0 );
    finalPosition.xy += offset.xy;


    gl_Position = finalPosition;
    //gl_Position = projectionMatrix * viewMatrix * vec4(pos,1.);


    vUv = uv;

    vec3 prev = previous;
    vec3 nxt = next;
    float s = side;
    float wi = width;
    float c = counters;

}