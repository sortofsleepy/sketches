uniform mat4 projectionMatrix;
uniform mat4 viewMatrix;

in vec3 position;

void main(){
    gl_Position = projectionMatrix * viewMatrix * vec4(position,1.);
}