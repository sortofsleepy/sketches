
uniform mat4 projectionMatrix;
uniform mat4 viewMatrix;
uniform mat4 modelMatrix;
uniform mat3 normalMatrix;

uniform float scale;

in vec3 position;
in vec3 normal;
in vec2 uv;


out vec3 vNormal;
out vec2 vUv;

void main(){
    vec4 pos = vec4(position,1.);

    pos.x *= scale;
    pos.y *= scale;
    pos.z *= scale;

    vNormal = normal;
    vUv = uv;

    gl_Position = projectionMatrix * viewMatrix * modelMatrix * pos;
}
