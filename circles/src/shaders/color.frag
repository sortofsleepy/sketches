precision highp float;

const vec3 color1 = normalize( vec3(128.0,127.0,238.0));
const vec3 color2 = normalize( vec3(105.0,180.0,229.0));

uniform float time;
uniform vec2 resolution;
uniform sampler2D map;
uniform float threshold;


//const vec2 center = vec2(0.5,0.5);
const float speed = 0.5;

out vec4 glFragColor;
in vec2 vUv;
vec3 solveColor(vec2 _uv){
  // Normalized pixel coordinates (from 0 to 1)
    vec2 uv = gl_FragCoord.xy/resolution.xy;
    uv += _uv;



    // Time varying pixel color
    vec3 col = 0.5 + 0.5 * cos(time + uv.xyx + vec3(1,2,4));

    col = mix(color1,col,1.);
    col = mix(color2,col,0.6);

    return col;
}


void main(){

    vec4 dat = texture(map,vUv);
 
    float invAr = resolution.y / resolution.x;
    vec2 uv = gl_FragCoord.xy / resolution.xy;
    vec2 center = vec2(0.5,0.5);
    vec3 texcol;

    float x = (center.x-uv.x);
    float y = (center.y-uv.y) * invAr;

    float r = -sqrt(x*x + y*y); //uncoment this line to symmetric ripples
    //float r = -(x*x + y*y);
  	
     float z = 1.0 + 0.5*sin((r + time * speed) / 0.1);
     float distanceFromCenter = sqrt(x * x + y * y);
  	 float sinArg = distanceFromCenter * 2.0 - time;
  	 float slope = cos(sinArg) ;
    texcol.x = sin(z);
    texcol.y = cos(z);
    texcol.z = z;

    dat.x *= cos(z) + 1.0;
    dat.y *= sin(z) + 1.0;
    dat.z *= texcol.x;
  
    vec4 tex =vec4(dat.xyz * texcol, uv + normalize(vec2(x, y) + texcol.xy) * slope);
	
    glFragColor = tex ;
    if(glFragColor.g > 0.8) glFragColor.g *= 0.5;

}
