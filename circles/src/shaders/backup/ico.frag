
precision highp float;
uniform float scale;

uniform vec3 light_position;
uniform vec3 eye_position;
uniform vec3 DiffuseLight;
uniform vec3 RimColor;
uniform vec3 fogColor;
uniform float FogDensity;

out vec4 glFragColor;

in mat3 vNormalMatrix;
in vec3 vE;
in vec3 vPosition;

in vec3 vPos;
in vec3 vNormal;





// ======= fog ========== //

in vec3 world_pos;
in vec3 world_normal;
in vec2 vUv;
in vec4 viewSpace;
in float vScale;


//const vec3 DiffuseLight = vec3(1.15, 0.15, 1.0);
//const vec3 RimColor = vec3(1.2, 0.0, 0.0);
//const vec3 fogColor = vec3(1.5,0.0,1.0);
//const float FogDensity = 0.039;

void main(){



    vec3 x = dFdx(vPosition);
    vec3 y = dFdy(vPosition);
    vec3 normal = normalize(cross(x,y));

    vec3 n = normalize(vNormalMatrix * normal);
    vec3 r = reflect(vE,n);
    float m = 2. * sqrt(
        pow(r.x,4.) +
        pow(r.y,4.) +
        pow(r.z + 1.,2.)
    );

    vec2 vN = r.xy / m + .5;

    // =========== CALCULATE FOG ============= //

    // get light and view directions
    vec3 L = normalize(light_position - world_pos);
    vec3 V = normalize(eye_position - world_pos);

    // diffuse lighting
    vec3 diffuse = DiffuseLight * max(0.0,dot(L,normal));


    // rim lighting
    float rim = 1.0 - max(dot(V,normal),0.0);
    rim = smoothstep(0.6,1.0,rim);

    vec3 finalRim = RimColor * vec3(rim);
    vec3 lightColor = finalRim + diffuse;
    vec3 finalColor = vec3(0.);

    // distance
    float dist = 0.0;
    float fogFactor = 0.0;

    // plane based distance
    dist = abs(viewSpace.z);


    // linear fog
    fogFactor = (80.0 - dist) / (80.0 - 20.0);
    fogFactor = clamp(fogFactor,0.0,1.0);

    finalColor = mix(fogColor,lightColor,fogFactor);

    // exponential fog
    fogFactor = 1.0 / exp(dist * FogDensity);
    fogFactor = clamp(fogFactor,0.0,1.0);

    finalColor = mix(fogColor,light_position,fogFactor);


    glFragColor = vec4(finalColor.x,vScale,0.0,1.0);
    //glFragColor = vec4(1.);
}
