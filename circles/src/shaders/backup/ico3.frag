precision highp float;
uniform sampler2D map;
uniform vec3 light_position;
uniform vec3 eye_position;
uniform vec3 DiffuseLight;
uniform vec3 RimColor;
uniform vec3 fogColor;
uniform float FogDensity;
uniform float time;
uniform vec2 resolution;

out vec4 glFragColor;

in mat3 vNormalMatrix;
in vec3 vE;
in vec3 vPosition;

in vec3 vPos;
in vec3 vNormal;




// ======= fog ========== //

in vec3 world_pos;
in vec3 world_normal;
in vec2 vUv;
in vec4 viewSpace;
in float vScale;


//const vec3 DiffuseLight = vec3(1.15, 0.15, 1.0);
//const vec3 RimColor = vec3(1.2, 0.0, 0.0);
//const vec3 fogColor = vec3(1.5,0.0,1.0);
//const float FogDensity = 0.039;

		// HSL to RGB Convertion helpers
		vec3 HUEtoRGB(float H){
			H = mod(H,1.0);
			float R = abs(H * 6.0 - 3.0) - 1.0;
			float G = 2.0 - abs(H * 6.0 - 2.0);
			float B = 2.0 - abs(H * 6.0 - 4.0);
			return clamp(vec3(R,G,B),0.0,1.0);
		}
		vec3 HSLtoRGB(vec3 HSL){
			vec3 RGB = HUEtoRGB(HSL.x);
			float C = (1.0 - abs(2.0 * HSL.z - 1.0)) * HSL.y;
			return (RGB - 0.5) * C + HSL.z;
		}
		vec3 pal( in float t, in vec3 a, in vec3 b, in vec3 c, in vec3 d )
        {
            return a + b*cos( 6.28318*(c*t+d) );
        }
void main(){
    float scale = vScale;
    scale = scale * 2.0 + 10.0;




    vec4 diffuseColor = texture( map, vUv);

    vec4 color = vec4(scale / 5.0, cos(scale) / 0.5, sin(scale) / 0.4,sin(time) * scale);
    color = vec4( diffuseColor.xyz * color.rgb, diffuseColor.w );

    glFragColor = color;

    if(diffuseColor.w < 0.5) discard;
}
