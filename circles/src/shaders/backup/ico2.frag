precision highp float;

uniform vec3 light_position;
uniform vec3 eye_position;
uniform vec3 DiffuseLight;
uniform vec3 RimColor;
uniform vec3 fogColor;
uniform float FogDensity;

out vec4 glFragColor;

in mat3 vNormalMatrix;
in vec3 vE;
in vec3 vPosition;

in vec3 vPos;
in vec3 vNormal;




// ======= fog ========== //

in vec3 world_pos;
in vec3 world_normal;
in vec2 vUv;
in vec4 viewSpace;
in float vScale;


//const vec3 DiffuseLight = vec3(1.15, 0.15, 1.0);
//const vec3 RimColor = vec3(1.2, 0.0, 0.0);
//const vec3 fogColor = vec3(1.5,0.0,1.0);
//const float FogDensity = 0.039;

		// HSL to RGB Convertion helpers
		vec3 HUEtoRGB(float H){
			H = mod(H,1.0);
			float R = abs(H * 6.0 - 3.0) - 1.0;
			float G = 2.0 - abs(H * 6.0 - 2.0);
			float B = 2.0 - abs(H * 6.0 - 4.0);
			return clamp(vec3(R,G,B),0.0,1.0);
		}
		vec3 HSLtoRGB(vec3 HSL){
			vec3 RGB = HUEtoRGB(HSL.x);
			float C = (1.0 - abs(2.0 * HSL.z - 1.0)) * HSL.y;
			return (RGB - 0.5) * C + HSL.z;
		}
void main(){



    vec3 x = dFdx(vPosition);
    vec3 y = dFdy(vPosition);
    vec3 normal = normalize(cross(x,y));

    vec3 n = normalize(vNormalMatrix * vNormal);
    vec3 r = reflect(vE,n);
    float m = 2. * sqrt(
        pow(r.x,4.) +
        pow(r.y,4.) +
        pow(r.z + 1.,2.)
    );

    vec2 vN = r.xy / m + .5;
    float scale = vScale;
    scale *= 20.0 + 20.0;

    // =========== CALCULATE FOG ============= //

    // get light and view directions
    vec3 L = normalize(light_position - world_pos);
    vec3 V = normalize(eye_position - world_pos);

    // diffuse lighting
    vec3 diffuse = DiffuseLight * max(0.0,dot(L,vNormal));


    // rim lighting
    float rim = 1.0 - max(dot(V,vNormal),0.0);
    rim = smoothstep(0.6,1.0,rim);

    vec3 finalRim = RimColor * vec3(vScale / 4.0,vScale / 9.0,10.2);
    vec3 lightColor = finalRim * diffuse;
    vec3 finalColor = vec3(0.);

    // distance
    float dist = 0.0;
    float fogFactor = 0.0;

    // plane based distance
    dist = abs(viewSpace.z);


    // linear fog
    fogFactor = (80.0 - dist) / (80.0 - 20.0);
    fogFactor = clamp(fogFactor,0.0,1.0);

    finalColor = mix(fogColor,lightColor,fogFactor);

    // exponential fog
    fogFactor = 1.0 / exp(dist * FogDensity * vScale) ;
    fogFactor = clamp(fogFactor,0.0,1.0);

    finalColor = mix(fogColor,light_position,fogFactor);

    finalColor = HSLtoRGB(finalColor);
    finalColor = HSLtoRGB(finalColor * (vScale / 2.0));

    vec4 fin = vec4(finalColor,vScale);

    fin = vec4(finalColor ,vScale);

    if(fin.w < 0.5) discard;



    glFragColor = fin;

}
