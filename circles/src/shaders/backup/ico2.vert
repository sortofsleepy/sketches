
uniform mat4 projectionMatrix;
uniform mat4 viewMatrix;
uniform mat4 modelMatrix;
uniform mat3 normalMatrix;

uniform float time;
uniform bool bgElement;

in vec3 position;
in vec3 normal;
in vec2 uv;

in vec3 iPos;

out float vId;
out mat3 vNormalMatrix;
out vec3 vE;
out vec3 vPosition;

out vec3 vNormal;
out float vScale;
//http://in2gpu.com/2014/07/22/create-fog-shader/
out vec3 world_pos;
out vec3 world_normal;
out vec2 vUv;
out vec4 viewSpace;
out float vTime;
out vec3 vPos;


vec3 rotateX(vec3 p, float theta) {
  float s = sin(theta);
  float c = cos(theta);
  return vec3(p.x, p.y * c - p.z * s, p.z * c + p.y * s);
}


vec3 rotateY(vec3 p, float theta) {
  float s = sin(theta);
  float c = cos(theta);
  return vec3(p.x * c + p.z * s, p.y, p.z * c - p.x * s);
}

vec3 rotateZ(vec3 p, float theta) {
  float s = sin(theta);
  float c = cos(theta);
  return vec3(p.x * c - p.y * s, p.y * c + p.x * s, p.z);
}

void main(){
    vec4 pos = vec4(position,1.);
    vec4 instancePos = viewMatrix * modelMatrix * vec4(iPos,1.);

    vec3 trTime = vec3(iPos.x + time,iPos.y + time,iPos.z + time);
    float scale = sin( trTime.x * 2.1 ) + sin( trTime.y * 3.2 ) + sin( trTime.z * 4.3 );


    vScale = scale;


    if(bgElement){
        //pos.z = 0.952899716646719925;
        pos.z = pos.z + 0.9528997166467199256;
    }


    vec4 mvMatrix = modelMatrix * viewMatrix * vec4(1.);
    vec3 e = normalize(vec3(mvMatrix * pos));


    vE = e;
    vPosition = pos.xyz;
    vNormalMatrix = normalMatrix;

    world_pos = (modelMatrix * pos).xyz;
    world_normal = normalize(mat3(modelMatrix) * normal);
    vUv = uv;
    vNormal = normal;
    viewSpace = viewMatrix * modelMatrix * pos;
    vTime = time;

    vPos = iPos;
    instancePos.xyz += position * scale;
    vUv = uv;


    gl_Position = projectionMatrix * instancePos;
}
