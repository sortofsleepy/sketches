precision highp float;
in vec2 vUv;
uniform sampler2D tex;
out vec4 glFragColor;
void main(){

    vec4 dat = texture(tex,vUv);
    glFragColor = dat;
}
