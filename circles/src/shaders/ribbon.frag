precision highp float;

uniform sampler2D map;
uniform sampler2D alphaMap;
uniform bool useMap;
uniform float useAlphaMap;
uniform vec2 dashArray;
uniform float visibility;
uniform float alphaTest;
uniform vec2 repeat;
uniform float time;

vec3 v3( float x, float y, float z ){
	return vec3( x, y, z );
}

vec3 h2rgb( float hue ){

  float h = abs(hue - floor(hue)) * 6.;
  vec3 c = vec3( 0., 0., 0. );

  int f = int(floor( h ));

  if(f==0)c=v3(1.,h,0.);else if(f==1)c=v3(2.-h,1.,0.);else if(f==2)c=v3(0.,1.,h-2.);else if(f==3)c=v3(0.,4.-h,1.);else if(f==4)c=v3(h-4.,0.,1.);else c=v3(1.,0.,6.-h);
  return c;
}

out vec4 glFragColor;
in vec2 vUv;
in vec4 vColor;
in float vCounters;
void main(){
    vec4 c = vColor;

    // throws errors even when wrapped in a if statement for some reason,
    // so uncomment as necessary.
    //c *= texture( map, vUv * repeat );
    //if( useAlphaMap == 1. ) c.a *= texture( alphaMap, vUv * repeat ).a;
    //if( c.a < alphaTest ) discard;
    vec3 col = h2rgb(time / 2.0);
    glFragColor = vec4(col,1.);
    //glFragColor.a *= step(vCounters,visibility);
}
