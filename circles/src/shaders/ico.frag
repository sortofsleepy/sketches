precision highp float;
uniform sampler2D map;
uniform vec3 light_position;
uniform vec3 eye_position;
uniform vec3 DiffuseLight;
uniform vec3 RimColor;
uniform vec3 fogColor;
uniform float FogDensity;
uniform float time;
uniform vec2 resolution;

out vec4 glFragColor;

in mat3 vNormalMatrix;
in vec3 vE;
in vec3 vPosition;

in vec3 vPos;
in vec3 vNormal;




// ======= fog ========== //

in vec3 world_pos;
in vec3 world_normal;
in vec2 vUv;
in vec4 viewSpace;
in float vScale;


//const vec3 DiffuseLight = vec3(1.15, 0.15, 1.0);
//const vec3 RimColor = vec3(1.2, 0.0, 0.0);
//const vec3 fogColor = vec3(1.5,0.0,1.0);
//const float FogDensity = 0.039;

		// HSL to RGB Convertion helpers
		vec3 HUEtoRGB(float H){
			H = mod(H,1.0);
			float R = abs(H * 6.0 - 3.0) - 1.0;
			float G = 2.0 - abs(H * 6.0 - 2.0);
			float B = 2.0 - abs(H * 6.0 - 4.0);
			return clamp(vec3(R,G,B),0.0,1.0);
		}
		vec3 HSLtoRGB(vec3 HSL){
			vec3 RGB = HUEtoRGB(HSL.x);
			float C = (1.0 - abs(2.0 * HSL.z - 1.0)) * HSL.y;
			return (RGB - 0.5) * C + HSL.z;
		}
		vec3 pal( in float t, in vec3 a, in vec3 b, in vec3 c, in vec3 d )
        {
            return a + b*cos( 6.28318*(c*t+d) );
        }


        vec3 mod289(vec3 x) {
          return x - floor(x * (1.0 / 289.0)) * 289.0;
        }

        vec2 mod289(vec2 x) {
          return x - floor(x * (1.0 / 289.0)) * 289.0;
        }

        vec3 permute(vec3 x) {
          return mod289(((x*34.0)+1.0)*x);
        }
        float snoise(vec2 v)
          {
          const vec4 C = vec4(0.211324865405187,  // (3.0-sqrt(3.0))/6.0
                              0.366025403784439,  // 0.5*(sqrt(3.0)-1.0)
                             -0.577350269189626,  // -1.0 + 2.0 * C.x
                              0.024390243902439); // 1.0 / 41.0
        // First corner
          vec2 i  = floor(v + dot(v, C.yy) );
          vec2 x0 = v -   i + dot(i, C.xx);

        // Other corners
          vec2 i1;
          //i1.x = step( x0.y, x0.x ); // x0.x > x0.y ? 1.0 : 0.0
          //i1.y = 1.0 - i1.x;
          i1 = (x0.x > x0.y) ? vec2(1.0, 0.0) : vec2(0.0, 1.0);
          // x0 = x0 - 0.0 + 0.0 * C.xx ;
          // x1 = x0 - i1 + 1.0 * C.xx ;
          // x2 = x0 - 1.0 + 2.0 * C.xx ;
          vec4 x12 = x0.xyxy + C.xxzz;
          x12.xy -= i1;

        // Permutations
          i = mod289(i); // Avoid truncation effects in permutation
          vec3 p = permute( permute( i.y + vec3(0.0, i1.y, 1.0 ))
        		+ i.x + vec3(0.0, i1.x, 1.0 ));

          vec3 m = max(0.5 - vec3(dot(x0,x0), dot(x12.xy,x12.xy), dot(x12.zw,x12.zw)), 0.0);
          m = m*m ;
          m = m*m ;

        // Gradients: 41 points uniformly over a line, mapped onto a diamond.
        // The ring size 17*17 = 289 is close to a multiple of 41 (41*7 = 287)

          vec3 x = 2.0 * fract(p * C.www) - 1.0;
          vec3 h = abs(x) - 0.5;
          vec3 ox = floor(x + 0.5);
          vec3 a0 = x - ox;

        // Normalise gradients implicitly by scaling m
        // Approximation of: m *= inversesqrt( a0*a0 + h*h );
          m *= 1.79284291400159 - 0.85373472095314 * ( a0*a0 + h*h );

        // Compute final noise value at P
          vec3 g;
          g.x  = a0.x  * x0.x  + h.x  * x0.y;
          g.yz = a0.yz * x12.xz + h.yz * x12.yw;
          return 130.0 * dot(m, g);
        }
void main(){
    float scale = vScale;
    scale = scale * 2.0 + 10.0;

     vec3 x = dFdx(vPosition);
        vec3 y = dFdy(vPosition);
        vec3 normal = normalize(cross(x,y));

        vec3 n = normalize(vNormalMatrix * vNormal);
        vec3 r = reflect(vE,n);
        float m = 2. * sqrt(
            pow(r.x,4.) +
            pow(r.y,4.) +
            pow(r.z + 1.,2.)
        );

        vec2 vN = r.xy / m + .5;
  // get light and view directions
    vec3 L = normalize(light_position - world_pos);
    vec3 V = normalize(eye_position - world_pos);

    // diffuse lighting
    vec3 diffuse = DiffuseLight * max(0.0,dot(L,vNormal));


    // rim lighting
    float rim = 1.0 - max(dot(V,vNormal),0.0);
    rim = smoothstep(0.6,1.0,rim);

    vec3 finalRim = RimColor * vec3(vScale / 4.0,vScale / 9.0,10.2);
    vec3 lightColor = finalRim * diffuse;
    vec3 finalColor = vec3(0.);

    // distance
    float dist = 0.0;
    float fogFactor = 0.0;

    // plane based distance
    dist = abs(viewSpace.z);


    // linear fog
    fogFactor = (80.0 - dist) / (80.0 - 20.0);
    fogFactor = clamp(fogFactor,0.0,1.0);

    finalColor = mix(fogColor,lightColor,fogFactor);

    // exponential fog
    fogFactor = 1.0 / exp(dist * FogDensity * vScale) ;
    fogFactor = clamp(fogFactor,0.0,1.0);

    finalColor = mix(fogColor,light_position,fogFactor);

    vec4 diffuseColor = texture( map, vUv);

    vec4 color = vec4( diffuseColor.xyz * HSLtoRGB(vec3(scale/10.0, scale/10.0, 0.5)), diffuseColor.w );
    //vec4 color = vec4(scale / 20.0, scale / 10.0, scale,1.0);
    color = vec4( mix(finalColor,color.rgb,0.1) * sin(scale), scale );


    glFragColor = color;

}
