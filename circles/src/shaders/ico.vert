
uniform mat4 projectionMatrix;
uniform mat4 viewMatrix;
uniform mat4 modelMatrix;
uniform mat3 normalMatrix;

uniform float time;
uniform bool bgElement;

in vec3 position;
in vec3 normal;
in vec2 uv;

in vec3 iPos;

out float vId;
out mat3 vNormalMatrix;
out vec3 vE;
out vec3 vPosition;

out vec3 vNormal;
out float vScale;
//http://in2gpu.com/2014/07/22/create-fog-shader/
out vec3 world_pos;
out vec3 world_normal;
out vec2 vUv;
out vec4 viewSpace;
out float vTime;
out vec3 vPos;


void main(){
    vec4 mvPosition = viewMatrix * modelMatrix * vec4( iPos, 1.0 );
	vec3 trTime = vec3(iPos.x + time,iPos.y + time,iPos.z + time);
	float scale =  sin( trTime.x * 2.1 ) + sin( trTime.y * 3.2 ) + sin( trTime.z * 4.3 );
	vScale = scale;
	scale = scale * 4.0 + 4.0;
	mvPosition.xyz += position * scale;

    vec4 pos = vec4(position,1.);
    pos.z += 10.0;
	vec4 mvMatrix = modelMatrix * viewMatrix * vec4(1.);
    vec3 e = normalize(vec3(mvMatrix * pos));

    vE = e;
    vPosition = pos.xyz;
    vNormalMatrix = normalMatrix;

    world_pos = (modelMatrix * pos).xyz;
    world_normal = normalize(mat3(modelMatrix) * normal);
    vUv = uv;
    vNormal = normal;
    viewSpace = viewMatrix * modelMatrix * pos;
    vTime = time;

	vUv = uv;
	vNormal = normal;
	gl_Position = projectionMatrix * mvPosition;
}
