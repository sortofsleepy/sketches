import Quad from '../jirachi/framework/quad'
import frag from '../shaders/color.frag'

class Background {
    constructor(gl){
        this.gl = gl;

        this.state = new Quad(gl,{
            fragment:frag
        })
    }

    draw(dt,texture=null){
        if(texture !== null){
            texture.bind();

            this.state.draw();

            this.state.shader.uniform("time",dt);
            this.state.shader.uniform("resolution",[window.innerWidth,window.innerHeight]);
            texture.unbind();
        }else{
            this.state.draw();

            this.state.shader.uniform("time",dt);
            this.state.shader.uniform("resolution",[window.innerWidth,window.innerHeight]);
        }
    }
}

export default Background;