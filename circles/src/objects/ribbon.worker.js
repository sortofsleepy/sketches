/**
 * WebWorker for processing updates to a line.
 * @type {{compareV3, (*, *, *): *, copyV3, (*, *): *}}
 */

const utils = require('./utils')
let positions = [];
let next = [];
let side = [];
let width = [];
let indices = [];
let uvs = [];
let counters = [];


this.onmessage = function(e) {

    positions = e.data.positions;
    next = e.data.next;
    side =  e.data.side;
    width = e.data.width;
    indices = e.data.indices;
    uvs = e.data.uvs;
    counters = e.data.counters;


};



function memcpy (src, srcOffset, dst, dstOffset, length) {
    var i

    src = src.subarray || src.slice ? src : src.buffer
    dst = dst.subarray || dst.slice ? dst : dst.buffer

    src = srcOffset ? src.subarray ?
        src.subarray(srcOffset, length && srcOffset + length) :
        src.slice(srcOffset, length && srcOffset + length) : src

    if (dst.set) {
        dst.set(src, dstOffset)
    } else {
        for (i=0; i<src.length; i++) {
            dst[i + dstOffset] = src[i]
        }
    }

    return dst
}
