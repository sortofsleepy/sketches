import Ribbon from './ribbon'
import {randFloat} from "../jirachi/math/core";
import RWorker from './ribbon.worker'

class RibbonSystem {
    constructor(gl){
        this.gl = gl;
    }

    setup(num=20){
        let lines = [];
        let size = 100;


        for(var i = 0; i < num;++i){
            let line = new Ribbon(this.gl);
            let lineData = new Float32Array( size );
            for( var j = 0; j < size; j += 3 ) {
                lineData[ j ] = -30 + .1 * j;
                lineData[ j + 1 ] = 5 * Math.sin( .01 *  j );
                lineData[ j + 2 ] = -20;
            }
            line.buildMesh(lineData);
            lines.push({
                line:line,
                phi:(Math.random() * 2)* Math.PI,
                theta:(Math.random() * 2) * Math.PI,
                thetaSpeed:randFloat(-0.001,0.001),
                phiSpeed:randFloat(-0.001,0.001),
                radius:200
            });
        }

        this.lines = lines;
        this.num = num;

        this.worker = new RWorker();
    }

    draw(){
        let num = this.num;
        let gl = this.gl;
        for(let i = 0; i < num;++i){
            let line = this.lines[i];
            let x = cos(line.theta) * sin(line.phi) * line.radius
            let y = sin(line.theta) * sin(line.phi) * line.radius
            let z = cos(line.phi) * line.radius



            //line.line.advance([x,y,z]);
            //line.line.draw(camera);
            //line.theta += line.thetaSpeed * 0.05;
            //line.phi += line.phiSpeed * 0.05;
        }


    }
}

export default RibbonSystem;