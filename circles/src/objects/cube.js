import Icohedron from './icohedron'
import createTexture from '../jirachi/core/texture'
class Cube {
    constructor(gl,{
        height=150,
        width=150,
        depth=150,
        count=3000
    }={}){
        this.gl = gl;

        this.tex = null;


        this.count = count;
        this._setup(count,height,width,depth);

        this.ico.setPosition([-width / 2,-height / 2,0]);


        let img = new Image();
        img.src = "circle.png";
        img.onload = () => {
            this.tex = createTexture(gl,{
                data:img
            })
        }

    }

    draw(camera,dt){



        let gl = this.gl;
        gl.enableDepth();
        gl.enableDepthWrite();

        if(this.tex !== null){

            this.tex.bind();
            this.ico.drawInstanced(this.count,camera,dt)

        }else{
            this.ico.drawInstanced(this.count,camera,dt)
        }
    }

    uniform(name,value){
        this.ico.state.uniform(name,value);
        return this;
    }

    _setup(particleCount,height,width,depth){

        // build positions for each instanced object
        let positions = []
        for ( var i = 0, i3 = 0, l = particleCount; i < l; i ++, i3 += 3 ) {
            positions.push(
                Math.random() * height - 1,
                Math.random() * width - 1,
                Math.random() * depth - 1
            )
        }

        // build icohedron
        this.ico = new Icohedron(this.gl);

        // setup instanced positions
        this.ico.state.setInstancedAttribute("iPos",positions);



    }
}

export default Cube;