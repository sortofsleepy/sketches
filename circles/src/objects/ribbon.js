import DrawState from '../jirachi/core/drawstate'
import {randInt} from "../jirachi/math/core";
import {compareV3,copyV3} from './utils'

import vert from '../shaders/ribbon.vert'
import frag from '../shaders/ribbon.frag'

class Ribbon {
    constructor(gl){

        // create a new draw state
        this.state = new DrawState(gl);
        this.state.setShader({
            vertex:vert,
            fragment:frag
        })

        this.uniforms = {
            map:[],
            alphaMap:[],
            useMap:0,
            useAlphaMap:0,
            color:[1,0,0],
            opacity:1,
            alphaTest:0.2,
            resolution:[window.innerWidth,window.innerHeight],
            sizeAttenuation:false,
            lineWidth:randInt(3,15),
            near:0.1,
            far:1000.0
        }

        window.addEventListener('resize',()=>{
            this.params.resolution = [window.innerWidth,window.innerHeight]
        });



    }


    /**
     * Constructs the mesh .
     * @param startingVertices
     */
    buildMesh(startingVertices){
        let gl = this.gl;
        this.positions = [];
        this.next = [];
        this.side = [];
        this.width = [];
        this.indices = [];
        this.uvs = [];
        this.counters = [];

        let g = startingVertices;
        for( let j = 0; j < g.length; j += 3 ) {
            let c = j/g.length;
            this.positions.push( g[ j ], g[ j + 1 ], g[ j + 2 ] );
            this.positions.push( g[ j ], g[ j + 1 ], g[ j + 2 ] );
            this.counters.push(c);
            this.counters.push(c);
        }



        this.process();

        this.state.setAttribute("position",this.positions);
        this.state.setAttribute("previous",this.previous);
        this.state.setAttribute("next",this.next);
        this.state.setAttribute("side",this.side,{
            size:1
        });
        this.state.setAttribute("uv",this.uvs,{
            size:2
        });
        this.state.setAttribute("counters",this.counters,{
            size:1
        });


        this.state.setIndex(this.indices);
    }

    draw(){
        this.state.useProgram();

        for(let i in this.uniforms){
            this.state.uniform(i,this.uniforms[i]);
        }
    }

    advance(worker,position){


    }

    process(){

        let l = this.positions.length / 6;

        this.previous = [];
        this.next = [];
        this.side = [];
        this.width = [];
        this.indices = [];
        this.uvs = [];

        for( let j = 0; j < l; j++ ) {
            this.side.push( 1 );
            this.side.push( -1 );
        }

        let w;
        for( let j = 0; j < l; j++ ) {
            //if( this.widthCallback ) w = this.widthCallback( j / ( l -1 ) );
            //else w = 1;
            w = 1;
            this.width.push( w );
            this.width.push( w );
        }

        for( let j = 0; j < l; j++ ) {
            this.uvs.push( j / ( l - 1 ), 0 );
            this.uvs.push( j / ( l - 1 ), 1 );
        }

        let v;

        if( compareV3(this.positions, 0, l - 1 ) ){
            v = copyV3(this.positions, l - 2 );
        } else {
            v = copyV3(this.positions, 0 );
        }
        this.previous.push( v[ 0 ], v[ 1 ], v[ 2 ] );
        this.previous.push( v[ 0 ], v[ 1 ], v[ 2 ] );
        for( let j = 0; j < l - 1; j++ ) {
            v = copyV3( this.positions,j );
            this.previous.push( v[ 0 ], v[ 1 ], v[ 2 ] );
            this.previous.push( v[ 0 ], v[ 1 ], v[ 2 ] );
        }

        for( let j = 1; j < l; j++ ) {
            v = copyV3( this.positions,j );
            this.next.push( v[ 0 ], v[ 1 ], v[ 2 ] );
            this.next.push( v[ 0 ], v[ 1 ], v[ 2 ] );
        }

        if( compareV3(this.positions, l - 1, 0 ) ){
            v = copyV3(this.positions, 1 );
        } else {
            v = copyV3(this.positions, l - 1 );
        }
        this.next.push( v[ 0 ], v[ 1 ], v[ 2 ] );
        this.next.push( v[ 0 ], v[ 1 ], v[ 2 ] );

        for( let j = 0; j < l - 1; j++ ) {
            let n = j * 2;
            this.indices.push( n, n + 1, n + 2 );
            this.indices.push( n + 2, n + 1, n + 3 );
        }


    }



}

export default Ribbon;