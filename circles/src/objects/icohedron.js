import Drawstate from '../jirachi/core/drawstate'
import createShader from '../jirachi/core/shader'
import {flattenArray} from "../jirachi/math/core";
import createVBO from '../jirachi/core/vbo'
import mat4 from '../jirachi/math/mat4'
import vec3 from '../jirachi/math/vec3'

import vert from '../shaders/ico.vert'
import frag from '../shaders/ico.frag'
import {calculateNormalMatrix} from "../jirachi/utils";

// more or less a port of https://github.com/hughsk/icosphere mashed together with
// https://github.com/glo-js/primitive-icosphere
// but adapted so as not to be reliant on a commonjs module
class Icohedron {
    constructor(gl,{
        subdivisions=3,
        vertex=vert,
        fragment=frag
    }={}){

        this.gl = gl;

        this.state = new Drawstate(gl);

        // set shader for the state
        this.state.setShader(createShader(gl,{
            vertex:vertex,
            fragment:fragment
        }));

        this.scale = 0.5;

        this._buildMesh(subdivisions);

        this.model = mat4.create();

        this.lightPos = null;
        this.eyePos = null;

        this.isBGElement = false;

        this.diffuseColor = [1.15, 0.15, 1.0];
        this.rimColor = [1.2, 2.0, 0.0];
        this.fogColor = [1.5,0.0,1.0];
        this.fogDensity = 0.02;
        this.position  = [];

    }

    setPosition(vec){
        if(vec instanceof Array && vec.length === 3){
            this.model = mat4.translate(this.model,this.model,vec);
            this.position = vec;
        }

        return this;
    }

    setLightPosition(lightPos){
        this.lightPos = lightPos;
        return this;
    }

    setEyePosition(eyePos){
        this.eyePos = eyePos;
        return this;
    }
    drawInstanced(numInstances,camera,dt){

        let gl = this.gl;


        // calculate normal matrix
        let normalMatrix = calculateNormalMatrix(this.model,camera.getViewMatrix());

        let rot = mat4.create();
        mat4.rotate(rot,this.model,dt,[1,1,1]);

        this.model = rot;

        gl.enableDepth();

        this.state.useProgram()
            .uniform("projectionMatrix",camera.getProjectionMatrix())
            .uniform("viewMatrix",camera.getViewMatrix())
            .uniform("normalMatrix",normalMatrix)
            .uniform("time",dt)
            .uniform("modelMatrix",this.model)
            .uniform("DiffuseLight",this.diffuseColor)
            .uniform("RimColor",this.rimColor)
            .uniform("fogColor",this.fogColor)
           // .uniform("resolution",[window.innerWidth,window.innerHeight])


        // reset model so we can keep it spinning at a constant rate.
        this.model = mat4.create();
        this.setPosition(this.position);


        if(this.isBGElement){
            this.state.uniform("bgElement",true);
        }
        this.state.shader.setFloat("FogDensity",this.fogDensity)
        //this.state.shader.setFloat("scale",this.scale);
        //this.state.drawElements();
        this.state.drawElementsInstanced(numInstances);


        //console.log(Math.sin(dt * 0.0005));


        gl.disableDepth();
    }

    /**
     * Draws the Icohedron
     * @param camera
     */
    draw(camera,dt){

        let gl = this.gl;


        // calculate normal matrix
        let normalMatrix = calculateNormalMatrix(this.model,camera.getViewMatrix());

        //this.model = mat4.rotate(this.model,this.model,0.002,[1,1,1]);

        gl.enableDepth();

        this.state.useProgram()
            .uniform("projectionMatrix",camera.getProjectionMatrix())
            .uniform("viewMatrix",camera.getViewMatrix())
            .uniform("normalMatrix",normalMatrix)
            .uniform("time",dt * 0.0005)
            .uniform("modelMatrix",this.model)
            .uniform("DiffuseLight",this.diffuseColor)
            .uniform("RimColor",this.rimColor)
            .uniform("fogColor",this.fogColor)



        if(this.isBGElement){
            this.state.uniform("bgElement",true);
        }
        this.state.shader.setFloat("FogDensity",this.fogDensity)
        this.state.shader.setFloat("scale",this.scale);
        this.state.drawElements();


        //console.log(Math.sin(dt * 0.0005));


        gl.disableDepth();
    }

    _buildMesh(subdivisions){

        let positions = []
        let faces = []
        let ids = [];
        let t = 0.5 + Math.sqrt(5) / 2

        positions.push([-1, +t,  0])
        positions.push([+1, +t,  0])
        positions.push([-1, -t,  0])
        positions.push([+1, -t,  0])

        positions.push([ 0, -1, +t])
        positions.push([ 0, +1, +t])
        positions.push([ 0, -1, -t])
        positions.push([ 0, +1, -t])

        positions.push([+t,  0, -1])
        positions.push([+t,  0, +1])
        positions.push([-t,  0, -1])
        positions.push([-t,  0, +1])


        faces.push([0, 11, 5])
        faces.push([0, 5, 1])
        faces.push([0, 1, 7])
        faces.push([0, 7, 10])
        faces.push([0, 10, 11])

        faces.push([1, 5, 9])
        faces.push([5, 11, 4])
        faces.push([11, 10, 2])
        faces.push([10, 7, 6])
        faces.push([7, 1, 8])

        faces.push([3, 9, 4])
        faces.push([3, 4, 2])
        faces.push([3, 2, 6])
        faces.push([3, 6, 8])
        faces.push([3, 8, 9])

        faces.push([4, 9, 5])
        faces.push([2, 4, 11])
        faces.push([6, 2, 10])
        faces.push([8, 6, 7])
        faces.push([9, 8, 1])


        let complex = {
            cells: faces
            , positions: positions
        }



        while (subdivisions-- > 0) {

            complex = this._subdivide(complex)
        }

        positions = complex.positions

        for (let i = 0; i < positions.length; i++) {

            this._normalize(positions[i])
        }

        for(let i = 0; i < complex.positions.length;++i){
            ids.push(Math.random());
        }
        // ============ BUILD UVs and NORMALS ====================== //
        let uvs = [];
        let normals = [];

        for(let i = 0; i < complex.positions.length; ++i){
            let position = complex.positions[i];
            let u = 0.5 * (-(Math.atan2(position[2], -position[0]) / Math.PI) + 1.0)
            let v = 0.5 + Math.asin(position[1]) / Math.PI
            uvs.push( 1 - u, 1 - v )

            let normal = vec3.normalize([0,0,0],position);
            normals.push(normal);
        }


        // ==========CALCULATE RANDOM DISPLACEMENT VALUE ============= //
        let displacement = new Float32Array(complex.positions.length * 3);
        for(let i = 0; i < complex.positions.length * 3; ++i){
            displacement[i] = Math.random() * 0.5;
        }

        // =========== BUILD BUFFERS AND ADD TO DRAWING STATE ================= //
        this.state.setAttribute('position',createVBO(this.gl,{
            data:flattenArray(complex.positions)
        }));

        this.state.setAttribute('normal',createVBO(this.gl,{
            data:flattenArray(normals)
        }));

        this.state.setAttribute('uv',createVBO(this.gl,{
            data:uvs
        }),{
            size:2
        })

        this.state.setIndex(createVBO(this.gl,{
            data:flattenArray(complex.cells),
            indexed:true
        }))


        this.cells = flattenArray(complex.cells);


        //this.addAttribute('position',flattenArray(complex.positions));
        //this.addIndices(flattenArray(complex.cells))


    }

    _normalize(vec){
        let mag = 0
        for (let n = 0; n < vec.length; n++) {
            mag += vec[n] * vec[n]
        }
        mag = Math.sqrt(mag)

        // avoid dividing by zero
        if (mag === 0) {
            return Array.apply(null, new Array(vec.length)).map(Number.prototype.valueOf, 0)
        }

        for (let n = 0; n < vec.length; n++) {
            vec[n] /= mag
        }

        return vec
    }

    _subdivide(complex){
        let positions = complex.positions
        let cells = complex.cells

        let newCells = []
        let newPositions = []
        let midpoints = {}
        let f = [0, 1, 2]
        let l = 0

        for (let i = 0; i < cells.length; i++) {
            let cell = cells[i]
            let c0 = cell[0]
            let c1 = cell[1]
            let c2 = cell[2]
            let v0 = positions[c0]
            let v1 = positions[c1]
            let v2 = positions[c2]

            let a = getMidpoint(v0, v1)
            let b = getMidpoint(v1, v2)
            let c = getMidpoint(v2, v0)

            let ai = newPositions.indexOf(a)
            if (ai === -1) ai = l++, newPositions.push(a)
            let bi = newPositions.indexOf(b)
            if (bi === -1) bi = l++, newPositions.push(b)
            let ci = newPositions.indexOf(c)
            if (ci === -1) ci = l++, newPositions.push(c)

            let v0i = newPositions.indexOf(v0)
            if (v0i === -1) v0i = l++, newPositions.push(v0)
            let v1i = newPositions.indexOf(v1)
            if (v1i === -1) v1i = l++, newPositions.push(v1)
            let v2i = newPositions.indexOf(v2)
            if (v2i === -1) v2i = l++, newPositions.push(v2)

            newCells.push([v0i, ai, ci])
            newCells.push([v1i, bi, ai])
            newCells.push([v2i, ci, bi])
            newCells.push([ai, bi, ci])
        }

        return {
            cells: newCells
            , positions: newPositions
        }

        // reuse midpoint vertices between iterations.
        // Otherwise, there'll be duplicate vertices in the final
        // mesh, resulting in sharp edges.
        function getMidpoint(a, b) {
            let point = midpoint(a, b)
            let pointKey = pointToKey(point)
            let cachedPoint = midpoints[pointKey]
            if (cachedPoint) {
                return cachedPoint
            } else {
                return midpoints[pointKey] = point
            }
        }

        function pointToKey(point) {
            return point[0].toPrecision(6) + ','
                + point[1].toPrecision(6) + ','
                + point[2].toPrecision(6)
        }

        function midpoint(a, b) {
            return [
                (a[0] + b[0]) / 2
                , (a[1] + b[1]) / 2
                , (a[2] + b[2]) / 2
            ]
        }
    }
}

export default Icohedron;