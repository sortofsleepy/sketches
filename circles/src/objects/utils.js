/**
 * Extra util for working with line meshes. In commonjs format to
 * allow for use in web worker.
 * @type {{compareV3(*, *, *): *, copyV3(*, *): *}}
 */
module.exports = {
    compareV3(arr,a,b){
        var aa = a * 6;
        var ab = b * 6;
        return ( arr[ aa ] === arr[ ab ] ) && ( arr[ aa + 1 ] === arr[ ab + 1 ] ) && ( arr[ aa + 2 ] === arr[ ab + 2 ] );

    },
    copyV3(arr,a){
        var aa = a * 6;
        return [ arr[ aa ], arr[ aa + 1 ], arr[ aa + 2 ] ];
    }

}