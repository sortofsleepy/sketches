
// https://developers.google.com/web/updates/2018/06/ar-for-the-web
//https://developers.google.com/web/updates/2018/05/welcome-to-immersive

/**
 * Sets up WebXR
 * @param cb {Function} A callback function to run when WebXR is supported
 * @param noSupportCb {Function} A callback function to run when there is no support for WebXR.
 */
export function setupXR(cb,noSupportCb){
    if(navigator.xr){
        navigator.xr.requestDevice().then(device => {
            cb(device);
        }).catch(err => {
            if(err.name === "NotFoundError"){

                let msg = "No XR Devices available";
                alert(msg);
                console.error(msg + ": ",err);
            }else{
                alert("Requesting XR Device failed");
                console.log("An error occured when requesting xr device");

            }
        })
    }else{
        //alert("This browser doesn't support the WebXR api");
        console.log("This browser doesn't support the WebXR api...starting default browser mode");

        noSupportCb();
    }
}

export function getContext(canvas){
    return canvas.getContext("xrpresent");
}

