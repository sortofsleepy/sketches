import {setupXR} from "./xr";
import createRenderer from './jirachi/core/gl'
import Camera, {setZoom} from "./jirachi/framework/camera";
import {startSketch,setupSketch} from './sketch'

let gl = null;
let cam = null;
let xrFrameOfRef = null;


setupXR(device => {

    let outputCanvas = document.createElement("canvas");
    let ctx = outputCanvas.getContext("xrpresent");

    device.requestSession({
        requestAR:true,
        outputContext:ctx
    }).then(session => {
        document.body.appendChild(outputCanvas);
        setup()
    })

},e => {

    setup();
    setupSketch(gl);
    animate();
});


function setup(session=null){

    if(session !== null){
        gl = createRenderer({
            webglOptions:{
                compatibleXRDevice:session.device
            }
        });
        session.baseLayer = new XRWebGLLayer(session,gl);
        session.requestFrameOfReference('eye-level').then((frameOfRef) => {
            xrFrameOfRef = frameOfRef;
            session.requestAnimationFrame(onXRFrame);
        });

    }else{
        gl = createRenderer();
        gl.setFullscreen().attachToScreen();
    }


    // setup the camera.
    cam = new Camera().setupPerspective(50,window.innerWidth / window.innerHeight,0.1,10000);
    cam = setZoom(cam,-300);

    window.addEventListener('resize',() => {
        cam.updatePerspective();
    })

}

/**
 * rAF for WebXR I'm assuming
 * @param t
 * @param frame
 */
function onXRFrame(t,frame){
    let session = frame.session;
    let pose = frame.getDevicePose(xrFrameOfRef);

}

// standard requestAnimationFrame.
function animate(){
    requestAnimationFrame(animate);
    gl.clearScreen();


    let time = performance.now() * 0.0005;
    startSketch(gl,cam,time);
}