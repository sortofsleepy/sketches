#include "AudioPlayer.h"
#include "cinder\Log.h"
using namespace ci;
using namespace std;


AudioPlayer::AudioPlayer():isPlaying(false),
currentPlayhead(0) {

	mBounds = Rectf(0, 0, app::getWindowWidth(), app::getWindowHeight());
	

	gl::GlslProg::Format fmt;

	fmt.vertex(app::loadAsset("sim.vert"));
	fmt.fragment(app::loadAsset("audio.glsl"));

	mShader = gl::GlslProg::create(fmt);
}


void AudioPlayer::loadFile(std::string filepath) {
	auto ctx = audio::Context::master();
	isPlaying = false;

	// By providing an FFT size double that of the window size, we 'zero-pad' the analysis data, which gives
	// an increase in resolution of the resulting spectrum data.
	auto monitorFormat = audio::MonitorSpectralNode::Format().fftSize(4048).windowSize(app::getWindowWidth() * 3);
	mMonitorSpectralNode = ctx->makeNode(new audio::MonitorSpectralNode(monitorFormat));


	// create a SourceFile and set its output samplerate to match the Context.
	audio::SourceFileRef sourceFile = audio::load(app::loadAsset(filepath), ctx->getSampleRate());

	// load the entire sound file into a BufferRef, and construct a BufferPlayerNode with this.
	audio::BufferRef buffer = sourceFile->loadBuffer();
	mBufferPlayerNode = ctx->makeNode(new audio::BufferPlayerNode(buffer));

	//mBufferPlayerNode >> mMonitorSpectralNode;

	// add a Gain to reduce the volume
	mGain = ctx->makeNode(new audio::GainNode(0.5f));

	// connect and enable the Context
	mBufferPlayerNode >> mGain >> mMonitorSpectralNode >> ctx->getOutput();


	mBufferPlayerNode->enable();

	// pause player right away so it doesn't start playing automatically.
	mBufferPlayerNode->stop();

	mMonitorSpectralNode->enable();
	ctx->enable();

	// try to pull spectrum size.
	mMagSpectrum = mMonitorSpectralNode->getMagSpectrum();


	// build buffer for magnitude spectrum
	audioData = gl::Vbo::create(GL_ARRAY_BUFFER,sizeof(float) * mMagSpectrum.size(),nullptr,GL_DYNAMIC_DRAW);

	mTex = gl::BufferTexture::create(audioData, GL_RGBA32F);
}

void AudioPlayer::play() {
	mBufferPlayerNode->start();
	mBufferPlayerNode->seekToTime(currentPlayhead);
	isPlaying = !isPlaying;
}

void AudioPlayer::pause() {
	currentPlayhead = mBufferPlayerNode->getReadPositionTime();

	mBufferPlayerNode->stop();
	isPlaying = !isPlaying;

}

void AudioPlayer::update() {
	if (isPlaying) {
		
		//mSpectrumPlot.setBounds(Rectf(0, 0, (float)app::getWindowWidth(), (float)app::getWindowHeight()));

		// We copy the magnitude spectrum out from the Node on the main thread, once per update:
		mMagSpectrum = mMonitorSpectralNode->getMagSpectrum();
	
		//audioData->bufferData(sizeof(float) * mMagSpectrum.size(),mMagSpectrum.data(),GL_DYNAMIC_DRAW);
		calculateMagnitude();

	}
}

void AudioPlayer::draw() {
	
	gl::setMatricesWindow(app::getWindowSize());
	gl::viewport(app::getWindowSize());

	gl::ScopedGlslProg shd(mShader);
	mTex->bindTexture(0);
	mShader->uniform("uTex0", 0);
	gl::drawSolidRect(mBounds);
	mTex->unbindTexture();

	//mSpectrumPlot.draw(mMagSpectrum);

}

void AudioPlayer::calculateMagnitude() {

	std::vector<float> processedData;


	size_t numBins = mMagSpectrum.size();

	size_t currVertex = 0;
	float m;

	for (size_t i = 0; i < numBins; i++) {
		m = mMagSpectrum[i];
		
		m = audio::linearToDecibel(m) / 100;
		processedData.push_back(m);
		currVertex += 2;
	}


	audioData->bufferData(sizeof(float) * processedData.size(), processedData.data(), GL_DYNAMIC_DRAW);

}