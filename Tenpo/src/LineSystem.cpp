//
//  LineSystem.cpp
//  ParticleFeedback
//
//  Created by Joseph Chow on 9/27/17.
//

#include "LineSystem.hpp"
#include "cinder/Log.h"
using namespace ci;
using namespace std;

LineSystem::LineSystem() {}

void LineSystem::setup(int numParticles, float uEnd, float numSeg, float uLength) {
	this->numParticles = numParticles;
	this->numSeg = numSeg;
	this->uEnd = uEnd;
	this->uLength = uLength;

	mCopyShader = gl::getStockShader(gl::ShaderDef().texture());

	setupRender();

	gl::Texture::Format tfmt;
	tfmt.setInternalFormat(GL_RGBA32F);
	tfmt.setMinFilter(GL_NEAREST);
	tfmt.setMagFilter(GL_NEAREST);
	tfmt.wrap(GL_CLAMP_TO_EDGE);

	gl::Fbo::Format fmt;
	fmt.setColorTextureFormat(tfmt);


	for (int i = 0; i < numSeg; ++i) {
		auto fbo = gl::Fbo::create(numParticles, numParticles, fmt);
		gl::ScopedFramebuffer f(fbo); {
			gl::clear();
		}
		linemaps.push_back(fbo);
	}

}

void LineSystem::draw(ArcCamRef mCam, ci::gl::TextureRef curr, ci::gl::TextureRef next, ci::gl::TextureRef extra) {

	mCam->useMatrices();
	gl::ScopedTextureBind tex0(curr, 0);
	gl::ScopedTextureBind tex1(next, 1);
	gl::ScopedTextureBind tex2(extra, 2);


	mRenderBatch->getGlslProg()->uniform("textureCurr", 0);
	mRenderBatch->getGlslProg()->uniform("textureNext", 1);
	mRenderBatch->getGlslProg()->uniform("textureExtra", 2);

	mRenderBatch->getGlslProg()->uniform("uNumSeg", numSeg);
	mRenderBatch->getGlslProg()->uniform("uEnd", uEnd);
	mRenderBatch->getGlslProg()->uniform("uLength", uLength);

	mRenderBatch->draw();


}

void LineSystem::render(ArcCamRef mCam, ci::gl::TextureRef extra) {
	gl::setMatricesWindow(app::getWindowSize());
	gl::viewport(app::getWindowSize());
	for (int i = 1; i < linemaps.size() - 2; ++i) {
		auto curr = linemaps[i]->getColorTexture();
		auto next = linemaps[i + 2]->getColorTexture();

		draw(mCam, curr, next, extra);
	}
}

void LineSystem::setupRender() {
	vector<vec3> positions;
	vector<int> indices;
	int count = 0;

	for (int j = 0; j < numParticles; ++j) {
		for (int i = 0; i < numParticles; ++i) {
			auto ux = (float)i / numParticles;
			auto uy = (float)j / numParticles;

			positions.push_back(vec3(ux, uy, 0));
			positions.push_back(vec3(ux, uy, 1));

			indices.push_back(count);
			indices.push_back(count + 1);
			count += 2;
		}
	}

	gl::VboMesh::Layout layout;
	layout.attrib(geom::POSITION, 3);

	mRenderMesh = gl::VboMesh::create(positions.size(), GL_LINES, { layout }, indices.size(), GL_UNSIGNED_INT);

	mRenderMesh->bufferAttrib(geom::POSITION, sizeof(vec3) * positions.size(), positions.data());
	mRenderMesh->bufferIndices(sizeof(int) * indices.size(), indices.data());

	gl::GlslProg::Format fmt;
	fmt.vertex(app::loadAsset("line.vert"));
	fmt.fragment(app::loadAsset("line.frag"));
	mRenderShader = gl::GlslProg::create(fmt);


	mRenderBatch = gl::Batch::create(mRenderMesh, mRenderShader);
}

void LineSystem::reset(ci::gl::FboRef fbo) {
	for (int i = 0; i < linemaps.size(); ++i) {
		save(fbo);
	}
}

void LineSystem::save(ci::gl::FboRef fbo) {
	auto targetFbo = linemaps.back();
	linemaps.pop_back();

	targetFbo->bindFramebuffer();
	gl::clear(ColorA(0, 0, 0, 0));
	gl::setMatricesWindow(targetFbo->getSize());
	gl::ScopedViewport view(targetFbo->getSize());

	gl::ScopedGlslProg shd(mCopyShader);
	gl::ScopedTextureBind tex0(fbo->getColorTexture(), 0);
	gl::drawSolidRect(targetFbo->getBounds());

	targetFbo->unbindFramebuffer();

	linemaps.emplace(linemaps.begin(), targetFbo);
}
