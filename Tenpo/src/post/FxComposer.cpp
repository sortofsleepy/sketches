//
//  FxComposer.cpp
//  ParticleFeedback
//
//  Created by Joseph Chow on 9/28/17.
//

#include "post/FxComposer.hpp"
using namespace ci;
using namespace std;

FxComposer::FxComposer(int width, int height,bool internalScene) {
	
	this->width = width;
	this->height = height;
	hasInternalScene = internalScene;

	if (internalScene) {
		gl::Texture::Format fmt;
		fmt.setInternalFormat(GL_RGBA);

		gl::Fbo::Format ffmt;
		ffmt.setColorTextureFormat(fmt);

		mSceneFbo = gl::Fbo::create(width, height);
		gl::ScopedFramebuffer fbo(mSceneFbo); {
			gl::clear(ColorA(0, 0, 0, 0), true);
		}
	}


}


void FxComposer::bind() {
	mSceneFbo->bindFramebuffer();
	gl::clear(ColorA(0, 0, 0, 0));
}

void FxComposer::unbind() {
	mSceneFbo->unbindFramebuffer();
}


void FxComposer::addPass(FxpassRef pass) {
	passes.push_back(pass);
}

void FxComposer::compile() {

	//! Stack passes - first pass takes input from scene fbo, next scenes take the output
	//! from the previous scene's output.
	for (int i = 0; i < passes.size(); ++i) {
		if (i == 0) {
			passes[i]->setInput(mSceneFbo->getColorTexture());
		}
		else {
			auto prevPass = passes[i - 1];
			passes[i]->setInput(prevPass->getOutput());
		}
	}
}

void FxComposer::compile(ci::gl::TextureRef mInputTex) {
	//! Stack passes - first pass takes input from scene fbo, next scenes take the output
	//! from the previous scene's output.
	for (int i = 0; i < passes.size(); ++i) {
		if (i == 0) {
			passes[i]->setInput(mInputTex);
		}
		else {
			CI_LOG_I("MADE IT!");
			auto prevPass = passes[i - 1];
			passes[i]->setInput(prevPass->getOutput());
		}
	}
}

void FxComposer::update() {
	if (passes.size() > 0) {
		for (int i = 0; i < passes.size(); ++i) {
			if (i == 0) {
				passes[i]->update();
			}
			else {
				passes[i]->update(true);
			}
		}
	}
}

void FxComposer::update(std::function<void(int index, FxpassRef pass)> func) {
	if (passes.size() > 0) {
		for (int i = 0; i < passes.size(); ++i) {


			func(i, passes[i]);
			if (i == 0) {
				passes[i]->update();
			}
			else {
				passes[i]->update(true);
			}
		}
	}
}

void FxComposer::drawOutput() {
	gl::setMatricesWindow(app::getWindowSize());
	gl::ScopedViewport view(app::getWindowSize());
	gl::draw(mSceneFbo->getColorTexture(), Rectf(0, 0, app::getWindowWidth(), app::getWindowHeight()));


	if (passes.size() > 0) {
		gl::draw(passes[passes.size() - 1]->getOutput(), Rectf(0, 0, app::getWindowWidth(), app::getWindowHeight()));
	}
	else {
		gl::draw(mSceneFbo->getColorTexture(), Rectf(0, 0, app::getWindowWidth(), app::getWindowHeight()));
	}

}

gl::TextureRef FxComposer::getOutput() {

	if (passes.size() > 0) {
		return passes[passes.size() - 1]->getOutput();
	}
	else {
		return mSceneFbo->getColorTexture();
	}

}


gl::TextureRef FxComposer::getSceneTexture() {

	return mSceneFbo->getColorTexture();

}
