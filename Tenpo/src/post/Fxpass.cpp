//
//  FxPass.cpp
//  ParticleFeedback
//
//  Created by Joseph Chow on 9/28/17.
//

#include "post/Fxpass.hpp"
using namespace ci;
using namespace std;

Fxpass::Fxpass() :currentFbo(0), targetFbo(1), numPasses(1) {

	width = ci::app::getWindowWidth();
	height = ci::app::getWindowHeight();

	mInput = nullptr;
}

void Fxpass::setup(bool useDepth) {
	gl::Fbo::Format ffmt;
	// if using depth texture, build shader so we can visualize and check
	if (useDepth) {
		gl::GlslProg::Format fmt;
		fmt.vertex(depthPass);
		fmt.fragment(depthFrag);
		mDepthShader = gl::GlslProg::create(fmt);
	}

	// setup default texture format
	gl::Texture::Format fmt;
	fmt.wrap(GL_CLAMP_TO_EDGE);
	fmt.setMagFilter(GL_LINEAR);
	fmt.setMinFilter(GL_LINEAR);
	fmt.setInternalFormat(GL_RGBA);



	// build depth atachment if specified.
	if (useDepth) {
		gl::Texture::Format dfmt;
		dfmt.wrap(GL_CLAMP_TO_EDGE);
		dfmt.setMagFilter(GL_NEAREST);
		dfmt.setMinFilter(GL_NEAREST);
		dfmt.setInternalFormat(GL_DEPTH_COMPONENT32F);

		//dfmt.setCompareMode(GL_COMPARE_REF_TO_TEXTURE);
		//dfmt.setCompareFunc(GL_LEQUAL);

		ffmt.attachment(GL_DEPTH_ATTACHMENT, gl::Texture2d::create(width, height, dfmt));
	}


	for (int i = 0; i < 2; ++i) {
		fbos[i] = gl::Fbo::create(width, height, ffmt);

		gl::ScopedFramebuffer fbo(fbos[i]);
		gl::clear();
	}


}

void Fxpass::bind(int index) {
	lastIndex = index;
	fbos[index]->bindFramebuffer();
}

void Fxpass::unbind() {
	fbos[lastIndex]->unbindFramebuffer();
	lastIndex = 0;
}



void Fxpass::setInput(ci::gl::TextureRef mInput) {
	textureInput = mInput;
	
	// ==== WRITE INPUT TO ALL COLOR ATTACHMENTS ==== //

	gl::ScopedGlslProg prog(mShader);
	gl::ScopedTextureBind tex0(mInput, 0);

	mShader->uniform("uTex0", 0);
	bind(0);
	gl::clear(ColorA(0, 0, 0, 0), 1);
	gl::setMatricesWindow(fbos[0]->getSize());
	gl::ScopedViewport view(fbos[0]->getSize());

	gl::drawSolidRect(fbos[0]->getBounds());
	unbind();

	bind(1);
	gl::clear(ColorA(0, 0, 0, 0), 1);
	gl::setMatricesWindow(fbos[0]->getSize());
	gl::ScopedViewport view2(fbos[0]->getSize());

	gl::drawSolidRect(fbos[0]->getBounds());
	unbind();

}

void Fxpass::setInput(ci::gl::FboRef mInput) {
	this->mInput = mInput;

	gl::ScopedTextureBind tex1(this->mInput->getColorTexture(), 0);
	mShader->uniform("uTex0", 0);

	// ==== WRITE INPUT TO ALL COLOR ATTACHMENTS ==== //

	bind(0);
	gl::clear(ColorA(0, 0, 0, 0), true);
	gl::setMatricesWindow(fbos[0]->getSize());
	gl::ScopedViewport view2(fbos[0]->getSize());
	gl::ScopedGlslProg mshd(mShader);

	gl::drawSolidRect(fbos[0]->getBounds());

	unbind();

	bind(1);
	gl::clear(ColorA(0, 0, 0, 0), true);
	gl::setMatricesWindow(fbos[0]->getSize());
	gl::ScopedViewport view1(fbos[0]->getSize());
	gl::ScopedGlslProg mshd2(mShader);


	gl::drawSolidRect(fbos[0]->getBounds());


	unbind();

}
void Fxpass::updateFbosWithNewScene() {

	if (!mInput) {
		//CI_LOG_I("input not set for Fxpass");
		return;
	}

	gl::ScopedTextureBind tex1(mInput->getColorTexture(), 0);
	mShader->uniform("uTex0", 0);

	// ==== WRITE INPUT TO ALL COLOR ATTACHMENTS ==== //

	bind(0);
	gl::clear(ColorA(0, 0, 0, 0), true);
	gl::setMatricesWindow(fbos[0]->getSize());
	gl::ScopedViewport view1(fbos[0]->getSize());
	gl::ScopedGlslProg mshd(mShader);

	gl::drawSolidRect(fbos[0]->getBounds());

	unbind();

	bind(1);
	gl::clear(ColorA(0, 0, 0, 0), true);
	gl::setMatricesWindow(fbos[0]->getSize());
	gl::ScopedViewport view2(fbos[0]->getSize());
	gl::ScopedGlslProg mshd2(mShader);


	gl::drawSolidRect(fbos[0]->getBounds());


	unbind();
}

void Fxpass::updateInputTexture() {
	gl::ScopedTextureBind tex1(textureInput, 0);
	mShader->uniform("uTex0", 0);

	// ==== WRITE INPUT TO ALL COLOR ATTACHMENTS ==== //

	bind(0);
	gl::clear(ColorA(0, 0, 0, 0), true);
	gl::setMatricesWindow(fbos[0]->getSize());
	gl::ScopedViewport view1(fbos[0]->getSize());
	gl::ScopedGlslProg mshd(mShader);

	gl::drawSolidRect(fbos[0]->getBounds());

	unbind();

	bind(1);
	gl::clear(ColorA(0, 0, 0, 0), true);
	gl::setMatricesWindow(fbos[0]->getSize());
	gl::ScopedViewport view2(fbos[0]->getSize());
	gl::ScopedGlslProg mshd2(mShader);


	gl::drawSolidRect(fbos[0]->getBounds());


	unbind();
}
void Fxpass::update(bool useTexture) {


	if (!textureInput && mInput) {
		updateFbosWithNewScene();
	}
	else if(!mInput && textureInput){
		updateInputTexture();
	}

	for (int i = 0; i < numPasses; ++i) {
		gl::ScopedFramebuffer fbo(fbos[targetFbo]);

		gl::setMatricesWindow(fbos[0]->getSize());
		gl::ScopedViewport view(fbos[0]->getSize());

		// bind shader
		gl::ScopedGlslProg shd(mShader);

		gl::ScopedTextureBind tex0(fbos[currentFbo]->getColorTexture(), 0);
		mShader->uniform("uTex0", 0);

		if (onUpdate != nullptr) {
			onUpdate(mShader, currentFbo);
		}

		gl::drawSolidRect(fbos[0]->getBounds());

		std::swap(targetFbo, currentFbo);
	}
}

void Fxpass::setUpdateFunction(std::function<void(ci::gl::GlslProgRef, int fboIndex)> onUpdate) {
	this->onUpdate = onUpdate;
}

void Fxpass::loadShader(std::string fragment, std::string vertex) {
	gl::GlslProg::Format fmt;
	if (vertex == "") {
		fmt.vertex(passthru);
	}
	else {
		fmt.vertex(app::loadAsset(vertex));
	}
	fmt.fragment(app::loadAsset(fragment));
	fmt.version(330);

	try {
		mShader = gl::GlslProg::create(fmt);
	}


	catch (ci::gl::GlslProgCompileExc &e) {
		CI_LOG_E("FxPass - Error loading shader");
		CI_LOG_E("FxPass | ShaderLoadError :" << e.what());
	}
}
