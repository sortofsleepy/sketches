#include "cinder/app/App.h"
#include "cinder/app/RendererGl.h"
#include "cinder/gl/gl.h"
#include "cinder/Rand.h"
#include <list>
#include "CinderImGui.h"
#include "CubedCube.h"
#include "ArcCam.h"
#include "AudioPlayer.h"
#include "post/FxComposer.hpp"
#include "WindowTypes.h"
#include "MainApp.h"
#include "VisualSettings.h"

using namespace ci;
using namespace ci::app;
using namespace std;


// We'll create a new Cinder Application by deriving from the App class
class TenpoApp : public App {
public:
	void setup();

	MainApp * mainWindow;
	GuiWindow * gui;

};



void TenpoApp::setup()
{

	// setup the main window!
	mainWindow = new MainApp();
	mainWindow->setup();
	getWindow()->setUserData(mainWindow);
	mainWindow->setFunctions(getWindowIndex(0));

	// setup gui window 
	gui = new GuiWindow();

	/*
	WindowRef guiWindow = createWindow(app::Window::Format().size(vec2(640, 480)));
	guiWindow->setUserData(gui);
	gui->setup(guiWindow);
	guiWindow->setPos(vec2(getWindowIndex(0)->getPos() - ivec2(700, 0)));*/
}




// This line tells Cinder to actually create the application
CINDER_APP(TenpoApp, RendererGl, [](App::Settings *settings) {
	settings->setWindowSize(1280, 720);
	settings->setMultiTouchEnabled(false);
});