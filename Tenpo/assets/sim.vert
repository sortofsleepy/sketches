#version 330

uniform mat4 ciModelViewProjection;
in vec4 ciPosition;
in vec2 ciTexCoord0;
out vec2 vUv;
const vec2 scale = vec2(0.5, 0.5);

flat out int vVertexId;
void main() {
	vUv = ciTexCoord0;
	vVertexId = gl_VertexID;
	gl_Position = ciModelViewProjection * ciPosition;
}
