#version 150

in vec4 ciPosition;
out vec2 uv;
const vec2 scale = vec2(0.5, 0.5);
                                     
void main() {
	uv = ciPosition.xy * scale + scale;
	gl_Position =  ciPosition;
}