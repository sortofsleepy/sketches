#version 330

precision highp float;


in vec2 vUv;


uniform sampler2D texturePos;
uniform sampler2D textureVel;
uniform sampler2D textureExtra;
uniform sampler2D textureOrigPos;

uniform float uEnd = 100.0;
uniform float uNumSeg = 40.0;
uniform float uLength = 30.0;
uniform float time;

layout (location = 0) out vec4 oPos;
layout (location = 1) out vec4 oVel;
layout (location = 2) out vec4 oExtra;
layout (location = 3) out vec4 oOrigPos;

#include "utils.glsl"
void main(){

	vec3 pos        = texture(texturePos, vUv).rgb;
	vec3 vel        = texture(textureVel, vUv).rgb;
	vec3 extra      = texture(textureExtra, vUv).rgb;
	vec3 orgPos     = texture(textureOrigPos, vUv).rgb;
	

	float posOffset = mix(extra.r, 1.0, .925) * 0.2;
	vec3 acc        = curlNoise(pos * posOffset + time * .35);
	// float speed = mix(extra.g, 1.0, .95);
	float speed = 1.0 + extra.g * 0.5;
	speed = pow(speed, 2.0) * (1.5 + acc.r);

	if(extra.b < uNumSeg) {
		speed *= 0.001;
	}
	
	vel += acc * .001 * speed;
	vec3 dir = normalize(pos);
	vel += dir * 0.002 * speed;



	const float decrease = .93;
	vel *= decrease;

	extra.b += 1.0;
	pos += vel;

	if(extra.b > uEnd + uLength) {
		vec3 axis = normalize(extra);
		orgPos = rotate(orgPos, axis, time * extra.r * extra.g);
		pos = orgPos;
		extra.b = 0.0;
	}
	oPos = vec4(pos,1.);
	oVel = vec4(vel,1.);
	oExtra = vec4(extra,1.);
	oOrigPos = vec4(orgPos,1.);
}


/*


uniform sampler2D texturePos;
uniform sampler2D textureVel;
uniform sampler2D textureExtra;
uniform sampler2D textureOrigPos;

uniform float uEnd = 100.0;
uniform float uNumSeg = 40.0;
uniform float uLength = 30.0;
uniform float time;
uniform vec2 resolution;

layout (location = 0) out vec4 oPos;
layout (location = 1) out vec4 oVel;
layout (location = 2) out vec4 oExtra;
layout (location = 3) out vec4 oOrigPos;


	float posOffset = mix(extra.r, 1.0, .925) * 0.2;
	vec3 acc        = curlNoise(pos * posOffset + time * .35);
	// float speed = mix(extra.g, 1.0, .95);
	float speed = 1.0 + extra.g * 0.5;
	speed = pow(speed, 2.0) * (1.5 + acc.r);

	if(extra.b < uNumSeg) {
		speed *= 0.001;
	}
	
	vel += acc * .001 * speed;
	vec3 dir = normalize(pos);
	vel += dir * 0.002 * speed;



	const float decrease = .93;
	vel *= decrease;

	extra.b += 1.0;
	pos += vel;

	if(extra.b > uEnd + uLength) {
		vec3 axis = normalize(extra);
		orgPos = rotate(orgPos, axis, time * extra.r * extra.g);
		pos = orgPos;
		extra.b = 0.0;
	}

	*/

