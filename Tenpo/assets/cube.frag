#version 150
uniform float time;
out vec4 glFragColor;
in vec3 vColor;
in float vScale;


vec3 HUEtoRGB(float H){
	H = mod(H,1.0);
	float R = abs(H * 6.0 - 3.0) - 1.0;
	float G = 2.0 - abs(H * 6.0 - 2.0);
	float B = 2.0 - abs(H * 6.0 - 4.0);
	return clamp(vec3(R,G,B),0.0,1.0);
	
}

vec3 HSLtoRGB(vec3 HSL){
	vec3 RGB = HUEtoRGB(HSL.x);
	float C = (1.0 - abs(2.0 * HSL.z - 1.0)) * HSL.y;
	return (RGB - 0.5) * C + HSL.z;
}

void main(){
	//glFragColor = vec4(vColor,1.);

	vec4 col = vec4( vColor.xyz * HSLtoRGB(vec3(vScale/2.0, 1.0, vColor.r)), 1. );

	if(col.r == 0.0){
		col += vec4(vColor * HUEtoRGB(vColor.g),1.);
	}

	glFragColor = vec4( vColor.xyz * HSLtoRGB(vec3(vScale/2.0, 1.0, vColor.r)), 1. );

}