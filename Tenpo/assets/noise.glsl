
uniform sampler2D uTex0;

// control parameter
uniform float time;

uniform bool grayscale;

// noise effect intensity value (0 = no effect 1 = full effect)
uniform float nIntensity = 0.5;

// scanlines effect intensity value (0 = no effect 1 = full effect)
uniform float sIntensity = 0.05;

// scanlines effect count value (0 = no effect 4096 = full effect)
uniform float sCount = 4096;

uniform sampler2D tDiffuse;

in vec2 vUv;

out vec4 glFragColor;
highp float rand( const in vec2 uv ) {
    const highp float a = 12.9898, b = 78.233, c = 43758.5453;
    highp float dt = dot( uv.xy, vec2( a,b ) ), sn = mod( dt, 3.14149 );
    return fract(sin(sn) * c);
}
void main(){
    
    vec4 cTextureScreen = texture( uTex0, vUv );
    // make some noise
    float dx = rand( vUv + time );
    
    // add noise
    vec3 cResult = cTextureScreen.rgb + cTextureScreen.rgb * clamp( 0.1 + dx, 0.0, 1.0 );
    
    // get us a sine and cosine
    vec2 sc = vec2( sin( vUv.y * sCount ), cos( vUv.y * sCount ) );
    
    // add scanlines
    cResult += cTextureScreen.rgb * vec3( sc.x, sc.y, sc.x ) * sIntensity;
    
    // interpolate between source and result by intensity
    cResult = cTextureScreen.rgb + clamp( nIntensity, 0.0,1.0 ) * ( cResult - cTextureScreen.rgb );
    
    
    glFragColor =  vec4( cResult, cTextureScreen.a );

}
