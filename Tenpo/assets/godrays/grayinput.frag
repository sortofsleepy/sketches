#version 150

#define TAPS_PER_PASS 6.0

in vec2 vUv;
uniform sampler2D tInput;
uniform vec2 vSunPositionScreenSpace;
uniform float fStepSize;// filter step size

out vec4 glFragColor;


void main() {

	// delta from current pixel to sun position
	vec2 delta = vSunPositionScreenSpace - vUv;
	float dist = length( delta );

	// Step vector (uv space)
	vec2 stepv = fStepSize * delta / dist;

	// Number of iterations between pixel and sun
float iters = dist/fStepSize;

vec2 uv = vUv.xy;
float col = 0.0;


if ( 0.0 <= iters && uv.y < 1.0 ) col += texture( tInput, uv ).r;
uv += stepv;

if ( 1.0 <= iters && uv.y < 1.0 ) col += texture( tInput, uv ).r;
uv += stepv;

if ( 2.0 <= iters && uv.y < 1.0 ) col += texture( tInput, uv ).r;
uv += stepv;

if ( 3.0 <= iters && uv.y < 1.0 ) col += texture( tInput, uv ).r;
uv += stepv;

if ( 4.0 <= iters && uv.y < 1.0 ) col += texture( tInput, uv ).r;
uv += stepv;

if ( 5.0 <= iters && uv.y < 1.0 ) col += texture( tInput, uv ).r;
uv += stepv;

// Should technically be dividing by 'iters', but 'TAPS_PER_PASS' smooths out
// objectionable artifacts, in particular near the sun position. The side
// effect is that the result is darker than it should be around the sun, as
// TAPS_PER_PASS is greater than the number of samples actually accumulated.
// When the result is inverted (in the shader 'godrays_combine', this produces
// a slight bright spot at the position of the sun, even when it is occluded.

//glFragColor = vec4( col/iters );
	glFragColor = vec4( col/TAPS_PER_PASS );

	glFragColor.a = 1.0;

}



/*
// delta from current pixel to sun position
vec2 delta = vSunPositionScreenSpace - vUv;
float dist = length( delta );

// Step vector (uv space)

vec2 stepv = fStepSize * delta / dist;

// Number of iterations between pixel and sun
float iters = dist/fStepSize;

vec2 uv = vUv.xy;
float col = 0.0;


if ( 0.0 <= iters && uv.y < 1.0 ) col += texture( tInput, uv ).r;
uv += stepv;

if ( 1.0 <= iters && uv.y < 1.0 ) col += texture( tInput, uv ).r;
uv += stepv;

if ( 2.0 <= iters && uv.y < 1.0 ) col += texture( tInput, uv ).r;
uv += stepv;

if ( 3.0 <= iters && uv.y < 1.0 ) col += texture( tInput, uv ).r;
uv += stepv;

if ( 4.0 <= iters && uv.y < 1.0 ) col += texture( tInput, uv ).r;
uv += stepv;

if ( 5.0 <= iters && uv.y < 1.0 ) col += texture( tInput, uv ).r;
uv += stepv;

// Should technically be dividing by 'iters', but 'TAPS_PER_PASS' smooths out
// objectionable artifacts, in particular near the sun position. The side
// effect is that the result is darker than it should be around the sun, as
// TAPS_PER_PASS is greater than the number of samples actually accumulated.
// When the result is inverted (in the shader 'godrays_combine', this produces
// a slight bright spot at the position of the sun, even when it is occluded.

glFragColor = vec4( col/iters );

glFragColor.a = 1.0;
*/
