#version 150 

in vec3 ciPosition;
in vec3 instancePosition;
in float instanceRotation;
in vec3 instanceColor;
in int instanceSpectrumIndex;

uniform float time;
uniform mat4 ciModelViewProjection;

#include "rotate.glsl"
uniform samplerBuffer scaleVal;

out float vScale;
out vec3 vColor;


void main(){
	// re-reference cube vertices 
	vec3 basePos = ciPosition;

	// set scale
	vec3 trTime = vec3(instancePosition.x + time,instancePosition.y + time,instancePosition.z + time);
	vec3 arTime = texelFetch(scaleVal,instanceSpectrumIndex).xyz;

	trTime = arTime;
	vScale = sin( trTime.x * 2.1 ) + sin( trTime.y * 3.2 ) + sin( trTime.z * 4.3 );
	
	basePos *= vec3(vScale);

	vec3 pos = basePos;

	// rotate
	pos = rotateX(pos,time * instanceRotation);
	pos = rotateY(pos,time * instanceRotation);
	pos = rotateZ(pos,time * instanceRotation);

	// set each instance's position
	pos += instancePosition;

	vColor = instanceColor;
	
	gl_Position = ciModelViewProjection * vec4(pos,1.);
}