#version 150

uniform sampler2D uTex0;

in vec2 uv;

out vec4 glFragColor;

void main(){
	glFragColor = texture(uTex0,uv);
}