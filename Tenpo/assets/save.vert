// save.vert
#version 150
precision highp float;
in vec3 ciPosition;
in vec2 ciTexCoord0;
in vec3 aExtra;
uniform mat4 ciModelViewProjection;
out vec2 vTextureCoord;
out vec3 vColor;
out vec3 vNormal;
out vec3 vExtra;

void main(void) {
	vColor       = ciPosition;
	vec3 pos     = vec3(ciTexCoord0, 0.0);
	gl_Position  = vec4(pos, 1.0);

	gl_PointSize = 1.0;

	vExtra       = aExtra;
}
