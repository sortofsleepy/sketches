

in vec2 uv;
uniform sampler2D uTex0;
uniform vec2 sample_offset;
out vec4 glFragColor;
void main(){
	vec2 vUv = uv;
    float attenuation = 2.6;
    vec3 sum = vec3( 0.0, 0.0, 0.0 );
    
    //float sample_offset = 0.005;
    sum += texture( uTex0, vUv + -10.0 * sample_offset ).rgb * 0.009167927656011385;
    sum += texture( uTex0, vUv +  -9.0 * sample_offset ).rgb * 0.014053461291849008;
    sum += texture( uTex0, vUv +  -8.0 * sample_offset ).rgb * 0.020595286319257878;
    sum += texture( uTex0, vUv +  -7.0 * sample_offset ).rgb * 0.028855245532226279;
    sum += texture( uTex0, vUv +  -6.0 * sample_offset ).rgb * 0.038650411513543079;
    sum += texture( uTex0, vUv +  -5.0 * sample_offset ).rgb * 0.049494378859311142;
    sum += texture( uTex0, vUv +  -4.0 * sample_offset ).rgb * 0.060594058578763078;
    sum += texture( uTex0, vUv +  -3.0 * sample_offset ).rgb * 0.070921288047096992;
    sum += texture( uTex0, vUv +  -2.0 * sample_offset ).rgb * 0.079358891804948081;
    sum += texture( uTex0, vUv +  -1.0 * sample_offset ).rgb * 0.084895951965930902;
    sum += texture( uTex0, vUv +   0.0 * sample_offset ).rgb * 0.086826196862124602;
    sum += texture( uTex0, vUv +  +1.0 * sample_offset ).rgb * 0.084895951965930902;
    sum += texture( uTex0, vUv +  +2.0 * sample_offset ).rgb * 0.079358891804948081;
    sum += texture( uTex0, vUv +  +3.0 * sample_offset ).rgb * 0.070921288047096992;
    sum += texture( uTex0, vUv +  +4.0 * sample_offset ).rgb * 0.060594058578763078;
    sum += texture( uTex0, vUv +  +5.0 * sample_offset ).rgb * 0.049494378859311142;
    sum += texture( uTex0, vUv +  +6.0 * sample_offset ).rgb * 0.038650411513543079;
    sum += texture( uTex0, vUv +  +7.0 * sample_offset ).rgb * 0.028855245532226279;
    sum += texture( uTex0, vUv +  +8.0 * sample_offset ).rgb * 0.020595286319257878;
    sum += texture( uTex0, vUv +  +9.0 * sample_offset ).rgb * 0.014053461291849008;
    sum += texture( uTex0, vUv + +10.0 * sample_offset ).rgb * 0.009167927656011385;
    
    glFragColor = vec4(attenuation * sum,1.);
    
}

