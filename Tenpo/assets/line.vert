#version 330 core

precision highp float;
in vec3 ciPosition;
//attribute vec3 aNormal;

uniform mat4 ciModelViewProjection;

uniform sampler2D textureCurr;
uniform sampler2D textureNext;
uniform sampler2D textureExtra;

uniform float uNumSeg=40.0;
uniform float uEnd=100.0;
uniform float uLength=100.0;

out vec3 vColor;
out float vAlpha;

void main(void) {
    

    vec2 uv      = ciPosition.xy;
    vec3 posCurr = texture(textureCurr, uv).rgb;
    vec3 posNext = texture(textureNext, uv).rgb;
    vec3 extra   = texture(textureExtra, uv).rgb;
    
    float a      = 1.0;
    if(extra.b > uEnd + uLength) {
        a = 0.0;
    } else if(extra.b > uEnd ) {
        a = smoothstep(uEnd + uLength, uEnd, extra.b);
    }
    
    const float lr = 20.0;
    
    if(extra.b < uNumSeg) {
        a = 0.0;
    } else if(extra.b < uNumSeg + lr) {
        a = smoothstep(uNumSeg, uNumSeg + lr, extra.b);
    }
    
    a = pow(a, 3.0);
    
    vAlpha = a;
    
    vec3 position = mix(posCurr, posNext, ciPosition.z);
    vColor = vec3(uv,position.z) + vec3(1.0,1.0,0.0);
    
    
    //gl_Position = uProjectionMatrix * uViewMatrix * uModelMatrix * vec4(position, 1.0);
    gl_Position = ciModelViewProjection * vec4(position,1.);
 
    
}

