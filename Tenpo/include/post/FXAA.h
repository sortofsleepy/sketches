#pragma once

#include "Fxpass.hpp"

class FXAA : public Fxpass {
public:
	FXAA() {
		loadShader("post/fxaa.glsl");
	}

};