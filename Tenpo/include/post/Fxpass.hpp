#ifndef Fxpass_hpp
#define Fxpass_hpp
#include <memory>
#include <string>
#include "cinder/app/App.h"
#include "cinder/gl/gl.h"
#include "cinder/gl/Fbo.h"
#include "cinder/gl/GlslProg.h"
#include "cinder/Log.h"
using namespace ci;
using namespace std;

typedef std::shared_ptr<class FxPass>FxpassRef;

#define STRINGIFY(A) #A

class FxPass {
protected:

	int width, height;

	int sceneIndex = 0;
	int fxIndex = 1;

	int mCurrFbo = 0;
	int mOtherFbo = 1;

	ci::gl::FboRef mFbo;

	bool doubleBuffer = false;
	bool useDepth = false;

	//! This is a texture reference used when slotting the pass in as part of a
	//! larger post-processing chain of effects.
	ci::gl::TextureRef mInput;

	//! post processing shader. By default, a pass-through texture shader.
	ci::gl::GlslProgRef mShader = gl::getStockShader(gl::ShaderDef().texture());
	ci::gl::GlslProgRef mDepthShader;


	//! passthru vertex shader 
	std::string passthru = STRINGIFY(
		uniform mat4 ciModelViewProjection;
		in vec4 ciPosition;
		in vec2 ciTexCoord0;
		out vec2 uv;
		out vec2 vUv;

		void main() {
			uv = ciTexCoord0;
			vUv = uv;
	
			gl_Position = ciModelViewProjection * ciPosition;
		}
	);

	// vertex shader for depth testing.
	std::string depthPass = STRINGIFY(
		uniform mat4 ciModelViewProjection;
	in vec3 ciPosition;
	in vec2 ciTexCoord0;

	out vec2 vUv;
	void main() {
		vUv = ciTexCoord0;
		gl_Position = ciModelViewProjection * vec4(ciPosition, 1.);
	}
	);

	// shader for depth texture checking.
	std::string depthFrag = STRINGIFY(
		uniform sampler2D depthTex;
	in vec2 vUv;
	in vec4 vPos;
	out vec4 glFragColor;

	float LinearizeDepth(float zoverw)
	{
		float n = 1.0; // camera z near  
		float f = 10000.0; // camera z far  
		return (2.0 * n) / (f + n - zoverw * (f - n));
	}


	void main() {
		float depth = texture(depthTex, vUv).r;
		float d = LinearizeDepth(depth);
		glFragColor = normalize(vec4(vec3(d), 1.));
	}
	);
public:
	FxPass() {
		width = ci::app::getWindowWidth();
		height = ci::app::getWindowHeight();
	}


	// sets dimensions for the FxPassRef
	void setDimensions(int width, int height) {
		this->width = width;
		this->height = height;

		// TODO resize FBO after setting new dimensions
	}

	// ================== CREATE FUNCTIONS ====================== //

	// creates a new FxPassRef. Primarily used for when you need to buffer multiple fbos in a ping-pong fashion.
	// Specify a fragment and optionally a vertex shader(a default one is used if nothing is specified).
	// Will return a normal FxPassRef if no shaders are specified.

	static FxpassRef create(std::string fragment = "", std::string vertex = "", bool useDepth = false) {

		if (fragment != "") {

			auto pass = new FxPass();
			pass->loadShader(fragment, vertex);

			// if we need a depth texture, set variable to true, otherwise false.
			if (useDepth) {
				pass->setup(true, true);
			}
			else {
				pass->setup(false, true);
			}

			return FxpassRef(pass);
		}
		else {
			return FxpassRef(new FxPass());
		}
	}

	// creates a new FxPassRef. Primarily used for when you simply need an FBO.
	static FxpassRef create(bool depthScene) {
		FxPass * pass = new FxPass();

		if (depthScene) {
			pass->setup(true);
		}
		else {
			pass->setup();
		}

		return FxpassRef(pass);
	}




	// ================== FUNCTIONS ====================== //


	// sets up a FxPass. 
	// useDepth - pass true if you need a depth texture.
	// doubleBuffer - pass true if this FxPass will ping-pong data back and forth.
	void setup(bool useDepth = false, bool doubleBuffer = false) {
		this->useDepth = useDepth;
		this->doubleBuffer = doubleBuffer;


		// if using depth texture, build shader so we can visualize and check
		if (useDepth) {
			gl::GlslProg::Format fmt;
			fmt.vertex(depthPass);
			fmt.fragment(depthFrag);
			mDepthShader = gl::GlslProg::create(fmt);
		}

		// setup default texture format
		gl::Texture::Format fmt;
		fmt.wrap(GL_CLAMP_TO_EDGE);
		fmt.setMagFilter(GL_LINEAR);
		fmt.setMinFilter(GL_LINEAR);
		fmt.setInternalFormat(GL_RGBA);

		gl::Fbo::Format ffmt;
		ffmt.setColorTextureFormat(fmt);
		ffmt.attachment(GL_COLOR_ATTACHMENT0 + sceneIndex, gl::Texture2d::create(width, height, fmt));

		// if we want to add a second attachment.
		if (doubleBuffer) {
			ffmt.attachment(GL_COLOR_ATTACHMENT0 + fxIndex, gl::Texture2d::create(width, height, fmt));
		}

		// build depth atachment if specified.
		if (useDepth) {
			gl::Texture::Format dfmt;
			dfmt.wrap(GL_CLAMP_TO_EDGE);
			dfmt.setMagFilter(GL_NEAREST);
			dfmt.setMinFilter(GL_NEAREST);
			dfmt.setInternalFormat(GL_DEPTH_COMPONENT32F);

			//dfmt.setCompareMode(GL_COMPARE_REF_TO_TEXTURE);
			//dfmt.setCompareFunc(GL_LEQUAL);

			ffmt.attachment(GL_DEPTH_ATTACHMENT, gl::Texture2d::create(width, height, dfmt));
		}

		// compile fbo
		mFbo = gl::Fbo::create(width, height, ffmt);

		gl::ScopedFramebuffer fb(mFbo); {
			gl::clear();
			
			if (doubleBuffer) {
				const static GLenum buffers[] = {
					GL_COLOR_ATTACHMENT0 + mCurrFbo,
					GL_COLOR_ATTACHMENT0 + mOtherFbo
				};
				gl::drawBuffers(2, buffers);
			}
		}


	}

	// Runs the pass over the input
	void update(float dt = 0.0) {

		if (!mInput) {
			//CI_LOG_I("IEESEOIEFHOSIEFOHS");
			return;
		}

		// if we're not double buffering, run once.
		if (!doubleBuffer) {
			bind();
			gl::clear(ColorA(0, 0, 0, 0));
			gl::setMatricesWindow(mFbo->getSize());
			gl::viewport(mFbo->getSize());

			gl::ScopedGlslProg shd(mShader);
			gl::ScopedTextureBind tex0(mInput, 0);

			mShader->uniform("uTex0", 0);
			gl::drawSolidRect(mFbo->getBounds());
			unbind();
		}	
		
	}

	ci::vec2 getSize() {
		return mFbo->getSize();
	}

	ci::Area getBounds() {
		return mFbo->getBounds();
	}
	ci::gl::GlslProgRef getShader() {
		return mShader;
	}
	ci::gl::FboRef getFbo() {
		return mFbo;
	}

	// returns the input texture reference.
	ci::gl::TextureRef getInput() {
		return mInput;
	}

	// binds the framebuffer.
	// When double buffering, pass in optional index to draw to a particular attachment.
	void bind(int index = 0) {
		if (doubleBuffer) {
			mFbo->bindFramebuffer();
			gl::drawBuffer(GL_COLOR_ATTACHMENT0 + index);
		}
		else {
			mFbo->bindFramebuffer();
		}

	}

	// unbinds the framebuffer
	void unbind() {
		mFbo->unbindFramebuffer();
	}

	// alternate function for setting up input texture
	void setInput(ci::gl::TextureRef mInput) {
		setInputTexture(mInput);
	}

	//! sets the input texture for the 
	void setInputTexture(ci::gl::TextureRef mInput) {
	
		this->mInput = mInput;
	}

	// loads shader for pass. Really looking for just a fragment shader, but if you have a custom 
	// vertex shader, you can pass that in as well.
	void loadShader(std::string fragment, std::string vertex = "") {
		gl::GlslProg::Format fmt;
		if (vertex == "") {
			fmt.vertex(passthru);
		}
		else {
			fmt.vertex(app::loadAsset(vertex));
		}
		fmt.fragment(app::loadAsset(fragment));

		try {
			mShader = gl::GlslProg::create(fmt);
		}
		catch (ci::gl::GlslProgCompileExc &e) {
			CI_LOG_E("FxPass - Error loading shader");
			CI_LOG_E("FxPass | ShaderLoadError :" << e.what());
		}
	}

	// returns the gl::TextureRef at the specified level.(should be 0 or 1)
	gl::TextureRef getTextureAttachment(int index) {
		return mFbo->getTexture2d(GL_COLOR_ATTACHMENT0 + index);
	}

	// Draws the current state of the FBO. 
	void draw() {
		gl::setMatricesWindow(app::getWindowSize());
		gl::viewport(app::getWindowSize());

		gl::ScopedGlslProg shader(mShader);
		gl::TextureRef tex;

		if (doubleBuffer) {
			tex = mFbo->getTexture2d(GL_COLOR_ATTACHMENT0 + mOtherFbo);
		}
		else {
			tex = mFbo->getColorTexture();
		}

		gl::ScopedTextureBind tex0(tex, 0);
		gl::drawSolidRect(app::getWindowBounds());
	}

	// Draws the depth texture on a solid plane. Really just used for checking things.
	void drawDepth() {
		gl::setMatricesWindow(app::getWindowSize());
		gl::viewport(app::getWindowSize());
		gl::enableAdditiveBlending();
		gl::ScopedGlslProg shader(mDepthShader);
		gl::ScopedTextureBind tex0(mFbo->getDepthTexture(), 0);
		gl::drawSolidRect(app::getWindowBounds());

	}

	// Gets the output from the ping-pong process, or the color texture of the main attachment.
	ci::gl::TextureRef getOutput(int index = -1) {
		if (doubleBuffer) {
			return mFbo->getTexture2d(GL_COLOR_ATTACHMENT0 + mOtherFbo);
		}
		else if (index != -1) {
			return mFbo->getTexture2d(GL_COLOR_ATTACHMENT0 + index);
		}
		else {
			return mFbo->getColorTexture();
		}
	}

	// returns the depth texture for the Fbo
	ci::gl::TextureRef getDepthOutput() {
		return mFbo->getDepthTexture();
	}

	// Pass-thru uniform function. Use it the same as the standard gl::GlslProg::uniform function.
	template<typename T>
	void uniform(std::string name, T val) {
		mShader->uniform(name, val);
	}


};
#endif 
