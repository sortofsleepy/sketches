//
//  ParticleSystem.hpp
//  PixelLines
//
//  Created by Joseph Chow on 7/25/17.
//
//

#ifndef ParticleSystem_hpp
#define ParticleSystem_hpp

#include "cinder/gl/VboMesh.h"
#include "cinder/gl/GlslProg.h"
#include "cinder/gl/Batch.h"
#include "cinder/gl/Texture.h"
#include "cinder/app/App.h"
#include "cinder/Rand.h"
#include "cinder/Log.h"
#include "cinder/gl/Fbo.h"
#include "cinder/Rand.h"
#include "ArcCam.h"

class ParticleSystem {

    float pLineLife;
    int currentFbo;
    int targetFbo;
    float time;
    int numParticles;

    ci::Rand mRand;
    ci::gl::FboRef fbos[2];

	ci::gl::TextureRef testTexture;
	ci::gl::TextureRef originalPositions;

	ci::mat4 rot;

	// ============ system props =================== //
	float uNumSeg;
	float uEnd;
	float uLength;

    // ===== save program ======= //
    ci::gl::GlslProgRef mDataShader;
    ci::gl::BatchRef mDataBatch;
    ci::gl::VboMeshRef mDataMesh;

    // ====== render mesh ====== //
    ci::gl::GlslProgRef mRenderShader;
    ci::gl::BatchRef mRenderBatch;
    ci::gl::VboMeshRef mRenderMesh;

	// ====== simulation ======== //
	ci::gl::GlslProgRef mSimShader;

	// ======== audio layer ============ //

    void setupBuffers();
    void setupData(std::vector<ci::vec3> positions);
    void loadSimulationShader();
    void setupRender(int sampleSize);

    void drawBuffers();
public:
	ParticleSystem(int numParticles=64);
	int getNumParticles() {
		return numParticles;
	}
	ci::gl::FboRef getFbo() {
		return fbos[currentFbo];
	}
	ci::gl::TextureRef getTexture(int index);
	void setParticleSystemProperties(float uNumSeg, float uEnd, float uLength);
    void setup(std::vector<ci::vec3> points,int sampleSize);
	void setup();
	void update();
	void draw(ci::gl::BufferTextureRef audio);
};
#endif /* ParticleSystem_hpp */
