#pragma once


#include "cinder/gl/Texture.h"
#include "cinder/app/App.h"
#include <string>
#include "cinder/audio//Context.h"
#include "cinder/audio/audio.h"
#include "cinder/audio/Voice.h"
#include "cinder/audio/Source.h"
#include "cinder/gl/gl.h"
#include "cinder/gl/Vbo.h"
#include <memory>

typedef std::shared_ptr<class AudioPlayer> AudioPlayerRef;

class AudioPlayer {

	AudioPlayer();

	ci::audio::BufferPlayerNodeRef		mBufferPlayerNode;
	ci::audio::MonitorSpectralNodeRef	mMonitorSpectralNode;
	std::vector<float>					mMagSpectrum;

	ci::audio::GainNodeRef				mGain;

	ci::gl::VboRef audioData;
	ci::gl::BufferTextureRef mTex;

	ci::Rectf mBounds;

	// current position in music
	double currentPlayhead;
	bool isPlaying;

	void calculateMagnitude();

	ci::gl::GlslProgRef mShader;
public:

	~AudioPlayer() {}
	static AudioPlayerRef create() {
		return AudioPlayerRef(new AudioPlayer());
	}

	static AudioPlayerRef getInstance() {
		static AudioPlayerRef instance = AudioPlayer::create();
		return instance;
	}

	void loadFile(std::string filepath);
	void play();
	void pause();
	void draw();
	void update();

	bool isPlayingAudio() {
		return isPlaying;
	}

	void rewindToBeginning() {
		currentPlayhead = 0.0;
	}

	ci::gl::BufferTextureRef getTexture() {
		return mTex;
	}

	int getSpectrumSize() {
		return mMagSpectrum.size();
	}
};