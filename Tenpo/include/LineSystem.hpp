//
//  LineSystem.hpp
//  ParticleFeedback
//
//  Created by Joseph Chow on 9/27/17.
//

#ifndef LineSystem_hpp
#define LineSystem_hpp

#include "ArcCam.h"
#include "cinder/gl/VboMesh.h"
#include "cinder/gl/Batch.h"
#include "cinder/gl/GlslProg.h"
#include "cinder/gl/Fbo.h"
#include "cinder/gl/gl.h"
#include <vector>

class LineSystem {

	int numParticles;

	ci::gl::VboMeshRef mRenderMesh;
	ci::gl::BatchRef mRenderBatch;
	ci::gl::GlslProgRef mRenderShader;

	ci::gl::GlslProgRef mCopyShader;

	std::vector<ci::gl::FboRef> linemaps;

	void setupRender();


	float numSeg;
	float uEnd;
	float uLength;

	/*
	uniform float uEnd = 100.0;
	uniform float uNumSeg = 40.0;
	uniform float uLength = 30.0;
	*/
public:
	LineSystem();
	void draw(ArcCamRef mCam, ci::gl::TextureRef curr, ci::gl::TextureRef next, ci::gl::TextureRef extra);
	void setup(int numParticles = 64, float uEnd = 100.0, float numSeg = 40.0, float uLength = 30.0);
	void reset(ci::gl::FboRef fbo);
	void save(ci::gl::FboRef fbo);

	void render(ArcCamRef mCam, ci::gl::TextureRef extra);
};

#endif /* LineSystem_hpp */
