#pragma once

#include "cinder/gl/Batch.h"
#include "cinder/gl/VboMesh.h"
#include "cinder/gl/GlslProg.h"
#include "cinder/gl/gl.h"
#include "cinder/gl/Vbo.h"
#include "cinder/app/App.h"
#include "cinder/Rand.h"
class CubedCube {

	ci::gl::VboMeshRef mMesh;
	ci::gl::BatchRef mBatch;
	ci::gl::GlslProgRef mShader;

	ci::gl::VboRef mCubePositions,mCubeRotation,mCubeColors;
	int numInstances;
	int resolution;
	float time;
	ci::mat4 rot;
	int sampleSize;

	std::vector<ci::vec3> cubePositions;

public:
	CubedCube();
	void setup(int sampleSize,int size=100, int resolution=10);
	void setSampleSize(int sampleSize) {
		this->sampleSize = sampleSize;
	}
	std::vector<ci::vec3> getPositions();
	void draw(ci::gl::BufferTextureRef audio);
};