#include "cinder/app/App.h"
#include "cinder/app/RendererGl.h"
#include "cinder/gl/gl.h"
#include "ParticleSystem.h"
#include "mocha/renderer/DeferredRenderer.h"
#include "cinder/TriMesh.h"
#include "mocha/loaders/ObjLoader.h"
#include "mocha/core/Camera.h"
#include "DeferredParticleSystem.h"
#include "LightParticles.h"
#include "mocha/post/PostProcessor.h"
#include "MotionBlur.h"

using namespace ci;
using namespace ci::app;
using namespace std;

class ComputeSystemApp : public App {
  public:
	void setup() override;
	void update() override;
	void draw() override;
	void buildScene();
	void buildWells();

	float angle = 0.0;

	mocha::EasyCameraRef mCam;
	mocha::post::PostProcessorRef processor;
	
	mocha::post::EffectRef hBlur,vBlur,fxaa;
	mocha::DeferredRendererRef renderer;
	
	MotionBlurRef motionBlur;

	//! Well data 
	ci::gl::SsboRef mWellData;


	LightParticle ls;
	DeferredParticleSystem ps;


	ci::gl::BatchRef mRenderBatch;
	ci::gl::FboRef mBlurSceneFbo;
	ci::gl::FboRef mCompositeSceneFbo;
	
};

void ComputeSystemApp::setup()
{

	buildScene();
	
	// init processor, renderer and cammera. 
	processor = mocha::post::PostProcessor::create();
	renderer = mocha::DeferredRenderer::create();
	mCam = mocha::EasyCamera::create();


	mCam->setZoom(20);

	motionBlur = MotionBlur::create(20);

	// setup post processing.
	hBlur = mocha::post::PostEffect::create(app::loadAsset("shaders/post/blur.glsl"));
	vBlur = mocha::post::PostEffect::create(app::loadAsset("shaders/post/blur.glsl"));
	fxaa = mocha::post::PostEffect::create(app::loadAsset("shaders/post/fxaa.frag"));

	processor->addStage(fxaa);
	processor->addStage(hBlur);
	processor->addStage(vBlur);

	processor->compile(renderer->getOutput());

	// build wells 
	buildWells();

	// set the renderer to use the main camera. 
	renderer->setCamera(mCam);

	// setup the deferred particle system.
	ps = DeferredParticleSystem(30000);
	ps.setup(mWellData);

	// setup the lighting system
	ls.setup();
	ls.addToRenderer(renderer);

	// turn off ability to see light objects
	renderer->toggleLightObjects();

	// add particle system to scene.
	renderer->addToScene(ps);


}

void ComputeSystemApp::update()
{
	//mSystem->update();

	ps.update();
	ls.update();

	renderer->update();



	motionBlur->update([=]() {

		gl::setMatricesWindow(app::getWindowSize());
		gl::viewport(app::getWindowSize());

		gl::pushMatrices();


		renderer->draw();

		gl::popMatrices();
	});


	fxaa->update([](gl::GlslProgRef shader) {
		shader->uniform("uRcpBufferSize", vec2(1) / vec2(app::getWindowSize()));
	});

	hBlur->update([](gl::GlslProgRef shader) {
		shader->uniform("sample_offset", vec2(1.0f / app::getWindowWidth(), 0.0f));
		shader->uniform("attenuation", 2.5f);
	});

	vBlur->update([](gl::GlslProgRef shader) {
		shader->uniform("sample_offset", vec2(0.0f, 1.0f / app::getWindowHeight()));
		shader->uniform("attenuation", 2.5f);
	});
	
}


void ComputeSystemApp::buildScene() {

	gl::Texture::Format tfmt;
	gl::Fbo::Format fmt;

	tfmt.setInternalFormat(GL_RGBA32F);
	fmt.setColorTextureFormat(tfmt);

	
	mRenderBatch = mocha::utils::fullScreenTriangle(app::loadAsset("shaders/compositeShader.glsl"));
	
	mBlurSceneFbo = gl::Fbo::create(app::getWindowWidth(),app::getWindowHeight(),fmt);
	mCompositeSceneFbo = gl::Fbo::create(app::getWindowWidth(), app::getWindowHeight(), fmt);

}

void ComputeSystemApp::buildWells() {
	mocha::loaders::ObjLoader loader(app::loadAsset("text.obj"));
	auto vertices = loader.getVertices();

	mWellData = gl::Ssbo::create(vertices.size() * sizeof(vec3), vertices.data(), GL_STATIC_DRAW);

}

void ComputeSystemApp::draw()
{
	gl::clear( Color( 0, 0, 0 ) ); 
	gl::viewport(app::getWindowSize());
	gl::setMatricesWindow(app::getWindowSize());

	gl::enableAdditiveBlending();
	
	mBlurSceneFbo->bindFramebuffer();
	gl::rotate(angle);
	processor->draw();
	motionBlur->draw();

	mBlurSceneFbo->unbindFramebuffer();


	mCompositeSceneFbo->bindFramebuffer();
	renderer->draw();
	mCompositeSceneFbo->unbindFramebuffer();


	

	gl::ScopedTextureBind tex0(mBlurSceneFbo->getColorTexture(), 0);
	gl::ScopedTextureBind tex1(mCompositeSceneFbo->getColorTexture(), 1);

	mRenderBatch->getGlslProg()->uniform("resolution", vec2(app::getWindowWidth(),app::getWindowHeight()));
	mRenderBatch->getGlslProg()->uniform("blurTexture", 0);
	mRenderBatch->getGlslProg()->uniform("normalTexture", 1);

	mRenderBatch->draw();

	angle += 0.1;

}

CINDER_APP( ComputeSystemApp, RendererGl, [](App::Settings *settings) {
	settings->setWindowSize(1024, 768);
})
