#version 430 core
precision highp float;
precision highp sampler2DShadow;
		
uniform float uEmissive;
in vec4 color;
in vec3 normal;
in vec4 position;
			
			
layout(location = 0) out vec4 oAlbedo;
layout(location = 1) out vec4 oNormalEmissive;
layout(location = 2) out vec4 oPosition;

// user output
layout(location = 3) out vec4 glFragColor;

void main(){
	oAlbedo = color;
	oNormalEmissive = vec4(normalize(normal), uEmissive);
	oPosition = position;			
}