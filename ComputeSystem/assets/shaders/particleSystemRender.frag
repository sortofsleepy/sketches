#version 430 core
precision highp float;
precision highp sampler2DShadow;
uniform float uEmissive;
uniform float time;
uniform vec2 resolution;

in vec4 color;
in vec3 normal;
in vec4 position;

layout(location = 0) out vec4 oAlbedo;
layout(location = 1) out vec4 oNormalEmissive;
layout(location = 2) out vec4 oPosition;

// user output
layout(location = 3) out vec4 glFragColor;

void main(){
	vec2 uv = gl_FragCoord.xy / resolution.xy;
	vec3 _color = 0.5 + 0.5*cos(time+uv.xyx+vec3(position.xyz));

	vec4 col = vec4(_color,1.);
	
	oAlbedo = color * col;
	oNormalEmissive = vec4(normalize(normal), uEmissive);
	oPosition = position;
}