#version 450 


uniform sampler2D blurTexture;
uniform sampler2D normalTexture;
uniform vec2 resolution;

in vec2 vUv;

out vec4 glFragColor;
void main(){
	vec2 uv = gl_FragCoord.xy / resolution.xy;

	vec2 reverseYUV = vec2(uv.x, 1.0 - uv.y);
	vec2 reverseXUV = vec2(1.0-uv.x, uv.y);

	vec4 blur = texture(blurTexture,reverseYUV);
	vec4 blur2 = texture(blurTexture,uv);
	vec4 blur3 = texture(blurTexture,reverseXUV);

	vec4 normal = texture(normalTexture,uv);
	vec4 normal2 = texture(normalTexture,reverseYUV);
	vec4 normal3 = texture(normalTexture,reverseXUV);

	vec4 norm = normal + normal2 + normal3;
	vec4 blurCombine = blur + blur2 + blur3;

	

	glFragColor = mix(norm, blurCombine,0.4) + mix(norm, blurCombine,0.6);

}