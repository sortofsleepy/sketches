#version 430 core 

uniform sampler2D uTex0;
uniform vec2 vUv;
uniform vec2		sample_offset;
uniform float		attenuation;
uniform vec2 resolution;
out vec4 glFragColor;

void main(){

	vec2 uv = gl_FragCoord.xy / resolution;
	
	vec3 sum = vec3( 0.0, 0.0, 0.0 );	
	sum += texture( uTex0, uv + -10.0 * sample_offset ).rgb * 0.009167927656011385;
	sum += texture( uTex0, uv +  -9.0 * sample_offset ).rgb * 0.014053461291849008;
	sum += texture( uTex0, uv +  -8.0 * sample_offset ).rgb * 0.020595286319257878;
	sum += texture( uTex0, uv +  -7.0 * sample_offset ).rgb * 0.028855245532226279;
	sum += texture( uTex0, uv +  -6.0 * sample_offset ).rgb * 0.038650411513543079;
	sum += texture( uTex0, uv +  -5.0 * sample_offset ).rgb * 0.049494378859311142;
	sum += texture( uTex0, uv +  -4.0 * sample_offset ).rgb * 0.060594058578763078;
	sum += texture( uTex0, uv +  -3.0 * sample_offset ).rgb * 0.070921288047096992;
	sum += texture( uTex0, uv +  -2.0 * sample_offset ).rgb * 0.079358891804948081;
	sum += texture( uTex0, uv +  -1.0 * sample_offset ).rgb * 0.084895951965930902;
	sum += texture( uTex0, uv +   0.0 * sample_offset ).rgb * 0.086826196862124602;
	sum += texture( uTex0, uv +  +1.0 * sample_offset ).rgb * 0.084895951965930902;
	sum += texture( uTex0, uv +  +2.0 * sample_offset ).rgb * 0.079358891804948081;
	sum += texture( uTex0, uv +  +3.0 * sample_offset ).rgb * 0.070921288047096992;
	sum += texture( uTex0, uv +  +4.0 * sample_offset ).rgb * 0.060594058578763078;
	sum += texture( uTex0, uv +  +5.0 * sample_offset ).rgb * 0.049494378859311142;
	sum += texture( uTex0, uv +  +6.0 * sample_offset ).rgb * 0.038650411513543079;
	sum += texture( uTex0, uv +  +7.0 * sample_offset ).rgb * 0.028855245532226279;
	sum += texture( uTex0, uv +  +8.0 * sample_offset ).rgb * 0.020595286319257878;
	sum += texture( uTex0, uv +  +9.0 * sample_offset ).rgb * 0.014053461291849008;
	sum += texture( uTex0, uv + +10.0 * sample_offset ).rgb * 0.009167927656011385;

	
	glFragColor.rgb = attenuation * sum;
	glFragColor.a = 1.0;

}