
#version 430 core



uniform mat4 	ciModelView;
uniform mat3 	ciNormalMatrix;
uniform mat4 	ciModelViewProjection;
uniform float time;


in vec4 ciColor;
in vec3 ciNormal;
in vec4 ciPosition;

// instanced attributes 
in vec3 iPosition;
in int particleId;
in float rotationOffset;
in float particleScale;
in vec4 iColor;


#include "rotations.glsl"

struct Particle
{
	// current position 
	vec4 position;

	vec4 origin;

	vec4 meta;
};


layout( std140, binding = 0 ) buffer Part
{
    Particle particles[];
};



out vec4 color;
out vec3 normal;
out vec4 position;

void main(){
	Particle p = particles[particleId];

	vec4 pos = ciPosition;
						
	pos.xyz = rotateX(pos.xyz,iPosition.x * rotationOffset * time * 6.);
	pos.xyz = rotateY(pos.xyz,iPosition.y * rotationOffset * time * 6.);
	pos.xyz = rotateZ(pos.xyz,iPosition.z * rotationOffset * time * 6.);


	vec3 instancedPosition = iPosition;

	pos.xyz *= vec3(particleScale) + sin(time * particleScale);
	



	pos.xyz += (instancedPosition + p.position.xyz);

	vec3 particlePos = p.position.xyz;



	color = normalize(iColor);
	
	// meta.y represents life.
	if(p.meta.y != 0.0){
		color.a = p.meta.y;
	}
	
	position = ciModelView * pos;
	vec3 n = ciNormal;
	normal = ciNormalMatrix * n;

	gl_Position = ciModelViewProjection * pos;
}