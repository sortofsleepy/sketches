#version 430 core

uniform mat4 ciModelViewProjection;
uniform mat4 	ciModelView;
uniform mat3 	ciNormalMatrix;

uniform float time;
in vec4 		ciColor;
in vec3 		ciNormal;
in vec4 		ciPosition;

in int particleId;
in float particleScale;
in float rotationOffset;

out float pLife;

out vec4 		color;
out vec3 		normal;
out vec4 		position;

#include "rotations.glsl"

struct Particle
{
	// current position 
	vec4 position;

	vec4 origin;

	vec4 meta;
};


layout( std140, binding = 0 ) buffer Part
{
    Particle particles[];
};

void main(){
	vec4 pos = ciPosition;

	pos.xyz *= vec3(particleScale * 2.0);

	Particle p = particles[particleId];
	
	vec3 particlePos = p.position.xyz;


	// rotate
	//pos.xyz = rotateX(pos.xyz,particlePos.x * rotationOffset * 0.5);
	//pos.xyz = rotateY(pos.xyz,particlePos.y * rotationOffset * 0.5);
	//pos.xyz = rotateZ(pos.xyz,particlePos.z * rotationOffset * 0.5);


	pos += p.position;
	pLife = p.meta.y;

	color = ciColor;
	position = ciModelViewProjection * pos;
	vec3 n = ciNormal;	
	normal = ciNormalMatrix * n;

}