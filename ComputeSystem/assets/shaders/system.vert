#version 430 core

uniform mat4 ciModelViewProjection;
uniform float time;
in vec3 ciPosition;

in int particleId;
in float particleScale;
in float rotationOffset;

out float pLife;
#include "rotations.glsl"

struct Particle
{
	// current position 
	vec4 position;

	vec4 origin;

	vec4 meta;
};


layout( std140, binding = 0 ) buffer Part
{
    Particle particles[];
};

void main(){
	vec3 pos = ciPosition;

	pos.xyz *= vec3(particleScale * 2.0);

	Particle p = particles[particleId];
	
	vec3 particlePos = p.position.xyz;


	// rotate
	pos = rotateX(pos,particlePos.x * rotationOffset * 0.5);
	pos = rotateY(pos,particlePos.y * rotationOffset * 0.5);
	pos = rotateZ(pos,particlePos.z * rotationOffset * 0.5);


	pos += p.position.xyz;
	pLife = p.meta.y;
	gl_Position = ciModelViewProjection * vec4(pos,1.);
}