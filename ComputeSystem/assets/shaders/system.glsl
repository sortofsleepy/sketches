#version 430 core

struct Particle
{
	// current position 
	vec4 position;

	vec4 origin;

	vec4 meta;
};


layout( std140, binding = 0 ) buffer Part
{
    Particle particles[];
};

layout (std140,binding=1) buffer GravWells
{
	vec3 wellPosition[];
};

//layout( local_size_variable ) in;
layout( local_size_x = 128, local_size_y = 1, local_size_z = 1 ) in;


void main(){

	uint gid = gl_GlobalInvocationID.x;	// The .y and .z are both 1 in this case.

	Particle p = particles[gid];

	
	p.position.x += 0.1;
	p.position.y = sin(p.meta.x) * p.position.w;

	p.meta.x += 0.04;

	// decrease life. 
	p.meta.y -= 0.001;


	if(p.meta.y < 0.0){
		p.position.x = p.origin.x;
		p.meta.y = 1.0;
	}

	particles[gid] = p;

}