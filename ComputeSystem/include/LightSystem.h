
#pragma once


#include <vector>
#include "cinder/Color.h"
#include "mocha/renderer/Light.h"
#include "mocha/renderer/DeferredRenderer.h"
#include "cinder/Rand.h"


typedef struct {
	mocha::LightRef light;
	ci::vec3 position;
	mocha::Object3DRef geo;

	float phi;
	float theta;
	float phiSpeed;
	float thetaSpeed;

	ci::mat4 rotation;
	float rotationSpeed;
}LightParticles;

using namespace ci;
using namespace std;

class LightSystem {

	std::vector<LightParticles> mLights;

	// bloom passes, horizontal and vertical.
	ci::gl::FboRef mHorizontal, mVertical;
	float radius;
public:
	LightSystem(float radius = 10.0f, int numLights = 20) {

		this->radius = radius;

		// limit the number of lights.
		if (numLights < 30) {
			for (int i = 0; i < numLights; ++i) {

				auto l = LightParticles();
				auto light = mocha::Light::create();
				light->colorDiffuse(ColorAf(0.95f, 1.0f, 0.92f, 1.0f))
					.intensity(0.2f).position(vec3(0.0f, 0.0f, 0.0f))
					.radius(0.1f).volume(3.0f);
				l.light = light;

				// build geometry for rendering
				l.geo = mocha::Object3D::create(
					gl::VboMesh::create(geom::Icosahedron())
				);

				l.rotationSpeed = randFloat();

				l.phi = 0.1f;
				l.theta = 0.1f;
				l.phiSpeed = randFloat();
				l.thetaSpeed = randFloat();

				mLights.push_back(l);

			}
		}

	}

	void update() {
		for (int i = 0; i < mLights.size(); ++i) {

			float x = cos(mLights[i].theta) * sin(mLights[i].phi) * radius;
			float y = sin(mLights[i].theta) * sin(mLights[i].phi) * radius;
			float z = cos(mLights[i].phi) * radius;

			mLights[i].position.x = x;
			mLights[i].position.y = y;
			mLights[i].position.z = z;


			mLights[i].geo->getModelMatrix() *= glm::rotate(toRadians(mLights[i].rotationSpeed), normalize(vec3(1)));

			mLights[i].light->setPosition(mLights[i].position);
			mLights[i].geo->setPosition(mLights[i].position);

			mLights[i].theta += mLights[i].thetaSpeed * 0.05;
			mLights[i].phi += mLights[i].phiSpeed * 0.05;
		}



	}

	//! Add lights to renderer
	void addToRenderer(mocha::DeferredRendererRef renderer) {

		// custom drawing function for rendering the lighting system.
		std::function<void(mocha::Object3D *)> func = [](mocha::Object3D * object) {

			gl::pushMatrices();
			gl::translate(object->getPosition());

			gl::scale(vec3(0.05));
			gl::multModelMatrix(object->getModelMatrix());
			object->getBatch()->draw();

			gl::popMatrices();
		};


		for (int i = 0; i < mLights.size(); ++i) {

			// set custom rendering function
			mLights[i].geo->setRenderFunction(func);

			// add light to the scene.
			renderer->addLight(mLights[i].light);

			// add light geometry to the scene.
			//renderer->addToScene(mLights[i].geo);
		}


	}


};