#pragma once

#include <vector>
#include <memory>
#include <future>
#include "cinder/Vector.h"
#include "cinder/gl/Vbo.h"
#include "cinder/gl/Fbo.h"
#include "cinder/Rand.h"
#include "cinder/app/App.h"
#include "mocha/renderer/Object3D.h"

using namespace std;
using namespace ci;


typedef std::shared_ptr<class ParticleSystem> ParticleSystemRef;

typedef struct {
	ci::vec4 position;
	ci::vec4 origin;
	ci::vec4 meta;
}Particle;

typedef struct {
	ci::vec4 position;
	ci::vec4 mass;
}GravWell;

class ParticleSystem : public mocha::Object3D {
public:
	ParticleSystem() = default;
	~ParticleSystem() = default;


	static ParticleSystemRef create() {
		return ParticleSystemRef(new ParticleSystem);
	}

	//! Setup particle system.
	void setup(int num = 100, std::vector<ci::vec3> positions=std::vector<ci::vec3>()) {
		numParticles = num;

		// build initial particle information
		vector<Particle> particleData;


		for (int i = 0; i < num; ++i) {
			Particle p;

			auto pos = vec4(vec3(-num , randFloat(), 0.0), randFloat() + randInt(1,10));

			// w holds ampliltude
			p.position = vec4(pos.x + i,pos.y +1.0, 0.0,pos.w);
			p.origin = vec4(pos.x + i, pos.y + 1.0, 0.0, pos.w);

			// y will be particle life
			p.meta = vec4(randFloat(), randFloat() + 1.0, 1.0, 1.0);
	

			particleData.push_back(p);
		}

		// load shaders
		loadShader();

		// build render-able geometry
		buildGeo();


		// load particles into ssbo
		mParticleData = gl::Ssbo::create(particleData.size() * sizeof(Particle), particleData.data(), GL_STATIC_DRAW);


	
	}

	void update() {
		gl::ScopedGlslProg shader(mUpdateShader);
		mUpdateShader->uniform("time", (float)app::getElapsedSeconds());

		mParticleData->bindBase(0);
		
		
		gl::dispatchCompute(numParticles, 1, 1);
		gl::memoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);

	}

	void loadShader() {

		// load particle system update shader. 
		gl::GlslProg::Format ufmt,rfmt;

		// generate update shader.
		ufmt.compute(app::loadAsset("shaders/system.glsl"));
		mUpdateShader = gl::GlslProg::create(ufmt);

		rfmt.vertex(app::loadAsset("shaders/systemGBuffer.vert"));
		rfmt.fragment(app::loadAsset("shaders/systemGBuffer.frag"));
            
		mRenderShader = gl::GlslProg::create(rfmt);

		setShader(mRenderShader);
	}

	void draw(mocha::Object3D * obj){
	
		if (mBatch && mParticleData) {
			gl::enableAlphaBlending();
			gl::disableDepthRead();
			gl::disableDepthWrite();

		
			mParticleData->bindBase(0);
			mBatch->getGlslProg()->uniform("time", (float)app::getElapsedSeconds());
			mBatch->getGlslProg()->uniform("uEmissive", uEmissive);
			gl::translate(100, 0);
			mBatch->drawInstanced(numParticles);

		}
	}

	void buildGeo() {

		gl::VboMesh::Layout layout;
		layout.attrib(geom::POSITION, 3);

		auto geo = geom::Icosahedron();

		gl::VboMeshRef mesh = gl::VboMesh::create(geo);
	
		// generate ids so we can look up particle information easily when rendering.
		std::vector<GLuint> ids(numParticles);
		GLuint currId = 0;
		ids.push_back(0);
		std::generate(ids.begin(), ids.end(), [&currId]() -> GLuint { return currId++; });

		mIdsVbo = gl::Vbo::create<GLuint>(GL_ARRAY_BUFFER, ids, GL_STATIC_DRAW);

		geom::BufferLayout instanceDataLayout;
		instanceDataLayout.append(geom::Attrib::CUSTOM_0, 1, 0, 0, 1 /* per instance */);
		mesh->appendVbo(instanceDataLayout, mIdsVbo);


		// generate random rotation offsets
		std::vector<float> rotationOffsets(numParticles);
		std::generate(rotationOffsets.begin(), rotationOffsets.end(), []() -> GLuint {
			return randFloat() + 1.0;
		});

		gl::VboRef rotations = gl::Vbo::create<float>(GL_ARRAY_BUFFER, rotationOffsets, GL_STATIC_DRAW);
		geom::BufferLayout rotationLayout;
		rotationLayout.append(geom::Attrib::CUSTOM_1, 1, 0, 0, 1 /* per instance */);
		mesh->appendVbo(rotationLayout, rotations);

		// generate enough spots to hold some positional data which will get manipulated by the compute shader
		std::vector<ci::vec3> positions(numParticles);
		std::generate(positions.begin(), positions.end(), []() -> vec3 {
			return randVec3();
		});

		// generate random scaling 
		std::vector<float> scale(numParticles);
		std::generate(scale.begin(), scale.end(), []() -> float {
			return randFloat();
		});
		gl::VboRef scales = gl::Vbo::create<float>(GL_ARRAY_BUFFER, scale, GL_STATIC_DRAW);
		geom::BufferLayout scaleLayout;
		scaleLayout.append(geom::Attrib::CUSTOM_2, 1, 0, 0, 1);
		mesh->appendVbo(scaleLayout, scales);

		// build the batch
		 auto rshader = mRenderShader;

		//auto rshader = mShader;
		mBatch = gl::Batch::create(mesh, rshader, {
			{geom::CUSTOM_0, "iPosition"},
			{geom::CUSTOM_1, "particleId"},
			{geom::CUSTOM_2, "rotationOffset"},
			{geom::CUSTOM_3, "particleScale"}
		});

		setMesh(mBatch);
		setRenderFunction(std::bind(&ParticleSystem::draw, this, std::placeholders::_1));
	}

protected:
	ci::gl::VboRef mIdsVbo;
	ci::gl::SsboRef mParticleData;

	// renderable geometry 
	ci::gl::BatchRef mGeo;

	int numParticles;

	// update shader for particles. 
	ci::gl::GlslProgRef mUpdateShader,mRenderShader;
};