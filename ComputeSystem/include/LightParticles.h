#pragma once
#include <vector>
#include "cinder/Color.h"
#include "mocha/renderer/Light.h"
#include "mocha/renderer/DeferredRenderer.h"
#include "cinder/Rand.h"

using namespace ci;
using namespace std;

#define DEBUG

typedef struct {
	mocha::LightRef light;
	ci::vec3 position;
	mocha::Object3DRef geo;

#ifdef DEBUG
	ci::gl::BatchRef batch;
	
#endif
	float origin;
	float amplitude;
	float speed;
}LParticle;

class LightParticle {
public:
	LightParticle() = default;


	//! Setup particles - max 30
	void setup(int num=20) {

		setupLightColors();

		if (num > 30) {
			return;
		}

		for (int i = 0; i < num; ++i) {

			auto color = getRandomColor();
			
			auto l = LParticle();
			auto light = mocha::Light::create();
			light->colorDiffuse(color + vec4(1.0,1.0,0.0,1.0) + vec4(1.0,0.0,0.0,1.0) + ciColors[0])
				.intensity(0.4f).position(vec3(0.0f, 0.0f, 0.0f))
				.radius(0.5f).volume(5.0f);
			l.light = light;

			// build geometry for rendering
			l.geo = mocha::Object3D::create(
				gl::VboMesh::create(geom::Icosahedron())
			);

#ifdef DEBUG
			l.batch = gl::Batch::create(
				gl::VboMesh::create(geom::Icosahedron()),
				gl::getStockShader(gl::ShaderDef().color())
			);
#endif

			l.amplitude = randFloat() + randInt(0, 10);

			// approximate good for window size and default camera zoom
			// TODO come up with more appropriate sizing.
			l.position.x = randInt(-20, 0);
			l.position.y = randFloat();
			l.origin = -30;
			l.speed = randFloat() * 0.5;
		

			mLights.push_back(l);
		}
	}
	void update() {
		
		for (int i = 0; i < mLights.size(); ++i) {
		

			mLights[i].position.x += mLights[i].speed;

			if (mLights[i].position.x > mLights[i].origin * -1) {
			
				mLights[i].position.x = mLights[i].origin;
			}

			mLights[i].position.y = sin(app::getElapsedSeconds()) * mLights[i].amplitude;
			
			//mLights[i].geo->getModelMatrix() *= glm::rotate(toRadians(mLights[i].rotationSpeed), normalize(vec3(1)));

			mLights[i].light->setPosition(mLights[i].position);
			mLights[i].geo->setPosition(mLights[i].position);

		
		}

	}

	void debug() {
#ifdef DEBUG
		for (int i = 0; i < mLights.size(); ++i) {

			auto l = mLights[i];

			gl::pushMatrices();
			gl::translate(l.position);


			gl::color(Color(1, 1, 0));
			l.batch->draw();

			gl::popMatrices();
	}
#endif
	}

	ci::ColorA getRandomColor() {

		int rand = randInt(0, ciColors.size());


		return ciColors[rand];
	}

	void setupLightColors() {

		std::vector<glm::vec4> colors = {
			ci::vec4(255,200,184,100),
			ci::vec4(255,215,171,68),
			ci::vec4(155,250,223,98)
		};


		for (int i = 0; i < colors.size(); ++i) {
			int rand = randInt(0, colors.size() - 1);

			auto col = colors[rand];
			ciColors.push_back(ci::ColorA(1.0 / col.r, 1.0 / col.g, 1.0 / col.b, 1.0 / col.a));
		}
	}


	//! Add lights to renderer
	void addToRenderer(mocha::DeferredRendererRef renderer) {

		// custom drawing function for rendering the lighting system.
		std::function<void(mocha::Object3D *)> func = [](mocha::Object3D * object) {

			gl::pushMatrices();
			gl::translate(object->getPosition());

			gl::scale(vec3(0.05));
			gl::multModelMatrix(object->getModelMatrix());

			object->getBatch()->draw();

			gl::popMatrices();
		};


		for (int i = 0; i < mLights.size(); ++i) {

			// set custom rendering function
			mLights[i].geo->setRenderFunction(func);

			// add light to the scene.
			renderer->addLight(mLights[i].light);
		}


	}
protected:
	std::vector<LParticle> mLights;

	//! colors for lights. 
	std::vector<ci::ColorA> ciColors;
};