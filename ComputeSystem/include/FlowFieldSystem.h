#pragma once

#include "cinder/gl/Ssbo.h"
#include "cinder/DataSource.h"
#include "cinder/Vector.h"
#include <vector>


/*
	Serves as modifers that affect the main particle system in some fashion.
*/
class FlowFieldSystem {
public:
	FlowFieldSystem() = default;

	void loadGeometry(ci::DataSourceRef path) {

	}

private:

	ci::gl::SsboRef mDataBuffer;

	// positions in the system
	std::vector<ci::vec3> positions;
};