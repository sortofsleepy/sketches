#pragma once


#include <vector>
#include <memory>
#include <future>
#include "cinder/Vector.h"
#include "cinder/gl/Vbo.h"
#include "cinder/gl/Fbo.h"
#include "cinder/Rand.h"
#include "cinder/app/App.h"
#include "mocha/renderer/Object3D.h"


using namespace ci; 
using namespace std;



class DeferredParticleSystem : public mocha::Object3D {
	typedef struct {
		ci::vec4 position;
		ci::vec4 origin;
		ci::vec4 meta;
	}Particle;

	typedef struct {
		ci::vec4 position;
		ci::vec4 meta;
	}Attractor;

public:
	DeferredParticleSystem() = default;
	DeferredParticleSystem(int num) { this->num = num; }

	void setup(ci::gl::SsboRef targets) {

		mTargetData = targets;

		// generate data for the system
		generateData();

		

		// load compute system
		loadCompute(targets->getSize());

		auto ico = geom::Icosahedron();

		gl::VboMeshRef mMesh = gl::VboMesh::create(ico);
		
		// generate enough spots to hold some positional data which will get manipulated by the compute shader
		std::vector<ci::vec3> positions(num);
		geom::BufferLayout positionLayout;
		positionLayout.append(geom::CUSTOM_0, 3, 0, 0, 1);
		std::generate(positions.begin(), positions.end(), []() -> vec3 {
			return randVec3();
		});

		gl::VboRef positionData = gl::Vbo::create(GL_ARRAY_BUFFER, sizeof(ci::vec3) * positions.size(), positions.data());
		mMesh->appendVbo(positionLayout, positionData);



		// generate ids so we can look up particle information easily when rendering.
		std::vector<GLuint> ids(num);
		GLuint currId = 0;
		ids.push_back(0);
		std::generate(ids.begin(), ids.end(), [&currId]() -> GLuint { return currId++; });

		mIdsVbo = gl::Vbo::create<GLuint>(GL_ARRAY_BUFFER, ids, GL_STATIC_DRAW);

		geom::BufferLayout instanceDataLayout;
		instanceDataLayout.append(geom::Attrib::CUSTOM_1, 1, 0, 0, 1 /* per instance */);
		mMesh->appendVbo(instanceDataLayout, mIdsVbo);



		// generate random rotation offsets
		std::vector<float> rotationOffsets(num);
		std::generate(rotationOffsets.begin(), rotationOffsets.end(), []() -> GLuint {
			return randFloat() + 1.0;
		});

		gl::VboRef rotations = gl::Vbo::create<float>(GL_ARRAY_BUFFER, rotationOffsets, GL_STATIC_DRAW);
		geom::BufferLayout rotationLayout;
		rotationLayout.append(geom::Attrib::CUSTOM_2, 1, 0, 0, 1 /* per instance */);
		mMesh->appendVbo(rotationLayout, rotations);



		// generate random scaling 
		std::vector<float> scale(num);
		std::generate(scale.begin(), scale.end(), []() -> float {
			return randFloat();
		});

		gl::VboRef scales = gl::Vbo::create<float>(GL_ARRAY_BUFFER, scale, GL_STATIC_DRAW);
		geom::BufferLayout scaleLayout;
		scaleLayout.append(geom::Attrib::CUSTOM_3, 1, 0, 0, 1);
		mMesh->appendVbo(scaleLayout, scales);


		// add color data 
		geom::BufferLayout colorLayout;
		colorLayout.append(geom::CUSTOM_4, 4, 0, 0, 1);
		mMesh->appendVbo(colorLayout, mColorData);


	
		gl::GlslProg::Format fmt;
		fmt.vertex(app::loadAsset("shaders/particleSystemRender.vert"));
		fmt.fragment(app::loadAsset("shaders/particleSystemRender.frag"));
		gl::GlslProgRef mShader = gl::GlslProg::create(fmt);
		
		mBatch = gl::Batch::create(mMesh, mShader, {
			{geom::CUSTOM_0, "iPosition"},
			{geom::CUSTOM_1, "particleId"},
			{geom::CUSTOM_2, "rotationOffset"},
			{geom::CUSTOM_3, "particleScale"},
			{geom::CUSTOM_4, "iColor"}
		});

		// bind rendering to this classe's function instead of base class.
		setRenderFunction(std::bind(&DeferredParticleSystem::draw, this, std::placeholders::_1));
	}

	void generateData() {


		// build initial particle information
		vector<Particle> particleData;


		for (int i = 0; i < num; ++i) {
			Particle p;

			auto pos = vec4(vec3(-num, randFloat(), randFloat(-1500,1500)), randFloat() + randInt(1, 10));

			// w holds ampliltude
			p.position = vec4(pos.x + i, pos.y + 1.0, 0.0, pos.w);
			p.origin = vec4(pos.x + i, pos.y + 1.0, 0.0, pos.w);

			// y will be particle life
			// z will be mass
			p.meta = vec4(randFloat(), randFloat() + 1.0, randFloat(), 1.0);


			particleData.push_back(p);
		}


		// load particles into ssbo
		mParticleData = gl::Ssbo::create(particleData.size() * sizeof(Particle), particleData.data(), GL_STATIC_DRAW);

		// Generate color data.
		generateColors();

	}

	void loadCompute(int numTargets) {

		buildComputeShader(numTargets);

		// load particle system update shader. 
		gl::GlslProg::Format ufmt, rfmt;

		// generate update shader.
		//ufmt.compute(app::loadAsset("shaders/system.glsl"));
		ufmt.compute(compute);
		ufmt.version(430);
		try {
			mUpdateShader = gl::GlslProg::create(ufmt);
		}
		catch (ci::gl::GlslProgLinkExc &e) {
			CI_LOG_E(e.what());
		}

	}


	//! Generate colors for each instance.
	void generateColors() {


		std::vector<glm::vec4> colors = {
			ci::vec4(184,255,235,100),
			ci::vec4(90,173,150,68),
			ci::vec4(155,250,223,98)
		};

		
		std::vector<ci::ColorA> ciColors;
		for (int i = 0; i < num; ++i) {
			int rand = randInt(0, colors.size());


			auto col = colors[rand];
			ci::ColorA _col = ci::ColorA( col.r, col.g, col.b, col.a);
			ciColors.push_back(_col);
		}

		mColorData = gl::Vbo::create(GL_ARRAY_BUFFER, sizeof(ci::ColorA) * ciColors.size(), ciColors.data(), GL_STATIC_DRAW);
	}

	void update() {
		gl::ScopedGlslProg shader(mUpdateShader);
		//mUpdateShader->uniform("time", (float)app::getElapsedSeconds());

		mParticleData->bindBase(0);
		if (mTargetData) {
			mTargetData->bindBase(1);
		}

		gl::dispatchCompute(num, 1, 1);
		gl::memoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);
	}

	//! Load target data of attractors / repulsors 
	void loadTarget(std::vector<ci::vec3> data) {


		vector<Attractor> attractors;
		attractors.reserve(data.size());

		for (int i = 0; i < data.size(); ++i) {
			Attractor attract;
			attract.position = vec4(data[i],0.0);
			attract.meta = vec4(randFloat(), 0.0, 0.0, 0.0);
			attractors.push_back(attract);
		}

		mTargetData = gl::Ssbo::create(sizeof(Attractor) * attractors.size(), attractors.data(), GL_STATIC_DRAW);
		//mTargetData = gl::Ssbo::create(sizeof(ci::vec3) * data.size(), data.data(), GL_STATIC_DRAW);
	}

	void draw(mocha::Object3D * obj) {
		
		gl::pushMatrices();
		//gl::rotate(ci::toRadians(-90.0f), 0, 1, 0);
		
		// bind particle update data
		mParticleData->bindBase(0);

		mBatch->getGlslProg()->uniform("time", (float)app::getElapsedSeconds());
		mBatch->getGlslProg()->uniform("resolution", vec2(app::getWindowWidth(), app::getWindowHeight()));

		// draw instanced. 
		mBatch->drawInstanced(num);
		gl::popMatrices();
	}

	//! Composite the compute shader.
	void buildComputeShader(int numTargets) {
		compute = STRINGIFY(



			struct Particle
			{
				// current position 
				vec4 position;

				vec4 origin;

				vec4 meta;
			};

			struct Attractor {
				vec4 position;
				vec4 meta;
			};


			layout(std140, binding = 0) buffer Part
			{
				Particle particles[];
			};

			layout(std140, binding = 1) buffer GravWells
			{
				// mass is meta.x
				Attractor attractors[];
			};

			//layout( local_size_variable ) in;
			layout(local_size_x = 128, local_size_y = 1, local_size_z = 1) in;

		);


		compute += "int numTargets = " + std::to_string(numTargets) + ";\n";
	

		compute += STRINGIFY(
			float G = 0.5;
			void main() {

				uint gid = gl_GlobalInvocationID.x;	// The .y and .z are both 1 in this case.

				Particle p = particles[gid];

				// general movement
				p.position.x += 0.1;
				p.position.y = sin(p.meta.x) * p.position.w;
				p.meta.x += 0.04;

				// decrease life. 
				p.meta.y -= 0.001;

				for (int i = 0; i < 110; ++i) {
					Attractor target = attractors[i];

					vec4 force = p.position - target.position;
					float distance = length(force);
					force = normalize(force);

					distance = clamp(distance, 5.0, 25.0);

					float strength = (G * target.meta.x * p.meta.z) / (distance * distance);
					force *= vec4(strength);

					
					force /= p.meta.z;

					p.position += force;

				}

				/*
					// calculate the attractive force
					vec4 force = p.position - target.position;
					float distance = length(force);

					float strength = (G * target.meta.x * p.meta.z) / (distance * distance);
					force *= vec4(strength);

					p.position += force;*/


				if (p.meta.y < 0.0) {
					p.position.x = p.origin.x;
					p.meta.y = 1.0;
				}

				particles[gid] = p;

			}
		);
	}

protected:
	ci::gl::GlslProgRef mUpdateShader;
	int num = 100;
	ci::gl::BatchRef mBatch;
	ci::gl::SsboRef mParticleData, mTargetData;
	ci::gl::VboRef mIdsVbo,mColorData;

	std::string compute;
};