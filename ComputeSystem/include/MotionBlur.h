#pragma once


#include "cinder/gl/Fbo.h"
#include "cinder/gl/gl.h"
#include "cinder/app/App.h"
#include <memory>
using namespace ci;

typedef std::shared_ptr<class MotionBlur>MotionBlurRef;
class MotionBlur {
public:
	MotionBlur() = default;


	void setSubframes(int subframes) {
		this->subframes = subframes;
	}


	static MotionBlurRef create(int subframes = 16) {
		auto blur = new MotionBlur();
		blur->setSubframes(subframes);
		blur->setup();
		return MotionBlurRef(blur);
	}

	void setup() {



		resize();

		// make sure fbo resizes when the window resizes.
		app::getWindow()->getSignalResize().connect(std::bind(&MotionBlur::resize, this));

	}

	void update(std::function<void()> drawFunc) {
		auto SUBFRAMES = subframes;

		// make 'mAccumFbo' the active framebuffer
		gl::ScopedFramebuffer fbScp(mAccumFbo);
		
		// clear out both of our FBOs
		gl::clear(Color::black());
		
		// iterate all the sub-frames
		for (float i = 0; i < SUBFRAMES; i+=0.4) {
		
			gl::color(1, 1, 1, 1);

			// draw the Cube's sub-frame into mFbo
			gl::enableDepth();
			gl::enableAlphaBlending();
		
			mFbo->bindFramebuffer();
	

			drawFunc();

		}

		for (float i = 0; i < SUBFRAMES; i += 1.0) {

			// now add this frame to the accumulation FBO
			mAccumFbo->bindFramebuffer();
			gl::setMatricesWindow(mAccumFbo->getSize());
		
			gl::disableDepthRead();
			gl::disableDepthWrite();
			gl::disableAlphaBlending();
			gl::enableAdditiveBlending();
			gl::draw(mFbo->getColorTexture());

		}

		

	}
	

	ci::gl::TextureRef getColorTexture() {
		return mFbo->getColorTexture();
	}

	void draw() {

		gl::color(1.0f / subframes, 1.0f / subframes, 1.0f / subframes, 1);
		
		gl::setMatricesWindow(app::getWindowSize());
		gl::viewport(app::getWindowSize());

		gl::draw(mAccumFbo->getColorTexture(), app::getWindowBounds());
	}

	void resize() {

		gl::Fbo::Format accumFbo;
		accumFbo.colorTexture(gl::Fbo::Format().getDefaultColorTextureFormat().internalFormat(GL_RGB16F)).disableDepth();

		tfmt.setInternalFormat(GL_RGBA32F);
		fmt.setColorTextureFormat(tfmt);


		mAccumFbo = gl::Fbo::create(app::getWindowWidth(), app::getWindowHeight(), accumFbo);
		mFbo = gl::Fbo::create(app::getWindowWidth(), app::getWindowHeight(), fmt);
	}

protected:
	int subframes;
	gl::Texture::Format tfmt;
	gl::Fbo::Format fmt;
	ci::gl::FboRef mFbo,mAccumFbo;
};