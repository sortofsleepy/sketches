#include "cinder/app/App.h"
#include "cinder/app/RendererGl.h"
#include "cinder/gl/gl.h"
#include "mocha/core/Camera.h"
#include "cinder/Log.h"
#include "AudioLoader.h"
#include "WebAudio.h"
#include "AudioProcessor.h"
#include "CustomNode.h"
#include "ParticleSystem.h"
#include "AudioFileNode.h"
using namespace ci;
using namespace ci::app;
using namespace std;


class WebAudioApp : public App {
  public:
	void setup() override;
	void update() override;
	void draw() override;

	//ParticleSystem system;
	mocha::EasyCameraRef mCam;

	bool processorsLoaded = false;
	bool audioLoaded = false;

#ifdef CINDER_EMSCRIPTEN
	audio::WebAudioLoaderRef loader;
	audio::WebAudioRef audioCtx;
	audio::CustomNodeRef mCustom;
	audio::WebAudioProcessor processor;
	audio::AudioFileRef mAudio;


#endif 
};



void WebAudioApp::setup()
{

	mCam = mocha::EasyCamera::create();

#ifdef CINDER_EMSCRIPTEN
	loader = audio::WebAudioLoader::Instance();
	audioCtx = audio::WebAudioContext::Instance();
	
	// initialize new audio file.
	mAudio = audio::AudioFileNode::create();

	// set up the processor 
	processor.setup("passthru");

	// add it to the list of procesors to load
	audioCtx->add(processor);


	// start loading processors
	audioCtx->loadProcessors([=](emscripten::val e)->void {
		processorsLoaded = true;
		mCustom = audio::CustomNode::create("passthru");

		// load audio file. 
		loader->load("assets/test.mp3", [=](emscripten::val e)->void {
			audioLoaded = true;
		//	mAudio->setup();
			mAudio->setSource(e);

			mAudio  >> mCustom >> audioCtx;

			mAudio->play();
		});
	});

	
#endif





}

void WebAudioApp::update()
{
#ifdef CINDER_EMSCRIPTEN
	if (processorsLoaded && audioLoaded) {
		//system.update();


	}
#else
	//system.update();
#endif 
}

void WebAudioApp::draw()
{
	gl::clear( Color( 0, 0, 0 ) ); 

#ifdef CINDER_EMSCRIPTEN

#else
	//system.draw();
#endif 


}


CINDER_APP(WebAudioApp, RendererGl, [=](App::Settings * settings) {
	settings->setWindowSize(1024, 768);
});
