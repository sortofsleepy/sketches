#pragma once

#include <vector>
#include "cinder/Vector.h"
#include "mocha/gpu/TransformFeedback.h"
#include "cinder/Rand.h"
#include "cinder/app/App.h"
#include "mocha/utils.h"

using namespace std;
using namespace ci;


// keeping each part to 4 bytes 
typedef struct {

	vec3	pos;
	vec3	ppos;
	vec3	home;
	ColorA  color;
	float	damping;
	
}Particle ;

class ParticleSystem {
public:
	ParticleSystem(int numParticles=10000) {
		
		buffer = mocha::gpu::TFBuffer::create();
		buffer->setNumPoints(numParticles);

		// setup particle data 

		particles.assign(numParticles, Particle());
		const float azimuth = 256.0f * M_PI / particles.size();
		const float inclination = M_PI / particles.size();
		const float radius = 180.0f;
		vec3 center = vec3(ci::app::getWindowCenter() + vec2(0.0f, 40.0f), 0.0f);
		for (int i = 0; i < particles.size(); ++i)
		{	// assign starting values to particles.
			float x = radius * sin(inclination * i) * cos(azimuth * i);
			float y = radius * cos(inclination * i);
			float z = radius * sin(inclination * i) * sin(azimuth * i);

			auto &p = particles.at(i);
			p.pos = center + vec3(x, y, z);
			p.home = p.pos;
			p.ppos = p.home + Rand::randVec3() * 10.0f; // random initial velocity
			p.damping = Rand::randFloat(0.965f, 0.985f);
			p.color = Color(CM_HSV, lmap<float>(i, 0.0f, particles.size(), 0.0f, 0.66f), 1.0f, 1.0f);
		}


		// build out buffers
		buffer->bufferAttrib(particles);

		buffer->enableAttribArrays(5);

		// point to data in the buffers. 
		buffer->vertexAttribPointerInterleaved([=]()->void {
			gl::vertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Particle), (const GLvoid*)offsetof(Particle, pos));
			gl::vertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, sizeof(Particle), (const GLvoid*)offsetof(Particle, color));
			gl::vertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, sizeof(Particle), (const GLvoid*)offsetof(Particle, ppos));
			gl::vertexAttribPointer(3, 3, GL_FLOAT, GL_FALSE, sizeof(Particle), (const GLvoid*)offsetof(Particle, home));
			gl::vertexAttribPointer(4, 1, GL_FLOAT, GL_FALSE, sizeof(Particle), (const GLvoid*)offsetof(Particle, damping));			
		});


		// load simulation shader
		buffer->loadShader("sim.glsl", {
			"position", "pposition", "home", "color", "damping"
			},
			GL_INTERLEAVED_ATTRIBS, 
			[=](ci::gl::GlslProg::Format fmt)->ci::gl::GlslProg::Format {


			// link shaders with atributes.
			fmt.attribLocation("iPosition", 0)
				.attribLocation("iColor", 1)
				.attribLocation("iPPosition", 2)
				.attribLocation("iHome", 3)
				.attribLocation("iDamping", 4);

			return fmt;
		});

		// load render shader
		//mShader = mocha::utils::loadShader("particleRender.vert", "particleRender.frag");
		mShader = gl::getStockShader(gl::ShaderDef().color());
	}

	void update() {
		buffer->updateInterleaved();
	}

	void draw() {

		gl::ScopedGlslProg shader(mShader);
		
		buffer->draw();


	}

protected:
	vector<Particle> particles;
	vector<ci::vec4> audioData;
	ci::gl::GlslProgRef mShader;
	mocha::gpu::TFBufferRef buffer;


};