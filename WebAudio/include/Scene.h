#pragma once

#include "mocha/core/Camera.h"
#include "cinder/app/App.h"
#include "cinder/gl/Fbo.h"
#include <memory>

typedef std::shared_ptr<class Scene> SceneRef;


//! Describes a basic scene that's part of the video.
class Scene {
public:
	Scene();
	Scene(int viewWidth,int viewHeight);
	
	static SceneRef create(int width = ci::app::getWindowWidth(), int height = ci::app::getWindowHeight()) {
		return SceneRef(new Scene(width, height));
	}

	void setCamera(mocha::EasyCameraRef camera) { mCamera = camera;  }
	ci::gl::TextureRef getTexture() { return mFbo->getColorTexture(); }
	virtual void update();
	virtual void draw();
	void resize();

protected:
	mocha::EasyCameraRef mCamera;
	ci::gl::FboRef mFbo;
	int viewWidth;
	int viewHeight;																												
};