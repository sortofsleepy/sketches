WebAudio Block for Cinder Emscripten
====

This is a block built around the AudioWorklet design of the WebAudio api that is currently implemented in Chrome. Currently Chrome appears to be the only browser that supports 
AudioWorklet.

Why AudioWorklet?
====
While there is some very basic WebAudio support build into Cinder-Emscripten, it uses a older design, non-thread design.
Javascript is only able to utilize a single thread by default in a browser, so as you might imagine, doing anything remotely computationally expensive could lead to issues with your app.
Using AudioWorklets changes this because the new api is designed to take advantage of WebWorkers which are essentially the "threads" of Javascript and allow for a better user experience.

Current API thoughts
====
Given the highly asyncronous nature of Javascript and due to the nature of WebWorkers and how they are in their own isolated scope, this unfortunately means your desktop Cinder audio code
is most likely not compatible with this api, though every effort has been made to make the porting effort as painless as possible.


__Basic Design__
* There is the concept of a AudioWorkletProcessor object in the api whose purpose is to handle any audio processing you might want to do. 
* The AudioWorkletProcessor is connected to an AudioWorkletNode which basically acts as the interface to the processor.
* You create a Processor, add it to the context and then create a CustomNode(which will store the id of the Processor to use) to use when connecting to other AudioNodes, for example
```c++
	audio::WebAudioLoaderRef loader;
	audio::WebAudioRef audioCtx;
	audio::CustomNodeRef mCustom;
	audio::WebAudioProcessor processor;
	audio::AudioFileRef mAudio;

	loader = audio::WebAudioLoader::Instance();
	audioCtx = audio::WebAudioContext::Instance();
	
	// initialize new audio file.
	mAudio = audio::AudioFileNode::create();

	// set up the processor and give it the id of "passthru"
	processor.setup("passthru");

	// add it to the list of procesors to load
	audioCtx->add(processor);

	// start loading processors
	audioCtx->loadProcessors([=](emscripten::val e)->void {
	
		// create custom node, pass in the name "passthru" because that's the AudioWorkletProcessor we want to use to handle audio processing
		mCustom = audio::CustomNode::create("passthru");

		// load audio file. 
		loader->load("assets/test.mp3", [=](emscripten::val e)->void {

			mAudio->setSource(e);

			// link nodes - note that when linking an AudioContext, it will automatically connect to the default output destination of the context
			mAudio  >> mCustom >> audioCtx;

			mAudio->play();
		});
	});

```
* Processors are loaded as a group due to the async nature of how they are added, which is why you have the `loadProcessors` function. This will ensure all processors are loaded
prior to use.


Notes
=====
* Unfortunately, customizing a AudioWorkletProcessor will require writing Javascript, as the AudioWorkletProcessor is in it's scope and it's impossible to pass something like a callback function to it directly from C++ / Emscripten code.
[According to this thread](https://github.com/emscripten-core/emscripten/issues/6230) it does seem possible to compile a separate Worker Emscripten project and use that to process your audio; but I might suggest just dealing with
Javascript since you really only need to understand arrays in this case which more or less function exactly the same as in C++.


