
//! A namespace for AudioWorklet related stuff.
//! Note that AudioWorklet is currently a Chrome only feature.
window.CINDER_AUDIOWORKLETS = {

    //! AudioWorklet processors created 
    processors: {},

    //! Total number of AudioWorkletProcessor objects
    numProcessors:0,

    //! Returns a JS object reference to the desired Processor object.
    getProcessor:function(id){
        return this.processor[id];
    },

    defaultProcessorCreated:false,

    // Creates a new buffer source which is used to do things like help load audio into the graph
    // @param srcData {*} the data used for the source. 
    // @param context {WebAudioContext} an optional WebAudioContext - if not defined, then will use main global context.
    createBufferSource: function(srcData,context){
        context = context !== null && context !== undefined ? context : window.CINDER_AUDIO.globalContext;

        var source = context.createBufferSource();
        source.buffer = srcData;
        return source;
    },


    // Returns a list of possible output devices.
    // pass in a callback function to run once all devices are obtained.
    getOutputDevices: function(cb){

        navigator.mediaDevices.enumerateDevices().then(function (devices) {
            var outputs = devices.filter(function (device) {
                if (device.kind === "audiooutput") {
                    return device
                }
            });

            cb(outputs);
        });

    },

    loadFile: function (path, callback) {
        let context = window.CINDER_AUDIO.globalContext;
        fetch(path).then(function (res) {
            //perhaps one of the great failings of the fetch api - need to
            // return audio as an ArrayBuffer first.
            return res.arrayBuffer();
        }).then(function (buffer) {

            // decode the audio data
            context.decodeAudioData(buffer, function (data) {

                callback(data);
            }, function (err) {
                console.error("error decoding audio information - ", err);
            });

        }).catch(function (err) {
            console.log("\n");
            console.error("There was an error fetching the audio file at " + path);
            console.error("The error message was ", err);
            console.log("\n");
        })
    },


    //! Helper function to return the global audio context.
    getGlobalContext: function(){
        return window.CINDER_AUDIO.globalContext;
    },

    // helper function to connect 2 web audio AudioNode objects
    // Note that it does not check to make sure that the parameters are the correct object type.
    connectNodes: function (node1, node2) {
        console.log("connecting nodes, node 1 is ", node1, " and node 2 is ", node2);
        node1.connect(node2);
    },

   
    //! This takes care of loading all of the processors for the application. 
    //! @param cb {function} the callback function to run once all the AudioWorkletProcessors are done loading.
    load_processors: function(cb){
        var context = window.CINDER_AUDIO.globalContext;
        var count = 0;
        var total = this.numProcessors;

        for (var i in this.processors) {
           
            var worklet = this.processors[i];
      
            context.audioWorklet.addModule(worklet.source).then(function () {
                count += 1;
            })
            .catch(function (e) {
                  console.error("There was an error loading the processor ", worklet.id, "\n", e);
            });
        }

      
        var timer = setInterval(function () {
            if (count === total) {
                cb(true);
                clearInterval(timer);
            } 
        })
    },

    //! Creates a new AudioWorkletNode.
    //! TODO allow ability to create custom AudioWorkletNode
    create_worklet: function(id,context){
        context = context !== undefined ? context : window.CINDER_AUDIO.globalContext;
        let node = new AudioWorkletNode(context, id);
        node.port.onmessage = function () {
            console.log("message")
        }
        return node;
    },


    // creates a new AudioWorkletProcessor
    // @param cb {function} the function to run when the processor is processing audio data
    // @param id {string} the id to use to look up the processor.
    create_processor: function (id, customSource) {
        var source = null;

        // prepare id, if ID is not defined, set custom name.
        id = id !== "" ? id : "default-processor";

        var processor_source_default = `
        class CustomProcessor extends AudioWorkletProcessor {
            constructor() { super();

                this.id = "${id}";
                this.processFunction = null;

                // CustomNode needs to send a reference to the processor because window global is unavailable.
                // It's assumed just one message will be sent.
                // TODO add id to event object so we know how to process things.
                this.port.onmessage = (event) => {
                    consolle.log("Processor port received message", event);
                    this.processFunction = event.data;
                }
            }

            // default process function is to act as a pass-thru
            process(inputs, outputs, params) {
               const input = inputs[0]
               const output = outputs[0];

               for(let channel = 0; channel < output.length; ++channel) {
                    output[channel].set(input[channel]);
               }

                this.port.postMessage({
                    inputs: inputs,
                    outputs: outputs,
                    channelCount: inputs.length
                })

       
                return true;
            }
        }

        registerProcessor("${id}", CustomProcessor)
        `

        // setup the source code for the processor. If no source specified, use default source.
        source = customSource !== "" ? customSource : processor_source_default;

  
        // ensure the procesor hasn't already been set to load.
        let alreadyLoaded = false;
        for (var i in this.processors) {
            if (i === id) {
                alreadyLoaded = true;
            }
        }

        if (!alreadyLoaded) {
            this.processors[`${id}`] = {
                source: CINDER_FILEIO.sourceToBlob(source)
            };

            this.numProcessors += 1;
        }

      
    }
};


/*

// save processing function to run when 
            window.CINDER_AUDIOWORKLETS.processors[`${id}`] = {
                source: CINDER_FILEIO.sourceToBlob(source)
            };

            this.numProcessors += 1;
            this.defaultProcessorCreated = true;
    loadProcessors: function(cb){



    },

    // kicks off loading audio worklets. 
    loadWorklets: function() {
        var context = window.CINDER_AUDIO.globalContext;

        for (var i in this.worklets) {
            let worklet = this.worklets[i];
            context.audioworklet.addModule(worklet.source).then(worklet.workletCb)
              .catch(function () {
                  console.error("There was an error loading the processor ",worklet.id);
              });

        }
    },

    // triggers the loading of a specific worklet
    // @param id {string} the id of the worklet to start loading
    loadWorklet: function(id){
        var context = window.CINDER_AUDIO.globalContext;

        let worklet = this.worklets[id];
        context.audioworklet.addModule(worklet.source).then(function (e) {
            worklet.workletCb({
                destination:context.destination
            })
        })
          .catch(function () {
              console.error("There was an error loading the processor ", worklet.id);
          });
    },

    */


/*
 // first reverse worklets cause things are gonna get inserted in the wrong order for 
    // ease of use
    var worklets = this.worklets.reverse();

    // gotta remove the first item for some reason - probably cause we aren't setting AudioContext as "enabled"
    //worklets.shift();

    // check worklets to see if any items are just functions, if they are we they are custom nodes and we need to instantiate
    // a custom AudioWorketNode
    worklets = worklets.map(function(itm) {
      if(itm !== undefined){
        if(typeof itm === "function"){
          return new CustomNode(itm);
         
        }else{
          return itm;
        }
      }
    });


    // connect all the nodes together. 
    for(var i = 0; i < worklets.length-1; ++i){
      if(i !== worklets.length - 1){
        var start = worklets[i];
        var next = worklets[i + 1];
        start.connect(next);
      }
    }
  }
  */