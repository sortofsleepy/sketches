
#include "NativeNode.h"

#ifdef CINDER_EMSCRIPTEN
namespace cinder { namespace audio {
	NativeNode::NativeNode(WebAudioApiNode type):
		mNode(emscripten::val::undefined()),
		mIsEnabled(false){

		id = "NativeNode";

		auto it = NativeWebTypes.find(type);
		if (it != NativeWebTypes.end()) {

			// get audio helpers
			emscripten::val ctx = ci::em::helpers::getAudioHelpers()["globalContext"];

			std::string command = it->second;

			// create the node. 
			mNode = ctx.call<emscripten::val>(command.c_str());
		}
	}


} } // end cinder::em
#endif