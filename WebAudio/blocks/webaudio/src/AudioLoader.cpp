#ifdef CINDER_EMSCRIPTEN

#include "AudioLoader.h"
using namespace ci::em;
using namespace std;
using namespace emscripten;

namespace cinder { namespace audio {


	WebAudioLoader::WebAudioLoader():mAudioHelpers(emscripten::val::undefined()) {
		mAudioHelpers = ci::em::helpers::getAudioHelpers();
	}

	void WebAudioLoader::load(std::string file, std::function<void(emscripten::val)> cb,emscripten::val ctx) {
		auto fn = helpers::generateCallback(cb);
		auto worklets = emscripten::val::global("window")["CINDER_AUDIOWORKLETS"];

		// if user passes in their own context, use that, otherwise use global context
		auto _ctx = ctx != emscripten::val::undefined() ? ctx : mAudioHelpers["globalContext"];

		//mAudioHelpers.call<void>("loadAudioFile", val(file), _ctx, fn);
		worklets.call<void>("loadFile", file, fn);
	}

}} // end ci::audio
#endif 