#pragma once
#ifdef CINDER_EMSCRIPTEN
#include <memory>
#include <emscripten/val.h>
#include "cinder/Log.h"
#include "AudioNode.h"
#include "cinder/emscripten/CinderEmscripten.h"
#include "NodeDefinitions.h"

namespace cinder {namespace audio {

	typedef std::shared_ptr<class NativeNode> NativeNodeRef;

	//! Represents a native WebAudio api node.
	class NativeNode : public AudioNode {
	public:
		NativeNode( WebAudioApiNode type = GainNode );

		std::string getId() {
			return "NativeNode";
		}
		//! Returns a property on the native audio node (usually a AudioParam object)
		emscripten::val getAudioParam(std::string name) {
			return mNode[name];
		}

		//! Returns reference to JS element for native node
		emscripten::val getNode() {
			return mNode;
		}

		/**
		 *
		 */
		template<typename T>
		void setAudioParam
		(std::string paramName, T value) {
			mNode[paramName].set("value", emscripten::val(value));
		}

		template<typename T>
		void setParam(std::string paramName, T value) {
			mNode.set(paramName, emscripten::val(value));
		}
	private:
		//! reference to the native Web audio nodee object on the JS side.
		emscripten::val mNode;

		//! indicates whether or not the node is enabled
		bool mIsEnabled;
	};

} } // end cinder::em

#endif