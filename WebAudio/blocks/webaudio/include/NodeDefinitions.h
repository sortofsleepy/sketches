#pragma once
#ifdef CINDER_EMSCRIPTEN


namespace cinder {	namespace audio {

	// enumerate built-in WebAudio nodes
	enum WebAudioApiNode
	{
		BiquadFilterNode,
		ConvolverNode,
		DelayNode,
		DynamicsCompressorNode,
		IIRFilterNode,
		PeriodicWaveNode,
		WaveShaperNode,
		ChannelMergerNode,
		ChannelSplitterNode,
		StereoPannerNode,
		GainNode,
		AnalyserNode,
		OscillatorNode
	};

	//! map enum names to JS functions
	const static std::map<WebAudioApiNode, std::string> NativeWebTypes =
	{
		{ AnalyserNode,"createAnalyser" },
		{ BiquadFilterNode,"createBiquadFilter" },
		{ ChannelMergerNode, "createChannelMerger" },
		{ ChannelSplitterNode,"createChannelSplitter" },
		{ DelayNode,"createDelay" },
		{ ConvolverNode,"createConvolver" },
		{ GainNode,"createGain" },
		{ PeriodicWaveNode,"createPeriodicWave" },
		{ WaveShaperNode,"createWaveSharper" },
		{ StereoPannerNode,"createStereoPanner" },
		{ DynamicsCompressorNode,"createDynamicsCompressor" },
		{ OscillatorNode , "createOscillator" }
	};

}} // end cinder::em

#endif