
#pragma once
#ifdef CINDER_EMSCRIPTEN
#include <memory>
#include <emscripten/bind.h>
#include <emscripten/val.h>


namespace cinder {	namespace audio {


	typedef std::shared_ptr<class AudioNode> AudioNodeRef;

	class AudioNode {
	public:
		AudioNode() : mNode(emscripten::val::undefined()),
		mConnectedNode(emscripten::val::undefined()){}

		//! Returns whether or not this node is connected to something.
		bool isConnected() {
			if (mConnectedNode != emscripten::val::undefined()) {
				return true;
			}

			return false;
		}

		//! Returns the DOM representation of this audio node.
		emscripten::val getNode() {
			return mNode;
		}

		//! returns the id of the AudioNode
		std::string getId() {
			return id;
		}

		virtual void connect(AudioNodeRef node){
			auto worklets = emscripten::val::global("window")["CINDER_AUDIOWORKLETS"];
		

			// if the input is an WebAudioContext - connect the destination, otherwise connect output to input                                  
			if (node->getId() == "context") {
				//emscripten::val destination = node->getNode()["destination"];
				emscripten::val destination = node->getNode()["output"];

				//worklets.call<void>("connectNodes", mNode, destination);
		
				mNode.call<void>("connect", destination);
			}
			else {
				//worklets.call<void>("connectNodes", mNode, node->getNode());
				mNode.call<void>("connect", node->getNode());
				//worklets.call<void>("connectNodes", mNode, node->getNode());
			}

			//CI_LOG_I("Connecting " << this->getId() << " to" << node->getId());
		}


	protected:
		//! Serves as a way to identify a node by a nicer name. 
		std::string id;

		//! Reference to this AudioNode's DOM reference
		emscripten::val mNode;

		//! Reference to the node this Audio node is connected to 
		emscripten::val mConnectedNode;
	};

	inline const AudioNodeRef& operator>>(const AudioNodeRef &input, const AudioNodeRef &output)
	{
		CI_LOG_I("Input is " << input->getId() << " and it is connected to output " << output->getId());
		input->connect(output);
		return output;
	}


}}
#endif