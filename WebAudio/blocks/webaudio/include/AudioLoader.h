#pragma once

#ifdef CINDER_EMSCRIPTEN
#include <emscripten/val.h>
#include <memory>
#include "cinder/emscripten/CinderEmscripten.h"
namespace cinder { namespace audio {

	typedef std::shared_ptr<class WebAudioLoader>WebAudioLoaderRef;

	//! A class to help load audio files into AudioWorklet compatible formats.
	class WebAudioLoader {
	public:

		WebAudioLoader();
		static WebAudioLoaderRef create() {
			return WebAudioLoaderRef(new WebAudioLoader);
		}
		static WebAudioLoaderRef Instance()
		{
			static WebAudioLoaderRef instance(new WebAudioLoader);
			return instance;
		}
	
		WebAudioLoader(WebAudioLoader const&) = delete;
		void operator=(WebAudioLoader const&) = delete;


		//! Loads an audio file.
		//! Pass in a callback to run once the audio has loaded.
		void load(std::string file, std::function<void(emscripten::val)> cb,emscripten::val ctx=emscripten::val::undefined());
	
	private:

		//! holds a reference to audio helpers written in JS - see cinder/emscripten/helpers.js
		emscripten::val mAudioHelpers;
	};

}} // end ci::audio
#endif 