#pragma once

#ifdef CINDER_EMSCRIPTEN

#include "AudioNode.h"
#include <string>
#include <memory>
#include <emscripten/val.h>

namespace cinder {	namespace audio {

	typedef std::shared_ptr<class CustomNode>CustomNodeRef;

	class CustomNode : public AudioNode {
	public:
		CustomNode(std::string processorName,std::string id="CustomNode"){
			this->id = id;

			auto worklets = emscripten::val::global("window")["CINDER_AUDIOWORKLETS"];
			mNode = worklets.call<emscripten::val>("create_worklet", processorName);

			// get a reference to the callback function of the processor this node is attached to.
			mNode["port"].set("onmessage",
				em::helpers::generateCallback(std::bind(&CustomNode::onMessage, this, std::placeholders::_1)));

		}

		static CustomNodeRef create(std::string processorName, std::string id = "CustomNode") {
			return CustomNodeRef(new CustomNode(processorName, id));
		}
		
		virtual void onMessage(emscripten::val data){}

	protected:
		//! The name of the processor to use for this audio node.
		std::string mProcessorName;

		std::vector<float> mData;
	};


}}
#endif 

/*
for (int i = 0; i < channel_count; ++i)
			{
				emscripten::val inputs = jsdata["data"]["inputs"];
				emscripten::val outputs = jsdata["data"]["outputs"];

				int size = inputs["length"].as<int>();

				// loop through arrays of each channel
				for (int a = 0; a < size; ++a)
				{
					float inValue = inputs[a].as<float>();

					// just pass through here - users should override CustomNode object.
					outputs[a].set(i, inValue);

				}
			}*/