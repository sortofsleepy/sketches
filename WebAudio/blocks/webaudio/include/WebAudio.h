#pragma once

#ifdef CINDER_EMSCRIPTEN
#include <emscripten.h>

#include <memory>
#include "cinder/emscripten/CinderEmscripten.h"
#include "cinder/Log.h"
#include "AudioNode.h"
#include "NativeNode.h"
#include "AudioProcessor.h"
#include "AudioProcessor.h"

#define STRINGIFY(A) #A

namespace cinder { namespace audio {
	typedef std::shared_ptr<class WebAudioContext>WebAudioRef;

	//! A Webaudio context based on the worklet model. This represents a global WebAudioContext so it is presented as a Singleton.
	//! See https://developers.google.com/web/updates/2017/12/audio-worklet for more information.
	class WebAudioContext : public AudioNode{
	public:
	
		static WebAudioRef Instance()
		{
			static WebAudioRef instance(new WebAudioContext);
			return instance;
		}
		//void enable(AudioNodeRef node = nullptr);
		WebAudioContext(WebAudioContext const&) = delete;
		void operator=(WebAudioContext const&) = delete;


		template<typename T>
		std::shared_ptr<T> makeNode(T *node)
		{
			static_assert(std::is_base_of<AudioNode, T>::value, "AudioNode must inherit from audio::Node");

			std::shared_ptr<T> result(node);
			return result;
		}


	

		//! Creates a new WebAudioContext for use.
		emscripten::val generateNewContext() {
			return emscripten::val::global("AudioContext").new_();
		}

		void test() {
			std::function<void(emscripten::val)> functor = [=](emscripten::val e)->void {
				CI_LOG_I("Passing a function worked");
			};

			auto worklets = emscripten::val::global("window")["CINDER_AUDIOWORKLETS"];
		


			//worklets.call<void>("create_processor",em::helpers::generateCallback(functor),emscripten::val(std::string("test")));


		}


		//! add a processor that needs to get loaded. 
		WebAudioContext& add(ci::audio::WebAudioProcessor processor){
			mProcessors.push_back(processor);
			return *this;
		}

		// triggers loading of all of the Processors to use in the application
		void loadProcessors(std::function<void(emscripten::val e)> fn) {
			auto worklets = emscripten::val::global("window")["CINDER_AUDIOWORKLETS"];
			worklets.call<void>("load_processors", em::helpers::generateCallback(fn));
		}

		//! Sets the output device destination
		void setDestination(std::string deviceId) {
			destinationMask.call<void>("setSinkId", deviceId);
		}

		std::map < emscripten::val,emscripten::val > getDeviceIds() {
			if (devicesLoaded) {
				return devices;
			}
			else {
				CI_LOG_I("Device list has not yet been loaded");
			}
		}

		void setDestinationMask(emscripten::val audioContext = emscripten::val::undefined()) {
			if (destinationMask == emscripten::val::undefined()) {
				destinationMask = emscripten::val::global("Audio").new_();
			}

			if (audioContext != emscripten::val::undefined()) {
				streamDestination = audioContext.call<emscripten::val>("createMediaStreamDestination");
				mNode = audioContext;
			}
			else {
				streamDestination = mNode.call<emscripten::val>("createMediaStreamDestination");
			}

			destinationMask.set("srcObject", streamDestination["stream"]);
			mNode["output"] = streamDestination;

			destinationMask.call<void>("play");

		}
	
	private:
		WebAudioContext():
			destinationMask(emscripten::val::undefined()),
			streamDestination(emscripten::val::undefined())
		{ 
			id = "context"; 
			mNode = emscripten::val::global("window")["CINDER_AUDIO"]["globalContext"];

			// set up the default processor 
			processor.setup();

			setDestinationMask();


			// get a list of output device information
			getOutputDeviceInformation();

		}

		//! Fetches a list of possible output devices and stores it in the "devices" map.
		//! The function is unfortunately async, so calling "getDeviceIds" will not immediately work
		//! Also note that the device id is randomly generated for each instance.
		//! See https://developer.mozilla.org/en-US/docs/Web/API/MediaDeviceInfo for more information on what device information is returned.
		void getOutputDeviceInformation() {
			auto worklets = emscripten::val::global("window")["CINDER_AUDIOWORKLETS"];

			std::function <void(emscripten::val)> cb = [=](emscripten::val ids)->void {
				devices.insert(std::pair<emscripten::val, emscripten::val >(
					ids["label"],
					ids["deviceid"]
				));

				devicesLoaded = true;
			};

			worklets.call<void>("getOutputDevices", em::helpers::generateCallback(cb));
		}

		//! A list of Device ids that are available.
		//! note that this list is not immediately available due ot the async nature of fetching the device list.
		std::map< emscripten::val, emscripten::val > devices;

		bool devicesLoaded = false;

		// a list of processors tha tneed to be loaded 
		std::vector<ci::audio::WebAudioProcessor> mProcessors;
		
		//! Reference to the default processor
		audio::WebAudioProcessor processor;

		//! A masked point referring to a DOM audio element. This is used as a work around 
		//! since you can't currently select a destination directly.
		//! See https://stackoverflow.com/questions/41863094/how-to-select-destination-output-device-using-web-audio-api
		emscripten::val destinationMask;
		emscripten::val streamDestination;
	};
} }
#endif 

