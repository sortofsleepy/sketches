#pragma once

#ifdef CINDER_EMSCRIPTEN
#include <emscripten/bind.h>
#include <string>
#include "cinder/emscripten/CinderEmscripten.h"
#define STRINGIFY(A) #A
namespace cinder { namespace audio {

		class WebAudioProcessor {

		public:
			WebAudioProcessor() = default;
			
			// generates a new WebAudioProcessor for use 
			void setup(std::string id="default-processor",std::string customSource="") {
				auto worklets = emscripten::val::global("window")["CINDER_AUDIOWORKLETS"];
				//auto cb = em::helpers::generateCallback(std::bind(&WebAudioProcessor::process,this, std::placeholders::_1));
				
				this->id = id;
				worklets.call<void>("create_processor", id, customSource);
			}

			//! Helper to construct the basic Javascript for a custom processor. You just need to add an id and the processing code.
			static std::string buildProcessor(std::string name, std::string processCode) {

				return "class " + name + " extends AudioWorkletProcessor {" + 
					STRINGIFY(

						constructor() {
							super();
						}

						process(inputs,outputs,params){
					) + processCode + " } } " + 
					"resgisterProcessor(\"" + name + "\", " + name + ")";
				

			}


		protected:
			std::string id;
	
		};

}} // end ci::audio

#endif