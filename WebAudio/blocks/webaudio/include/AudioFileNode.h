#pragma once


#ifdef CINDER_EMSCRIPTEN
#include <emscripten/val.h>
#include <memory>
#include "AudioNode.h"
namespace cinder { namespace audio {

	typedef std::shared_ptr<class AudioFileNode>AudioFileRef;

	class AudioFileNode : public AudioNode {
	public:
		AudioFileNode() {
			id = "AudioFile";
		}

		static AudioFileRef create() {
			return AudioFileRef(new AudioFileNode);
		}
		
		//! Sets the source data for the audio file. 
		void setSource(emscripten::val data) {
			auto worklets = emscripten::val::global("window")["CINDER_AUDIOWORKLETS"];
			mNode = worklets.call<emscripten::val>("createBufferSource", data);
		}

		//! Starts playback of audio file
		void play() {
			mNode.call<void>("start");
		}
	protected:
	};

}} // end cinder::audio


#endif // CINDER_EMSCRIPTEN
