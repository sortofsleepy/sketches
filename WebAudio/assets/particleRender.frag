#version 300 es


in highp vec4	Color;
out highp vec4 	oColor;

void main( void )
{
	oColor = Color;
}