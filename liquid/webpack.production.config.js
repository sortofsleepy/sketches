const path = require('path');
module.exports = {
    entry:'./src/app.ts',
    mode:"production",
    module:{

        rules: [
            {
                test:/\.(glsl|vert|frag|vs|fs)$/,
                use:'raw-loader'
            },
            {
                test:/\.ts$/,
                exclude:/(node_modules | bower_components)/,
                use:{
                    loader:'ts-loader'
                }
            },


            {
                test:/\.js$/,
                exclude:/(node_modules | bower_components)/,
                use:{
                    loader:'babel-loader',
                    options:{
                        presets:[
                        ],
                        plugins:[
                        ]
                    }
                }
            }

        ],


    },
    resolve:{

        extensions:[".tsx",".ts",".js",".vert",".frag",".glsl"]
    },
    output:{
        filename:'app.js',
        path:path.resolve(__dirname,"public")
    },
    plugins:[]
}