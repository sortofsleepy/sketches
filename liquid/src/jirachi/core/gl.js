import {clamp, randFloat, randInt, range} from "../math/core";


/**
 * Creates a WebGLRenderingContext.
 * @param params {Object} parameters for how the context is made.
 * @returns {*|CanvasRenderingContext2D|WebGLRenderingContext}
 */
export default function(params={
    node:false,
    webglOptions:false
}){

    let defaultOptions = {
        alpha: true,
        antialias: true,
        depth: true
    };

    let options = params.webglOptions;

    if(options === false){
        Object.assign(options,defaultOptions);
    }else{
        Object.assign(defaultOptions,options);
        options = defaultOptions;
    }

    // determine the context type - default to WebGL2
    let contextType = params.hasOwnProperty("contextType") ? params.contextType : "webgl2"

    // make the canvas. If you want to build a context off a specific canvas, you can pass in a
    // value for the "node" property and it will use that, otherwise it creates a new canvas node.
    let canvas = params.node !== false ? params.node : document.createElement("canvas");
    let ctx = canvas.getContext(contextType,options);

    // extend GL prototypes
    for(let i in GLUtils.prototype){
        ctx[i] = GLUtils.prototype[i];
    }

    // extend other props onto the context
    Object.assign(ctx,new GLUtils());

    // write main constants onto the window
    writeConstants(ctx);

    return ctx;

}


/**
 * Write some useful and commonly used constants onto the window variable
 * @param gl {WebGLRedneringContext}
 */
export function writeConstants(gl){
    let constants = {
        "FLOAT":gl.FLOAT,
        "UNSIGNED_BYTE":gl.UNSIGNED_BYTE,
        "UNSIGNED_SHORT":gl.UNSIGNED_SHORT,
        "ARRAY_BUFFER":gl.ARRAY_BUFFER,
        "ELEMENT_BUFFER":gl.ELEMENT_ARRAY_BUFFER,
        "RGBA":gl.RGBA,
        "RGB":gl.RGB,
        "RGBA16F":gl.RGBA16F,
        "TEXTURE_2D":gl.TEXTURE_2D,
        "STATIC_DRAW":gl.STATIC_DRAW,
        "DYNAMIC_DRAW":gl.DYNAMIC_DRAW,
        "TRIANGLES":gl.TRIANGLES,
        "TRIANGLE_STRIP":gl.TRIANGLE_STRIP,
        "POINTS":gl.POINTS,
        "FRAMEBUFFER":gl.FRAMEBUFFER,
        "COLOR_ATTACHMENT0":gl.COLOR_ATTACHMENT0,
        // texture related
        "CLAMP_TO_EDGE":gl.CLAMP_TO_EDGE,
        "LINEAR":gl.LINEAR,
        "NEAREST":gl.NEAREST,
        "MAG_FILTER":gl.TEXTURE_MAG_FILTER,
        "MIN_FILTER":gl.TEXTURE_MIN_FILTER,
        "WRAP_S":gl.TEXTURE_WRAP_S,
        "WRAP_T":gl.TEXTURE_WRAP_T,
        "TEXTURE0":gl.TEXTURE0,
        "TEXTURE1":gl.TEXTURE1,
        "TEXTURE2":gl.TEXTURE2,

        // uniform related
        "UNIFORM_BUFFER":gl.UNIFORM_BUFFER,

        // simplify some math related stuff
        "PI":3.14149,
        "M_PI":3.14149, // same but Cinder alternative var
        "M_2_PI":3.14149 * 3.14149, // same but also from Cinder in case I accidentally ever get the two mixed up
        "2_PI": 3.14149 * 3.14149,
        "sin":Math.sin,
        "cos":Math.cos,
        "tan":Math.tan,
        "random":Math.random,
        "randFloat":randFloat,
        "randInt":randInt,
        "clamp":clamp,
        "range":range
    };

    if(window.hasOwnProperty("WebGL2RenderingContext")){
        // add more color attachment constants

        constants["COLOR_ATTACHMENT1"] = gl.COLOR_ATTACHMENT1;
        constants["COLOR_ATTACHMENT2"] = gl.COLOR_ATTACHMENT2;
        constants["COLOR_ATTACHMENT3"] = gl.COLOR_ATTACHMENT3;
        constants["COLOR_ATTACHMENT4"] = gl.COLOR_ATTACHMENT4;
        constants["COLOR_ATTACHMENT5"] = gl.COLOR_ATTACHMENT5;
    }

    if(!window.GL_CONSTANTS_SET){
        for(var i in constants){
            window[i] = constants[i];
        }
        window.GL_CONSTANTS_SET = true;
    }
}



/**
 * This class is meant to be extended and it's properties
 * added on top of a WebGLRenderingContext
 */

function GLUtils (width=window.innerWidth,height=window.innerHeight) {
    this.viewportX = 0;
    this.viewportY = 0;
    this.width = width;
    this.height = height;
}

GLUtils.prototype = {

    /**
     * Resets the viewport to your original settings. Will use current settings, or you
     * can override by passing in x,y,width,height
     * @param x
     * @param y
     * @param width
     * @param height
     */
    resetViewport(x=null,y=null,width=null,height=null){

        x = x !== 0 ? x : this.viewportX;
        y = y !== 0 ? y : this.viewportY;

        width = width !== null ? width : this.width;
        height = height !== null ? height : this.height;


        this.viewport(x,y,width,height);
    },

    /**
     * Enables depth write
     * @returns {GLUtils}
     */
    enableDepthWrite(){
        this.depthMask(true);
        return this;
    },

    /**
     * Disables depth write
     * @returns {GLUtils}
     */
    disableDepthWrite(){
        this.depthMask(false);
        return this;
    },

    clearScreen(r=0,g=0,b=0,a=1){
        this.clearColor(r,g,b,a);
        this.viewport(this.viewportX,this.viewportY, this.canvas.width,this.canvas.height);
        this.clear(this.COLOR_BUFFER_BIT | this.DEPTH_BUFFER_BIT);
        return this;
    },

    /**
     * Appends the canvas to the DOM.
     * @param {node} el the element you want to append to. By default will append to body
     */
    attachToScreen(el=document.body){
        el.appendChild(this.canvas);
        return this;
    },

    /**
     * Alias for above
     * @param el {Node} the element to append to
     * @returns {WebGLRenderingContext}
     */
    attachTo(el=document.body){

        this.attachToScreen(el);
        return this;
    },

    /**
     * Sets the viewport for the context
     * @param {number} x the x coordinate for the viewport
     * @param {number} y the y coordinate for the viewport
     * @param {number} width the width for the viewport
     * @param {number} height the height for the viewport
     */
    setViewport(x=0,y=0,width=window.innerWidth,height=window.innerHeight){
        this.viewport(x,y,width,height);
    },

    /**
     * Shorthand for enabling blending. Note that no blend functions are set
     */
    enableBlending(){
        this.enable(this.BLEND);
        return this;
    },

    disableBlend(){
        this.disable(this.BLEND);
        return this;
    },

    disableBlending(){
        this.disable(this.BLEND);
        return this;
    },
    /**
     * Enable depth testing
     */
    enableDepth(){
        this.enable(this.DEPTH_TEST);
        return this;
    },

    disableDepth(){
        this.disable(this.DEPTH_TEST);
        return this;
    },

    /**
     * Alternative to setting face culling.
     * @param mode
     */
    faceCull(mode="BACK"){
        this.cullFace(this[mode]);
        return this;
    },

    getCullStatus(){
        switch(this.getParameter(this.CULL_FACE_MODE) ){
            case this.FRONT :
                return "FRONT";
                break;

            case this.BACK:
                return "BACK";
                break;

            case this.FRONT_AND_BACK:
                return "FRONT AND BACK";
                break;
        }
    },



    /**
     * Enables alpha blending
     */
    enableAlphaBlending(){
        this.enable(this.BLEND);
        //this.setBlendFunction("SRC_ALPHA","ONE_MINUS_SRC_ALPHA");
        this.blendFunc(this.SRC_ALPHA,this.ONE_MINUS_SRC_ALPHA);

    },
    /**
     * Enables additive blending.
     * Note - that it assumes depth testing is already turned off.
     */
    enableAdditiveBlending(){
        this.enable(this.BLEND);
        this.blendEquationSeparate( this.FUNC_ADD, this.FUNC_ADD );
        this.blendFuncSeparate( this.ONE, this.ONE, this.ONE, this.ONE );
    },

    /**
     * Sets the size of the gl canvas
     * @param width {Number} Width that the canvas should be. Defaults to entire window
     * @param height { Number} Height that the canvas should be. Defaults to window.innerHeight
     * @returns {RendererFormat}
     */
    setSize(width=window.innerWidth,height=window.innerHeight){
        this.canvas.width = width;
        this.canvas.height = height;
        return this;
    },

    /**
     * Sets the context to be full screen of it's containing element.
     * @param {function} customResizeCallback specify an optional callback to deal with what happens when the screen re-sizes.
     * @returns {WebGLRenderingContext}
     */
    setFullscreen(customResizeCallback=null){

        let parent = this.canvas.parentElement;

        this.canvas.width = window.innerWidth;
        this.canvas.height = window.innerHeight;


        //set the viewport size
        this.setViewport();

        if(customResizeCallback){
            window.addEventListener("resize",customResizeCallback);
        }else {
            window.addEventListener("resize",() => {

                this.canvas.width = window.innerWidth;
                this.canvas.height = window.innerHeight;

                this.setViewport(0,0,this.canvas.width,this.canvas.height);
            });
        }
        return this;
    }
};

