


export default function(gl,{
    width=512,
    height=512,
    format=null,
    data=null
}={},name=null){

    let tex = gl.createTexture();

    // first, assign all the prototype props onto the WebGLTexture object
    for(let i in GLTexture.prototype){
        tex[i] = GLTexture.prototype[i];
    }

    // check to see if there's a format object describing how the texture
    // should work, if not, get some sensible defaults.
    let fmt = null;
    if(format === null){
        fmt = GLFormat.getDefaults(gl,width,height);
    }else{
        fmt = GLFormat.getDefaults(gl,width,height);

        // override defaults with anything passed in
        Object.assign(fmt,format);
    }


    // emable some extensions that still need to be enabled.
    gl.getExtension('OES_texture_float');
    gl.getExtension('OES_texture_float_linear');


    // next initialize a new texture object
    let obj = new GLTexture(gl,width,height,fmt,name);


    Object.assign(tex,obj);


    // ===================== INIT DATA ========================== //

    // if data is a HTML image, initialize a texture based on that information.
    if(data instanceof Image){
        tex.initializeImageTexture(data);
    }else{
        tex.initializeDataTexture(data);
    }



    return tex;

}


function GLTexture(gl,width,height,format,name=null){

    this.gl = gl;
    this.format = format;
    this.width = width;
    this.height = height;
    this.isNodeBasedTexture = false;
    this.name = name;

    return this;
}





GLTexture.prototype = {

    setName(name=null){

        this.name = name;
    },

    bind(index=0){
        let gl = this.gl;
        gl.activeTexture(gl[`TEXTURE${index}`]);
        gl.bindTexture(gl.TEXTURE_2D,this);

    },

    unbind(){
        this.gl.bindTexture(this.gl.TEXTURE_2D,null);
    },


    /**
     * Builds a depth texture. Note that this will override any previous settings
     * created by the format object.
     * @param wrapS
     * @param wrapT
     * @param minFiter
     * @param magFilter
     * @param format
     * @param width
     * @param height
     * @param depthType
     * @returns {GLTexture}
     */
    createDepthTexture({
        wrapS=this.gl.CLAMP_TO_EDGE,
        wrapT=this.gl.CLAMP_TO_EDGE,
        minFilter=this.gl.LINEAR,
        magFilter=this.gl.LINEAR,
        depthType=this.gl.DEPTH_COMPONENT16,
        width=this.width,
        height=this.height
    }={}){
        let gl = this.gl;


        gl.bindTexture(gl.TEXTURE_2D,this);
        gl.texParameteri(gl.TEXTURE_2D,gl.TEXTURE_MAG_FILTER,magFilter);
        gl.texParameteri(gl.TEXTURE_2D,gl.TEXTURE_MIN_FILTER,minFilter);

        gl.texParameteri(gl.TEXTURE_2D,gl.TEXTURE_WRAP_S,wrapS);
        gl.texParameteri(gl.TEXTURE_2D,gl.TEXTURE_WRAP_T,wrapT);

        if(gl instanceof WebGLRenderingContext){
            gl.texImage2D(gl.TEXTURE_2D,0,depthType,width,height,0,depthType,gl.UNSIGNED_SHORT,null);
        }else{
            gl.texImage2D(gl.TEXTURE_2D,0,depthType,width,height,0,gl.DEPTH_COMPONENT,gl.UNSIGNED_SHORT,null);
        }

        gl.bindTexture(gl.TEXTURE_2D,null);

        return this;
    },

    /**
     * initializes anything that isn't a video or image texture.
     * TODO somehow pass thru width / height?
     * @param data
     */
    initializeDataTexture(data){
        let gl = this.gl;
        let options = this.format;



        gl.bindTexture(gl.TEXTURE_2D,this);

        if(options.flipY){

        //    gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL,options.flipY);
        }

        // set the image
        // set the image
        // known to work with random floating point data
        //gl.texImage2D(gl.TEXTURE_2D,0,gl.RGBA16F,width,height,0,gl.RGBA,gl.FLOAT,data);
        gl.texImage2D(
            gl.TEXTURE_2D,
            0,
            options.internalFormat,
            options.width,
            options.height,
            0,
            options.format,
            options.texelType,
            data
        );


        // set min and mag filters
        gl.texParameteri(gl.TEXTURE_2D,gl.TEXTURE_MAG_FILTER,options.magFilter);
        gl.texParameteri(gl.TEXTURE_2D,gl.TEXTURE_MIN_FILTER,options.minFilter)

        if(this.name){
            //console.log(this.name ,"-", options.minFilter,options.magFilter);
            //console.log("gl check is ",gl.getTexParameter(gl.TEXTURE_2D,gl.TEXTURE_MAG_FILTER))
            //console.log("value should be ",gl.LINEAR)
        }

        //set wrapping
        gl.texParameteri(gl.TEXTURE_2D,gl.TEXTURE_WRAP_S,options.wrapS)
        gl.texParameteri(gl.TEXTURE_2D,gl.TEXTURE_WRAP_T,options.wrapT)

        // generate mipmaps if necessary
        if(options.generateMipMaps){
            gl.generateMipmap(gl.TEXTURE_2D);
        }


        gl.bindTexture(gl.TEXTURE_2D,null);


        this.contents = data;
        return this;

    },

    /**
     * Initializes a texture with an Image Node
     * @param image
     * @returns {WebGLTexture}
     */
    initializeImageTexture(image){
        let gl = this.gl;
        let options = this.format;
        this.width = image.width;
        this.height = image.height;

        gl.bindTexture(gl.TEXTURE_2D,this);

        if(options.flipY){
            gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL,options.flipY);
        }

        // set the image
        gl.texImage2D(gl.TEXTURE_2D,0,options.format,options.format,options.texelType,image);
        //gl.texImage2D(gl.TEXTURE_2D,0,options.format,options.format,gl.UNSIGNED_BYTE,image);

        // set min and mag filters
        gl.texParameteri(gl.TEXTURE_2D,gl.TEXTURE_MAG_FILTER,options.magFilter);
        gl.texParameteri(gl.TEXTURE_2D,gl.TEXTURE_MIN_FILTER,options.minFilter)



        //set wrapping
        gl.texParameteri(gl.TEXTURE_2D,gl.TEXTURE_WRAP_S,options.wrapS)
        gl.texParameteri(gl.TEXTURE_2D,gl.TEXTURE_WRAP_T,options.wrapT)

        // generate mipmaps if necessary
        if(options.generateMipMaps){
            gl.generateMipmap(gl.TEXTURE_2D);
        }


        gl.bindTexture(gl.TEXTURE_2D,null);

        // store contents as an attribute of the texture object
        this.contents = image;
        this.isNodeBasedTexture = true;

        return this;
    },


    /**
     * Generates some random floating point data
     * @param width {Number} width of texture
     * @param height {Number} height of texture
     * @param alpha {Boolean} whether or not this texture should contain an alpha channel
     * @returns {Float32Array}
     * @private
     */
    _generateRandomFloatingPointData(width,height,alpha=false){

        let data = null;
        alpha = alpha !== false ? 4 : 3;

        data = new Float32Array(width * height * alpha);

        let len = width * height;
        for(let i = 0; i < len; ++i){
            data[i] = Math.random();
        }

        return data;

    },

    resize(w,h){
        let gl = this.gl;
        let options = this.format;
        this.width = w;
        this.height = h;

        this.bind();

        if(this.isNodeBasedTexture){
            gl.texImage2D(gl.TEXTURE_2D,0,options.format,options.format,options.texelType,this.contents);
        }else{

            gl.texImage2D(
                gl.TEXTURE_2D,
                0,
                options.internalFormat,
                this.width,
                this.height,
                0,
                options.format,
                options.texelType,
                this.contents
            );
        }


        this.unbind(0)
    }
};


/**
 * An organizational class to help deal with all the possible parameters
 * when it comes to creating a texture.
 */
export class GLFormat {
    constructor(gl,{
        wrapS=gl.CLAMP_TO_EDGE,
        wrapT=gl.CLAMP_TO_EDGE,
        minFilter=gl.NEAREST,
        magFilter=gl.NEAREST,
        flipY=false,
        generateMipmaps=false,
        format=gl.RGBA
    }={}){
        this.gl = gl;


        this.format = format;
        this.generateMipmaps = generateMipmaps;
        this.wrapS = wrapS;
        this.wrapT = wrapT;
        this.minFilter = minFilter;
        this.magFilter = magFilter;
        this.flipY = flipY;

    }

    setWrap(wrapSName,wrapTName){
        this.wrapS = this.gl[wrapSName];
        this.wrapT = this.gl[wrapTName];
        return this;
    }

    setFilter(minFilter,magFilter){
        this.minFilter = this.gl[minFilter];
        this.magFilter = this.gl[magFilter];
        return this;
    }

    flipY(shouldFlip=false){
        this.flipY = shouldFlip;
        return this;
    }

    getFormat(){
        return {
            wrapS:this.wrapS,
            wrapT:this.wrapT,
            minFilter:this.minFilter,
            magFilter:this.magFilter,
            flipY:this.flipY,
            format:this.format
        }
    }


    /**
     * Return default properties of the class.
     * @returns {{wrapS: *, wrapT: *, minFilter: *, magFilter: *}}
     */
    static getDefaults(gl,width,height,flipY=true){

        return {
            width:width,
            height:height,
            wrapS:gl.CLAMP_TO_EDGE,
            wrapT:gl.CLAMP_TO_EDGE,
            minFilter:gl.NEAREST,
            magFilter:gl.NEAREST,
            texelType:gl.UNSIGNED_BYTE,
            generateMipmaps:false,
            format:gl.RGB,
            internalFormat:gl.RGB,

            // by default, all textures are upside down, this corrects that
            flipY:flipY
        }
    }
}


