import createVAO from './vao'

/**
 * This is largely based on David Li's implementation
 */
class DrawState {
    constructor(gl,{

        framebuffer=null,
        width=window.innerWidth,
        height=window.innerHeight
    }={}){

        this.gl = gl;
        this.vao = createVAO(gl);
        this.framebuffer = framebuffer;
        this.blending = false;
        this.blendFunc = null;
        this.hasIndexBuffer = false;

        this.shader = null;

        this.isInstanced = false;

        // reference to all textures that need to be bound for the state.
        this.textures = {};

        // contains references to attributes along with their data buffer.
        this.attributes = {};

        // uniforms
        this.uniforms = {};

        this.viewport = [0,0,width,height];

        this.clearColor = [0,0,0,0];

        // if we're drawing to the main viewport.
        window.addEventListener('resize',() => {
            this.viewport = [0,0,width,height];
        })
    }

    setClear(useClearColor=false){
        this.shouldClear = true;
        this.shouldUseClearcolor = useClearColor;
        return this;
    }

    /**
     * Sets a FrameBuffer for use. Note that if set, drawing will always run on the FrameBuffer
     * @param fbo {WebGLFramebuffer} a framebuffer for the state.
     * @returns {DrawState}
     */
    setFramebuffer(fbo){
        if(fbo instanceof WebGLFramebuffer){
            this.framebuffer = fbo;
        }

        return this;
    }

    setShader(shader){
        if (shader instanceof WebGLProgram && shader.hasOwnProperty("bind")){
            this.shader = shader;
        }
        return this;
    }

    setBlending(func=false,eq=false){
        this.blending = true;
        this.blendFunc = func !== false ? func : () => {}
        this.blendEquation = eq !== false ? eq : ()=>{}

        return this;
    }


    setIndexBuffer(){

    }


    /**
     * Returns Texture from FBO if available
     * @returns {*|WebGLTexture}
     */
    getTexture(){
        if(this.framebuffer){
            return this.framebuffer.getTexture();
        }
    }

    setInstancedAttribute(name,buffer,{
        size=3,
        type=this.gl.FLOAT,
        normalized=false,
        stride=0,
        offset=0,
        numInstances=1
    }={}){

        this.isInstanced = true;

        if(buffer instanceof WebGLBuffer &&
            buffer.hasOwnProperty("bind")) {


            if (this.shader === null) {
                console.error(`Cannot add attribute "${name}" as there was no shader passed in - thus we can't get the attribute location`)
            } else {




                // ensure data is written onto VAO
                if (buffer.data !== null) {

                    this.vao.bind();
                    buffer.bind();
                    buffer.bufferData(buffer.data);

                    buffer.unbind();
                    this.vao.unbind();
                }


                this.attributes[name] = {
                    buffer: buffer,
                    name: name,
                    dataOptions: {
                        size: size,
                        type: type,
                        normalized: normalized,
                        stride: stride,
                        offset: offset
                    },
                    location: this.shader.attributes[name].location
                }

                this.vao.bind();
                // write attribute to VAO
                buffer.bind();

                this.vao.enableAttribute(this.attributes[name].location);
                this.vao.vertexAttribPointer(this.attributes[name].location, this.attributes[name].dataOptions);
                this.vao.makeInstancedAttribute(this.attributes[name].location,1);
                buffer.unbind();
                this.vao.unbind();
            }
        }

    }

    /**
     * Adds an attribute to the object for drawing
     * @param name
     * @param attr
     * @param options
     */
    setAttribute(name,buffer,{
        size=3,
        type=this.gl.FLOAT,
        normalized=false,
        stride=0,
        offset=0
    }={}){

        /**
         * Handle situation where we pass in a Jirachi VBO .
         */
        if(buffer instanceof WebGLBuffer &&
            buffer.hasOwnProperty("bind")){


            if(this.shader === null){
                console.error(`Cannot add attribute "${name}" as there was no shader passed in - thus we can't get the attribute location`)
            }else{




                // ensure data is written onto VAO
                if(buffer.data !== null){

                    this.vao.bind();
                    buffer.bind();
                    buffer.bufferData(buffer.data);
                    buffer.unbind();
                    this.vao.unbind();
                }



                this.attributes[name] = {
                    buffer:buffer,
                    name:name,
                    dataOptions:{
                        size:size,
                        type:type,
                        normalized:normalized,
                        stride:stride,
                        offset:offset
                    },
                    location:this.shader.attributes[name].location
                }

                this.vao.bind();
                // write attribute to VAO
                buffer.bind();
                this.vao.enableAttribute(this.attributes[name].location);
                this.vao.vertexAttribPointer(this.attributes[name].location,this.attributes[name].dataOptions);
                buffer.unbind();
                this.vao.unbind();
            }
        }

    }

    useProgram(shader=null){

        if(!this.shader && shader !== null){
            this.shader = shader;
        }


        return this;
    }

    uniform(name,value,shaderFunc=null){



        this.uniforms[name] = {
            func:shaderFunc !== null ? shaderFunc : "uniform",
            name:name,
            value:value
        }

        return this;
    }

    uniformTexture(name,value,texture){
        this.setTextureUniform(name,value,texture);
        return this;
    }


    setTextureUniform(name,value,texture){
        let gl = this.gl;
        if(!texture instanceof WebGLTexture){
            return;
        }

        this.textures[name] = {
            location:value,
            texture:texture
        };
        return this;
    }

    setViewport(x,y,width,height){
        this.viewport = [x,y,width,height];
        return this;
    }

    drawElements(
        mode = this.gl.TRIANGLES,
        count=this.numElements,
        type=this.gl.UNSIGNED_SHORT,
        offset=0,
        {
            viewportReset=[0,0,window.innerWidth,window.innerHeight]
        }={}
    ) {



    }
    /**
     *
     * @param mode {Number}
     * @param start {Number}
     * @param count {Number}
     * @param options {Object}
     */
    drawArrays(mode=this.gl.TRIANGLES,start=0,count=0,options={
        numInstances:1,
        viewportReset:[0,0,window.innerWidth,window.innerHeight]
    }){

        let gl = this.gl;





        this._updateViewport()

        this.vao.bind();
        if(this.shader){
            this.shader.bind();


            for(let i in this.uniforms){
                let uniform = this.uniforms[i];

                // TODO rethink this cause it breaks passing array uniforms.
                if(this.shader.uniforms[i] === undefined){
                    //console.log("Unable to find uniform ",i)
                   // return;
                }

                this.shader[uniform.func](uniform.name,uniform.value);
            }

            for(let i in this.textures){
                let tex = this.textures[i];
                if(this.shader.uniforms[i] === undefined){
                    console.log("Unable to find uniform for texture ",i)
                    return;
                }

                tex.texture.bind(tex.location);
                this.shader.uniform(i,tex.location);

            }

        }

        if(this.framebuffer){
            this.framebuffer.bind();

            if(this.shouldClear){
                gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT)
            }
        }


        if(this.isInstanced){
            gl.drawArraysInstanced(mode,start,count,options.numInstances)
        }else{
            gl.drawArrays(mode,start,count)
        }


        if(this.framebuffer){
            this.framebuffer.unbind();
        }
        this._updateViewport(options.viewportReset)
        this.vao.unbind();
    }


    // ====================== PRIVATE ========================== //

    _updateViewport(resetArray=null){
        if(resetArray!== null){
            this.gl.viewport(resetArray[0],resetArray[1],resetArray[2],resetArray[3]);
        }else{
            this.gl.viewport(this.viewport[0],this.viewport[1],this.viewport[2],this.viewport[3]);
        }
    }

    _unbindTextures(){
        this.gl.bindTexture(this.gl.TEXTURE_2D,null);
    }

    _enableAttributes(){
        let attributes = this.attributes;
        let vao = this.vao;

        for(let i in attributes){
            let attr = attributes[i];

            attr.buffer.bind();
            vao.enableAttribute(attr.location)
            //vao.vertexAttribPointer(attr.location,attr.dataOptions)
        }


    }

    _disableAttributes(){
        let gl = this.gl;
        let attributes = this.attributes;
        let vao = this.vao;


        for(let i in attributes){
            let attr = attributes[i];

            attr.buffer.bind();
            vao.disableAttribute(attr.location)
            //vao.vertexAttribPointer(attr.location,attr.dataOptions)
        }


    }
}

export default DrawState;