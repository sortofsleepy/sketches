

export default function(gl){

    let vao = null;
    let ext = null;

    //if(gl instanceof WebGL2RenderingContext){
    if(window.hasOwnProperty("WebGL2RenderingContext")){
        vao = gl.createVertexArray();
    }else{

        ext = gl.getExtension("OES_vertex_array_object");
        if(ext === null || undefined){
            console.error("Your graphics card doesn't appear to support the VAO extension - please use a newer computer");
            return;
        }
        vao = ext.createVertexArrayOES();
    }


    for(var i in VAO.prototype){
        vao[i] = VAO.prototype[i];
    }

    Object.assign(vao,new VAO(gl));

    return vao;
}

// ========== VAO DEFINITION ================= //

function VAO(gl){
    this.gl = gl;
    this.attributes = {}
    this.attribCount = 0;
}

VAO.prototype = {

    bind(){
        this.gl.bindVertexArray(this);
    },
    unbind(){

        this.gl.bindVertexArray(null);
    },

    /**
     * Wrapper around the regular vertexAttribPointer, but provides
     * some common defaults
     * @param idx {Number} index to point 2
     * @param size {Number} size of each element of the attribute
     * @param type {Number} value type of the attribute(ie FLOAT)
     * @param normalized {Boolean} whether or not the data is normalized for the attribute
     * @param stride {Number}
     * @param offset {Number}the place in the buffer where the data for the attribute begins(0 is usually ok but this may change
     * depending on how your buffer is setup)
     */
    vertexAttribPointer(idx,{
        size=3,
        type=this.gl.FLOAT,
        normalized=false,
        stride=0,
        offset=0
    }={}){


        this.gl.vertexAttribPointer(idx,size,type,normalized,stride,offset);
    },

    /**
     * Sets an attribute's location
     * @param shader {WebGLProgram} a WebGl shader program to associate with the attribute location
     * @param name {String} the name of the attribute
     * @param index {Number} an optional index. If null, will utilize the automatically assigned location
     * @returns {number} returns the location for the attribute
     */
    setAttributeLocation(shader,name,index=0){
        let gl = this.gl;
        return  gl.bindAttribLocation(shader,index,name);
    },

    /**
     * Enables an attribute to become instanced
     * @param attribute {Number} the attribute location
     * @param divisor {Number} the divisor value. It will almost always end up being 1
     * @returns {boolean}
     */
    makeInstancedAttribute(attribute,divisor=1){

        let gl = this.gl;

        if(!attribute instanceof Number){
            return;
        }

        if(gl.vertexAttribDivisor === null || gl.vertexAttribDivisor === undefined){
            let ext = null;
            if(gl.hasOwnProperty('ANGLE_instanced_arrays')){
                ext = gl.ANGLE_instanced_arrays;
            }else{
                try {
                    ext = gl.getExtension('ANGLE_instanced_arrays');
                }catch(e){
                    console.error("cannot utilize instanced attributes on this GPU");
                    return false;
                }
            }
            ext.vertexAttribDivisorANGLE(attribute,divisor);
        }else{



            gl.vertexAttribDivisor(attribute,divisor);
        }

        return this;

    },

    /**
     * Disable instanced attribute
     * @param attribute {Number} the location of the attribute.
     * @returns {*}
     */
    disableInstancedAttribute(attribute){
        let gl = this.gl;

        if(!attribute instanceof Number){
            return;
        }


        if(gl.vertexAttribDivisor === null || gl.vertexAttribDivisor === undefined){
            let ext = null;
            if(gl.hasOwnProperty('ANGLE_instanced_arrays')){
                ext = gl.ANGLE_instanced_arrays;
            }else{
                try {
                    ext = gl.getExtension('ANGLE_instanced_arrays');
                }catch(e){
                    console.error("cannot utilize instanced attributes on this GPU");
                    return false;
                }
            }
            ext.vertexAttribDivisorANGLE(attribute,0);
        }else{
            gl.vertexAttribDivisor(attribute,0);
        }

        return this;
    },

    /**
     * Adds an attribute for the VAO to lookup
     * @param shader {WebGLShader} a WebGL shader program that has it's active attributes appended to it.
     * @param name {String} name of the attribute
     * @param size {Number} size of each item of the attribute
     * @param dataOptions {Object} a set of options on how to interpret the data.
     */
    addAttribute(shader,name,size=3,dataOptions={
        type:this.gl.FLOAT,
        normalized:false,
        stride:0,
        offset:0
    }){

        let gl = this.gl;
        let attributes = shader.attributes;


        if(attributes.hasOwnProperty(name)){
            if(attributes[name].hasOwnProperty("location")){

                gl.enableVertexAttribArray(attributes[name].location);
                this.vertexAttribPointer(attributes[name].location,{
                    size:size,
                    type:dataOptions.type,
                    normalized:dataOptions.normalized,
                    stride:dataOptions.stride,
                    offset:dataOptions.offset
                });

            }else{

                // if for some reason we've haven't fetched the attribute already, pre-set an attribute location.
                let loc = setAttributeLocation(shader,name,this.attribCount);
                gl.enableVertexAttribArray(loc);
                this.vertexAttribPointer(loc,{
                    size:size,
                    type:dataOptions.type,
                    normalized:dataOptions.normalized,
                    stride:dataOptions.stride,
                    offset:dataOptions.offset
                });



                // increment attribute count
                this.attribCount += 1;
            }
        }else{
            console.error(`Unable to add attribute ${name} to the VAO because it does not appear to be active.`)
        }

    },

    /**
     * Enables a vertex attribute location.
     * @param val {String} can also be a number. If it's a string, it does a lookup in the
     * attributes property.
     */
    enableAttribute(val,shader){

        // enable vertex attribute at the location
        if(typeof val === "number"){
            this.gl.enableVertexAttribArray(val);
        }else{
            this.gl.enableVertexAttribArray(shader.attributes[val].loc);
        }
    },

    disableAttribute(){

    }
}