import createTexture from './texture'

export default function (gl,{
    width=512,
    height=512,
    textureFormat = null,
    initialTexture=null,
    attachments=[],
    hasDepthTexture=false
}={}){
    let fbo = gl.createFramebuffer();

    for(var i in FBO.prototype){
        fbo[i] = FBO.prototype[i];
    }



    Object.assign(fbo,new FBO(gl,{
        width:width,
        height:height,
        textureFormat:textureFormat,
        initialTexture:initialTexture,
        attachments:attachments,
        depthTexture:hasDepthTexture
    }));

    fbo._build();
    return fbo;
}

// =========== DEFINITION  =================== //

function FBO(gl,options){
    this.gl = gl;
    this.width = options.width;
    this.height = options.height;
    this.textureFormat = options.textureFormat;
    this.options = options;
    this.attachments = options.attachments;
    this.texture = null;




    // need to enable this extension for some reason to do floating point stuff.
    // https://webgl2fundamentals.org/webgl/lessons/webgl1-to-webgl2.html
    gl.getExtension("EXT_color_buffer_float");
}

FBO.prototype = {

    /**
     * Returns the default color attachment texture.
     * @returns {WebGLTexture}
     */
    getTexture(){
        if(this.attachments < 1){
            return this.texture;
        }else{
            return this.getTextureAt();
        }
    },

    /**
     * Resizes a framebuffer
     * @param w {Number} New width for the framebuffer
     * @param h {Number} New height for the framebuffer.
     */
    resize(w,h){
        let gl = this.gl;
        this.width = w;
        this.height = h;
        this.texture.resize(w,h);

        //TODO heard its cheaper to rebuild fbo vs re-attach texture , not sure which is true
        this._build();

        return this;
    },


    /**
     * Replaces the FBO's main color attachment with another texture.
     * @param texture {WebGLTexture} a WebGL texture.
     * @param width {Number} optional - the new width for the FBO.
     * @param height {Number} optional - the new height for the FBO
     * @returns {FBO}
     */
    replaceTexture(texture,{
        clear=false,
        shouldClearColor=false,
        clearColor=[0,0,0,0],
        width=512,height=512
    }={}){
        if(texture instanceof WebGLTexture){
            this.options.initialTexture = texture;
            this.width = texture.width !== undefined ? texture.width : width;
            this.height = texture.height !== undefined ? texture.height : height;


            this._build();

            if(clear){
                this.clear(shouldClearColor,clearColor[0],clearColor[1],clearColor[2],clearColor[3]);
            }

        }




        return this;
    },

    /**
     * Sets the current WebGL viewport to match the Framebuffer.
     */
    setViewport(){
        this.gl.viewport(0,0,this.width,this.height);
        return this;
    },

    /**
     * Returns the texture at the specified color attachment
     * @param attachment {Number} the index of the color attachment you want.
     * @returns {*}
     */
    getTextureAt(attachment=0){
        return this.attachments[attachment];
    },
    /**
     * For binding the Fbo to draw onto it.
     */
    bind(){

        this.gl.bindFramebuffer(this.gl.FRAMEBUFFER,this);
    },

    /**
     * Unbinds the previously bound FBO, returning drawing commands to
     * the main context Framebuffer.
     */
    unbind(){
        this.gl.bindFramebuffer(this.gl.FRAMEBUFFER,null);
    },


    /**
     * Constructs FrameBuffer based on options
     * @returns {WebGLFramebuffer}
     * @private
     */
    _build(){

        let gl = this.gl;
        let options = this.options;


        gl.bindFramebuffer(gl.FRAMEBUFFER,this);

        // =================== ATTACHMENTS ================================ //


        // --------------- COLOR ATTACHMENT 0 --------------

        // attach texture - if texture was passed in with options, use that, otherwise build a new texture.
        if(options.initialTexture){
            gl.framebufferTexture2D(gl.FRAMEBUFFER, gl.COLOR_ATTACHMENT0, gl.TEXTURE_2D, options.initialTexture, 0);
            this.texture = options.initialTexture;
        }else{

            let texture = createTexture(gl,{
                width:this.width,
                height:this.height,
                format:this.textureFormat
            });

            // store the texture so we can use it later.
            this.texture = texture;
            gl.framebufferTexture2D(gl.FRAMEBUFFER, gl.COLOR_ATTACHMENT0, gl.TEXTURE_2D, texture, 0);
        }


        // ---------------  OTHER ATTACHMENTS -------------

        if(options.attachments.length > 0){

            // any additional attachments should just be an array of textures.
            // they will get added after the first color attachment slot.
            // remember, all attachments need to be the same size.
            options.attachments.forEach((attachment,index) => {
                let slot = gl.COLOR_ATTACHMENT0 + (index + 1);
                console.log(slot,attachment)
                gl.framebufferTexture2D(gl.FRAMEBUFFER,slot, gl.TEXTURE_2D, attachment, 0);

            });
        }

        // =============== DEPTH TEXTURE ====================
        if(this.options.depthTexture){
            let texture = createTexture(gl,{
                width:this.width,
                height:this.height,
                format:this.textureFormat
            });


            texture.createDepthTexture();
            gl.framebufferTexture2D(gl.FRAMEBUFFER,gl.DEPTH_ATTACHMENT,gl.TEXTURE_2D,texture,0);
        }


        // last check of FBO status before un-bind
        let status = gl.checkFramebufferStatus(gl.FRAMEBUFFER);
        this._throwError(status);
        gl.bindFramebuffer(gl.FRAMEBUFFER,null);



        return this;

    },

    /**
     * Allows you to clear the framebuffer's texture to get rid of junk.
     * @param r
     * @param g
     * @param b
     * @param a
     */
    clear(clearcolor=false,r=0,g=0,b=0,a=0){
        this.bind();

        if(clearcolor){
            this.gl.clearColor(r,g,b,a);
        }
        this.gl.clear(this.gl.COLOR_BUFFER_BIT);

        this.unbind();

       /*
        this.bind();
        this.gl.clearColor(r,g,b,a);
        this.gl.clear(this.gl.COLOR_BUFFER_BIT|this.gl.DEPTH_BUFFER_BIT);

        this.unbind();
        */

    },

    _throwError(status){
        let gl = this.gl;
        switch(status){
            case gl.FRAMEBUFFER_UNSUPPORTED:
                throw new Error('gl-fbo: Framebuffer unsupported')
            case gl.FRAMEBUFFER_INCOMPLETE_ATTACHMENT:
                throw new Error('gl-fbo: Framebuffer incomplete attachment')
            case gl.FRAMEBUFFER_INCOMPLETE_DIMENSIONS:
                throw new Error('gl-fbo: Framebuffer incomplete dimensions')
            case gl.FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT:
                throw new Error('gl-fbo: Framebuffer incomplete missing attachment')

            case gl.FRAMEBUFFER_COMPLETE:
                return true;
            default:
                console.error("unknown error creating framebuffer")
                return false;
        }
    }
};