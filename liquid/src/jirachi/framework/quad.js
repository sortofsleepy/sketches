import Mesh from './mesh'
import vert from '../shaders/quad.vert'
import frag from '../shaders/quad.frag'

/**
 * Provides a quick way to draw a standard full-screen quad.
 */
class Quad extends Mesh{
    constructor(gl,{
        vertex=vert,
        fragment=frag,
        hasTexture=false
    }={}){
        super(gl,{
            vertex:vertex,
            fragment:hasTexture !== false ? ["#define HAS_TEXTURE",fragment].join("/") : fragment
        });


        this.hasTexture = hasTexture;

        this.addAttribute('position',[-1, -1, -1, 4, 4, -1],{
            size:2
        });



        this.resolution = [window.innerWidth,window.innerWidth]

        window.addEventListener('resize',() => {
            this.resolution = [window.innerWidth,window.innerWidth]
        })
    }

    /**
     * static method for getting vertices to render a quad.
     * @returns {*[]}
     */
    static getVertices(){
        return [-1, -1, -1, 4, 4, -1];
    }

    static getVertexShader(){
        return vert;
    }

    uniform(name,value){
        this.shader.uniform(name,value);
    }

    /**
     * Draws the quad
     * @param texture
     * @param oneTime
     */
    draw(texture = null,oneTime=false){
        let gl = this.gl;

        gl.useProgram(this.shader);

        this.vao.bind();

        if(this.hasTexture || texture !== null){
            texture.bind();
           // this.shader.setTextureUniform("inputTexture",0);
        }

        gl.drawArrays(gl.TRIANGLES,0,3);
        this.vao.unbind();

        if(this.hasTexture || texture !== null){
            texture.unbind();
        }

        if(oneTime){
            gl.deleteProgram(this.shader);
        }
    }
}

export default Quad;