import createShader from '../core/shader'
import createVBO from '../core/vbo'
import createVAO from '../core/vao'


/**
 * Provides a way to draw a basic mesh.
 */
class Mesh {
    constructor(gl,{
        vertex="",
        fragment="",
        defineStatements={},
        drawPrimitive=gl.TRIANGLES,
        shaderProgram=null
    }={}){

        this.gl = gl;
        this.name = name;
        this.vao = createVAO(gl);
        this.vertex = vertex;
        this.fragment = fragment;
        this.defines = defineStatements;
        this.indicesSet = false;
        this.primitive = drawPrimitive;

        if(vertex !== "" && fragment !== ""){
            this.shader = createShader(gl,{
                vertex:vertex,
                fragment:fragment
            })
        }

        if(shaderProgram !== null){
            this.shader = shaderProgram;
        }
    }


    /**
     * Adds an attribute to the mesh
     * @param verts
     */
    addAttribute(name,data,{
        size=3,
        dataOptions={
            type:this.gl.FLOAT,
            normalized:this.gl.FALSE,
            stride:0,
            offset:0
        }
    }={}){
        if((!data instanceof Array) || (!data instanceof ArrayBuffer)){
            return;
        }

        let gl = this.gl;

        if(data instanceof Array){
            data = new Float32Array(data);
        }

        let buffer = createVBO(gl);


        this.vao.bind();


        buffer.bind();

        buffer.bufferData(data);

        this.vao.addAttribute(this.shader,name,size)


        this.vao.unbind();

        buffer.unbind();

    }


    /**
     * Adds an index buffer to the mesh
     * @param data {Array}
     */
    addIndices(data){
        let gl = this.gl;

        let buffer = createVBO(gl,{
            indexed:true
        });


        this.vao.bind();

        buffer.bind();

        buffer.bufferData(data);

        this.vao.unbind();
        buffer.unbind();


        this.indicesSet = true;
        this.numVertices = data.length;

    }

    /**
     * Default drawing function - does some basic setup and then passes back the shader and
     * the final step in drawing a mesh.
     * @param func {Function} your drawing function. Gets passed back the shader object as well as a function
     * to run that calls either drawArrays or drawElements based on your mesh setup.
     * @param numVertices {Number} optional - the number of vertices to render. Note that when an index buffer is setup,
     * this value is already set.
     */
    draw(func,numVertices=100){
        let gl = this.gl;

        this.numVertices = this.numVertices !== undefined ? this.numVertices : numVertices

        // load shader
        this.shader.bind();

        this.vao.bind();

        gl.drawElements(gl.TRIANGLES,this.numVertices,gl.UNSIGNED_SHORT,0);
        this.vao.unbind();


    }
}

export default Mesh;