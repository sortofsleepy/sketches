export function getMouseRay(mouseX,mouseY,fov){
    return {
        x:mouseX * Math.tan(fov / 2.0) * (window.innerWidth / window.innerHeight),
        y:mouseY * Math.tan(fov / 2.0),
        z:-1
    }
}


/**
 * Retrieves the current mouse position within an element.
 * @param e {MouseEvent} the mouse event
 * @param el {Node} the DOM node you want to find the position within.
 * @param normalize {Boolean} whether or not you want the value normalized or not.
 * @returns {{x: number, y: number}}
 */
export function getMousePosition(e,el,normalize=false){
    let boundingRect = el.getBoundingClientRect();

    let x = e.clientX - boundingRect.left;
    let y = e.clientY - boundingRect.top;


    if(!normalize){
        return {
            x:x,
            y:y
        };
    }else{
        return normaalizeXY(x,y,el);
    }
}


/**
 * Normalizes the mouse position
 * @param x {Number} x value
 * @param y {Number} y value
 * @param el {Node} element within which you want to normalize mouse position
 * @returns {{x: number, y: number}}
 */
export function normaalizeXY(x,y,el){

    let normalizedX = x / el.width;
    let normalizedY = y / el.height;

    let _x = normalizedX * 2.0 - 1.0;
    let _y = (1 - normalizedY) * 2.0 - 1.0;

    return {
        x:_x,
        y:_y
    }
}