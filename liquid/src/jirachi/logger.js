/**
 * A alternative logging class that makes it possible to log messages to the console
 * and only have the error spit out once, no matter where you log.
 * This helps mitigate the sometimes annoying issue of needing to log something in a loop
 * and not have it spit out all the time.
 */
class Logger {
    constructor(maxcount=1){

        window.logs = [];

        this.maxcount = maxcount;
        this.count = 0;

        this.event = new Event("msg");
        window.addEventListener("msg",this.output.bind(this));

        window.console = this;

    }

    error(msg){

        let found = window.logs.find(itm => {
            console.log(itm);
        })

        if(found === undefined){
            logs.push({
                type:"error",
                css:"background:red;color:white; padding-left:2px; padding-right:2px;",
                msg:msg,
                count:0
            });
            this.count = 0;
            window.dispatchEvent(this.event);
        }
    }

    info(msg){

        let found = logs.find(itm => {
            return itm.msg === msg;
        });



        if(found === undefined){
            logs.push({
                type:"log",
                css:"background:red;color:white; padding-left:2px; padding-right:2px;",
                msg:msg,
                count:0
            });
            this.count = 0;
            window.dispatchEvent(this.event);
        }

    }
    warn(msg){

        let found = window.logs.find(itm => {
            return itm.msg === msg;
        })

        if(found === undefined){
            logs.push({
                type:"warn",
                css:"background:yellow;color:red; padding-left:2px; padding-right:2px;",
                msg:msg,
                count:0
            });
            this.count = 0;
            window.dispatchEvent(this.event);
        }
    }


    output(){

        if(this.count < this.maxcount){
            logs.forEach(log => {

                if(log.count < 1){
                    console[log.type](log.msg);
                    log.count += 1;
                }
            })
            this.count++;
        }
    }

}


window.log = new Logger();


export default Logger