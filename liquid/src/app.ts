import FluidSimulator from './liquid/FluidSimulator'
import {getMousePosition} from './jirachi/interaction/mouse'

window["mouse"] = {
    x:0,
    y:0,
    velocity:[0,0,0]
}
window.addEventListener("mousemove",(e) => {
    let position = getMousePosition(e,document.querySelector("canvas"),true);

    window["mouse"].x = position.x;
    window["mouse"].y = position.y;

});

new FluidSimulator();

