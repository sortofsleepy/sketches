
in vec2 vUv;
uniform sampler2D blurState;
uniform sampler2D normalState;


out vec4 glFragColor;
void main(){

    vec4 blur = texture(blurState,vUv);
    vec4 normal = texture(normalState,vUv);

    vec4 fin = mix(normal,blur,0.7);

    glFragColor = fin;



}