
uniform vec3 colorPallet[8];
out vec4 glFragColor;


in vec3 v_viewSpacePosition;
in float v_speed;
in vec3 v_velocity;


void main(){


    vec3 col = vec3(1.);

    for(int i = 0; i < 8; ++i){
        col += mix(rgb2Hsl(colorPallet[i]),rgb2Hsl(colorPallet[i + 1]),v_speed + v_velocity);
        col *= smoothstep(vec3(v_velocity),col,vec3(0.6));
        col *= v_speed;
    }

    float r = 0.0;
    float delta = 0.0;
    float alpha = 1.0;

    vec2 cxy = 2.0 * gl_PointCoord - 1.0;
    r = dot(cxy,cxy);
    if(r > 1.0){
        discard;
    }


    glFragColor = vec4(col,v_velocity);

}