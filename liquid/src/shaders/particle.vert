precision highp float;

in vec3 position;

in vec2 a_textureCoordinates;

uniform mat4 projectionMatrix;
uniform mat4 modelViewMatrix;
uniform mat4 modelMatrix;

uniform sampler2D u_positionTexture;
uniform sampler2D u_velocityTexture;
uniform float radius;

out vec3 v_viewSpacePosition;
out float v_speed;
out vec3 v_velocity;


void main () {

    vec3 spherePosition = texture(u_positionTexture, a_textureCoordinates).rgb;

    vec3 velocity = texture(u_velocityTexture, a_textureCoordinates).rgb;
    v_velocity = velocity;
    v_speed = length(velocity);


    vec3 pos;

    gl_PointSize = 0.5 * v_speed;
    if(radius != 0.0){
        pos = position + spherePosition * radius;
    }else{
        pos = position + spherePosition;
    }


    v_viewSpacePosition = vec3(modelViewMatrix * vec4(pos, 1.0));

    gl_Position = projectionMatrix * modelMatrix * vec4(v_viewSpacePosition, 1.0);

}
