precision highp float;

uniform sampler2D u_texture;

in vec2 v_coordinates;

out vec4 glFragColor;

void main () {
    glFragColor = texture(u_texture, v_coordinates);
}
