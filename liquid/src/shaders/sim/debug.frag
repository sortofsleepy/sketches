precision highp float;
uniform sampler2D uTex0;
in vec2 v_coordinates;

void main(){
    vec4 d = texture2D(uTex0,v_coordinates);

    glFragColor = d;
    glFragColor.a = 1.0;

}