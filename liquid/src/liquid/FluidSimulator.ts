import AABB from '../jirachi/math/aabb'
import createRenderer from '../jirachi/core/gl'
import Renderer from './renderer'
import Simulator from './simulator'
import {setZoom} from '../jirachi/framework/camera'

class FluidSimulator {
    gridCellDensity:number;
    gridWidth:number;
    gridHeight:number;
    gridDepth:number;
    particlesPerCell:number;
    timeStep:number;
    gl:any;
    renderer:any;
    simulator:any;
    constructor({
        gridWidth=40,
        gridHeight=20,
        gridDepth=40,
        particlesPercell=10
    }={}){

        this.gridWidth = gridWidth;
        this.gridHeight = gridHeight;
        this.gridDepth = gridDepth;
        this.particlesPerCell = particlesPercell;


        let gl = createRenderer().setFullscreen().attachToScreen();
        this.gl = gl;


        //this.simulatorRenderer = new SimulatorRenderer(gl);

        //using gridCellDensity ensures a linear relationship to particle count
        this.gridCellDensity = 0.5; //simulation grid cell density per world space unit volume
        this.timeStep = 1.0 / 60.0;


        this.simulator = new Simulator(gl);

        this.startSimulation();

        gl.canvas.width = window.innerWidth;
        gl.canvas.height = window.innerHeight;


        let animate = () => {
            requestAnimationFrame(animate);
            this.update();
        }

        animate();
    }


    getParticleCount(){

        let gridCells = this.gridWidth * this.gridHeight * this.gridDepth * this.gridCellDensity;

        //assuming x:y:z ratio of 2:1:1
        let gridResolutionY = Math.ceil(Math.pow(gridCells / 2, 1.0 / 3.0));
        let gridResolutionZ = gridResolutionY * 1;
        let gridResolutionX = gridResolutionY * 2;

        let totalGridCells = gridResolutionX * gridResolutionY * gridResolutionZ;


        let totalVolume = 0;
        let cumulativeVolume = []; //at index i, contains the total volume up to and including box i (so index 0 has volume of first box, last index has total volume)


        let box = new AABB([0, 0, 0], [15, 20, 20]);
        let volume = box.computeVolume();

        totalVolume += volume;
        cumulativeVolume[0] = totalVolume;

        let fractionFilled = totalVolume / (this.gridWidth * this.gridHeight * this.gridDepth);

        let desiredParticleCount = fractionFilled * totalGridCells * this.particlesPerCell; //theoretical number of particles


        return desiredParticleCount;
    }


    startSimulation(){

        let desiredParticleCount = this.getParticleCount(); //theoretical number of particles
        let particlesWidth = 512; //we fix particlesWidth
        let particlesHeight = Math.ceil(desiredParticleCount / particlesWidth); //then we calculate the particlesHeight that produces the closest particle count

        let particleCount = particlesWidth * particlesHeight;
        let particlePositions = [];

        let box = new AABB([0, 0, 0], [15, 20, 20]);
        let volume = box.computeVolume();


        let totalVolume = volume;


        let particlesCreatedSoFar = 0;
        let particlesInBox = 0;
        particlesInBox = Math.floor(particleCount * box.computeVolume() / totalVolume);

        for (let j = 0; j < particlesInBox; ++j) {
            let position = box.randomPoint();
            particlePositions.push(position);
        }

        particlesCreatedSoFar += particlesInBox;

        let gridCells = this.gridWidth * this.gridHeight * this.gridDepth * this.gridCellDensity;


        //assuming x:y:z ratio of 2:1:1
        let gridResolutionY = Math.ceil(Math.pow(gridCells / 2, 1.0 / 3.0));
        let gridResolutionZ = gridResolutionY * 1;
        let gridResolutionX = gridResolutionY * 2;


        let gridSize = [this.gridWidth,this.gridHeight,this.gridDepth];
        let gridResolution = [gridResolutionX, gridResolutionY, gridResolutionZ];

        let sphereRadius = 7.0 / gridResolutionX;


        this.renderer = new Renderer(this.gl,{
            particlesWidth:particlesWidth,
            particlesHeight:particlesHeight
        });
        this.simulator.setup(particlesWidth, particlesHeight, particlePositions, gridSize, gridResolution, this.particlesPerCell);

    }
    update(){
        let timeStep = 1.0 / 60.0;
        this.simulator.update(timeStep);
        this.renderer.draw(this.simulator);
    }
}

export default FluidSimulator;