// borrowed from https://github.com/spite/looper/blob/master/modules/floriandelooij.js

export default {
    pallet: [
        [32,160,170], // #20a0aa
        [236,64,57],//'#ec4039',
        [255,174,18],//'#ffae12',
        [234,34,94],//'#ea225e',
        [237,91,53],//'#ed5b35',
        [245,181,46],//'#f5b52e',
        [0,163,150],//'#00a396',
        [22,116,188]//'#1674bc',
    ],
};