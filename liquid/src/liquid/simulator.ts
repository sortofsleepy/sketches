//simulation grid dimensions and resolution
//all particles are in the world position space ([0, 0, 0], [GRID_WIDTH, GRID_HEIGHT, GRID_DEPTH])

//when doing most grid operations, we transform positions from world position space into the grid position space ([0, 0, 0], [GRID_RESOLUTION_X, GRID_RESOLUTION_Y, GRID_RESOLUTION_Z])


//in grid space, cell boundaries are simply at integer values

//we emulate 3D textures with tiled 2d textures
//so the z slices of a 3d texture are laid out along the x axis
//the 2d dimensions of a 3d texture are therefore [width * depth, height]


/*
we use a staggered MAC grid
this means the velocity grid width = grid width + 1 and velocity grid height = grid height + 1 and velocity grid depth = grid depth + 1
a scalar for cell [i, j, k] is positionally located at [i + 0.5, j + 0.5, k + 0.5]
x velocity for cell [i, j, k] is positionally located at [i, j + 0.5, k + 0.5]
y velocity for cell [i, j, k] is positionally located at [i + 0.5, j, k + 0.5]
z velocity for cell [i, j, k] is positionally located at [i + 0.5, j + 0.5, k]
*/

//the boundaries are the boundaries of the grid
//a grid cell can either be fluid, air (these are tracked by markTexture) or is a wall (implicit by position)

import createShader from '../jirachi/core/shader'
import createFBO from '../jirachi/core/fbo'
import createVBO from "../jirachi/core/vbo"
import createTexture from '../jirachi/core/texture'

// shaders
import fullScreen from '../shaders/sim/fullscreen.vert'
import common from '../shaders/sim/common.frag'
import transferToGridVert from '../shaders/sim/transferToGrid.vert'
import transferToGridFrag from '../shaders/sim/transferToGrid.frag'
import normalizeGrid from '../shaders/sim/normalizeGrid.frag'
import markv from '../shaders/sim/mark.vert'
import markf from '../shaders/sim/mark.frag'
import addForce from '../shaders/sim/addForce.frag'
import enforceBoundries from '../shaders/sim/enforceboundaries.frag'
import transferToParticles from '../shaders/sim/transferToParticles.frag'
import divergence from '../shaders/sim/divergence.frag'
import jacobi from '../shaders/sim/jacobi.frag'
import advect from '../shaders/sim/advect.frag'
import copy from '../shaders/sim/copy.frag'
import subtract from '../shaders/sim/subtract.frag'


export default class {
    gl:any;

    particlesWidth:number = 0;
    particlesHeight:number = 0;
    gridWidth:number = 0;
    gridHeight:number = 0;
    gridDepth:number = 0;
    gridResolutionX:number = 0;
    gridResolutionY:number = 0;
    gridResolutionZ:number = 0;
    particleDensity:number = 0;
    velocityTextureWidth:number = 0;
    velocityTextureHeight:number = 0;
    scalarTextureWidth:number = 0;
    scalarTextureHeight:number = 0;

    simulationNumberType:number;

    flipness:number = 0.99;
    frameNumber:number = 0;

    quadVertexBuffer:any;

    simulationFramebuffer:any;
    particleVertexBuffer:any;


    particlePositionTextureTemp:any;
    particleVelocityTexture:any;
    particleVelocityTextureTemp:any;
    particleRandomTexture:any;
    particlePositionTexture:any;

    ////////////////////////////////////////////////////
    // create simulation textures

    velocityTexture:any;
    tempVelocityTexture:any;
    originalVelocityTexture:any;
    weightTexture:any;

    markerTexture:any; //marks fluid/air, 1 if fluid, 0 if air
    divergenceTexture:any;
    pressureTexture:any;
    tempSimulationTexture:any;


    /// ============== SHADERS

    transferToGridProgram:any;
    normalizeGridProgram:any;
    markProgram:any;
    addForceProgram:any;
    enforceBoundriesProgram:any;
    transferToParticlesProgram:any;
    divergenceProgram:any;
    jacobiProgram:any;
    subtractProgram:any;
    advectProgram:any;
    copyProgram:any;

    constructor(gl){


        this.gl = gl;
        this.simulationNumberType = gl.HALF_FLOAT;


    }


    setup(particlesWidth, particlesHeight, particlePositions, gridSize, gridResolution, particleDensity){
        let gl = this.gl;

        /////////////////////////////////////////////////
        // simulation objects (most are filled in by reset)
        this.quadVertexBuffer = createVBO(gl,{
            data:[-1.0, -1.0, -1.0, 1.0, 1.0, -1.0, 1.0, 1.0]
        })

        this.simulationFramebuffer = createFBO(gl);
        this.particleVertexBuffer = createVBO(gl);



        this.particlePositionTextureTemp = gl.createTexture();
        this.particleVelocityTexture = gl.createTexture();
        this.particleVelocityTextureTemp = gl.createTexture();
        this.particleRandomTexture = gl.createTexture(); //contains a random normalized direction for each particle



        ////////////////////////////////////////////////////
        // create simulation textures

        this.velocityTexture = gl.createTexture();
        this.tempVelocityTexture = gl.createTexture();
        this.originalVelocityTexture = gl.createTexture();
        this.weightTexture = gl.createTexture();

        this.markerTexture = gl.createTexture(); //marks fluid/air, 1 if fluid, 0 if air
        this.divergenceTexture = gl.createTexture();
        this.pressureTexture = gl.createTexture();
        this.tempSimulationTexture = gl.createTexture();


        this._loadShaders();
        this._reset(particlesWidth, particlesHeight, particlePositions, gridSize, gridResolution, particleDensity)
    }

    update(timeStep){
        this.transferToGrid();
        this.normalizeGrid();
        this.markGrid();
        this.saveGrid();
        this.addForces(timeStep);
        this.enforceBoundaries();
        this.divergence();
        this.runJacobi();
        this.subtractFromGradient();
        this.transferToParticles();
        this.advect(timeStep);

    }


    transferToGrid(){
        //////////////////////////////////////////////////////
        //transfer particle velocitis to grid

        //we transfer particle velocities to the grid in two steps
        //in the first step, we accumulate weight * velocity into tempVelocityTexture and then weight into weightTexture
        //in the second step: velocityTexture = tempVelocityTexture / weightTexture

        //we accumulate into velocityWeightTexture and then divide into velocityTexture

        let gl = this.gl;

        let uniforms = this.transferToGridProgram.uniforms;

        gl.enable(gl.BLEND)
        gl.blendEquation(gl.FUNC_ADD)
        gl.blendFuncSeparate(gl.ONE, gl.ONE, gl.ONE, gl.ONE);

        //
        //accumulate weight
        gl.bindFramebuffer(gl.FRAMEBUFFER,this.simulationFramebuffer);
        gl.framebufferTexture2D(gl.FRAMEBUFFER, gl.COLOR_ATTACHMENT0, gl.TEXTURE_2D, this.weightTexture, 0);
        gl.clearColor(0,0,0,0);
        gl.clear(gl.COLOR_BUFFER_BIT);


        gl.viewport(0,0,this.velocityTextureWidth,this.velocityTextureHeight);

        //each particle gets splatted layer by layer from z - (SPLAT_SIZE - 1) / 2 to z + (SPLAT_SIZE - 1) / 2
        var SPLAT_DEPTH = 5;

        for (var z = -(SPLAT_DEPTH - 1) / 2; z <= (SPLAT_DEPTH - 1) / 2; ++z) {

            gl.useProgram(this.transferToGridProgram);
            gl.activeTexture(gl.TEXTURE0);
            gl.bindTexture(gl.TEXTURE_2D,this.particlePositionTexture);
            gl.uniform1i(uniforms["u_positionTexture"].location,0);

            gl.activeTexture(gl.TEXTURE0 + 1);
            gl.bindTexture(gl.TEXTURE_2D,this.particleVelocityTexture)
            gl.uniform1i(uniforms["u_velocityTexture"].location,1);

            gl.uniform3fv(uniforms["u_gridResolution"].location,[this.gridResolutionX,this.gridResolutionY,this.gridResolutionZ]);
            gl.uniform3fv(uniforms["u_gridSize"].location,[this.gridWidth,this.gridHeight,this.gridDepth]);


            //transferToGridDrawState.uniform1f('u_zOffset', z);
            gl.uniform1i(uniforms['u_accumulate'].location, 0)
            gl.uniform1f(uniforms['u_zOffset'].location, z);

            gl.bindBuffer(gl.ARRAY_BUFFER,this.particleVertexBuffer);
            gl.enableVertexAttribArray(0);
            gl.vertexAttribPointer(0,2,gl.FLOAT,gl.FALSE,0,0);

            gl.drawArrays(gl.POINTS,0,this.particlesWidth * this.particlesHeight);
            gl.bindBuffer(gl.ARRAY_BUFFER,null);
        }

        gl.bindFramebuffer(gl.FRAMEBUFFER,null);


        gl.bindFramebuffer(gl.FRAMEBUFFER,this.simulationFramebuffer);
        gl.framebufferTexture2D(gl.FRAMEBUFFER, gl.COLOR_ATTACHMENT0, gl.TEXTURE_2D, this.tempVelocityTexture, 0);
        gl.clearColor(0,0,0,0);
        gl.clear(gl.COLOR_BUFFER_BIT)


        for (var z = -(SPLAT_DEPTH - 1) / 2; z <= (SPLAT_DEPTH - 1) / 2; ++z) {

            gl.useProgram(this.transferToGridProgram);
            gl.activeTexture(gl.TEXTURE0);
            gl.bindTexture(gl.TEXTURE_2D,this.particlePositionTexture);
            gl.uniform1i(uniforms["u_positionTexture"].location,0);

            gl.activeTexture(gl.TEXTURE0 + 1);
            gl.bindTexture(gl.TEXTURE_2D,this.particleVelocityTexture)
            gl.uniform1i(uniforms["u_velocityTexture"].location,1);

            gl.uniform3fv(uniforms["u_gridResolution"].location,[this.gridResolutionX,this.gridResolutionY,this.gridResolutionZ]);
            gl.uniform3fv(uniforms["u_gridSize"].location,[this.gridWidth,this.gridHeight,this.gridDepth]);

            gl.uniform1i(uniforms['u_accumulate'].location, 1)
            gl.uniform1f(uniforms['u_zOffset'].location, z);


            gl.bindBuffer(gl.ARRAY_BUFFER,this.particleVertexBuffer);
            gl.enableVertexAttribArray(0);
            gl.vertexAttribPointer(0,2,gl.FLOAT,gl.FALSE,0,0);

            gl.drawArrays(gl.POINTS,0,this.particlesWidth * this.particlesHeight)


            gl.bindBuffer(gl.ARRAY_BUFFER,null);
        }

        gl.bindFramebuffer(gl.FRAMEBUFFER,null);

        gl.disable(gl.BLEND);
        gl.viewport(0,0,window.innerWidth,window.innerHeight);

    }

    normalizeGrid(){

        let gl = this.gl;
        let uniforms = this.normalizeGridProgram.uniforms;
        // normalize grid
        gl.bindFramebuffer(gl.FRAMEBUFFER,this.simulationFramebuffer);
        gl.framebufferTexture2D(gl.FRAMEBUFFER, gl.COLOR_ATTACHMENT0, gl.TEXTURE_2D, this.velocityTexture, 0);




        gl.bindBuffer(gl.ARRAY_BUFFER,this.quadVertexBuffer);
        gl.enableVertexAttribArray(0);
        gl.vertexAttribPointer(0,2,gl.FLOAT,gl.FALSE,0,0)

        gl.useProgram(this.normalizeGridProgram);
        gl.activeTexture(gl.TEXTURE0);
        gl.bindTexture(gl.TEXTURE_2D,this.weightTexture);
        gl.uniform1i(uniforms["u_weightTexture"].location,0);

        gl.activeTexture(gl.TEXTURE0 + 1);
        gl.bindTexture(gl.TEXTURE_2D,this.tempVelocityTexture);
        gl.uniform1i(uniforms["u_accumulatedVelocityTexture"].location,1);



        gl.viewport(0,0,this.velocityTextureWidth,this.velocityTextureHeight)
        gl.drawArrays(gl.TRIANGLE_STRIP,0,4);

        gl.bindBuffer(gl.ARRAY_BUFFER,null);
        gl.bindFramebuffer(gl.FRAMEBUFFER,null)
    }

    markGrid(){

        let gl = this.gl;//

        let uniforms = this.markProgram.uniforms
        //////////////////////////////////////////////////////


        //////////////////////////////////////////////////////
        // mark cells with fluid
        gl.bindFramebuffer(gl.FRAMEBUFFER,this.simulationFramebuffer);
        gl.framebufferTexture2D(gl.FRAMEBUFFER, gl.COLOR_ATTACHMENT0, gl.TEXTURE_2D, this.markerTexture, 0);
        gl.clear(gl.COLOR_BUFFER_BIT)


        gl.viewport(0,0,this.scalarTextureWidth,this.scalarTextureHeight);

        gl.bindBuffer(gl.ARRAY_BUFFER,this.particleVertexBuffer);
        gl.enableVertexAttribArray(0);
        gl.vertexAttribPointer(0,2,gl.FLOAT,false,0,0);

        gl.useProgram(this.markProgram);
        gl.uniform3f(uniforms["u_gridResolution"].location,this.gridResolutionX, this.gridResolutionY, this.gridResolutionZ)
        gl.uniform3f(uniforms["u_gridSize"].location,this.gridWidth, this.gridHeight, this.gridDepth)
        gl.activeTexture(gl.TEXTURE0);
        gl.bindTexture(gl.TEXTURE_2D,this.particlePositionTexture)
        gl.uniform1i(uniforms["u_positionTexture"].location,0);

        gl.drawArrays(gl.POINTS,0,this.particlesWidth * this.particlesHeight);
        //wgl.drawArrays(markDrawState, wgl.POINTS, 0, this.particlesWidth * this.particlesHeight);

        gl.bindBuffer(gl.ARRAY_BUFFER,null);
        gl.bindFramebuffer(gl.FRAMEBUFFER,null);
    }

    saveGrid(){
        let gl = this.gl;



        ////////////////////////////////////////////////////
        // save our original velocity grid

        //gl.framebufferTexture2D(this.simulationFramebuffer, wgl.FRAMEBUFFER, wgl.COLOR_ATTACHMENT0, wgl.TEXTURE_2D, this.originalVelocityTexture, 0);
        gl.bindFramebuffer(gl.FRAMEBUFFER,this.simulationFramebuffer);
        gl.framebufferTexture2D(gl.FRAMEBUFFER, gl.COLOR_ATTACHMENT0, gl.TEXTURE_2D, this.originalVelocityTexture, 0);



        gl.viewport(0,0,this.velocityTextureWidth,this.velocityTextureHeight);
        gl.bindBuffer(gl.ARRAY_BUFFER,this.quadVertexBuffer);
        gl.enableVertexAttribArray(0);
        gl.vertexAttribPointer(0,2,gl.FLOAT,false,0,0);

        gl.useProgram(this.copyProgram);

        gl.activeTexture(gl.TEXTURE0);
        gl.bindTexture(gl.TEXTURE_2D,this.velocityTexture);
        gl.uniform1i(this.copyProgram.uniforms["u_texture"].location,0);

        gl.drawArrays(gl.TRIANGLE_STRIP,0,4);


        gl.bindBuffer(gl.ARRAY_BUFFER,null);
        gl.bindFramebuffer(gl.FRAMEBUFFER,null);

    }

    addForces(timeStep){

        /////////////////////////////////////////////////////
        // add forces to velocity grid

        let gl = this.gl;
        let mouseVelocity = window["mouse"].velocity !== undefined ? window['mouse'].velocity : [0,0,0];
        let mouseRayOrigin = [0,0,0];
        //let mouseRayDirection = window["mouse"].worldSpaceMouseRay !== undefined ? window["mouse"].worldSpaceMouseRay : [0,0,0,0]
        let mouseRayDirection = [0,0,0]



        gl.bindFramebuffer(gl.FRAMEBUFFER,this.simulationFramebuffer);
        gl.framebufferTexture2D(gl.FRAMEBUFFER,gl.COLOR_ATTACHMENT0, gl.TEXTURE_2D, this.tempVelocityTexture, 0);




        gl.viewport(0,0,this.velocityTextureWidth,this.velocityTextureHeight);
        gl.bindBuffer(gl.ARRAY_BUFFER,this.quadVertexBuffer);
        gl.enableVertexAttribArray(0);
        gl.vertexAttribPointer(0,2,gl.FLOAT,false,0,0);


        gl.useProgram(this.addForceProgram);
        gl.activeTexture(gl.TEXTURE0);
        gl.bindTexture(gl.TEXTURE_2D,this.velocityTexture);
        gl.uniform1i(this.addForceProgram.uniforms["u_velocityTexture"].location,0);


        gl.useProgram(this.addForceProgram);
        gl.activeTexture(gl.TEXTURE0 + 1);
        gl.bindTexture(gl.TEXTURE_2D,this.particlePositionTexture);
        gl.uniform1i(this.addForceProgram.getUniformLoc("particlePositions"),1);



        gl.uniform1f(this.addForceProgram.uniforms["u_timeStep"].location,timeStep);
        gl.uniform3f(this.addForceProgram.uniforms["u_mouseVelocity"].location,mouseVelocity[0], mouseVelocity[1], mouseVelocity[2])
        gl.uniform3f(this.addForceProgram.uniforms["u_gridResolution"].location,this.gridResolutionX, this.gridResolutionY, this.gridResolutionZ);
        gl.uniform3f(this.addForceProgram.uniforms["u_gridSize"].location,this.gridWidth, this.gridHeight, this.gridDepth)
        gl.uniform3f(this.addForceProgram.uniforms["u_mouseRayOrigin"].location, mouseRayOrigin[0], mouseRayOrigin[1], mouseRayOrigin[2])
        gl.uniform3f(this.addForceProgram.uniforms["u_mouseRayDirection"].location, mouseRayDirection[0], mouseRayDirection[1], mouseRayDirection[2])




        gl.drawArrays(gl.TRIANGLE_STRIP,0,4);


        gl.bindBuffer(gl.ARRAY_BUFFER,null)
        gl.bindFramebuffer(gl.FRAMEBUFFER,null)
        this._swap(this, 'velocityTexture', 'tempVelocityTexture');


        /////////////////////////////////////////////////////
    }

    enforceBoundaries(){

        // enforce boundary velocity conditions
        let gl = this.gl;



        gl.bindFramebuffer(gl.FRAMEBUFFER,this.simulationFramebuffer);
        gl.framebufferTexture2D(gl.FRAMEBUFFER, gl.COLOR_ATTACHMENT0, gl.TEXTURE_2D, this.tempVelocityTexture, 0);



        gl.viewport(0,0,this.velocityTextureWidth,this.velocityTextureHeight);
        gl.bindBuffer(gl.ARRAY_BUFFER,this.quadVertexBuffer);
        gl.enableVertexAttribArray(0);
        gl.vertexAttribPointer(0,2,gl.FLOAT,false,0,0);



        gl.useProgram(this.enforceBoundriesProgram);
        gl.activeTexture(gl.TEXTURE0);
        gl.bindTexture(gl.TEXTURE_2D,this.velocityTexture);
        gl.uniform1i(this.enforceBoundriesProgram.uniforms["u_velocityTexture"].location,0);
        gl.uniform3f(this.enforceBoundriesProgram.uniforms["u_gridResolution"].location,this.gridResolutionX, this.gridResolutionY, this.gridResolutionZ);



        gl.drawArrays(gl.TRIANGLE_STRIP,0,4);

        gl.bindBuffer(gl.ARRAY_BUFFER,null);
        gl.bindFramebuffer(gl.FRAMEBUFFER,null)
        this._swap(this, 'velocityTexture', 'tempVelocityTexture');
    }

    divergence(){
        let gl = this.gl;

        /////////////////////////////////////////////////////
        // update velocityTexture for non divergence


        //..compute divergence for pressure projection



        gl.bindFramebuffer(gl.FRAMEBUFFER,this.simulationFramebuffer);
        gl.framebufferTexture2D(gl.FRAMEBUFFER, gl.COLOR_ATTACHMENT0, gl.TEXTURE_2D, this.divergenceTexture, 0);
        gl.clear(gl.COLOR_BUFFER_BIT);


        gl.viewport(0,0,this.scalarTextureWidth,this.scalarTextureHeight);
        gl.bindBuffer(gl.ARRAY_BUFFER,this.quadVertexBuffer);
        gl.enableVertexAttribArray(0);
        gl.vertexAttribPointer(0,2,gl.FLOAT,false,0,0);

        gl.useProgram(this.divergenceProgram)
        gl.activeTexture(gl.TEXTURE0);
        gl.bindTexture(gl.TEXTURE_2D,this.velocityTexture);
        gl.uniform1i(this.divergenceProgram.uniforms["u_velocityTexture"].location,0);

        gl.activeTexture(gl.TEXTURE1);
        gl.bindTexture(gl.TEXTURE_2D,this.markerTexture);
        gl.uniform1i(this.divergenceProgram.uniforms["u_markerTexture"].location,1);

        gl.activeTexture(gl.TEXTURE2);
        gl.bindTexture(gl.TEXTURE_2D,this.weightTexture);
        gl.uniform1i(this.divergenceProgram.uniforms["u_weightTexture"].location,2);

        gl.uniform1f(this.divergenceProgram.uniforms["u_maxDensity"].location,this.particleDensity)

        gl.uniform3f(this.divergenceProgram.uniforms["u_gridResolution"].location,this.gridResolutionX, this.gridResolutionY, this.gridResolutionZ);



        gl.drawArrays(gl.TRIANGLE_STRIP,0,4);

        gl.bindBuffer(gl.ARRAY_BUFFER,null);
        gl.bindFramebuffer(gl.FRAMEBUFFER,null)
    }

    runJacobi(){
//=====compute pressure via jacobi iteration
        let gl = this.gl;


        gl.bindFramebuffer(gl.FRAMEBUFFER,this.simulationFramebuffer);
        gl.framebufferTexture2D(gl.FRAMEBUFFER, gl.COLOR_ATTACHMENT0, gl.TEXTURE_2D, this.pressureTexture, 0);
        gl.clear(gl.COLOR_BUFFER_BIT)
        gl.bindFramebuffer(gl.FRAMEBUFFER,null);


        var PRESSURE_JACOBI_ITERATIONS = 50;
        for (var i = 0; i < PRESSURE_JACOBI_ITERATIONS; ++i) {


            gl.bindFramebuffer(gl.FRAMEBUFFER,this.simulationFramebuffer);
            gl.framebufferTexture2D(gl.FRAMEBUFFER, gl.COLOR_ATTACHMENT0, gl.TEXTURE_2D, this.tempSimulationTexture, 0);



            gl.viewport(0,0,this.scalarTextureWidth,this.scalarTextureHeight);
            gl.bindBuffer(gl.ARRAY_BUFFER,this.quadVertexBuffer);
            gl.enableVertexAttribArray(0);
            gl.vertexAttribPointer(0,2,gl.FLOAT,false,0,0);



            gl.useProgram(this.jacobiProgram);
            gl.uniform3f(this.jacobiProgram.uniforms["u_gridResolution"].location, this.gridResolutionX, this.gridResolutionY, this.gridResolutionZ)

            gl.activeTexture(gl.TEXTURE0);
            gl.bindTexture(gl.TEXTURE_2D,this.pressureTexture);
            gl.uniform1i(this.jacobiProgram.uniforms["u_pressureTexture"].location,0);

            gl.activeTexture(gl.TEXTURE1);
            gl.bindTexture(gl.TEXTURE_2D,this.divergenceTexture);
            gl.uniform1i(this.jacobiProgram.uniforms["u_divergenceTexture"].location,1);

            gl.activeTexture(gl.TEXTURE2);
            gl.bindTexture(gl.TEXTURE_2D,this.markerTexture);
            gl.uniform1i(this.jacobiProgram.uniforms["u_markerTexture"].location,2);


            gl.drawArrays(gl.TRIANGLE_STRIP,0,4);

            gl.bindBuffer(gl.ARRAY_BUFFER,null);
            gl.bindFramebuffer(gl.FRAMEBUFFER,null);
            this._swap(this, 'pressureTexture', 'tempSimulationTexture');
        }
    }

    subtractFromGradient(){

        let gl = this.gl;


        //subtract pressure gradient from velocity

        gl.bindFramebuffer(gl.FRAMEBUFFER,this.simulationFramebuffer);
        gl.framebufferTexture2D(gl.FRAMEBUFFER, gl.COLOR_ATTACHMENT0, gl.TEXTURE_2D, this.tempVelocityTexture, 0);



        gl.viewport(0,0,this.velocityTextureWidth,this.velocityTextureHeight);
        gl.bindBuffer(gl.ARRAY_BUFFER,this.quadVertexBuffer);
        gl.enableVertexAttribArray(0);
        gl.vertexAttribPointer(0,2,gl.FLOAT,false,0,0);



        gl.useProgram(this.subtractProgram);
        gl.activeTexture(gl.TEXTURE0);
        gl.bindTexture(gl.TEXTURE_2D,this.pressureTexture);
        gl.uniform1i(this.subtractProgram.uniforms["u_pressureTexture"].location,0);

        gl.activeTexture(gl.TEXTURE1);
        gl.bindTexture(gl.TEXTURE_2D,this.velocityTexture);
        gl.uniform1i(this.subtractProgram.uniforms["u_velocityTexture"].location,1);

        //gl.activeTexture(gl.TEXTURE2);
        //gl.bindTexture(gl.TEXTURE_2D,this.markerTexture);
        //gl.uniform1i(this.subtractProgram.uniformLocations["u_markerTexture"],2);

        gl.uniform3f(this.subtractProgram.uniforms["u_gridResolution"].location, this.gridResolutionX, this.gridResolutionY, this.gridResolutionZ)

        gl.drawArrays(gl.TRIANGLE_STRIP,0,4);
        gl.bindBuffer(gl.ARRAY_BUFFER,null)
        gl.bindFramebuffer(gl.FRAMEBUFFER,null);
        this._swap(this, 'velocityTexture', 'tempVelocityTexture');

    }

    transferToParticles(){

        let gl = this.gl;
        /////////////////////////////////////////////////////////////
        // transfer velocities back to particles




        gl.bindFramebuffer(gl.FRAMEBUFFER,this.simulationFramebuffer);
        gl.framebufferTexture2D(gl.FRAMEBUFFER, gl.COLOR_ATTACHMENT0, gl.TEXTURE_2D, this.particleVelocityTextureTemp, 0);


        gl.viewport(0,0,this.particlesWidth,this.particlesHeight);
        gl.bindBuffer(gl.ARRAY_BUFFER,this.quadVertexBuffer);
        gl.enableVertexAttribArray(0);
        gl.vertexAttribPointer(0,2,gl.FLOAT,false,0,0);


        gl.useProgram(this.transferToParticlesProgram);
        gl.activeTexture(gl.TEXTURE0);
        gl.bindTexture(gl.TEXTURE_2D,this.particlePositionTexture);
        gl.uniform1i(this.transferToParticlesProgram.uniforms["u_particlePositionTexture"].location,0);

        gl.activeTexture(gl.TEXTURE1);
        gl.bindTexture(gl.TEXTURE_2D,this.particleVelocityTexture);
        gl.uniform1i(this.transferToParticlesProgram.uniforms["u_particleVelocityTexture"].location,1);

        gl.activeTexture(gl.TEXTURE2);
        gl.bindTexture(gl.TEXTURE_2D,this.velocityTexture);
        gl.uniform1i(this.transferToParticlesProgram.uniforms["u_gridVelocityTexture"].location,2);

        gl.activeTexture(gl.TEXTURE3);
        gl.bindTexture(gl.TEXTURE_2D,this.originalVelocityTexture);
        gl.uniform1i(this.transferToParticlesProgram.uniforms["u_originalGridVelocityTexture"].location,3);

        gl.uniform3f(this.transferToParticlesProgram.uniforms["u_gridResolution"].location,this.gridResolutionX, this.gridResolutionY, this.gridResolutionZ)
        gl.uniform3f(this.transferToParticlesProgram.uniforms["u_gridSize"].location,this.gridWidth, this.gridHeight, this.gridDepth);
        gl.uniform1f(this.transferToParticlesProgram.uniforms["u_flipness"].location,0.99);


        gl.drawArrays(gl.TRIANGLE_STRIP,0,4);


        gl.bindBuffer(gl.ARRAY_BUFFER,null)
        gl.bindFramebuffer(gl.FRAMEBUFFER,null)

        this._swap(this, 'particleVelocityTextureTemp', 'particleVelocityTexture');
    }

    advect(timeStep){

        let gl = this.gl;

        /////////
        ///////////////////////////////////////////////
        // advect particle positions with velocity grid using RK2

        this.frameNumber += 1;

        gl.bindFramebuffer(gl.FRAMEBUFFER,this.simulationFramebuffer);
        gl.framebufferTexture2D(gl.FRAMEBUFFER, gl.COLOR_ATTACHMENT0, gl.TEXTURE_2D, this.particlePositionTextureTemp, 0);
        gl.clear(gl.COLOR_BUFFER_BIT);



        gl.viewport(0, 0, this.particlesWidth, this.particlesHeight);
        gl.bindBuffer(gl.ARRAY_BUFFER,this.quadVertexBuffer);
        gl.enableVertexAttribArray(0);//
        gl.vertexAttribPointer(0, 2, gl.FLOAT, gl.FALSE, 0, 0)

        gl.useProgram(this.advectProgram);
        gl.activeTexture(gl.TEXTURE0);
        gl.bindTexture(gl.TEXTURE_2D,this.particlePositionTexture);
        gl.uniform1i(this.advectProgram.uniforms["u_positionsTexture"].location.location,0);

        gl.activeTexture(gl.TEXTURE1);
        gl.bindTexture(gl.TEXTURE_2D,this.particleRandomTexture);
        gl.uniform1i(this.advectProgram.uniforms["u_randomsTexture"].location,1);

        gl.activeTexture(gl.TEXTURE2);
        gl.bindTexture(gl.TEXTURE_2D,this.velocityTexture);
        gl.uniform1i(this.advectProgram.uniforms["u_velocityGrid"].location,2);

        gl.uniform3f(this.advectProgram.uniforms["u_gridResolution"].location,this.gridResolutionX, this.gridResolutionY, this.gridResolutionZ);
        gl.uniform3f(this.advectProgram.uniforms["u_gridSize"].location, this.gridWidth, this.gridHeight, this.gridDepth);
        gl.uniform1f(this.advectProgram.uniforms["u_timeStep"].location,timeStep);
        gl.uniform1f(this.advectProgram.uniforms["u_frameNumber"].location,this.frameNumber);
        gl.uniform2f(this.advectProgram.uniforms["u_particlesResolution"].location,this.particlesWidth,this.particlesHeight);

        gl.drawArrays(gl.TRIANGLE_STRIP,0,4);


        gl.bindBuffer(gl.ARRAY_BUFFER,null)
        gl.bindFramebuffer(gl.FRAMEBUFFER,null)

        this._swap(this, 'particlePositionTextureTemp', 'particlePositionTexture');
    }

    // =============== PRIVATE ================ //

    _loadShaders(){
        let gl = this.gl;
        /////////////////////////////
        // load programs

        this.transferToGridProgram = createShader(gl,{
            vertex:transferToGridVert,
            fragment:[common,transferToGridFrag]
        });

        this.normalizeGridProgram = createShader(gl,{
            vertex:fullScreen,
            fragment:normalizeGrid
        });

        this.markProgram = createShader(gl,{
            vertex:markv,
            fragment:markf
        })

        this.addForceProgram = createShader(gl,{
            vertex:fullScreen,
            fragment:[common,addForce]
        })

        this.enforceBoundriesProgram = createShader(gl,{
            vertex:fullScreen,
            fragment:[common,enforceBoundries]
        })


        this.transferToParticlesProgram = createShader(gl,{
            vertex:fullScreen,
            fragment:[common,transferToParticles]
        });

        this.divergenceProgram = createShader(gl,{
            vertex:fullScreen,
            fragment:[common,divergence]
        });

        this.jacobiProgram = createShader(gl,{
            vertex:fullScreen,
            fragment:[common,jacobi]
        })

        this.subtractProgram = createShader(gl,{
            vertex:fullScreen,
            fragment:[common,subtract]
        })

        this.advectProgram = createShader(gl,{
            vertex:fullScreen,
            fragment:[common,advect]
        })

        this.copyProgram = createShader(gl,{
            vertex:fullScreen,
            fragment:copy
        });
    }

    _swap(object,a,b){
        var temp = object[a];
        object[a] = object[b];
        object[b] = temp;
    }

    _reset(particlesWidth, particlesHeight, particlePositions, gridSize, gridResolution, particleDensity) {

        this.particlesWidth = particlesWidth;
        this.particlesHeight = particlesHeight;

        this.gridWidth = gridSize[0];
        this.gridHeight = gridSize[1];
        this.gridDepth = gridSize[2];

        this.gridResolutionX = gridResolution[0];
        this.gridResolutionY = gridResolution[1];
        this.gridResolutionZ = gridResolution[2];

        this.particleDensity = particleDensity;

        this.velocityTextureWidth = (this.gridResolutionX + 1) * (this.gridResolutionZ + 1);
        this.velocityTextureHeight = (this.gridResolutionY + 1);

        this.scalarTextureWidth = this.gridResolutionX * this.gridResolutionZ;
        this.scalarTextureHeight = this.gridResolutionY;



        ///////////////////////////////////////////////////////////
        // create particle data

        var particleCount = this.particlesWidth * this.particlesHeight;

        //fill particle vertex buffer containing the relevant texture coordinates
        var particleTextureCoordinates = new Float32Array(this.particlesWidth * this.particlesHeight * 2);
        for (var y = 0; y < this.particlesHeight; ++y) {
            for (var x = 0; x < this.particlesWidth; ++x) {
                particleTextureCoordinates[(y * this.particlesWidth + x) * 2] = (x + 0.5) / this.particlesWidth;
                particleTextureCoordinates[(y * this.particlesWidth + x) * 2 + 1] = (y + 0.5) / this.particlesHeight;
            }
        }



        //generate initial particle positions amd create particle position texture for them
        var particlePositionsData = new Float32Array(this.particlesWidth * this.particlesHeight * 4);
        var particleRandoms = new Float32Array(this.particlesWidth * this.particlesHeight * 4);
        for (var i = 0; i < this.particlesWidth * this.particlesHeight; ++i) {
            particlePositionsData[i * 4] = particlePositions[i][0];
            particlePositionsData[i * 4 + 1] = particlePositions[i][1];
            particlePositionsData[i * 4 + 2] = particlePositions[i][2];
            particlePositionsData[i * 4 + 3] = 0.0;

            var theta = Math.random() * 2.0 * Math.PI;
            var u = Math.random() * 2.0 - 1.0;
            particleRandoms[i * 4] = Math.sqrt(1.0 - u * u) * Math.cos(theta);
            particleRandoms[i * 4 + 1] = Math.sqrt(1.0 - u * u) * Math.sin(theta);
            particleRandoms[i * 4 + 2] = u;
            particleRandoms[i * 4 + 3] = 0.0;
        }




        let gl = this.gl;


        this.particleVertexBuffer = createVBO(gl,{
            data:particleTextureCoordinates
        })


        this.particlePositionTexture = createTexture(gl,{
            width:this.particlesWidth,
            height:this.particlesHeight,
            data:particlePositionsData,
            format:{
                format:gl.RGBA,
                internalFormat:gl.RGBA32F,
                texelType:gl.FLOAT,
                wrapS:gl.CLAMP_TO_EDGE,
                wrapT:gl.CLAMP_TO_EDGE,
                minFilter:gl.NEAREST,
                magFilter:gl.NEAREST
            }
        });


        this.particlePositionTextureTemp = createTexture(gl,{
            width:this.particlesWidth,
            height:this.particlesHeight,
            format:{
                format:gl.RGBA,
                internalFormat:gl.RGBA32F,
                texelType:gl.FLOAT,
                wrapS:gl.CLAMP_TO_EDGE,
                wrapT:gl.CLAMP_TO_EDGE,
                minFilter:gl.NEAREST,
                magFilter:gl.NEAREST
            }
        });

        this.particleVelocityTexture = createTexture(gl,{
            width:this.particlesWidth,
            height:this.particlesHeight,
            format:{
                format:gl.RGBA,
                internalFormat:gl.RGBA32F,
                texelType:gl.FLOAT,
                wrapS:gl.CLAMP_TO_EDGE,
                wrapT:gl.CLAMP_TO_EDGE,
                minFilter:gl.NEAREST,
                magFilter:gl.NEAREST
            }
        });


        this.particleVelocityTextureTemp = createTexture(gl,{
            width:this.particlesWidth,
            height:this.particlesHeight,
            format:{
                format:gl.RGBA,
                internalFormat:gl.RGBA32F,
                texelType:gl.FLOAT,
                wrapS:gl.CLAMP_TO_EDGE,
                wrapT:gl.CLAMP_TO_EDGE,
                minFilter:gl.NEAREST,
                magFilter:gl.NEAREST
            }
        })



        this.particleRandomTexture = createTexture(gl,{
            width:this.particlesWidth,
            height:this.particlesHeight,
            data:particleRandoms,
            format:{
                format:gl.RGBA,
                internalFormat:gl.RGBA32F,
                texelType:gl.FLOAT,
                wrapS:gl.CLAMP_TO_EDGE,
                wrapT:gl.CLAMP_TO_EDGE,
                minFilter:gl.NEAREST,
                magFilter:gl.NEAREST
            }
        })


        ////////////////////////////////////////////////////
        // create simulation textures

        this.velocityTexture = createTexture(gl,{
            width:this.velocityTextureWidth,
            height:this.velocityTextureHeight,
            format:{
                format:gl.RGBA,
                internalFormat:gl.RGBA16F,
                texelType:this.simulationNumberType,
                wrapS:gl.CLAMP_TO_EDGE,
                wrapT:gl.CLAMP_TO_EDGE,
                minFilter:gl.LINEAR,
                magFilter:gl.LINEAR
            }
        })


        this.tempVelocityTexture = createTexture(gl,{
            width:this.velocityTextureWidth,
            height:this.velocityTextureHeight,
            format:{
                format:gl.RGBA,
                internalFormat:gl.RGBA16F,
                texelType:this.simulationNumberType,
                wrapS:gl.CLAMP_TO_EDGE,
                wrapT:gl.CLAMP_TO_EDGE,
                minFilter:gl.LINEAR,
                magFilter:gl.LINEAR
            }
        })

        this.originalVelocityTexture = createTexture(gl,{
            width:this.velocityTextureWidth,
            height:this.velocityTextureHeight,
            format:{
                format:gl.RGBA,
                internalFormat:gl.RGBA16F,
                texelType:this.simulationNumberType,
                wrapS:gl.CLAMP_TO_EDGE,
                wrapT:gl.CLAMP_TO_EDGE,
                minFilter:gl.LINEAR,
                magFilter:gl.LINEAR
            }
        })

        this.weightTexture = createTexture(gl,{
            width:this.velocityTextureWidth,
            height:this.velocityTextureHeight,
            format:{
                format:gl.RGBA,
                internalFormat:gl.RGBA16F,
                texelType:this.simulationNumberType,
                wrapS:gl.CLAMP_TO_EDGE,
                wrapT:gl.CLAMP_TO_EDGE,
                minFilter:gl.LINEAR,
                magFilter:gl.LINEAR
            }
        })


        this.markerTexture = createTexture(gl,{
            width:this.scalarTextureWidth,
            height:this.scalarTextureHeight,
            format:{
                format:gl.RGBA,
                internalFormat:gl.RGBA,
                texelType:gl.UNSIGNED_BYTE,
                wrapS:gl.CLAMP_TO_EDGE,
                wrapT:gl.CLAMP_TO_EDGE,
                minFilter:gl.LINEAR,
                magFilter:gl.LINEAR
            }
        })

        this.divergenceTexture = createTexture(gl,{
            width:this.scalarTextureWidth,
            height:this.scalarTextureHeight,
            format:{
                format:gl.RGBA,
                internalFormat:gl.RGBA16F,
                texelType:this.simulationNumberType,
                wrapS:gl.CLAMP_TO_EDGE,
                wrapT:gl.CLAMP_TO_EDGE,
                minFilter:gl.LINEAR,
                magFilter:gl.LINEAR
            }
        })

        this.pressureTexture = createTexture(gl,{
            width:this.scalarTextureWidth,
            height:this.scalarTextureHeight,
            format:{
                format:gl.RGBA,
                internalFormat:gl.RGBA16F,
                texelType:this.simulationNumberType,
                wrapS:gl.CLAMP_TO_EDGE,
                wrapT:gl.CLAMP_TO_EDGE,
                minFilter:gl.LINEAR,
                magFilter:gl.LINEAR
            }
        })

        this.tempSimulationTexture = createTexture(gl,{
            width:this.scalarTextureWidth,
            height:this.scalarTextureHeight,
            format:{
                format:gl.RGBA,
                internalFormat:gl.RGBA16F,
                texelType:this.simulationNumberType,
                wrapS:gl.CLAMP_TO_EDGE,
                wrapT:gl.CLAMP_TO_EDGE,
                minFilter:gl.LINEAR,
                magFilter:gl.LINEAR
            }
        });
    }
}

