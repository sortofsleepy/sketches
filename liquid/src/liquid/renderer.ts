import createVBO from '../jirachi/core/vbo'
import createShader from '../jirachi/core/shader'
import DrawState from '../jirachi/core/drawstate'
import mat4 from '../jirachi/math/mat4'
import createFBO from '../jirachi/core/fbo'
import Quad from '../jirachi/framework/quad'
import Camera, {setZoom} from '../jirachi/framework/camera'
import vert from '../shaders/particle.vert'
import frag from '../shaders/particle.frag'
import colors from './colors'
import {getMouseRay} from '../jirachi/interaction/mouse'

import composite from '../shaders/composite.frag'
import colorFuncs from '../jirachi/shaders/color.glsl'
import blurShader from '../shaders/post/blur.frag'
import bgFrag from '../shaders/bg.frag'

class ParticleScene {
    gl:any;
    debugProgram:any;
    quadVertexBuffer:any;

    camera:Camera;

    // base state
    state:any;

    // state to use for rendering
    renderState:any;

    backgroundState:any;

    // state for blurring
    blurState:any;
    blurBuffer1:any;
    blurBuffer2:any;

    compositeState:any;
    compositeBuffer:any;

    colorPallet:any;

    numParticles:number;
    particlesWidth:number;
    particlesHeight:number;
    modelMatrix:mat4;

    lastMousePlaneX:number;
    lastMousePlaneY:number;
    capture:any;

    bgQuad:any;

    constructor(gl,{
       particlesWidth=100,
       particlesHeight=100
    }={}){
        this.gl = gl;
        this.particlesWidth = particlesWidth;
        this.particlesHeight = particlesHeight;
        this.numParticles =particlesWidth * particlesHeight;
        this.modelMatrix = mat4.create();


        this.modelMatrix = mat4.translate(this.modelMatrix,this.modelMatrix,[-20,-5,-10]);

        // capture buffer to store main scene.
        this.capture = createFBO(gl,{
            width:window.innerWidth,
            height:window.innerHeight
        });

        // first blur buffer to do horizontal blur
        this.blurBuffer1 = createFBO(gl,{
            width:window.innerWidth,
            height:window.innerHeight
        })

        // second blur buffer to do vertical blur.
        this.blurBuffer2 = createFBO(gl,{
            width:window.innerWidth,
            height:window.innerHeight
        });

        // setup FBO to composite all steps
        this.compositeBuffer = createFBO(gl,{
            width:window.innerWidth,
            height:window.innerHeight
        })


        // setup color pallet and store into 1 array
        this.colorPallet = [];
        colors.pallet.forEach(val => {
            val.forEach(itm => {
                this.colorPallet.push(itm);
            })
        })



        this.camera = new Camera();
        this.camera.setupPerspective(Math.PI / 3, window.innerWidth / window.innerHeight, 0.1, 1000);
        setZoom(this.camera,-20);


        window.addEventListener('resize',() => {

            this.camera.updatePerspective({
                fov:Math.PI / 3,
                aspect:window.innerWidth / window.innerHeight,
                near:0.1,
                far:100.0
            });
        })


        this._buildDebug();
        this._buildRender();
    }
    transformDirectionByMatrix(out, v, m) {
        var x = v[0], y = v[1], z = v[2];
        out[0] = m[0] * x + m[4] * y + m[8] * z;
        out[1] = m[1] * x + m[5] * y + m[9] * z;
        out[2] = m[2] * x + m[6] * y + m[10] * z;
        out[3] = m[3] * x + m[7] * y + m[11] * z;

        return out;
    }
    setNumParticles(numParticles){
        this.numParticles = numParticles;
    }
    draw(simulator){

        //this.debugDraw(simulator);
        this.drawActual(this.camera,simulator);

    }

    drawActual(camera,simulator){

        let gl = this.gl;
        gl.clearScreen(0);


        let ray = getMouseRay(window["mouse"].x,window["mouse"].y,Math.PI / 3);
        let mousePlaneX = ray.x * -20;
        let mousePlaneY = ray.y * -20;
        let mouseVelX = mousePlaneX - this.lastMousePlaneX;
        let mouseVelY = mousePlaneY - this.lastMousePlaneY;

        if(isNaN(mouseVelX)){
            mouseVelX = 0;
        }

        if(isNaN(mouseVelY)){
            mouseVelY = 0;
        }

        this.lastMousePlaneX = mousePlaneX;
        this.lastMousePlaneY = mousePlaneY;



        window["mouse"].velocity[0] = mouseVelX;
        window["mouse"].velocity[1] = mouseVelY;

        // ========== render the main scene ============= //


        this.modelMatrix = mat4.translate(this.modelMatrix,this.modelMatrix,[20,5,0]);
        mat4.rotate(this.modelMatrix,this.modelMatrix,0.005,[0,1,0]);

        this.modelMatrix = mat4.translate(this.modelMatrix,this.modelMatrix,[-20,-5,0]);



        this.state
            .setClear()
            .uniform("projectionMatrix",camera.getProjectionMatrix())
            .uniform("modelViewMatrix",camera.getViewMatrix())
            .uniform("modelMatrix",this.modelMatrix)
            .uniform("colorPallet",this.colorPallet,"setVec3Array")
            .setTextureUniform('u_positionTexture',0,simulator.particlePositionTexture)
            .setTextureUniform('u_velocityTexture',1,simulator.particleVelocityTexture)
            .drawArrays(gl.POINTS,0,1,{
                numInstances:this.numParticles / 3
            });



        // ============= POST PROCESSING ================= //
        // draw first blur state
        this.blurState.setFramebuffer(this.blurBuffer1);
        this.blurState.setClear()
            .setTextureUniform("tex0",0,this.state.getTexture())
            .uniform("attenuation",4.5)
            .uniform("sample_offset",[1.0 / window.innerWidth,0.0])
            .drawArrays(gl.TRIANGLES,0,3);

        // draw second blur state
        this.blurState.setFramebuffer(this.blurBuffer2);
        this.blurState.setClear()
            .setTextureUniform("tex0",0,this.state.getTexture())
            .uniform("attenuation",4.5)
            .uniform("sample_offset",[0.0,1.0 / window.innerHeight])
            .drawArrays(gl.TRIANGLES,0,3);


        // draw things into composite state.
        this.compositeState.setClear()
            .setTextureUniform("blurState",0,this.blurState.getTexture())
            .setTextureUniform("normalState",1,this.state.getTexture())
            .drawArrays(gl.TRIANGLES,0,3);





        this.renderState.draw(this.compositeState.getTexture());


        /*

         */
    }


    _buildRender(){
        let gl = this.gl;//

        let particles = [];

        this.state = new DrawState(this.gl);

        this.backgroundState = new DrawState(gl);
        this.backgroundState.setShader(createShader(this.gl,{
            vertex:Quad.getVertexShader(),
            fragment:bgFrag
        }))

        this.backgroundState.setAttribute("position",createVBO(gl,{
            data:Quad.getVertices()
        }),{
            size:2
        })
        // =============== SETUP BASE STATE ====================== //

        let particleCount = this.particlesWidth * this.particlesHeight;

        //fill particle vertex buffer containing the relevant texture coordinates
        let particleTextureCoordinates = new Float32Array(this.particlesWidth * this.particlesHeight * 2);
        for (let y = 0; y < this.particlesHeight; ++y) {
            for (let x = 0; x < this.particlesWidth; ++x) {
                particleTextureCoordinates[(y * this.particlesWidth + x) * 2] = (x + 0.5) / this.particlesWidth;
                particleTextureCoordinates[(y * this.particlesWidth + x) * 2 + 1] = (y + 0.5) / this.particlesHeight;
            }
        }


        this.state.setShader( createShader(this.gl,{
            vertex:vert,
            fragment:[colorFuncs,frag]
        }));


        let rands = [];
        for(let i = 0; i < particleCount; ++i){
            particles.push(Math.random() );
            rands.push(Math.random() * 20.0)
        }

        this.state.setAttribute('position',createVBO(this.gl,{data:particles}));

        this.state.setInstancedAttribute("a_textureCoordinates",createVBO(gl,{
            //data:rands
            data:particleTextureCoordinates,
            size:2
        }));

        this.state.setFramebuffer(this.capture);


        this.numParticles = particleTextureCoordinates.length;
        // ================= SETUP RENDER STATE ================== //
        this.blurState = new DrawState(gl);

        this.blurState.setShader(createShader(gl,{
            vertex:Quad.getVertexShader(),
            fragment:blurShader
        }));

        this.blurState.setAttribute("position",createVBO(gl,{
            data:Quad.getVertices()
        }),{
            size:2
        })
        // ================= SETUP COMPOSITE AND RENDER STATE ================== //

        this.compositeState = new DrawState(gl);
        this.compositeState.setShader(createShader(gl,{
            vertex:Quad.getVertexShader(),
            fragment:composite
        }));

        this.compositeState.setAttribute('position',createVBO(gl,{
            data:Quad.getVertices()
        }),{
            size:2
        });

        this.compositeState.setFramebuffer(this.compositeBuffer);

        this.renderState = new Quad(gl,{
            hasTexture:true
        })

    }

    // =========== build renderable shape ============= //
    _buildRenderShape(){

    }


    // ========== debuging stuff ================= //
    debugDraw(simulator){
        let gl = this.gl;//

        /////////////////////////////////////////////
        // draw particles


        //let projectionViewMatrix = Utilities.premultiplyMatrix(new Float32Array(16), viewMatrix, projectionMatrix);



        gl.viewport(0,0,window.innerWidth,window.innerHeight)
        gl.useProgram(this.debugProgram)
        gl.activeTexture(gl.TEXTURE0)

        gl.bindTexture(gl.TEXTURE_2D,simulator.tempVelocityTexture);
        gl.uniform1i(this.debugProgram.uniforms["uTex0"].location,0);

        gl.bindBuffer(gl.ARRAY_BUFFER,this.quadVertexBuffer);
        gl.enableVertexAttribArray(0);
        gl.vertexAttribPointer(0,2,gl.FLOAT,gl.FALSE,0,0)

        gl.drawArrays(gl.TRIANGLE_STRIP,0,4);

        gl.bindBuffer(gl.ARRAY_BUFFER,null);


    }



    _buildDebug(){
        let gl = this.gl;
        this.quadVertexBuffer = createVBO(gl,{
            data:[-1.0, -1.0, -1.0, 1.0, 1.0, -1.0, 1.0, 1.0]
        })

        this.debugProgram = createShader(gl,{
            vertex:`
        precision highp float;

        in vec2 a_position;

        out vec2 v_coordinates;
    

        void main () {
            v_coordinates = a_position * 0.5 + 0.5;

            gl_Position = vec4(a_position, 0.0, 1.0);
        }`,

            fragment:`
        precision highp float;
        uniform sampler2D uTex0;
        in vec2 v_coordinates;
        out vec4 glFragColor;
        void main(){
            vec4 d = texture(uTex0,v_coordinates);

            glFragColor = d;
            glFragColor.a = 1.0;
    
        }`

        })
    }
}

export default ParticleScene;