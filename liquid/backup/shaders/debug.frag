precision highp float;
uniform sampler2D uTex0;
varying vec2 v_coordinates;

void main(){
    vec4 d = texture2D(uTex0,v_coordinates);

    gl_FragColor = d;
    gl_FragColor.a = 1.0;

}