var path = require("path")
var webpack = require("webpack")

module.exports = {
    entry:[],
    output:{
        path:__dirname + "/public/js",
        filename:"app.js"
    },
    module:{
        rules:[
            {
                test:/\.js$/,
                exclude:/(node_modules|bower_components)/,
                loader:'babel-loader',
                options:{
                    presets:['babel-preset-es2015']
                }
            },

            { test: /\.(glsl|frag|vert)$/, loader: 'raw-loader', exclude: /node_modules/ }
            //{ test: /\.(glsl|frag|vert)$/, loader: 'glslify', exclude: /node_modules/ }
        ]
    },
    plugins:[]
}