import {PerspectiveCamera,setZoom,fullscreenAspectRatio,updateProjection} from './jirachi/framework/Camera'
import {createRenderer} from './jirachi/core/gl'
import Letters from './Letters'

var gl = createRenderer();
gl.attachToScreen()
    .setFullscreen();

var camera = PerspectiveCamera(Math.PI / 4,fullscreenAspectRatio(),0.1,10000);
camera = setZoom(camera,-70.0);
var letters = new Letters(gl);

class App {
    constructor() {

    }

    render(){
        gl.clearScreen();
        camera = updateProjection(camera);
        if(letters.ready){
            letters.update();
            letters.draw(camera);
        }
    }
}

export default App;