import Mesh from './jirachi/framework/Mesh'
import GPU from './GPU'
// rendering shaders
import vert from './shaders/type.vert'
import frag from './shaders/type.frag'

// simulation shader for transform feedback
import sim from './shaders/sim.vert'
import noise from './shaders/noise2d.glsl'
class Letters extends Mesh{
    constructor(gl){
        super(gl,{
            vertex:vert,
            fragment:frag
        });

        this.buffer = new GPU(gl);
        this.buffer.setShader([noise,sim],[
            "outpos"
        ])


        this.font = null;
        this.ready = false;
        this._buildData();

        this.count = 0;

    }

    draw(camera){

        let gl = this.gl;

        this.shader.bind();

        // bind default uniforms
        this.shader.set4x4Uniform('projectionMatrix',camera.projection);
        this.shader.set4x4Uniform('viewMatrix',camera.view);
        this.shader.uniform("viewMatrix",camera.view);
        this.shader.uniform("view",camera.view);
        this.shader.uniform("modelMatrix",this.model);
        this.shader.uniform("model",this.model);

        gl.disable(gl.DEPTH_TEST);
        gl.enable(gl.BLEND);
        gl.blendEquationSeparate( gl.FUNC_ADD, gl.FUNC_ADD );
        gl.blendFuncSeparate( gl.ONE, gl.ONE, gl.ONE, gl.ONE );


        this.buffer.bind();
        this.gl.drawArrays(this.gl.POINTS,0,this.num);
        this.buffer.unbind();


    }


    /**
     * Setup callback for when the typography is loaded and the
     * mesh has been created.//
     * @param cb
     */
    onload(cb){
        this.onLetterLoaded = cb;
    }

    _buildData(){

        var loader = new THREE.FontLoader();
        loader.load('/js/droid_sans_bold.typeface.json',res => {
            this._postLoad(res);
        })

    }

    update(){
        let gl = this.gl;
        this.count += 0.02;
        this.buffer.update(this.count);
    }

    /**
     * Builds rendering geometry
     * @param geo the THREE.TypeGeometry object
     * @private
     */
    _buildRenderGeometry(data){

        // using points
        this.mode = this.gl.POINTS;
        this.ready = true;

        this.addAttribute('position',data,4);

        //this.pos = this.buffer.getAttribute('position-1');
        // call the callback if set
        if(this.onLetterLoaded !== undefined){
            this.onLetterLoaded();
        }
    }

    _postLoad(font){
        var width = 1024;
        var height = 1024;
        // Start creation of typography geometry
        var textGeo = new THREE.TextGeometry( "WebGL 2", {
            font: font,
            size: 1.0,
            height: 0.25,
            curveSegments: 0,
            bevelThickness: 2,
            bevelSize: 1.4,
            weight:"bold"
        });
        textGeo.computeBoundingBox();
        var bounds = textGeo.boundingBox;
        textGeo.applyMatrix( new THREE.Matrix4().makeTranslation(
            (bounds.max.x - bounds.min.x) * -0.5,
            (bounds.max.y - bounds.min.y) * -0.5,
            (bounds.max.z - bounds.min.z) * -0.5));

        // grab random points in the geometry
        var points = THREE.GeometryUtils.randomPointsInGeometry( textGeo, width * height );
        var n = 800, n2 = n/2;	// triangles spread in the cube
        var data = new Float32Array( width * height * 4 );
        for ( var i = 0, j = 0, l = data.length; i < l; i += 4, j += 1 ) {
            data[ i ] = points[j].x
            data[ i + 1 ] = points[ j ].y;
            data[ i + 2 ] = points[ j ].z;
            data[ i + 3 ] = 0.0;
        }

        this.buffer.addAttribute(data,4,{
            stride:16
        });

        this.buffer.addAttribute(data,4,{
            stride:16
        });

        this.buffer.setNumItems(data.length / 4)


        this.num = data.length / 4;
        this._buildRenderGeometry(data);


    }

}

export default Letters;