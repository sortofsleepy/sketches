
layout(location = 0) in vec4 position;
layout(location = 1) in vec4 origin;

uniform float uTime;
out vec4 outpos;
 float rand(vec2 co){
        float frac = snoise(co);
      return fract(sin(dot(co.xy ,vec2(12.9898,78.233))) * 43758.5453) * frac;
    }

    vec4 runSimulation(vec4 pos) {
      float x = pos.x + uTime;
      float y = pos.y;
      float z = pos.z;

      if (pos.w < 0.001 && pos.w > -0.001) {
        pos.x += sin( y * 4.0 ) * cos( z * 11.0 ) * 0.04;
        pos.y += sin( x * 5.0 ) * cos( z * 13.0 ) * 0.04;
        pos.z += sin( x * 7.0 ) * cos( y * 17.0 ) * 0.004;
      } else {
        pos.y -= pos.w;
        pos.w += 0.005;
        if (pos.y < -2.0) {
          pos.y += pos.w;
          pos.w *= -0.3;
        }
      }


      return pos;
    }

void main(){
     vec4 pos = position;
     if ( rand(position.xy + uTime) > 0.47 ) {
       pos = vec4(origin.xyz, 0.0);
     } else {
       pos = runSimulation(pos);
     }

     outpos = pos;
}