precision highp float;
out vec4 glFragColor;
// transform feedback doesn't use the fragment shader but we need one anyways.
void main(){
    glFragColor = vec4(1.);
}