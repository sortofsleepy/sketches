#version 300 es

uniform mat4 projectionMatrix;
uniform mat4 viewMatrix;


layout(location = 0) in vec3 position;
out vec3 vPosition;


void main(){
    vPosition = position;
    vec3 pos = position;

    pos.x *= 15.0;
    pos.y *= 15.0;
    pos.z *= 15.0;

    gl_PointSize = 1.0;
    gl_Position = projectionMatrix * viewMatrix  * vec4(pos,1.);
}