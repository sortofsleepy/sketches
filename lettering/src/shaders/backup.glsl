
precision highp float;

layout(location = 0) in vec4 position;
layout(location = 1) in vec4 origin;

uniform float uTime;
out vec4 outpos;

float rand(vec2 co){
    float frac = snoise(co);
    frac = snoise(vec2(frac));
    return fract(sin(dot(co.xy ,vec2(12.9898,78.233))) * 43758.5453) * frac;
}

vec4 runSimulation(vec4 pos) {
  float x = pos.x + uTime * 5.0;
  float y = pos.y;
  float z = pos.z;
  if (pos.w < 0.001 && pos.w > -0.001) {

  	pos.x += sin( y * 0.033 ) * cos( z * 0.037 ) * 0.4;
    pos.y += sin( x * 0.035 ) * cos( x * 0.035 ) * 0.4;
  	pos.z += sin( x * 0.037 ) * cos( y * 0.033 ) * 0.4;

  } else {
    pos.y -= pos.w;
    pos.w += 0.05;
    if (pos.y < -2.0) {
      pos.y += pos.w;
     pos.w -= 3.0;
    }
  }
  return pos;
}

void main(){
     vec4 pos = position;
     if ( rand(position.xy + uTime) > 0.17) {
       pos = vec4(origin.xyz,0.0);
     } else {
       pos = runSimulation(pos);
     }

     outpos = pos;
  }