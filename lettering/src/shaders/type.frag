#version 300 es
precision highp float;
in vec3 vPosition;

out vec4 glFragColor;
void main(){
   float depth = smoothstep( 10.24, 0.5, gl_FragCoord.z / gl_FragCoord.w );
    glFragColor = vec4( (vec3(0.0, 0.03, 0.05) + (vPosition * 0.25)), depth );
    //gl_FragColor = vec4(1.0,1.0,0.0,1.0);
 }