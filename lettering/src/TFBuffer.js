import {createShader} from './jirachi/core/shader'
import {createVBO} from './jirachi/core/vbo'
import {createVAO} from './jirachi/core/vao'

/**
 * A wrapper for doing transform feedback powered things by bouncing
 * data between two VBOs.
 *
 * Assumes un-interleaved attributes will be used.
 * This is largely based off of this example
 * https://github.com/WebGLSamples/WebGL2Samples/blob/master/samples/transform_feedback_separated_2.html
 */
class TFBuffer {
    constructor(gl){
        this.gl = gl;
        this.count = 0;
        this.tf = gl.createTransformFeedback();

        this.vaos = [createVAO(gl),createVAO(gl)]
        this.mode = gl.SEPARATE_ATTRIBS;
        this.varyings = [];
        this.attributes = [];

        // the 2 WebGLBuffer objects to bounce stuff between
        this.buffers = new Array(2);

        // this keeps track of the number of items to update.
        this.numItems = 100;

        // flag to help with switching
        this.flag = 0;
    }

    /**
     * Adds a varying to the varying stack.
     * @param varyings can be a string or an array of strings
     */
    addVarying(varyings){
        if(varyings instanceof Array){
            this.varyings = [...this.varyings,...varyings];
        }else{
            this.varyings.push(varyings);
        }
    }

    /**
     * Retrieve an attribute buffer from the stack.
     * @param name The name of the buffer you want to grab. Assumes that you've set a name for the buffer in question.
     * If not, bad things will happen.
     * @returns {WebGLBuffer}
     */
    getAttribute(name){
        let found = false;
        for(var i = 0; i < 2; ++i) {
            for (var j = 0; j < this.numAttributes; ++j) {

                if(this.buffers[i][j].name === name){
                    found = this.buffers[i][j]
                }
            }
        }

        return found;
    }

    /**
     * Adds attributes to keep track of during the run stage.
     * @param data
     */
    addAttribute(data,size=3,{name="",datatype=FLOAT,normalized=false,stride=0,offset=0}={}){

        let gl = this.gl;
        let buffer = createVBO(gl);

        buffer.fill(data);

        this.attributes.push({
            vbo:buffer,
            index:this.attributes.length,
            size:size,
            type:datatype,
            normalized:normalized,
            stride:stride,
            offset:offset,
            name:name
        })

        this.numAttributes = this.attributes.length;

        // re-adjust the number of WebGLBuffer objects every time we add a new attribute
        // and set the data
        for(var i = 0; i < 2; ++i){
            this.buffers[i] = new Array(this.numAttributes);
            for(var j = 0; j < this.numAttributes;++j){
                this.buffers[i][j] = gl.createBuffer();
                this.buffers[i][j].name = `${this.attributes[j].name}`;

                // buffer data onto the destination buffers
                gl.bindBuffer(gl.ARRAY_BUFFER,this.buffers[i][j]);
                gl.bufferData(gl.ARRAY_BUFFER,this.attributes[j].vbo.data,gl.STATIC_DRAW);
                gl.bindBuffer(gl.ARRAY_BUFFER,null);
            }
        }
        console.log(this.buffers);

    }

    setNumItems(num=100){
        this.numItems = num;
    }


    /**
     * Sets the shader for the Transform Feedback process.
     * @param vertex {String} the source for the vertex shader
     * @param fragment {String} the source for the fragment shader
     * @param varyings {String or Array} optional - any varyings to add to the step
     * @returns {boolean}
     */
    setShader(vertex=null,varyings=null,uniforms=[]){
        if(vertex === null && fragment === null){
            console.warn("Transform Feedback buffer does not have shaders");
            return false;
        }

        let dUniforms = [
            'uTime'
        ];

        dUniforms = [...dUniforms,...uniforms];


        if(varyings !== null){
            this.addVarying(varyings);
        }

        let vertexShader = "";

        /**
         * If shader source is passed in as an array,
         * join the strings.
         */
        if(vertex instanceof Array){
            vertexShader = [...vertex].join("");
        }else{
            vertexShader = vertex;
        }



        // Note that transform feedback doesn't utilize a fragment shader so we have a passthru essentially
        // instead.
        this.shader = createShader(this.gl,{
            vertex:[
                "#version 300 es\n",
                vertexShader],
            fragment:[
                '#version 300 es\n',
                'precision highp float;',
                'out vec4 glFragColor;',
                'void main(){',
                'glFragColor = vec4(1.);',
                '}'
            ].join(""),
            uniforms:dUniforms
        },{
            varyings:this.varyings,
            mode:this.mode
        });

    }


    bind(){
        this.vaos[(this.flag + 1) % 2].bind();
    }

    unbind(){
        this.vaos[(this.flag + 1) % 2].unbind();
    }

    _setupVertexAttribs(vao,vbo){
        let gl = this.gl;

        let numAttribs = this.attributes.length;
        vao.bind();
        for(var i = 0; i < numAttribs; ++i){
            let attrib = this.attributes[i];

            gl.bindBuffer(gl.ARRAY_BUFFER,vbo[i]);
            gl.vertexAttribPointer(i,attrib.size,attrib.type,attrib.normalized,attrib.stride,attrib.offset);
            gl.enableVertexAttribArray(i)
        }

    }

    bindConstantBuffer(buff,idx,size,{datatype=FLOAT,normalized=false,stride=0,offset=0}={}){
        let gl = this.gl
        gl.bindBuffer(gl.ARRAY_BUFFER,buff);
        gl.vertexAttribPointer(idx,size,datatype,normalized,stride,offset);
        gl.enableVertexAttribArray(idx);
        gl.bindBuffer(gl.ARRAY_BUFFER,null);
    }

    _bindBufferBase(){
        let gl = this.gl;
        let destinationVBO = this.buffers[(this.flag + 1) % 2];

        for(var i = 0; i < this.numAttributes;++i){
            gl.bindBufferBase(gl.TRANSFORM_FEEDBACK_BUFFER,i,destinationVBO[i]);
        }
    }

    _unbindBufferBase(){
        let gl = this.gl;

        for(var i = 0; i < this.numAttributes;++i){
            gl.bindBufferBase(gl.TRANSFORM_FEEDBACK_BUFFER,i,null);
        }
    }

    run(time=0.0,cb=null){
        let gl = this.gl;
        let tf = this.tf;
        let sourceVAO = this.vaos[this.flag];
        let sourceVBO = this.buffers[this.flag];



        this.shader.bind();

        this.shader.uniform('uTime',time);


        this._setupVertexAttribs(sourceVAO,sourceVBO);


        // run a callback so we can inject other uniforms
        if(cb !== null){
            cb(this.shader);
        }


        gl.bindTransformFeedback(gl.TRANSFORM_FEEDBACK,tf);
       this._bindBufferBase();

        gl.enable(gl.RASTERIZER_DISCARD);

        gl.beginTransformFeedback(gl.POINTS);
        gl.drawArrays(gl.POINTS,0,this.numItems);
        gl.endTransformFeedback();


        gl.disable(gl.RASTERIZER_DISCARD);
        this._unbindBufferBase();


        /*
         this._unbindBufferBase();

         sourceVAO.unbind();
         gl.useProgram(null);
         gl.bindBuffer(gl.ARRAY_BUFFER, null);
         gl.disable(gl.RASTERIZER_DISCARD);
         gl.bindTransformFeedback(gl.TRANSFORM_FEEDBACK,null);
         */

        this.flag =  (this.flag + 1) % 2;
    }
}

export default TFBuffer;