/**
 * Returns an array of common uniform locations that might
 * need to be looked up.
 * @returns {[string,string,string,string]}
 */
export function defaultUniforms(){
    return [
        "projectionMatrix",
        "modelMatrix",
        "modelViewMatrix",
        "viewMatrix",
        "time",
        "uTime"
    ]
}

//TODO flush out UBO interaction when WebGL2 hits officially

export function bindUBO(gl,shader,name){
    gl.bindBuffer(gl.UNIFORM_BUFFER,shader.getUniform(name));
}

export function unbindUBO(gl){
    gl.bindBuffer(gl.UNIFORM_BUFFER,null);
}