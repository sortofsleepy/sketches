window.jraDebug = false;
window.toggleDebug = function(){
    window.jraDebug = !window.jraDebug;
}


/**
 * Logs an error message, only when window.jraDebug is set to true;
 * @param message
 */
export function logError(message,renderImmediate){
    let css = "background:red;color:white; padding-left:2px; padding-right:2px;";
    if(window.jraDebug || renderImmediate){
        console.log(`%c ${message}`,css);
    }
}

/**
 * Checks the context to ensure it has the desired extension enabled
 * @param ctx {WebGLRenderingContext} the webgl context to check
 * @param extension {String} the name of the extension to look for
 */
export function checkExtension(ctx,extension){
    if(ctx.getExtension(extension)){
        return true;
    }else{
        return false;
    }
}

/**
 * Logs a warning message, only when window.jraDebug is set to true
 * @param message
 */
export function logWarn(message,renderImmediate){
    let css = "background:yellow;color:red; padding-left:2px; padding-right:2px;";
    if(window.jraDebug || renderImmediate){
        console.log(`%c ${message}`,css);
    }
}

/**
 * Logs a regular console.log call, only when window.jraDebug is set to true
 * @param message
 */
export function log(message,renderImmediate){
    let css = "background:#46A6B2;color:#296169;padding-left:2px; padding-right:2px;";
    if(window.jraDebug || renderImmediate){
        console.log(`%c ${message}`,css);
    }
}
