import {createRenderer} from './core/gl';
import {logError,logWarn} from './utils';
import {makeShader,compileShader} from './core/shader'
import {createTexture2d} from './core/texture'
import {createVBO} from './core/vbo'
import {createFBO,createFBOWithAttachments} from './core/fbo'
import {createVAO} from './core/vao'

// core shaders
import passthru from './shaders/passthru.glsl'
import passthru_frag from './shaders/passthru-frag.glsl'
import noise3d from './shaders/noise3d.glsl'
import noise4d from './shaders/noise4d.glsl'

var exps = {
    logError:logError,
    logWarn:logWarn,
    createRenderer:createRenderer,
    makeShader:makeShader,
    compileShader:compileShader,
    createTexture2d:createTexture2d,
    createVBO:createVBO,
    createVAO:createVAO,
    createFBO:createFBO,
    createFBOWithAttachments:createFBOWithAttachments,
    // shaders
    passthruVertex:passthru,
    passthruFragment:passthru_frag,
    noise3d:noise3d,
    noise4d:noise4d
}

export default exps;