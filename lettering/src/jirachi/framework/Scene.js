

class Scene {
    constructor(gl){
        this.gl = gl;
        this.children = [];
    }

    add(child){
        this.children.push(child);
    }
}

export default Scene;