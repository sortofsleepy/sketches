import {createVBO} from '../core/vbo'
import {createVAO} from '../core/vao'
import mat4 from '../math/mat4'
import vec3 from '../math/vec3'
import {createShader} from '../core/shader'
import {defaultUniforms} from '../core/uniforms'

class Mesh {
    constructor(gl,{vertex="",fragment="",uniforms=[],mode=""}={}){
        this.vao = createVAO(gl);
        this.gl = gl;
        this.model = mat4.create();
        this.rotation = {
            x:0,
            y:0,
            z:0
        };
        this.attributes = {};
        this.numVertices = 3;
        this.mode = mode !== "" ? mode : gl.TRIANGLES;

        // for instanced drawing make sure at least 1 instance is set.
        this.setNumInstances();

        this.rotateAxis = vec3.create();
        vec3.set(this.rotateAxis,this.rotation.x,this.rotation.y,this.rotation.z);
        this.scale = vec3.create();
        this.position = vec3.create();

        /**
         * If vertex and fragment attributes are set,  build the shader.
         */
        if(vertex !== "" && fragment !== ""){
            this.setShader({
                vertex:vertex,
                fragment:fragment,
                uniforms:uniforms
            });
        }
    }

    /**
     * Sets shader for the mesh
     * @param vertex {String} vertex shader
     * @param fragment {String} fragment shader
     * @param uniforms {Array} any uniforms to save locations for
     * @param precision {String} the floating point precision to use. By default it's highp
     */
    setShader({vertex="",fragment="",uniforms=[],precision="highp"}={}){
        if(vertex !== "" && fragment !== ""){
            let defaults = defaultUniforms();


           if(vertex instanceof Array){
               // when using glslify, seems like we need to pre-call join twice for some reason even
               // though the end result is the same code(createShader should automatically do this though). ¯\_(ツ)_/¯
               // get rid of the glslify statements if there
                vertex = vertex.map(itm => {
                    if(itm.search("GLSLIFY") !== -1){
                        let lines = itm.split("\n");
                        lines.splice(0,1);
                        return lines.join("\n");
                    }else{
                        return itm;
                    }

                }).join("")

            }

            if(fragment instanceof Array){
                fragment = fragment.map(itm => {
                    if(itm.search("GLSLIFY") !== -1){
                        let lines = itm.split("\n");
                        lines.splice(0,1);

                        // add floating point precision to the start
                        lines.unshift(`precision ${precision} float;`)
                        return lines.join("\n\n")
                    }else{
                        return itm;
                    }
                }).join("");
            }

            this.shader = createShader(this.gl,{
                vertex:vertex,
                fragment:fragment,
                uniforms:[
                    ...defaults,
                    ...uniforms
                ]
            });


            this.shaderSet = true;
        }
    }

    /**
     * Adds an attribute to the mesh
     * @param name name of the attribute in the shader
     * @param data data of the attribute. Can be regular or TypedArray
     * @param dataSize the size of each component for the attribute. It's assumed that
     * each component falls in line with the normal xyz schema so it's set to 3.
     * @returns {Mesh}
     */
    addAttribute(name,data,size=3,dataOptions={}){
        if(this.shaderSet){

            let gl = this.gl;
            let buffer = createVBO(gl);

            this.vao.bind();
            buffer.bind();

            buffer.bufferData(data);
            this.vao.addAttribute(this.shader,name,{
                size:size,
                dataOptions:dataOptions
            });
            buffer.unbind();
            this.vao.unbind();

            this.attributes[name] = buffer;

            return this;
        }

    }

    /**
     * Adds an attribute in the form of a pre-built VBO
     * @param name the name of the attribute on the shader
     * @param buffer {Object} a VBO object created using createVBO
     * @returns {Mesh}
     */
    addAttributeBuffer(name,buffer){
        if(this.shaderSet){

            let gl = this.gl;
            this.vao.bind();
            buffer.bind();
            this.vao.addAttribute(this.shader,name,{
                size:size,
                dataOptions:dataOptions
            });
            buffer.unbind();
            this.vao.unbind();

            this.attributes[name] = buffer;

            return this;
        }
    }

    updateRawBuffer(name,buffer,{size=3,dataOptions}={}){
        let gl = this.gl;
        this.vao.bind();
        gl.bindBuffer(gl.ARRAY_BUFFER,buffer);
        this.vao.addAttribute(this.shader,name,{
            size:size,
            dataOptions:dataOptions
        });
        //this.vao.makeInstancedAttribute(name);
        gl.bindBuffer(gl.ARRAY_BUFFER,null);
        this.vao.unbind();
    }

    addInstancedAttributeBuffer(name,buffer,{size=3,dataOptions}={}){
        if(this.shaderSet){
            let gl = this.gl;
            if(buffer instanceof WebGLBuffer){

                this.vao.bind();
                gl.bindBuffer(gl.ARRAY_BUFFER,buffer);
                this.vao.addAttribute(this.shader,name,{
                    size:size,
                    dataOptions:dataOptions
                });
                this.vao.makeInstancedAttribute(name);
                gl.bindBuffer(gl.ARRAY_BUFFER,null);
                this.vao.unbind();

                this.attributes[name] = buffer;

            }else{
                this.vao.bind();
                buffer.bind();
                this.vao.addAttribute(this.shader,name,{
                    size:size,
                    dataOptions:dataOptions
                });
                this.vao.makeInstancedAttribute(name);
                buffer.unbind();
                this.vao.unbind();

                this.attributes[name] = buffer;

            }


            return this;
        }
    }

    /**
     * Adds an attribute but also makes it instanced
     * @param name the name of the attribute
     * @param data data for the attribute
     * @returns {Mesh}
     */
    addInstancedAttribute(name,data,size=3,dataOptions={}){
        if(this.shaderSet){

            let gl = this.gl;
            let buffer = createVBO(gl);

            this.vao.bind();
            buffer.bind();

            buffer.bufferData(data);
            this.vao.addAttribute(this.shader,name,{
                size:size,
                dataOptions:dataOptions
            });
            this.vao.makeInstancedAttribute(name);
            buffer.unbind();
            this.vao.unbind();

            this.attributes[name] = buffer;

            this.instanced = true;
            return this;
        }
    }

    /**
     * Updates data for an attribute
     * @param name {String} the name of the attribute
     * @param data {Array} the new dataset to use.
     */
    updateAttribute(name,data){
        let buffer = this.attributes[name];
        this.vao.bind();
        buffer.bind();
        buffer.updateBuffer(data);
        buffer.unbind();
        this.vao.unbind();
    }

    /**
     * Sets the number of vertices to utilize while drawing.
     * This is only for things where you are using gl.drawArrays and may mess things up
     * if you call this on a mesh with indices. This function operates under the assumption that the
     * value you pass in is the total number of items in your position(s) array and not the actual number of vertices.
     * This function divides by a divisor to figure that out
     *
     * @param num the total number of vertices in your mesh. Will divide by 3 automatically
     * as long as the value you input is greater than 10.
     *
     * Sets the numVertices attribute of the mesh which can be used in gl.drawArrays
     * @param divisor {Number} the number used to figure out how many vertices are in a mesh.
     */
    setNumVertices(num,divisor=3){
        if(!this.indicesSet){

            //TODO it feels like there has to be a better way of doing this and ensuring that we need to divide
            if(num > 10){
                num = num / divisor;
            }
            this.numVertices = num;
        }


        return num;
    }

    /**
     * Adds an indices buffer to the mesh. The number of elements to use while drawing is
     * automatically inferred by the data length
     *
     * @param data Data for the indices. Can be a regular or Typed array.
     * @returns {Mesh}
     */
    addIndices(data){
        if(this.shaderSet){
            let gl = this.gl;
            let buffer = createVBO(gl,{
                indexed:true
            });

            this.vao.bind();
            buffer.bind();
            buffer.bufferData(data);

            this.vao.unbind();
            buffer.unbind();
            this.indicesSet = true;
            this.numVertices = data.length;

            return this;
        }
    }

    /**
     * Sets the number of instances when using instanced attributes
     * @param num
     */
    setNumInstances(num=1){
        if(this.instanced){
            this.numInstances = num;
        }
    }

    /**
     * Draws the mesh;
     */
    draw(camera,cb){
        if(this.shaderSet){
            this.shader.bind();

            // bind default uniforms
            this.shader.set4x4Uniform('projectionMatrix',camera.projection);
            this.shader.set4x4Uniform('viewMatrix',camera.view);
            this.shader.uniform("viewMatrix",camera.view);
            this.shader.uniform("view",camera.view);
            this.shader.uniform("modelMatrix",this.model);
            this.shader.uniform("model",this.model);

            // run callback so user can add any additional uniform values
            if(cb !== undefined){
                cb(this.shader);
            }

            // bind vao
            this.vao.bind();

            // if we've set indices, we need to call a different draw function
            if(this.indicesSet && !this.instanced){
                this.gl.drawElements(this.mode,this.numVertices,UNSIGNED_SHORT,0);
            }else if(!this.instanced){
                this.gl.drawArrays(this.mode,0,this.numVertices);
            }

            // if we have instanced attributes
            if(this.indicesSet && this.instanced){

                this.gl.drawInstancedElements(this.mode,this.numVertices,this.numInstances);
            }else if(this.instanced){

            }

            // unbind vao
            this.vao.unbind();
        }
    }


    drawOrtho(camera,cb){
        if(this.shaderSet){
            this.shader.bind();

            // run callback so user can add any additional uniform values
            if(cb !== undefined){
                cb(this.shader);
            }

            // bind vao
            this.vao.bind();

            // if we've set indices, we need to call a different draw function
            if(this.indicesSet && !this.instanced){
                this.gl.drawElements(this.mode,this.numVertices,UNSIGNED_SHORT,0);
            }else if(!this.instanced){
                this.gl.drawArrays(this.mode,0,this.numVertices);
            }

            // if we have instanced attributes
            if(this.indicesSet && this.instanced){
                this.gl.drawInstancedElements(this.mode,this.numVertices,this.numInstances);
            }else if(this.instanced){

            }

            // unbind vao
            this.vao.unbind();
        }
    }

    update(){

    }



    translate(x=1,y=1,z=0){
        vec3.set(this.position,x,y,z);
        mat4.translate(this.model,this.model,this.position);
    }

    translateX(x){
        vec3.set(this.position,x,this.position.y,this.position.z);
        mat4.translate(this.model,this.model,this.position);
    }

    translateY(y){
        vec3.set(this.position,this.position.x,y,this.position.z);
        mat4.translate(this.model,this.model,this.position);
    }

    translateZ(z){
        vec3.set(this.position,this.position.x,this.position.y,z);
        mat4.translate(this.model,this.model,this.position);
    }

    scaleModel(x=1,y=1,z=1){
        vec3.set(this.scale,x,y,z);
        mat4.scale(this.model,this.model,this.scale);
        return this;
    }

    rotateX(angle){
        mat4.rotateX(this.model,this.model,angle);
        return this;
    }

    rotateY(angle){
        mat4.rotateY(this.model,this.model,angle);
        return this;
    }

    rotateZ(angle){
        mat4.rotateZ(this.model,this.model,angle);
        return this;
    }
}

export default Mesh;