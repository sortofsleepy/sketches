#ifndef AudioAnalyser_hpp
#define AudioAnalyser_hpp

#include "cinder/audio/audio.h"
#include "cinder/app/App.h"
#include "cinder/audio/Voice.h"
#include "cinder/audio/Source.h"
#include "cinder/gl/Texture.h"
#include <vector>
#include <string>
#include <memory>

typedef std::shared_ptr<class AudioAnalyser> AudioAnalyserRef;

class AudioAnalyser {
    
    
    //! texture for uploading to shader
    ci::gl::TextureRef mTex;
    
    //! texture format object
    ci::gl::Texture::Format fmt;
    
    //! store input audio
    ci::audio::VoiceRef mVoice;
    
    //! graph to grab audio spectrum
    ci::audio::MonitorSpectralNodeRef mMonitorSpectralNode;
    
    //! Holds audio data 
    std::vector<float> mMagSpectrum;

    bool initialized;
public:
    AudioAnalyser();
    void loadAudio(std::string file,int fftSize=2048,int windowSize=1024);

    static AudioAnalyserRef create(std::string file=""){
        return AudioAnalyserRef(new AudioAnalyser());
    }
    
    void loadData(std::vector<float> * data,int width=512,int height=512);
};

#endif 