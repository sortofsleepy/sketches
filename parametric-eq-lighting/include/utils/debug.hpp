
#pragma once

#include "cinder/gl/VboMesh.h"
#include "cinder/Vector.h"
#include "cinder/gl/Shader.h"
#include "cinder/gl/Batch.h"

using namespace ci;
using namespace std;

namespace mocha {
    
    /**
     *  Returns a simple mesh that can be used for debugging.
     *  @param points {std::vector} a set of vertices to use for drawing
     */
    static ci::gl::BatchRef debugMesh(std::vector<ci::vec3> points,GLuint prim=GL_POINTS){
        gl::VboMesh::Layout layout;
        layout.attrib(geom::POSITION,3);
        
        gl::VboMeshRef mMesh;
        mMesh = gl::VboMesh::create(points.size(),prim,{layout});
        mMesh->bufferAttrib(geom::POSITION,sizeof(vec3) * points.size(),points.data());
        
        
        auto shader = gl::getStockShader(gl::ShaderDef().color());
        return gl::Batch::create(mMesh,shader);
    }
}