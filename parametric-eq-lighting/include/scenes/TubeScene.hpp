#ifndef TubeScene_hpp
#define TubeScene_hpp
#include "cinder/CinderMath.h"
#include "mocha/Scene.hpp"
#include "objects/HexSystem.hpp"
#include "mocha/FxComposer.hpp"
#include "objects/MeshTube.hpp"
#include "cinder/gl/Texture.h"
#include "objects/Blorb.hpp"
using namespace ci;
using namespace std;
class TubeScene : public mocha::Scene{
    ci::gl::TextureCubeMapRef mEnvMap;
    ci::gl::TextureCubeMapRef mRadianceMap;
       ci::gl::TextureCubeMapRef mIrradianceMap;
    MeshTube tube;
    std::vector<LightRef> lights;
    ci::gl::TextureRef mTex;
    

    gl::VboMeshRef mLoopMesh;
    gl::GlslProgRef mLoopShader;
    gl::BatchRef mLoopBatch;
    float spin;
    std::vector<float> path,angles;
    float roughness;
    float metalic;
    float specular;
    float exposure;
    float gamma;
    vec3 baseColor;
    
public:
    TubeScene(){
        roughness = 1.04f;
        metalic = 1.0;
        specular = 1.0;
        exposure = 2.5;
        gamma = 2.4;
        baseColor = vec3(1.0,0.0,0.0);
        
        loadTextures();
        
        
        vector<vec3> pts;
       
        pts.push_back( vec3( -3,  4, 0 ) );
        pts.push_back( vec3(  5,  1, 0 ) );
        pts.push_back( vec3( -5, -1, 0 ) );
        pts.push_back( vec3(  3, -4, 0 ) );
        pts.push_back( vec3(  8, -4, 0 ) );
        pts.push_back( vec3(  9, -4, 0 ) );
                                      
        tube.setSpline(pts);
        tube.setNumSegments( 3000 )
        .sampleCurve()
        .buildPTF()
        .buildMesh();
        
        mTex = gl::Texture::create(loadImage(app::loadAsset("mcap.jpg")));
        
        // build parametric eq mesh
        auto indices = tube.indices;
        auto positions = tube.positions;
        ci::vec2 tmpVec = vec2(0);
     
        // build angles
        for(int i = 0; i < indices.size();i += 3){
            
            auto a = indices[i];
            auto b = indices[i + 1];
            auto c = indices[i + 2];
        
            auto v0 = positions[a];
            auto v1 = positions[b];
            auto v2 = positions[c];
            
            std::vector<ci::vec3> verts;
            verts.push_back(v0);
            verts.push_back(v1);
            verts.push_back(v2);
            
            std::vector<ci::vec3>::iterator it = verts.begin();
            
            for(;it != verts.end();++it){    
                tmpVec.x = it->y;
                tmpVec.y = it->z;
                tmpVec = glm::normalize(tmpVec);
                
                auto angle = atan2(tmpVec.y,tmpVec.x);
                angles.push_back(angle);
                path.push_back(it->x);
             }
             
        }
        
        gl::VboMesh::Layout layout;
        layout.attrib(geom::POSITION,1);
        layout.attrib(geom::CUSTOM_0,1);
        
        mLoopMesh = gl::VboMesh::create(path.size(),GL_TRIANGLES,{layout});
        mLoopMesh->bufferAttrib(geom::POSITION,sizeof(float) * path.size(),path.data());
        mLoopMesh->bufferAttrib(geom::CUSTOM_0,sizeof(float) * angles.size(),angles.data());
        
        gl::GlslProg::Format fmt;
        fmt.vertex(app::loadAsset("tube/tube.glslv"));
        fmt.fragment(app::loadAsset("tube/tube.glslf"));
        mLoopShader = gl::GlslProg::create(fmt);
        
        mLoopBatch = gl::Batch::create(mLoopMesh,mLoopShader,{
            {geom::CUSTOM_0,"angle"}
        });
        	    
        	
        	   
        	    
    }
    void addLight(LightRef light){
        lights.push_back(light);
    }
    
    void draw(){
        spin += 0.2;
        auto overhead = lights[0]->getColorAmbient();
        baseColor.x = overhead.r;
        baseColor.y = overhead.g;
        baseColor.z = overhead.b;
        
        gl::ScopedBlend(true);
        gl::ScopedTextureBind tex0(mTex,0);
        gl::ScopedTextureBind tex1(mRadianceMap,1);
        gl::ScopedTextureBind tex2(mIrradianceMap,2);
        
      
        bind();
        mCam->useMatrices();
        mCam->orbitTarget(2.0);
       
        // set uniforms for the loop 
        mLoopBatch->getGlslProg()->uniform("uMatCap",0);
        mLoopBatch->getGlslProg()->uniform("time",(float)app::getElapsedSeconds());
        mLoopBatch->getGlslProg()->uniform("uRoughness",roughness);
        mLoopBatch->getGlslProg()->uniform("uMetallic",metalic);
        mLoopBatch->getGlslProg()->uniform("uSpecular",specular);
        mLoopBatch->getGlslProg()->uniform("uGamma",gamma);
        mLoopBatch->getGlslProg()->uniform("uBaseColor",baseColor);
        mLoopBatch->getGlslProg()->uniform("uExposure",exposure);
        mLoopBatch->getGlslProg()->uniform("uRadianceMap",1);
        mLoopBatch->getGlslProg()->uniform("uIrradianceMap",2);
                
        if(lights[0]->isOn()){
            mLoopBatch->getGlslProg()->uniform("isPowerOn",true);             
        }else{
            mLoopBatch->getGlslProg()->uniform("isPowerOn",false);   
        }
                
        mLoopBatch->draw();
        
        // draw the loop
        gl::pushModelMatrix();
        gl::scale(vec3(130));     
        mLoopBatch->draw();
        gl::popModelMatrix();
    
  
        unbind();
     
       
        drawSceneTexture();
    }
    void loadTextures(){
            
        ImageSourceRef radiance[6] = {
                
            loadImage(app::loadAsset("cubemap/irr_posx.hdr")),
            loadImage(app::loadAsset("cubemap/irr_negx.hdr")),
            loadImage(app::loadAsset("cubemap/irr_posy.hdr")),
            loadImage(app::loadAsset("cubemap/irr_negy.hdr")),
            loadImage(app::loadAsset("cubemap/irr_posz.hdr")),
            loadImage(app::loadAsset("cubemap/irr_negz.hdr")),
                                                                                     
        };
            
            auto cubeMapFormat = gl::TextureCubeMap::Format().mipmap().internalFormat( GL_RGB16F )
              .minFilter( GL_LINEAR_MIPMAP_LINEAR ).magFilter( GL_LINEAR );
              	
            
            mIrradianceMap = gl::TextureCubeMap::create(radiance,cubeMapFormat);
            mRadianceMap = gl::TextureCubeMap::createFromDds(loadAsset("hexsystem/studio_radiance.dds"),cubeMapFormat);
            
      }
};

#endif