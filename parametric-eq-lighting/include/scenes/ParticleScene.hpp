#ifndef ParticleScene_hpp
#define ParticleScene_hpp


#include "cinder/CinderMath.h"
#include "mocha/Scene.hpp"
#include "objects/HexSystem.hpp"
#include "mocha/FxComposer.hpp"
class ParticleScene : public mocha::Scene{
    
    HexSystem system;
    FxComposer composer;
    
    // horizontal blur
    FxPassRef mHBloom;
    
    // vertical blur
    FxPassRef mVBloom;
    
    // run fxaa
    FxPassRef mFxaa;
    
    // holds all lights for the scene
    std::vector<LightRef> lights;
public:
    ParticleScene(){
        mHBloom = Fxpass::create("post/bloom.glslf");
        mVBloom = Fxpass::create("post/bloom.glslf");
        mFxaa = Fxpass::create("post/fxaa.glslf");
        
        composer.addPass(mFxaa);
        composer.addPass(mHBloom);
        composer.addPass(mVBloom);
        composer.addPass(mHBloom);
        composer.addPass(mVBloom);
        //composer.addPass(mFxaa);
        //composer.createPass("post/fxaa.glslf");
        
        composer.compile();
    }
    
    void addLight(LightRef light){
        lights.push_back(light);
    }
    
    
    void update(){
       system.update();
       auto w = app::getWindowWidth();
       auto h = app::getWindowHeight();
       
       
       mHBloom->uniform("direction",vec2(1.3,0.0));
       mVBloom->uniform("direction",vec2(0.0,1.3));
       mFxaa->uniform( "uExtents", vec4( 1.0f / w, 1.0f / h, w, h ) );
       composer.update();
           
    }
    
    void draw(){
        
        composer.bind();
        gl::clear(ColorA(0,0,0,0),true);
        
        gl::pushMatrices();
        gl::rotate(toRadians(90.0f),vec3(1,0,0));
        
        for(int i = 0; i < lights.size(); ++i){
           system.getShader()->uniform("diffuseColor",lights[i]->getColorDiffuse());
           system.getShader()->uniform("specularColor",lights[i]->getColorSpecular());
           system.getShader()->uniform("lightIsOn",lights[i]->isOn());
           //system.getShader()->uniform("lightPosition",lights[i]->getPosition());
        }
        
        system.draw(mCam,(float)app::getElapsedSeconds());
        gl::popMatrices();
        
        composer.unbind();
        
        composer.drawOutput();
        
    }
    
   
    
};
/*
gl::pushMatrices();
    gl::rotate(toRadians(90.0f),vec3(1,0,0));
   
    for(int i = 0; i < lights.size(); ++i){
        system.getShader()->uniform("diffuseColor",lights[i].getColorDiffuse());
        system.getShader()->uniform("specularColor",lights[i].getColorSpecular());
        system.getShader()->uniform("lightPosition",lights[i].getPosition());
    }
   
  
    system.draw(mCam,(float)app::getElapsedSeconds());
    gl::popMatrices();
    */
#endif 
