#ifndef Blobs_hpp
#define Blobs_hpp

#include "cinder/gl/gl.h"
#include "mocha/core.hpp"
#include "mocha/Fxpass.hpp"
#include "cinder/gl/GlslProg.h"
using namespace ci;
using namespace std;


struct Metaball {
    float x;
};

// Blob layer. This should be the last thing to run before more post processing.
// Feed the main drawn geometry scene to the update function
class Blobs : public Fxpass{

    gl::GlslProgRef mFxShader;
    int numSamples;
    int numBlobs;
    
    // uniform buffer so we can fake dynamic arrays in GLSL
    gl::UboRef mUBuffer;
    
    // the light position of the room
    ci::vec3 lightPosition;
    
    // the number of metaballs we ant in the scene
    vector<float> metaballs;
public:
    Blobs(int numSamples=1,int numBlobs=8){
        mFxShader = mocha::loadShader("blobs/vertex.glslv","blobs/fragment.glslf");        
        this->numSamples = numSamples;
        this->numBlobs = numBlobs;
        
        for(int i = 0; i < numBlobs;++i){
            metaballs.push_back(1.0);
        }
        
        mUBuffer = gl::Ubo::create(sizeof(metaballs) * metaballs.size(),metaballs.data(),GL_DYNAMIC_DRAW);
        mUBuffer->bindBufferBase(0);
        mShader->uniformBlock("Metaballs",0);
        mUBuffer->unbind();
        
        
    }
    
    void setLightPosition(ci::vec3 lightPosition){
        this->lightPosition = lightPosition;
    }
    
    
    
    // updates the scene with the blob layer
    void update(Fxpass scene){
       bind();
       gl::clear(ColorA(0,0,0,0),true);
       gl::ScopedGlslProg shd(mFxShader);
       gl::ScopedTextureBind tex0(scene.getOutput(),0);
       mFxShader->uniform("resolution",vec2(app::getWindowWidth(),app::getWindowHeight()));
       mFxShader->uniform("uScene",0);
       gl::pushMatrices();
       gl::setMatricesWindow(getWindowSize());
       gl::viewport(getWindowSize());
       gl::drawSolidRect(getWindowBounds());
       gl::popMatrices();
       unbind();
   
     
    }
    
};

#endif 