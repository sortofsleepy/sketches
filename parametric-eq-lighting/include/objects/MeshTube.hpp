#ifndef MeshMeshTube_hpp
#define MeshMeshTube_hpp

#include "cinder/app/App.h"
#include "cinder/BSpline.h"
#include "cinder/Matrix.h"
#include "cinder/gl/VboMesh.h"
#include "cinder/gl/Batch.h"
#include "cinder/gl/GlslProg.h"
#include "cinder/gl/gl.h"

using namespace cinder;

// This is largely based on Cinder's Tubular example.

class MeshTube {
public:
    MeshTube();
    MeshTube( const MeshTube& obj );
    MeshTube( const BSpline3f& bspline, const std::vector<vec3>& prof );
    ~MeshTube() {}
    MeshTube& setProfile(std::vector<ci::vec3> &prof);
    
    MeshTube& operator=( const MeshTube& rhs );
    MeshTube& setBSpline( const BSpline3f& bspline ) { mBSpline = bspline; return *this; }
    MeshTube& setSpline(std::vector<ci::vec3> pts,int degree=3,bool loop=false,bool open=true);
    MeshTube& setNumSegments( int n ) { mNumSegs = n; return *this;}
    MeshTube& sampleCurve();
    // Builds parallel transport frames for each curve sample
    MeshTube& buildPTF();
    
    // Builds frenet frames for each curve sample - optional method
    MeshTube& buildFrenet();
    
    void buildMesh();
    void draw();
    MeshTube& setShader(std::string vertex,std::string fragment);
    
    //! a flag to update the mesh. If false, it indicates we need to initialize
    //! a VboMesh
    bool meshBuilt;
    
    //! Holds the tube's positions
    std::vector<ci::vec3> positions;
    
    //! Holds the tube's indices
    std::vector<uint32_t> indices;
    
    //! Holds tube's radial angles
    std::vector<float> angles;
    
    //! Mesh to build tube
    ci::gl::VboMeshRef mMesh;
    
    //! Shader for tube
    ci::gl::GlslProgRef mShader;
    
    //! Batch for the tube
    ci::gl::BatchRef mBatch;
    
    template<typename T>
    void uniform(std::string name,T val){
        this->mShader->uniform(name,val);
    }

private:
    
    //! the default drawing profile which should render a tube.
    void defaultProfile();
    
    //! Compiles all the components of the mesh
    void compileMesh(const vec3& P0, const vec3& P1, const vec3& P2, const vec3& P3);
    
    int mNumSegs;
    BSpline3f mBSpline;		// b-spline path
    std::vector<vec3> mProf;
    float mScale0;		// min scale of profile along curves
    float mScale1;		// max scale of profile along curves
    std::vector<vec3> mPs;			// Points in b-spline sample
    std::vector<vec3> mTs;			// Tangents in b-spline sample
    std::vector<mat4> mFrames;		// Coordinate frame at each b-spline sample
};

#endif