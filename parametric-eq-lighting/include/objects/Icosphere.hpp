#ifndef Icosphere_hpp
#define Icosphere_hpp

#include "mocha/core.hpp"
#include "cinder/gl/VboMesh.h"
#include "cinder/gl/Batch.h"

class Icosphere {
protected:
    ci::gl::VboMeshRef mMesh;
    ci::gl::GlslProgRef mShader = ci::gl::getStockShader(ci::gl::ShaderDef().color());
    ci::gl::BatchRef mBatch;
    
    mutable std::vector<ci::vec3> mPositions, mNormals, mColors;
    mutable std::vector<ci::vec2> mTexCoords;
    mutable std::vector<uint32_t> mIndices;
    
    int subdivisions;
    static float sPositions[12*3];
    static float sTexCoords[60*2];
    static uint32_t sIndices[60];
     
public:
    Icosphere();
    void setup();
    void explodeModifier();
    void calculateImplUV();
    void subdivide();
    void loadShader(std::string vertex,std::string fragment){
        mShader = mocha::loadShader(vertex,fragment);
    }
    
    
    void draw();
    
    template<typename T>
    void uniform(std::string name, T val){
        mBatch->getGlslProg()->uniform(name,val);
    }
};

#endif