#ifndef HexSystem_hpp
#define HexSystem_hpp

#include "cinder/gl/Vbo.h"
#include "cinder/gl/VboMesh.h"
#include "cinder/gl/GlslProg.h"
#include "cinder/gl/Batch.h"
#include "cinder/gl/Texture.h"
#include "mocha/ArcCam.hpp"
#include "mocha/Light.hpp"
#include "mocha/buffers/FeedbackBuffer.hpp"
#include "cinder/Rand.h"


typedef struct {
    ci::vec3 position;
    ci::vec3 velocity;
    float phi;
    float theta;
    float phiSpeed;
    float thetaSpeed;
}Particle;

const int PositionIndex	= 0;
const int VelocityIndex = 1;
const int PhiIndex = 2;
const int ThetaIndex = 3;
const int ThetaSpeedIndex = 4;
const int PhiSpeedIndex = 5;

class HexSystem{
    
    int numParticles;
    
    // base shape for hexagon
    ci::gl::VboMeshRef mHexMesh;
    ci::gl::GlslProgRef mShader;
    ci::gl::BatchRef mBatch;
    
    //! shader for animation update
    ci::gl::GlslProgRef mUpdateShader;
    
    //! store instance positions
    ci::gl::VboRef positionData;    
    
    //! store rotation offsets
    ci::gl::VboRef rotationOffsets;
    
    ci::gl::TextureCubeMapRef mEnvMap;
    ci::gl::TextureCubeMapRef mRadianceMap;
    ci::gl::TextureCubeMapRef mIrradianceMap;
    
    // buffer for transform feedback
    FeedbackBuffer buffer;

    float scale;
    
    ci::Rand mRand;
    
    float mSphereRadius;
    
    
    
    struct{
        ci::vec4 uRoughnessSpecularMetallicExposure;
        ci::vec4 uGamma;
        ci::vec4 uBaseColor;
        
        
    }mPBRSettings;
    
    ci::gl::UboRef mPBR;
    ci::gl::VboRef system;
    int mHexCount;
    std::vector<Particle> particles;
    
    
    void loadShaders();
    void initPBR();
    void buildShape();
public:
    HexSystem();
    void setup();
    void update();
    
    
    ci::gl::GlslProgRef getShader(){
        return mShader;
    }
    
    void draw(ArcCamRef mCam,float dt=0.0);
    ci::gl::VboMeshRef mLightMesh;
    ci::gl::GlslProgRef mLightShader;
    ci::gl::BatchRef mLightBatch;
};

#endif