#ifndef Blorb_hpp
#define Blorb_hpp


#include "objects/Icosphere.hpp"
#include "cinder/gl/Texture.h"
class Blorb : public Icosphere {
    
    ci::gl::TextureCubeMapRef mEnvMap;
    ci::gl::TextureCubeMapRef mRadianceMap;
    ci::gl::TextureCubeMapRef mIrradianceMap;
    
    ci::gl::VboMeshRef mSphereMesh;
    ci::gl::BatchRef mBatch;
public:
    Blorb(){
        subdivisions = 2;
        
        loadTextures();
        loadShader("blobs/blorb.glslv","blobs/blorb.glslf");
        setup();
        mBatch = gl::Batch::create(mMesh,mShader);
    
        mSphereMesh = gl::VboMesh::create(geom::Sphere().radius(30.0).subdivisions(40));
        //mBatch = gl::Batch::create(mSphereMesh,mShader);
    }
    
    void draw(){
        gl::ScopedTextureBind tex0(mRadianceMap,0);
        gl::ScopedTextureBind tex1(mIrradianceMap,1);
        
        mBatch->getGlslProg()->uniform("uRoughness",1.05f);
        mBatch->getGlslProg()->uniform("uMetallic",1.0f);
        mBatch->getGlslProg()->uniform("uSpecular",1.0f);
        mBatch->getGlslProg()->uniform("uExposure",2.5f);
        mBatch->getGlslProg()->uniform("uGamma",2.4f);
        mBatch->getGlslProg()->uniform("uBaseColor",vec3(1.0f,0.5f,1.0f));
        mBatch->getGlslProg()->uniform("uRadianceMap",0);
        mBatch->getGlslProg()->uniform("uIrradianceMap",1);
        mBatch->draw();
        
        
        
    }
    
    void loadTextures(){
        ImageSourceRef radiance[6] = {
            loadImage(loadAsset("cubemap/irr_posx.hdr")),
            loadImage(loadAsset("cubemap/irr_negx.hdr")),
            loadImage(loadAsset("cubemap/irr_posy.hdr")),
            loadImage(loadAsset("cubemap/irr_negy.hdr")),
            loadImage(loadAsset("cubemap/irr_posz.hdr")),
            loadImage(loadAsset("cubemap/irr_negz.hdr")),
                                                                                 
        };
        
        auto cubeMapFormat = gl::TextureCubeMap::Format().mipmap().internalFormat( GL_RGB16F )
          .minFilter( GL_LINEAR_MIPMAP_LINEAR ).magFilter( GL_LINEAR );
          	
        
        mIrradianceMap = gl::TextureCubeMap::create(radiance,cubeMapFormat);
        mRadianceMap = gl::TextureCubeMap::createFromDds(loadAsset("hexsystem/studio_radiance.dds"),cubeMapFormat);
        
    }
};


#endif