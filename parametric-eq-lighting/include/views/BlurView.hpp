#ifndef BlurView_hpp
#define BlurView_hpp

#include "cinder/gl/gl.h"
#include "cinder/gl/Fbo.h"
#include "cinder/app/App.h"
#include <memory>


typedef std::shared_ptr<class BlurView>BlurViewRef;

class BlurView {
    
    ci::gl::FboRef mFbo;
    ci::gl::FboRef mAccumFbo;
    ci::vec2 resolution;
    
    int subframes;
    
public:
    BlurView(int width,int height,int subframes);
    static BlurViewRef create(int width=ci::app::getWindowWidth(),
                              int height=ci::app::getWindowHeight(),
                              int subframes=16){
        
        return BlurViewRef(new BlurView(width,height,subframes));
    }
    void bind();
    void unbind();
    void update();
};

#endif