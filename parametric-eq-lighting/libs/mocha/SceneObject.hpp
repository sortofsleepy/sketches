#ifndef SceneObject_hpp
#define SceneObject_hpp

// this really just serves as a unifying passthru type object
// so it's eaiser to fit into a Scene 
#include <string>

class SceneObject {
protected:
    
    std::string name;
public:
    SceneObject(std::string name){
        this->name = name;
    }
    std::string getName(){
        return name;
    }
    virtual void update(){}
    virtual void draw(){}
    
};


#endif