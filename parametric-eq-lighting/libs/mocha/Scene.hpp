#ifndef ScenePass_hpp
#define ScenePass_hpp

#include "mocha/ArcCam.hpp"
#include "cinder/app/App.h"
#include "mocha/SceneObject.hpp"
#include "cinder/gl/gl.h"
#include "cinder/app/App.h"
#include "cinder/gl/Fbo.h"
#include <vector>
#include <memory>
using namespace ci;
using namespace std;
namespace mocha {
    
    
    typedef std::shared_ptr<class Scene> SceneRef;
    
    class Scene {
    protected:
        
        ci::gl::FboRef mFbo;
        std::vector<SceneObject> objects;
        int width;
        int height;
        
        ArcCamRef mCam;
    public:
        Scene(){
            width = app::getWindowWidth();
            height = app::getWindowHeight();
            
            
            gl::Texture::Format fmt;
            fmt.setInternalFormat(GL_RGBA32F);
            //fmt.loadTopDown();
            
            gl::Fbo::Format ffmt;
            ffmt.setColorTextureFormat(fmt);
            
            mFbo = gl::Fbo::create(width,height,ffmt);
            gl::ScopedFramebuffer fbo(mFbo);{
                
                gl::clear(ColorA(0,0,0,0),true);
            }
        }
        
        Scene(int width,int height){
            this->width = width;
            this->height = height;
            
            gl::Texture::Format fmt;
            fmt.setInternalFormat(GL_RGBA);
            //fmt.loadTopDown();
            
            gl::Fbo::Format ffmt;
            ffmt.setColorTextureFormat(fmt);
            
            mFbo = gl::Fbo::create(width,height,ffmt);
            gl::ScopedFramebuffer fbo(mFbo);{
                gl::clear(ColorA(0,0,0,0),true);
            }
        }
        
        static SceneRef create(int width=ci::app::getWindowWidth(),
                               int height=ci::app::getWindowHeight()){
            return SceneRef(new Scene(width,height));        
        }
     
        void bind(){
            mFbo->bindFramebuffer();
            gl::clear(ColorA(0,0,0,0),true);
            
        }
        
        void setCamera(ArcCamRef mCam){
            this->mCam = mCam;
        }
        
        void unbind(){
            mFbo->unbindFramebuffer();
        }
        
        gl::TextureRef getScene(){
            return mFbo->getColorTexture();
        }
        
        //! To be used in case you overwrite the draw function
        void drawSceneTexture(){
            gl::setMatricesWindow(app::getWindowSize());
            gl::viewport(app::getWindowSize());
            gl::draw(getScene(),Rectf(0,0,width,height));
        }
        
        void draw(){
            gl::draw(getScene(),Rectf(0,0,width,height));
        }
    };
};

#endif