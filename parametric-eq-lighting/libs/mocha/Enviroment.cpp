#include "Enviroment.hpp"
#include "cinder/Log.h"
using namespace ci;
using namespace std;

const float MAX_TIMEMULTI = 120.0f;

Enviroment::Enviroment():
mBoxSize(vec3(450.0f,250.0f,450.0f)),
scaleVal(20.0),
isPowerOn(false){
    mOverheadLight = Light::create();
    mOverheadLight->position(vec3(0.0f,mBoxSize.y,0.0f));
    
    // the overall room color
    mOverheadLight->colorAmbient(ColorA(1.0,1.0,0.0,1.0));
    mOverheadLight->colorDiffuse(ColorA(1.0,1.0,0.0,1.0));
    //mOverheadLight->toggleLight();
    
    
    mLightMesh = gl::VboMesh::create(geom::Sphere().radius(20));
    mLightShader = gl::getStockShader(gl::ShaderDef().color());
    mLightBatch = gl::Batch::create(mLightMesh,mLightShader);
    
}

ci::vec3 Enviroment::getLightPosition(){
    return mOverheadLight->getPosition();
}

void Enviroment::drawOverheadLight(){
    gl::pushMatrices();
    gl::translate(getLightPosition());
    gl::color(1,1,0);
    mLightBatch->draw();
    gl::popMatrices();
}

void Enviroment::updateTime(bool saveFrame){
    if( isPowerOn ) mPower -= ( mPower - 1.0f ) * 0.2f;
    	else mPower -= ( mPower - 0.0f ) * 0.2f;

    float prevTime = mTime;
    mTime = (float)app::getElapsedSeconds();
    float dt = mTime - prevTime;
    
    if( saveFrame ) dt = 1.0f/60.0f;
    
    mTimeAdjusted = dt * mTimeMulti;
    mTimeElapsed += mTimeAdjusted;
    
    mTimer += mTimeAdjusted;
    mTick = false;
    
    if( mTimer > 1.0f ){
        mTick = true;
        mTimer = 0.0f;
    }
}

float Enviroment::getEnvTime(){
    return mTimer;
}

void Enviroment::togglePower(){
    isPowerOn = !isPowerOn;
    mOverheadLight->toggleLight();
}

void Enviroment::draw(ArcCamRef mCam){
    glEnable( GL_CULL_FACE );
    glCullFace( GL_BACK );
    
    gl::pushModelView();
    
    gl::scale(mBoxSize);
    //mat4 mod = glm::scale(mod,mBoxSize);
    mBatch->getGlslProg()->uniform("uLightColor",mOverheadLight->getColorAmbient());
    mBatch->getGlslProg()->uniform("eyePos",mCam->getEye());
    mBatch->getGlslProg()->uniform("roomDimensions",mBoxSize);
    mBatch->getGlslProg()->uniform("lightPower",getLightPower());
    mBatch->getGlslProg()->uniform("power",getPower());
    mBatch->getGlslProg()->uniform("timePer",getTimePer() * 1.5f + 0.5f);
    mBatch->draw(); 
    gl::popModelView();
    
    glDisable( GL_CULL_FACE );
    
   
   
}

ci::ColorA Enviroment::getLightColor(){
    return mOverheadLight->getColorAmbient();
}

void Enviroment::setupControls(){
    
    // default key for lighting is l 
    app::getWindow()->getSignalKeyDown().connect([this](KeyEvent event){
        if(event.getCode() == event.KEY_l){
            togglePower();
        }
    });      
}

float Enviroment::getTimePer(){
    return mTimeMulti/MAX_TIMEMULTI;
}

float Enviroment::getPower(){
    return mPower;
}

float Enviroment::getLightPower(){
   
    float p = getPower() * 5.0f * M_PI;
    float lightPower = cos( p ) * 0.5f;
   
    return lightPower;
}

void Enviroment::setup(){
    int index = 0;
    std::vector<uint32_t>	indices;
    std::vector<ci::vec3>	posCoords;
    std::vector<ci::vec3>	normals;
    std::vector<ci::vec2>	texCoords;
    
    float X = 1.0f;
    float Y = 1.0f;
    float Z = 1.0f;
    static vec3 verts[8] = {
        vec3(-X,-Y,-Z ), vec3(-X,-Y, Z ),
        vec3( X,-Y, Z ), vec3( X,-Y,-Z ),
        vec3(-X, Y,-Z ), vec3(-X, Y, Z ),
        vec3( X, Y, Z ), vec3( X, Y,-Z )
    };
    
    static GLuint vIndices[12][3] = {
        
        {0,1,3}, {1,2,3},	// floor
        {4,7,5}, {7,6,5},	// ceiling
        {0,4,1}, {4,5,1},	// left
        {2,6,3}, {6,7,3},	// right
        {1,5,2}, {5,6,2},	// back
        {3,7,0}, {7,4,0}  // front  
    };
    
    static vec3 vNormals[6] = {
        vec3( 0, 1, 0 ),	// floor
        vec3( 0,-1, 0 ),	// ceiling
        vec3( 1, 0, 0 ),	// left
        vec3(-1, 0, 0 ),	// right
        vec3( 0, 0,-1 ),	// back
        vec3( 0, 0, 1 ) 	// front
        
    };
    static vec2 vTexCoords[4] = {
        
        vec2( 0.0f, 0.0f ),
        vec2( 0.0f, 1.0f ),
        vec2( 1.0f, 1.0f ),
        vec2( 1.0f, 0.0f )
    };
    
    static GLuint tIndices[12][3] = {
        
        {0,1,3}, {1,2,3},	// floor
        {0,1,3}, {1,2,3},	// ceiling
        {0,1,3}, {1,2,3},	// left
        {0,1,3}, {1,2,3},	// right
        {0,1,3}, {1,2,3},	// back
        {0,1,3}, {1,2,3} 	// front
        
    };
    
    gl::VboMesh::Layout layout;
    layout.attrib(geom::POSITION,3);
    layout.attrib(geom::TEX_COORD_0,2);
    layout.attrib(geom::NORMAL,3);
    
    for( int i=0; i<12; i++ ){
        
        posCoords.push_back( verts[vIndices[i][0]] );
        posCoords.push_back( verts[vIndices[i][1]] );
        posCoords.push_back( verts[vIndices[i][2]] );
        indices.push_back( index++ );
        indices.push_back( index++ );
        indices.push_back( index++ );
        normals.push_back( vNormals[i/2] );
        normals.push_back( vNormals[i/2] );
        normals.push_back( vNormals[i/2] );
        texCoords.push_back( vTexCoords[tIndices[i][0]] );
        texCoords.push_back( vTexCoords[tIndices[i][1]] );
        texCoords.push_back( vTexCoords[tIndices[i][2]] );
    }
    
   
    
    mMesh = gl::VboMesh::create(posCoords.size(),GL_TRIANGLES,{layout},indices.size(),GL_UNSIGNED_INT);
    mMesh->bufferAttrib(geom::POSITION,sizeof(vec3) * posCoords.size(),posCoords.data());
    mMesh->bufferAttrib(geom::TEX_COORD_0,sizeof(vec2) * texCoords.size(),texCoords.data());
    mMesh->bufferAttrib(geom::Attrib::NORMAL,sizeof(vec3) * normals.size(),normals.data());
   
    mMesh->bufferIndices(sizeof(uint32_t) * indices.size(),indices.data());

    mShader = mocha::loadShader("enviroment/vertex.glslv","enviroment/fragment.glslf");
    mBatch = gl::Batch::create(mMesh,mShader);
    mPower = 1.0f;
   
    if(!isPowerOn){
        mPower = 0.0f;
        mPower -= ( mPower - 0.0f ) * 0.2f;
    }
  
    
    mTime = (float)app::getElapsedSeconds();
    mTimer = 0.0f;
    mTick = false;
    mTimeMulti = 60.0f;
}
