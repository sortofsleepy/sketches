#ifndef FxComposer_hpp
#define FxComposer_hpp

#include "cinder/gl/gl.h"
#include "cinder/app/App.h"
#include "mocha/FxPass.hpp"
#include "cinder/Log.h"
#include <memory>
#include <vector>

/**
 * A very basic post-processing setup.
 * 1. Init and run add all of your post processing steps.
 * 2. Run compile()
 * 3. Finally, remember to write something to the scene fbo by calling bind/unbind
 */

class FxComposer {

    std::vector<FxPassRef> passes;
    ci::gl::FboRef mSceneFbo;
    int width, height;
public:
    FxComposer(int width = ci::app::getWindowWidth(),
               int height = ci::app::getWindowHeight()){
        
        gl::Texture::Format fmt;
        fmt.setInternalFormat(GL_RGBA);
        
        gl::Fbo::Format ffmt;
        ffmt.setColorTextureFormat(fmt);
        
        mSceneFbo = gl::Fbo::create(width,height);
        gl::ScopedFramebuffer fbo(mSceneFbo);{
           gl::clear(ColorA(0,0,0,0),true);
        }
        
        this->width = width;
        this->height = height;
    }
    
    FxPassRef getPass(int idx){
        return passes.at(idx); 
    }
    
    // builds a post processing effect 
    void createPass(std::string fragmentShader){
        FxPassRef pass = Fxpass::create(fragmentShader);
        pass->setup();
        passes.push_back(pass);
    }
    
    void addPass(FxPassRef pass){
        passes.push_back(pass);
    }
    
    void bind(){
        mSceneFbo->bindFramebuffer();
    }
    
    void unbind(){
        mSceneFbo->unbindFramebuffer();
    }
    
    // sets up scenes and passes the previous step to each following step
    void compile(){
        for(int i = 0; i < passes.size();++i){
            auto pass = passes.at(i);
            pass->setDimensions(width,height);
        
            if(i == 0){
                pass->setInput(mSceneFbo->getColorTexture());
            }else{
                auto prevPass = passes.at(i - 1);
                pass->setInput(prevPass->getInput());
            }
        }
        
    }
    
    
    
    void update(){
        for(int i = 0; i < passes.size();++i){
            passes.at(i)->update();
        }
    }
    
    gl::TextureRef getOutput(){
        return passes.at(passes.size() - 1)->getOutput();
    }
    
    void drawOutput(){
        passes.at(passes.size() - 1)->draw();
    }
};

#endif 