#ifndef Fxpass_hpp
#define Fxpass_hpp

#include <memory>
#include <string>
#include "cinder/app/App.h"
#include "cinder/gl/gl.h"
#include "cinder/gl/Fbo.h"
#include "cinder/gl/GlslProg.h"
#include "cinder/Log.h"
using namespace ci;
using namespace std;


typedef std::shared_ptr<class Fxpass>FxPassRef;

#define STRINGIFY(A) #A

class Fxpass {
protected:
    
    int width,height;
    
    int sceneIndex = 0;
    int fxIndex = 1;
    
    int mCurrFbo = 0;
    int mOtherFbo = 1;
    
    ci::gl::FboRef mFbo;
    
    bool doubleBuffer = false;
    
    //! This is a texture reference used when slotting the pass in as part of a
    //! larger post-processing chain of effects.
    ci::gl::TextureRef mInput;
    
    //! post processing shader. By default, a pass-through texture shader.
    ci::gl::GlslProgRef mShader = gl::getStockShader(gl::ShaderDef().texture());
    
    //! passthru vertex shader 
    std::string passthru = STRINGIFY(
           uniform mat4 ciModelViewProjection;
           in vec4 ciPosition;
           in vec2 ciTexCoord0;                             
           out vec2 uv;
           out vec2 vUv;                             
           const vec2 scale = vec2(0.5,0.5);
                                        
           void main(){
               uv = ciPosition.xy * scale + scale;
               vUv = ciTexCoord0;
               gl_Position = ciModelViewProjection * ciPosition;
           }
    );
public:
    Fxpass(){
        width = ci::app::getWindowWidth();
        height = ci::app::getWindowHeight();
        setup();
    }
    void setDimensions(int width,int height){
        this->width = width;
        this->height = height;
    }
    static FxPassRef create(std::string fragment=""){
        if(fragment != ""){
            auto pass = new Fxpass();
            pass->loadShader(fragment);
            return FxPassRef(pass);
        }else{
            return FxPassRef(new Fxpass());
        }
    }
    
    void setup(){
        gl::Texture::Format fmt;
        fmt.wrap(GL_CLAMP_TO_EDGE);
        fmt.setMagFilter(GL_NEAREST);
        fmt.setMinFilter(GL_NEAREST);
        fmt.setInternalFormat(GL_RGBA32F);
        
        gl::Fbo::Format ffmt;
        ffmt.setColorTextureFormat(fmt);
        
        ffmt.attachment(GL_COLOR_ATTACHMENT0 + sceneIndex, gl::Texture2d::create(width,height,fmt));
        
        // if we want to add a second attachment.
        if(doubleBuffer){
            ffmt.attachment(GL_COLOR_ATTACHMENT0 + fxIndex, gl::Texture2d::create(width,height,fmt));
        }
        
        mFbo = gl::Fbo::create(width,height,ffmt);
        
        gl::ScopedFramebuffer fb(mFbo);{    
            gl::clear();
        }     
    }
    //! Runs the pass over the input
    //! TODO write function covering double-buffering
    void update(float dt=0.0){
        gl::ScopedFramebuffer fbo(mFbo);
        gl::ScopedGlslProg shd(mShader);
        gl::clear(ColorA(0,0,0,0),1);
        
        // add default uniform values
        if(dt > 0.0){
            mShader->uniform("time",dt);
        }
        mShader->uniform("resolution",vec2(mFbo->getWidth(),mFbo->getHeight()));
        
        gl::ScopedTextureBind tex0(mInput,0);
        gl::drawSolidRect(mFbo->getBounds());
        
        std::swap(mCurrFbo,mOtherFbo);
    }
    
    
    
    ci::gl::TextureRef getInput(){
        return mInput;
    }
    
    void setInput(ci::gl::TextureRef mInput){
        setInputTexture(mInput);   
    }
    
    void bind(){
        mFbo->bindFramebuffer();
    }
    
    void unbind(){
        mFbo->unbindFramebuffer();
    }
    
    //! sets the input texture for the 
    void setInputTexture(ci::gl::TextureRef mInput){
        this->mInput = mInput;
    }
    
    //! loads shader for pass
    void loadShader(std::string fragment){
        gl::GlslProg::Format fmt;
        fmt.vertex(passthru);
        fmt.fragment(app::loadAsset(fragment));
        
        try {
            mShader = gl::GlslProg::create(fmt);
        }catch(ci::gl::GlslProgCompileExc &e){
            CI_LOG_E("FxPass - Error loading shader at " << fragment);
            CI_LOG_E("FxPass | ShaderLoadError :" <<e.what());
        }
    }
    
    void draw(){
        gl::setMatricesWindow(app::getWindowSize());
        gl::viewport(app::getWindowSize());
        
        gl::ScopedGlslProg shader(mShader);
        gl::ScopedTextureBind tex0(mFbo->getColorTexture());
        gl::drawSolidRect(app::getWindowBounds());
    }
    
    ci::gl::TextureRef getOutput(){
        return mFbo->getColorTexture();
    }
    
    template<typename T>
    void uniform(std::string name,T val){
        mShader->uniform(name,val);
    }
                                       
                                       
};

#endif 