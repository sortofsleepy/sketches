#ifndef Light_hpp
#define Light_hpp

#include "cinder/Color.h"
#include "cinder/Vector.h"
#include <memory>

// this is pretty much just straight out of the DeferredShading example in Cinder

typedef std::shared_ptr<class Light>LightRef;
class Light {
protected:
    ci::ColorAf mColorAmbient;
    ci::ColorAf mColorDiffuse;
    ci::ColorAf mColorSpecular;
    float mIntensity;
    float mRadius;
    float mVolume;
    ci::vec3 mPosition;
    bool mIsOn;
public:
    Light();
    
    static LightRef create(){
        return LightRef(new Light());
    }
    Light& colorAmbient( const ci::ColorAf& c );
    Light& colorDiffuse( const ci::ColorAf& c );
    Light& colorSpecular( const ci::ColorAf& c );
    Light& intensity( float v );
    Light& radius( float v );
    Light& volume( float v );
    Light& position( const ci::vec3& v );
    
    const ci::ColorAf& getColorAmbient() const;
    const ci::ColorAf& getColorDiffuse() const;
    const ci::ColorAf& getColorSpecular() const;
    float getIntensity() const;
    const ci::vec3& getPosition() const;
    float getRadius() const;
    float getVolume() const;
    
    bool isOn(){
        return mIsOn;
    }
    
    void toggleLight(){
        mIsOn = !mIsOn;
    }
    
    void setColorAmbient( const ci::ColorAf& c );
    void setColorDiffuse( const ci::ColorAf& c );
    void setColorSpecular( const ci::ColorAf& c );
    void setIntensity( float v );
    void setRadius( float v );
    void setVolume( float v );
    void setPosition( const ci::vec3& v );
};

#endif