#ifndef Enviroment_hpp
#define Enviroment_hpp

#include "mocha/ArcCam.hpp"
#include "mocha/core.hpp"
#include "cinder/gl/VboMesh.h"
#include "cinder/gl/Batch.h"
#include "cinder/gl/GlslProg.h"
#include "mocha/SceneObject.hpp"
#include "mocha/Light.hpp"
// largely based on Robert Hodgin's Eyeo 2012 code.
// http://roberthodgin.com/portfolio/work/eyeo-2012/

class Enviroment{

    ci::gl::VboMeshRef mMesh,mWireMesh;
    ci::gl::GlslProgRef mShader,mWireShader;
    ci::gl::BatchRef mBatch,mWireBatch;
    ci::vec3 mBoxSize;
    
    ci::gl::VboMeshRef mLightMesh;
    ci::gl::GlslProgRef mLightShader;
    ci::gl::BatchRef mLightBatch;
    
    float scaleVal;
    float mTime;				
    float mTimeElapsed;		
    float mTimeMulti;			
    float mTimeAdjusted;		
    float mTimer;				
    bool mTick;
    
    bool isPowerOn;
    float mPower;
    
    LightRef mOverheadLight;
public:
    Enviroment();
    ci::vec3 getLightPosition();
    ci::ColorA getLightColor();
  
    LightRef getLight(){
        return mOverheadLight;
    }
    void drawOverheadLight();
    void setupControls();
    void togglePower();
    float getPower();
    float getLightPower();
    float getTimePer();
    float getEnvTime();
    void setup();
    void updateTime(bool saveFrame=false);
    void draw(ArcCamRef mCam);
};

#endif