#version 150

uniform mat4 ciProjectionMatrix;
uniform mat4 ciModelMatrix;
uniform mat4 ciViewMatrix;
uniform vec3 eyePos;

in vec3 ciPosition;
in vec2 ciTexCoord0;
in vec3 ciNormal;

out vec2 vUv;
out vec3 vEyeDir;
out vec3 vNormal;
out vec4 vVertex;
void main(){
    
    
    vVertex = ciModelMatrix * vec4(ciPosition,1.0);
    vNormal = ciNormal;
    vEyeDir = normalize(eyePos - vVertex.xyz);
    vUv = ciTexCoord0;
    
    gl_Position = ciProjectionMatrix * ciViewMatrix * vVertex;
}