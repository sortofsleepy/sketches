#version 150
uniform vec3 eyePos;
uniform float power;
uniform float lightPower;
uniform vec3 roomDimensions;
uniform float timePer;
uniform vec4 uLightColor;
in vec3 eyeDir;
in vec4 vVertex;
in vec3 vNormal;

out vec4 glFragColor;

void main(){
  vec4 pos = normalize(vVertex);
  float aoLight = 1.0 - length( vVertex.xyz ) * ( 0.0015 + ( power * 0.00015 ) );
  
  float ceiling = 0.5;
  if( vNormal.y < -1.0 || vNormal.x < -1.0 ){
    ceiling = 1.0;
  }
  
  float yPer = clamp( vVertex.y / roomDimensions.y, 0.0, 1.0 );
  float ceilingGlow = pow( yPer, 2.0 ) * .25;
  ceilingGlow += pow( yPer, 20.0 );
  ceilingGlow += pow( max( yPer - 0.7, 0.0 ), 3.0 );
  
  
  vec3 litRoomColor = vec3( aoLight + ( ceiling + ceilingGlow * timePer ) * lightPower);
  
  // if we've set a tint, use that. By default, it's just white.
  litRoomColor *= uLightColor.rgb;

  
  glFragColor = vec4(litRoomColor,1.0);
}

