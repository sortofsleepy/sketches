/**
 * This provides a way for faking a dynamic number of lights for lighting purposes.
 * The idea is to define a UBO that contains all of your lighting data.
 * While the data uploaded can be dynamic, you must keep the number of lights under
 * the amount MAX_LIGHTS is set to for this to work.
*/
#define MAX_LIGHTS 100

// general structure for a light
struct Light {
    vec4 ambient;
    vec4 specular;
    vec4 diffuse;
    vec3 position;
    float radius;
};

// uniform buffer that can take the variable number of lights
layout(std140) uniform Lighting {
    Light lights[MAX_LIGHTS];
};

// ========= FUNCTIONS ================
// Calculates face normal for a position
vec3 calcFaceNormals(vec3 vViewPos){
    vec3 x = dFdx(vViewPos);
    vec3 y = dFdy(vViewPos);
    return normalize(cross(x,y));
}



// calculates basic phong shading
vec4 calculatePhong(vec3 vNormal,vec3 vPos,vec3 position, vec3 diffuse,vec3 specularColor ){
    vec3 normal = normalize(vNormal);
    vec3 lightDir = normalize(position - vPos);
    float dist = length(lightDir);
    
    float lambertian = max(dot(lightDir,normal), 0.0);
    float specular = 0.0;
    
    if(lambertian > 0.0){
        vec3 reflectDir = reflect(-lightDir,normal);
        vec3 viewDir = normalize(-vPos);
        float specAngle = max(dot(reflectDir,viewDir),0.0);
        specular = pow(specAngle,4.0);
        specular = pow(specAngle,16.0);
    }
    
    return vec4(lambertian * diffuse.rgb + specular * specularColor.rgb,1.);
       
}


// calculates basic phong shading
vec4 calculatePhong(vec3 vNormal,vec3 vPos,Light lightAttribs){
    vec3 normal = normalize(vNormal);
    vec3 lightDir = normalize(lightAttribs.position - vPos);
    float dist = length(lightDir);
    
    float lambertian = max(dot(lightDir,normal), 0.0);
    float specular = 0.0;
    
    if(lambertian > 0.0){
        vec3 reflectDir = reflect(-lightDir,normal);
        vec3 viewDir = normalize(-vPos);
        float specAngle = max(dot(reflectDir,viewDir),0.0);
        specular = pow(specAngle,4.0);
        specular = pow(specAngle,16.0);
    }
    
    return vec4(lambertian * lightAttribs.diffuse.rgb + specular * lightAttribs.specular.rgb,1.);
       
}