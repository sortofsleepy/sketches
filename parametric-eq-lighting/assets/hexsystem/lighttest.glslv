#version 150

uniform mat4 ciProjectionMatrix;
uniform mat4 ciViewMatrix;
uniform mat4 ciModelMatrix;
uniform mat3 ciNormalMatrix;
uniform mat4 ciModelView;
uniform mat4 ciViewMatrixInverse;
uniform float scale;
uniform float time;


in vec3 ciPosition;
in vec3 ciNormal;

out vec3 normalInterp;
out vec3 vPos;
out vec3 vNormal;
void main(){
    
    vec3 inputPosition = ciPosition;
    vec3 inputNormal = ciNormal;
    
     gl_Position = ciProjectionMatrix * ciModelView * vec4(inputPosition, 1.0);
    vec4 vertPos4 = ciModelView * vec4(inputPosition, 1.0);
    vPos = vec3(vertPos4) / vertPos4.w;
    normalInterp = vec3(ciNormalMatrix * inputNormal);
    vNormal = ciNormal;
}