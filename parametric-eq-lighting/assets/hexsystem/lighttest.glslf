#version 150
// light attributes - simple phong shading
uniform vec3 lightPosition;
uniform vec4 diffuseColor;
uniform vec4 ambientColor;
uniform vec4 specularColor;
uniform vec4 testColor;
uniform bool uLightOn;
out vec4 glFragColor;

#include "../utils/lighting.glslv"

in vec3 normalInterp;
in vec3 vPos;
in vec3 vNormal;

vec3 lightPos = lightPosition;
void main(){

    vec3 vertPos = vPos;
    vec3 normal = normalize(normalInterp);
    vec3 lightDir = normalize(lightPos - vertPos);
    
    float lambertian = max(dot(lightDir,normal), 0.0);
    float specular = 0.0;
    
    if(lambertian > 0.0){
      vec3 reflectDir = reflect(-lightDir,normal);
      vec3 viewDir = normalize(-vertPos);
      float specAngle = max(dot(reflectDir,viewDir),0.0);
      specular = pow(specAngle,4.0);
      specular = pow(specAngle,16.0);
    }
    
    glFragColor = vec4(lambertian * diffuseColor.rgb + specular * specularColor.rgb,1.);
   
 
}