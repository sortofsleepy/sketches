#version 150

uniform mat4 ciProjectionMatrix;
uniform mat4 ciViewMatrix;
uniform mat4 ciModelMatrix;
uniform mat3 ciNormalMatrix;
uniform mat4 ciModelView;
uniform mat4 ciViewMatrixInverse;
uniform float scale;
uniform float time;


in vec3 ciPosition;
in vec3 ciNormal;
in vec3 positionOffset;
in vec3 rotationOffset;

out mat3 vNormalMatrix;
out mat4 vViewMatrixInverse;
out vec3 vPos;
out vec3 vWsPosition;
out vec3 vEyePosition;
out vec3 vViewPos;



#include "../utils/rotate.glslv"

void main(){
    
   
    vec3 position = ciPosition;
    
    // scale hexagons 
    position.x *= scale;
    position.y *= scale;
    position.z *= scale;
    
    position = rotateX(position,time);
    position = rotateY(position,time);
    position = rotateZ(position,time);
    
    vec3 vCv = cross(rotationOffset,position);
    position = vCv * (2.0 * rotationOffset.z) + (cross(rotationOffset, vCv) * 2.0 + position);
    
    
    
    vec3 pos = position + positionOffset;

   
    vec4 worldSpacePosition = ciModelMatrix * vec4(pos,1.);
    vec4 viewSpacePosition = ciViewMatrix * worldSpacePosition;
    vec4 eyeDirViewSpace = viewSpacePosition - vec4(0,0,0,1);
    
    // lighting
  
    
    vEyePosition = -vec3(ciViewMatrixInverse * eyeDirViewSpace);
    vWsPosition = worldSpacePosition.xyz; 
    vPos = viewSpacePosition.xyz;
    vNormalMatrix = ciNormalMatrix;
    vViewMatrixInverse = ciViewMatrixInverse;
    
    vec4 vView = (ciModelView * vec4(pos,1.)) * -1.0;   
    vViewPos = vView.xyz;
       
    
    gl_Position = ciProjectionMatrix * viewSpacePosition;
}