#version 150

uniform samplerCube uRadianceMap;
uniform samplerCube uIrradianceMap;

// light attributes - simple phong shading
const vec3 lightPosition = vec3(0.0,-1.0,0.0);
uniform vec4 diffuseColor;
uniform vec4 specularColor;

uniform bool lightIsOn;

layout(std140) uniform PBR {
   vec4 uRoughnessSpecularMetallicExposure;
   vec4 uBaseColor;
   vec4 uGamma;
};


in mat3 vNormalMatrix;
in mat4 vViewMatrixInverse;
in vec3 vPos;
in vec3 vWsPosition;
in vec3 vEyePosition;
in vec3 vViewPos;


#include "../utils/pbr.glslv"
#include "../utils/lighting.glslv"
out vec4 glFragColor;



void main(){

    // ==== KINDA GENERATE NORMALS ==== //
    vec3 x = dFdx(vViewPos);
    vec3 y = dFdy(vViewPos);
    vec3 normal = normalize(cross(x,y));
    //normal = normal * 0.5 + 0.5;
    
    vec3 vNormal = vNormalMatrix * normal;
    vec3 vWsNormal = vec3(vViewMatrixInverse * vec4(vNormal,0.0));
    // === lighting === //
    vec4 phong = calculatePhong(normal,vPos,lightPosition,diffuseColor.rgb,specularColor.rgb);
 
    // === PBR STUFF ===//
    vec3 N = normalize(vWsNormal);
    vec3 V = normalize(vEyePosition);
    float uRoughness = uRoughnessSpecularMetallicExposure.x;
    float uMetallic = uRoughnessSpecularMetallicExposure.z;
    float uSpecular = uRoughnessSpecularMetallicExposure.y;
    float uExposure = uRoughnessSpecularMetallicExposure.w;
    
    vec3 color = getPbr(N,V,uBaseColor.xyz,uRoughness,uMetallic,uSpecular);
    color = toneMap(color * uExposure);
    color = color * (1.0 / toneMap(vec3(20.0)));
    color = pow(color,vec3(1.0 / uGamma.x));
    
    float g = (color.r + color.g + color.b);
    
    vec4 finalColor = vec4(color*color,g);
    
    if(lightIsOn){
        glFragColor = mix(finalColor,phong,0.7);
    }else{
        glFragColor = finalColor;
        glFragColor.a = 0.2;
    }
}
