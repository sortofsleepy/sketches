#version 150


uniform samplerCube uRadianceMap;
uniform samplerCube uIrradianceMap;
uniform sampler2D uMatCap;
uniform float uRoughness;
uniform float uMetallic;
uniform float uSpecular;
uniform vec3 uBaseColor;
uniform float uGamma;
uniform float uExposure;
uniform bool isPowerOn;

in vec3 vPos;
in mat3 vNormalMatrix;
in vec4 vE;
in mat4 vViewMatrixInverse;
in vec3 vEyePosition;

out vec4 glFragColor;

#include "../utils/pbr.glslv"
void main(){
    // ==== KINDA GENERATE NORMALS ==== //
    vec3 x = dFdx(vPos);
    vec3 y = dFdy(vPos);
    vec3 normal = normalize(cross(x,y));
    vec3 vNormal = vNormalMatrix * normal;
    vec3 vWsNormal = vec3(vViewMatrixInverse * vec4(normal,0.0));
             
      
    // ===== BUILD OUT SEM ====== //  
    vec3 n = normal * vNormalMatrix;
    vec3 r = reflect(vE.xyz,n);
    float m = 2. * sqrt(
        pow(r.x,2.) +
        pow(r.y,2.) +
        pow(r.z + 1.,2.)
    );
    
    
    vec2 vN = r.xy / m + 0.5;
    vec4 dat = texture(uMatCap,vN);
    
    // ==== PBR STUFF ======= //
    vec3 N = normalize(vWsNormal);
    vec3 V = normalize(vEyePosition);
    vec3 color = getPbr(N,V,uBaseColor,uRoughness,uMetallic,uSpecular);
    color = toneMap(color * uExposure);
    color = color * (1.0 / toneMap(vec3(20.0)));
    color = pow(color,vec3(1.0 / uGamma));
    
    float g = (color.r + color.g + color.b);
    vec4 finalColor = vec4(color + color + color,g);
    
 
   if(!isPowerOn){
       glFragColor = finalColor;
       glFragColor.a = 0.2;
   }else{
       glFragColor = finalColor;
   }       
}