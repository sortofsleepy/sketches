#version 150
uniform mat4 ciViewMatrix;
uniform mat4 ciModelMatrix;
uniform mat3 ciNormalMatrix;
uniform mat4 ciModelViewProjection;
uniform mat4 ciModelView;
uniform mat4 ciViewMatrixInverse;
uniform float time;

const float lengthSegments = 300.0;

in float ciPosition;
in float angle;
in vec3 ciNormal;

#define PI 3.14149

// equation based on this article.
// https://mattdesl.svbtle.com/shaping-curves-with-parametric-equations.
// some minor alterations since I'm calculating PTFs CPU side.

// Angles to spherical coordinates
vec3 spherical (float r, float phi, float theta) {
  return r * vec3(
    cos(phi) * cos(theta),
    cos(phi) * sin(theta),
    sin(phi)
  );
}


vec3 sample (float t) {
  float beta = t * PI;
  float r = sin(beta * 1.2) * 0.75;
  float phi = sin(beta * 3.0 + time);
  float theta = 4.0 * beta;
  return spherical(r, phi, theta);
}

float index = 0.0;

out mat3 vNormalMatrix;
out vec3 vPos;
out vec4 vE;
out mat4 vViewMatrixInverse;
out vec3 vEyePosition;
void main(){
    
    
    
    float t = (ciPosition * 2.0) * 0.5 + 0.5;
    vec3 pos = sample(t);
    
    vE = normalize(vec4(pos,1.) * ciModelView);
 
    
    vec4 worldSpacePosition = ciModelMatrix * vec4(pos,1.);
    vec4 viewSpacePosition = ciViewMatrix * worldSpacePosition;
    vec4 eyeDirViewSpace = viewSpacePosition - vec4(0,0,0,1);
      
    vec4 vView = (ciModelView * vec4(pos,1.)) * -1.0;
    vPos = vView.xyz;
    vEyePosition = -vec3(ciViewMatrixInverse * eyeDirViewSpace);
    vNormalMatrix = ciNormalMatrix;
    vViewMatrixInverse = ciViewMatrixInverse;
    gl_Position = ciModelViewProjection * vec4(pos,1.);    
}