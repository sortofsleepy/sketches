#version 150
// largely based/ported from iq's
// https://www.shadertoy.com/view/ld2GRz

// number of samples
uniform float samples;

uniform vec2 resolution;
uniform sampler2D uScene;
in vec2 vUv;


const float precis = 0.01;

// using uniform buffer objects to help fake the ability to use
// "dynamic" uniform arrays.
// MAX_METABALLS should be set to be greater than the number of metaballs you
// intend to use
#define MAX_METABALLS 100

layout(std140) uniform Metaballs {
    float balls[MAX_METABALLS];
};


// the number of metaballs
int numballs = balls.length();

// metaball array
vec4 blobs[balls.length()];



// include helper functions
#include "helpers.glslv"
#include "metaballs.glslv"

out vec4 glFragColor;
void main(){
    
    vec2 q = gl_FragCoord.xy / resolution.xy;
    vec2 m = vec2(0.5);
    vec4 scene = texture(uScene,vUv);
    
    
    glFragColor = scene;
}