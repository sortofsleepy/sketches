
float sdMetaBalls( vec3 pos )
{
    float m = 0.0;
    float p = 0.0;
    float dmin = 1e20;
    float h = 1.0; // track Lipschitz constant
    
    for( int i=0; i<numballs; i++ ){
    
        // bounding sphere for ball
        float db = length( blobs[i].xyz - pos );
        if( db < blobs[i].w ){
            float x = db/blobs[i].w;
            p += 1.0 - x*x*x*(x*(x*6.0-15.0)+10.0);
            m += 1.0;
            h = max( h, 0.5333*blobs[i].w );
            
        }else {
            dmin = min( dmin, db - blobs[i].w );
    	}
    	
    	
    }
    	
    float d = dmin + 0.1;
    
    if( m>0.5 ){
        float th = 0.2;
        d = h*(th-p);
        
    }
	
    return d;
}


vec3 norMetaBalls( vec3 pos )
{
	vec3 nor = vec3( 0.0, 0.0001, 0.0 );
		
	for( int i=0; i<numballs; i++ )
	{
        float db = length( blobs[i].xyz - pos );
		float x = clamp( db/blobs[i].w, 0.0, 1.0 );
		float p = x*x*(30.0*x*x - 60.0*x + 30.0);
		nor += normalize( pos - blobs[i].xyz ) * p / blobs[i].w;
	}
	
	return normalize( nor );
}

float map( in vec3 p )
{
	return sdMetaBalls( p );
}

vec2 intersect( in vec3 ro, in vec3 rd )
{
    
    float maxd = 10.0;
    float h = precis*2.0;
    float t = 0.0;
    float m = 1.0;
    for( int i=0; i<75; i++ )
    {
        if( h<precis||t>maxd ) continue;//break;
        t += h;
	    h = map( ro+rd*t );
    }

    if( t>maxd ) m=-1.0;
    return vec2( t, m );
}

vec3 calcNormal( in vec3 pos )
{
#ifdef ANALYTIC_NORMALS	
	return norMetaBalls( pos );
#else	
    vec3 eps = vec3(precis,0.0,0.0);
	return normalize( vec3(
           map(pos+eps.xyy) - map(pos-eps.xyy),
           map(pos+eps.yxy) - map(pos-eps.yxy),
           map(pos+eps.yyx) - map(pos-eps.yyx) ) );
#endif
}