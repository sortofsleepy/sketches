#include "Particles.hpp"
#include "cinder/Log.h"
#include "cinder/app/App.h"

using namespace ci;
using namespace std;

Particles::Particles():numParticles(400),scale(5.0),mSphereRadius(30){
    setup();
    buildShape();
	loadShaders();
}


void Particles::draw() {
	buffer.bind();
	gl::pushMatrices();

	gl::setDefaultShaderVars();
	mBatch->getGlslProg()->uniform("scale", scale);
	mBatch->getGlslProg()->uniform("time", (float)app::getElapsedSeconds());


	mBatch->drawInstanced(numParticles);
	gl::popMatrices();

	buffer.unbind();
}

void Particles::buildShape(){
    vector<vec3> positions;
    vector<vec3> normals;
    vector<uint32_t>indices;
    
    float angle = 1.73205;
    float h = angle * 0.5;
    
    positions.push_back(vec3(0,0,0));
    positions.push_back(vec3(0,1,0));
    positions.push_back(vec3(-h,0.5,0));
    positions.push_back(vec3(-h,-0.5,0));
    positions.push_back(vec3(0,-1,0));
    positions.push_back(vec3(h,-0.5,0));
    positions.push_back(vec3(h,0.5,0));
    
    
    normals.push_back(vec3(1,0,0));
    normals.push_back(vec3(1,0,1));
    normals.push_back(vec3(0,1,0));
    normals.push_back(vec3(0,1,0));
    
    indices.push_back(0);
    indices.push_back(1);
    indices.push_back(2);
    indices.push_back(0);
    indices.push_back(2);
    indices.push_back(3);
    indices.push_back(0);
    indices.push_back(3);
    indices.push_back(4);
    indices.push_back(0);
    indices.push_back(4);
    indices.push_back(5);
    indices.push_back(0);
    indices.push_back(5);
    indices.push_back(6);
    indices.push_back(0);
    indices.push_back(6);
    indices.push_back(1);
    
    gl::VboMesh::Layout layout;
    layout.attrib(geom::POSITION,3);
    layout.attrib(geom::NORMAL,3);
    
    mHexMesh = gl::VboMesh::create(positions.size(),GL_POINTS,{layout},indices.size(),GL_UNSIGNED_INT);
    mHexMesh->bufferAttrib(geom::POSITION,sizeof(vec3) * positions.size(),positions.data());
    mHexMesh->bufferAttrib(geom::NORMAL,sizeof(vec3) * normals.size(),normals.data());
    mHexMesh->bufferIndices(sizeof(uint32_t) * indices.size(),indices.data());
    
    // setup the buffer
    // create the VBO which will contain per-instance (rather than per-vertex) data
    system = buffer.getBuffer(0);
     
     geom::BufferLayout instanceDataLayout;
     instanceDataLayout.append( geom::CUSTOM_0, 3, sizeof(Particle), offsetof(Particle, position ), 1 );
    
     //buffer Particle data onto your instanced mHexMesh
     system->bufferSubData(0,sizeof(Particle) * particles.size(), particles.data());
     
     // now add it to the VboMesh we already made of the hexagons
     mHexMesh->appendVbo( instanceDataLayout, system );
     
     
     
     // ====== BUILD ROTATION OFFSETS ============ //
     vector<vec3> rotOffsets;
     for(int i = 0; i < numParticles;++i){
         rotOffsets.push_back(randVec3());
     }
     
     rotationOffsets = gl::Vbo::create(GL_ARRAY_BUFFER,sizeof(vec3) * rotOffsets.size(),rotOffsets.data(),GL_STATIC_DRAW);
     
     geom::BufferLayout rotLayout;
     rotLayout.append(geom::CUSTOM_1,1,0,0,1);
     mHexMesh->appendVbo(rotLayout,rotationOffsets);
     
     //======= INIT ======
     
     
     //build out a rendering shader
     gl::GlslProg::Format fmt;
     //fmt.vertex(app::loadAsset("soundsphere/SSphereParticles.glslv"));
     //fmt.fragment(app::loadAsset("soundsphere/SSphereParticles.glslf"));
     fmt.vertex(app::loadAsset("shaders/particles/particle.vertex"));
     fmt.fragment(app::loadAsset("shaders/particles/particle.frag"));
     
     
     mShader = gl::GlslProg::create(fmt);
     //gl::GlslProgRef render = gl::getStockShader(gl::ShaderDef().lambert());
     
     //build out the batch object with a reference to our instanced attribute
     mBatch = gl::Batch::create( mHexMesh, mShader, {
        {geom::CUSTOM_0,"positionOffset"},
        {geom::CUSTOM_1,"rotationOffset"}
     } );
     
}


void Particles::loadShaders(){
    try {
        
        // Create a vector of Transform Feedback "Varyings".
        // These strings tell OpenGL what to look for when capturing
        // Transform Feedback data. For instance, Position, Velocity,
        // and StartTime are variables in the updateSmoke.vert that we
        // write our calculations to.
        std::vector<std::string> transformFeedbackVaryings( 6 );
        transformFeedbackVaryings[0] = "Position";
        transformFeedbackVaryings[1] = "Velocity";
        transformFeedbackVaryings[2] = "Phi";
        transformFeedbackVaryings[3] = "Theta";
        transformFeedbackVaryings[4] = "ThetaSpeed";
        transformFeedbackVaryings[5] = "PhiSpeed";
        
        ci::gl::GlslProg::Format mUpdateParticleGlslFormat;
        // Notice that we don't offer a fragment shader. We don't need
        // one because we're not trying to write pixels while updating
        // the position, velocity, etc. data to the screen.
        mUpdateParticleGlslFormat.vertex( app::loadAsset( "shaders/particles/particleUpdate.glsl" ) )
        // This option will be either GL_SEPARATE_ATTRIBS or GL_INTERLEAVED_ATTRIBS,
        // depending on the structure of our data, below. We're using multiple
        // buffers. Therefore, we're using GL_SEPERATE_ATTRIBS
        .feedbackFormat( GL_INTERLEAVED_ATTRIBS )
        // Pass the feedbackVaryings to glsl
        .feedbackVaryings( transformFeedbackVaryings )
        .attribLocation( "VertexPosition", PositionIndex )
        .attribLocation( "VertexVelocity", VelocityIndex )
        .attribLocation( "VertexPhi", PhiIndex)
        .attribLocation( "VertexTheta", ThetaIndex)
        .attribLocation( "VertexPhiSpeed", PhiSpeedIndex)
        .attribLocation( "VertexThetaSpeed", ThetaSpeedIndex);
        mUpdateShader = ci::gl::GlslProg::create( mUpdateParticleGlslFormat );
    }catch( const ci::gl::GlslProgCompileExc &ex ) {
        
        CI_LOG_E(ex.what());
    }
         
       
    buffer.setUpdateShader(mUpdateShader);   
}

void Particles::update(){
    
    buffer.update([=]()->void{
        buffer.updateShader->uniform("radius",mSphereRadius);
        gl::bindBufferBase(GL_TRANSFORM_FEEDBACK_BUFFER, 0, buffer.getDrawBuffer());
    });
}

void Particles::setup(){

    float time = 0.0f;
    float rate = 0.001f;
    particles.reserve(numParticles);
    const float azimuth = 256.0f * M_PI / numParticles;
    const float inclination = M_PI / numParticles;
    const float radius = 340.0f;
    mSphereRadius = radius;
    for(int i = 0; i < numParticles;++i){
        
        float x = radius * sin( inclination * i ) * cos( azimuth * i );
        float y = radius * cos( inclination * i );
        float z = radius * sin( inclination * i ) * sin( azimuth * i );
        Particle p;
        p.position = vec3(x,y,z);
        p.velocity = ci::randVec3() * mix( 0.0f, 1.5f, mRand.nextFloat() );
        p.phiSpeed = randFloat(-0.5,0.5);
        p.thetaSpeed = randFloat(-0.5,0.5);
        p.theta = M_PI * randFloat() * 2;
        p.phi = M_PI * randFloat() * 2;
        particles.push_back(p);
        time += rate;
    }
    
    buffer.setData(particles, [=]()->void{
        
        gl::vertexAttribPointer( PositionIndex, 3, GL_FLOAT, GL_FALSE, sizeof(Particle), (const GLvoid*)offsetof(Particle, position) );
        gl::enableVertexAttribArray(PositionIndex);
    
        gl::vertexAttribPointer( VelocityIndex, 3, GL_FLOAT, GL_FALSE, sizeof(Particle), (const GLvoid*)offsetof(Particle, velocity) );
        gl::enableVertexAttribArray(VelocityIndex);
        
        gl::vertexAttribPointer(PhiIndex,1,GL_FLOAT,GL_FALSE,sizeof(Particle),(const GLvoid*) offsetof(Particle,phi));
        gl::enableVertexAttribArray(PhiIndex);
        
        gl::vertexAttribPointer(ThetaIndex,1,GL_FLOAT,GL_FALSE,sizeof(Particle),(const GLvoid*) offsetof(Particle,theta));
        gl::enableVertexAttribArray(ThetaIndex);
        
        gl::vertexAttribPointer( PhiSpeedIndex, 1, GL_FLOAT, GL_FALSE, sizeof(Particle), (const GLvoid*)offsetof(Particle, phiSpeed) );
        gl::enableVertexAttribArray(PhiSpeedIndex);
        
        gl::vertexAttribPointer(ThetaSpeedIndex,1,GL_FLOAT,GL_FALSE,sizeof(Particle),(const GLvoid*) offsetof(Particle,thetaSpeed));
        gl::enableVertexAttribArray(ThetaSpeedIndex);
        
    });

}
