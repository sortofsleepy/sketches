#include "objects/MeshTube.hpp" 
#include "cinder/gl/gl.h"



MeshTube::MeshTube() 
: mNumSegs( 16 ),
  mScale0( 1 ),
  mScale1( 1 )
{
    defaultProfile();	
}

MeshTube::MeshTube( const MeshTube& obj )
: mNumSegs( obj.mNumSegs ),
  mBSpline( obj.mBSpline ),	
  mProf( obj.mProf ),
  mScale0( obj.mScale0 ),
  mScale1( obj.mScale1 ),
  mPs( obj.mPs ),
  mTs( obj.mTs ),
  mFrames( obj.mFrames )
{
      defaultProfile();
  
}

MeshTube::MeshTube( const BSpline3f& bspline, const std::vector<vec3>& prof ) 
	: mNumSegs( 16 ), mBSpline( bspline ), mProf( prof ), mScale0( 1 ), mScale1( 1 )
{
}

MeshTube& MeshTube::setSpline(std::vector<ci::vec3> pts,int degree, bool loop, bool open){
    mBSpline = BSpline3f( pts, degree, loop, open );
    
    return *this;
}


void MeshTube::compileMesh(const vec3& P0, const vec3& P1, const vec3& P2, const vec3& P3){
    positions.push_back(P0);
    positions.push_back(P1);
    positions.push_back(P2);
    positions.push_back(P3);
    
    int vert0 = positions.size() - 4;
    int vert1 = positions.size() - 1;
    int vert2 = positions.size() - 2;
    int vert3 = positions.size() - 3;
    
    indices.push_back(vert0);
    indices.push_back(vert1);
    indices.push_back(vert3);
    indices.push_back(vert3);
    indices.push_back(vert1);
    indices.push_back(vert2);
    
}

MeshTube& MeshTube::operator=( const MeshTube& rhs ) 
{
	if( &rhs != this ) {
		mNumSegs	= rhs.mNumSegs;
		mBSpline	= rhs.mBSpline;
		mProf		= rhs.mProf;
		mScale0		= rhs.mScale0;
		mScale1		= rhs.mScale1;
		mPs			= rhs.mPs;
		mTs			= rhs.mTs;
		mFrames		= rhs.mFrames;
	}
	return *this;
}

MeshTube& MeshTube::sampleCurve()
{
    mPs.clear();
    mTs.clear();
    float dt = 1.0f/(float)mNumSegs;
    for( int i = 0; i < mNumSegs; ++i ) {
        
        float t = i*dt;
        vec3 P = mBSpline.getPosition( t );
        mPs.push_back( P );
        vec3 T = mBSpline.getDerivative( t );
        mTs.push_back( normalize( T ) );
    }
    
    return *this;
}

MeshTube& MeshTube::buildPTF() 
{
    mFrames.clear();
    int n = mPs.size();
    // Make sure we have at least 3 points because the first frame requires it
    if( n >= 3 ) {
        
        mFrames.resize( n );
        // Make the parallel transport frame
        mFrames[0] = firstFrame( mPs[0], mPs[1],  mPs[2] );
        // Make the remaining frames - saving the last
        for( int i = 1; i < n - 1; ++i ) {
            
            vec3 prevT = mTs[i - 1];
            vec3 curT  = mTs[i];
            mFrames[i] = nextFrame( mFrames[i - 1], mPs[i - 1], mPs[i], prevT, curT );
		
	}
	// Make the last frame
	mFrames[n - 1] = lastFrame( mFrames[n - 2], mPs[n - 2], mPs[n - 1] );	
    }
    
    return *this;
}

MeshTube& MeshTube::buildFrenet()
{
    
    mFrames.clear();
    int n = mPs.size();
    mFrames.resize( n );
    for( int i = 0; i < n; ++i ) {
        
        vec3 p0, p1, p2;
        if( i < (n - 2) ) {
            
            p0 = mPs[i];
            p1 = mPs[i + 1];
            p2 = mPs[i + 2];
		
    	}else if( i == (n - 2) ) {
    	    
        	p0 = mPs[i - 1];
        	p1 = mPs[i];
        	p2 = mPs[i + 1];
        	
        }else if( i == (n - 1) ) {
            
            p0 = mPs[i - 3];
            p1 = mPs[i - 2];
            p2 = mPs[i - 1];
	}
	
	vec3 t = normalize( p1 - p0 );
    	vec3 n = normalize( cross( t, p2 - p0 ) );
    	if( length( n ) == 0.0f ) {
    	    
        	int i = fabs( t[0] ) < fabs( t[1] ) ? 0 : 1;
        	if( fabs( t[2] ) < fabs( t[i] ) )
        	    i = 2;
        	
        	vec3 v( 0.0f, 0.0f, 0.0f );
        	v[i] = 1.0f;
        	n = normalize( cross( t, v ) );
	}
	
	vec3 b = cross( t, n );
	mat4& m = mFrames[i];
	m[0] = vec4( b, 0 );
	m[1] = vec4( n, 0 );
	m[2] = vec4( t, 0 );
	m[3] = vec4( mPs[i], 1 );
    }
    
    return *this;
}

void MeshTube::buildMesh(){
    
    
    if( ( mPs.size() != mFrames.size() ) || mFrames.size() < 3 || mProf.empty() )
        return;
	
	// ensure we clear previous positions and indices.
	positions.clear();
	indices.clear();
	

	for( int i = 0; i < mPs.size() - 1; ++i ) {
	    
	    mat4 mat0 = mFrames[i];
	    mat4 mat1 = mFrames[i + 1];
	    
	    float r0 = sin( (float)(i + 0)/(float)(mPs.size() - 1)*3.141592f );
	    float r1 = sin( (float)(i + 1)/(float)(mPs.size() - 1)*3.141592f );
	    float rs0 = (mScale1 - mScale0)*r0 + mScale0;
	    float rs1 = (mScale1 - mScale0)*r1 + mScale0;
	    
	    for( int ci = 0; ci < mProf.size(); ++ci ) {
	        
        	    int idx0 = ci;
        	    int idx1 = (ci == (mProf.size() - 1)) ? 0 : ci + 1;
        	    vec3 P0 = vec3( mat0 * vec4(mProf[idx0]*rs0, 1) );
        	    vec3 P1 = vec3( mat0 * vec4(mProf[idx1]*rs0, 1) );
        	    vec3 P2 = vec3( mat1 * vec4(mProf[idx1]*rs1, 1) );
        	    vec3 P3 = vec3( mat1 * vec4(mProf[idx0]*rs1, 1) );
        	    //addQuadToMesh( *tubeMesh, P0, P3, P2, P1 );
        	    compileMesh(P0,P3,P2,P1);
            }	    
	}
	
	if(!meshBuilt){
	
	    
	    
	    gl::VboMesh::Layout layout;
	    layout.attrib(geom::POSITION,3);
	    mMesh = gl::VboMesh::create(positions.size(),GL_TRIANGLES,{layout},indices.size(),GL_UNSIGNED_INT);	    		    	   	    	    		    
	    mMesh->bufferAttrib(geom::POSITION,sizeof(float) * positions.size(),positions.data());
	    mMesh->bufferIndices(sizeof(uint32_t) * indices.size(),indices.data());
	    
	    gl::GlslProg::Format fmt;
	    fmt.vertex(app::loadAsset("tube/tube.glslv"));
	    fmt.fragment(app::loadAsset("tube/tube.glslf"));
	    
	    mShader = gl::getStockShader(gl::ShaderDef().color());
	    mBatch = gl::Batch::create(mMesh,mShader);
	}else{	    
	    mMesh->bufferAttrib(geom::POSITION,sizeof(vec3) * positions.size(),positions.data());
	}
}

MeshTube& MeshTube::setShader(std::string vertex, std::string fragment){
    gl::GlslProg::Format fmt;
    fmt.vertex(app::loadAsset(vertex));
    fmt.fragment(app::loadAsset(fragment));
    fmt.preprocess(true);
    
    mShader = gl::GlslProg::create(fmt);
    
    return *this;
}

void MeshTube::draw(){
    mBatch->draw();
}

MeshTube& MeshTube::setProfile(std::vector<vec3> &prof){
    mProf.clear();
    mProf = prof;
    return *this;
}
void MeshTube::defaultProfile( )
{
    mProf.clear();
    float rad = 0.25f;
    int segments = 16;
    float dt = 6.28318531f/(float)segments;
    for( int i = 0; i < segments; ++i ) {
        
        float t = i*dt;
        mProf.push_back( vec3( cos( t )*rad, sin( t )*rad, 0 ) );
        
    }
}

