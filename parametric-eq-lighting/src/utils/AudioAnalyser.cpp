#include "utils/AudioAnalyser.hpp"
using namespace ci;
using namespace std;

AudioAnalyser::AudioAnalyser():initialized(false){}

void AudioAnalyser::loadAudio(std::string file,int fftSize,int windowSize){
    
    auto ctx = audio::Context::master();
    mVoice = audio::Voice::create(audio::load(app::loadAsset(file)));
    
    auto monitorFormat = audio::MonitorSpectralNode::Format().fftSize( fftSize ).windowSize( windowSize );
    mMonitorSpectralNode = ctx->makeNode( new audio::MonitorSpectralNode( monitorFormat ) );
    
    mVoice->getInputNode() >> mMonitorSpectralNode;
    
    ctx->enable();
    
    // setup texture format for storing audio data
    fmt.minFilter(GL_NEAREST);
    fmt.magFilter(GL_NEAREST);
    fmt.setWrap(GL_CLAMP_TO_EDGE,GL_CLAMP_TO_EDGE);
    fmt.setDataType(GL_FLOAT);
    
    // initialize texture with some dummy data
    vector<float> data;
    for(int i = 0; i < 100;++i){    
        data.push_back(0.0f);
    }
    
    loadData(&data);
}

void AudioAnalyser::loadData(std::vector<float> * data,int width, int height){
    // make sure we initialize texture first. Set flag once that's occured.
    if(!initialized){
        mTex = gl::Texture::create(data->data(), GL_RGBA,width,height,fmt);
        initialized = true;
    }else{
        // note - default mip level is 0
        mTex->update(data->data(),GL_RGBA,GL_FLOAT,0,width,height);
    }
}