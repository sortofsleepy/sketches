#include "BlurView.hpp"
using namespace ci;
using namespace std;

BlurView::BlurView(int width,int height,int subframes){
    resolution = vec2(width,height);
    
    // setup format for accumulation buffer
    gl::Texture::Format tfmt;
    tfmt.setInternalFormat(GL_RGB16F);
    
    gl::Fbo::Format fmt;
    fmt.setColorTextureFormat(tfmt);
    fmt.disableDepth();
    fmt.colorTexture();
    
    // init main fbo
    mFbo = gl::Fbo::create(width,height);
    
    // init accumulation fbo
    mAccumFbo = gl::Fbo::create(width,height,fmt);
    
    // set subframes
    this->subframes = subframes;
}

void BlurView::bind(){
    mFbo->bindFramebuffer();
}

void BlurView::unbind(){
    mFbo->unbindFramebuffer();
}

void BlurView::update(){
    gl::ScopedFramebuffer fbo(mAccumFbo);
    gl::clear(Color::black());
    gl::color(1,1,1,1);
    
    
}