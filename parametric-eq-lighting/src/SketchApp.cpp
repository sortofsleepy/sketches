#include "cinder/app/App.h"
#include "cinder/app/RendererGl.h"
#include "cinder/gl/gl.h"
#include "mocha/ArcCam.hpp"
#include "mocha/Enviroment.hpp"


#include "scenes/ParticleScene.hpp"
#include "scenes/TubeScene.hpp"
using namespace ci;
using namespace ci::app;
using namespace std;

class SketchApp : public App{
 public:
     void setup() override;
     void update() override;
     void keyDown(KeyEvent event) override;
     void draw() override;
     
     ArcCamRef mCam;
     
     //! the main enviroment we're in
     Enviroment env;
     
     //! scenes to draw
     ParticleScene pscene;
     TubeScene tubes;
};

void SketchApp::keyDown(cinder::app::KeyEvent event){
   
}
void SketchApp::setup(){
    mCam = ArcCam::create();
    mCam->setZoom(400.0);
    
    env.setup();
    env.setupControls();
    
    pscene.setCamera(mCam);
    pscene.addLight(env.getLight());
    
    tubes.setCamera(mCam);
    tubes.addLight(env.getLight());
}

void SketchApp::update(){
    env.updateTime(true);
    pscene.update();
}

void SketchApp::draw(){
    gl::clear(Color(0,0,0));
    mCam->useMatrices();
    env.draw(mCam);
    //env.drawOverheadLight();
    
    pscene.draw();
    tubes.draw();

    
}

CINDER_APP( SketchApp, RendererGl,[] ( App::Settings *settings ) {
	settings->setWindowSize( 1280, 720 );
	settings->setMultiTouchEnabled( false );
} )