Sketches
====
This repo just contains a series of different tiny projects of different ideas I was exploring at the time. 
Given the slightly unkempt nature of everything, as of 2/21/2020, I am slowly working my way through and transfering everything to a new repo called ["bite-sized"](https://gitlab.com/sortofsleepy/bite-sized)

While that is happening, this repo will still remain up, but will eventually be deleted. 