#version 420
#extension GL_ARB_shader_storage_buffer_object : require

in vec3 ciPosition;

uniform float time;
uniform float delta;
uniform mat4 ciModelViewProjection;
uniform mat4 ciModelView;
uniform mat3 ciNormalMatrix;

// instance attribute
in int particleId;

struct Particle
{
	vec4 position;
	vec4 velocity;
	vec4 acceleration;
};
layout( std140, binding = 0 ) buffer Part
{
    Particle particles[];
};

out vec3 vColor;
void main(){


	Particle p = particles[particleId];
	vec3 pos = p.position.xyz * 0.05;
	vColor = p.velocity.xyz;
	gl_Position = ciModelViewProjection * vec4( ciPosition + pos,1.);
}