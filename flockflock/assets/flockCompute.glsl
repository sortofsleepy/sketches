#version 420 core
#extension GL_ARB_compute_shader : enable
#extension GL_ARB_shader_storage_buffer_object : enable
#extension GL_ARB_compute_variable_group_size : enable





uniform float separationDistance;
uniform float alignmentDistance;
uniform float cohesionDistance;

uniform float delta;

// basic definition of all the properties that make up a "boid"
struct Boid
{
	// note that alpha will be used to hold an id value.
	vec4 position;
	vec4 acceleration;
	vec4 velocity;
};


// TODO get better random function 
float rand( vec2 co ){
	return fract( sin( dot( co.xy, vec2(12.9898,78.233) ) ) * 43758.5453 );
}


layout( std140, binding = 0 ) buffer Part
{
    Boid boids[];
};

//layout( local_size_variable ) in;
layout( local_size_x = 228, local_size_y = 1, local_size_z = 1 ) in;


void main(){
	uint gid = gl_GlobalInvocationID.x;	// The .y and .z are both 1 in this case.

	float PI = 3.141592653589793;
	float PI_2 = PI * 2;

	// current boid.
	Boid boid = boids[gid];
	

	

     const float SPEED_LIMIT = 5.0;
     float zoneRadius = 400.0;
     float separationThresh = separationDistance / zoneRadius;
     float alignmentThresh = ( separationDistance + alignmentDistance ) / zoneRadius;
     float zoneRadiusSquared = zoneRadius * zoneRadius;


     vec3 birdPosition, birdVelocity;

     vec3 selfPosition = boid.position.xyz;
     vec3 selfVelocity = boid.velocity.xyz;

     float dist;
     vec3 dir; // direction
     float distSquared;

     float f;
     float percent;

     vec3 velocity = selfVelocity;

     float limit = SPEED_LIMIT;

        // Attract flocks to the center
        vec3 central = vec3( 0., 0., 0. );
        dir = selfPosition - central;
        dist = length( dir );

        dir.y *= 2.5;
		
		if(dist > zoneRadius){
		  velocity -= normalize( dir ) * (delta + 0.5);
		}


        for ( int i = 0; i < boids.length() + 1; i++ ) {

			//if(i != boid.position.a) continue;
			
				birdPosition = boids[i].position.xyz;

                dir = birdPosition - selfPosition;
                dist = length( dir );

                if ( dist < 0.0000001 ) continue;

                distSquared = dist * dist;

                if ( distSquared > zoneRadiusSquared ) continue;

				 percent = distSquared / zoneRadiusSquared;

                if ( percent < separationThresh ) { // low

                    // Separation - Move apart for comfort
                    f = ( separationThresh / percent - 1.0 ) * delta;
                    velocity -= normalize( dir ) * f;

                } else if ( percent < alignmentThresh ) { // high

                    // Alignment - fly the same direction
                    float threshDelta = alignmentThresh - separationThresh;
                    float adjustedPercent = ( percent - separationThresh ) / threshDelta;

                    birdVelocity = boids[i].velocity.xyz;

                    f = ( 0.5 - cos( adjustedPercent * PI_2 ) * 0.5 + 0.5 ) * delta;
                    velocity += normalize( birdVelocity ) * f;

                } else {

                    // Attraction / Cohesion - move closer
                    float threshDelta = 1.0 - alignmentThresh;
                    float adjustedPercent = ( percent - alignmentThresh ) / threshDelta;

                    f = ( 0.5 - ( cos( adjustedPercent * PI_2 ) * -0.5 + 0.5 ) ) * delta;

                    velocity += normalize( dir ) * f;

                }
			
              
			
			
            

        } // end for loop



        // this make tends to fly around than down or up
        // if (velocity.y > 0.) velocity.y *= (1. - 0.2 * delta);

        // Speed Limits
        if ( length( velocity ) > limit ) {
            velocity = normalize( velocity ) * limit;
        }


	boid.velocity.xyz = velocity;
	boid.position += boid.velocity;

	// update boid. 
	boids[gid] = boid;
}

