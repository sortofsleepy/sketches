#version 420

uniform sampler2D uLastFrame;
out vec4 glFragColor;
in vec3 vColor;
void main(){
	glFragColor = vec4(vColor,1.);
}