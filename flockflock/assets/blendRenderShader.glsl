#version 450 

uniform sampler2D uTex0;
uniform vec2 resolution;
uniform float alphaVal;
in vec2 vUv;
out vec4 glFragColor;
void main (){
	vec2 uv = gl_FragCoord.xy / resolution.xy;
	vec4 frame = texture(uTex0,vUv);

	glFragColor = frame;
	//glFragColor = vec4(vUv,0.0,1.0);
	glFragColor.a = alphaVal;

}