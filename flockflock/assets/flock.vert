#version 330 
uniform float time;
uniform float separationDistance;
uniform float alignmentDistance;
uniform float cohesionDistance;

uniform float freedomFactor;
uniform vec2 resolution;
uniform vec2 readbackResolution;
uniform sampler2D positionsTexture;


#define PI 3.14159
#define PI_2 PI * 2.0

#define UPPER_BOUNDS 800
#define LOWER_BOUNDS UPPER_BOUNDS * -1
#define SPEED_LIMIT 9.0


in vec4 iPosition;
in vec4 iVelocity;
in vec4 iAcceleration;

out vec4 pos;
out vec4 vel;
out vec4 accel;



float rand( vec2 co ){
	return fract( sin( dot( co.xy, vec2(12.9898,78.233) ) ) * 43758.5453 );
}

// Largely based on three js sample, 
// transform feedback takes current position, use of texture demarcates the other particles positions. 

void main(){


	pos = iPosition;
	vel = iVelocity;
	accel = iAcceleration;

	float zoneRadius = 40.0;
	float zoneRadiusSquared = 1600.0;

	float separationThresh = 0.45;
	float alignmentThresh = 0.65;


	zoneRadius = separationDistance + alignmentDistance + cohesionDistance;
	separationThresh = separationDistance / zoneRadius;
	alignmentThresh = ( separationDistance + alignmentDistance ) / zoneRadius;
	zoneRadiusSquared = zoneRadius * zoneRadius;

	float dist;
	vec3 dir;
	float distSquared;

	float separationSquared = separationDistance * separationDistance;
	float cohesionSquared = cohesionDistance * cohesionDistance;

	float f;
	float percent;

	vec3 velocity = vel.xyz;

	float limit = SPEED_LIMIT;

	for(float y = 0.0; y < readbackResolution.y;++y){
		for(float x = 0.0; x < readbackResolution.x; ++x){

			vec2 ref = vec2( x + 0.5, y + 0.5 ) / readbackResolution.xy;
			vec4 data = texture(positionsTexture,ref);


			pos = data;
		}
	}


}