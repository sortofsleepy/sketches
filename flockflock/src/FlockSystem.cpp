#include "FlockSystem.h"
#include "cinder/Log.h"

using namespace ci;
using namespace std;

FlockSystem::FlockSystem(int numParticles) :
	delta(0),
	time(0),
	lastTime(0)
{




	settings.separationDistance = 2020.0;
	settings.alignmentdistance = 1020.0;
	settings.cohesionDistance = 1020.0;

	boids.resize(numParticles);
	setup();
}





void FlockSystem::setup()
{

	for(int i = 0; i < boids.size(); ++i)
	{
		Boid b;
		b.position = vec4(randVec3(), i);
		b.velocity = vec4(randVec3(), 1.);
		b.acceleration = vec4(randVec3(), 1.);

		boids[i] = b;
	}

	boidData = gl::Ssbo::create(boids.size() * sizeof(Boid), boids.data(), GL_DYNAMIC_DRAW);


	gl::GlslProg::Format fmt;
	fmt.compute(app::loadAsset("flockCompute.glsl"));

	boidShader = gl::GlslProg::create(fmt);
};

ci::vec3 FlockSystem::getPosition(int index)
{
	auto pos = new ci::vec3();
	boidData->getBufferSubData(sizeof(ci::vec3) * index, sizeof(ci::vec3), pos);

	return *pos;
}

void FlockSystem::update()
{

	time = app::getElapsedSeconds();
	delta = (time - lastTime) / 100.0;

	boidData->bindBase(0);
	gl::ScopedGlslProg shader(boidShader);

	//boidShader->uniform("time", time);
	boidShader->uniform("delta", delta);



	
	boidShader->uniform("separationDistance", settings.separationDistance);
	boidShader->uniform("alignmentDistance", settings.alignmentdistance);
	boidShader->uniform("cohesionDistance", settings.cohesionDistance);

	
	// may need to adjust work group size, just dispatch everything for now. 
	gl::dispatchCompute(boids.size(), 1, 1);
	gl::memoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);

	lastTime = time;
	

	/*
	 * for (int i = 0; i < boids.size(); ++i)
	{
		auto pos = getPosition(i);
		if (pos.x == 0 && pos.y == 0 && pos.z == 0)
		{
			CI_LOG_I("BAD INDEX IS " << i);
		}else
		{
			CI_LOG_I(pos);
		}
	}
	 */

}
void FlockSystem::draw(){}
