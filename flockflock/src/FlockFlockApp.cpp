#include "cinder/app/App.h"
#include "cinder/app/RendererGl.h"
#include "cinder/gl/gl.h"

#include "cinder/gl/Fbo.h"

#include "FlockSystem.h"
#include "ParticleSystem.h"
#include "mocha/core/Camera.h"
#include <cinder/Log.h>
#include "PostProcessor.h"

using namespace ci;
using namespace ci::app;
using namespace std;


const int SUBFRAMES = 16;

class FlockFlockApp : public App {
  public:
	void setup() override;
	void update() override;
	void updateRenderFbo();
	void draw() override;
	void setupScene();
	
	gl::FboRef fbos[2];
	size_t sourceIdx = 0;
	size_t destinationIdx = 1;

	gl::FboRef renderFbo;

	FlockSystemRef system;
	ParticleSystem ps;

	mocha::EasyCameraRef mCam;

	ci::gl::GlslProgRef mFrameShader;

	PostProcessor processor;
	PostProcessEffect hBlur,vBlur;
};

void FlockFlockApp::setup()
{
	setupScene();

	mCam = mocha::EasyCamera::create();
	mCam->setZoom(-40);

	mCam->enableUI();

	// setup the particle system to calculate positions. 
	system = FlockSystem::create();

	// setup the visual system
	ps.setup(system->getData());

	// setup the blur pre-process effect
	hBlur = PostProcessEffect("blur.glsl");
	vBlur = PostProcessEffect("blur.glsl");

	processor.addEffect(hBlur);
	processor.addEffect(vBlur);
	processor.compile(renderFbo->getColorTexture());
}


void FlockFlockApp::setupScene()
{

	gl::Texture::Format sceneTextureFormat;
	gl::Fbo::Format sceneFmt;

	sceneTextureFormat.setInternalFormat(GL_RGBA32F);
	sceneFmt.setColorTextureFormat(sceneTextureFormat);

	fbos[0] = gl::Fbo::create(app::getWindowWidth(), app::getWindowHeight(),sceneFmt);
	fbos[1] = gl::Fbo::create(app::getWindowWidth(), app::getWindowHeight(), sceneFmt);

	renderFbo = gl::Fbo::create(app::getWindowWidth(), app::getWindowHeight(), sceneFmt);

	//mFrameShader = gl::getStockShader(gl::ShaderDef().texture());

	gl::GlslProg::Format fmt;
	fmt.vertex(STRINGIFY(
		uniform mat4 ciModelViewProjection;
		in vec3 ciPosition;
		in vec2 ciTexCoord0;
		out vec2 vUv;
		void main()
		{
			vUv = ciTexCoord0;
			gl_Position = ciModelViewProjection * vec4(ciPosition, 1.);
		}
	));

	fmt.fragment(app::loadAsset("blendRenderShader.glsl"));

	mFrameShader = gl::GlslProg::create(fmt);

}


void FlockFlockApp::update()
{
	system->update();
	int time = app::getElapsedSeconds();


	
	gl::ScopedFramebuffer fbo(fbos[sourceIdx]);
	{

		gl::clear(ColorA(0, 0, 0));

		gl::enableAdditiveBlending();
		
		mCam->useCamera();
		ps.draw();
		gl::viewport(app::getWindowSize());
		gl::setMatricesWindow(app::getWindowSize());

		gl::ScopedTextureBind tex(fbos[destinationIdx]->getColorTexture(), 0);
		gl::ScopedGlslProg shader(mFrameShader);
		mFrameShader->uniform("uTex0", 0);
		mFrameShader->uniform("resolution", vec2(app::getWindowWidth(), app::getWindowHeight()));
		mFrameShader->uniform("alphaVal", 0.5f);
		gl::drawSolidRect(app::getWindowBounds());
	
		mFrameShader->uniform("alphaVal", 0.2f);
		gl::drawSolidRect(app::getWindowBounds());

	}

	
	

	std::swap(sourceIdx,destinationIdx);
}

void FlockFlockApp::updateRenderFbo()
{
	gl::ScopedFramebuffer fbo(renderFbo);
	{
		gl::clear(ColorA(0, 0, 0, 0.2));
		gl::viewport(app::getWindowSize());
		gl::setMatricesWindow(app::getWindowSize());

		gl::draw(fbos[destinationIdx]->getColorTexture(), app::getWindowBounds());
	}

	// update post processor
	processor.update();
	hBlur.uniform("sample_offset", vec2(1.0 / app::getWindowWidth(), 0.0));
	vBlur.uniform("sample_offset", vec2(0.0, 1.0 / app::getWindowHeight()));
	
	hBlur.uniform("attenuation", 2.5);
	vBlur.uniform("attenuation", 2.5);
}

void FlockFlockApp::draw()
{
	gl::clear( Color( 0, 0, 0 ) );
	gl::viewport(app::getWindowSize());
	gl::setMatricesWindow(app::getWindowSize());
	updateRenderFbo();
	//gl::draw(renderFbo->getColorTexture(), app::getWindowBounds());
	gl::draw(processor.getOutput(), app::getWindowBounds());
}

CINDER_APP( FlockFlockApp, RendererGl, [](App::Settings *settings) {
	settings->setWindowSize(1280, 720);
	settings->setMultiTouchEnabled(false);
})

