#include "PostProcessEffect.h"

using namespace ci;
using namespace std;
#define STRINGIFY(A) #A
PostProcessEffect::PostProcessEffect(ci::DataSourceRef shaderPath)
{

	gl::GlslProg::Format fmt;
	fmt.vertex(STRINGIFY(
		uniform mat4 ciModelViewProjection;
		in vec3 ciPosition;

		void main()
		{
			gl_Position = ciModelViewProjection * vec4(ciPosition, 1.);
		}
	
	));

	fmt.fragment(shaderPath);

	shader = gl::GlslProg::create(fmt);
}

PostProcessEffect::PostProcessEffect(std::string shaderPath)
{
	gl::GlslProg::Format fmt;
	fmt.vertex(STRINGIFY(
		uniform mat4 ciModelViewProjection;
		in vec3 ciPosition;

		void main()
		{
			gl_Position = ciModelViewProjection * vec4(ciPosition, 1.);
		}

	));

	fmt.fragment(app::loadAsset(shaderPath));

	shader = gl::GlslProg::create(fmt);
}

void PostProcessEffect::setup()
{
	gl::Fbo::Format fmt;
	gl::Texture::Format tfmt;

	tfmt.setInternalFormat(GL_RGBA32F);
	fmt.setColorTextureFormat(tfmt);

	fbo = gl::Fbo::create(app::getWindowWidth(), app::getWindowHeight(), fmt);
	
}

void PostProcessEffect::update()
{
	gl::ScopedFramebuffer _fbo(fbo);
	gl::ScopedGlslProg shd(shader);
	gl::ScopedTextureBind tex0(input);

	gl::clear(ColorA(0, 0, 0, 0));

	gl::ScopedViewport view(0, 0, app::getWindowWidth(), app::getWindowHeight());
	gl::ScopedMatrices matrices;
	gl::setMatricesWindow(app::getWindowSize());
	
	shader->uniform("uTex0", 0);
	shader->uniform("resolution", vec2(app::getWindowWidth(), app::getWindowHeight()));

	
	gl::drawSolidRect(app::getWindowBounds());
}

