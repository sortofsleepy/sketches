#include "ParticleSystem.h"

using namespace ci;
using namespace std;


void ParticleSystem::setup(ci::gl::SsboRef data,int numParticles)
{
	this->data = data;

	
	// generate ids so we can look up particles easily when rendering.
	ids.resize(numParticles);
	GLuint currId = 0;
	ids.push_back(0);
	std::generate(ids.begin(), ids.end(), [&currId]() -> GLuint { return currId++; });

	mIdsVbo = gl::Vbo::create<GLuint>(GL_ARRAY_BUFFER, ids, GL_STATIC_DRAW);


	auto geo = geom::Teapot();
	mMesh = gl::VboMesh::create(geo);


	geom::BufferLayout instanceDataLayout;
	instanceDataLayout.append(geom::Attrib::CUSTOM_0, 1, 0, 0, 1 /* per instance */);
	mMesh->appendVbo(instanceDataLayout, mIdsVbo);
	
	
	gl::GlslProg::Format fmt;
	fmt.vertex(app::loadAsset("flockRender.vert"));
	fmt.fragment(app::loadAsset("flockRender.frag"));
	mShader = gl::GlslProg::create(fmt);

	mBatch = gl::Batch::create(mMesh, mShader,{

		{geom::CUSTOM_0,"particleId"}
	});
		
}


void ParticleSystem::draw()
{


	gl::ScopedBuffer scopedParticleSsbo(data);

	// need to render one less cause otherwise it's always stuck at 0,0,0.
	// not sure why that is. 
	mBatch->drawInstanced(ids.size()-1);
	
}