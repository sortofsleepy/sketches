#pragma once

#include "cinder/gl/VboMesh.h"
#include "cinder/gl/GlslProg.h"
#include "cinder/gl/Batch.h"


class LineSystem
{
public:
	
	LineSystem() = default;

protected:

	ci::gl::BatchRef mBatch;
	ci::gl::GlslProgRef mShader;
	ci::gl::VboMeshRef mMesh;

	ci::gl::VboRef mPositionBuffer;
	ci::gl::VboRef mNormalBuffer;
	ci::gl::VboRef mTangentBuffer;
	ci::gl::VboRef mLengthBuffer;
};
