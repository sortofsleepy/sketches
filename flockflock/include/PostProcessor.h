#pragma once

#include <vector>
#include "PostProcessEffect.h"
class PostProcessor
{
public:
	PostProcessor() = default;


	PostProcessor& addEffect(PostProcessEffect effect)
	{
		effects.push_back(effect);
		return *this;
	}

	void compile(ci::gl::TextureRef input)
	{
		for(int i = 0; i < effects.size(); ++i)
		{
			if(i == 0)
			{
				effects[i].setInput(input);
			}else
			{
				effects[i].setInput(effects[i - 1].getOutput());
			}

		
		}
	}

	void update()
	{
		for(int i = 0; i < effects.size(); ++i)
		{
			effects[i].update();
		}
	}

	ci::gl::TextureRef getOutput() { return effects[effects.size() - 1].getOutput(); }
protected:
	std::vector<PostProcessEffect> effects;
};