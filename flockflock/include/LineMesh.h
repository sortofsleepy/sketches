#pragma once
#include <string>

#define STRINGIFY(A) # A 

class LineMesh
{
public:
	LineMesh() = default;

protected:
	std::string vertex;
};