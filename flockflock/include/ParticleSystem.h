#pragma once

#include <vector>
#include "cinder/app/App.h"
#include "cinder/gl/VboMesh.h"
#include "cinder/gl/GlslProg.h"
#include "cinder/gl/Batch.h"
#include "cinder/gl/gl.h"

class ParticleSystem
{
public:
	ParticleSystem() = default;
	void setup(ci::gl::SsboRef data,int numParticles = 100);
	void setData(ci::gl::SsboRef data) { this->data = data; }
	
	void draw();

	template<typename T>
	void uniform(std::string name, T value) { mShader->uniform(name, value); }
protected:
	std::vector<GLuint> ids;
	ci::gl::VboRef mIdsVbo;

	//! Particle data from compute shader which is generated from FlockSystem
	ci::gl::SsboRef data;

	ci::gl::VboMeshRef mMesh;
	ci::gl::GlslProgRef mShader;
	ci::gl::BatchRef mBatch;
};