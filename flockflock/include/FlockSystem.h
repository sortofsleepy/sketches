#pragma once

#include "cinder/Rand.h"
#include "cinder/Vector.h"
#include "cinder/gl/gl.h"
#include "cinder/gl/Ssbo.h"
#include <cinder/app/AppBase.h>
#include <memory>


#define STRINGIFY(A) #A

struct SystemSettings
{
	float upperBounds;
	float lowerBounds;
	float speedLimit;
	float separationDistance;
	float alignmentdistance;
	float cohesionDistance;
	float freedomFactor;

	float zoneRadius;
	float zoneRadiusSquared;
	float separationThreshold;
	float alignmentThreshold;

};
struct Boid
{
	// note that alpha will be used to hold an id value.
	ci::vec4 position;
	ci::vec4 acceleration;
	ci::vec4 velocity;
};

typedef std::shared_ptr<class FlockSystem>FlockSystemRef;

class FlockSystem
{
public:
	FlockSystem(int numParticles);

	static FlockSystemRef create(int num=100)
	{
		return FlockSystemRef(new FlockSystem(num));
	}

	void update();
	void draw();

	ci::gl::SsboRef getData() { return boidData; }

	ci::vec3 getPosition(int index);
	SystemSettings settings;
protected:
	void setup();
	float time;
	float delta;
	float lastTime;
	
	ci::gl::SsboRef boidData;
	std::vector<Boid> boids;

	ci::gl::GlslProgRef boidShader;
	

};