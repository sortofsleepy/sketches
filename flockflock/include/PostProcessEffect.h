#pragma once

#include "cinder/gl/GlslProg.h"
#include "cinder/gl/Fbo.h"
#include "cinder/app/AppBase.h"
#include "cinder/gl/gl.h"

class PostProcessEffect
{
public:
	PostProcessEffect() = default;
	PostProcessEffect(std::string shaderPath);
	PostProcessEffect(ci::DataSourceRef shaderPath);

	void update();

	//! Set the input texture for this stage. 
	void setInput(ci::gl::TextureRef input)
	{
		this->input = input;
		setup();
	}

	//! Set a uniform on the shader
	void uniform(std::string name, ci::vec2 value)
	{
		shader->uniform(name, value);
	}

	//! Add same function for floats (templates aren't working for some reason)
	void uniform(std::string name, float value)
	{
		shader->uniform(name, value);
	}
	ci::gl::TextureRef getOutput() { return fbo->getColorTexture(); }
protected:
	void setup();
	ci::gl::FboRef fbo;
	ci::gl::GlslProgRef shader;
	ci::gl::TextureRef input;
};