#pragma once

#include "cinder/gl/VboMesh.h"
#include "cinder/gl/GlslProg.h"
#include "cinder/gl/Batch.h"
#include "cinder/gl/Vbo.h"
#include "cinder/Rand.h"
#include "mocha/core/core.h"

class CubeField {
	ci::gl::VboRef fieldData, offsetData;

	ci::gl::VboMeshRef mMesh;
	ci::gl::GlslProgRef mShader;
	ci::gl::BatchRef mBatch;

	int instanceCount;
	float width, height, depth;

	ci::gl::TextureRef diffuseMap;
	ci::mat4 mCubeRotation;
public:
	CubeField(int width=40,int height=40,int depth=40, int resolution=8);
	~CubeField();

	void draw();
};

