#pragma once
#include <vector>
#include "cinder/gl/Vbo.h"
#include "cinder/gl/Vao.h"
#include "cinder/Vector.h"
#include "cinder/gl/gl.h"
#include "mocha/core/core.h"
#include <memory>
#include "mocha/core/ArcCam.hpp"
#include "cinder/Thread.h"
using namespace ci;
using namespace std;
class Cube {

	ci::gl::VaoRef mVao;
	ci::gl::VboRef mPositions, mIndices;
	ci::gl::GlslProgRef mShader;
	std::shared_ptr<std::thread> mThread;
	std::mutex mMutex;
	vector<float> positions;
	vector<uint32_t> indices;
public:
	Cube() {
	}
	void update() {
		while (1) {

			CI_LOG_I("update in cube");
		}
	}
	void setup() {

		//mThread = shared_ptr<thread>(new thread(bind(&Cube::update, this)));
		//mThread->detach();
	
		positions = {
			// Front face
			-1.0, -1.0,  1.0,
			1.0, -1.0,  1.0,
			1.0,  1.0,  1.0,
			-1.0,  1.0,  1.0,

			// Back face
			-1.0, -1.0, -1.0,
			-1.0,  1.0, -1.0,
			1.0,  1.0, -1.0,
			1.0, -1.0, -1.0,

			// Top face
			-1.0,  1.0, -1.0,
			-1.0,  1.0,  1.0,
			1.0,  1.0,  1.0,
			1.0,  1.0, -1.0,

			// Bottom face
			-1.0, -1.0, -1.0,
			1.0, -1.0, -1.0,
			1.0, -1.0,  1.0,
			-1.0, -1.0,  1.0,

			// Right face
			1.0, -1.0, -1.0,
			1.0,  1.0, -1.0,
			1.0,  1.0,  1.0,
			1.0, -1.0,  1.0,

			// Left face
			-1.0, -1.0, -1.0,
			-1.0, -1.0,  1.0,
			-1.0,  1.0,  1.0,
			-1.0,  1.0, -1.0
		};

		indices = {
			0,  1,  2,      0,  2,  3,    // front
			4,  5,  6,      4,  6,  7,    // back
			8,  9,  10,     8,  10, 11,   // top
			12, 13, 14,     12, 14, 15,   // bottom
			16, 17, 18,     16, 18, 19,   // right
			20, 21, 22,     20, 22, 23    // left
		};
		mVao = gl::Vao::create();
	
		gl::ScopedVao v(mVao);

		mPositions = gl::Vbo::create(GL_ARRAY_BUFFER, positions.size() * sizeof(float), positions.data(), GL_DYNAMIC_DRAW);
		mIndices = gl::Vbo::create(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(uint32_t), indices.data(), GL_DYNAMIC_DRAW);

		mPositions->bind();
		gl::enableVertexAttribArray(0);
		gl::vertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);
		mPositions->unbind();

	
		mShader = mocha::loadShader("cubetest.vert", "cubetest.frag");

	}

	void draw(ArcCamRef mCam) {
		gl::ScopedVao v(mVao);
		gl::ScopedGlslProg shd(mShader);
		gl::ScopedBuffer idx(mIndices);
		//mCam->useMatrices();
		mShader->uniform("projection", mCam->getProjection());
		mShader->uniform("modelView", mCam->getView());
	

		gl::drawElements(GL_TRIANGLE_STRIP, indices.size() * 3, GL_UNSIGNED_SHORT, (GLvoid*) 0);

	}
};