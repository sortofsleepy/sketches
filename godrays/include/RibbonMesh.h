#pragma once

#include <vector>
#include "cinder/gl/Vbo.h"
#include "cinder/gl/Vao.h"
#include "cinder/Vector.h"
#include "cinder/gl/gl.h"
#include "mocha/core/core.h"
#include <memory>
#include "mocha/core/ArcCam.hpp"

const int POSITION_INDEX = 0;
const int PREVIOUS_POSITION_INDEX = 1;
const int NEXT_INDEX = 2;
const int SIDE_INDEX = 3;
const int WIDTH_INDEX = 4;
const int COUNTER_INDEX = 5;

typedef std::shared_ptr<class RibbonMesh>RibbonMeshRef;
// this is basiclly a port of Three.Meshline
// https://github.com/spite/THREE.MeshLine/blob/master/src/THREE.MeshLine.js
class RibbonMesh
{

	std::vector<float> positions;
	std::vector<uint32_t> indices;
	std::vector<ci::vec2> uvs;

	std::vector<float> previous;
	std::vector<float> next;
	std::vector<float> side;
	std::vector<float> width;
	std::vector<float> counters;

	float opacity;
	float near;
	float far;
	float lineWidth;
	float sizeAttenuation;
	ci::vec2 resolution;

	ci::gl::VaoRef mVao;
	ci::gl::VboRef mPositions, mPrevious, mNext, mSide, mWidth, mCounters,mIndices;
	ci::gl::GlslProgRef mShader;
private:
	void process();
	bool compareV3(int a, int b);
	ci::vec3 copyV3(int a);
	void copy(std::vector<float> &src, int srcOffset, std::vector<float> &dst, int dstOffset, int length);

public:
	RibbonMesh();
	~RibbonMesh();
	void update(ci::vec3);
	static RibbonMeshRef create() {
		return RibbonMeshRef(new RibbonMesh());
	}
	void draw(ArcCamRef mCam);
	void setData(std::vector<ci::vec3> &data);
};

