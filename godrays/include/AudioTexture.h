#pragma once

#include "cinder/audio/Context.h"
#include "cinder/gl/Texture.h"

class AudioTexture {
	
	ci::gl::TextureRef mTexture;

public:
	AudioTexture();
};