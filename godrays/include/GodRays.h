#pragma once

#include "cinder/gl/Texture.h"
#include "mocha/post/FxPass.h"
#include "mocha/post/FxComposer.h"


using namespace ci;
using namespace std;

#define TAPS_PER_PASS 6.0
class GodRays {

	// generator pass for the Godrays scene.
	FxPassRef generate;

	// composer to handle compositing
	FxComposer composer;

	// position of our "sun"
	ci::vec2 sunPosition;

	// position of our lighting
	ci::vec2 screenSpacePosition;
	
	// flag for whether or not we've set an input image
	bool generateSet = false;

	// filter length
	float filterLen = 1.0;
	float stepLen = 0.0;

	ci::gl::GlslProgRef mGenShader,mCombineShader;

	// input texture to overlay godrays on
	gl::TextureRef mTex;
public:
	GodRays() {
	
		generate = FxPass::create("grayinput.frag", "graypass.vert");


		gl::GlslProg::Format gfmt;
		gfmt.vertex(app::loadAsset("graypass.vert"));
		gfmt.fragment(app::loadAsset("grayinput.frag"));

		gl::GlslProg::Format cfmt;
		cfmt.vertex(app::loadAsset("graypass.vert"));
		cfmt.fragment(app::loadAsset("graycombine.frag"));

		mGenShader = gl::GlslProg::create(gfmt);
		mCombineShader = gl::GlslProg::create(cfmt);
	}


	void setInputTexture(gl::TextureRef mTex) {
		this->mTex = mTex;
		generateSet = true;
	}

	
	void update() {
	
		// setup uniforms for generate pass
		screenSpacePosition = sunPosition;
		screenSpacePosition.x = (screenSpacePosition.x + 1) / 2;
		screenSpacePosition.y = (screenSpacePosition.y + 1) / 2;

		if (generateSet) {
			auto shader = generate->getShader();
			auto bounds = generate->getBounds();

			
			gl::pushMatrices();
			gl::setMatricesWindow(app::getWindowSize());
			gl::viewport(app::getWindowSize());
		
			shader->bind();
			shader->uniform("vSunPositionScreenSpace", screenSpacePosition);


			// pass 1
			stepLen = filterLen * pow(TAPS_PER_PASS, -1.0);
			generate->bind(1);
			mTex->bind();
			shader->uniform("fStepSize", stepLen);
			shader->uniform("tInput", 0);
			gl::drawSolidRect(bounds);
			mTex->unbind();
			generate->unbind();

			// pass 2
			stepLen = filterLen * pow(TAPS_PER_PASS, -2.0);
			generate->bind(0);
			generate->getTextureAttachment(1)->bind();
			shader->uniform("fStepSize", stepLen);
			shader->uniform("tInput", 0);
			gl::drawSolidRect(bounds);
			generate->getTextureAttachment(1)->unbind();
			generate->unbind();


			// pass 3
			stepLen = filterLen * pow(TAPS_PER_PASS, -3.0);
			generate->bind(1);
			generate->getTextureAttachment(0)->bind();
			shader->uniform("fStepSize", stepLen);
			shader->uniform("tInput", 0);
			gl::drawSolidRect(bounds);
			generate->getTextureAttachment(0)->unbind();
			generate->unbind();
		}
	
	
	}

	void draw(gl::TextureRef mColors) {

		gl::ScopedGlslProg shd(mCombineShader);
		gl::ScopedTextureBind grays(generate->getOutput(),0);
		gl::ScopedTextureBind colors(mColors, 1);

		// render final scene
		gl::setMatricesWindow(app::getWindowSize());
		gl::viewport(app::getWindowSize());
	
		gl::color(Color(1, 1, 0));
		mCombineShader->uniform("fGodRayIntensity", 0.75f);
		mCombineShader->uniform("tGodRays", 0);
		mCombineShader->uniform("tColors", 1);
		gl::drawSolidRect(Rectf(0, 0,app::getWindowWidth(), app::getWindowHeight()));
	}
};
