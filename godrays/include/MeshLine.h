#pragma once

#include "cinder/gl/VboMesh.h"
#include "cinder/gl/Batch.h"
#include "mocha/core/core.h"

typedef std::shared_ptr<class MeshLine>MeshLineRef;

/*
	Essentially a port of Three.MeshLine
*/
class MeshLine {

	ci::gl::VboMeshRef mMesh;
	ci::gl::GlslProgRef mShader;
	ci::gl::BatchRef mBatch;

	std::vector<float> positions;
	std::vector<uint32_t> indices;
	std::vector<ci::vec2> uvs;

	std::vector<float> previous;
	std::vector<float> next;
	std::vector<float> side;
	std::vector<float> width;
	std::vector<float> counters;

	float opacity;
	float near;
	float far;
	float lineWidth;
	float sizeAttenuation;
	ci::vec2 resolution;

	std::vector<float> mChunk;

private:
	void process();
	bool compareV3(int a, int b);
	ci::vec3 copyV3(int a);
	void copy(std::vector<float> &src,int srcOffset, std::vector<float> &dst, int dstOffset,int length);

public:
	MeshLine();
	~MeshLine();

	static MeshLineRef create() {
		return MeshLineRef(new MeshLine());
	}
	void setLineWidth(float lineWidth);
	void clear();
	void setData(std::vector<ci::vec3> &data);
	void draw();
	void draw(ci::gl::TextureRef mDiffuse);

	// Updates mesh with new values
	void updateMesh();

	// updates the line to a new position
	void update(ci::vec3 newPos = ci::vec3(0.0));

	
};

