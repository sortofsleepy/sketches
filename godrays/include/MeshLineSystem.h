#pragma once

#include "MeshLine.h"
#include <vector>
#include "cinder/Rand.h"
#include "cinder/gl/Context.h"
#include "cinder/Thread.h"
#include "cinder/Function.h"
#include "mocha/core/ArcCam.hpp"
typedef struct {
	MeshLineRef line;
	//RibbonMeshRef line;

	float theta;
	float phi;
	float thetaSpeed;
	float phiSpeed;
	float radius;
}Ribbon;

class MeshLineSystem {

	std::vector<Ribbon> lines;
	std::shared_ptr<std::thread> mThread;
	std::mutex mMutex;
	bool updateLines;
	int times = 0;
	bool hasDiffuse;
	ci::gl::TextureRef mDiffuse;
public:
	MeshLineSystem();
	void setup(int numlines = 1, int radius = 10);
	void update();
	void setDiffuseTexture(std::string texpath);
	void stop();
	void draw(ArcCamRef mCam);

	void toggleLineUpdate();
	~MeshLineSystem();
};

