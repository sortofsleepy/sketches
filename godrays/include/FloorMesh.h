#pragma once

#include "cinder/gl/VboMesh.h"
#include "cinder/gl/Batch.h"
#include "cinder/gl/GlslProg.h"
#include "mocha/core/core.h"
#include <memory>


typedef std::shared_ptr<class FloorMesh> FloorMeshRef;

class FloorMesh {

	ci::gl::VboMeshRef mMesh;
	ci::gl::GlslProgRef mShader;
	ci::gl::BatchRef mBatch;

	float elevation;
	float scale_first_noise;
	float scale_second_noise;

	ci::gl::TextureRef mNoiseTex;
	float time;
	void build(int width, int height, int widthSegments, int heightSegments);

public:
	FloorMesh();
	static FloorMeshRef create(int width=10, int height=10, int widthSegments=1, int heightSegments=1) {
		auto mesh = new FloorMesh();
		mesh->build(width, height, widthSegments, heightSegments);
		return FloorMeshRef(mesh);
	}
	void draw(std::function<void()> fn=nullptr);
};
