#version 150
#define ANIMATE
uniform vec2 iResolution;
uniform sampler2D noiseTex;
uniform float uTime;

in vec2 vCenter;
in float v_Height;
out vec4 glFragColor;

const float speed = 0.5;
//const vec2 center = vec2(0.5,0.5);

vec3 HUEtoRGB(float H){
	H = mod(H,1.0);
	float R = abs(H * 6.0 - 3.0) - 1.0;
	float G = 2.0 - abs(H * 6.0 - 2.0);
	float B = 2.0 - abs(H * 6.0 - 4.0);
	return clamp(vec3(R,G,B),0.0,1.0);
}
vec3 HSLtoRGB(vec3 HSL){
	vec3 RGB = HUEtoRGB(HSL.x);
	float C = (1.0 - abs(2.0 * HSL.z - 1.0)) * HSL.y;
	return (RGB - 0.5) * C + HSL.z;
}
void main(){
	float invAr = iResolution.y / iResolution.x;
    vec2 uv = gl_FragCoord.xy / iResolution.xy;
	
	vec2 middle = vec2(0.5,0.5);
	vec2 center = middle;
	
	float x = (center.x-uv.x);
    float y = (center.y-uv.y);

    float r = -(x*x + y*y);
	float z = 1.0 + 0.5*sin((r + uTime * speed) / 0.1);
    float distanceFromCenter = sqrt(x * x + y * y);
	float sinArg = distanceFromCenter * 10.0 - uTime;
	float slope = cos(sinArg) ;
    
	vec3 texCol = vec3(z);

	vec2 coords = uv + (vec2(x, y) + texCol.xy) * slope;
	vec4 tex =texture(noiseTex, coords);
	
	vec4 fColor = vec4( tex.xyz * HSLtoRGB(vec3(x,y, 0.5)), tex.w );

   	float n = v_Height; //the height of our displacement define the color of the vertices
	vec3 col = vec3(n * z);
	
	col.x = mix(col.x * fColor.y * col.z,sin(tex.y * 0.005),0.6);
	col.x = mix(col.y * fColor.z * col.x,cos(tex.x * 0.005),0.6);
	
	vec4 terr = vec4(HSLtoRGB(col) * 4.0, 1.);
	glFragColor = terr;
	


}