#version 150

uniform sampler2D depthTex;
in vec2 vUv;
in vec4 vPos;
out vec4 glFragColor;

float LinearizeDepth(float zoverw)  
{  
	float n = 1.0; // camera z near  
	float f = 10000.0; // camera z far  
	return (2.0 * n) / (f + n - zoverw * (f - n));  
}  


void main(){
	float d = texture(depthTex,vUv).r;
	//d = LinearizeDepth(d);
	glFragColor = normalize(vec4(vec3(d),1.));
}