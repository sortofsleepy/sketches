#version 150
uniform sampler2D diffuseMap;

in vec2 vUv;
in vec4 vPos;
in float vScale;
in float vOffset;
out vec4 glFragColor;

// HSL to RGB Convertion helpers
vec3 HUEtoRGB(float H){
	H = mod(H,1.0);
	float R = abs(H * 6.0 - 3.0) - 1.0;
	float G = 2.0 - abs(H * 6.0 - 2.0);
	float B = 2.0 - abs(H * 6.0 - 4.0);
	return clamp(vec3(R,G,B),0.0,1.0);
}
vec3 HSLtoRGB(vec3 HSL){
	vec3 RGB = HUEtoRGB(HSL.x);
	float C = (1.0 - abs(2.0 * HSL.z - 1.0)) * HSL.y;
	return (RGB - 0.5) * C + HSL.z;
}

void main(){
	vec4 diffuseColor = texture(diffuseMap,vUv);

	vec4 fColor = vec4( diffuseColor.xyz * HSLtoRGB(vec3(vScale/vOffset, 1.0, 0.5)), diffuseColor.w );

	glFragColor = fColor;
	//glFragColor = vec4(0.);
	glFragColor.a = 1.0;


}