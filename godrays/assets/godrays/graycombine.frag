#version 150

uniform sampler2D tColors;
uniform sampler2D tGodRays;
uniform vec2 vSunPositionScreenSpace;
uniform float fGodRayIntensity;

in vec2 vUv;

out vec4 glFragColor;

void main(){
	vec4 gData =  texture(tGodRays,vUv);
	vec4 colors = texture(tColors,vUv);

	glFragColor = colors * colors + fGodRayIntensity * vec4(1. - gData.r);

}