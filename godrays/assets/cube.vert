#version 150

uniform mat4 ciModelViewProjection;
uniform float time;

in vec4 ciPosition;
in vec2 ciTexCoord0;
in vec3 fieldPosition;
in float offset;

out vec2 vUv;
out vec4 vPos;
out float vScale;
out float vOffset;


vec3 rotateX(vec3 p, float theta) {
  float s = sin(theta);
  float c = cos(theta);
  return vec3(p.x, p.y * c - p.z * s, p.z * c + p.y * s);
}

vec3 rotateY(vec3 p, float theta) {
  float s = sin(theta);
  float c = cos(theta);
  return vec3(p.x * c + p.z * s, p.y, p.z * c - p.x * s);
}

vec3 rotateZ(vec3 p, float theta) {
  float s = sin(theta);
  float c = cos(theta);
  return vec3(p.x * c - p.y * s, p.y * c + p.x * s, p.z);
}
void main (){
	vec4 coreShape = ciPosition;
	
	coreShape.xyz = rotateX(coreShape.xyz,time * offset);
	coreShape.xyz = rotateY(coreShape.xyz,time * offset);
	coreShape.xyz = rotateZ(coreShape.xyz,time * offset);

	coreShape.x *= sin(time * offset) * 2.0;
	coreShape.y *= sin(time * offset) * 2.0;
	coreShape.z *= sin(time * offset) * 2.0;
	
	
	vec4 pos = coreShape + vec4(fieldPosition,0.0);
	
	
	vScale = sin(time * offset) * 2.0;
    vPos = pos;
    vUv = ciTexCoord0;
	vOffset = offset;
    gl_Position = ciModelViewProjection * pos;

}