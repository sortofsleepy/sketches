
#version 150
uniform mat4 ciModelViewProjection;
uniform mat4 projection;
uniform mat4 modelView;
in vec3 ciPosition;

void main(){
	//gl_Position = ciModelViewProjection * vec4(ciPosition,1.);
	gl_Position = projection * modelView * vec4(ciPosition,1.);
}