
#version 150

uniform mat4 ciModelViewProjection;

uniform float uTime;
uniform float u_elevation;
uniform float u_scale_first_noise;
uniform float u_scale_second_noise;

#include "noise.glsl"


in vec3 ciPosition;
in vec2 ciTexCoord0;
out float v_Height;
out vec2 vUv;
out vec2 vCenter;


float hash(in float n) { return fract(sin(n)*43758.5453123); }

float hash(vec2 p){
	return fract(sin(dot(p,vec2(127.1,311.7))) * 43758.5453123);
}

float noise(vec2 p){
	vec2 i = floor(p), f = fract(p); 
    f *= f*(3.-2.*f);

   vec2 c = vec2(0,1);

   return mix(mix(hash(i + c.xx), 
                       hash(i + c.yx), f.x),
                   mix(hash(i + c.xy), 
                       hash(i + c.yy), f.x), f.y);
}

float fbm(in vec2 p){
	vec3 val = vec3(p,0.0);
	float simplex = noise3(val);
	float f =.5000 * noise(p)
           +.2500 * noise(p * 2.)
           +.1250 * noise(p * 4.)
           +.0625 * noise(p * 8.);

	return f + simplex * 0.5;

}

void main() {
      v_Height = 0.1 * fbm(ciPosition.xy * u_scale_first_noise + uTime) * u_elevation; // Fractal noise
      v_Height -= fbm(ciPosition.xy * u_scale_second_noise - uTime) * 1.0;
  
      float wv = 1.0-abs(sin(v_Height));
      float swv = abs(cos(v_Height));    
      
      v_Height = pow(1.0-pow(wv,0.1), 1.);

      //using two noises we define the displacement of our vertices
       
			
	  vec3 modPos = ciPosition.xyz - vec3(0., 0.,v_Height) * u_elevation;
	  vCenter = modPos.xy;
	 gl_Position = ciModelViewProjection * vec4(modPos,1.);
}