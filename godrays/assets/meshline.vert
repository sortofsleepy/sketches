#version 150

uniform mat4 ciProjectionMatrix;
uniform mat4 ciModelView;

uniform vec2 resolution;
uniform float lineWidth;
uniform vec3 color;
uniform float opacity;
uniform float near;
uniform float far;
uniform float sizeAttenuation;

in vec3 ciPosition;
in vec2 ciTexCoord0;
in vec3 next;
in vec3 previous;
in float side;
in float width;
in float counters;

out vec2 vUv;
out vec4 vPos;
out float vCounters;



vec2 fix( vec4 i, float aspect ) {
	vec2 res = i.xy / i.w;
	res.x *= aspect;
	vCounters = counters;

    return res;
}

void main(){
	
	float aspect = resolution.x / resolution.y;
	float pixelWidthRatio = 1. / (resolution.x * ciProjectionMatrix[0][0]);

	
	mat4 m = ciProjectionMatrix * ciModelView;
	vec4 finalPosition = m * vec4(ciPosition,1.);
	vec4 prevPos = m * vec4(previous,1.);
	vec4 nextPos = m * vec4(next,1.);

	vec2 currentP = fix(finalPosition,aspect);
	vec2 prevP = fix(prevPos,aspect);
	vec2 nextP = fix(nextPos,aspect);
	
	float pixelWidth = finalPosition.w * pixelWidthRatio;
	float w = 1.8 * pixelWidth * lineWidth * width;
	
	if(sizeAttenuation == 1.){
		w = 1.8 * lineWidth * width;
	} 

	vec2 dir;
	if(nextP == currentP) dir = normalize(currentP - prevP);
	else if(prevP == currentP) dir = normalize(nextP - currentP);
	else {
		vec2 dir1 = normalize(currentP - prevP);
		vec2 dir2 = normalize(nextP - currentP);
		dir = normalize(dir1 + dir2);

		vec2 perp = vec2(-dir1.y,dir1.x);
		vec2 miter = vec2(-dir.y,dir.x);

	}

	vec2 normal = vec2(-dir.y,dir.x);
	normal.x /= aspect;
	normal *= .5 * w;

	vec4 offset = vec4(normal * side,0.0,1.0);
	finalPosition.xy += offset.xy;

	vUv = ciTexCoord0;

	gl_Position = finalPosition;
}