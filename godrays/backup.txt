#include "MeshLine.h"
using namespace ci;
using namespace std;


MeshLine::MeshLine():
	lineWidth(1.0f),
	near(1.0f),
	far(1.0f),
	sizeAttenuation(1.0f)
{
	resolution = vec2(app::getWindowWidth(), app::getWindowHeight());
}


MeshLine::~MeshLine()
{
}

// === PRIVATE FUNCTIONS ==== //
bool MeshLine::compareV3(int a, int b) {
	auto aa = a * 6;
	auto ab = b * 6;
	auto val = false;


	if (positions[aa] == positions[ab]) {
		if (positions[aa + 1] == positions[ab + 1]) {
			if (positions[aa + 2] == positions[ab + 2]) {
				val = true;
			}
		}
	}

	return val;
}

ci::vec3 MeshLine::copyV3(int a) {
	auto aa = a * 6;
	auto x = positions[aa];
	auto y = positions[aa + 1];
	auto z = positions[aa + 2];
	return vec3(x, y, z);
}


void MeshLine::setData(std::vector<ci::vec3> &data) {

	for (int i = 0; i < data.size(); ++i) {
		auto vec = data[i];
		auto c = i / (data.size());
		positions.push_back(vec.x);
		positions.push_back(vec.y);
		positions.push_back(vec.z);

		positions.push_back(vec.x);
		positions.push_back(vec.y);
		positions.push_back(vec.z);

		counters.push_back(c);
		counters.push_back(c);

	}
	process();
}

void MeshLine::draw() {

	
	mBatch->getGlslProg()->uniform("resolution", resolution);
	mBatch->getGlslProg()->uniform("lineWidth", lineWidth);
	mBatch->getGlslProg()->uniform("opacity", opacity);
	mBatch->getGlslProg()->uniform("near", near);
	mBatch->getGlslProg()->uniform("far", far);
	mBatch->getGlslProg()->uniform("sizeAttenuation", sizeAttenuation);

	mBatch->draw();

}

void MeshLine::clear() {
	positions.clear();
	indices.clear();
	uvs.clear();
}

void MeshLine::process() {
	
	auto l = positions.size() / 6;


	for (int j = 0; j < l; ++j) {
		side.push_back(1);
		side.push_back(-1);
	}

	for (int j = 0; j < l; ++j) {
		width.push_back(1);
		width.push_back(1);
	}

	for (int j = 0; j < l; ++j) {
		auto x = j / (l - 1);
		auto y = 0;
		uvs.push_back(vec2(x, y));
		uvs.push_back(vec2(x, 1));

	}

	auto v = vec3();
	if (compareV3(0, l - 1)) {
		v = copyV3(l - 2);
	}
	else {
		v = copyV3(0);
	}

	previous.push_back(v.x);
	previous.push_back(v.y);
	previous.push_back(v.z);


	previous.push_back(v.x);
	previous.push_back(v.y);
	previous.push_back(v.z);

	for (int j = 0; j < l - 1; ++j) {
		v = copyV3(j);
		previous.push_back(v.x);
		previous.push_back(v.y);
		previous.push_back(v.z);

		previous.push_back(v.x);
		previous.push_back(v.y);
		previous.push_back(v.z);
	}

	for (int j = 1; j < l; ++j) {
		v = copyV3(j);
		next.push_back(v.x);
		next.push_back(v.y);
		next.push_back(v.z);

		next.push_back(v.x);
		next.push_back(v.y);
		next.push_back(v.z);
	}

	if (compareV3(l - 1, 0)) {
		v = copyV3(1);
	}
	else {
		v = copyV3(l - 1);
	}


	next.push_back(v.x);
	next.push_back(v.y);
	next.push_back(v.z);

	next.push_back(v.x);
	next.push_back(v.y);
	next.push_back(v.z);

	for (int j = 0; j < l - 1; ++j) {
		auto n = j * 2;
		indices.push_back(n);
		indices.push_back(n + 1);
		indices.push_back(n + 2);
		indices.push_back(n + 2);
		indices.push_back(n + 1);
		indices.push_back(n + 3);
	}


	gl::VboMesh::Layout layout;
	layout.attrib(geom::POSITION, 3);

	// next
	layout.attrib(geom::CUSTOM_0, 3);

	// previous
	layout.attrib(geom::CUSTOM_1, 3);

	// side 
	layout.attrib(geom::CUSTOM_2, 1);

	// width 
	layout.attrib(geom::CUSTOM_3, 1);

	// counters
	layout.attrib(geom::CUSTOM_4, 1);

	layout.usage(GL_DYNAMIC_DRAW);
	


	mMesh = gl::VboMesh::create(positions.size(), GL_TRIANGLES, { layout }, indices.size(), GL_UNSIGNED_INT);
	
	mMesh->bufferAttrib(geom::POSITION, sizeof(float) * positions.size(), positions.data());
	mMesh->bufferAttrib(geom::CUSTOM_0, sizeof(float) * next.size(), next.data());
	mMesh->bufferAttrib(geom::CUSTOM_1, sizeof(float) * previous.size(),previous.data());
	mMesh->bufferAttrib(geom::CUSTOM_2, sizeof(float) * side.size(), side.data());
	mMesh->bufferAttrib(geom::CUSTOM_3, sizeof(float) * width.size(),width.data());
	mMesh->bufferAttrib(geom::CUSTOM_4, sizeof(float) * counters.size(), counters.data());
	mMesh->bufferIndices(sizeof(uint32_t) * indices.size(), indices.data());

	mShader = mocha::loadShader("meshline.vert", "meshline.frag");
	mBatch = gl::Batch::create(mMesh, mShader, {
		{geom::CUSTOM_0,"next"},
		{geom::CUSTOM_1,"previous"},
		{geom::CUSTOM_2,"side"},
		{geom::CUSTOM_3,"width"},
		{geom::CUSTOM_4,"counters"}
	});


}

void MeshLine::update(ci::vec3 newPos) {
	auto mesh = mBatch->getVboMesh();

	auto l = positions.size();
	auto position = newPos;

	// PREVIOUS
	copy(positions, 0, previous, 0, l);

	// POSITIONS
	copy(positions, 6, positions, 0, l - 6);

	positions[l - 6] = position.x;
	positions[l - 5] = position.y;
	positions[l - 4] = position.z;
	positions[l - 3] = position.x;
	positions[l - 2] = position.y;
	positions[l - 1] = position.z;



	// NEXT
	copy(positions, 6, next, 0, l - 6);

	next[l - 6] = position.x;
	next[l - 5] = position.y;
	next[l - 4] = position.z;
	next[l - 3] = position.x;
	next[l - 2] = position.y;
	next[l - 1] = position.z;
	
	
	updateMesh();
}

void MeshLine::updateMesh() {
	auto mesh = mBatch->getVboMesh();
	
	
	mesh->bufferAttrib(geom::POSITION, sizeof(float) * positions.size(), positions.data());
	//mesh->bufferAttrib(geom::CUSTOM_0, sizeof(float) * next.size(), next.data());
	mesh->bufferAttrib(geom::CUSTOM_1, sizeof(float) * previous.size(), previous.data());

}

void MeshLine::copy(std::vector<float> &src, int srcOffset, std::vector<float> &dst, int dstOffset, int length) {
	vector<float> newSrc;
	vector<float>::const_iterator first = src.begin() + srcOffset;
	vector<float>::const_iterator last = src.begin() + (srcOffset + length);
	newSrc = vector<float>(first, last);

	for (int j = 0; j < newSrc.size(); ++j) {
		dst[j + dstOffset] = newSrc[j];
	}
} 


//////////////////////////////
#pragma once

#include "cinder/gl/VboMesh.h"
#include "cinder/gl/Batch.h"
#include "mocha/core/core.h"

typedef std::shared_ptr<class MeshLine>MeshLineRef;

class MeshLine {

	ci::gl::VboMeshRef mMesh;
	ci::gl::GlslProgRef mShader;
	ci::gl::BatchRef mBatch;

	std::vector<float> positions;
	std::vector<uint32_t> indices;
	std::vector<ci::vec2> uvs;

	std::vector<float> previous;
	std::vector<float> next;
	std::vector<float> side;
	std::vector<float> width;
	std::vector<float> counters;

	float opacity;
	float near;
	float far;
	float lineWidth;
	float sizeAttenuation;
	ci::vec2 resolution;


private:
	void process();
	bool compareV3(int a, int b);
	ci::vec3 copyV3(int a);
	void copy(std::vector<float> &src,int srcOffset, std::vector<float> &dst, int dstOffset,int length);

public:
	MeshLine();
	~MeshLine();

	static MeshLineRef create() {
		return MeshLineRef(new MeshLine());
	}
	void clear();
	void setData(std::vector<ci::vec3> &data);
	void draw();

	void updateMesh();

	// updates the line to a new position
	void update(ci::vec3 newPos = ci::vec3(0.0));

	
};

