#include "mocha/core/ArcCam.hpp"
using namespace ci;
using namespace std;
using namespace ci::app;
ArcCam::ArcCam(float fov, float aspect, float near, float far, vec3 eye, vec3 target) {

	mCam = CameraPersp(getWindowWidth(), getWindowHeight(), fov, near, far).calcFraming(Sphere(vec3(0), 200));
	mCam.lookAt(eye, target);
	mCamUi = CameraUi(&mCam, getWindow(), -1);
	mCam.setAspectRatio(aspect);

	this->target = target;

}

ci::vec3 ArcCam::getEye() {
	return mCam.getEyePoint();
}

ci::quat ArcCam::getOrientation() {
	return mCam.getOrientation();
}

ci::mat4 ArcCam::getProjection() {
	return mCam.getProjectionMatrix();
}

void ArcCam::orbitTarget(float speed) {

	float deltaX = (target.x - speed) / (float)getWindowSize().x * mCam.getPivotDistance();
	float deltaY = (target.z - 0.0) / (float)getWindowSize().y * mCam.getPivotDistance();

	vec3 right, up;
	mCam.getBillboardVectors(&right, &up);
	mCam.setEyePoint(mCam.getEyePoint() - right * deltaX + up * deltaY);
	mCam.lookAt(target);

}
void ArcCam::orbitTarget(float speed, ci::vec3 target) {

	float deltaX = (target.x - speed) / (float)getWindowSize().x * mCam.getPivotDistance();
	float deltaY = (target.z - 0.0) / (float)getWindowSize().y * mCam.getPivotDistance();

	vec3 right, up;
	mCam.getBillboardVectors(&right, &up);
	mCam.setEyePoint(mCam.getEyePoint() - right * deltaX + up * deltaY);
	mCam.lookAt(target);

}
void ArcCam::enableTweening() {
#if defined(CINDER_MAC) || defined(CINDER_MSW)
	

	getWindow()->getSignalMouseDown().connect([this](MouseEvent event) {
		mMouse.stop();
		mMouse = event.getPos();
		mCamUi.mouseDown(mMouse());

	});

	getWindow()->getSignalMouseDrag().connect([this](MouseEvent event) {
		float time = 1.0f;


		// on windows, a shorter tween time seems to work better
		#ifdef CINDER_MSW
			time = 0.8f;
		#endif

		timeline().apply(&mMouse, vec2(event.getPos()), time, EaseOutQuint()).updateFn([=] {
			mCamUi.mouseDrag(mMouse(), event.isLeftDown(), false, event.isRightDown());
		});
	});

#endif

}

void ArcCam::orthoMatrices() {
	gl::setMatricesWindow(getWindowSize());
	gl::viewport(getWindowSize());
}
void ArcCam::rotateView(ci::mat4 rotateMat) {


}
void ArcCam::useMatrices() {
	gl::setMatrices(mCam);
}
void ArcCam::setTarget(ci::vec3 target) {
	mCam.lookAt(target);
}
void ArcCam::setFar(float far) {
	mCam.setFarClip(far);
}
void ArcCam::setNear(float near) {
	mCam.setNearClip(near);
}
void ArcCam::setZoom(float zoom) {
	auto currentEye = mCam.getEyePoint();
	currentEye.z = zoom;
	mCam.lookAt(currentEye, target);
}