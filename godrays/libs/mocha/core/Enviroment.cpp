#include "mocha/core/Enviroment.h"
using namespace std;
using namespace ci;
using namespace app;

Enviroment::Enviroment():sphereTextureSet(false){
}


Enviroment::~Enviroment(){
}

void Enviroment::update(bool saveFrame) {
	if (isPowerOn) mPower -= (mPower - 1.0f) * 0.2f;
	else mPower -= (mPower - 0.0f) * 0.2f;
	float prevTime = mTime;
	mTime = (float)app::getElapsedSeconds();
	float dt = mTime - prevTime;

	if (saveFrame) dt = 1.0f / 60.0f;

	mTimeAdjusted = dt * mTimeMulti;
	mTimeElapsed += mTimeAdjusted;

	mTimer += mTimeAdjusted;
	mTick = false;

	if (mTimer > 1.0f) {
		mTick = true;
		mTimer = 0.0f;
	}
}
void Enviroment::draw(ArcCamRef mCam) {

	gl::ScopedDepthTest t(true);

	// when drawing a room, there are some additional uniform values to take into account
	if (isRoom) {
		mBatch->getGlslProg()->uniform("eyePos", mCam->getEye());
		mBatch->getGlslProg()->uniform("roomDimensions", mBoxSize);
		mBatch->getGlslProg()->uniform("lightPower", getLightPower());
		mBatch->getGlslProg()->uniform("power", getPower());
		mBatch->getGlslProg()->uniform("timePer", getTimePer() * 1.5f + 0.5f);
		mBatch->draw();
	}

	if (sphereTextureSet) {
		mTexture->bind();
		mShader->uniform("uEnvTex", 0);
		mBatch->draw();
		mTexture->unbind();
	}

}

float Enviroment::getPower() {
	return mPower;
}
float Enviroment::getLightPower() {

	float p = getPower() * 5.0f * M_PI;
	float lightPower = cos(p) * 0.5f;

	return lightPower;
}
float Enviroment::getTimePer() {
	auto MAX_TIMEMULTI = 120.0f;
	return mTimeMulti / MAX_TIMEMULTI;
}
// ==================== SPHERE FUNCTIONS ========================= //

void Enviroment::loadIrradianceMap(std::string irradianceTex) {
	auto cubeMapFormat = gl::TextureCubeMap::Format().mipmap().internalFormat(GL_RGB16F).minFilter(GL_LINEAR_MIPMAP_LINEAR).magFilter(GL_LINEAR);
}

void Enviroment::loadRadianceMap(std::string radianceTex) {
	auto cubeMapFormat = gl::TextureCubeMap::Format().mipmap().internalFormat(GL_RGB16F).minFilter(GL_LINEAR_MIPMAP_LINEAR).magFilter(GL_LINEAR);
}

void Enviroment::loadTexture(std::string texture) {
	gl::Texture::Format fmt;
	fmt.setWrapS(GL_CLAMP_TO_EDGE);
	fmt.setWrapT(GL_CLAMP_TO_EDGE);

	mTexture = gl::Texture::create(loadImage(loadAsset(texture)),fmt);
	sphereTextureSet = true;
}

ci::vec3 Enviroment::getCenter(ci::vec3 p0, ci::vec3 p1) {
	auto x = (p0.x + p1.x) / 2.0;
	auto y = (p0.y + p1.y) / 2.0;
	auto z = (p0.z + p1.z) / 2.0;
	return vec3(x, y, z);
}
ci::vec3 Enviroment::getNormal(ci::vec3 p0, ci::vec3 p1, ci::vec3 p2) {
	auto pp0 = p0;
	auto pp1 = p1;
	auto pp2 = p2;
	auto v0 = vec3();
	auto v1 = vec3();
	auto n = vec3();

	v0 = pp1 - pp0;
	v1 = pp2 - pp0;
	n = glm::cross(v1, v0);
	n = glm::normalize(n);
	return n;
}

void Enviroment::buildSphere(std::string vertex, std::string fragment, int numSegments, int size) {
	this->numSegments = numSegments;
	this->size = size;

	isRoom = false;

	mShader = mocha::loadShader(vertex, fragment);

	const float num = 60.0;
	vector<ci::vec3> positions;
	vector<ci::vec3> normals;
	vector<ci::vec3> centers;
	vector<ci::vec2> uvs;
	vector<uint32_t> indices;
	int count = 0;
	const float uvGap = 1 / num;



	for (int j = 0; j < num; ++j) {
		for (int i = 0; i < num; ++i) {
			vec3 v0 = getPosition(i, j);
			vec3 v1 = getPosition(i + 1, j);
			vec3 v2 = getPosition(i + 1, j + 1);
			vec3 v3 = getPosition(i, j + 1);
			vec3 n = getNormal(v0, v1, v3);
			vec3 c = getCenter(v0, v2);

			positions.push_back(v0);
			positions.push_back(v1);
			positions.push_back(v2);
			positions.push_back(v3);

			normals.push_back(n);
			normals.push_back(n);
			normals.push_back(n);
			normals.push_back(n);

			centers.push_back(c);
			centers.push_back(c);
			centers.push_back(c);
			centers.push_back(c);

			uvs.push_back(vec2(i / num, j / num));
			uvs.push_back(vec2(i / num + uvGap, j / num));
			uvs.push_back(vec2(i / num + uvGap, j / num + uvGap));
			uvs.push_back(vec2(i / num, j / num + uvGap));
			indices.push_back(count * 4 + 0);
			indices.push_back(count * 4 + 1);
			indices.push_back(count * 4 + 2);
			indices.push_back(count * 4 + 0);
			indices.push_back(count * 4 + 2);
			indices.push_back(count * 4 + 3);


			count++;
		}
	}

	gl::VboMesh::Layout layout;
	layout.attrib(geom::POSITION, 3);
	layout.attrib(geom::TEX_COORD_0, 2);
	layout.attrib(geom::NORMAL, 3);
	layout.attrib(geom::CUSTOM_0, 3);


	mMesh = gl::VboMesh::create(positions.size(), GL_TRIANGLES, { layout }, indices.size(), GL_UNSIGNED_INT);
	mMesh->bufferAttrib(geom::POSITION, sizeof(vec3) * positions.size(), positions.data());
	mMesh->bufferAttrib(geom::NORMAL, sizeof(vec3) * normals.size(), normals.data());
	mMesh->bufferAttrib(geom::TEX_COORD_0, sizeof(vec2) * uvs.size(), uvs.data());
	mMesh->bufferAttrib(geom::CUSTOM_0, sizeof(vec3) * centers.size(), centers.data());
	mMesh->bufferIndices(sizeof(uint32_t) * indices.size(), indices.data());

	//mMesh = gl::VboMesh::create(geom::Sphere().radius(50.0));
	//mShader = mocha::loadShader("enviroment/enviroment.glslv", "enviroment/enviroment.glslf");
	mBatch = gl::Batch::create(mMesh, mShader, {
		{ geom::CUSTOM_0,"center" }
	});


}

void Enviroment::buildRoom(std::string vertex, std::string fragment) {
	int index = 0;
	indices.clear();
	positions.clear();
	normals.clear();
	uvs.clear();

	isRoom = true;

	float X = 1.0f;
	float Y = 1.0f;
	float Z = 1.0f;
	static vec3 verts[8] = {
		vec3(-X,-Y,-Z), vec3(-X,-Y, Z),
		vec3(X,-Y, Z), vec3(X,-Y,-Z),
		vec3(-X, Y,-Z), vec3(-X, Y, Z),
		vec3(X, Y, Z), vec3(X, Y,-Z)
	};

	static GLuint vIndices[12][3] = {

		{ 0,1,3 },{ 1,2,3 },	// floor
		{ 4,7,5 },{ 7,6,5 },	// ceiling
		{ 0,4,1 },{ 4,5,1 },	// left
		{ 2,6,3 },{ 6,7,3 },	// right
		{ 1,5,2 },{ 5,6,2 },	// back
		{ 3,7,0 },{ 7,4,0 }  // front  
	};

	static vec3 vNormals[6] = {
		vec3(0, 1, 0),	// floor
		vec3(0,-1, 0),	// ceiling
		vec3(1, 0, 0),	// left
		vec3(-1, 0, 0),	// right
		vec3(0, 0,-1),	// back
		vec3(0, 0, 1) 	// front

	};
	static vec2 vTexCoords[4] = {

		vec2(0.0f, 0.0f),
		vec2(0.0f, 1.0f),
		vec2(1.0f, 1.0f),
		vec2(1.0f, 0.0f)
	};

	static GLuint tIndices[12][3] = {

		{ 0,1,3 },{ 1,2,3 },	// floor
		{ 0,1,3 },{ 1,2,3 },	// ceiling
		{ 0,1,3 },{ 1,2,3 },	// left
		{ 0,1,3 },{ 1,2,3 },	// right
		{ 0,1,3 },{ 1,2,3 },	// back
		{ 0,1,3 },{ 1,2,3 } 	// front

	};

	gl::VboMesh::Layout layout;
	layout.attrib(geom::POSITION, 3);
	layout.attrib(geom::TEX_COORD_0, 2);
	layout.attrib(geom::NORMAL, 3);

	for (int i = 0; i<12; i++) {

		positions.push_back(verts[vIndices[i][0]]);
		positions.push_back(verts[vIndices[i][1]]);
		positions.push_back(verts[vIndices[i][2]]);
		indices.push_back(index++);
		indices.push_back(index++);
		indices.push_back(index++);
		normals.push_back(vNormals[i / 2]);
		normals.push_back(vNormals[i / 2]);
		normals.push_back(vNormals[i / 2]);
		uvs.push_back(vTexCoords[tIndices[i][0]]);
		uvs.push_back(vTexCoords[tIndices[i][1]]);
		uvs.push_back(vTexCoords[tIndices[i][2]]);
	}

	if (!isPowerOn) {
		mPower = 0.0f;
		mPower -= (mPower - 0.0f) * 0.2f;
	}


	mTime = (float)app::getElapsedSeconds();
	mTimer = 0.0f;
	mTick = false;
	mTimeMulti = 60.0f;


	mMesh = gl::VboMesh::create(positions.size(), GL_TRIANGLES, { layout }, indices.size(), GL_UNSIGNED_INT);

	mMesh->bufferAttrib(geom::POSITION, sizeof(vec3) * positions.size(), positions.data());
	mMesh->bufferAttrib(geom::TEX_COORD_0, sizeof(vec2) * uvs.size(), uvs.data());
	mMesh->bufferAttrib(geom::Attrib::NORMAL, sizeof(vec3) * normals.size(), normals.data());
	mMesh->bufferIndices(sizeof(uint32_t) * indices.size(), indices.data());

	mShader = mocha::loadShader(vertex, fragment);
	mBatch = gl::Batch::create(mMesh, mShader);
}

// ======= PRIVATE ======= //
ci::vec3 Enviroment::getPosition(float i, float j, bool isNormal) {
	float rx = i / numSegments * M_PI - M_PI * 0.5;
	float ry = j / numSegments * M_PI * 2;
	float r = size;

	ci::vec3 pos;

	pos.y = sin(rx) * r;
	float t = cos(rx) * r;
	pos.x = cos(ry) * t;
	pos.z = sin(ry) * t;


	return pos;
}
