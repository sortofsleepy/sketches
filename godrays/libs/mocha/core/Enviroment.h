#pragma once
#include "cinder/gl/VboMesh.h"
#include "cinder/gl/GlslProg.h"
#include "cinder/gl/Batch.h"
#include "cinder/app/App.h"
#include "mocha/core/core.h"
#include "mocha/core/ArcCam.hpp"
#include "cinder/gl/Fbo.h"
#include "cinder/gl/Texture.h"
#include <string>
#include <memory>
#include <vector>


typedef std::shared_ptr<class Enviroment>EnvRef;

#define STRINGIFY(A) #A

class Enviroment {

	ci::gl::BatchRef mBatch;
	ci::gl::GlslProgRef mShader;
	ci::gl::VboMeshRef mMesh;

	std::vector<ci::vec3> positions;
	std::vector<uint32_t> indices;
	std::vector<ci::vec2> uvs;
	std::vector<ci::vec3> normals;

	ci::gl::FboRef mFbo;

	bool isRoom;
	
	// ===== ROOM ENV ===== //
	float scaleVal;
	float mTime;
	float mTimeElapsed;
	float mTimeMulti;
	float mTimeAdjusted;
	float mTimer;
	bool mTick;

	bool isPowerOn;
	float mPower;

	ci::vec3 mBoxSize;

	// vertex shader source for room
	static std::string roomVert;

	// fragment shader source for room
	static std::string roomFrag;


	// ===== SPHERE ENV ====== //
	int numSegments, size;

	// loads a radiance texture for the sphere
	ci::gl::TextureRef mRadianceTexture;

	// loads an irradiance texture for the sphere
	ci::gl::TextureRef mIrradianceTexture;

	// loads a general texture for the sphere
	ci::gl::TextureRef mTexture;

	bool sphereTextureSet;

private:
	ci::vec3 getPosition(float i, float j, bool isNormal=false);
	ci::vec3 getCenter(ci::vec3 p0, ci::vec3 p1);
	ci::vec3 getNormal(ci::vec3 p0, ci::vec3 p1, ci::vec3 p2);

public:
	Enviroment();
	~Enviroment();

	static EnvRef create() {
		return EnvRef(new Enviroment());
	}

	void loadIrradianceMap(std::string irradianceTex);
	void loadRadianceMap(std::string radianceTex);
	void loadTexture(std::string texture);

	// builds a sphere enviroment similar to a CubeMap
	void buildSphere(std::string vertex, std::string fragment, int numSegments=60, int size=400);

	// builds a room based enviroment
	void buildRoom(std::string vertex, std::string fragment);

	// update the scene
	void update(bool saveFrame);

	// draw the scene
	void draw(ArcCamRef mCam);

	float getPower();
	float getLightPower();
	float getTimePer();
};
