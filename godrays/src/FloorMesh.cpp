#include "FloorMesh.h"
using namespace std;
using namespace ci;


FloorMesh::FloorMesh():elevation(-1.0f),
scale_first_noise(-4.7f),
scale_second_noise(-1.0f),
time(0.0f){
}


void FloorMesh::build(int width, int height, int widthSegments,int heightSegments) {
	
	vector<ci::vec3> positions;
	vector<ci::vec3> normals;
	vector<uint32_t> indices;
	
	float width_half = width / 2.0f;
	float height_half = height / 2.0f;

	auto gridX = widthSegments;
	auto gridY = heightSegments;

	auto gridX1 = gridX + 1;
	auto gridY1 = gridY + 1;

	float segment_width = (float)width / gridX;
	float segment_height = (float)height / gridY;

	int ix, iy;


	for (iy = 0; iy < gridY1; iy++) {

		float y = (float)iy * segment_height - height_half;

		for (ix = 0; ix < gridX1; ix++) {

			float x = (float)ix * segment_width - width_half;

			positions.push_back(vec3(x, -y, 0));

			normals.push_back(vec3(0,0,1));

			//uvs.push(ix / gridX);
			//uvs.push(1 - (iy / gridY));

		}

	}



	// indices

	for (iy = 0; iy < gridY; iy++) {

		for (ix = 0; ix < gridX; ix++) {

			auto a = ix + gridX1 * iy;
			auto b = ix + gridX1 * (iy + 1);
			auto c = (ix + 1) + gridX1 * (iy + 1);
			auto d = (ix + 1) + gridX1 * iy;

			// faces

			indices.push_back(a);
			indices.push_back(b);
			indices.push_back(d);

			indices.push_back(b);
			indices.push_back(c);
			indices.push_back(d);


		}

	}
	
	gl::VboMesh::Layout layout;
	layout.attrib(geom::POSITION, 3);
	layout.attrib(geom::NORMAL, 3);

	mMesh = gl::VboMesh::create(positions.size(), GL_TRIANGLES, { layout }, indices.size(), GL_UNSIGNED_INT);
	mMesh->bufferAttrib(geom::POSITION, positions.size() * sizeof(vec3), positions.data());
	mMesh->bufferAttrib(geom::NORMAL, normals.size() * sizeof(vec3), normals.data());
	mMesh->bufferIndices(sizeof(uint32_t) * indices.size(), indices.data());

	mShader = mocha::loadShader("floor.vert", "floor.frag");
	mBatch = gl::Batch::create(mMesh, mShader);

	// load noise texture
	mNoiseTex = gl::Texture::create(loadImage(app::loadAsset("space.jpg")));
}

void FloorMesh::draw(std::function<void()> fn) {
	time += 0.01;
	gl::ScopedTextureBind tex0(mNoiseTex, 0);
	auto size = app::getWindowSize();

	mBatch->getGlslProg()->uniform("noiseTex", 0);
	mBatch->getGlslProg()->uniform("iResolution", vec2(size.x, size.y));
	mBatch->getGlslProg()->uniform("uTime", (float)app::getElapsedSeconds());
	mBatch->getGlslProg()->uniform("u_elevation", elevation);
	mBatch->getGlslProg()->uniform("u_scale_first_noise", scale_first_noise);
	mBatch->getGlslProg()->uniform("u_scale_second_noise", scale_second_noise);
	
	gl::pushModelMatrix();
	if (fn != nullptr) {
		fn();
	}
	mBatch->draw();

	gl::popModelMatrix();

	/*
	gl::translate(vec3(0, -100, 0));
	gl::scale(vec3(500.0));
	gl::rotate(ci::toRadians(90.0f), vec3(1, 0, 0));
*/
}

