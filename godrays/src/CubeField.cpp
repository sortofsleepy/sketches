#include "CubeField.h"
using namespace ci;
using namespace std;


CubeField::CubeField(int width,int height,int depth,int resolution)
{
	auto cols = width / resolution;
	auto rows = height / resolution;
	auto dep = depth / resolution;

	vector<vec3> positions;
	vector<float> offset;

	for (int i = 0; i < cols; ++i) {
		for (int j = 0; j < rows; ++j) {
			for (int z = 0; z < dep; ++z) {
				auto x = i * resolution;
				auto y = j * resolution;
				auto zed = z * resolution;

				vec3 pos = vec3(x, y, zed);
				positions.push_back(pos);

				offset.push_back(randFloat());
			}
		}
	}

	instanceCount = cols * rows * dep;

	// build the position data for the cube
	fieldData = gl::Vbo::create(GL_ARRAY_BUFFER,sizeof(vec3) * positions.size(), positions.data(), GL_DYNAMIC_DRAW);

	// build offset data
	offsetData = gl::Vbo::create(GL_ARRAY_BUFFER, sizeof(float) * offset.size(), offset.data(), GL_STATIC_DRAW);

	geom::BufferLayout instanceDataLayout;
	instanceDataLayout.append(geom::Attrib::CUSTOM_0, 3, 0, 0, 1 /* per instance */);

	geom::BufferLayout instanceDataLayout2;
	instanceDataLayout2.append(geom::Attrib::CUSTOM_1, 1, 0, 0, 1 /* per instance */);


	// build the main shape for each point in the cube
	auto geo = geom::Cube().size(vec3(2, 2, 2));
	mMesh = gl::VboMesh::create(geo);

	// append position for each cube in the field.
	mMesh->appendVbo(instanceDataLayout, fieldData);

	// append offset values for each cube in the field.
	mMesh->appendVbo(instanceDataLayout2, offsetData);

	mShader = mocha::loadShader("cube.vert", "cube.frag");

	mBatch = gl::Batch::create(mMesh, mShader, {
		{geom::CUSTOM_0,"fieldPosition"},
		{geom::CUSTOM_1, "offset"}
	});

	this->width = width;
	this->height = height; 
	this->depth = depth;

	diffuseMap = gl::Texture::create(loadImage(app::loadAsset("diffuse.jpg")));
}




CubeField::~CubeField()
{
}

void CubeField::draw() {
	
	gl::ScopedDepth t(true);

	gl::ScopedTextureBind tex0(diffuseMap, 0);

	auto timer = (float)app::getElapsedSeconds() + 0.5;
	mShader->uniform("time", (float)timer);
	mShader->uniform("diffuseMap", 0);

	gl::pushModelView();
	mCubeRotation *= rotate(toRadians(0.2f), normalize(vec3(1, 1, 1)));

	gl::multModelMatrix(mCubeRotation);

	gl::translate(vec3(-width / 2, -height / 2, -depth / 2));
	
	mBatch->drawInstanced(instanceCount);
	gl::popModelView();
}

