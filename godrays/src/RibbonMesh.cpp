#include "RibbonMesh.h"
using namespace ci;
using namespace std;


RibbonMesh::RibbonMesh():lineWidth(1.0f),
	near(1.0f),
	far(1.0f),
	sizeAttenuation(1.0f)
{
	resolution = vec2(app::getWindowWidth(), app::getWindowHeight());
}


RibbonMesh::~RibbonMesh()
{
}

// === PRIVATE FUNCTIONS ==== //
bool RibbonMesh::compareV3(int a, int b) {
	auto aa = a * 6;
	auto ab = b * 6;
	auto val = false;


	if (positions[aa] == positions[ab]) {
		if (positions[aa + 1] == positions[ab + 1]) {
			if (positions[aa + 2] == positions[ab + 2]) {
				val = true;
			}
		}
	}

	return val;
}

ci::vec3 RibbonMesh::copyV3(int a) {
	auto aa = a * 6;
	auto x = positions[aa];
	auto y = positions[aa + 1];
	auto z = positions[aa + 2];
	return vec3(x, y, z);
}

void RibbonMesh::setData(std::vector<ci::vec3> &data) {

	for (int i = 0; i < data.size(); ++i) {
		auto vec = data[i];
		auto c = i / (data.size());
		positions.push_back(vec.x);
		positions.push_back(vec.y);
		positions.push_back(vec.z);

		positions.push_back(vec.x);
		positions.push_back(vec.y);
		positions.push_back(vec.z);

		counters.push_back(c);
		counters.push_back(c);

	}
	process();
}
void RibbonMesh::process() {

	auto l = positions.size() / 6;


	for (int j = 0; j < l; ++j) {
		side.push_back(1);
		side.push_back(-1);
	}

	for (int j = 0; j < l; ++j) {
		width.push_back(1);
		width.push_back(1);
	}

	for (int j = 0; j < l; ++j) {
		auto x = j / (l - 1);
		auto y = 0;
		uvs.push_back(vec2(x, y));
		uvs.push_back(vec2(x, 1));

	}

	auto v = vec3();
	if (compareV3(0, l - 1)) {
		v = copyV3(l - 2);
	}
	else {
		v = copyV3(0);
	}

	previous.push_back(v.x);
	previous.push_back(v.y);
	previous.push_back(v.z);


	previous.push_back(v.x);
	previous.push_back(v.y);
	previous.push_back(v.z);

	for (int j = 0; j < l - 1; ++j) {
		v = copyV3(j);
		previous.push_back(v.x);
		previous.push_back(v.y);
		previous.push_back(v.z);

		previous.push_back(v.x);
		previous.push_back(v.y);
		previous.push_back(v.z);
	}

	for (int j = 1; j < l; ++j) {
		v = copyV3(j);
		next.push_back(v.x);
		next.push_back(v.y);
		next.push_back(v.z);

		next.push_back(v.x);
		next.push_back(v.y);
		next.push_back(v.z);
	}

	if (compareV3(l - 1, 0)) {
		v = copyV3(1);
	}
	else {
		v = copyV3(l - 1);
	}


	next.push_back(v.x);
	next.push_back(v.y);
	next.push_back(v.z);

	next.push_back(v.x);
	next.push_back(v.y);
	next.push_back(v.z);

	for (int j = 0; j < l - 1; ++j) {
		auto n = j * 2;
		indices.push_back(n);
		indices.push_back(n + 1);
		indices.push_back(n + 2);

		indices.push_back(n + 2);
		indices.push_back(n + 1);
		indices.push_back(n + 3);
	}

	mVao = gl::Vao::create();

	mVao->bind();

	mPositions = ci::gl::Vbo::create(GL_ARRAY_BUFFER, sizeof(float) * positions.size()  , positions.data(), GL_DYNAMIC_DRAW);
	mPrevious = ci::gl::Vbo::create(GL_ARRAY_BUFFER, sizeof(float) * previous.size() , previous.data(), GL_DYNAMIC_DRAW);
	mNext= ci::gl::Vbo::create(GL_ARRAY_BUFFER, sizeof(float) * next.size() , next.data(), GL_DYNAMIC_DRAW);
	mSide = ci::gl::Vbo::create(GL_ARRAY_BUFFER, sizeof(float) * side.size() , side.data(), GL_DYNAMIC_DRAW);
	mWidth = ci::gl::Vbo::create(GL_ARRAY_BUFFER, sizeof(float) *width.size() , width.data(), GL_DYNAMIC_DRAW);
	mCounters = ci::gl::Vbo::create(GL_ARRAY_BUFFER, sizeof(float) * counters.size() , counters.data(), GL_DYNAMIC_DRAW);
	mIndices = gl::Vbo::create(GL_ELEMENT_ARRAY_BUFFER, sizeof(float) * indices.size() * sizeof(uint32_t), indices.data(), GL_STATIC_DRAW);

	// bind positions to vao 
	mPositions->bind();
	ci::gl::vertexAttribPointer(POSITION_INDEX, 3, GL_FLOAT, GL_FALSE, 0, 0);
	ci::gl::enableVertexAttribArray(POSITION_INDEX);
	mPositions->unbind();
	
	// bind previous positions
	mPrevious->bind();
	gl::vertexAttribPointer(PREVIOUS_POSITION_INDEX, 3, GL_FLOAT, GL_FALSE, 0, 0);
	gl::enableVertexAttribArray(PREVIOUS_POSITION_INDEX);
	mPrevious->unbind();

	// bind next
	mNext->bind();
	gl::vertexAttribPointer(NEXT_INDEX, 3, GL_FLOAT, GL_FALSE, 0, 0);
	gl::enableVertexAttribArray(NEXT_INDEX);
	mNext->unbind();

	mSide->bind();
	gl::vertexAttribPointer(SIDE_INDEX, 1, GL_FLOAT, GL_FALSE, 0, 0);
	gl::enableVertexAttribArray(SIDE_INDEX);
	mSide->unbind();

	mWidth->bind();
	gl::vertexAttribPointer(WIDTH_INDEX, 1, GL_FLOAT, GL_FALSE, 0, 0);
	gl::enableVertexAttribArray(WIDTH_INDEX);
	mWidth->unbind();


	mCounters->bind();
	gl::vertexAttribPointer(COUNTER_INDEX, 1, GL_FLOAT, GL_FALSE, 0, 0);
	gl::enableVertexAttribArray(COUNTER_INDEX);
	mCounters->unbind();
	
	mShader = mocha::loadShader("meshline.vert", "meshline.frag");

	mVao->unbind();
}

void RibbonMesh::draw(ArcCamRef mCam) {
	gl::ScopedGlslProg shd(mShader);
	gl::ScopedVao v(mVao);
	gl::ScopedBuffer idx(mIndices);

	mShader->uniform("resolution", resolution);
	mShader->uniform("lineWidth", lineWidth);
	mShader->uniform("opacity", opacity);
	mShader->uniform("near", near);
	mShader->uniform("far", far);
	mShader->uniform("sizeAttenuation", sizeAttenuation);
	mShader->uniform("ciProjectionMatrix", mCam->getProjection());
	mShader->uniform("ciModelView", mCam->getView());
	glDrawElements(GL_TRIANGLES,indices.size() * 3,GL_UNSIGNED_INT,  (GLvoid*) 0);
}

void RibbonMesh::update(ci::vec3 newPos) {

	auto l = positions.size();


	// PREVIOUS
	copy(positions, 0, previous, 0, l);

	// POSITIONS
	copy(positions, 6, positions, 0, l - 6);
	

	positions[l - 6] = newPos.x;
	positions[l - 5] = newPos.y;
	positions[l - 4] = newPos.z;
	positions[l - 3] = newPos.x;
	positions[l - 2] = newPos.y;
	positions[l - 1] = newPos.z;


	// NEXT
	copy(positions, 6, next, 0, l - 6);

	next[l - 6] = newPos.x;
	next[l - 5] = newPos.y;
	next[l - 4] = newPos.z;
	next[l - 3] = newPos.x;
	next[l - 2] = newPos.y;
	next[l - 1] = newPos.z;

	gl::ScopedVao v(mVao);
	mPositions->bufferSubData(0, sizeof(float) *positions.size() , positions.data());
	mNext->bufferSubData(0, sizeof(float) *next.size() , next.data());
	mPrevious->bufferSubData(0, sizeof(float) * previous.size() , previous.data());


}

void RibbonMesh::copy(std::vector<float> &src, int srcOffset, std::vector<float> &dst, int dstOffset, int length) {
	
	vector<float>::const_iterator first = src.begin() + srcOffset;
	vector<float>::const_iterator last = src.begin() + (srcOffset + length);
	vector<float> newSrc = vector<float>(first, last);

	for (int j = 0; j < newSrc.size(); ++j) {
		dst[j + dstOffset] = newSrc[j];
	}
}