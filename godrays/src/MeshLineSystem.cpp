#include "MeshLineSystem.h"
using namespace ci;
using namespace std;


MeshLineSystem::MeshLineSystem():updateLines(false),
hasDiffuse(false)
{}

void MeshLineSystem::draw(ArcCamRef mCam) {
	for (Ribbon &line : lines) {
		line.line->updateMesh();
		if (hasDiffuse) {
			line.line->draw(mDiffuse);
		}
		else {
			line.line->draw();
		}
	}
}

void MeshLineSystem::setup(int numlines, int radius) {
	
	
	vector < ci::vec3> lineData;
	for (int j = 0; j < 100; ++j) {
		auto vec = vec3();
		vec.x = -30.0 + .1 * j;
		vec.y = 5.0 * sin(0.01 * j);
		vec.z = -20.0;

		lineData.push_back(vec);
	}

	for (int i = 0; i < numlines; ++i) {
		auto ribbon = Ribbon();
		ribbon.line = MeshLine::create();
		ribbon.line->setLineWidth(50.0f);
		ribbon.line->setData(lineData);

		// add other properties
		ribbon.phi = (randFloat() * 2.0) * M_PI;
		ribbon.theta = (randFloat() * 2.0) * M_PI;
		ribbon.thetaSpeed = randFloat(-0.01, 0.01);
		ribbon.phiSpeed = randFloat(-0.01, 0.01);


		ribbon.radius = radius;

		lines.push_back(ribbon);

	}

	
	//mThread->join();
	//mThread->detach();
}

void MeshLineSystem::toggleLineUpdate() {
	
	updateLines = !updateLines;
	if (updateLines) {
		mThread = shared_ptr<thread>(new thread(bind(&MeshLineSystem::update, this)));
	}
	else if (updateLines == false) {
		mThread->join();
	}
}

MeshLineSystem::~MeshLineSystem()
{
	lines.clear();
	stop();
}

void MeshLineSystem::stop() {
	mThread->join();
	
}

void MeshLineSystem::update() {

	
	ci::ThreadSetup threadSetup;
	while (updateLines) {
		for (Ribbon &line : lines) {
			auto x = cos(line.theta) * sin(line.phi) * line.radius;
			auto y = sin(line.theta) * sin(line.phi) * line.radius;
			auto z = cos(line.phi) * line.radius;
			line.theta += line.thetaSpeed;
			line.phi += line.phiSpeed;

			line.line->update(vec3(x, y, z));
		}
	}
}

void MeshLineSystem::setDiffuseTexture(std::string texpath) {
	mDiffuse = gl::Texture::create(loadImage(loadAsset(texpath)));
	hasDiffuse = true;
}

