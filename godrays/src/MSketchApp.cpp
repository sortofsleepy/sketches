#include "cinder/app/App.h"
#include "cinder/app/RendererGl.h"
#include "cinder/gl/gl.h"
#include "mocha/core/ArcCam.hpp"
#include "MeshLineSystem.h"
#include "mocha/post/FxPass.h"
#include "FloorMesh.h"
#include "CubeField.h"
#include "GodRays.h"
using namespace ci;
using namespace ci::app;
using namespace std;

class MSketchApp : public App {
  public:
	  ~MSketchApp();

	void setup() override;
	void mouseDown( MouseEvent event ) override;
	void update() override;
	void draw() override;

	ArcCamRef mCam;
	MeshLineSystem system;
	FloorMeshRef mFloor;
	FxPassRef scene;
	CubeField cube;
	GodRays rays;

};
MSketchApp::~MSketchApp() {

	// MeshLineSystem runs a thread, stop that thread when the app exits.
	system.stop();


}

void MSketchApp::setup()
{
	// setup camera
	mCam = ArcCam::create();
	mCam->setZoom(-4000);
	mCam->enableTweening();

	mFloor = FloorMesh::create(10,10,800,500);

	system.setup(40,2000);
	system.toggleLineUpdate();
	system.setDiffuseTexture("env.jpg");


	scene = FxPass::create(true);
	rays.setInputTexture(scene->getOutput());


}

void MSketchApp::mouseDown( MouseEvent event )
{
}

void MSketchApp::update()
{
	rays.update();
	mCam->orbitTarget(5.);
}

void MSketchApp::draw()
{
	gl::ScopedDepth t(true);
	gl::clear( Color( 0, 0, 0 ) ); 
	mCam->useMatrices();
	
	scene->bind();
	gl::clear(Color::black());
	gl::color(Color::white());
	mFloor->draw([=]()->void {
		gl::translate(vec3(0, -800, 0));
		gl::scale(vec3(1000.0));
		gl::rotate(ci::toRadians(90.0f), vec3(1, 0, 0));
	});

	gl::pushModelView();
	system.draw(mCam);
	gl::popModelView();

	gl::pushModelView();
	gl::scale(vec3(50));
	cube.draw();
	gl::popModelView();
	scene->unbind();


	rays.draw(scene->getOutput());


}

CINDER_APP(MSketchApp, RendererGl, [](App::Settings * settings) {
	settings->setWindowSize(1024, 768);
})