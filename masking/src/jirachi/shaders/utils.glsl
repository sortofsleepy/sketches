vec3 rotateX(vec3 p, float theta){
  float s = sin(theta);
  float c = cos(theta);
  return vec3(p.x, p.y * c - p.z * s, p.z * c + p.y * s);
}

vec3 rotateY(vec3 p, float theta) {
  float s = sin(theta);
  float c = cos(theta);
  return vec3(p.x * c + p.z * s, p.y, p.z * c - p.x * s);
}

vec3 rotateZ(vec3 p, float theta) {
  float s = sin(theta);
  float c = cos(theta);
  return vec3(p.x * c - p.y * s, p.y * c + p.x * s, p.z);
}

// intended for a set of UV coords - returns the coordinates with the Y axis flipped
vec2 flipUvY(vec2 vUv){
  return vec2(vUv.x,1.0 - vUv.y);
}

// intended for a set of UV coords - returns the coordinates with the X axis flipped
vec2 flipUvX(vec2 vUv){
  return vec2(1.0 - vUv.x,vUv.y);
}
