import createRenderer from './jirachi/core/gl'
import {PerspectiveCamera} from './jirachi/framework/camera'
import {ShaderFormat, TextureFormat} from "./jirachi/core/formats";
import createTexture from './jirachi/core/texture'
import createFbo from './jirachi/core/fbo'
import Quad from "./jirachi/geometry/quad";
import ImageFlipper from "./ImageFlipper";
import ParticleSystem from "./ParticleSystem";
import ObjLoader from "./jirachi/loaders/ObjLoader";
import Loader from './loader'

// -----------------------------------------
let ps = null;
let img = new Image();
let img2 = new Image();
let img3 = new Image();
let flipper = null;
let fbo = null;
// --------------------------
const gl = createRenderer()["setFullscreen"]();
document.body.appendChild(gl.canvas);

fbo = createFbo(gl,new TextureFormat(gl).setWidth(window.innerWidth).setHeight(window.innerHeight));

const camera = new PerspectiveCamera({
    near:0.1,
    far:10000.0,
    aspect: gl.canvas.width / gl.canvas.height,
});

camera.setZoom(-50);

img.src = "./assets/thumb.jpg"
img2.src = "./assets/thumb2.jpg"
img3.src = './assets/transition1.png'

Loader.loadAssets([img,img2,"./assets/petal.txt",img3]).then(assets =>{

    let img = assets[0];
    let img2 = assets[1];

    ps = new ParticleSystem(gl,assets[2],[window.innerWidth,window.innerHeight],10000)
    flipper = new ImageFlipper(gl,img["width"],img["height"],
        createTexture(gl,new TextureFormat(gl,{
            data:assets[3]
        })),
        [
        createTexture(gl,new TextureFormat(gl,{
            data:img
        })),
        createTexture(gl,new TextureFormat(gl,{
            data:img2
        }))
    ]);

    window.addEventListener('resize',e =>{
        flipper.resize(window.innerWidth,window.innerHeight);
        camera.resize(window.innerWidth / window.innerHeight);
        ps.resize(window.innerWidth,window.innerHeight)
    });
    animate();
});

// ============== BUILD PLANE ================== //



function animate(){
    requestAnimationFrame(animate);
    flipper._transitionTexture();

    gl.clearScreen();
    if(flipper && ps){
        flipper.update(camera);
        //flipper.draw(camera);
        flipper.getOutput().bind(0);
        ps.draw(camera);
}
}