import createFbo from './jirachi/core/fbo'
import Mesh from "./jirachi/framework/mesh";
import Plane from "./jirachi/geometry/plane";
import createShader from './jirachi/core/shader'
import {ShaderFormat, TextureFormat} from "./jirachi/core/formats";
import Quad from "./jirachi/geometry/quad";

import pvert from './shaders/plane.vert'
import pfrag from './shaders/plane.frag'
import fpvert from './shaders/flipperRender.vert'
import fpfrag from './shaders/flipperRender.frag'

import * as dat from 'dat.gui'
const gui = new dat.GUI();
/**
 * Renders images on a plane to an FBO. Assumes all images are the same size.
 */
export default class ImageFlipper {
    gl:any;
    assets:Array<WebGLTexture> = [];
    planeMesh:any;
    planeRenderMesh:any;
    planeShader:any;
    planeRenderShader:any;
    fbo:any;
    idx:number = 0;
    resolution:Array<any>;
    width:number;
    height:number;
    threshold:number = 0.3;
    transitionSpeed:number = 0.2;
    transition:number = 0.5;
    transitionTexture:WebGLTexture;
    mixRatio:number = 0.0;

    /**
     * Constructor
     * @param gl {WebGLRenderingContext} A WebGL context
     * @param w {number} width of all the images
     * @param h {number} height of all the images.
     * @param transitionTexture {WebGLTexture} the webgl texture to use in the transition.
     * @param assets {Array} an Array of WebGL textures.
     */
    constructor(gl,w,h,transitionTexture:WebGLTexture,assets:Array<WebGLTexture>) {
        this.fbo = createFbo(gl,new TextureFormat(gl).setWidth(window.innerWidth).setHeight(window.innerHeight));
        this.planeShader = createShader(gl,new ShaderFormat(pvert,pfrag));
        this.planeRenderShader = createShader(gl,new ShaderFormat(fpvert,fpfrag))
        this.planeMesh = new Mesh(this.planeShader,new Plane(w,h)).setFbo(this.fbo);

        let quad = new Quad();
        let planeRender = new Mesh(this.planeRenderShader);
        planeRender.addAttribute('position',quad.vertices,{
            size:2
        });
        this.planeRenderMesh = planeRender

        this.gl = gl;
        this.assets = assets;
        this.width = w;
        this.height = h;
        this.transitionTexture = transitionTexture;

        gui.add(this,'mixRatio', 0, 1, 0.01);
    }

    /**
     * Return the current output
     */
    getOutput(){
        return this.fbo.getColorTexture();
    }

    _transitionTexture(){

    }

    update(camera){
        let gl = this.gl;

        this.planeMesh.draw(camera,shader => {

            this.assets[0]["bind"]();
            this.assets[1]["bind"](1);
            this.transitionTexture["bind"](2);

            shader.int("currentTexture",0);
            shader.int("nextTexture",1);
            shader.int("transitionTexture",2);
            shader.float("mixRatio",this.mixRatio);
            shader.float("threshold",this.threshold);

        });


        // make sure there are no bound textures
        this.gl.unbindTexture();

    }

    draw(camera){
        let gl = this.gl;
        this.fbo.getColorTexture().bind(0);
        this.planeRenderMesh.draw(camera);
        this.planeRenderMesh.shader.int("uTex0",0);
    }

    next(){
        if(this.idx < this.assets.length){
            this.idx += 1;
        }else{
            this.idx = 0;
        }
    }

    getNext(){
        if(this.idx < this.assets.length){
            return this.idx = 1;
        }else{
            return 0;
        }
    }

    previous(){
        if(this.idx > 0){
            this.idx -= 1;
        }else{
            this.idx = this.assets.length - 1;
        }
    }
    /**
     * Resizes all litems
     * @param w {number} new width
     * @param h {number} new height
     */
    resize(w,h){
        this.fbo.resize(window.innerWidth,window.innerHeight);
        this.planeMesh = new Mesh(this.planeShader,new Plane(this.width,this.height)).setFbo(this.fbo);

    }

}