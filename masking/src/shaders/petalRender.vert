
uniform mat4 projectionMatrix;
uniform mat4 modelViewMatrix;
uniform float time;
in vec3 position;
in vec4 iPosition;
in float iScale;
out float vAlpha;
void main(){

    gl_PointSize = 5.0;

    vec3 pos = position;
    pos.x *= iScale;
    pos.y *= iScale;
    pos.z *= iScale;

    vec3 rotPos = rotateX(pos,time * iScale * 0.5);
    rotPos = rotateY(rotPos,time * iScale * 0.5);
    rotPos = rotateZ(rotPos,time * iScale * 0.5);

    //vec4 pos = vec4(position + iPosition.xyz,1.);
    vec4 finalPos = vec4(rotPos + iPosition.xyz,1.);
    vAlpha = iPosition.a;

    gl_Position = projectionMatrix * modelViewMatrix * finalPos;
    //gl_Position =  vec4(position,1.);

}