precision highp float;
uniform sampler2D currentTex;
uniform sampler2D nextTex;
uniform float alpha;
uniform vec2 resolution;
out vec4 glFragColor;


in vec2 vUv;
void main() {

    vec2 uv  = vUv;
    // need to flip uvs
    uv.y = 1.0 - uv.y;
    uv.x = 1.0 - uv.x;


    vec4 color = texture(currentTex,uv);
	glFragColor = color;
    glFragColor.a = alpha;

}
