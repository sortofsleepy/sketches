uniform mat4 projectionMatrix;
uniform mat4 viewMatrix;
uniform mat4 modelMatrix;
uniform bool isRendering;
uniform vec2 resolution;
in vec3 position;
in vec2 uv;

out vec4 vCol;
out vec2 vUv;
void main(){
    vUv = uv;
    vec3 pos = position;

    pos.xy *= vec2(0.03);

    vCol = modelMatrix * vec4(1.);

    gl_Position = projectionMatrix * modelMatrix * viewMatrix * vec4(pos,1.);
}
