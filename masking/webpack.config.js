const path = require('path');
const webpack = require("webpack");

module.exports = {
    entry:'./src/app.ts',
    mode:"development",
  //  devtool:"source-map",
    module:{
        rules: [
            {
                test:/\.(glsl|vert|frag|vs|fs)$/,
                use:'raw-loader'
            },
            {
                test:/\.(ts|tsx)$/,
                exclude:/(node_modules | bower_components)/,
                use:{
                    loader:'awesome-typescript-loader'
                }
            },
            {
                test:/\.scss$/,
                use:[
                    "style-loader",
                    "css-loader",
                    "sass-loader"
                ]
            }

        ],


    },
    resolve:{
        extensions:[".tsx",".ts",".js",".vert",".frag",".glsl",".scss",".css"]
    },
    output:{
        filename:'app.js',
        chunkFilename: '[name].bundle.js',
        path:path.resolve(__dirname,"public")
    },
    plugins:[
        new webpack.SourceMapDevToolPlugin({
            filename:"[file]".map
        })
    ]
}