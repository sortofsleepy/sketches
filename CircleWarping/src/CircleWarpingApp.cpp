#include "cinder/app/App.h"
#include "cinder/app/RendererGl.h"
#include "cinder/gl/gl.h"
#include "mocha/post/PostProcessor.h"
#include "mocha/core/Camera.h"
#include "mocha/utils.h"
#include "CircleWarper.h"
#include "GlitchEffect.h"
#include "cinder/Rand.h"

using namespace ci;
using namespace ci::app;
using namespace std;

class CircleWarpingApp : public App {
public:
	void setup() override;
	void update() override;
	void draw() override;
	void updateScene();

	mocha::EasyCameraRef mCam;
	mocha::post::PostProcessor processor;
	mocha::post::EffectRef hBlur, vBlur, abrasion;

	GlitchEffect * glitcher;
	ci::gl::FboRef mScene;

	gl::BatchRef mBatch;
	gl::GlslProgRef mShader;

	CircleWarper warper;

};

void CircleWarpingApp::setup()
{
	mCam = mocha::EasyCamera::create();
	mCam->setZoom(-300);

	// enable camera controls if not on web
#ifndef CINDER_EMSCRIPTEN
	mCam->enableUI();
#endif

	// initialize effects
	glitcher = new GlitchEffect();
	hBlur = mocha::post::PostEffect::create(app::loadAsset("blur.glsl"));
	vBlur = mocha::post::PostEffect::create(app::loadAsset("blur.glsl"));
	abrasion = mocha::post::PostEffect::create(app::loadAsset("abrasion.txt"));

	// add effects in order of application
	processor.addStage(hBlur).addStage(vBlur).addStage(abrasion).addStage(glitcher);

	mScene = mocha::utils::createFbo(app::getWindowSize());

	// compile the scene into the post processor
	processor.compile(mScene->getColorTexture());


	// setup circle warper
	warper.setup();
}

void CircleWarpingApp::updateScene()
{
	gl::ScopedFramebuffer fbo(mScene);
	gl::clear(ColorA(0, 0, 0, 0));
	mCam->useCamera();

	warper.draw();
}

void CircleWarpingApp::update()
{

	warper.update();

	updateScene();

	// update all stages of the effects
	hBlur->update([=](ci::gl::GlslProgRef shader)->void {
		shader->uniform("sample_offset", vec2(1.0f / hBlur->getWidth(), 0.0));
		shader->uniform("attenuation", 2.5f);
	});

	vBlur->update([=](ci::gl::GlslProgRef shader)->void {
		shader->uniform("sample_offset", vec2(0.0f, 1.0 / vBlur->getHeight()));
		shader->uniform("attenuation", 2.5f);
	});

	abrasion->update();
	glitcher->update();
}

void CircleWarpingApp::draw()
{
	gl::clear(Color(0, 0, 0));

	// render the composited scene.
	processor.draw();

}

CINDER_APP(CircleWarpingApp, RendererGl, [](App::Settings *settings) {
	settings->setWindowSize(1280, 720);
	settings->setMultiTouchEnabled(false);
});
