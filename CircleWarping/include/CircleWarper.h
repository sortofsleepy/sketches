#pragma once


#pragma once

#include "cinder/gl/VboMesh.h"
#include "cinder/gl/GlslProg.h"
#include "cinder/gl/Batch.h"
#include "mocha/core/math.h"
#include <vector>
#include "cinder/Log.h"
#include "cinder/Rand.h"
#include "cinder/gl/gl.h"
#include "cinder/app/App.h"

class CircleWarper {
public:
	CircleWarper(double radius = 100, double startA = 0, double endA = 120.0f) :
		mRadius(radius),
		mStartA(startA),
		mEndA(endA),
		mTheta(0.0),
		mNumInstances(10) {}

	void setup() {
		vec3 start = mocha::math::polarToCartesian(mRadius, mStartA, true);
		vec3 end = mocha::math::polarToCartesian(mRadius, mEndA, true);

		int spacing = 4;
		for (int a = 0; a < 360; a += spacing) {
			vec3 cv = mocha::math::polarToCartesian(mRadius, a, true);
			cirPath.push_back(cv);

			float amt = (a % 120) / (mEndA - mStartA);
			vec3 tv = glm::lerp(start, end, amt);
			triPath.push_back(tv);

			if ((a + spacing) % 120 == 0) {
				mStartA += 120;
				mEndA += 120;
				start = mocha::math::polarToCartesian(mRadius, mStartA, true);
				end = mocha::math::polarToCartesian(mRadius, mEndA, true);
			}

		}

		// setup outputData;
		outputData.reserve(cirPath.size());
		for (int i = 0; i < cirPath.size(); ++i) {
			outputData.push_back(vec3());
		}

		mNumInstances = cirPath.size();

		buildMesh();
	}

	void buildMesh() {

		// setup positions for each instance.
		std::vector<ci::vec3>positions;
		std::vector<float> scales, rotationOffset;
		for (int i = 0; i < mNumInstances; ++i) {
			positions.push_back(randVec3());
			scales.push_back(randFloat() + 1.0f);
			rotationOffset.push_back(randFloat());
			zOffsets.push_back(randFloat());


			zOffsetIncrement.push_back(randFloat());
		}

		mInstancePositions = gl::Vbo::create(GL_ARRAY_BUFFER, sizeof(ci::vec3) * positions.size(), positions.data(), GL_DYNAMIC_DRAW);

		mInstanceScales = gl::Vbo::create(GL_ARRAY_BUFFER, sizeof(float) * scales.size(), scales.data(), GL_DYNAMIC_DRAW);

		mInstanceRotationOffset = gl::Vbo::create(GL_ARRAY_BUFFER, sizeof(float) * rotationOffset.size(), rotationOffset.data(), GL_DYNAMIC_DRAW);

		mInstanceZOffset = gl::Vbo::create(GL_ARRAY_BUFFER, sizeof(float) * zOffsets.size(), zOffsets.data());



		geom::BufferLayout positionLayout, scaleLayout, rotationLayout, zLayout;
		positionLayout.append(geom::CUSTOM_0, 3, 0, 0, 1);
		scaleLayout.append(geom::CUSTOM_1, 1, 0, 0, 1);
		rotationLayout.append(geom::CUSTOM_2, 1, 0, 0, 1);
		zLayout.append(geom::CUSTOM_3, 1, 0, 0, 1);
		// ================ build mesh ================== //

		gl::VboMesh::Layout layout;
		layout.attrib(geom::POSITION, 3);

		gl::VboMeshRef mesh = gl::VboMesh::create(geom::Icosahedron());

		mesh->appendVbo(positionLayout, mInstancePositions);
		mesh->appendVbo(scaleLayout, mInstanceScales);
		mesh->appendVbo(rotationLayout, mInstanceRotationOffset);
		mesh->appendVbo(zLayout, mInstanceZOffset);

		gl::GlslProg::Format fmt;
		fmt.vertex(ci::app::loadAsset("particleRender.vert"));
		fmt.fragment(ci::app::loadAsset("particleRender.frag"));

		try {
			mShader = gl::GlslProg::create(fmt);
		}
		catch (ci::Exception &e) {
			CI_LOG_I(e.what());
		}

		mBatch = gl::Batch::create(mesh, mShader, {
			{ geom::CUSTOM_0,"instancePosition" },
			{ geom::CUSTOM_1,"instanceScale" },
			{ geom::CUSTOM_2,"instanceRotationOffset" },
			{ geom::CUSTOM_3, "instanceZOffset" }
			});
	}

	void update() {


		auto amt = (sin(mTheta) + 1) / 2;
		mTheta += 0.1f;

		for (int i = 0; i < cirPath.size(); ++i) {
			auto cv = cirPath[i];
			auto tv = triPath[i];
			outputData[i].x = glm::lerp(cv.x, tv.x, amt);
			outputData[i].y = glm::lerp(cv.y, tv.y, amt);

			zOffsets[i] += zOffsetIncrement[i] * 10.0;

			if (zOffsets[i] > 200.0) {
				zOffsets[i] = -200.0;
			}
		}

		mInstanceZOffset->bufferData(sizeof(float) * zOffsets.size(), zOffsets.data(), GL_DYNAMIC_DRAW);

		mInstancePositions->bufferData(sizeof(ci::vec3)*outputData.size(), outputData.data(), GL_DYNAMIC_DRAW);

	}

	void draw() {
		gl::rotate(mTheta *0.05f);

		mBatch->getGlslProg()->uniform("theta", mTheta);
		mBatch->drawInstanced(mNumInstances);

	}
private:
	int mNumInstances;
	float mTheta;
	double mRadius, mStartA, mEndA;
	std::vector<ci::vec3> cirPath, triPath, outputData;
	std::vector<float> zOffsets, zOffsetIncrement;
	ci::gl::BatchRef mBatch;
	ci::gl::GlslProgRef mShader;


	// store instance data.
	ci::gl::VboRef mInstancePositions, mInstanceScales, mInstanceRotationOffset, mInstanceZOffset;
};

/*	int stride;
	std::vector<ci::vec2> lookupData;
	ci::gl::FboRef mTransitionData;
void buildBuffers() {

		// ================= build buffers ================= //
		gl::Texture::Format fmt;
		fmt.setInternalFormat(GL_RGBA32F);

		gl::Fbo::Format ffmt;
		ffmt.setColorTextureFormat(fmt);

		const ivec2 winSize = getWindowSize();
		const int32_t h = winSize.y;
		const int32_t w = winSize.x;
		try {

			// first texture stores circle data, second triangle data, last stores output data.
			for (size_t i = 0; i < 3; ++i) {
				ffmt.attachment(GL_COLOR_ATTACHMENT0 + (GLenum)i,
					gl::Texture::create(w,h,fmt));
			}

			mTransitionData = gl::Fbo::create(w, h, ffmt);
			const gl::ScopedFramebuffer scopedFramebuffer(mTransitionData);
			const gl::ScopedViewport scopedViewport(ivec2(0), mTransitionData->getSize());
			gl::clear();
		}
		catch (const std::exception& e) {
			console() << "mFboGBuffer failed: " << e.what() << std::endl;
		}


	}*/

