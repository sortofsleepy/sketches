#pragma once
#include "mocha/post/PostEffect.h"
#include <string>
#include <vector>
#include "cinder/Rand.h"
#define STRINGIFY(A) #A
class GlitchEffect : public mocha::post::PostEffect {

public:
	GlitchEffect() {
		updateSettings();
		setup(glitch);
		resize();

	
		Surface32f surf(64, 64, true);
		Area area(0, 0, 64, 64);
		Surface32f::Iter iter = surf.getIter(area);

		while (iter.line()) {
			while (iter.pixel()) {
				iter.r() = randFloat();
				iter.g() = randFloat();
				iter.b() = randFloat();
			}
		}


		gl::Texture::Format fmt;
		fmt.setInternalFormat(GL_RGBA32F);
		fmt.setWrap(GL_CLAMP_TO_EDGE, GL_CLAMP_TO_EDGE);
		fmt.setMinFilter(GL_NEAREST);
		fmt.setMagFilter(GL_NEAREST);
		
		mHeight = gl::Texture::create(surf,fmt);
	}

	void updateSettings(bool altSetttings = false) {
		if (!altSetttings) {
			amount = randFloat() / 30;
			angle = randFloat(-1 * M_PI, M_PI);
			seed = 0.002f;
			seed_x = randFloat();
			seed_y = randFloat();
			distortion_x = randFloat(-0.3f, 0.3f);
			distortion_y = randFloat(-0.3f, 0.3f);
			col_s = 0.05f;

			trigger = randInt(120, 240);
		}
		else {
			amount = randFloat() / 90;
			angle = randFloat(-1 * M_PI, M_PI);
			seed = 0.002f;
			seed_x = randFloat(-0.3f, 0.3f);
			seed_y = randFloat(-0.3f, 0.3f);
			distortion_x = randFloat();
			distortion_y = randFloat();
			col_s = 0.05f;
		}
	}

	void update() {
		gl::ScopedFramebuffer fbo(mFbo);
		gl::ScopedViewport    viewport(0, 0, mFbo->getWidth(), mFbo->getHeight());
		gl::ScopedTextureBind tex(mInput, 0);
		gl::ScopedTextureBind tex2(mHeight, 1);
		gl::setMatricesWindow(mFbo->getWidth(), mFbo->getHeight());

		if (curF % trigger == 0) {
			updateSettings();
		}
		else if (curF % trigger < trigger / 5) {
			updateSettings(true);
		}
	

		// set resolution. Necessary to calculate uv
		mPlane->getGlslProg()->uniform("resolution", vec2(mFbo->getWidth(), mFbo->getHeight()));

		// set input
		mPlane->getGlslProg()->uniform("uTex0", 0);
		mPlane->getGlslProg()->uniform("tDisp", 1);
		mPlane->getGlslProg()->uniform("time", (float)app::getElapsedSeconds());

		mPlane->getGlslProg()->uniform("amount", amount);
		mPlane->getGlslProg()->uniform("angle", angle);
		mPlane->getGlslProg()->uniform("seed", seed);
		mPlane->getGlslProg()->uniform("seed_x", seed_x);
		mPlane->getGlslProg()->uniform("seed_y", seed_y);
		mPlane->getGlslProg()->uniform("distortion_x", distortion_x);
		mPlane->getGlslProg()->uniform("distortion_y", distortion_y);
		mPlane->getGlslProg()->uniform("col_s", col_s);


		// draw triangle.
		mPlane->draw();

		curF++;
	}

private:
	ci::gl::TextureRef mHeight;
	float amount;
	float angle;
	float seed;
	float seed_x;
	float seed_y;
	float distortion_x;
	float distortion_y;
	float col_s;
	
	int trigger;
	int curF;

	// glitch shader adapted from Three.js
	std::string glitch = STRINGIFY(
		precision highp float;
		uniform sampler2D uTex0;
		uniform sampler2D tDisp;
		
		uniform float amount;
		uniform float angle;
		uniform float seed;
		uniform float seed_x;
		uniform float seed_y;
		uniform float distortion_x;
		uniform float distortion_y;
		uniform float col_s;
		uniform vec2 resolution;
		out vec4 glFragColor;

		float rand(vec2 co) {
			return fract(sin(dot(co.xy ,vec2(12.9898,78.233))) * 43758.5453);
		}
		void main() {
			vec2 p = gl_FragCoord.xy / resolution.xy;
			float xs = floor(gl_FragCoord.x / 0.5);
			float ys = floor(gl_FragCoord.y / 0.5);
			
			//based on staffantans glitch shader for unity https://github.com/staffantan/unityglitch
			vec4 normal = texture (tDisp, p*seed*seed);


			if(p.y<distortion_x+col_s && p.y>distortion_x-col_s*seed) {
				if(seed_x>0.){
					p.y = 1. - (p.y + distortion_y);
				}
				else {
					p.y = distortion_y;
				}
			}
			if(p.x<distortion_y+col_s && p.x>distortion_y-col_s*seed) {
				if(seed_y>0.){
					p.x=distortion_x;
				}
				else {
					p.x = 1. - (p.x + distortion_x);
				}
			}
			p.x+=normal.x*seed_x*(seed/5.);
			p.y+=normal.y*seed_y*(seed/5.);
			
			//base from RGB shift shader
			vec2 offset = amount * vec2( cos(angle), sin(angle));
			vec4 cr = texture(uTex0, p + offset);
			vec4 cga = texture(uTex0, p);
			vec4 cb = texture(uTex0, p - offset);
			glFragColor = vec4(cr.r, cga.g, cb.b, cga.a);
			
			//add noise
			vec4 snow = 200.*amount*vec4(rand(vec2(xs * seed,ys * seed*50.))*0.2);
			
			glFragColor = glFragColor;
			
			//glFragColor = vec4(1.);
		}

	);

};