

vec3 rotateY(vec3 p, float theta) {
  float s = sin(theta);
  float c = cos(theta);
  return vec3(p.x * c + p.z * s, p.y, p.z * c - p.x * s);
}

vec3 rotateX(vec3 p, float theta) {
  float s = sin(theta);
  float c = cos(theta);
  return vec3(p.x, p.y * c - p.z * s, p.z * c + p.y * s);
}

vec3 rotateZ(vec3 p, float theta) {
  float s = sin(theta);
  float c = cos(theta);
  return vec3(p.x * c - p.y * s, p.y * c + p.x * s, p.z);
}

/*

// returns the appropriate coordinate for a specific point in a texture
// when using a texture as a storage mechanism.
vec2 textureLookup(int modelId,float textureWidth,float textureHeight){
	float col = mod(modelId,textureWidth / 2.0) * 2.;
	float row = floor(modelId / textureWidth / 2.0);
	return vec2((col + 0.5) / textureWidth,(row + 0.5) / textureHeight;
}
*/