

uniform mat4 ciModelViewProjection;
uniform float theta;

in vec3 ciPosition;

in float instanceScale;
in float instanceRotationOffset;
in vec3 instancePosition;
in float instanceZOffset;



vec3 rotateY(vec3 p, float theta) {
  float s = sin(theta);
  float c = cos(theta);
  return vec3(p.x * c + p.z * s, p.y, p.z * c - p.x * s);
}

vec3 rotateX(vec3 p, float theta) {
  float s = sin(theta);
  float c = cos(theta);
  return vec3(p.x, p.y * c - p.z * s, p.z * c + p.y * s);
}

vec3 rotateZ(vec3 p, float theta) {
  float s = sin(theta);
  float c = cos(theta);
  return vec3(p.x * c - p.y * s, p.y * c + p.x * s, p.z);
}

void main(){


	vec3 pos = ciPosition;

	pos = rotateX(pos,theta * instanceRotationOffset);
	pos = rotateY(pos,theta * instanceRotationOffset);
	pos = rotateZ(pos,theta * instanceRotationOffset);


	pos += instancePosition;

	pos *= instanceScale;

	pos.z += instanceZOffset;
	

	pos = rotateZ(pos,theta * instanceRotationOffset * 0.5);

	gl_PointSize = 2.0;
	gl_Position = ciModelViewProjection * vec4(pos,1.);
}