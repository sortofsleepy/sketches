
#include "SphereSystem.h"
using namespace ci;
using namespace std;
using namespace mocha;
using namespace ci::app;

SphereSystem::SphereSystem():radius(20) {}

void SphereSystem::update() {

	mBuffer->update([=](gl::GlslProgRef shader)->void {
		shader->uniform("radius", (float)radius);
		shader->uniform("time", (float)app::getElapsedSeconds());
	});


}

void SphereSystem::draw() {
	// Rotate the cube by 0.2 degrees around the y-axis
	mRotation *= ci::rotate(ci::toRadians(0.5f), ci::normalize(vec3(1, 1, 1)));

	gl::ScopedMatrices mats();

	gl::multModelMatrix(mRotation);

	gl::setDefaultShaderVars();
	mParticleShape->getGlslProg()->uniform("time", (float)app::getElapsedSeconds());
	mParticleShape->drawInstanced(mNumParticles);

	/*
		auto vao = mBuffer->getVao();
	gl::ScopedVao v(vao);
	gl::ScopedGlslProg render(mRenderProg);
	gl::setDefaultShaderVars();
	gl::drawArrays(GL_POINTS, 0, mNumParticles);*/
}

void SphereSystem::setup(int mNumParticles, std::string vertex, std::string fragment) {

	mBuffer = mocha::GPU::TFBuffer::create();

	this->mNumParticles = mNumParticles;

	mShape = Pyramid();

	vector<Particle> particles;
	vector<float> rotationOffset;
	vector<float> opacity;

	particles.assign(mNumParticles, Particle());
	const float azimuth = 256.0f * M_PI / particles.size();
	const float inclination = M_PI / particles.size();
	const float radius = 180.0f;
	vec3 center = vec3(getWindowCenter() + vec2(0.0f, 40.0f), 0.0f);

	for (int i = 0; i < particles.size(); ++i)
	{	// assign starting values to particles.
		float x = radius * sin(inclination * i) * cos(azimuth * i);
		float y = radius * cos(inclination * i);
		float z = radius * sin(inclination * i) * sin(azimuth * i);

		auto &p = particles.at(i);
		p.pos = vec3(x, y, z);
		p.sphereInfo = vec4(

			randFloat(),
			randFloat(),
			randFloat(-1,1),
			randFloat(-1,1)
		);
		p.velocity = randVec3();
		p.acceleration = randVec3();
		/*
		p.phi = randFloat();
		p.theta = randFloat();
		p.phiSpeed = randFloat(-1, 1);
		p.thetaSpeed = randFloat(-1, 1);*/

		// note - this won't get input into the Transform Feedback process.
		rotationOffset.push_back(randFloat(-1, 1));

		opacity.push_back(1.0);
	}


	// build vbo for rotation
	ci::gl::VboRef rotationBuffer = gl::Vbo::create(GL_ARRAY_BUFFER, sizeof(float) * rotationOffset.size(), rotationOffset.data(), GL_DYNAMIC_DRAW);

	// builds VBOs for TransformFeedback
	mBuffer->bufferAttrib(particles);

	// go through and setup attributes, mapping to data.
	mBuffer->vertexAttribPointerInterleaved([=]()->void {

		gl::enableVertexAttribArray(0);
		gl::enableVertexAttribArray(1);
		gl::enableVertexAttribArray(2);
		gl::enableVertexAttribArray(3);

		gl::vertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Particle), (const GLvoid*)offsetof(Particle, pos));
		gl::vertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, sizeof(Particle), (const GLvoid*)offsetof(Particle, sphereInfo));
		gl::vertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, sizeof(Particle), (const GLvoid*)offsetof(Particle, velocity));
		gl::vertexAttribPointer(3, 3, GL_FLOAT, GL_FALSE, sizeof(Particle), (const GLvoid*)offsetof(Particle, acceleration));

	});

	// Load our update program.
	// Match up our attribute locations with the description we gave.
	mBuffer->loadShader("shaders/update.glsl", {
		"position",
		"sphereInfo",
		"velocity",
		"acceleration"
		}, GL_INTERLEAVED_ATTRIBS, [=](gl::GlslProg::Format fmt)->gl::GlslProg::Format {
		fmt.feedbackFormat(GL_INTERLEAVED_ATTRIBS)
			.attribLocation("iPosition", 0)
			.attribLocation("iSphereInfo", 1)
			.attribLocation("iVelocity", 2)
			.attribLocation("iAcceleration", 3);
		return fmt;
	});
	mBuffer->setNumPoints(mNumParticles);

	mRenderProg = mocha::loadShader(vertex,fragment);

	// =================== BUILD INSTANCED SHAPE ==================== //

	// setup the particle shape
	const gl::VboMeshRef shape = mShape.mMesh;

	// build  out instanced attributes for position and tie to VBO
	geom::BufferLayout positionLayout, rotationLayout;
	positionLayout.append(geom::Attrib::CUSTOM_0, 3, sizeof(Particle), offsetof(Particle, pos), 1 /* per instance */);
	rotationLayout.append(geom::CUSTOM_1, 1, 0, 0, 1);

	shape->appendVbo(positionLayout, mBuffer->getVbo());
	shape->appendVbo(rotationLayout, rotationBuffer);

	// setup particle batch
	mParticleShape = gl::Batch::create(shape, mRenderProg, {
		{ geom::CUSTOM_0, "iPosition" },
		{ geom::CUSTOM_1, "iRotation" }
	});
}

