#include "cinder/app/App.h"
#include "cinder/app/RendererGl.h"
#include "cinder/gl/gl.h"
#include "CinderImGui.h"
#include "ui/Interface.h"
#include "cinder/Log.h"
#include "SphereSystem.h"
#include "mocha/EasyCam.h"

#include "windows/AppWindow.h"
#include "ui/Settings.h"
#include "windows/GuiWindow.h"
using namespace ci;
using namespace ci::app;
using namespace std;
using namespace akarui::ui;

#define STRINGIFY(A) #A
class AkaruiApp : public App {
  public:
	void setup() override;

	EasyCamRef mCam;
	akarui::Settings * appSettings;

	SphereSystem sphere;

	AppWindow app;
	GuiWindow ui;
	BasicWindowRef main;

};

void AkaruiApp::setup()
{


	// create a new WindowRef for the settings.
	//WindowRef guiWindow = createWindow(app::Window::Format().size(vec2(640, 480)));
	//ui.setup(guiWindow);
	app.setup(getWindowIndex(0),appSettings);
	

}



CINDER_APP(AkaruiApp, RendererGl, [](App::Settings *settings) {
	settings->setWindowSize(1280, 720);
	settings->setMultiTouchEnabled(false);
});