

#include "AudioAnalyser.h"
using namespace ci;

namespace mocha {
	namespace audio {
		AudioAnalyser::AudioAnalyser():
			mFileIsLoaded(false),
			mIsPlaying(false),
			mFFTSize(4048){}

		AudioAnalyser::~AudioAnalyser() {
			mThread->join();
		}

		void AudioAnalyser::loadFile(fs::path filepath) {
			// indicate we've tried to load a file before so we can properly unload, then reload a new song.
			if (!mFileIsLoaded) {
				mFileIsLoaded = true;
			}
			else {
				unloadAndResetNodes();
			}


			auto ctx = ci::audio::Context::master();
			mIsPlaying = false;

			// By providing an FFT size double that of the window size, we 'zero-pad' the analysis data, which gives
			// an increase in resolution of the resulting spectrum data.
			auto monitorFormat = ci::audio::MonitorSpectralNode::Format().fftSize(mFFTSize).windowSize(app::getWindowWidth() * 3);
			mMonitorSpectralNode = ctx->makeNode(new ci::audio::MonitorSpectralNode(monitorFormat));
			mMonitorLeft = ctx->makeNode(new ci::audio::MonitorSpectralNode(monitorFormat));
			mMonitorRight = ctx->makeNode(new ci::audio::MonitorSpectralNode(monitorFormat));

			auto splitRouterLeft = ctx->makeNode(new ci::audio::ChannelRouterNode);
			auto splitRouterRight = ctx->makeNode(new ci::audio::ChannelRouterNode);

			// create a SourceFile and set its output samplerate to match the Context.
			ci::audio::SourceFileRef sourceFile = ci::audio::load(ci::loadFile(filepath), ctx->getSampleRate());
		
		

			// load the entire sound file into a BufferRef, and construct a BufferPlayerNode with this.
			ci::audio::BufferRef buffer = sourceFile->loadBuffer();
			mBufferPlayerNode = ctx->makeNode(new ci::audio::BufferPlayerNode(buffer));

			//mBufferPlayerNode >> mMonitorSpectralNode;

			// add a Gain to reduce the volume
			mGain = ctx->makeNode(new ci::audio::GainNode(0.5f));

			// connect and enable the Context
			mBufferPlayerNode >> mGain >> mMonitorSpectralNode >> ctx->getOutput();

			mBufferPlayerNode->enable();

			// pause player right away so it doesn't start playing automatically.
			mBufferPlayerNode->stop();

			mMonitorSpectralNode->enable();
			ctx->enable();

			// try to pull spectrum size.
			mMagSpectrum = mMonitorSpectralNode->getMagSpectrum();

			// build buffer for magnitude spectrum
			audioData = gl::Vbo::create(GL_ARRAY_BUFFER, sizeof(float) * mMagSpectrum.size(), nullptr, GL_DYNAMIC_DRAW);

			mTex = gl::BufferTexture::create(audioData, GL_RGBA32F);

		}
		void AudioAnalyser::loadFile(std::string filepath) {
			// indicate we've tried to load a file before so we can properly unload, then reload a new song.
			if (!mFileIsLoaded) {
				mFileIsLoaded = true;
			}
			else {
				unloadAndResetNodes();
			}


			auto ctx = ci::audio::Context::master();
			mIsPlaying = false;

			// By providing an FFT size double that of the window size, we 'zero-pad' the analysis data, which gives
			// an increase in resolution of the resulting spectrum data.
			auto monitorFormat = ci::audio::MonitorSpectralNode::Format().fftSize(mFFTSize).windowSize(app::getWindowWidth() * 3);
			mMonitorSpectralNode = ctx->makeNode(new ci::audio::MonitorSpectralNode(monitorFormat));
			mMonitorLeft = ctx->makeNode(new ci::audio::MonitorSpectralNode(monitorFormat));
			mMonitorRight = ctx->makeNode(new ci::audio::MonitorSpectralNode(monitorFormat));

			auto splitRouterLeft = ctx->makeNode(new ci::audio::ChannelRouterNode);
			auto splitRouterRight = ctx->makeNode(new ci::audio::ChannelRouterNode);

			// create a SourceFile and set its output samplerate to match the Context.
			ci::audio::SourceFileRef sourceFile = ci::audio::load(app::loadAsset(filepath), ctx->getSampleRate());

			// load the entire sound file into a BufferRef, and construct a BufferPlayerNode with this.
			ci::audio::BufferRef buffer = sourceFile->loadBuffer();
			mBufferPlayerNode = ctx->makeNode(new ci::audio::BufferPlayerNode(buffer));

			//mBufferPlayerNode >> mMonitorSpectralNode;

			// add a Gain to reduce the volume
			mGain = ctx->makeNode(new ci::audio::GainNode(0.5f));

			// connect and enable the Context
			mBufferPlayerNode >> mGain >> mMonitorSpectralNode >> ctx->getOutput();

			mBufferPlayerNode->enable();

			// pause player right away so it doesn't start playing automatically.
			mBufferPlayerNode->stop();

			mMonitorSpectralNode->enable();
			ctx->enable();

			// try to pull spectrum size.
			mMagSpectrum = mMonitorSpectralNode->getMagSpectrum();

			// build buffer for magnitude spectrum
			audioData = gl::Vbo::create(GL_ARRAY_BUFFER, sizeof(float) * mMagSpectrum.size(), nullptr, GL_DYNAMIC_DRAW);

			mTex = gl::BufferTexture::create(audioData, GL_RGBA32F);

		
		}

		void AudioAnalyser::unloadAndResetNodes() {
			auto ctx = ci::audio::Context::master();
			ctx->disable();
			ctx->disconnectAllNodes();
		}


		void AudioAnalyser::play() {
			mBufferPlayerNode->start();
			mBufferPlayerNode->seekToTime(currentPlayhead);
			mIsPlaying = !mIsPlaying;
		}

		void AudioAnalyser::pause() {
			currentPlayhead = mBufferPlayerNode->getReadPositionTime();
			mBufferPlayerNode->stop();
			mIsPlaying = !mIsPlaying;
		}
	}
}