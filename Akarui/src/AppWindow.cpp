#include "AppWindow.h"

using namespace ci;
using namespace std;

AppWindow::AppWindow() {}

void AppWindow::setup(ci::app::WindowRef window, akarui::Settings * appSettings) {
	mCam = EasyCam::create();
	mCam->setZoom(-100);

	this->window = window;
	this->appSettings = appSettings;
	currentScene = 0;

	// ensure things are set up with the correct Window's OpenGL context.
	window->getRenderer()->makeCurrentContext();

	scenes.push_back(&opening);

	scenes[currentScene]->setup();

	onDraw(std::bind(&AppWindow::draw, this));
	onClose(std::bind(&AppWindow::destroy, this));
}

void AppWindow::update() {

}

void AppWindow::draw() {
	window->getRenderer()->makeCurrentContext();
	gl::clear(Color(0, 0, 0));

}

void AppWindow::destroy() {}

