#include "objects/CrystalMesh.h"
using namespace std;
using namespace ci;


CrystalMesh::CrystalMesh() {}

void CrystalMesh::draw(bool usePBR) {



	if (meshRebuilt) {
		mBatch = gl::Batch::create(mMesh, mShader);

		meshRebuilt = false;

		if (!meshAvailable) {
			meshAvailable = true;
		}
	}

	if (meshAvailable) {

	
		gl::disableBlending();
		gl::disableAlphaBlending();
	
		gl::ScopedTextureBind tex0(mRadiance, 0);
	
		gl::ScopedTextureBind tex1(mIrradianceCube, 1);
		auto viewMatrix = gl::getViewMatrix();

	
		mBatch->getGlslProg()->uniform("uRadianceCubeMap", 0);
		mBatch->getGlslProg()->uniform("uIrradianceCubeMap", 1);

		mBatch->getGlslProg()->uniform("uMetallic", 0.5f);
		mBatch->getGlslProg()->uniform("uSpecular", 1.0f);
		mBatch->getGlslProg()->uniform("uGamma", 1.2f);
		mBatch->getGlslProg()->uniform("uExposure", 2.0f);
		mBatch->getGlslProg()->uniform("uRoughness", 0.5f);

		mBatch->getGlslProg()->uniform("usePBR", usePBR);

		mBatch->getGlslProg()->uniform("viewInverse", glm::inverse(viewMatrix));
		mBatch->draw();
	}
}

void CrystalMesh::buildTextureMaps() {

	auto cubeMapFormat = gl::TextureCubeMap::Format().mipmap().internalFormat(GL_RGB16F).minFilter(GL_LINEAR_MIPMAP_LINEAR).magFilter(GL_LINEAR);

	ImageSourceRef images[6] = {
		loadImage(app::loadAsset("pbr/irr_negx.png")),
		loadImage(app::loadAsset("pbr/irr_negy.png")),
		loadImage(app::loadAsset("pbr/irr_negz.png")),
		loadImage(app::loadAsset("pbr/irr_posx.png")),
		loadImage(app::loadAsset("pbr/irr_posy.png")),
		loadImage(app::loadAsset("pbr/irr_posz.png"))
	};

	mIrradianceCube = gl::TextureCubeMap::create(images,cubeMapFormat);
	mRadiance = gl::TextureCubeMap::createFromDds(app::loadAsset("pbr/studio_radiance.dds"),cubeMapFormat);
}

geom::SourceMods CrystalMesh::randShape() {
	// generate base geometry 
	int geometryID = randInt(0, 2);

	geom::SourceMods baseShape;

	switch (geometryID) {
	case 0:
		baseShape = geom::Cube();
		break;

	case 1:
		baseShape = geom::Icosahedron();
		break;

	case 2:
		baseShape = geom::Cylinder();
		break;
	}

	return baseShape;
}

void CrystalMesh::setShader(string vertex, string fragment) {
	gl::GlslProg::Format fmt;
	fmt.vertex(app::loadAsset(vertex));
	fmt.fragment(app::loadAsset(fragment));

	mShader = gl::GlslProg::create(fmt);
}

void CrystalMesh::setup(int maxElements, float boundingSphereRadius) {

	mShader = gl::getStockShader(gl::ShaderDef().color());

	buildTextureMaps();

	this->boundingSphereRadius = boundingSphereRadius;
	this->numElements = randInt(5, maxElements);

	buildShape = false;
	meshRebuilt = false;
	threadStop = false;
	meshAvailable = false;


	// setup thread for re-generation of crystals.
	gl::ContextRef backgroundCtx = gl::Context::create(gl::context());
	mThread = shared_ptr<thread>(new thread(bind(&CrystalMesh::generateNewCrystal, this, backgroundCtx)));
}

ci::vec3 CrystalMesh::attract(ci::vec3 targetPos, float targetMass, bool repel) {

	vec3 force = position - targetPos;
	float distance = glm::length(force);

	//distance = glm::normalize(distance);
	distance = ci::constrain(distance, 5.0f, 20.0f);

	force = glm::normalize(force);

	float strength = (G * mass * targetMass) / (distance * distance);


	if (distance < 6) {

		force *= vec3(0.);
	}
	else {
		force *= strength;
	}

	return force;

}

void CrystalMesh::generateNewCrystal(gl::ContextRef context) {


	ci::ThreadSetup threadSetup;
	context->makeCurrent();

	while (!threadStop) {
		if (buildShape) {


			vec3 up = vec3(0, 1, 0);
			float min = randFloat(0.5, 2);
			float max = min + randFloat(1, 3);
			float minOffset = randFloat(0.5, 1);
			float maxOffset = randFloat(1, 2);

			// holds final shape + transforms
			geom::SourceMods shape;

			for (int i = 0; i < numElements; ++i) {


				AxisAlignedBox bounds;
				geom::SourceMods g = randShape();

				auto sx = randFloat(0.1, 1);
				auto sy = randFloat(0.1, 1);
				auto sz = randFloat(min, max);

				// grab bounding box
				g = g >> geom::Bounds(&bounds);

				// scale mesh
				g = g >> geom::Transform(glm::scale(vec3(.5 * sx, .5 * sy, .5 * sz)));

				// calculate offset (may just need to set manually)
				float offset = randFloat(minOffset, maxOffset);
				g = g >> geom::Transform(glm::translate(vec3(0, 0, bounds.getMax().z * offset)));


				// rotate element
				float d = std::min(sx, sy);
				vec3 p = vec3(randFloat(-d, d), randFloat(-d, d), randFloat(-d, d));

				g = g >> geom::Transform(glm::lookAt(vec3(0), p, up));


				shape = shape & g;

			}


			mMesh = gl::VboMesh::create(shape);
			mVertices.clear();



			auto fence = gl::Sync::create();
			fence->clientWaitSync();




			// there isn't an easy way to extract attributes from built in geom sources, so unmesh things. 
			auto mappedPosAttrib = mMesh->mapAttrib3f(geom::Attrib::POSITION, false);

			for (int i = 0; i < mMesh->getNumVertices(); i++) {
				vec3 pos = *mappedPosAttrib;

				mVertices.push_back(pos);
				++mappedPosAttrib;
			}
			mappedPosAttrib.unmap();



			// grab index data.
			auto idxVBO = mMesh->getIndexVbo();
			auto indices = idxVBO->map(GL_READ_ONLY);

			auto idxArray = ((unsigned short*)indices);

			mIndices.clear();
			for (int i = 0; i < mMesh->getNumIndices(); ++i) {
				auto val = mocha::math::unsignedShortToInt(idxArray[i]);
				mIndices.push_back(val);
			}

			idxVBO->unmap();


			// we want to store random points on the generated face.
			auto faces = mocha::math::mapToFaces(mIndices);
			for (int i = 0; i < mVertices.size(); ++i) {
				auto face = faces[randFloat() * faces.size()];


				vec3 vertex1 = mVertices[face.x];
				vec3 vertex2;
				if (randFloat() > 0.5) {
					vertex2 = mVertices[face.y];
				}
				else {
					vertex2 = mVertices[face.z];
				}

				vec3 point = vertex2 - vertex1;
				point *= randFloat();
				point += vertex1;
				randomPoints.push_back(point);
			}

			// make sure while loop stops while we don't need it. 
			threadStop = true;

			// signal that the mesh was rebuilt.
			meshRebuilt = true;

			// signal that a new shape is ready to be built. 
			buildShape = false;
		}


	}

}