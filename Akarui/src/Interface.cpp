#include "ui/Interface.h"
#include "cinder/Log.h"

using namespace ci;
using namespace std;


namespace akarui {
	namespace ui {
		Interface::Interface() {}

		Interface& Interface::setupInterface() {
			try {
				ImGui::initialize(ImGui::Options().autoRender(false));
			}
			catch (Exception &e) {
				CI_LOG_E("Problem initializing ImGui - " << e.what());
			}

			return *this;
		}

		Interface& Interface::setupInterface(app::WindowRef window) {
			try {
				ImGui::initialize(ImGui::Options().window(window).autoRender(false));
			}
			catch (Exception &e) {
				CI_LOG_E("Problem initializing ImGui - " << e.what());
			}

			return *this;
		}


		void Interface::addLineBreak() {
			addItem("");
		}

		void Interface::addItem( std::string label) {

			InterfaceItem item;
			item.textContent = label;
			item.type = TEXT;

			mControls.push_back(item);
		}

		void Interface::addItem(std::string label, float * value, float min, float max) {
			InterfaceItem item;
			item.label = label;
			item.value = value;
			item.min = min;
			item.max = max;
			item.type = SLIDER;

			mControls.push_back(item);
		}

		void Interface::addItem(std::string label, std::function<void()> callback, vec2 size) {
			InterfaceItem item;
			item.label = label;
			item.type = BUTTON;
			item.callback = callback;

			mControls.push_back(item);
		}

		void Interface::renderInterface() {

		
			if (mControls.size() > 0) {
				for (InterfaceItem &item : mControls) {

					switch (item.type) {

					case TEXT:
						ImGui::Text(item.textContent.c_str());
						break;
					case BUTTON:

						if (ImGui::Button(item.label.c_str(), item.size)) {
							item.callback();
						}

						break;

					case SLIDER:
						ImGui::SliderFloat(item.label.c_str(), item.value, item.min, item.max);
						break;
					}
				}

				ImGui::Render();
				ImGui::NewFrame();
			}
		}
	}
}

