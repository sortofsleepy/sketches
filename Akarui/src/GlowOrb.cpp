#include "objects/GlowOrb.h"

using namespace ci;
using namespace std;

GlowOrb::GlowOrb() {
	mMesh = gl::VboMesh::create(geom::Icosahedron());
	mShader = gl::getStockShader(gl::ShaderDef().color());
	mBatch = gl::Batch::create(mMesh, mShader);
}

void GlowOrb::update(EasyCamRef mCam) {

}

void GlowOrb::draw() {
	gl::ScopedMatrices mats;

	// Rotate the cube by 0.2 degrees around the y-axis
	mRotation *= ci::rotate(ci::toRadians(2.0f), ci::normalize(vec3(1, 1, 1)));

	gl::scale(vec3(2));
	gl::multModelMatrix(mRotation);
	gl::ScopedColor color(1, 0, 0);
	mBatch->draw();
}