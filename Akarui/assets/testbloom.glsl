precision highp float;

uniform vec2 sample_offset;
uniform sampler2D uTex0;
uniform float attenuation;
uniform float time;
in vec2 vUv;

out vec4 glFragColor;
void main(){
     vec3 sum = vec3( 0.0, 0.0, 0.0 );
     vec3 sum2 = vec3(0.);
	 vec4 texDat = texture( uTex0, vUv );


     glFragColor = texDat;
	 
}
