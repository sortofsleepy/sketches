#version 330
uniform samplerCube uRadianceCubeMap;
uniform samplerCube uIrradianceCubeMap;
uniform bool usePBR = false;
uniform float uMetallic;
uniform float uSpecular;
uniform float uExposure;
uniform float uRoughness;
uniform float uGamma;


in vec3 vPosition;
in vec3 vWsPosition;
in vec3 vEyePosition;
in mat3 vNormalMatrix;
in mat4 vViewInverse;

#include "pbr.glsl"


const vec3 uBaseColor = vec3(1.0,0.0,1.0);
out vec4 glFragColor;
void main(){

   // ====== calculate normals ========== //
   vec3 x = dFdx(vPosition);
   vec3 y = dFdy(vPosition);
   vec3 normal = normalize(cross(x,y));
   vec3 vNormal = vNormalMatrix * normal;
   vec3 vWsNormal = vec3(vViewInverse * vec4(vNormal,0.0));

   // ====== PBR STUFF ======== //
   vec3 N = normalize(vWsNormal);
   vec3 V = normalize(vEyePosition);


   vec3 color = getPbr(N,V,uRadianceCubeMap,uIrradianceCubeMap,uBaseColor,uRoughness,uMetallic,uSpecular);
   color = toneMap(color * uExposure);
   color = color * (1.0 / toneMap(vec3(10.0)));
   color = pow(color,vec3(1.0 / uGamma));

   float g = (color.r + color.g + color.b);

   vec4 finalColor = vec4(color * g , 1.);
   vec4 f = vec4(mix(N,V,0.8),1.) * finalColor;
	glFragColor = finalColor * f;
}


