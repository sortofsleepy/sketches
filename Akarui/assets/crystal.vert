

#version 150
uniform mat4 ciProjectionMatrix;
uniform mat4 ciModelView;
uniform mat4 ciViewMatrix;
uniform mat3 ciNormalMatrix;
uniform mat4 ciModelMatrix;
uniform mat4 viewInverse;

in vec3 ciPosition;


out vec3 vPosition;
out vec3 vWsPosition;
out vec3 vEyePosition;
out mat3 vNormalMatrix;
out mat4 vViewInverse;

void main(){

	vec4 worldSpacePosition = ciModelMatrix * vec4(ciPosition,1.);
    vec4 viewSpacePosition = ciViewMatrix * worldSpacePosition;

    vec4 eyeDirViewSpace = viewSpacePosition - vec4(0.0,0.0,0.0,1.0);
	
	vPosition = ciPosition;
    vWsPosition = worldSpacePosition.xyz;
    vEyePosition = -vec3(ciViewMatrix * eyeDirViewSpace);
    vNormalMatrix = ciNormalMatrix;
    vViewInverse = viewInverse;



	gl_Position = ciProjectionMatrix * ciModelView * vec4(ciPosition,1.);
}