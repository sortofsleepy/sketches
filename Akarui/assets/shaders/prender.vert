#version 330 core

uniform float time;
uniform mat4 ciModelViewProjection;

in vec3 ciPosition;

// instanced attributes
in vec3 iPosition;
in float iRotation;

#include "utils.glsl"
void main(){

	vec3 pos = ciPosition;

	pos *= 0.5;

	pos = rotateX(pos,time * iRotation);
	pos = rotateY(pos,time * iRotation);
	pos = rotateZ(pos,time * iRotation);


	vec3 position = pos + iPosition;



	gl_Position = ciModelViewProjection * vec4(position,1.);
}
