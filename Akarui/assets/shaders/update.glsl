#version 330 core
uniform float time;
uniform float radius;

in vec3  iPosition;
in vec4 iSphereInfo;
in vec3 iVelocity;
in vec3 iAcceleration;

out vec3  position;
out vec4 sphereInfo;
out vec3 velocity;
out vec3 acceleration;

#include "noise.glsl"

void main()
{
	position = iPosition;
	sphereInfo = iSphereInfo;
	velocity = iVelocity;
	acceleration = iAcceleration;

	// order is phi, theta, phiSpeed, thetaSpeed
	sphereInfo.x += sphereInfo.z * 0.005;
	sphereInfo.y +=  sphereInfo.w * 0.005;

	float r = radius;
	float x = cos(sphereInfo.y) * sin(sphereInfo.x) * r;
	float y = sin(sphereInfo.y) * sin(sphereInfo.x) * r;
	float z = cos(sphereInfo.x) * r;

	

	position = vec3(x,y,z);

}
