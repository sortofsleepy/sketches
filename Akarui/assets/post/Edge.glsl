#version 150
uniform vec2 resolution;
uniform sampler2D uTex0;
uniform bool showEdge;
uniform float time;

// color for the edges. Defaults to Magenta.
uniform vec4 edgeColor;

// optional background color to better match the scene. Defaults to black.
uniform vec4 backgroundColor;

out vec4 glFragColor;

in vec2 uv;
in vec2 vUv;

float GetTexture(float x, float y)
{
    return texture(uTex0, vec2(x, y) / resolution.xy).x;
}

vec4 edgeDetect(){
	vec4 fragColor = vec4(0.);
	float threshold = 0.5;

	vec4 edgeColoring = edgeColor;
	if(edgeColor.x == 0.0){
		edgeColoring = vec4(1.0,0.0,1.0,1.0);
	}


	// TODO link to audio
	//edgeColoring.x *= sin(time);
	//edgeColoring.y *= cos(time);
	//edgeColoring.z = edgeColoring.x * edgeColoring.y;

    
    float x = gl_FragCoord.x;
    float y = gl_FragCoord.y;
    
    float xValue = -GetTexture(x-1.0, y-1.0) - 2.0*GetTexture(x-1.0, y) - GetTexture(x-1.0, y+1.0)
        + GetTexture(x*1.0, y-1.0) + 2.0*GetTexture(x+1.0, y) + GetTexture(x-1.0, y);
    
    float yValue = GetTexture(x-1.0, y-1.0) + 2.0*GetTexture(x, y-1.0) + GetTexture(x+1.0, y-1.0)
        - GetTexture(x-1.0, y+1.0) - 2.0*GetTexture(x, y+1.0) - GetTexture(x+1.0, y+1.0);
    
    if(length(vec2(xValue, yValue)) > threshold)
    {
        fragColor = edgeColoring;
    }
    else
    {
        //vec2 uv = vec2(x, y) / resolution.xy;
    	//vec4 currentPixel = texture(uTex0, uv);
        
		vec4 bg = backgroundColor;

		if(bg.r == 0.0 && bg.g == 0.0 && bg.b == 0.0){
			bg = vec4(0.);
		}

		// we only need the edge so all other pixels should be black or match the background.
		fragColor = bg;
    }

	return fragColor;
}


void main(){
	
	glFragColor = edgeDetect();
}