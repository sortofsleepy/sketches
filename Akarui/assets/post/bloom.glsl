precision highp float;

uniform vec2 sample_offset;
uniform sampler2D uTex0;
uniform float attenuation;
uniform float time;
in vec2 vUv;

out vec4 glFragColor;
void main(){
     vec3 sum = vec3( 0.0, 0.0, 0.0 );
     vec3 sum2 = vec3(0.);
	 vec4 texDat = texture( uTex0, vUv );

     sum += texture( uTex0, vUv + -10.0 * sample_offset.x ).rgb * 0.009167927656011385;
     sum += texture( uTex0, vUv +  -9.0 * sample_offset.x ).rgb * 0.014053461291849008;
     sum += texture( uTex0, vUv +  -8.0 * sample_offset.x ).rgb * 0.020595286319257878;
     sum += texture( uTex0, vUv +  -7.0 * sample_offset.x ).rgb * 0.028855245532226279;
     sum += texture( uTex0, vUv +  -6.0 * sample_offset.x ).rgb * 0.038650411513543079;
     sum += texture( uTex0, vUv +  -5.0 * sample_offset.x ).rgb * 0.049494378859311142;
     sum += texture( uTex0, vUv +  -4.0 * sample_offset.x ).rgb * 0.060594058578763078;
     sum += texture( uTex0, vUv +  -3.0 * sample_offset.x ).rgb * 0.070921288047096992;
     sum += texture( uTex0, vUv +  -2.0 * sample_offset.x ).rgb * 0.079358891804948081;
     sum += texture( uTex0, vUv +  -1.0 * sample_offset.x ).rgb * 0.084895951965930902;
     sum += texture( uTex0, vUv +   0.0 * sample_offset.x ).rgb * 0.086826196862124602;
     sum += texture( uTex0, vUv +  +1.0 * sample_offset.x ).rgb * 0.084895951965930902;
     sum += texture( uTex0, vUv +  +2.0 * sample_offset.x ).rgb * 0.079358891804948081;
     sum += texture( uTex0, vUv +  +3.0 * sample_offset.x ).rgb * 0.070921288047096992;
     sum += texture( uTex0, vUv +  +4.0 * sample_offset.x ).rgb * 0.060594058578763078;
     sum += texture( uTex0, vUv +  +5.0 * sample_offset.x ).rgb * 0.049494378859311142;
     sum += texture( uTex0, vUv +  +6.0 * sample_offset.x ).rgb * 0.038650411513543079;
     sum += texture( uTex0, vUv +  +7.0 * sample_offset.x ).rgb * 0.028855245532226279;
     sum += texture( uTex0, vUv +  +8.0 * sample_offset.x ).rgb * 0.020595286319257878;
     sum += texture( uTex0, vUv +  +9.0 * sample_offset.x ).rgb * 0.014053461291849008;
     sum += texture( uTex0, vUv + +10.0 * sample_offset.x ).rgb * 0.009167927656011385;

	 sum2 += texture( uTex0, vUv + -10.0 * sample_offset.y ).rgb * 0.009167927656011385;
     sum2 += texture( uTex0, vUv +  -9.0 * sample_offset.y ).rgb * 0.014053461291849008;
     sum2 += texture( uTex0, vUv +  -8.0 * sample_offset.y ).rgb * 0.020595286319257878;
     sum2 += texture( uTex0, vUv +  -7.0 * sample_offset.y ).rgb * 0.028855245532226279;
     sum2 += texture( uTex0, vUv +  -6.0 * sample_offset.y ).rgb * 0.038650411513543079;
     sum2 += texture( uTex0, vUv +  -5.0 * sample_offset.y ).rgb * 0.049494378859311142;
     sum2 += texture( uTex0, vUv +  -4.0 * sample_offset.y ).rgb * 0.060594058578763078;
     sum2 += texture( uTex0, vUv +  -3.0 * sample_offset.y ).rgb * 0.070921288047096992;
     sum2 += texture( uTex0, vUv +  -2.0 * sample_offset.y ).rgb * 0.079358891804948081;
     sum2 += texture( uTex0, vUv +  -1.0 * sample_offset.y ).rgb * 0.084895951965930902;
     sum2 += texture( uTex0, vUv +   0.0 * sample_offset.y ).rgb * 0.086826196862124602;
     sum2 += texture( uTex0, vUv +  +1.0 * sample_offset.y ).rgb * 0.084895951965930902;
     sum2 += texture( uTex0, vUv +  +2.0 * sample_offset.y ).rgb * 0.079358891804948081;
     sum2 += texture( uTex0, vUv +  +3.0 * sample_offset.y ).rgb * 0.070921288047096992;
     sum2 += texture( uTex0, vUv +  +4.0 * sample_offset.y ).rgb * 0.060594058578763078;
     sum2 += texture( uTex0, vUv +  +5.0 * sample_offset.y ).rgb * 0.049494378859311142;
     sum2 += texture( uTex0, vUv +  +6.0 * sample_offset.y ).rgb * 0.038650411513543079;
     sum2 += texture( uTex0, vUv +  +7.0 * sample_offset.y ).rgb * 0.028855245532226279;
     sum2 += texture( uTex0, vUv +  +8.0 * sample_offset.y ).rgb * 0.020595286319257878;
     sum2 += texture( uTex0, vUv +  +9.0 * sample_offset.y ).rgb * 0.014053461291849008;
     sum2 += texture( uTex0, vUv + +10.0 * sample_offset.y ).rgb * 0.009167927656011385;

     glFragColor = vec4(attenuation * attenuation * mix(sum,sum2,0.5) * mix(sum,sum2,1.0) * 23.23082,1.0);
	 //glFragColor=vec4(1.);
}
