#include "mocha/Compositor.h"
#include "cinder/Log.h"
using namespace ci;
using namespace std;

namespace mocha {

	Compositor::Compositor() {}
	Compositor::Compositor(std::vector<ci::gl::TextureRef> mTextures) {
		this->mTextures = mTextures;
	}

	void Compositor::compile() {

		// build string to form texture array uniform
		string textures = "uniform sampler2D uTextures[" +
			std::to_string(mTextures.size()) +
			"];\n";

		// store the number of textures as a constant value
		string numTextures = "const int numTextures = " + std::to_string(mTextures.size()) + ";\n";

		// form the shader
		string f = textures + numTextures + fragment;


		// compile shader
		gl::GlslProg::Format fmt;
		fmt.vertex(vertex);
		fmt.fragment(f);

		mShader = gl::GlslProg::create(fmt);


		//  build FBO
		gl::Fbo::Format ffmt;
		gl::Texture::Format tfmt;

		tfmt.setWrap(GL_CLAMP_TO_EDGE, GL_CLAMP_TO_EDGE);
		tfmt.setMagFilter(GL_NEAREST);
		tfmt.setMinFilter(GL_NEAREST);
		tfmt.setInternalFormat(GL_RGBA32F);

		ffmt.setColorTextureFormat(tfmt);
		mFbo = gl::Fbo::create(app::getWindowWidth(), app::getWindowHeight(), ffmt);

		mRenderShader = gl::getStockShader(gl::ShaderDef().texture());
	}


	void Compositor::addTexture(ci::gl::TextureRef mTex) {
		mTextures.push_back(mTex);
	}

	void Compositor::update() {
		gl::ScopedFramebuffer fbo(mFbo);
		gl::clear(ColorA(0, 0, 0, 0), true);

		gl::enableBlending();
		gl::enableAdditiveBlending();
		gl::ScopedGlslProg shd(mShader);

		for (int i = 0; i < mTextures.size(); ++i) {
			mTextures[i]->bind(i);

			// form uniform name
			string uname = "uTextures[" + std::to_string(i) + "]";

			// set uniform value
			mShader->uniform(uname, i);

			mTextures[i]->unbind(i);

		}

		gl::ScopedViewport view(0, 0, app::getWindowWidth(), app::getWindowHeight());
		gl::pushMatrices();
		gl::setMatricesWindow(app::getWindowSize());
		
		gl::drawSolidRect(Rectf(0, 0, app::getWindowWidth(), app::getWindowHeight()));
		gl::popMatrices();
		for (int i = 0; i < mTextures.size(); ++i) {
			mTextures[i]->unbind(i);
		}

	}

	void Compositor::drawOutput() {
		gl::setMatricesWindow(app::getWindowSize());
		gl::viewport(app::getWindowSize());

		gl::ScopedGlslProg ren(mRenderShader);
		gl::ScopedTextureBind tex0(mFbo->getColorTexture(), 0);
		gl::setMatricesWindow(app::getWindowSize());
		gl::viewport(app::getWindowSize());

		mRenderShader->uniform("uTex0", 0);
		gl::drawSolidRect(Rectf(0, 0, app::getWindowWidth(), app::getWindowHeight()));



	}


}