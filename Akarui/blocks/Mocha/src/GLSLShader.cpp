
#include "mocha/GLSLShader.h"

using namespace ci;
using namespace std;

namespace mocha {
	GLSLShader::GLSLShader():
		shaderBuilt(false),
		geometryChunks(""),
		version(330){
	}

	void GLSLShader::bind() {
		if (shaderBuilt) {
			mGLSLShader->bind();
		}
		else {
			try {
				mGLSLShader = gl::GlslProg::create(mFormat);
			}
			catch (gl::GlslProgExc &e) {
				CI_LOG_E(e.what());
			}
			shaderBuilt = true;
		}
	}

	void GLSLShader::buildShaderFormat() {
	
		mFormat.vertex(vertexChunks);
		mFormat.fragment(fragmentChunks);
		mFormat.version(version);
		if (geometryChunks != "") {
			mFormat.geometry(geometryChunks);
		}
	}

	ci::gl::GlslProgRef GLSLShader::getShader() {
		if (!shaderBuilt) {
			buildShaderFormat();
			try {
				mGLSLShader = gl::GlslProg::create(mFormat);
				shaderBuilt = true;
			}
			catch (gl::GlslProgExc &e) {
				CI_LOG_E(e.what());
			}


		}
		return mGLSLShader;
	}

	GLSLShader& GLSLShader::loadVertexChunks(std::vector<std::string> pathorchunks) {

		for (string chunk : pathorchunks) {
			loadVertexChunk(chunk);
		}

		return *this;
	}

	GLSLShader& GLSLShader::loadFragmentChunks(std::vector<std::string> pathorchunks) {

		for (string chunk : pathorchunks) {
			loadFragmentChunk(chunk);
		}

		return *this;
	}


	GLSLShader& GLSLShader::loadGeometryChunks(std::vector<std::string> pathorchunks) {

		for (string chunk : pathorchunks) {
			loadGeometryChunk(chunk);
		}

		return *this;
	}


	GLSLShader& GLSLShader::loadVertexChunk(std::string pathorchunk) {
		
		// check to see if this might be a path first, if it is, we use Cinder's loadAsset function.
		
		
		// First check is simple - see if there's an include statement
		auto hasInclude = pathorchunk.find("include");
		if (hasInclude != std::string::npos) {
			vertexChunks += pathorchunk + "\n";
			return *this;
		}

		// check to see if we have two consecutive "/" characters, if we do, it's also likely not a path
		auto hasDoubleSlash = pathorchunk.find("//");
		if (hasDoubleSlash != std::string::npos) {
			vertexChunks += pathorchunk + "\n";
			return *this;
		}


		// lastly do an extention check 
		auto extentionCheck = pathorchunk.find(".vert");
		auto glslCheck = pathorchunk.find(".glsl");
		if (extentionCheck != std::string::npos ||
			glslCheck != std::string::npos) {
			mFormat.vertex(app::loadAsset(pathorchunk));

		}
		else {
			vertexChunks += pathorchunk + "\n";
		}
		
		
	
		return *this;

	}

	GLSLShader& GLSLShader::loadFragmentChunk(std::string pathorchunk) {

		// check to see if this might be a path first, if it is, we use Cinder's loadAsset function.


		// First check is simple - see if there's an include statement
		auto hasInclude = pathorchunk.find("include");
		if (hasInclude != std::string::npos) {
			fragmentChunks += pathorchunk + "\n";
			return *this;
		}

		// check to see if we have two consecutive "/" characters, if we do, it's also likely not a path
		auto hasDoubleSlash = pathorchunk.find("//");
		if (hasDoubleSlash != std::string::npos) {
			fragmentChunks += pathorchunk + "\n";
			return *this;
		}



		// lastly do an extention check 
		auto extentionCheck = pathorchunk.find(".frag");
		auto glslCheck = pathorchunk.find(".glsl");
		if (extentionCheck != std::string::npos ||
			glslCheck != std::string::npos) {
			mFormat.fragment(app::loadAsset(pathorchunk));

		}
		else {
			fragmentChunks += pathorchunk + "\n";
		}

		
		return *this;
	}


	GLSLShader& GLSLShader::loadGeometryChunk(std::string pathorchunk) {

		// check to see if this might be a path first, if it is, we use Cinder's loadAsset function.


		// First check is simple - see if there's an include statement
		auto hasInclude = pathorchunk.find("include");
		if (hasInclude != std::string::npos) {
			geometryChunks += pathorchunk + "\n";
			return *this;
		}

		// check to see if we have two consecutive "/" characters, if we do, it's also likely not a path
		auto hasDoubleSlash = pathorchunk.find("//");
		if (hasDoubleSlash != std::string::npos) {
			geometryChunks += pathorchunk + "\n";
			return *this;
		}

		// lastly do an extention check 
		auto extentionCheck = pathorchunk.find(".geom");
		auto glslCheck = pathorchunk.find(".glsl");
		if (extentionCheck != std::string::npos ||
			glslCheck != std::string::npos) {
			mFormat.geometry(app::loadAsset(pathorchunk));

		}
		else {
			geometryChunks += pathorchunk + "\n";
		}
		return *this;
	}
}