#include "mocha/PingpongBuffer.h"

using namespace std;
using namespace ci;

namespace mocha {
	PingpongBuffer::PingpongBuffer(int width,int height):current(0),target(1),shaderSet(false) {
		this->width = width;
		this->height = height;
	}

	void PingpongBuffer::setupBuffers() {
		gl::Texture::Format tfmt;
		tfmt.setInternalFormat(GL_RGBA);

		gl::Fbo::Format fmt;
		fmt.setColorTextureFormat(tfmt);

		for (int i = 0; i < 2; ++i) {
			mFbos[i] = gl::Fbo::create(width, height, fmt);

			gl::ScopedFramebuffer fbo(mFbos[i]); {
				gl::clear(ColorA(0, 0, 0, 0));
			}
		}
	}

	void PingpongBuffer::updateBuffers() {
		
		if (shaderSet) {

			gl::ScopedFramebuffer fbo(mFbos[current]);
			gl::ScopedViewport view(0, 0, width, height);

			gl::ScopedTextureBind tex0(mFbos[target]->getColorTexture(), 0);
			gl::ScopedGlslProg shader(mShader);

			mShader->uniform("uTex0", 0);
			mShader->uniform("resolution", vec2(width, height));
			gl::drawSolidRect(mFbos[current]->getBounds());

			std::swap(current, target);
		}
	}

	void PingpongBuffer::setShader(std::string path) {

		gl::GlslProg::Format fmt;
		fmt.vertex(vertexShader);
		fmt.fragment(app::loadAsset(path));


		try {
			mShader = gl::GlslProg::create(fmt);
			shaderSet = true;
		}
		catch (ci::gl::GlslProgCompileExc &e) {
			CI_LOG_E(e.what());
		}
	}


}