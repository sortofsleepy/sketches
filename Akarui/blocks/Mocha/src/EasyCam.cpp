#include "mocha/EasyCam.h"
using namespace ci;
using namespace std;
using namespace ci::app;
EasyCam::EasyCam(float fov, float aspect, float near, float far, vec3 eye, vec3 target) {

	mCam = CameraPersp(getWindowWidth(), getWindowHeight(), fov, near, far);
	mCam.lookAt(eye, target);
	mCamUi = CameraUi(&mCam, getWindow(), -1);
	mCam.setAspectRatio(aspect);

	this->target = target;

}

EasyCam::EasyCam(app::WindowRef window, float fov, float aspect, float near, float far, vec3 eye, vec3 target) {
	
	mCam = CameraPersp(window->getWidth(),window->getHeight(), fov, near, far);
	mCam.lookAt(eye, target);
	mCamUi = CameraUi(&mCam, window, -1);
	mCam.setAspectRatio(aspect);

	this->target = target;


}


float EasyCam::getNear() {
	return mCam.getNearClip();
}

float EasyCam::getFar() {
		
	return mCam.getFarClip();
}

void EasyCam::setPerspective(float fov, float aspect, float near, float far) {
	mCam.setPerspective(fov, aspect, near, far);
}

void EasyCam::lookAt(vec3 eye, vec3 target) {
	mCam.lookAt(eye, target);
}

void EasyCam::orbitTarget(float speed) {

	float deltaX = (target.x - speed) / (float)getWindowSize().x * mCam.getPivotDistance();
	float deltaY = (target.z - 0.0) / (float)getWindowSize().y * mCam.getPivotDistance();

	vec3 right, up;
	mCam.getBillboardVectors(&right, &up);
	mCam.setEyePoint(mCam.getEyePoint() - right * deltaX + up * deltaY);

	mCam.lookAt(target);

}

void EasyCam::setEye(ci::vec3 position) {
	mCam.setEyePoint(position);
}

void EasyCam::orbitTarget(float speed, ci::vec3 target) {

	float deltaX = (target.x - speed) / (float)getWindowSize().x * mCam.getPivotDistance();
	float deltaY = (target.z - 0.0) / (float)getWindowSize().y * mCam.getPivotDistance();

	vec3 right, up;
	mCam.getBillboardVectors(&right, &up);
	mCam.setEyePoint(mCam.getEyePoint() - right * deltaX + up * deltaY);
	mCam.lookAt(target);

}

void EasyCam::enableControls() {
	getWindow()->getSignalMouseDown().connect([this](MouseEvent event) {
		mMouse.stop();
		mMouse = event.getPos();
		mCamUi.mouseDown(mMouse());

	});

	getWindow()->getSignalMouseDrag().connect([this](MouseEvent event) {
		mCamUi.mouseDrag(mMouse(), event.isLeftDown(), false, event.isRightDown());
	});

}

void EasyCam::enableTweening() {
#ifdef CINDER_MAC 
	getWindow()->getSignalMouseDown().connect([this](MouseEvent event) {
		mMouse.stop();
		mMouse = event.getPos();
		mCamUi.mouseDown(mMouse());

	});

	getWindow()->getSignalMouseDrag().connect([this](MouseEvent event) {
		timeline().apply(&mMouse, vec2(event.getPos()), 1.0f, EaseOutQuint()).updateFn([=] {
			mCamUi.mouseDrag(mMouse(), event.isLeftDown(), false, event.isRightDown());
		});
	});

#endif

}


void EasyCam::rotateView(ci::mat4 rotateMat) {
	getWindow()->getSignalMouseDown().connect([this](MouseEvent event) {
		mMouse.stop();
		mMouse = event.getPos();
		mCamUi.mouseDown(mMouse());

	});

	getWindow()->getSignalMouseDrag().connect([this](MouseEvent event) {
		mCamUi.mouseDrag(mMouse(), event.isLeftDown(), false, event.isRightDown());
	});

}


void EasyCam::setMatrices() {
	gl::setMatrices(mCam);
}
void EasyCam::setTarget(ci::vec3 target) {
	mCam.lookAt(target);
}
void EasyCam::setFar(float far) {
	mCam.setFarClip(far);
}
void EasyCam::setNear(float near) {
	mCam.setNearClip(near);
}
void EasyCam::setZoom(float zoom) {
	auto currentEye = mCam.getEyePoint();
	currentEye.z = zoom;
	mCam.lookAt(currentEye, target);
}