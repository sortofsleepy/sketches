#include "mocha/DeferredScene.h"
#include "cinder/Log.h"

using namespace ci;
using namespace std;
using namespace mocha::DeferredRendering;
using namespace mocha::shaders::DeferredRenderer;
using namespace mocha::shaders;
using namespace ci::app;


namespace mocha {

	DeferredScene::DeferredScene() :
		mOffset(0.0f),
		enableShadows(true),
		mTrailAlpha(0.3),
		mResolution(app::getWindowSize()),
		ping(0),
		pong(1){}

	void DeferredScene::setup(ci::CameraPersp mainCam) {
		auto camera = EasyCam::create(mainCam);
		setup(camera);
	}

	void DeferredScene::setup(EasyCamRef mainCam) {

		// store some variables
		mCam = mainCam;
		projMatrixInverse = glm::inverse(mCam->getProjectionMatrix());
	
		// build rendering batch
		auto rect = gl::VboMesh::create(geom::Rect());
		auto shader = gl::getStockShader(gl::ShaderDef().texture());
		auto colorShader = gl::getStockShader(gl::ShaderDef().color());

		mBatchColorRect = gl::Batch::create(rect, colorShader);
		mRenderBatch = gl::Batch::create(rect, shader);


		// build default lighting mesh in case light does not provide it's own mesh.
		auto shdFmt = DeferredRenderer::getSimpleLightingFormat();
		const gl::VboMeshRef cube = gl::VboMesh::create(geom::Cube().size(vec3(2.0f)));
		const gl::GlslProgRef lBuffer = DeferredRenderer::getSimpleLightingShader();
		mBatchLBufferCube = gl::Batch::create(cube, lBuffer);
		mBatchLBufferCube->getGlslProg()->uniform("uSamplerAlbedo", 0);
		mBatchLBufferCube->getGlslProg()->uniform("uSamplerNormalEmissive", 1);
		mBatchLBufferCube->getGlslProg()->uniform("uSamplerPosition", 2);

		// build rect for light accumulation
		gl::GlslProg::Format lightAccumFmt;
		lightAccumFmt.vertex(mocha::shaders::passthru);
		lightAccumFmt.fragment(DeferredRenderer::emissive);

		gl::GlslProgRef accumShader;

		try {
			accumShader = gl::GlslProg::create(lightAccumFmt);
		}
		catch (ci::gl::GlslProgCompileExc &e) {
			CI_LOG_E(e.what());
		}

		mBatchEmissiveRect = gl::Batch::create(gl::VboMesh::create(geom::Rect()), accumShader);
		
		// build FBOs needed
		buildBuffers();

		// setup shadow camera
		mShadowCam = EasyCam::create(120.0f, mShadowBuffer->getAspectRatio(), mCam->getNear(), mCam->getFar());
		mShadowCam->lookAt(vec3(0.0, 0.7, 0.0), vec3(0.0));


	}

	void DeferredScene::setShadowCamera(ci::CameraPersp shadowCam) {
		mShadowCam = EasyCam::create(shadowCam);
	}

	void DeferredScene::setShadowCamera(EasyCamRef shadowCam) {
		this->mShadowCam = shadowCam;
	}

	void DeferredScene::draw() {

		// Set up window for screen render
		const gl::ScopedViewport scopedViewport(ivec2(0), ci::app::toPixels(app::getWindowSize()));
		const gl::ScopedMatrices scopedMatrices;
		gl::disableDepthRead();
		gl::disableDepthWrite();
		gl::clear();
		gl::translate(app::getWindowCenter());
		gl::scale(app::getWindowSize());


		const gl::ScopedTextureBind scopedTextureBind(mFboAccum->getColorTexture(), 0);
		//const gl::ScopedTextureBind scopedTextureBind(mLBuffer[ping]->getColorTexture(), 0);
		mRenderBatch->draw();


	}

	void DeferredScene::updateLights(std::function<void(Light)> func) {
		for (int i = 0; i < mLights.size(); ++i) {
			func(mLights[i]);
		}
	}

	void DeferredScene::update(std::function<void()> func, std::function<void()> shadowFunc) {
		// ========== GEOMETRY STAGE ============= //
		{
			gl::ScopedFramebuffer g(mGBuffer);
			const static GLenum buffers[] = {
				GL_COLOR_ATTACHMENT0,
				GL_COLOR_ATTACHMENT1,
				GL_COLOR_ATTACHMENT2
			};
			gl::drawBuffers(3, buffers);
			const gl::ScopedViewport scopedViewport(ivec2(0), mGBuffer->getSize());
			const gl::ScopedMatrices scopedMatrices;
			gl::enableDepthRead();
			gl::enableDepthWrite();
			gl::clear();

			mCam->setMatrices();

			func();
		}
		// ========== SHADOWING STAGE ============= //
		if (enableShadows && shadowFunc != nullptr) {
		
			const gl::ScopedFramebuffer scopedFrameBuffer(mShadowBuffer);
			const gl::ScopedViewport scopedViewport(ivec2(0), mShadowBuffer->getSize());
			const gl::ScopedMatrices scopedMatrices;
			gl::enableDepthRead(true);
			gl::enableDepthWrite(true);
			gl::clear();
			mShadowCam->setMatrices();
			shadowFunc();
		}

		// ========== LIGHTING STAGE ============= //


		{
			const gl::ScopedFramebuffer scopedFrameBuffer(mLBuffer[ping]);
			const gl::ScopedViewport scopedViewport2(ivec2(0), mLBuffer[ping]->getSize());
			gl::clear();
			const gl::ScopedMatrices scopedMatrices2;
			const gl::ScopedBlendAdditive scopedAdditiveBlend;
			const gl::ScopedFaceCulling scopedFaceCulling(true, GL_FRONT);

			gl::enableDepthRead();
			gl::disableDepthWrite();

			mCam->setMatrices();

			// Bind G-buffer textures and shadow map
			gl::ScopedTextureBind scopedTextureBind0(mTextureFboGBuffer[0], 0);
			gl::ScopedTextureBind scopedTextureBind1(mTextureFboGBuffer[1], 1);
			gl::ScopedTextureBind scopedTextureBind2(mTextureFboGBuffer[2], 2);
			
			if (enableShadows) {
				gl::ScopedTextureBind scopedTextureBind3(mShadowBuffer->getDepthTexture(), 3);
			}

			for (Light& light : mLights) {

				// if light has custom mesh, use that instead of default cube shape.
				if (light.hasMesh()) {
					mBatchLBufferCube = light.getBatch();
				}

				// Draw light volumes
				mBatchLBufferCube->getGlslProg()->uniform("uSamplerAlbedo", 0);
				mBatchLBufferCube->getGlslProg()->uniform("uSamplerNormalEmissive", 1);
				mBatchLBufferCube->getGlslProg()->uniform("uSamplerPosition", 2);

				if (enableShadows) {
					mBatchLBufferCube->getGlslProg()->uniform("uShadowEnabled", enableShadows);
					mBatchLBufferCube->getGlslProg()->uniform("uShadowMatrix",
						mShadowCam->getProjection() * mShadowCam->getView());
					mBatchLBufferCube->getGlslProg()->uniform("uViewMatrixInverse", projMatrixInverse);

				}



				mBatchLBufferCube->getGlslProg()->uniform("uLightColorAmbient", light.getColorAmbient());
				mBatchLBufferCube->getGlslProg()->uniform("uLightColorDiffuse", light.getColorDiffuse());
				mBatchLBufferCube->getGlslProg()->uniform("uLightColorSpecular", light.getColorSpecular());
				mBatchLBufferCube->getGlslProg()->uniform("uLightPosition",
					vec3((mCam->getView() * vec4(light.getPosition(), 1.0))));
				mBatchLBufferCube->getGlslProg()->uniform("uLightIntensity", light.getIntensity());
				mBatchLBufferCube->getGlslProg()->uniform("uLightRadius", light.getVolume());
				mBatchLBufferCube->getGlslProg()->uniform("uWindowSize", vec2(getWindowSize()));

				gl::ScopedModelMatrix scopedModelMatrix;
				gl::translate(light.getPosition());
				gl::scale(vec3(light.getVolume()));
				mBatchLBufferCube->draw();
			}
		}


		// =============== LIGHT ACCUMULATION ================= //
		const gl::ScopedFramebuffer scopedFrameBuffer(mFboAccum);
		gl::drawBuffer(GL_COLOR_ATTACHMENT0);

		const gl::ScopedViewport scopedViewport(ivec2(0), mFboAccum->getSize());
		const gl::ScopedMatrices scopedMatrices;
		gl::setMatricesWindow(mFboAccum->getSize());
		gl::disableDepthRead();
		gl::disableDepthWrite();
		gl::translate(mFboAccum->getSize() / 2);
		gl::scale(mFboAccum->getSize());

		// Dim the light accumulation buffer to produce trails. Lower alpha 
		// makes longer trails.
		{

			const gl::ScopedBlendAlpha scopedBlendAlpha;
			const gl::ScopedColor scopedColor(ColorAf(Colorf::black(), mTrailAlpha));
			mBatchColorRect->draw();
		}

		gl::ScopedBlendAdditive blend;
		gl::ScopedTextureBind lightTex(mLBuffer[ping]->getColorTexture(), 0);
		mBatchEmissiveRect->draw();

		std::swap(ping, pong);
	}


	void DeferredScene::addLight(Light light) {
		auto shader = DeferredRenderer::getSimpleLightingFormat();
		light.setupMesh(shader);
		mLights.push_back(light);

	}

	void DeferredScene::addLight(LightRef light) {
		auto shader = DeferredRenderer::getSimpleLightingFormat();
		light->setupMesh(shader);
		mLights.push_back(*light.get());
	}


	void DeferredScene::resize() {
		buildBuffers();
	}


	void DeferredScene::buildBuffers() {

		mResolution = app::getWindowSize();

		auto w = mResolution.x;
		auto h = mResolution.y;

		// Color texture format
		const gl::Texture2d::Format colorTextureFormat = gl::Texture2d::Format()
			.internalFormat(GL_RGBA8)
			.magFilter(GL_NEAREST)
			.minFilter(GL_NEAREST)
			.wrap(GL_CLAMP_TO_EDGE);

		// Data texture format
		const gl::Texture2d::Format dataTextureFormat = gl::Texture2d::Format()
			.internalFormat(GL_RGBA16F)
			.magFilter(GL_NEAREST)
			.minFilter(GL_NEAREST)
			.wrap(GL_CLAMP_TO_EDGE);

		GLuint colorInternalFormat = GL_RGB10_A2;
		gl::Texture2d::Format colorTextureFormatLinear = gl::Texture2d::Format()
			.internalFormat(colorInternalFormat)
			.magFilter(GL_LINEAR)
			.minFilter(GL_LINEAR)
			.wrap(GL_CLAMP_TO_EDGE)
			.dataType(GL_FLOAT);

		// Create FBO for the the geometry buffer (G-buffer). This G-buffer uses
		// three color attachments to store position, normal, emissive (light), and
		// color (albedo) data.

		try {
			gl::Fbo::Format fboFormat;
			mTextureFboGBuffer[0] = gl::Texture2d::create(w, h, colorTextureFormat);
			mTextureFboGBuffer[1] = gl::Texture2d::create(w, h, dataTextureFormat);
			mTextureFboGBuffer[2] = gl::Texture2d::create(w, h, dataTextureFormat);
			for (size_t i = 0; i < 3; ++i) {
				fboFormat.attachment(GL_COLOR_ATTACHMENT0 + (GLenum)i, mTextureFboGBuffer[i]);
			}
			fboFormat.depthTexture();
			mGBuffer = gl::Fbo::create(w, h, fboFormat);
			const gl::ScopedFramebuffer scopedFramebuffer(mGBuffer);
			const gl::ScopedViewport scopedViewport(ivec2(0), mGBuffer->getSize());
			gl::clear();
		}
		catch (const std::exception& e) {
			CI_LOG_E("DeferredScene initialization failed" << "mFboGBuffer failed: " << e.what());
		}

		// Create FBO for the light buffer (L-buffer). The L-buffer reads the
		// G-buffer textures to render the scene inside light volumes.
		try {
			for (int i = 0; i < 2; ++i) {
				mLBuffer[i] = gl::Fbo::create(w, h, gl::Fbo::Format().colorTexture());
				const gl::ScopedFramebuffer scopedFramebuffer(mLBuffer[i]);
				const gl::ScopedViewport scopedViewport(ivec2(0), mLBuffer[i]->getSize());
				gl::clear();
			}
		}
		catch (const std::exception& e) {
			CI_LOG_E("DeferredScene initialization failed" << "mLBuffer failed: " << e.what());
		}

		// Create shadow map buffer
		{
			const GLsizei sz = 2048;
			mShadowBuffer = gl::Fbo::create(sz, sz, gl::Fbo::Format().depthTexture());
			const gl::ScopedFramebuffer scopedFramebuffer(mShadowBuffer);
			const gl::ScopedViewport scopedViewport(ivec2(0), mShadowBuffer->getSize());
			gl::clear();
			mShadowBuffer->getDepthTexture()->setCompareMode(GL_COMPARE_REF_TO_TEXTURE);
		}

		// Light accumulation frame buffer
		// 0 GL_COLOR_ATTACHMENT0 Light accumulation
		{
		
			gl::Fbo::Format fboFormat;
			fboFormat.disableDepth();
			for (size_t i = 0; i < 3; ++i) {
				mTextureFboAccum[i] = gl::Texture2d::create(w,h, colorTextureFormatLinear);
				fboFormat.attachment(GL_COLOR_ATTACHMENT0 + (GLenum)i, mTextureFboAccum[i]);
			}
			mFboAccum = gl::Fbo::create(w,h, fboFormat);
			const gl::ScopedFramebuffer scopedFramebuffer(mFboAccum);
			const gl::ScopedViewport scopedViewport(ivec2(0), mFboAccum->getSize());
			gl::clear();
		}

	}

}