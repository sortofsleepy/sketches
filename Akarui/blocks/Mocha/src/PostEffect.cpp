#include "mocha/PostEffect.h"

using namespace std;
using namespace ci;

namespace mocha {

	PostEffect::PostEffect(bool mMultiPass):mMultiPass(mMultiPass),maxLogCount(1) {

		gl::Texture::Format tfmt;
		tfmt.setInternalFormat(GL_RGBA32F);

		gl::Fbo::Format fmt;
		fmt.setColorTextureFormat(tfmt);

		// add depth texture
		gl::Texture::Format depthFormat;
		depthFormat.setInternalFormat(GL_DEPTH_COMPONENT32F);
		depthFormat.setCompareMode(GL_COMPARE_REF_TO_TEXTURE);
		depthFormat.setMagFilter(GL_LINEAR);
		depthFormat.setMinFilter(GL_LINEAR);
		depthFormat.setWrap(GL_CLAMP_TO_EDGE, GL_CLAMP_TO_EDGE);


		gl::TextureRef depth = gl::Texture::create(app::getWindowWidth(), app::getWindowHeight(), depthFormat);

		fmt.attachment(GL_DEPTH_ATTACHMENT, depth);

		mFbos[0] = gl::Fbo::create(app::getWindowWidth(), app::getWindowHeight(),fmt);
		

		if(mMultiPass){
			mFbos[1] = gl::Fbo::create(app::getWindowWidth(), app::getWindowHeight(), fmt);
		}

	}


	

	void PostEffect::setup(ci::gl::Fbo::Format fmt)
	{
		mFbos[0] = gl::Fbo::create(app::getWindowWidth(), app::getWindowHeight(), fmt);
		mFbos[1] = gl::Fbo::create(app::getWindowWidth(), app::getWindowHeight(), fmt);

	}

	void PostEffect::setup(ci::gl::Texture::Format tfmt) {

		gl::Fbo::Format fmt;
		fmt.setColorTextureFormat(tfmt);

		mFbos[0] = gl::Fbo::create(app::getWindowWidth(), app::getWindowHeight(), fmt);
		mFbos[1] = gl::Fbo::create(app::getWindowWidth(), app::getWindowHeight(), fmt);
	}

	void PostEffect::setShader(std::string path)
	{
		gl::GlslProg::Format fmt;
		fmt.vertex(vertexShader);
		fmt.fragment(app::loadAsset(path));
		

		try {
			mShader = gl::GlslProg::create(fmt);
		}
		catch (ci::gl::GlslProgCompileExc &e) {
			CI_LOG_E(e.what());
		}
	}
	
	void PostEffect::setInputTexture(ci::gl::TextureRef mInput) {
		this->mInput = mInput;
	}

	void PostEffect::bind() {

		if (mMultiPass) {
			// bind framebuffer and clear
			mFbos[current]->bindFramebuffer();
			gl::clear(ColorA(0, 0, 0, 0));

		}
		
	}

	void PostEffect::unbind() {
		if (mMultiPass) {
			mFbos[current]->unbindFramebuffer();
			std::swap(current, target);
		}
	}

	void PostEffect::applyEffect(std::function<void(ci::gl::GlslProgRef)> func) {
		
		//TODO does this need to be released?
		gl::TextureRef tex;
		
		gl::ScopedFramebuffer fbo(mFbos[current]);
		
		if (mMultiPass) {
			tex = mFbos[target]->getColorTexture();
		}
		else {
			tex = mInput;
		}

		gl::ScopedTextureBind tex0(tex, 0);

		gl::clear(ColorA(0, 0, 0, 0));

		// bind shader
		mShader->bind();

		if (func != nullptr) {
			func(mShader);
		}
	
		// set default uniforms
		mShader->uniform("uTex0", 0);
		mShader->uniform("resolution", resolution);

		gl::setMatricesWindow(resolution);
		gl::viewport(resolution);

		gl::drawSolidRect(Rectf(0, 0,resolution.x,resolution.y));

		
		if (mMultiPass) {
			std::swap(current, target);
		}
	}

	ci::gl::TextureRef PostEffect::getOutput() {
		if (mMultiPass) {
			return mFbos[target]->getColorTexture();
		}
		else {
			return mFbos[0]->getColorTexture();
		}
	}

	void PostEffect::drawOutput() {
		gl::setMatricesWindow(app::getWindowSize());
		gl::viewport(app::getWindowSize());
		gl::draw(getOutput(), Rectf(0, 0, app::getWindowWidth(), app::getWindowHeight()));
	}
}