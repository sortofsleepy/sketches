//
//  TFBuffer.hpp
//  VolParticles
//
//  Created by Joseph Chow on 7/14/17.
//
//

#ifndef TFBuffer_hpp
#define TFBuffer_hpp

#include "cinder/gl/Vao.h"
#include "cinder/gl/Vbo.h"
#include "cinder/gl/GlslProg.h"
#include "cinder/gl/BufferTexture.h"
#include <vector>
#include <string>
#include "cinder/app/App.h"
#include "cinder/gl/gl.h"
#include "cinder/Log.h"

/**

A simple TransformFeedback class for using transform feedback to manipulate data.

Relies on using seperate attributes vs interleaved at the moment.

*/
namespace mocha {
	namespace GPU {

		typedef std::shared_ptr<class TFBuffer> TFBufferRef;
		class TFBuffer {

			// variables to help with swapping
			uint32_t mIterationsPerFrame;

			// number of points to do feedback on
			int numPoints;

			// the mode to do feedback on
			GLenum mode;

			//! The type of feedback transform being done
			GLenum feedbackType;

			// flag to indicate whether or not the update shader has been set.
			bool updateShaderSet;

			std::uint32_t	mSourceIndex = 0;
			std::uint32_t	mDestinationIndex = 1;
		public:

			static TFBufferRef create() {
				return TFBufferRef(new TFBuffer());
			}

			// keeps track of all the attributes
			std::vector<std::array<ci::gl::VboRef, 2> > mVbos;

			ci::gl::VaoRef mVaos[2];
			ci::gl::GlslProgRef mUpdateShader;
			TFBuffer();

			void setNumPoints(int numPoints);
			ci::gl::VaoRef getVao() {

				return  mVaos[mDestinationIndex];
			}

			//! Return one of the vbos of data. Pass in the attribute index, and optionally, the vbo of that 
			//! attribute you want.
			ci::gl::VboRef getVbo(int attribIndex = 0, int index = 0) {
				return mVbos[attribIndex][index];
			}
			// set the number of iterations the transform feedback process goes through each frame.
			// in general, 1(the default) should be fine.
			void setIterationsPerFrame(int mIterationsPerFrame=1);

			// returns the VAO at the specified index
			ci::gl::VaoRef getVao(int index) {
				return mVaos[index];
			}

			// sets a uniform on the update shader.
			template<typename T>
			void uniform(std::string name, T value) {
				if (updateShaderSet) {
					mUpdateShader->uniform(name, value);
				}
			}

			// set the transform feedback mode. GL_POINTS is the default but can also be GL_TRIANGLES and
			// 
			void setFeedbackMode(GLenum mode);

			//! Updates your VBOs via transform feedback. Mostly automatic. 
			//! Accepts a lamda that gives you a reference to the update shader. so you can easily update shader uniforms. 
			void update(std::function<void(ci::gl::GlslProgRef)> fn = nullptr);


			//! alternate update method. Pass in a lambda if you need a bit more control of the process.
			//! Callback receives the current buffer index we're on (either 0 or 1)
			//void update(std::function<void(int idx)> fn);

			//! Returns the set of Vbos associated with an attribute. There are no names so you'll have to pass in the
			//! index of the set.
			std::array<ci::gl::VboRef, 2> getAttributeSet(int index = 0);


			//! returns the Transform feedback update shader.
			ci::gl::GlslProgRef getShader() {
				return mUpdateShader;
			}


			//! loads a shader for transform feedback
			//! Pass the string pathname to the shader and a vector of the varyings needed.
			void loadShader(std::string updatepath, std::vector<std::string> feedbackVaryings, GLenum feedbackType = GL_SEPARATE_ATTRIBS, std::function<ci::gl::GlslProg::Format(ci::gl::GlslProg::Format)> fn = nullptr);


			// buffers layout onto vaos.
			void vertexAttribPointer(int bufferIndex, int size, GLenum data_type = GL_FLOAT, GLboolean normalized = GL_FALSE, int stride = 0, const GLvoid * pointer = (const GLvoid*)0);

			void vertexAttribPointerInterleaved(std::function<void()> fn);

			//! Adds an attribute onto the stack of data you want to manipulate with transform feedback.
			template<typename T>
			void bufferAttrib(std::vector<T> &data) {
				auto vbo = ci::gl::Vbo::create(GL_ARRAY_BUFFER, sizeof(T) * data.size(), data.data(), GL_DYNAMIC_DRAW);

				auto vbo2 = ci::gl::Vbo::create(GL_ARRAY_BUFFER, sizeof(T) * data.size(), data.data(), GL_DYNAMIC_DRAW);

				// push back a new VBO set
				mVbos.push_back({ vbo,vbo2 });

			}


			//! Same as the above but accepts an std::array instead.
			template<typename T, size_t SIZE>
			void bufferAttrib(std::array<T, SIZE> &data) {

				auto vbo = ci::gl::Vbo::create(GL_ARRAY_BUFFER, sizeof(T) * data.size(), data.data(), GL_STATIC_DRAW);
				auto vbo2 = ci::gl::Vbo::create(GL_ARRAY_BUFFER, sizeof(T) * data.size(), data.data(), GL_STATIC_DRAW);


				// push back a new VBO set
				mVbos.push_back({ vbo,vbo2 });
			}
		};
	}
}
#endif /* TFBuffer_hpp */
