#pragma once


#include "cinder/gl/Fbo.h"
#include "cinder/gl/gl.h"
#include "cinder/app/App.h"
#include "cinder/Log.h"
using namespace ci;
using namespace std;

#define STRINGIFY(A) #A

namespace mocha {
	class PingpongBuffer {

		gl::FboRef mFbos[2];
	
		int width, height;

		int current, target;

		ci::gl::GlslProgRef mShader;
		bool shaderSet;
		//! Default passthru shader
		std::string vertexShader = STRINGIFY(
			uniform mat4 ciModelViewProjection;
			in vec4 ciPosition;
			in vec2 ciTexCoord0;
			out vec2 uv;
			out vec2 vUv;
			const vec2 scale = vec2(0.5, 0.5);

			void main() {
				uv = ciPosition.xy * scale + scale;
				vUv = ciTexCoord0;
				gl_Position = ciModelViewProjection * ciPosition;
			}
		);
	public:
		PingpongBuffer(int width=512,int height=512);
		void setShader(std::string path);
		void setupBuffers();
		void updateBuffers();
	};
	
}