#pragma once

#include "cinder/Log.h"
#include "cinder/Vector.h"

using namespace ci;
using namespace std;

#define STRINGIFY(A) #A

/*
	Holds simple shaders for various uses.

*/

namespace mocha {
	namespace shaders {
		//================= UTILS =================== //

		// passthru shader
		const std::string passthru = STRINGIFY(
			uniform mat4 ciModelViewProjection;

			in vec4 	ciPosition;
			in vec2 	ciTexCoord0;

			out vec2    uv;

			void main(void)
			{
				uv = ciTexCoord0;
				gl_Position = ciModelViewProjection * ciPosition;
			}
		);

		//================= POST PROCESSING =================== //


		namespace BasicPost {
			const std::string fxaa = STRINGIFY(

				precision highp float;
				precision highp sampler2DShadow;


				uniform vec2		uPixel;
				uniform sampler2D	uSampler;

				in vec2     uv;
				out vec4 	oColor;

				void main(void)
				{
					float fxaaSpanMax = 8.0;
					float fxaaReduceMul = 1.0 / fxaaSpanMax;
					float fxaaReduceMin = 1.0 / 128.0;

					vec3 rgbUL = texture(uSampler, uv + vec2(-1.0, -1.0) * uPixel).xyz;
					vec3 rgbUR = texture(uSampler, uv + vec2(1.0, -1.0) * uPixel).xyz;
					vec3 rgbBL = texture(uSampler, uv + vec2(-1.0, 1.0) * uPixel).xyz;
					vec3 rgbBR = texture(uSampler, uv + vec2(1.0, 1.0) * uPixel).xyz;
					vec3 rgbM = texture(uSampler, uv).xyz;

					vec3 luma = vec3(0.299, 0.587, 0.114);
					float lumaUL = dot(rgbUL, luma);
					float lumaUR = dot(rgbUR, luma);
					float lumaBL = dot(rgbBL, luma);
					float lumaBR = dot(rgbBR, luma);
					float lumaM = dot(rgbM, luma);

					float lumaMin = min(lumaM, min(min(lumaUL, lumaUR), min(lumaBL, lumaBR)));
					float lumaMax = max(lumaM, max(max(lumaUL, lumaUR), max(lumaBL, lumaBR)));

					vec2 dir = vec2(-((lumaUL + lumaUR) - (lumaBL + lumaBR)), (lumaUL + lumaBL) - (lumaUR + lumaBR));

					float dirReduce = max((lumaUL + lumaUR + lumaBL + lumaBR) * (0.25 * fxaaReduceMul), fxaaReduceMin);
					float rcpDirMin = 1.0 / (min(abs(dir.x), abs(dir.y)) + dirReduce);

					dir = min(vec2(fxaaSpanMax, fxaaSpanMax),
						max(vec2(-fxaaSpanMax, -fxaaSpanMax),
							dir * rcpDirMin)) * uPixel;

					vec3 color0 = 0.5 * (
						texture(uSampler, uv + dir * (1.0 / 3.0 - 0.5)).rgb +
						texture(uSampler, uv + dir * (2.0 / 3.0 - 0.5)).rgb);
					vec3 color1 = color0 * 0.5 + 0.25 * (
						texture(uSampler, uv + dir * -0.5).rgb +
						texture(uSampler, uv + dir * 0.5).rgb);

					float lumaB = dot(color1, luma);

					vec4 color = vec4(color1, 1.0);
					if (lumaB < lumaMin || lumaB > lumaMax) {
						color.rgb = color0;
					}
					oColor = color;
				}
			);
		}

		//================= DEFERED RENDERING =================== //

		namespace DeferredRenderer {

			const std::string emissive = STRINGIFY(

			

				in vec2 uv;
				uniform sampler2D uSamplerAlbedo;

				out vec4 oColor;
				void main() {
			
					oColor = texture(uSamplerAlbedo, uv);

					oColor.a = 0.5;
				}

			);
			
			const std::string lBufferSimpleFragment = STRINGIFY(
				precision highp float;
				precision highp sampler2DShadow;

				uniform sampler2D		uSamplerAlbedo;
				uniform sampler2D		uSamplerNormalEmissive;
				uniform sampler2D		uSamplerPosition;
				uniform sampler2DShadow uSamplerShadowMap;

				uniform vec4	uLightColorAmbient;
				uniform vec4	uLightColorDiffuse;
				uniform vec4	uLightColorSpecular;
				uniform float	uLightIntensity;
				uniform vec3	uLightPosition;
				uniform float	uLightRadius;

				uniform bool	uShadowEnabled;
				uniform mat4 	uShadowMatrix;

				uniform mat4 	uViewMatrixInverse;
				uniform vec2	uWindowSize;
	
				in vec2     uv;
				out vec4 	oColor;

				void main(void)
				{
					vec2 uv = gl_FragCoord.xy / uWindowSize;
					vec4 position = texture(uSamplerPosition, uv);

					// Do not draw background
					if (length(position.xyz) <= 0.0) {
						discard;
					}

					vec3 L = uLightPosition - position.xyz;
					float d = length(L);

					// Only draw fragment if it occurs inside the light volume
					if (d > uLightRadius) {
						discard;
					}
					L /= d;

					// Calculate lighting
					vec4 normalEmissive = texture(uSamplerNormalEmissive, uv);
					vec3 N = normalize(normalEmissive.xyz);
					vec3 V = normalize(position.xyz);
					vec3 H = normalize(L + V);
					float NdotL = max(0.0, dot(N, L));
					float HdotN = max(0.0, dot(H, N));
					float Ks = pow(HdotN, 100.0);
					float att = 1.0 / pow((d / (1.0 - pow(d / uLightRadius, 2.0))) / uLightRadius + 1.0, 2.0);

					// Calculate color
					vec4 Ia = uLightColorAmbient;
					vec4 Id = NdotL * uLightColorDiffuse * texture(uSamplerAlbedo, uv);
					vec4 Is = Ks * uLightColorSpecular;
					vec4 Ie = vec4(vec3(normalEmissive.w), 1.0);

					oColor = Ia + att * (Id + Is);

					oColor *= uLightIntensity;

					// Calculate shadow
					if (uShadowEnabled) {
						const float bias = 0.9986;
						vec4 shadowClip = uShadowMatrix * uViewMatrixInverse * position;
						vec3 shadowCoord = shadowClip.xyz / shadowClip.w;
						shadowCoord = shadowCoord * 0.5 + 0.5;

						float s = 0.0;
						if (shadowCoord.x >= 0.0 && shadowCoord.x <= 1.0 && shadowCoord.y >= 0.0 && shadowCoord.y <= 1.0) {
							float depth = texture(uSamplerShadowMap, shadowCoord);
							if (depth < shadowCoord.z - bias) {
								oColor *= 0.2;
							}
						}
					}

					// Add emissive
					oColor += Ie;
					oColor.a = 1.0;
				}

			);
			/*
			oColor = Ia + att * (Id + Is);

				oColor *= uLightIntensity;

					// Calculate shadow
					if (uShadowEnabled) {
						const float bias = 0.9986;
						vec4 shadowClip = uShadowMatrix * uViewMatrixInverse * position;
						vec3 shadowCoord = shadowClip.xyz / shadowClip.w;
						shadowCoord = shadowCoord * 0.5 + 0.5;

						float s = 0.0;
						if (shadowCoord.x >= 0.0 && shadowCoord.x <= 1.0 && shadowCoord.y >= 0.0 && shadowCoord.y <= 1.0) {
							float depth = texture(uSamplerShadowMap, shadowCoord);
							if (depth < shadowCoord.z - bias) {
								oColor *= 0.2;
							}
						}
					}

					// Add emissive
					oColor += Ie;
					oColor.a = 1.0;
			
			*/

			// =============== GEOMETRY BUFFER ================================ //

			//! For use when you want to compose a simple geometry buffer vertex shader.
			//! Includes core variables and uniforms. Assuming you'll fill in the rest in terms
			//! of calculating the vertex position using the varying name of "position"
			static std::string gBufferSimpleCoreVertexVariables = STRINGIFY(

				precision highp float;
				precision highp sampler2DShadow;
			
				uniform mat4 	ciModelView;
				uniform mat3 	ciNormalMatrix;
				uniform mat4 	ciModelViewProjection;

				in vec4 		ciColor;
				in vec3 		ciNormal;
				in vec4 		ciPosition;

				out vec4 		color;
				out vec3 		normal;
				

			);

			//! For use when you want to compose a simple geometry buffer vertex shader.
			//! Includes all of the typical varyings declarations, assuming you'll fill in the rest in terms
			//! of calculating the vertex position using the varying name of "position"
			static std::string gBufferSimpleCoreVaryingsPass = STRINGIFY(

				color = ciColor;
				vec3 n = ciNormal;
				normal = ciNormalMatrix * n;
			);

			//! a basic geometry buffer vertex shader. Same thing as the one in Cinder's deferred renderer example.
			static std::string gBufferSimpleVertex = STRINGIFY(
				uniform mat4 	ciModelView;
				uniform mat3 	ciNormalMatrix;
				uniform mat4 	ciModelViewProjection;

				in vec4 		ciColor;
				in vec3 		ciNormal;
				in vec4 		ciPosition;

				out vec4 		color;
				out vec3 		normal;
				out vec4 		position;

				void main()
				{
					color = ciColor;
					position = ciModelView * ciPosition;
					vec3 n = ciNormal;
					normal = ciNormalMatrix * n;

					gl_Position = ciModelViewProjection * ciPosition;
				}

			);

			//! a basic geometry buffer fragment shader. Same thing as the one in Cinder's deferred renderer example.
			static std::string gBufferSimpleFragment = STRINGIFY(

				precision highp float;
				precision highp sampler2DShadow;


				uniform float uEmissive;

				in vec4 color;
				in vec3 normal;
				in vec4 position;

				layout(location = 0) out vec4 oAlbedo;
				layout(location = 1) out vec4 oNormalEmissive;
				layout(location = 2) out vec4 oPosition;

				void main(void)
				{
					oAlbedo = color;
					oNormalEmissive = vec4(normalize(normal), uEmissive);
					oPosition = position;
				}

			);


				///// =============== FUNCTIONS ======================= //

				static ci::gl::GlslProgRef getSimpleLightingShader() {
					gl::GlslProg::Format fmt;
					fmt.vertex(passthru);
					fmt.fragment(lBufferSimpleFragment);

					gl::GlslProgRef shader;
					try {
						shader = gl::GlslProg::create(fmt);
					}
					catch (gl::GlslProgCompileExc &e) {

						CI_LOG_E("Issue loading simple lighting shader" << e.what());
					}

					return shader;
				}

				static ci::gl::GlslProg::Format getSimpleLightingFormat() {
					gl::GlslProg::Format fmt;
					fmt.vertex(passthru);
					fmt.fragment(lBufferSimpleFragment);

					return fmt;

				}

				static gl::GlslProgRef getSimpleGBufferShader(int version=330) {
					gl::GlslProg::Format fmt;
					fmt.vertex(gBufferSimpleVertex);
					fmt.fragment(gBufferSimpleFragment);
					fmt.version(330);

					gl::GlslProgRef shader;
					try {
						shader = gl::GlslProg::create(fmt);
					}
					catch (gl::GlslProgCompileExc &e) {
						
						CI_LOG_E("Issue loading simple geometry shader" << e.what());
					}
					return shader;
				}

		}
	}
}