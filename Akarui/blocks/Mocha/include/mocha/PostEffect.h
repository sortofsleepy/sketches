#pragma once

#include "cinder/gl/Fbo.h"
#include "cinder/gl/GlslProg.h"
#include "cinder/app/App.h"
#include "cinder/gl/gl.h"
#include "cinder/Log.h"

#include <memory>
#define STRINGIFY(A) #A


namespace mocha {

	typedef std::shared_ptr<class PostEffect> PostEffectRef;

	class PostEffect {


	protected:
		ci::gl::FboRef mFbos[2];
		ci::gl::GlslProgRef mShader;

		int current = 0;
		int target = 1;

		bool mMultiPass;

		ci::gl::TextureRef mInput;

		ci::vec2 resolution = ci::app::getWindowSize();

		//! Default passthru shader
		std::string vertexShader = STRINGIFY(
			uniform mat4 ciModelViewProjection;
			in vec4 ciPosition;
			in vec2 ciTexCoord0;
			out vec2 vUv;
			const vec2 scale = vec2(0.5, 0.5);

			void main() {
				//uv = ciPosition.xy * scale + scale;
				vUv = ciTexCoord0;
				gl_Position = ciModelViewProjection * ciPosition;
			}
		);


		//! Flag used to keep track of logging counts in repeating functions.
		int maxLogCount;

		void setup(ci::gl::Fbo::Format fmt);
		void setup(ci::gl::Texture::Format tfmt);
	public:
		PostEffect(bool mMultiPass);

		static PostEffectRef create(bool mMultiPass = false) {
			return PostEffectRef(new PostEffect(mMultiPass));
		}

		static PostEffectRef create(ci::gl::Fbo::Format fmt, bool mMultiPass = true) {
			auto p = new PostEffect(mMultiPass);
			p->setup(fmt);

			return PostEffectRef(p);
		}

		static PostEffectRef create(ci::gl::Texture::Format tfmt, bool mMultiPass = true) {
			auto p = new PostEffect(mMultiPass);
			p->setup(tfmt);

			return PostEffectRef(p);

		}

		void setShader(std::string path);
		void setInputTexture(ci::gl::TextureRef mInput);
		void bind();
		void unbind();
		void applyEffect(std::function<void(ci::gl::GlslProgRef)> func=nullptr);

		ci::gl::TextureRef getOutput();

		void drawOutput();

		template<typename T>
		void uniform(std::string name, T value) {
			mShader->uniform(name, value);
		}
	};
}

