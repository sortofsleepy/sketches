#pragma once

#include "mocha/PostEffect.h"
#include <functional>
#include <vector>
	
namespace mocha {


	typedef std::shared_ptr<class PostEffectComposer>PostComposerRef;

	class PostEffectComposer {

		std::vector<PostEffectRef> mEffects;

		ci::gl::TextureRef mInput;
	public:
		PostEffectComposer() {}
	
		void addEffect(PostEffectRef effect) {
			mEffects.push_back(effect);
		}

		void process(ci::gl::TextureRef mInput) {

			for (int i = 0; i < mEffects.size(); ++i) {
				
				// if we're not on the first layer, take the previous layer as the 
				// input for the effect.
				if (i != 0) {
					mEffects[i]->setInputTexture(mEffects[i - 1]->getOutput());
				}
				else {
					mEffects[0]->setInputTexture(mInput);
				}
			}

		}

		//! Update all effects. 
		void update() {
			for (mocha::PostEffectRef &effect : mEffects) {
				effect->applyEffect();
			}
		}

		//! Return the output from the whole process.
		ci::gl::TextureRef getOutput() {
			auto last = mEffects.size() - 1;
			return mEffects[last]->getOutput();
		}
	};
}