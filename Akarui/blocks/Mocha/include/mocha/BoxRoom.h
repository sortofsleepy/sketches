#ifndef BoxRoom_hpp
#define BoxRoom_hpp

#include "EasyCam.h"
#include "core.h"
#include "cinder/gl/VboMesh.h"
#include "cinder/gl/Batch.h"
#include "cinder/gl/GlslProg.h"
#include "Light.hpp"
#include <memory>

// largely based on Robert Hodgin's Eyeo 2012 code.
// http://roberthodgin.com/portfolio/work/eyeo-2012/


typedef std::shared_ptr<class BoxRoom> BoxRoomRef;
class BoxRoom {

	ci::gl::VboMeshRef mMesh, mWireMesh;
	ci::gl::GlslProgRef mShader, mWireShader;
	ci::gl::BatchRef mBatch, mWireBatch;
	ci::vec3 mBoxSize;

	ci::gl::VboMeshRef mLightMesh;
	ci::gl::GlslProgRef mLightShader;
	ci::gl::BatchRef mLightBatch;

	float scaleVal;
	float mTime;
	float mTimeElapsed;
	float mTimeMulti;
	float mTimeAdjusted;
	float mTimer;
	bool mTick;

	bool isPowerOn;
	float mPower;

	mocha::DeferredRendering::LightRef mOverheadLight;
public:
	BoxRoom();

	static BoxRoomRef create() {
		return BoxRoomRef(new BoxRoom());
	}

	ci::vec3 getLightPosition();
	ci::ColorA getLightColor();

	mocha::DeferredRendering::LightRef getLight() {
		return mOverheadLight;
	}
	void drawOverheadLight();
	void setupControls();
	void togglePower();
	float getPower();
	float getLightPower();
	float getTimePer();
	float getEnvTime();
	void setup();
	void updateTime(bool saveFrame = false);
	void draw(EasyCamRef mCam);
};

#endif