#pragma once


#include "cinder/gl/Fbo.h"
#include "cinder/gl/Texture.h"
#include "cinder/gl/GlslProg.h"
#include <vector>
#include "cinder/app/App.h"
#include "cinder/gl/gl.h"
#include <memory>

#define STRINGIFY(A) #A

namespace mocha {
	typedef std::shared_ptr<class Compositor> CompositorRef;

	class Compositor {

		//! Storage for all the textures that need to be composited. 
		std::vector<ci::gl::TextureRef> mTextures;

		//! Compositing shader
		ci::gl::GlslProgRef mShader;

		//! Rendering shader
		ci::gl::GlslProgRef mRenderShader;

		//! FBO to composite things together;
		ci::gl::FboRef mFbo;

		std::string vertex = STRINGIFY(
			uniform mat4 ciModelViewProjection;
			in vec4 ciPosition;
			in vec2 ciTexCoord0;
			out vec2 vUv;
			const vec2 scale = vec2(0.5, 0.5);

			void main() {
				vUv = ciTexCoord0;
				gl_Position = ciModelViewProjection * ciPosition;
			}
		);

		std::string fragment = STRINGIFY(
			in vec2 vUv;
			out vec4 glFragColor;

			void main() {

				vec4 finalColor = vec4(0.);

				for (int i = 0; i < numTextures; ++i) {
					vec4 tex = texture(uTextures[i], vUv);
					finalColor = mix(finalColor, tex, tex.a);
				}

				//glFragColor = finalColor;
				texture(uTextures[0], vUv);

			}
		);



	public:
		Compositor();

		//! alternate constructor - accepts array of textures to composite
		Compositor(std::vector<ci::gl::TextureRef> mTextures);

		static CompositorRef create() {
			return CompositorRef(new Compositor());
		}

		static CompositorRef create(std::vector<ci::gl::TextureRef> mTextures) {
			return CompositorRef(new Compositor(mTextures));
		}

		//! Updates the compositor
		void update();

		//! compiles the shader to use for the compositor. this MUST be run prior to trying to call 
		//! update
		void compile();

		//! adds a new texture to composite
		void addTexture(ci::gl::TextureRef mTex);

		//! renders the output from the compositor
		void drawOutput();

		//! Gets the output from the compositor
		ci::gl::TextureRef getOutput() {
			return mFbo->getColorTexture();
		}
	};


}