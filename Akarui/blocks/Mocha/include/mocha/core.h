#pragma once

#include <string>
#include "cinder/gl/GlslProg.h"
#include "cinder/gl/Texture.h"
#include "cinder/app/App.h"
#include "cinder/gl/VboMesh.h"
#include "cinder/gl/Batch.h"
#include "cinder/gl/GlslProg.h"

using namespace ci;
using namespace std;
using namespace gl;

namespace mocha {

	// wrapper around a mesh, shader, and a batch object since they're used so often.
	typedef struct {
		ci::gl::VboMeshRef mMesh;
		ci::gl::BatchRef mBatch;
		ci::gl::GlslProgRef mShader;
	}Object3D;

	static ci::gl::GlslProgRef loadShader(std::string vertex, std::string fragment, std::string geometry = "") {
		gl::GlslProg::Format fmt;
		fmt.vertex(app::loadAsset(vertex));
		fmt.fragment(app::loadAsset(fragment));

		if (geometry != "") {
			fmt.geometry(app::loadAsset(geometry));
		}

		return gl::GlslProg::create(fmt);
	}

	//! Builds a cubemap from a list of 6 images. Pass in the path to the images using the format, 
	//! <path + prefix>. Each image path  is assumed to follow the standard format of <name>_posx ... etc.
	//! <path + prefix> refers to everything before the name part, IE <path + prefix>_posx.png etc... 

	static ci::gl::TextureCubeMapRef buildCubemapFromImages(std::string pathAndPrefix,gl::TextureCubeMap::Format fmt= gl::TextureCubeMap::Format()) {

		ci::ImageSourceRef imgs[6];

		imgs[0] = loadImage(app::loadAsset(pathAndPrefix + "_posx.png"));
		imgs[1] = loadImage(app::loadAsset(pathAndPrefix + "_negx.png"));
		imgs[2] = loadImage(app::loadAsset(pathAndPrefix + "_posy.png"));
		imgs[3] = loadImage(app::loadAsset(pathAndPrefix + "_negy.png"));
		imgs[4] = loadImage(app::loadAsset(pathAndPrefix + "_posz.png"));
		imgs[5] = loadImage(app::loadAsset(pathAndPrefix + "_negz.png"));

		gl::TextureCubeMapRef mCube = gl::TextureCubeMap::create(imgs,fmt);

		return mCube;
	}


	//! Helper function to build a texture format object for handling raw data.
	static ci::gl::Texture::Format getStandardDataTextureFormat(){
		gl::Texture::Format tfmt;
		tfmt.setInternalFormat(GL_RGBA32F);
		tfmt.setWrap(GL_CLAMP_TO_EDGE, GL_CLAMP_TO_EDGE);
		tfmt.setMinFilter(GL_NEAREST);
		tfmt.setMagFilter(GL_NEAREST);

		return tfmt;
	}
}