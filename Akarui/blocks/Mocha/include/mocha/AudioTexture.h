#pragma once

#include "cinder/gl/Texture.h"
#include "cinder/app/App.h"
#include <string>
#include "cinder/audio//Context.h"
#include "cinder/audio/audio.h"
#include "cinder/audio/Voice.h"
#include "cinder/audio/Source.h"
#include "cinder/gl/gl.h"
#include "cinder/gl/Vbo.h"
#include "cinder/Thread.h"
#include "cinder/gl/Context.h"
#include <memory>
#include "cinder/ConcurrentCircularBuffer.h"
typedef std::shared_ptr<class AudioTexture> AudioTextureRef;
class AudioTexture {

	bool isPlaying;

	//! reference to the main audio context.
	ci::audio::Context * mContext;

	//! player node to help facilitate playing of audio.
	ci::audio::BufferPlayerNodeRef mBufferPlayerNode;

	//! Monitor nodes for left and right channels. 
	ci::audio::MonitorSpectralNodeRef mMonitorLeft, mMonitorRight, mTestMonitor;
	ci::audio::ChannelRouterNodeRef splitRouterLeft, splitRouterRight;

	std::vector<float> mLeftMagSpectrum,mRightMagSpectrum;

	//! Gain thread to help pre-adjust audio levels.
	ci::audio::GainNodeRef mGain;

	//! Background thread to load audio in.
	std::shared_ptr<std::thread> mThread;

	//! Path to the audio file currently in memory.
	std::string filePath;

	//! reference to the current source file that we're loading.
	ci::audio::SourceFileRef sourceFile;

	// a flag, used to ensure only one audio file is loaded at a time.
	bool loadingAudio;

	//! indicates that an audio file has loaded into memory
	bool audioLoaded;

	//! Indicates whether or not the audio thread is currently looping.
	bool threadStop;

	//! Callback function to run once an audio file is loaded.
	std::function<void()> onLoad;

	double currentPlayhead;
public:
	AudioTexture(ci::audio::MonitorSpectralNode::Format fmt);

	static AudioTextureRef create(ci::audio::MonitorSpectralNode::Format fmt = ci::audio::MonitorSpectralNode::Format().fftSize(4048).windowSize(ci::app::getWindowWidth() * 3)) {
		return AudioTextureRef(new AudioTexture(fmt));
	}
	bool isPlayingAudio() {
		return isPlaying;
	}
	void setup();

	void play();
	void pause();
	//! Loads a new audio file into a texture.
	void loadAudio(std::string filePath);

	//! Stops audio processing and threading.
	void kill();

	//! threaded function to load audio.
	void loadAudioThread(ci::gl::ContextRef context);
};