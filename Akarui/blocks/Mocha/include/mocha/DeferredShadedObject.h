#pragma once

#include "cinder/Color.h"
#include "cinder/gl/Ubo.h"
#include "Shaders.h"

using namespace ci;
using namespace std;

namespace mocha {

	typedef struct {
		glm::vec4 mColorAmbient = ColorAf::gray(0.2f);
		glm::vec4 mColorDiffuse = ColorAf::gray(0.8f);
		glm::vec4 mColorEmission = ColorAf::black();
		glm::vec4 mColorSpecular = ColorAf::black();
		float mShininess = 0.0;
	}Material;


	class DeferredShadedObject {

	protected:
		float uEmissive = 1.0;

		//! Stores the id for the object to do material lookup in lighting stage.
		float materialId;

		//! Stores the current material settings for the object.
		Material lightingColors;


		//! uploads lighting information onto shader.
		ci::gl::UboRef mLightData;

	public:
		ci::gl::VboMeshRef mMesh;
		ci::gl::BatchRef mBatch;
		//! shader for the object
		ci::gl::GlslProgRef mShader;
		DeferredShadedObject() {}

		void setEmissive(float uEmissive) {
			this->uEmissive = uEmissive;
		}

		//! set the material id in so the renderer can know which material to look up
		void setMaterialId(int materialId) {
			this->materialId = materialId;
		}

		int getMaterialId() {
			return materialId;
		}

		//! pass-thru to ensure a drawing function exists.
		virtual void draw() {
			mBatch->draw();
		}
		
		//! Pass-thru to ensure update function exists.
		virtual void update() {}


		//! Returns the material for the object
		Material getMaterial() {
			return lightingColors;
		}

		DeferredShadedObject& colorAmbient(const ci::ColorAf& c)
		{
			lightingColors.mColorAmbient = c;
			return *this;
		}

		DeferredShadedObject& colorDiffuse(const ci::ColorAf& c)
		{
			lightingColors.mColorDiffuse = c;
			return *this;
		}

		DeferredShadedObject& colorEmission(const ci::ColorAf& c)
		{
			lightingColors.mColorEmission = c;
			return *this;
		}

		DeferredShadedObject& colorSpecular(const ci::ColorAf& c)
		{
			lightingColors.mColorSpecular = c;
			return *this;
		}

		DeferredShadedObject& shininess(float v)
		{
			lightingColors.mShininess = v;
			return *this;
		}

		//! passthur to the GlslProgRef's uniform function
		template<typename T>
		void uniform(std::string name, T value) {
			mShader->uniform(name, value);
		}
	};
}