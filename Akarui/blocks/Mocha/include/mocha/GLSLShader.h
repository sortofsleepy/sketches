#pragma once

#include "cinder/gl/gl.h"
#include "cinder/gl/GlslProg.h"
#include "cinder/app/App.h"
#include "cinder/Log.h"

#define STRINGIFY(A) #A

namespace mocha {

	class GLSLShader {

		ci::gl::GlslProgRef mGLSLShader;
		ci::gl::GlslProg::Format mFormat;

		std::string vertexChunks;
		std::string fragmentChunks;
		std::string geometryChunks;

		bool shaderBuilt;
		void buildShaderFormat();

		GLuint version;
	public:
		GLSLShader();

		GLSLShader& loadVertexChunk(std::string pathorchunk);
		GLSLShader& loadFragmentChunk(std::string pathorchunk);
		GLSLShader& loadGeometryChunk(std::string pathorchunk);


		GLSLShader& loadVertexChunks(std::vector<std::string> pathorchunks);
		GLSLShader& loadFragmentChunks(std::vector<std::string> pathorchunks);
		GLSLShader& loadGeometryChunks(std::vector<std::string> pathorchunks);

		void setVersion(GLuint version) {
			this->version = version;
		}

		void checkVertex() {
			CI_LOG_I(vertexChunks);
		}

		void checkFragment() {
			CI_LOG_I(fragmentChunks);
		}

		void checkGeometry() {
			CI_LOG_I(geometryChunks);
		}

		//! Binds shader for use.
		void bind();

		ci::gl::GlslProgRef getShader();
		

		// passthru to uniform function on GLSLProg object
		template<typename T>
		void uniform(std::string name, T value) {
			mGLSLShader->uniform(name, value);
		}
	};

}