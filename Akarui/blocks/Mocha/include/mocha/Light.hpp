#ifndef Light_hpp
#define Light_hpp

#include "cinder/Color.h"
#include "cinder/Vector.h"
#include "cinder/gl/Batch.h"
#include "cinder/gl/gl.h"
#include "Shaders.h"
#include <memory>

// this is pretty much just straight out of the DeferredShading example in Cinder

namespace mocha {
	namespace DeferredRendering {
		typedef std::shared_ptr<class Light>LightRef;
		class Light {
		protected:
			ci::ColorAf mColorAmbient;
			ci::ColorAf mColorDiffuse;
			ci::ColorAf mColorSpecular;
			float mIntensity;
			float mRadius;
			float mVolume;
			ci::vec3 mPosition;
			bool mIsOn;


			ci::gl::GlslProgRef mShader;
			ci::gl::VboMeshRef mMesh;
			ci::gl::BatchRef mBatch;

			bool meshSet;
	
		public:
			Light();
			
			static LightRef create() {
				return LightRef(new Light());
			}

			static LightRef create(Light light) {
				Light * mLight = new Light();

				mLight->colorAmbient(light.getColorAmbient());
				mLight->colorSpecular(light.getColorSpecular());
				mLight->colorDiffuse(light.getColorDiffuse());
				mLight->intensity(light.getIntensity());
				mLight->radius(light.getRadius());
				mLight->volume(light.getVolume());
				mLight->position(light.getPosition());

				return LightRef(mLight);
			}

			Light& colorAmbient(const ci::ColorAf& c);
			Light& colorDiffuse(const ci::ColorAf& c);
			Light& colorSpecular(const ci::ColorAf& c);
			Light& intensity(float v);
			Light& radius(float v);
			Light& volume(float v);
			Light& position(const ci::vec3& v);

			const ci::ColorAf& getColorAmbient() const;
			const ci::ColorAf& getColorDiffuse() const;
			const ci::ColorAf& getColorSpecular() const;
			float getIntensity() const;
			const ci::vec3& getPosition() const;
			float getRadius() const;
			float getVolume() const;

			bool isOn() {
				return mIsOn;
			}

			void toggleLight() {
				mIsOn = !mIsOn;
			}

			void setColorAmbient(const ci::ColorAf& c);
			void setColorDiffuse(const ci::ColorAf& c);
			void setColorSpecular(const ci::ColorAf& c);
			void setIntensity(float v);
			void setRadius(float v);
			void setVolume(float v);
			void setPosition(const ci::vec3& v);

			//! Sets the representaiton for the light that you want to use. 
			//! By default - we draw lights as points.
			Light& setLightMesh(ci::gl::BatchRef mBatch);
			
			//! Sets up the mesh for rendering.
			void setupMesh(ci::gl::GlslProg::Format shdFmt);
		
			void draw() {
				if (meshSet) {
					mBatch->draw();
				}
			}

			ci::gl::BatchRef getBatch() {
				return mBatch;
			}

			bool hasMesh() {
				return meshSet;
			}

			ci::gl::GlslProgRef getShader() {
				return mBatch->getGlslProg();
			}

			template<typename T>
			void uniform(std::string name, T value) {
				mBatch->getGlslProg()->uniform(name, value);
			}
			
		};
	}
}

#endif