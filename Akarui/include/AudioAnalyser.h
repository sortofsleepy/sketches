#pragma once

#include "cinder/gl/Texture.h"
#include "cinder/app/App.h"
#include "cinder/audio//Context.h"
#include "cinder/audio/audio.h"
#include "cinder/audio/Voice.h"
#include "cinder/audio/Source.h"
#include "cinder/Thread.h"
#include "cinder/ConcurrentCircularBuffer.h"
#include "cinder/gl/gl.h"
#include "cinder/gl/Vbo.h"
#include <memory>
#include <string>
#include <thread>
namespace mocha {
	namespace audio {
		class AudioAnalyser {

			//! Thread function for loading audio
			std::shared_ptr<std::thread>	mThread;

			//! audio player responsible for playing things. 
			ci::audio::BufferPlayerNodeRef mBufferPlayerNode;

			//! Notes responsible for obtaining and updating data
			ci::audio::MonitorSpectralNodeRef	mMonitorSpectralNode, mMonitorLeft, mMonitorRight;

			std::vector<float>					mMagSpectrum;

			ci::audio::GainNodeRef				mGain;

			ci::gl::VboRef audioData;
			ci::gl::BufferTextureRef mTex;

			ci::Rectf mBounds;

			// current position in music
			double currentPlayhead;

			int mFFTSize;

			// flag for whether or not music is playing
			bool mIsPlaying;

			// whether or not a file is loaded
			bool mFileIsLoaded;


			void unloadAndResetNodes();
		public:
			AudioAnalyser();
			~AudioAnalyser();

			void loadFile(std::string filepath);
			void loadFile(ci::fs::path filepath);
			void play();
			void pause();
			
		};
	}
}