#pragma once

#include "mocha/core.h"
#include "mocha/PostEffect.h"
#include "mocha/EasyCam.h"
class GlowOrb : public mocha::Object3D{

	ci::mat4 mRotation;
public:
	GlowOrb();
	void update(EasyCamRef mCamera = nullptr);
	void draw();
};