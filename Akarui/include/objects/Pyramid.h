
#pragma once

#pragma once


#include "cinder/gl/VboMesh.h"
#include "cinder/gl/GlslProg.h"
#include "cinder/gl/GlslProg.h"
#include "cinder/gl/Batch.h"
#include "cinder/gl/gl.h"
#include <vector>
#include "cinder/app/App.h"
#include "cinder/Rand.h"

using namespace std;
using namespace ci;

class Pyramid  {
public:
	ci::gl::VboMeshRef mMesh;
	ci::gl::BatchRef mBatch;
	ci::gl::GlslProgRef mShader;

	ci::vec3 scale = vec3(50.0);


	Pyramid() {
		mShader = gl::getStockShader(gl::ShaderDef().color());
		setup();
	}

	Pyramid(std::string vertex, std::string fragment) {
		gl::GlslProg::Format fmt;
		fmt.vertex(app::loadAsset(vertex));
		fmt.fragment(app::loadAsset(fragment));
		mShader = gl::GlslProg::create(fmt);
		setup();
	}

	Pyramid(gl::GlslProgRef shader) {
		mShader = shader;
		setup();
	}

	void setScale(vec3 scale) {
		this->scale = scale;
	}

	void setup() {

		vector <ci::vec3> positions,colors,normals;
		vector <uint32_t> indices;


		// add vertices
		positions.push_back(vec3(0.0, 1.0, -0.5));
		positions.push_back(vec3(-1.0, -0.5, -0.5));
		positions.push_back(vec3(1.0, -0.5, -0.5));
		positions.push_back(vec3(0.0, 0.0, 1.0));

		// add colors
		colors.push_back(vec4(randVec3(),randFloat()));
		colors.push_back(vec4(randVec3(), randFloat()));
		colors.push_back(vec4(randVec3(), randFloat()));
		colors.push_back(vec4(randVec3(), randFloat()));

		// add indices
		indices.push_back(0);
		indices.push_back(1);
		indices.push_back(2);
		indices.push_back(0);
		indices.push_back(2);
		indices.push_back(3);
		indices.push_back(0);
		indices.push_back(3);
		indices.push_back(1);
		indices.push_back(2);
		indices.push_back(1);
		indices.push_back(3);

		// add normals
		vec3 U = positions[1] - positions[0];
		vec3 V = positions[2] - positions[0];

		float x = (U.y * V.z) - (U.z * V.y);
		float y = (U.z * V.x) - (U.x * V.z);
		float z = (U.x * V.y) - (U.y * V.x);

		// faking normals a bit
		normals.push_back(glm::normalize(vec3(x, y, z)));
		normals.push_back(glm::normalize(vec3(x, y, z)));
		normals.push_back(glm::normalize(vec3(x, y, z)));
		normals.push_back(glm::normalize(vec3(x, y, z)));

		// build mesh
		gl::VboMesh::Layout layout;
		layout.attrib(geom::POSITION, 3);
		layout.attrib(geom::NORMAL, 3);
		layout.attrib(geom::COLOR, 4);

		mMesh = gl::VboMesh::create(positions.size(), GL_TRIANGLES, { layout }, indices.size(), GL_UNSIGNED_INT);
		mMesh->bufferAttrib(geom::POSITION, sizeof(vec3) * positions.size(), positions.data());
		mMesh->bufferAttrib(geom::COLOR, sizeof(vec4) * colors.size(), colors.data());
		mMesh->bufferAttrib(geom::NORMAL, sizeof(vec3) * normals.size(), normals.data());
		mMesh->bufferIndices(sizeof(uint32_t) * indices.size(), indices.data());
		mBatch = gl::Batch::create(mMesh, mShader);

	}

	void draw() {
		gl::pushMatrices();
		gl::scale(vec3(scale));
		mBatch->draw();
		gl::popMatrices();
	}

};