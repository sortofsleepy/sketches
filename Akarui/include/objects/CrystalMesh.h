#pragma once

#include "mocha/core.h"
#include "cinder/gl/gl.h"
#include "cinder/Thread.h"
#include "cinder/ConcurrentCircularBuffer.h"
#include "cinder/app/App.h"
#include "mocha/math.h"
#include "cinder/Rand.h"

using namespace ci;
using namespace std;

class CrystalMesh : public mocha::Object3D {


	//! Position of the crystal 
	ci::vec3 position;

	float mass = 40.0f;

	float G = 1.0f;

	//! number of elements associated with a crystal.
	int numElements;

	//! Flag to ensure we only build one mesh when tasked.
	bool buildShape;

	//! Flag for telling us when a mesh was rebuilt, indicating the need to rebuild the batch.
	bool meshRebuilt;

	//! flag to kick off a while statement within the seperate thread.
	bool threadStop;

	//! indicates when the mesh is first available for drawing.
	bool meshAvailable;

	//! Thread function for generating new shapes. 
	shared_ptr<thread>	mThread;

 	vector<ci::vec3> mVertices;

	vector<uint32_t> mIndices;

	vector<ci::vec3> randomPoints;

	float boundingSphereRadius = 30;

	ci::gl::TextureCubeMapRef mIrradianceCube;
	ci::gl::TextureCubeMapRef mRadiance;

public:
	CrystalMesh();
	void buildTextureMaps();
	void setShader(string vertex, string fragment);

	void setup(int maxElements = 150, float boundingSphereRadius = 30.0f);

	float getRadius() {
		return boundingSphereRadius;
	}

	//! Calculates attractive force between crystal and a target. 
	//! Pass in target's position and mass. 
	ci::vec3 attract(ci::vec3 targetPos, float targetMass, bool repel = false);

	//! Triggers rebuild of crystal mesh.
	void buildCrystal() {
		buildShape = !buildShape;
		threadStop = false;
	}

	void destroy() {
		threadStop = true;
		mThread->join();
	}

	//! Get vertices from the final crystal.
	vector<ci::vec3> getVertices() {
		return mVertices;
	}

	void generateNewCrystal(gl::ContextRef context);
	void draw(bool usePBR=true);
	geom::SourceMods randShape();
};

