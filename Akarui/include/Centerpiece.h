#pragma once

#include "Scene.h"
#include "objects/CrystalMesh.h"
#include "SoundGlobe.h"
#include "mocha/PostEffect.h"
class Centerpiece {

	// the particle system around the main sphere. 
	//SphereSystem csystem;

	SoundGlobe globe;

	// the central object of the scene. 
	CrystalMesh crystal;

	// a effect to grab the edge of the crystal.
	mocha::PostEffectRef edgeHighlight;

	// a effect to bloom the edges
	mocha::PostEffectRef edgeBloom;

	//! Crystal is by default, rather small. Need to scale it up a bit. 
	//! TODO need to adjust depending on camera zoom.
	ci::vec3 crystalScale = vec3(10);

	EasyCamRef mCam;
public:

	void setup(EasyCamRef mCam){
		this->mCam = mCam;


		// ============== CRYSTAL SETUP =================== // 

		globe.setup(30);
		globe.setCamera(mCam);

		edgeHighlight = mocha::PostEffect::create(true);
		edgeHighlight->setShader("post/Edge.glsl");
		edgeHighlight->uniform("edgeColor", vec4(1.0, 1.0, 0.0, 1.));

		edgeBloom = mocha::PostEffect::create();
		edgeBloom->setShader("post/bloom.glsl");
		edgeBloom->setInputTexture(edgeHighlight->getOutput());

		crystal.setup();
		crystal.setShader("crystal.vert", "crystal.frag");
		crystal.buildCrystal();

	}
	void updatePost() {





		// =========== UPDATE CRYSTAL POST PROCESSING ============= //
		edgeHighlight->bind();
		mCam->setMatrices();

		gl::pushMatrices();
		gl::scale(crystalScale);
		crystal.draw(false);

		gl::popMatrices();

		edgeHighlight->unbind();

		edgeHighlight->applyEffect([=](gl::GlslProgRef shader)->void {
			shader->uniform("time", (float)app::getElapsedSeconds());
		});

		edgeBloom->applyEffect([=](gl::GlslProgRef shader)->void {
			shader->uniform("sample_offset", vec2(1.0f / app::getWindowWidth(), 1.0f / app::getWindowHeight()));
			shader->uniform("attenuation", 1.5f);
		});

	}

	void update() {
		//gl::enableBlending();
		updatePost();

	

		mCam->setMatrices();
		gl::disableBlending();

		gl::enableDepth();
		gl::enableDepthRead();
		gl::enableDepthWrite();

		// draw crystal 
		gl::pushMatrices();
		gl::scale(crystalScale);
		crystal.draw();
		gl::popMatrices();


		// draw crystal layer with post processing. 
		gl::enableBlending();
		gl::enableAdditiveBlending();

		// draw the edge layer
		edgeBloom->drawOutput();



	}


	void draw() override {
		gl::setMatricesWindow(app::getWindowSize());
		gl::viewport(app::getWindowSize());


		// draw the composited scene. 
		gl::draw(mFbo->getColorTexture(), app::getWindowBounds());


		globe.draw();

	}

	void destroy() override {
		crystal.destroy();
	}

};