#pragma once

#include "cinder/gl/Fbo.h"
#include "cinder/gl/gl.h"
#include "cinder/app/App.h"
#include "mocha/Shaders.h"
using namespace ci;
using namespace std;

#define STRINGIFY (A) #A


/*
	A basic class for creating a texture with a fragment shader of some kind. 
	Provides vertex shader, just provide your own fragment shader.
*/
class ShaderMaterial {
	bool mShaderSet = false;
	ci::gl::FboRef mFbo;
	ci::gl::GlslProgRef mShader;
public:
	ShaderMaterial(int width=app::getWindowWidth(),int height = app::getWindowHeight()) {
	
		mFbo = gl::Fbo::create(width, height);
	}

	void update(std::function<void(ci::gl::GlslProgRef)> func=nullptr) {
		if(!mShaderSet){
			return;
		}

		gl::ScopedFramebuffer fbo(mFbo);
		gl::ScopedGlslProg shader(mShader);
		gl::clear(ColorA(0, 0, 0, 0));
		

		if (func != nullptr) {
			func(mShader);
		}
	
		// TODO maybe re-do to build own mesh triangle? (ala a-big-triangle)
		gl::drawSolidRect(mFbo->getBounds());


	}

	gl::TextureRef getTexture() {
		return mFbo->getColorTexture();
	}

	void loadShader(std::string path) {
		gl::GlslProg::Format fmt;
		fmt.vertex(mocha::shaders::passthru);
		fmt.fragment(app::loadAsset(path));
		
		mShader = gl::GlslProg::create(fmt);
		mShaderSet = true;
	}

};