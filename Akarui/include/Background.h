#pragma once

#include "cinder/gl/gl.h"
#include "mocha/core.h"

using namespace ci;
using namespace std;
class Background : public mocha::Object3D{

public:
	Background() {

		geom::Sphere sphere;
		sphere.subdivisions(20).radius(400);

		mMesh = gl::VboMesh::create(sphere);

		gl::GlslProg::Format fmt;
		fmt.vertex(app::loadAsset("bg.vert"));
		fmt.fragment(app::loadAsset("bg.frag"));

		try {
			mShader = gl::GlslProg::create(fmt);
		}
		catch (ci::gl::GlslProgCompileExc &e) {
			CI_LOG_I(e.what());
		}

		mBatch = gl::Batch::create(mMesh, mShader);
	}

	void draw() {

		//TODO tie color shift to audio input as well. 

		mBatch->getGlslProg()->uniform("time", (float)app::getElapsedSeconds());
		mBatch->getGlslProg()->uniform("resolution", vec2(app::getWindowWidth(), app::getWindowHeight()));
		mBatch->draw();
	}
};