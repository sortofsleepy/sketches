#pragma once


#include "mocha/TFBuffer.h"
#include "mocha/core.h"
#include "cinder/gl/GlslProg.h"
#include "cinder/Rand.h"
#include "cinder/app/App.h"
#include "objects/Pyramid.h"
#include "mocha/Shaders.h"
#include "mocha/GLSLShader.h"


/**
Particle type holds information for rendering and simulation.
Used to buffer initial simulation values.
*/
struct Particle
{
	ci::vec3 pos;
	ci::vec4 sphereInfo;
	ci::vec3 velocity;
	ci::vec3 acceleration;

	/*
	float phi;
	float theta;
	float phiSpeed;
	float thetaSpeed;*/
};


class SphereSystem {

	//! Transform feedback buffer to use to animate system.
	mocha::GPU::TFBufferRef mBuffer;

	//! Render shader for rendering particle system
	ci::gl::GlslProgRef mRenderProg;

	//! Number of particles
	int mNumParticles;

	//! The batch for each particle's shape.
	ci::gl::BatchRef mParticleShape;

	//! VBO to hold particle's positions
	ci::gl::VboRef mInstancedPositions;

	//! The basic shape for each particle. 
	Pyramid mShape;

	int radius;

	ci::mat4 mRotation;
public:
	SphereSystem();
	void setup(int mNumParticles=600, std::string vertex="shaders/prender.vert",std::string fragment="shaders/prender.frag");
	void update();
	void draw();
};