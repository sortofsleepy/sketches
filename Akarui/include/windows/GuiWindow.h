#pragma once

#include "BasicWindow.h"
#include "ui/Interface.h"

class GuiWindow : public BasicWindow {

public:
	GuiWindow() {}
	void setup(ci::app::WindowRef window) {
		
		this->window = window;

		window->getSignalDraw().connect(std::bind(&GuiWindow::draw, this));
	}

	void draw() {
		gl::clear(Color(1, 0, 0));
		window->getRenderer()->makeCurrentContext();


	}
};
