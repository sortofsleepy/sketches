#pragma once


#include "BasicWindow.h"
#include "mocha/EasyCam.h"
#include "cinder/Rand.h"
#include "ui/Settings.h"
#include "scenes/Scene.h"

#include "Background.h"

// include scenes
#include "scenes/Scene1.h"

class AppWindow : public BasicWindow {
	
	Background bg;

	// main camera
	EasyCamRef mCam;

	// global application settings
	akarui::Settings * appSettings;

	// all the scens. 
	vector<Scene*> scenes;

	// current scene we're looking at. 
	int currentScene;

	// all the scenes.
	Scene1 opening;
public:
	AppWindow() {
		
	}

	void setup(ci::app::WindowRef window, akarui::Settings * appSettings) {
		mCam = EasyCam::create(window);
		mCam->setZoom(-100);
		mCam->enableControls();

		

		this->window = window;
		this->appSettings = appSettings;
		currentScene = 0;
		
		// ensure things are set up with the correct Window's OpenGL context.
		window->getRenderer()->makeCurrentContext();

		// add all scenes to the deck.
		scenes.push_back(&opening);

		// run through setup for all scenes.
		for (Scene * scene : scenes) {
			
			// setup with main camera
			scene->setCamera(mCam);

			// run setup process.
			scene->setup();
		}

		// scenes are set up with the paused state to be true, 
		// eensure the first scene has is false for the paused state.
		scenes[0]->togglePause();

		onDraw(std::bind(&AppWindow::draw, this));
		onClose(std::bind(&AppWindow::destroy, this));
		onResize([this] {

			mCam->getCamera().setPerspective(60.0, app::getWindowWidth() / app::getWindowHeight(), 1.0, 1000.0);
		});
	}

	void goToScene(int index = 0) {
		scenes[currentScene]->togglePause();
		currentScene = index;

		scenes[currentScene]->togglePause();
	}

	void update() {
		scenes[currentScene]->update();
	}

	void draw() {
		window->getRenderer()->makeCurrentContext();
		gl::clear(Color(0,0, 0));

		update();

	
		mCam->setMatrices();
		bg.draw();

		scenes[currentScene]->draw();
		
	
	}

	void destroy() {
		scenes[currentScene]->destroy();
	}
};