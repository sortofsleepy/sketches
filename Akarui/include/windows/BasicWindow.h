#pragma once

#include "cinder/app/App.h"
#include "cinder/app/RendererGl.h"
#include "cinder/gl/gl.h"
#include "cinder/Log.h"

using namespace ci;
using namespace app;

typedef std::shared_ptr<class BasicWindow>BasicWindowRef;

class BasicWindow {
protected:
	ci::app::WindowRef window;
public:

	BasicWindow() {}
	BasicWindow(WindowRef window) {
		this->window = window;
	}

	static BasicWindowRef create(ci::app::WindowRef window) {
		return BasicWindowRef(new BasicWindow(window));
	}

	void onDraw(std::function<void()> func) {
		window->getSignalDraw().connect(func);
	}

	void onResize(std::function<void()> func) {
		window->getSignalResize().connect(func);
	}

	void onClose(std::function<void()> func) {
		window->getSignalClose().connect(func);
	}


	virtual void setup() {}
	virtual void draw() {
		gl::clear(Color(1, 1, 0));
	}
};