#pragma once


#include "BasicWindow.h"
#include "mocha/EasyCam.h"

#include "CrystalMesh.h"
#include "SoundGlobe.h"

#include "cinder/Rand.h"
#include "Settings.h"

class AppWindow : public BasicWindow {
	EasyCamRef mCam;

	CrystalMesh crystal;
	std::vector<SoundGlobe> systems;

	// global application settings
	akarui::Settings * appSettings;

public:
	AppWindow() {
		mCam = EasyCam::create();
		mCam->setZoom(-100);
	}

	void setup(ci::app::WindowRef window, akarui::Settings * appSettings) {

		this->window = window;
		this->appSettings = appSettings;

		// ensure things are set up with the correct Window's OpenGL context.
		window->getRenderer()->makeCurrentContext();
		setupSystems();

		// setup central object.
		crystal.setup();

		// build initial crystal
		crystal.buildCrystal();

		onDraw(std::bind(&AppWindow::draw, this));
		onResize(std::bind(&AppWindow::resize, this));
		onClose(std::bind(&AppWindow::destroy, this));

		window->getSignalKeyDown().connect([this](KeyEvent e) {

		});
	}

	void setupSystems() {

		int num = 1;
		for (int i = 0; i < num; ++i) {
			SoundGlobe globe;
			globe.setup(crystal.getRadius());
			systems.push_back(globe);
		}

	}

	void resize() {

	}

	void update() {
		for (SoundGlobe &globe : systems) {
			ci::vec3 force = crystal.attract(globe.getPosition(), globe.getMass());
			ci::vec3 fric = globe.calculateFriction();

			float x = randFloat(1, 50);
			float y = randFloat(1, 50);
			float z = randFloat(1, 50);

			globe.addForce(fric);
			globe.addForce(force);

			globe.update();


		}
	}

	void draw() {
		window->getRenderer()->makeCurrentContext();
		gl::clear(Color(0, 0, 0));

		update();

		mCam->setMatrices();
		//crystal.draw();

		for (SoundGlobe &globe : systems) {
			globe.draw();
		}
	}

	void destroy() {
		crystal.destroy();
	}
};