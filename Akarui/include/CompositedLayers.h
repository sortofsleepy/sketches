#pragma once

#include "cinder/gl/Fbo.h"
#include "cinder/app/App.h"
#include "mocha/core.h"
using namespace std;
using namespace ci;

#define STRINGIFY(A) #A

class CompositedLayers : public mocha::Object3D{

	ci::gl::FboRef mFbo;



	float uMix = 0.7f;

	std::string vertex = STRINGIFY(
		uniform mat4 ciModelViewProjection;
	in vec4 ciPosition;
	in vec2 ciTexCoord0;
	out vec2 vUv;
	const vec2 scale = vec2(0.5, 0.5);

	void main() {
		vUv = ciTexCoord0;
		gl_Position = ciModelViewProjection * ciPosition;
	}
	);

	std::string fragment = STRINGIFY(
		in vec2 vUv;
		out vec4 glFragColor;
		uniform sampler2D uTex0;
		uniform sampler2D uTex1;
		uniform float uMix;

		void main() {
		
			vec4 tex1 = texture(uTex0, vUv);
			vec4 tex2 = texture(uTex1, vUv);
			tex2 += tex2;

			glFragColor = mix(tex1, tex2, uMix);

		}
	);

	
public:
	ci::gl::TextureRef tex1;
	ci::gl::TextureRef tex2;
	CompositedLayers() {

		gl::Texture::Format tx;
		tx.setInternalFormat(GL_RGBA32F);
		tx.setWrap(GL_CLAMP_TO_EDGE, GL_CLAMP_TO_EDGE);
		tx.minFilter(GL_NEAREST);
		tx.magFilter(GL_NEAREST);

		gl::Fbo::Format fmt;
		fmt.setColorTextureFormat(tx);

		mFbo = gl::Fbo::create(app::getWindowWidth(), app::getWindowHeight(), fmt);

		mMesh = gl::VboMesh::create(geom::Rect().rect(Rectf(0.0f,0.0f,(float)app::getWindowWidth(),(float)app::getWindowHeight())));
		
		gl::GlslProg::Format sfmt;
		sfmt.vertex(vertex);
		sfmt.fragment(fragment);

		mShader = gl::GlslProg::create(sfmt);

		mBatch = gl::Batch::create(mMesh, mShader);
	}

	void update() {
		gl::ScopedFramebuffer fbo(mFbo);
		gl::clear(ColorA(0, 0, 0, 0));
		gl::setMatricesWindow(app::getWindowSize());
		gl::viewport(app::getWindowSize());
		
		gl::ScopedGlslProg shader(mShader);
		gl::ScopedTextureBind uTex0(tex1, 0);
		gl::ScopedTextureBind utex1(tex2, 1);

		mShader->uniform("uTex0", 0);
		mShader->uniform("uTex1", 1);
		mShader->uniform("uMix", uMix);
		mBatch->draw();
	}

	gl::TextureRef getOutput() {
		return mFbo->getColorTexture();
	}
};