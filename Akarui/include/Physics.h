#pragma once

#include "cinder/Vector.h"
using namespace ci;

namespace mocha {
	class Physics {

		ci::vec3 acceleration;
		ci::vec3 velocity;
		ci::vec3 position;

		float frictionMag;
	public:
		Physics() {}

		void addForce(ci::vec3 force) {

		}

		vec3 calculateFriction() {

			vec3 friction = vec3(velocity);
			friction *= -10;
			friction = glm::normalize(friction);
			friction *= frictionMag;
			return friction;
		}

		void updatePhysics() {
			velocity += acceleration;
			position += velocity;

			acceleration *= vec3(0);
		}
	};
}