#pragma once

#include "Scene.h"
#include "objects/CrystalMesh.h"
#include "SoundGlobe.h"
#include "mocha/PostEffect.h"
#include "Background.h"
#include "CompositedLayers.h"

class Scene1 : public Scene {
	Background bg;
	CompositedLayers layers;

	// AABB box to ensure things don't spiral out of bounds. 
	ci::AxisAlignedBox aabb;

	ci::gl::FboRef mAccum;

	vector<SoundGlobe> globes;

	// the central object of the scene. 
	CrystalMesh crystal;

	// a effect to grab the edge of the crystal.
	mocha::PostEffectRef edgeHighlight;

	// a effect to bloom the edges
	mocha::PostEffectRef edgeBloom;

	// ssao to help render everything a bit nicer. 
	mocha::PostEffectRef ssao;

	//! Crystal is by default, rather small. Need to scale it up a bit. 
	//! TODO need to adjust depending on camera zoom. Also maybe move this into the crystal files?
	ci::vec3 crystalScale = vec3(20);
public:

	void setup() override {

		gl::Fbo::Format fmt; 
		mAccum = gl::Fbo::create(app::getWindowWidth(), app::getWindowHeight(), fmt);

		auto boundValue = 530;
		aabb.set(vec3(-boundValue,boundValue,boundValue), vec3(boundValue, -boundValue, -boundValue));

	
		for (int i = 0; i < 3; ++i) {
			
			SoundGlobe globe;
			// setup globe
			globe.setup(50);

			globes.push_back(globe);
			
		}

		layers.tex2 = mFbo->getColorTexture();
		layers.tex1 = mAccum->getColorTexture();

		// ============== POST SETUP =================== // 

	
		edgeHighlight = mocha::PostEffect::create(true);
		edgeHighlight->setShader("post/Edge.glsl");
		edgeHighlight->uniform("edgeColor", vec4(1.0, 1.0, 0.0, 1.));

		edgeBloom = mocha::PostEffect::create();
		edgeBloom->setShader("post/bloom.glsl");
		edgeBloom->setInputTexture(edgeHighlight->getOutput());

		crystal.setup();
		crystal.setShader("crystal.vert", "crystal.frag");
		crystal.buildCrystal();

	}
	void updatePost() {


		mCam->orbitTarget(2.2);
		// =========== UPDATE CRYSTAL POST PROCESSING ============= //
		edgeHighlight->bind();
		mCam->setMatrices();

		// draw globes
		for (SoundGlobe &globe : globes) {
			globe.drawGlobe();
		}

		gl::pushMatrices();
		gl::scale(crystalScale);
		crystal.draw(false);

		gl::popMatrices();

		edgeHighlight->unbind();

		edgeHighlight->applyEffect([=](gl::GlslProgRef shader)->void {
			shader->uniform("time", (float)app::getElapsedSeconds());
		});

		edgeBloom->applyEffect([=](gl::GlslProgRef shader)->void {
			shader->uniform("sample_offset", vec2(1.0f / app::getWindowWidth(), 1.0f / app::getWindowHeight()));
			shader->uniform("attenuation", 1.5f);
		});

	}

	void update() override {
		//gl::enableBlending();
		updatePost();

		// update globes
		for (SoundGlobe &globe : globes) {
			globe.update();
		}

		gl::ScopedFramebuffer fbo(mFbo);
		gl::clear(ColorA(0, 0, 0, 0));

		mCam->setMatrices();
		gl::disableBlending();

		gl::enableDepth();
		gl::enableDepthRead();
		gl::enableDepthWrite();

		// draw globe systems
		for (SoundGlobe &globe : globes) {
			globe.drawSystem();
		}

		// draw crystal 
		gl::pushMatrices();
		gl::scale(crystalScale);
		crystal.draw();
		gl::popMatrices();


		// draw crystal layer with post processing. 
		gl::enableBlending();
		gl::enableAdditiveBlending();

		// draw the edge layer
		edgeBloom->drawOutput();
		edgeBloom->drawOutput();




	}

	void updateBackground() {
		gl::ScopedFramebuffer fbo(mAccum);
		gl::clear(ColorA(0, 0, 0, 0));

		mCam->setMatrices();
		bg.draw();
	}


	void draw() override {

		updateBackground();
		gl::setMatricesWindow(app::getWindowSize());
		gl::viewport(app::getWindowSize());

		layers.tex2 = mFbo->getColorTexture();
		layers.update();

		gl::enableBlending();
		gl::enableAdditiveBlending();

		gl::draw(layers.getOutput(), app::getWindowBounds());
		
	}

	void destroy() override {
		crystal.destroy();
	}

	void checkGlobeIntersections() {
		

		// check to see if globes are close to instersecting and steer them away. 
		for (SoundGlobe &globe : globes) {
			globe.checkIntersection(globes);
			globe.checkBoundries(aabb);
		}

	}

};

/*
float desiredseparation = 25.0f;
PVector steer = new PVector(0, 0, 0);
int count = 0;
// For every boid in the system, check if it's too close
for (Boid other : boids) {
float d = PVector.dist(position, other.position);
// If the distance is greater than 0 and less than an arbitrary amount (0 when you are yourself)
if ((d > 0) && (d < desiredseparation)) {
// Calculate vector pointing away from neighbor
PVector diff = PVector.sub(position, other.position);
diff.normalize();
diff.div(d);        // Weight by distance
steer.add(diff);
count++;            // Keep track of how many
}
}
// Average -- divide by how many
if (count > 0) {
steer.div((float)count);
}

// As long as the vector is greater than 0
if (steer.mag() > 0) {
// First two lines of code below could be condensed with new PVector setMag() method
// Not using this method until Processing.js catches up
// steer.setMag(maxspeed);

// Implement Reynolds: Steering = Desired - Velocity
steer.normalize();
steer.mult(maxspeed);
steer.sub(velocity);
steer.limit(maxforce);
}
return steer;*/