#pragma once

#include "cinder/gl/Fbo.h"
#include "cinder/gl/gl.h"
#include "cinder/gl/Texture.h"
#include "mocha/core.h"
#include "mocha/EasyCam.h"

using namespace ci;
using namespace std;

class Scene {

protected:
	gl::FboRef mFbo;
	gl::Fbo::Format fmt;
	bool isPaused = true;

	EasyCamRef mCam;
public:
	Scene() {

	
		// setup fbo.
		fmt.setColorTextureFormat(mocha::getStandardDataTextureFormat());
		
		// add depth texture
		gl::Texture::Format depthFormat;
		depthFormat.setInternalFormat(GL_DEPTH_COMPONENT32F);
		depthFormat.setCompareMode(GL_COMPARE_REF_TO_TEXTURE);
		depthFormat.setMagFilter(GL_LINEAR);
		depthFormat.setMinFilter(GL_LINEAR);
		depthFormat.setWrap(GL_CLAMP_TO_EDGE, GL_CLAMP_TO_EDGE);

		
		gl::TextureRef depth = gl::Texture::create(app::getWindowWidth(), app::getWindowHeight(), depthFormat);

		fmt.attachment(GL_DEPTH_ATTACHMENT, depth);
		
		mFbo = gl::Fbo::create(app::getWindowWidth(), app::getWindowHeight(), fmt);
		

		// ensure it resizes as the window changes 
		app::getWindow()->getSignalResize().connect([this] {
			mFbo = gl::Fbo::create(app::getWindowWidth(), app::getWindowHeight(), fmt);
		});
	}

	void setCamera(EasyCamRef mCam) {
		this->mCam = mCam;
	}
	void pause() {
		isPaused = true;
	}

	void unpause() {
		isPaused = false;
	}

	void togglePause() {
		isPaused = !isPaused;
	}

	virtual void destroy() {}
	virtual void setup() {}
	virtual void update() {}
	virtual void draw() {

		gl::viewport(app::getWindowSize());
		gl::setMatricesWindow(app::getWindowSize());
		gl::draw(mFbo->getColorTexture(), Rectf(0, 0, app::getWindowWidth(), app::getWindowHeight()));
	}
};