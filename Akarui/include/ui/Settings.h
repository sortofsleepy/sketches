#pragma once

#include <string>

#include "AudioAnalyser.h"
namespace akarui {

	typedef struct {
		float volume = 0.0f;
		float particleMass = 2.0;
		float crystalMass = 20.0;


		std::string currentSong = "test song";

		mocha::audio::AudioAnalyser analyser;
	}Settings;
}