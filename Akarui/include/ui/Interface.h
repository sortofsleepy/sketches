#pragma once

#include "CinderImGui.h"
#include "cinder/Vector.h"
#include <string>
#include <functional>
#include <vector>

// A thin wrapper around ImGui / CinderImGui to make things a little easier 
// to understand and work with..
namespace akarui {
	namespace ui {


		// types of interface to wrap
		enum INTERFACE_TYPE {
			BUTTON,
			TEXT,
			SLIDER
		};

		//! All encompasing struct of properties an Interface object might
		//! need to use. 
		typedef struct {
			std::string label;
			float min, max;
			float * value;
			std::string textContent;
			INTERFACE_TYPE type;
			std::function<void()> callback;
			ci::vec2 size;
		}InterfaceItem;


		//! A light wrapper to make ImGui a little bit cleaner to use. 
		class Interface {

			//! A reference to all the controls added to a panel.
			std::vector<InterfaceItem> mControls;

		public:
			Interface();

			Interface& setupInterface();

			Interface& setupInterface(ci::app::WindowRef window);


			//! Adds a line break in the form of a line of blank text.
			void addLineBreak();

			//! Adds a text item to the panel. 
			void addItem(std::string label);
		
			//! Adds a slider to the panel. 
			void addItem(std::string label, float * value, float min, float max);

			//! Adds a button object to the panel. 
			void addItem(std::string label, std::function<void()> callback, ci::vec2 size = ci::vec2(100, 10));

			//! Render all the elements of the interface we want to build. Note that 
			//! it will render things in the order Interface::addItem was called. 
			void renderInterface();
		};
	}
}