#pragma once

#include "SphereSystem.h"
#include "objects/GlowOrb.h"
#include "cinder/Rand.h"
#include "mocha/math.h"

class SoundGlobe {
	SphereSystem system;
	GlowOrb orb;

	// props for updating the entire sphere itself. 
	ci::vec3 position = vec3(0.);
	ci::vec3 velocity = vec3(0.);
	ci::vec3 acceleration = vec3(0.);
	float repulsiveForce;
	float mass = 10.0;

	// friction
	float c = 0.5;
	float normal = 1;
	float frictionMag = c * normal;

	float phi;
	float theta;
	float phiSpeed;
	float thetaSpeed;

	float parentRadius;

public:
	SoundGlobe(float parentRadius = 500) {
		this->parentRadius;

		phi = randFloat();
		theta = randFloat();
		phiSpeed = randFloat(-0.01, 0.01);
		thetaSpeed = randFloat(-0.01, 0.01);
	}

	ci::vec3 getPosition() {
		return position;
	}

	float getMass() {
		return mass;
	}


	// helps to ensure a globe doesn't get too close to another globe. 
	void checkIntersection(vector<SoundGlobe> globes,float desiredSeperation=25.0f) {
		vec3 steer(0, 0, 0);
		int count = 0;
		for (SoundGlobe & globe : globes) {

			float d = glm::distance(position, globe.position);

			if ((d > 0) && (d < desiredSeperation)) {
				// Calculate vector pointing away from neighbor
				vec3 diff = position - globe.getPosition();
				glm::normalize(diff);

				diff /= d;        // Weight by distance
				steer += diff;
				count++;            // Keep track of how many
			}

		}

		// average the value
		if (count > 0) {
			steer /= count;
		}

		if (glm::length(steer) > 0) {
			steer = glm::normalize(steer);
			steer *= 1.0;
			steer -= velocity;
			steer = glm::clamp(steer, vec3(1), vec3(50));
		}

		// add force to the globe. 
		addForce(steer);
	}

	//! Helps to ensure our globe doesn't get too far away from the main 
	//! viewing area. 
	void checkBoundries(ci::AxisAlignedBox box) {

		// if the globe has left the box
		if (!box.contains(position)) {
			
			// reverse velocity
			velocity *= -1;
			
		}

	}

	void setup(float parentRadius) {
		system.setup();
		this->parentRadius = parentRadius;

	}

	void attractTowardsCenter() {
		vec3 force = vec3(0.) - position;
		float distance = glm::length(force);
		force = glm::normalize(force);

		float strength = (1.0 * 40.0 * mass) / (distance * distance);

		force *= strength;

		addForce(force);
	}

	void update() {

		orb.update();
		system.update();

		position = mocha::math::pointOnSphere(parentRadius, theta, phi);

		// try to keep objects close to the center as much as possible. 
		attractTowardsCenter();

		velocity += acceleration;
		position += velocity;

		acceleration *= vec3(0);

		phi += phiSpeed;
		theta += thetaSpeed;
		
	}

	void addForce(ci::vec3 force) {
		acceleration += (force / mass);
	}

	vec3 calculateFriction() {

		vec3 friction = vec3(velocity);
		friction *= -10;
		friction = glm::normalize(friction);
		friction *= frictionMag;
		return friction;
	}

	void drawSystem() {
		gl::pushMatrices();
		gl::translate(position);
		system.draw();
		gl::popMatrices();
	}

	void drawGlobe() {
		//system.draw();;
	
		gl::pushMatrices();
		gl::translate(position);
		orb.draw();
		gl::popMatrices();
	}
};