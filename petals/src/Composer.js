import View from './jirachi/framework/View'

class Composer extends View {
	constructor(gl){
		super(gl);

		this.passes = [];
	}

	addLayer(...views){
		if(views[0] instanceof View){
			this.passes = views;
		}
	}

	/**
	 * Compiles all the layers together and sets up resizing on the final layer
	 * so it will render properly.
	 * Regardless of whether or not you have any post processing passes, as long
	 * as you use a Composer in place of a View you must call this function prior to drawing
	 */
	compileLayers({
		resize=true,
		resizeWidth=window.innerWidth,
		resizeHeight = window.innerHeight
				  }={}){


		this._sortPasses();

		// figure out what the last pass is.
		let pass = null;
		if(this.passes.length > 0){
			pass = this.passes[this.passes.length - 1];
		}else{
			this.passes.push(this);
		}



        window.addEventListener('resize',() => {
            this.resolution[0] = window.innerWidth;
            this.resolution[1] = window.innerHeight;
            this.gl.viewport(0,0,window.innerWidth,window.innerHeight);

           if(resize){
               this.fbo.resize(window.innerWidth,window.innerHeight);

               this.passes.forEach(itm => {
                   itm.fbo.resize(window.innerWidth,window.innerHeight)
               })

               if(this.passes.length > 1){
                   if(pass.input !== null){
                       pass.input.resize(window.innerWidth,window.innerHeight);
                   }
               }
		   }
        })
		/*
		 window.addEventListener('resize',() => {
		 this.gl.viewport(0,0,window.innerWidth,window.innerHeight);
		 if(this.passes.length > 1){
		 if(pass.input !== null){
		 pass.input.resize(window.innerWidth,window.innerHeight);
		 }
		 }
		 })
		 */
	}

	run(){
		let gl = this.gl;
		//gl.disable(gl.BLEND);
		//gl.enable(gl.DEPTH_TEST);


		this.passes.forEach(obj => {
			obj.runPass();
		})
	}

	drawComposedScene(){
		let pass = this.passes[this.passes.length - 1];
		this.drawQuad.drawTexture(pass.getComposedLayer());
	}

	// ========== PRIVATE ========== //
	/**
	 * Assigns input textures to all of the passes
	 * @private
	 */
	_sortPasses(){
		let passes = this.passes;
		let input = this.input;
		let index = 0;
		let finalSetup = [];
		passes.forEach((obj) => {
			if(index === 0){
				obj.setInput(this.fbo.getTexture());
				finalSetup.push(obj);
			}else {
				obj.setInput(passes[index - 1].fbo.getTexture());
				finalSetup.push(obj);
			}

			index++;
		});


		this.passes = finalSetup;

	}
}

export default Composer;