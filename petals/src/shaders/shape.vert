// attributes of our mesh
in vec3 position;

// built-in uniforms from ThreeJS camera and Object3D
uniform mat4 projectionMatrix;
uniform mat4 modelViewMatrix;
uniform mat4 modelMatrix;
uniform mat3 normalMatrix;

void main(){
    gl_Position = projectionMatrix * modelMatrix * modelViewMatrix * vec4(position,1.);
}