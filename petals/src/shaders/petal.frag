uniform vec2 resolution;
uniform float time;
out vec4 glFragColor;
in vec3 vNormal;
in vec2 vUv;

const vec3 col1 = normalize(vec3(255,78,103));
const vec3 col2 = normalize(vec3(204,42,65));


void main(){

    vec2 pc = (2.0*gl_FragCoord.xy-resolution.xy)/min(resolution.y,resolution.x);
    vec2 uv = gl_FragCoord.xy / resolution.xy;
    vec3 uvCol = vec3(mix(uv,col1.xy,0.8),col2.y) * cos(time * 0.0005);

    vec4 sc = vec4(uvCol.xy, uv.x + mix(uvCol.y,col2.z,0.9) * sin(time),1.0);
    vec4 sa = sc;
    vec3 col = sc.xyz;

    vec3 nor = normalize( vec3( sa.x-sc.x, 0.01, sc.x-sc.x ) );
	float dif = clamp(0.5 + 0.5*dot( nor,vec3(0.5773) ),0.0,1.0);
	col *= 0.5 + 0.7 * dif * col;
	col += 0.3 * pow(nor.y,128.0);

    glFragColor = vec4(col,1.);
    //glFragColor = vec4(1.0,1.0,0.0,1.0);
    //glFragColor.a = 0.0002;

}
