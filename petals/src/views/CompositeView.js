import View from '../jirachi/framework/View'
import DrawQuad from '../jirachi/drawquad'
import {createTexture2d} from '../jirachi/core/texture'

/**
 * A simple compositing setup to join multiple View objects together.
 */
class CompositeView extends View{
	/**
	 * Constructor - pass in context and any number of View objects
	 * @param gl {WebGLRenderingContext} the webgl context to use
	 * @param views {View} a View object. The constructor can take any number of arguments after the context.
	 */
	constructor(gl,...views){
		super(gl);

		this.loaded = 0;
		this.tests = [];

		let img = new Image();
		img.src = "test.jpg";

		img.onload = () => {
			this.loaded += 1;
			this.tests.push(img);
            if(this.loaded === 2){
                this.renderView();
            }
		}

		let img2 = new Image();
		img2.src = "test2.jpg"

		img2.onload = () => {
			this.loaded += 1;
			this.tests.push(img2);
			if(this.loaded === 2){
				this.renderView();
			}
		}



		this.views = views;
	}

	renderView(){
		this.quad = new DrawQuad(this.gl);
		this.tests = this.tests.map(itm => {
			return createTexture2d(this.gl,{
				data:itm
			})
		});
		this.ready = true;
	}

	/**
	 * Adds a new layer to composite with other views.
	 * @param view
	 */
	addView(view){
		this.views.push(view);
	}
	/**
	 * Renders views in the order they've been passed in
	 */
	renderViews(){
		let gl = this.gl;

		this.fbo.bind();
		gl.clearScreen(0,0,0,1);
		gl.enableDepth();


		//gl.enableBlending();
        //gl.enableAdditiveBlending()
		//this.gl.setBlendFunction("SRC_ALPHA","ONE_MINUS_SRC_ALPHA");

        if(this.ready){
			//this.quad.drawTexture(this.tests[0]);
			//this.quad.drawTexture(this.tests[1]);
		}

		this.views.forEach(view => {
			view.draw();
		});
		this.fbo.unbind();

		this.draw();
	}

}

export default CompositeView;