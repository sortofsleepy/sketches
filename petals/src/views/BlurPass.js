import View from '../jirachi/framework/View'
import bloom from '../jirachi/shaders/post/blur.glsl'
class BlurPass extends View {
	constructor(gl){
		super(gl,{
			fragment:bloom,
			uniforms:[
				'resolution',
				'direction'
			]
		});

	}
	runPass(){

		let gl = this.gl;
		this.bindView()
		gl.clearScreen(0,0,0,0);

		//this.gl.enableAlphaBlending();
		if(this.input !== null){
			this.input.bind();
		}
		this.drawQuad.drawTexture(this.input,(shader)=>{
			shader.uniform('resolution',[window.innerWidth,window.innerHeight]);
			shader.uniform('direction',[1.0,0.0]);
		})
		this.unbindView();


	}
}

export default BlurPass;