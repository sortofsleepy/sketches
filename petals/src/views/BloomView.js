import View from '../jirachi/framework/View'
import bloom from '../jirachi/shaders/post/bloom.glsl'
class BloomView extends View {
	constructor(gl){
		super(gl,{
			fragment:bloom,
			uniforms:[
				'resolution',
				'sample_offset'
			]
		});
	}
	runPass(){

		this.fbo.bind();
		this.gl.clearScreen();
		this.gl.enableAdditiveBlending();
		this.gl.enableAlphaBlending();
		this.drawQuad.drawTexture(this.input,(shader) => {
			shader.uniform('resolution',this.resolution);
			shader.uniform('sample_offset',(1.0 / window.innerWidth))
		});
		this.fbo.unbind();
	}
}

export default BloomView