
import View from '../jirachi/framework/View'
import Parametric from '../objects/Parametric'
import Icosphere from '../objects/Icosphere'

/**
 * For rendering all non petal shapes.
 */
class RenderView extends View {
	constructor(gl,camera){
		super(gl)

		this.camera = camera;
		this.lines = [];

		this.time = 0.0;
		this._buildLines();
		this.ico = new Icosphere(gl);

	}

	_buildLines(){
		for(let i = 0; i < 5; ++i){
			let para = new Parametric(this.gl,{
				numSides:8,
				subdivisions:300
			});
			para.scale([10,10,10])
			this.lines.push(para);
		}
	}

	draw(){
		this.time += 0.01;
		let camera = this.camera;
		this.fbo.bind();

		this.gl.clearTransparent()

		this.ico.draw(this.camera);
		this.fbo.unbind();

		this.drawQuad.drawTexture(this.fbo.getTexture())
	}

}

export default RenderView;

/*
 this.lines.forEach((itm,idx) => {
 itm.rotateZ(0.5)
 itm.draw(camera, (shader) => {
 shader.uniform("index",idx);
 shader.uniform("time",this.time);
 shader.uniform('radialSegments',300);
 shader.uniform("thickness",itm.thickness);
 })
 })
 */