import View from '../jirachi/framework/View'
import Icosphere from '../objects/Icosphere'
class IcoView extends View {
    constructor(gl,camera){
        super(gl);


        this.shape =  new Icosphere(gl);

        this.camera = camera;
    }

    drawShapes(){
        let camera = this.camera;
        let gl = this.gl;
        this.bindView();

        gl.clearScreen(0,0,0,0);

        this.shape.draw(camera);
        this.unbindView();

        this.drawQuad.drawTexture(this.fbo.getTexture());
    }
}

export default IcoView;