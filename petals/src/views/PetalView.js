import Petals from '../objects/Petals'
import DataView from '../objects/DataView'
import {loadContextConditionalShader} from '../jirachi/core/shader'
import DrawQuad from '../jirachi/drawquad'
import Icosphere from '../objects/Icosphere'
// shaders
import sim from '../shaders/sim.frag'
import sim1 from '../shaders/es1/sim.frag'

// post
import Composer from '../Composer'
import BloomPass from './BloomView'
import BlurPass from './BlurPass'

class PetalView extends Composer {
	constructor(gl,content,camera,numParticles){
		super(gl);

		// keep track of timing
		this.time = 0.0;

		// helps offset updates a bit
		this.count = 0;

		// reference to camera
		this.camera = camera;

		// number of particles
		this.numParticles = numParticles;

		// Petal mesh
		this.petals = new Petals(gl,content,numParticles);
		this.quad = new DrawQuad(gl);
		this.data = new DataView(gl,numParticles);

		this.shape = new Icosphere(gl);
		/**
		 * Simulation setup
		 * @type {DrawQuad}
		 */
		this.sim = new DrawQuad(gl,{
			fragment:loadContextConditionalShader(gl,{
				webgl1:sim1,
				webgl2:sim
			}),
			uniforms:[
				'texturePos',
				'textureVel',
				'textureExtra',
				'maxRadius'
			]
		});

		this.resolution = [window.innerWidth,window.innerHeight];
		this.petals.setNumInstances(this.numParticles);
		this.current = this.data.buffers[0];
		this.target = this.data.buffers[1];

		/**
		 * Just cause I'm lazy - post processing only for WebGL2 for now.
		 */
		if(this.gl instanceof WebGL2RenderingContext){
			this.blur = new BlurPass(this.gl);
			this.addLayer(this.blur);
			this.compileLayers();
		}

	}
	updateFbo(){
		let gl = this.gl;
		this.target.bind();


		gl.clearScreen();

		this.current.getTexture(1).bind(0);
		this.current.getTexture().bind(1);
		this.current.getTexture(2).bind(2);
		gl.setViewport(0,0,this.numParticles,this.numParticles);
		this.sim.draw(shader => {
			shader.setTextureUniform('texturePos',1);
			shader.setTextureUniform('textureVel',0);
			shader.setTextureUniform('textureExtra',2);
			shader.uniform('time',deltaTime)
			shader.uniform('maxRadius',40.5);
		});

		gl.bindTexture(gl.TEXTURE_2D,null);

		this.target.unbind();


		let tmp = this.current;
		this.current = this.target;
		this.target = tmp;

	}
	renderPetals(){
		let gl = this.gl;
		this.time += 0.05;
		this.count ++;

		if(this.count % 2 == 0) {
			this.count = 0;
			this.updateFbo();
		}


        this.bindView();
		gl.setViewport();

		gl.clearScreen(0,0,0,0)
		gl.disableBlending();
        this.target.getTexture().bind(0);
        this.target.getTexture().bind(1);
        this.target.getTexture(2).bind(2);
        this.petals.draw(this.camera,(shader)=>{
            shader.setTextureUniform("textureCurr",0);
            shader.setTextureUniform('textureNext',1);
            shader.setTextureUniform('textureExtra',2);
            shader.uniform('percent',this.count);
            shader.uniform('resolution',this.resolution);
            shader.uniform('time',deltaTime * 0.001);
            shader.uniform('scale',1.0);
        });


        gl.bindTexture(gl.TEXTURE_2D,null);

		this.unbindView();

	}

	update(){
        this.renderPetals();
	}
	draw(){
		let gl = this.gl;
		gl.setViewport();

		if(this.gl instanceof WebGL2RenderingContext) {
			//this.draw();
			this.run();
            this.drawComposedScene();

		}
	}
}

export default PetalView;