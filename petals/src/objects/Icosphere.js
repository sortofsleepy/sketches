import Mesh from '../jirachi/framework/Mesh'
import createIco from '../jirachi/framework/geometry/IcoSphere'
import vert from '../shaders/shape.vert'
import frag from '../shaders/shape.frag'
import {explode} from '../jirachi/utils/modfiers'
import {flattenArray} from '../jirachi/math/core'
class Icosphere extends Mesh {
    constructor(gl){
        super(gl,{
            vertex:vert,
            fragment:frag
        });

        let geo = createIco();



       let geo2 = explode(geo);
       console.log(geo2);
        this.addAttribute('position',geo2.vertices);
        this.addIndices(geo2.indices);

    }
}

export default Icosphere;