import {createFBO,createFBOAttachmentFormat} from '../jirachi/core/fbo'
import {createTexture2d,createTextureFormat} from '../jirachi/core/texture'
import {range} from '../jirachi/math/core'


class DataView {
	constructor(gl,numParticles = 100){

		this.gl = gl;
		this.flag = 0;

		this._buildTextures(numParticles);
	}

	_buildTextures(numParticles){
		let gl = this.gl;
		let positions = [];
		let coords = [];
		let indices = [];
		let extras = [];
		let count = 0;
		let textures1 = [];
		let textures2 = [];


		let totalParticles = numParticles * numParticles;
		console.debug('Total Particles : ', totalParticles);
		let ux, uy;
		let max = 3;

		for(let j = 0; j < numParticles; j++) {
			for(let i = 0; i < numParticles; i++) {
				positions.push(random(-max, max), random(-max, max), random(-max, max));

				ux = i / numParticles * 2.0 - 1.0 + .5 / numParticles;
				uy = j / numParticles * 2.0 - 1.0 + .5 / numParticles;

				extras.push(Math.random(), Math.random(), Math.random());
				coords.push(ux, uy);
				indices.push(count);
				count ++;
			}
		}

		// ======== BUILD INITIAL DATA ============== //
		//============= SETUP COLORS ================= //
		let colors = new Float32Array(numParticles * numParticles * 4);
		positions.forEach((itm,idx) => {
			colors[idx] = itm;
		})
		textures1.push(createTexture2d(gl,{
			width:numParticles,
			height:numParticles,
			data:colors,
			floatingPoint:true
		}))
		textures2.push(createTexture2d(gl,{
			width:numParticles,
			height:numParticles,
			data:colors,
			floatingPoint:true
		}));
		//============= SETUP (black?) ================= //
		let black = new Float32Array(numParticles * numParticles * 4);
		let b_len = black.length;
		for(let i = 0; i < b_len; i += 4){
			black[i] = 0.0;
			black[i + 1] = 0.0;
			black[i + 2] = 0.0;
			black[i + 3] = 1.0;
		}
		textures1.push(createTexture2d(gl,{
			width:numParticles,
			height:numParticles,
			floatingPoint:true,
			data:black
		}))
		textures2.push(createTexture2d(gl,{
			width:numParticles,
			height:numParticles,
			floatingPoint:true,
			data:black
		}));
		// ================ SETUP EXTRA ================= //
		let extra = new Float32Array(numParticles * numParticles * 4);
		extras.forEach((itm, idx) => {
			extra[idx] = itm;
		})
		textures1.push(createTexture2d(gl,{
			width:numParticles,
			height:numParticles,
			floatingPoint:true,
			data:extra
		}))
		textures2.push(createTexture2d(gl,{
			width:numParticles,
			height:numParticles,
			floatingPoint:true,
			data:extra
		}));

		this.textures = textures1;
		this.textures2 = textures2;
		this._buildBuffers();
	}


	_buildBuffers(){
		let gl = this.gl;

		this.buffers = [
			createFBO(gl,{
				format:createFBOAttachmentFormat(gl,{
					textures:this.textures
				})
			}),
			createFBO(gl,{
				format:createFBOAttachmentFormat(gl,{
					textures:this.textures2
				})
			})
		]

		this.buffers[0].bind();
		this.buffers[0].drawBuffers()
		this.buffers[0].unbind();

		this.buffers[1].bind();
		this.buffers[1].drawBuffers()
		this.buffers[1].unbind();
	}

}

export default DataView;