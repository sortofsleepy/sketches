import PetalView from './views/PetalView'
import RenderView from './views/RenderView'
class Boot {
	constructor(gl,camera){

		this.gl = gl;
		this.camera = camera;
		let numParticles = 500;
		this.lastUpdate = 0;
		this.numParticles = numParticles;

	}


	updateTime(){
		let now = Date.now();
		let dt = now - this.lastUpdate;
		this.lastUpdate = now;
		window.deltaTime = dt;
	}

	setup(content){
		let gl = this.gl;
		this.petals = new PetalView(gl,content,this.camera,this.numParticles);
		//this.render = new RenderView(gl,this.camera);
	}

	draw(){
		let gl = this.gl;
		this.updateTime();
		this.petals.update();

		gl.clearTransparent()
		gl.enableAlphaBlending()
		this.petals.draw();
		//this.render.draw();

	}
}

export default Boot;