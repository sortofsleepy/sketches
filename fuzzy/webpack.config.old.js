var PORT = 9000;
var ROOT = process.cwd();
var path = require("path")
var webpack = require("webpack")
module.exports = {
    entry: [
        "webpack/hot/dev-server",
        `webpack-dev-server/client?http://localhost:${PORT}/`,
        `${__dirname}/src/App.js`
    ],
    css:{
        srcDir:`${__dirname}/css`,
        destDir:`${__dirname}/public/css`,
        entry:`main.css`,
        dest:`main.css`,
        plugins:[]
    },
    output:{
        path:__dirname + "/public/js",
        filename:"App.js"
    },
    module:{
        loaders:[
            {
                test:/\.js$/,
                exclude:/(node_modules|bower_components)/,
                loader:'babel-loader',
                query:{
                    presets:['babel-preset-es2015']
                }
            },
            {
                test:/\.js$/,
                exclude:/(node_modules|bower_components)/,
                loader:`kantan/lib/devsetup.js`,
                query:{
                    css:"../css/main.css"
                }
            },
            { test: /\.(glsl|frag|vert)$/, loader: 'raw', exclude: /node_modules/ },
            { test: /\.(glsl|frag|vert)$/, loader: 'glslify', exclude: /node_modules/ }
        ],
        resolve:{
            extensions:['','.js','.jsx']
        }
    },
    plugins:[]
}