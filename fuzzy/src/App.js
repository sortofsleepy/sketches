import {createRenderer} from './libs/jirachi/core/gl'
import {PerspectiveCamera,fullscreenAspectRatio,setZoom} from './framework/Camera'
import {createFBO} from './libs/jirachi/core/fbo'
import {createQuad} from './libs/jirachi/quad'

// post processing
import FilmGrain from './post/FilmGrain'
import HBlur from './post/HBlur'
import VBlur from './post/VBlur'
import AbrasionPass from './post/AbrasionPass'
import Composer from './libs/jirachi/post/Composer'


// import environment
import Env from './objects/Env'
import Grid from './objects/Grid'


const controls = orbitControls({
    position:[0,0,-210]
})

// gl renderer
const gl = createRenderer();

// main camera
var camera = PerspectiveCamera(Math.PI / 4,fullscreenAspectRatio(),0.1,10000);
camera = setZoom(camera,-210);

// main scene FBO
const scene = createFBO(gl,{
    width:window.innerWidth,
    height:window.innerHeight,
    floatingPoint:true,
    depthTexture:true
})

const sceneQuad = createQuad(gl,{
    withTexture:true
});


const grain = new FilmGrain(gl);
const hblur = new HBlur(gl);
const vblur = new VBlur(gl);
const asci = new AbrasionPass(gl);
const composer = new Composer(
    gl,
    scene.getTexture(),
    grain,
    hblur,
    vblur,
    asci
);


var anchor = document.querySelector("#GL_LAYER");
gl.setFullscreen().attachToScreen(anchor);
var env = new Env(gl);
var grid = new Grid(gl);

animate();
function animate(){
    window.requestAnimationFrame(animate);
    gl.clearScreen(0,0,0,1);
    controls.update();
    camera.update(controls.position,controls.direction,controls.up);



    scene.bind();
    gl.clearScreen(0,0,0,0);
    env.draw(camera);
    grid.draw(camera);
    scene.unbind();


    composer.run();

    gl.bindTexture(gl.TEXTURE_2D,null);

    composer.getOutput().bindTexture();
    sceneQuad.draw();


}
