import FxPass from '../libs/jirachi/post/FxPass'
import blur from '../shaders/post/blur.glsl'

class VBlur extends FxPass{
    constructor(gl){
        super(gl,blur,{
            uniformMap:[
                'direction'
            ]
        });
    }
    runPass(){

        this.fbo.bind();
        this.gl.clearScreen();
        if(this.input !== null){
            this.input.bind();
        }
        this.drawQuad.drawWithCallback(shader => {
            shader.uniform('resolution',this.resolution);
            shader.uniform('direction',[0.0,1.0]);
            if(this.input !== null){
                shader.setTextureUniform('tex0',0);
            }
        })
        this.fbo.unbind();
    }
}

export default VBlur;