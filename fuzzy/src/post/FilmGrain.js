import FxPass from '../libs/jirachi/post/FxPass'
import periodic from '../libs/jirachi/shaders/noise/periodic3d.glsl'
import simplex from '../libs/jirachi/shaders/noise/noise3d.glsl'
import grain from '../shaders/post/grain.glsl'
import common from '../libs/jirachi/shaders/noise/common.glsl'


var shader = [common,simplex,periodic,grain];
shader = shader.map(itm => {
    itm = itm.replace("#define GLSLIFY 1","");
    return itm;
})

class FilmGrain extends FxPass{
    constructor(gl){
        super(gl,shader);
    }
}

export default FilmGrain;