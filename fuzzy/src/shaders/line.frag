#extension GL_OES_standard_derivatives : enable
precision mediump float;

uniform sampler2D map;
uniform float useMap;
uniform float useDash;
uniform vec2 dashArray;
uniform float visibility;
uniform float alphaTest;

varying vec2 vUV;
varying vec4 vColor;
varying vec3 vPosition;
varying float vCounters;

void main() {

    vec4 c = vColor;
	 if( c.a < alphaTest ) discard;
    if( useMap == 1. ) c *= texture2D( map, vUV );
	 if( useDash == 1. ){

	 }
    gl_FragColor = c;
	 gl_FragColor.a *= step(vCounters,visibility);
}