precision highp float;

uniform sampler2D sphereData;
uniform sampler2D speedData;

varying vec2 uv;

void main() {

    // phi is x, theta is y
    vec4 data = texture2D(sphereData, gl_FragCoord.xy / vec2(128.0));
    vec4 speeds = texture2D(speedData, gl_FragCoord.xy / vec2(128.0));

    data.x += speeds.x * 0.005;
    data.y += speeds.y * 0.005;

    if(data.x >= 10.0){
        data.x *= 0.05;
    }

    if(data.y >= 10.0){
        data.y *= 0.05;
    }
    if(data.z >= 10.0){
        data.z *= 0.05;
    }


    gl_FragData[0] = data;
}
