#extension GL_OES_standard_derivatives : enable
precision highp float;
varying vec3 vPosition;
varying vec3 vEyeDir;
uniform float time;
uniform sampler2D uGradient;
uniform vec2 resolution;
void main() {
    vec2 uv = gl_FragCoord.xy / resolution.xy;
    vec4 grad = texture2D(uGradient,uv);

    vec2  px = 4.0*(-resolution.xy + 2.0* gl_FragCoord.xy) / resolution.y;

    float id = 20.5 + 0.5 * cos(time + sin(dot(floor(px+0.5),vec2(113.1,17.81)))*43758.545);

    vec3  co = 0.5 + 0.5*cos(time + 3.5*id + vec3(0.0,1.57,3.14) );

    vec2  pa = smoothstep( 0.0, 0.2, id*(0.5 + 0.5*cos(6.2831*px)) );
    pa = pow(pa,uv + resolution);
    vec4 color = vec4( co - pa.x + pa.y, 0.8 );
    vec4 mixedColor = mix(grad,color,0.6);

    vec4 texColor = mixedColor + vec4(vPosition,1.);
    vec4 finalColor = mix(texColor,vec4(uv,0.0,1.0),0.5);
   gl_FragColor = finalColor;
}
