
uniform mat4 projectionMatrix;
uniform mat4 modelMatrix;
uniform mat4 viewMatrix;
uniform vec3 eyeDir;
uniform float time;

uniform sampler2D sphereData;

attribute vec3 position;
attribute vec3 offsets;

varying vec3 vPosition;
varying vec3 vEyeDir;
const float r = 50.0;
void main(){

    vec4 pos = vec4(position,1.);

    vec4 sphere = texture2D(sphereData,offsets.xy);
    float n = noise3d(offsets);

    float x = cos(sphere.y) * sin(sphere.x) * r;
    float y = sin(sphere.y) * sin(sphere.x) * r;
    float z = cos(sphere.x) * r;


    vec3 circ = vec3(x,y,z) + vec3(n);





    vec3 offs = offsets;
    offs += circ;


    vec4 finalPos = pos + vec4(offs,1.);



    gl_Position = projectionMatrix * viewMatrix * modelMatrix * vec4(finalPos);

    vEyeDir = eyeDir;
    vPosition = offsets;
}
