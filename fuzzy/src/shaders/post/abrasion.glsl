precision highp float;

uniform float time;
uniform vec2 resolution;
uniform sampler2D uScene;
varying vec2 uv;

// based on this shadertoy
// https://www.shadertoy.com/view/Mds3zn

void main(){
    vec4 scene = texture2D(uScene,uv);
    vec2 vUv = gl_FragCoord.xy / resolution.xy;
    float amount = 0.0;

    amount = (1.0 + sin(time*6.0)) * 0.5;
    amount *= 1.0 + sin(time*6.0) * 0.5;
    amount *= 1.0 + sin(time*19.0) * 0.5;
    amount *= 1.0 + sin(time*2.0) * 0.5;

    amount *= 0.05;
    scene.r = texture2D( uScene, vec2(uv.x + amount,uv.y) ).g;
    scene.g = texture2D( uScene, uv ).g;
    scene.b = texture2D( uScene, vec2(uv.x - amount,uv.y) ).b;

    scene *= (1.0 - amount * 0.5);
        	
    gl_FragColor = scene;
}