precision highp float;
uniform mat4 projectionMatrix, modelMatrix, viewMatrix;
uniform float time;

attribute vec3 position;
attribute vec3 normal;
attribute vec3 offset;
attribute vec2 uv;

varying vec3 vOffset;
varying vec3 vNormal;
varying vec2 vUv;
varying vec3 vPosition;
varying float vScale;


void main () {
    vec3 trTime = vec3(offset.x + time,offset.y + time,offset.z + time);
    float scale =  sin( trTime.x * 2.1 ) + sin( trTime.y * 3.2 ) + sin( trTime.z * 4.3 );
    scale = (sin(time) * (offset.x + offset.y + offset.z) * scale)  * 10.0;

    scale = clamp(scale,0.5,5.0);
    gl_PointSize = scale;

    gl_Position = projectionMatrix * viewMatrix * modelMatrix * vec4(position, 1.0);

    vScale = scale;
    vUv = uv;
    vNormal = normal;
    vOffset = offset;
    vPosition = position;
}
