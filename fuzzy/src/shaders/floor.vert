precision highp float;
uniform vec3 eyeDir;
uniform mat4 projectionMatrix;
uniform mat4 modelMatrix;
uniform mat4 viewMatrix;
uniform float time;

attribute vec3 position;
attribute vec2 uv;
attribute vec3 normal;

varying vec3 vPosition;
varying vec2 vUv;
varying vec3 vNormal;
varying vec3 vEyeDir;

const vec2 TERRAIN_OFFSET_POSITION = vec2(10.0);
const float LARGE_TERRAIN_HEIGHT = 0.3;
const float LARGE_TERRAIN_SCALE = 2.0;
const float SMALL_TERRAIN_HEIGHT = 0.05;
const float SMALL_TERRAIN_SCALE = 7.0;
const float SMALLEST_TERRAIN_HEIGHT = 0.02;
const float SMALLEST_TERRAIN_SCALE = 13.0;
const float TERRAIN_SPEED = 0.02;
const float EPSILON = 0.01;

vec3 terrain(vec2 coordinate) {
  coordinate.x = (coordinate.x) - 0.5;
  float divet = 0.3 * clamp(-1.0, 0.0, -1.0 + length(4.0 * (coordinate + vec2(0.0, 0.1))));


  vec3 val = vec3(
                  coordinate.x,
                  LARGE_TERRAIN_HEIGHT
                     * coordinate.x
                     * noise3d(vec3(coordinate * LARGE_TERRAIN_SCALE + TERRAIN_OFFSET_POSITION, sin(time) + time * TERRAIN_SPEED)) +
                   SMALL_TERRAIN_HEIGHT
                     * coordinate.x
                     * noise3d(vec3(coordinate * SMALL_TERRAIN_SCALE + TERRAIN_OFFSET_POSITION, cos(time) * TERRAIN_SPEED)) +
                   SMALLEST_TERRAIN_HEIGHT
                     * coordinate.y
                     * noise3d(vec3(coordinate * SMALLEST_TERRAIN_SCALE + TERRAIN_OFFSET_POSITION, time * TERRAIN_SPEED)),
                   coordinate.y
                     );

  return val;
}
      vec3 calculateNormal(vec3 cartesian, vec2 coordinate) {
      	vec3 tangent = normalize(terrain(vec2(coordinate.x, coordinate.y + EPSILON)) - cartesian);
      	vec3 binormal = normalize(terrain(vec2(coordinate.x + EPSILON, coordinate.y)) - cartesian);
      	return cross(tangent, binormal);
      }

void main () {
    vec3 pos = position;

    vec2 coordinate = pos.xz;
    vec3 cartesian = terrain(coordinate);
    cartesian.z -= 0.5;



    gl_Position = projectionMatrix * viewMatrix * modelMatrix * vec4(cartesian, 1.0);


    vPosition = cartesian;
    vEyeDir = normalize(eyeDir - vPosition);
    vUv = uv;
    vNormal = normal;
}
