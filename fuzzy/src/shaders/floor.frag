precision highp float;
const vec4 lightColor = vec4(1.0,0.5,0.0,1.0);

uniform sampler2D envTex;

varying vec3 vPosition;
varying vec3 vNormal;
varying vec2 vUv;
varying vec3 vEyeDir;
void main(){

   float alpha = min(0.2,
         30.2 * (0.5 - length(2.0 * vUv - vec2(1.0)))
    );

    float ppEyeDiff = max( dot( vNormal, vEyeDir ), 0.0 );
    float ppEyeFres = pow( 1.0 - ppEyeDiff, 2.0 );
    vec3 reflectDir = reflect( vEyeDir, vNormal * vec3( 1.0, 1.5, 1.0 ) );

    vec3 envColor = texture2D( envTex, reflectDir.xy ).rgb;
    vec3 envSolid = vec3(1.);

    float envGrey = envColor.r;
    float envSpec = envColor.g;
    float bloomShadow = vPosition.y * 0.005 + 0.8;
    float reflectionRim = envGrey * ppEyeFres * 4.3;
    float fres = ppEyeFres * 2.2;
    float lighting = ( envSpec - ppEyeFres ) * 0.1;
    float centerGlow = ppEyeDiff * 0.4 * bloomShadow;

    // final terain color
    vec3 col = vec3( pow(reflectionRim,5.0) + fres * pow(centerGlow,20.0));

    vec4 finalColor = vec4(mix(col,envColor,0.8),alpha);
    finalColor = mix(finalColor,vec4(envColor,0.5),alpha);
    gl_FragColor = finalColor;
 }