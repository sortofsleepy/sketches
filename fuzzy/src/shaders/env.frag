precision highp float;
uniform sampler2D uGradient;
varying vec2 vUv;
varying vec4 vColor;
void main(){
    vec4 grad = texture2D(uGradient,vUv);

    gl_FragColor = vec4(0.1,0.1,0.1,1.0);
}