precision highp float;
uniform float time;
uniform sampler2D uGradient;
uniform sampler2D uParticle;
uniform vec2 resolution;

varying float vScale;
varying vec2 vUv;
varying vec3 vPosition;
// based off of https://www.shadertoy.com/view/4dBSRK
void main() {

    vec4 pTex = texture2D(uParticle,gl_PointCoord);
    vec4 grad = texture2D(uGradient,gl_PointCoord);

    vec2  px = 4.0*(-resolution.xy + 2.0* gl_FragCoord.xy) / resolution.y;
    
    float id = 0.5 + 0.5*cos(time + sin(dot(floor(px+0.5),vec2(113.1,17.81)))*43758.545);
    
    vec3  co = 0.5 + 0.5*cos(time + 3.5*id + vec3(0.0,1.57,3.14) );
    
    vec2  pa = smoothstep( 0.0, 0.2, id*(0.5 + 0.5*cos(6.2831*px)) );
    vec4 color = vec4( co - pa.x + pa.y, 0.8 );
    vec4 mixedColor = mix(grad,color,1.0) * vScale;

    vec4 texColor = vec4(vec3(1.0 - pTex.r, 1.0 - pTex.g, 1.0 - pTex.b) * mixedColor.xyz,pTex.a);

    gl_FragColor = texColor;
}
