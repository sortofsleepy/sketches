import {createFBO} from '../libs/jirachi/core/fbo'
import {createQuad} from '../libs/jirachi/quad'

/**
 * A container for a layer in your "Scene"
 * Backed by an FBO so you can do a little post-processing, though
 * you are currently limited to one pass every frame update.
 */
class View {
    constructor(gl,{
        width=window.innerWidth,
        height=window.innerHeight,
        post="",
        uniforms=[]}={}){
        this.gl = gl;
        this.buffer = createFBO(gl,{
            width:width,
            height:height,
            depthTexture:true
        });

        /**
         * If post processing is enabled on the view,
         * make sure to setup the quad with that shader.
         */
        if(post !== ""){
            this.postFx = true;
            this.quad = createQuad(gl,{
                withTexture:true,
                fragmentShader:post,
                uniforms:uniforms
            })
        }else{
            this.quad = createQuad(gl,{
                withTexture:true
            })
        }

    }

    bind(){
        this.buffer.bind();
    }

    unbind(){
        this.buffer.unbind();
    }

    getBuffer(){
        return this.buffer;
    }

    draw(unit=0,cb=null){
        this.buffer.bindTexture(unit);
        if(cb !== null){
            this.quad.drawWithCallback(cb);
        }else{
            this.quad.draw(unit);
        }
        this.buffer.unbindTexture();
    }

    /**
     * Helper function in case you decide to override the draw command above.
     */
    drawView(unit=0){
        this.buffer.bindTexture(unit);
        this.quad.draw(unit);
        this.buffer.unbindTexture();
    }
}

export default View;