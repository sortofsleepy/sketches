import {createTexture2d} from '../libs/jirachi/core/texture'

class ImageLoader {
    constructor(list = [],cb){
        this.queue = list;
        this.cb = cb;

        // note - this will end up in bytes
        this.bundleSize = 0;

        // request object
        this.req = new XMLHttpRequest();

        this._getSizes(() => {
            this._startLoad();
        });
    }

    /**
     * A simplified loading interface that just takes an array of images and spits out simple objects with
     * constructed textures. Doesn't do checking of any kind though.
     * @param gl a WebGLRendingContext
     * @param images the array of images. Or it can just be a string for one image.
     * @param waitTillQueueComplete option to simply pass back an array of images before or after
     * all of them have finished loading.
     * @returns {*}
     * @constructor
     */
    static SimpleLoader(gl,images,cb=null){
        let results = [];
        if(images instanceof Array && cb === null){
            images.forEach(img => {
                let itm = new Image();
                itm.src = img;
                itm["isLoaded"] = false;
                let result = {
                    path:img,
                    obj:itm,
                    checkLoaded:function(){
                        if(this.obj.isLoaded && !this.textureCreated){
                            this.texture = createTexture2d(gl,{
                                data:this.obj,
                                width:this.obj.width,
                                height:this.obj.height
                            });

                            this.textureCreated = true;
                        }
                        return this.obj.isLoaded;
                    }
                }

                result.obj.onload = function(){
                    this.isLoaded = true;
                }
                results.push(result)
            })

            return results;
        }else if (images instanceof Array && cb !== null){
            let numImages = images.length;
            images.forEach(path => {
                let img = new Image();
                img.src = path;
                img.onload = function(){
                    let texture = createTexture2d(gl,{
                        data:this,
                        width:this.width,
                        height:this.height
                    });

                    results.push(texture);

                }
            })

            let timer = setInterval(() => {
                if(results.length === numImages){
                    cb(results);
                    clearInterval(timer);
                }
            })

        }


        if (images instanceof String){
            let itm = new Image();
            itm.src = img;
            itm["isLoaded"] = false;
            let result = {
                path:img,
                obj:itm,
                checkLoaded:function(){
                    return this.obj.isLoaded;
                }
            }

            return result;
        }
    }

    /**
     * Pulls the total file size of all the images passed to the loader in bytes
     * @private
     */
    _getSizes(cb){
        let queue = this.queue;
        let len = this.queue.length;
        let imgRegEx = /.(jpg|gif|png)/;
        let req = this.req;
        let self = this;

        for(var i = 0; i < len; ++i){
            let item = queue[i];
            if(item.match(imgRegEx)){
                req.open("HEAD",item,true);
                req.onreadystatechange = function(){
                    if(this.readyState === this.DONE){
                        self.bundleSize += parseInt(req.getResponseHeader("Content-Length"));
                    }
                }

                req.send();
            }
        }
    }

    /**
     * Starts loading of the queue.
     * @private
     */
    _startLoad(){
        let queue = this.queue;
        let len = this.queue.length;
        let imgRegEx = /.(jpg|gif|png)/;
        let loaded = [];

        for(var i = 0; i < len; ++i){
            let item = queue[i];
            if(item.match(imgRegEx)){

            }
        }
    }
}

export default ImageLoader;