
import vert from '../shaders/line.vert'
import frag from '../shaders/line.frag'
import Mesh from '../framework/Mesh'
import {flattenArray} from '../libs/jirachi/math/core'

// Ported from Three.MeshLine
class MeshLine extends Mesh{
    constructor(gl){
        super(gl);

        this.positions = [];
        this.previous = [];
        this.next = [];
        this.side = [];
        this.width = [];
        this.indices_array = [];
        this.uvs = [];
        this.counters = [];
        this.vertex = vert;
        this.fragment = frag;

        this.setShader({
            vertex:vert,
            fragment:frag
        });
    }

    compareV3(a,b){
        var aa = a * 6;
        var ab = b * 6;
        return ( this.positions[ aa ] === this.positions[ ab ] ) && ( this.positions[ aa + 1 ] === this.positions[ ab + 1 ] ) && ( this.positions[ aa + 2 ] === this.positions[ ab + 2 ] );
    }

    copyV3(a){
        var aa = a * 6;
        return [ this.positions[ aa ], this.positions[ aa + 1 ], this.positions[ aa + 2 ] ];
    }

    setGeometry(g){
        for( var j = 0; j < g.length; j += 3 ) {
            var c = j/g.length;
            this.positions.push( g[ j ], g[ j + 1 ], g[ j + 2 ] );
            this.positions.push( g[ j ], g[ j + 1 ], g[ j + 2 ] );
            this.counters.push(c);
            this.counters.push(c);
        }

        this.addAttribute('position',this.positions);
        this.addAttribute('counters',this.counters);
    }
    advance(){

    }

    process(){
        let l = this.positions.length / 6;

        this.previous = [];
        this.next = [];
        this.side = [];
        this.width = [];
        this.indices_array = [];
        this.uvs = [];

        for( var j = 0; j < l; j++ ) {
            this.side.push( 1 );
            this.side.push( -1 );
        }

        var w;
        for( var j = 0; j < l; j++ ) {
            let w = 1;
            this.width.push( w );
            this.width.push( w );
        }

        for( var j = 0; j < l; j++ ) {
            this.uvs.push( j / ( l - 1 ), 0 );
            this.uvs.push( j / ( l - 1 ), 1 );
        }

        var v;

        if( this.compareV3( 0, l - 1 ) ){
            v = this.copyV3( l - 2 );
        } else {
            v = this.copyV3( 0 );
        }
        this.previous.push( v[ 0 ], v[ 1 ], v[ 2 ] );
        this.previous.push( v[ 0 ], v[ 1 ], v[ 2 ] );
        for( var j = 0; j < l - 1; j++ ) {
            v = this.copyV3( j );
            this.previous.push( v[ 0 ], v[ 1 ], v[ 2 ] );
            this.previous.push( v[ 0 ], v[ 1 ], v[ 2 ] );
        }

        for( var j = 1; j < l; j++ ) {
            v = this.copyV3( j );
            this.next.push( v[ 0 ], v[ 1 ], v[ 2 ] );
            this.next.push( v[ 0 ], v[ 1 ], v[ 2 ] );
        }

        if( this.compareV3( l - 1, 0 ) ){
            v = this.copyV3( 1 );
        } else {
            v = this.copyV3( l - 1 );
        }
        this.next.push( v[ 0 ], v[ 1 ], v[ 2 ] );
        this.next.push( v[ 0 ], v[ 1 ], v[ 2 ] );

        for( var j = 0; j < l - 1; j++ ) {
            var n = j * 2;
            this.indices_array.push( n, n + 1, n + 2 );
            this.indices_array.push( n + 2, n + 1, n + 3 );
        }

        this.addAttribute('previous',flattenArray(this.previous));
        this.addAttribute('next',flattenArray(this.next));
        this.addAttribute('uv',flattenArray(this.uvs));
        this.addAttribute('side',this.side);
        this.addAttribute('width',this.width);
        this.addIndices(this.indices_array);

    }
}

export default MeshLine;