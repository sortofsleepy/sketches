import Mesh from '../framework/Mesh'
import {createList,unrollMap} from '../framework/math/core'
import {randFloat,toRadians} from '../libs/jirachi/math/core'
import {createMultiPingpongBuffer,generatePingpongTexture} from '../libs/jirachi/gpu/pingpong'
import vert from '../shaders/spaceship/spaceship.vert'
import frag from '../shaders/spaceship/spaceship.frag'
import sim from '../shaders/spaceship/position.glsl'
import noise from '../libs/jirachi/shaders/noise3d.glsl'
class SpaceShips extends Mesh{
    constructor(gl){
        super(gl);

        this.numShips = 400;

        // rotate so centerpoint is hidden by floor
        this.rotateX(toRadians(90))

        // set the shader. SphereData is an additional uniform to keep track of.
        this.setShader({
            vertex:[noise,vert],
            fragment:frag,
            uniforms:[
                'sphereData',
                'eyeDir',
                'resolution',
                'uGradient'
            ]
        })

        this.time = 0.0;
        this._build();
    }

    draw(camera,gradient){
        let gl = this.gl;
        this.time += 0.01;
        this.rotateY(0.002);
        if(this.shaderSet){

            this.shader.bind();
            gl.disable(gl.BLEND);

            if(gradient !== undefined){
                gradient.bind(2);
                this.shader.setTextureUniform('uGradient',2);

            }
            this.buffer.bindTextures();
            // bind default uniforms
            this.shader.uniform('time',this.time);
            this.shader.set4x4Uniform('projectionMatrix',camera.projection);
            this.shader.set4x4Uniform('viewMatrix',camera.view);
            this.shader.uniform("modelMatrix",this.model);
            this.shader.setTextureUniform("sphereData",0);
            this.shader.uniform('eyeDir',camera.eye);
            this.shader.uniform('resolution',[window.innerWidth,window.innerHeight]);

            // bind vao
            this.vao.bind();
            this.gl.drawInstancedElements(this.mode,this.numVertices,this.numInstances);
            // unbind vao
            this.vao.unbind();
            this.buffer.unbindTextures();
        }
    }

    _build(){
        let gl = this.gl;
        let num = this.numShips;

        this._buildShape();

        // setup positions for all of the spaceships, as well as phi, theta attributes
        let positions = createList(num,true);
        var azimuth = 256 * PI / num;
        var inclination = PI / num;
        var radius = 50.0;


        let sphereData = new Float32Array(128 * 128 * 4);
        let speeds = new Float32Array(128 * 128 * 4);

        // setup sphere data
        let datalen = sphereData.length;
        for(var i = 0; i < datalen; i += 4){
            sphereData[i] = PI * Math.random();
            sphereData[i + 1] = PI * Math.random();
        }

        // setup speed data
        let speedlen = speeds.length;
        for(var i = 0; i < speedlen; i += 4){
            let v1 = randFloat(-0.5,0.5);
            let v2 = randFloat(-0.5,0.5);

            if(v1 > 0){
                v1 -= 1;
            }

            if (v2 > 0){
                v2 -= 1;
            }
            speeds[i] = v1;
            speeds[i + 1] = v2;
        }


        let data = generatePingpongTexture(gl,sphereData);
        let sData = generatePingpongTexture(gl,speeds);

        let buffer = createMultiPingpongBuffer(gl,sim,{
            textureMap:[
                data,
                sData
            ],
            uniformMap:[
                'sphereData',
                'speedData'
            ]
        });



        this.addInstancedAttribute('offsets',unrollMap(positions))

        this.setNumInstances(positions.length);

        this.buffer = buffer;
    }

    update(){
        this.buffer.update((shader) => {
            shader.setTextureUniform('sphereData',0);
            shader.setTextureUniform('speedData',1);
        });
    }

    /**
     * Builds the base shape for each spaceship, just a pyramid.
     * @private
     */
    _buildShape(){
        var vertices = [
            1,  1,  1,   - 1, - 1,  1,   - 1,  1, - 1,    1, - 1, - 1
        ];

        var indices = [
            2,  1,  0,    0,  3,  2,    1,  3,  0,    2,  3,  1
        ];


        this.addAttribute('position',vertices);
        this.addIndices(indices);

    }
}

export default SpaceShips;