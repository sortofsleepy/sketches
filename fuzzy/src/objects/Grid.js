// core components
import View from '../framework/View'
import Mesh from '../framework/Mesh'

// objects in the view
import SpeakerSphere from './StarsSphere'
import ImageLoader from '../framework/ImageLoader'

import Floor from '../objects/Floor'
import SpaceShips from './SpaceShips'
// import shaders
import blur from '../shaders/post/blur.glsl'

class Grid extends View {
    constructor(gl,options){
        super(gl,{
            post:blur,
            uniforms:[
                'uScene',
                'resolution'
            ]
        });


        this.textures = ImageLoader.SimpleLoader(gl,[
            '/textures/spark1.png',
            '/textures/gradient.png'
        ],(results) => {
            // allow drawing
            this.ready = true;
            this.particleTexture = results[0];
            this.gradient = results[1];

        })


        this.sphere = new SpeakerSphere(gl,{
            mode:gl.POINTS,
            uniforms:[
                'time',
                'uParticle',
                'resolution'
            ]
        });


        this.floor = new Floor(gl,{
            width:2,
            height:2,
            wSegments:250,
            hSegments:250
        });


        this.ships = new SpaceShips(gl);

        this.ready = false;
        this.angle = 0.0;

    }

    draw(camera){
        let gl = this.gl;
        this.angle += 0.01;
        if(this.ready){

            this.sphere.update();
            // blend the particle sphere

            gl.enable(gl.DEPTH_TEST);
            gl.setBlendFunction("SRC_ALPHA","ONE_MINUS_SRC_ALPHA");
            this.particleTexture.bind(1)
            this.gradient.bind(2);
            this.sphere.draw(camera,shader => {
                shader.uniform('time',this.angle);
                shader.uniform('resolution',[window.innerWidth,window.innerHeight])
                shader.setTextureUniform('uParticle',1);
                shader.setTextureUniform('uGradient',2);
            })


            gl.bindTexture(TEXTURE_2D,null);
            // disable blending and enable depth checking so "stars" don't mix with the land.



            gl.disable(gl.BLEND);
            gl.enable(gl.DEPTH_TEST);
            this.floor.draw(camera);
            this.floor.update(camera,this.gradient);

            this.ships.draw(camera,this.gradient);
            this.ships.update();

            gl.enable(gl.BLEND);
            gl.disable(gl.DEPTH_TEST);
        }
    }

}

export default Grid;