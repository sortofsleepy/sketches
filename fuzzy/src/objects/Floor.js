import Mesh from '../framework/Mesh'
import {unrollMap} from '../framework/math/core'
import vert from '../shaders/floor.vert'
import frag from '../shaders/floor.frag'
import noise3d from '../libs/jirachi/shaders/noise3d.glsl'
import rotate from '../libs/jirachi/shaders/rotation.glsl'
import {toRadians} from '../libs/jirachi/math/core'

/**
 * Mixing a few things together
 * https://github.com/vorg/primitive-plane.
 * https://github.com/gregtatum/sessions/blob/gh-pages/008/terrain.js
 *
 * with styling changes
 */
class Floor extends Mesh {
    constructor(gl,{width,height,wSegments,hSegments,options}={}){
        super(gl,options);
        this.setShader({
            vertex:[noise3d,vert],
            fragment:[rotate,frag],
            uniforms:[
                'eyeDir',
                'envTex',
                'time'
            ]
        })


        this.translate(0,-100,0);
        this.rotateY(toRadians(45))
        this.scaleModel(200,200,200);

        this._build(width,height,wSegments,hSegments,options);
        this.time = 0.0;
    }

    update(camera,env){
        let gl = this.gl;
        let shader = this.shader;
        this.time += 0.02;

        gl.enable(gl.DEPTH_TEST);
        env.bind(0);

        shader.uniform('time',this.time);
        shader.setTextureUniform('envTex',0);
        shader.uniform('eyeDir',camera.eye);
        env.unbind();

        this.rotateY(0.002);

    }

    _build(sx, sy, nx, ny, options){
        let gl = this.gl;
        sx = sx || 1
        sy = sy || 1
        nx = nx || 1
        ny = ny || 1
        var quads = (options && options.quads) ? options.quads : false

        var positions = []
        var uvs = []
        var normals = []
        var cells = []

        for (var iy = 0; iy <= ny; iy++) {
            for (var ix = 0; ix <= nx; ix++) {
                var u = ix / nx
                var v = iy / ny
                var x = -sx / 2 + u * sx // starts on the left
                var y = sy / 2 - v * sy // starts at the top
                positions.push([x, y, 0])
                uvs.push([u, v])
                normals.push([0, 0, 1])
                if (iy < ny && ix < nx) {
                    if (quads) {
                        cells.push([iy * (nx + 1) + ix, (iy + 1) * (nx + 1) + ix, (iy + 1) * (nx + 1) + ix + 1, iy * (nx + 1) + ix + 1])
                    } else {
                        cells.push([iy * (nx + 1) + ix, (iy + 1) * (nx + 1) + ix + 1, iy * (nx + 1) + ix + 1])
                        cells.push([(iy + 1) * (nx + 1) + ix + 1, iy * (nx + 1) + ix, (iy + 1) * (nx + 1) + ix])
                    }
                }
            }
        }

        positions.forEach(position => {
            const [x, z, y] = position
            position[0] = x + 0.5
            position[1] = y
            position[2] = z + 0.5
        })

        // build out data for everything
        this.addAttribute('position',unrollMap(positions));
        this.addAttribute('uv',unrollMap(uvs));
        this.addAttribute('normal',unrollMap(normals));
        this.addIndices(unrollMap(cells));
    }
}

export default Floor;