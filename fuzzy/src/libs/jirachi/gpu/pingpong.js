import {createFBO,createFBOWithAttachments} from '../core/fbo'
import {createQuad,drawQuad} from '../quad'
import {createTexture2d} from '../core/texture'


/**
 * Helper function to generate textures suitable for ping-ponging
 * @param gl {WebGLRenderingContext} a webgl context
 * @param data {Array or TypedArray} data for the texture. can either be a typed array or a regular array
 * @param width {Number} width for the texture. Defaults to 128
 * @param height {Number} height for the texture. Defaults to 128
 * @param type {Number} the datatype to use for the texture. Defaults to Floating point(gl.FLOAT)
 * @returns {*[]} an array of two textures built from the provided data
 */
export function generatePingpongTexture(gl,data=null,{width=128,height=128,type=5126}={}){
    if(data === null){
        console.error("generatePingpongTexture error - Need to provide texture data");
        return false;
    }else {
        return [
            createTexture2d(gl, {
                width: width,
                height: height,
                data: data,
                textureOptions: {
                    type: type
                }
            }),
            createTexture2d(gl, {
                width: width,
                height: height,
                data: data,
                textureOptions: {
                    type: type
                }
            })
        ]
    }
}

/**
 * Creates a Multi attachment ping pong buffer. The difference between this one and a regular one is that you can write to a bunch
 * of textures simultaneously on just two fbos instead of having a pair of fbos for each property you want to manipulate.
 * @param gl {WebGLRenderingContext} a webgl rendering context
 * @param simulation the source GLSL for the ping pong code that you want to run
 * @param textureMap {Array} an optional texture map of initial starting data. If this is undefined, random data will be generated
 * @param uniformMap {Array} an optional map of uniforms to feed to the simulation pass. This lets the shader set values for those uniforms
 * @param numAttachments {Number} the number of attachments to create on each FBO. This is ignored if textureMap has a filled array
 * @param width {Number} The width
 * @param height
 * @returns {*}
 */
export function createMultiPingpongBuffer(gl,simulation,{textureMap,uniformMap=[],numAttachments=1,width=128,height=128}){
    let rt1,rt2;

    // for the moment, this requires the WEBGL_draw_buffers extension. First check to make sure it's enabled
    if(!gl.hasOwnProperty('WEBGL_draw_buffers') || !gl.getExtension('WEBGL_draw_buffers')){
        console.error("The current computer does not have the WEBGL_draw_buffers extension available.");
        return false;
    }

    // if we've passed in a texture map
    if(textureMap !== undefined){
        let texSet1 = [];
        let texSet2 = [];
        if(textureMap instanceof Array){
            // it's assumed that each set is an array of two textures
            textureMap.forEach((set) => {
                texSet1.push(set[0]);
                texSet2.push(set[1]);
            });
        }else{
            console.error("createMultiPingpongBuffer error - textureMap must be an array")
            return false;
        }

        rt1 = createFBOWithAttachments(gl,textureMap.length,{
            floatingPoint:true,
            textures:texSet1
        })

        rt2 = createFBOWithAttachments(gl,textureMap.length,{
            floatingPoint:true,
            textures:texSet2
        })
    }else{
        rt1 = createFBOWithAttachments(gl,numAttachments,{
            width:512,
            height:512,
            floatingPoint:true
        })

        rt2 = createFBOWithAttachments(gl,numAttachments,{
            width:512,
            height:512,
            floatingPoint:true
        })
    }

    // append extension to the glsl
    let extension = "#extension GL_EXT_draw_buffers : require \n";

    // build quad for drawing
    let quad = createQuad(gl,{
        fragmentShader:extension + simulation,
        uniformMap:uniformMap
    });

    rt1.bindBuffers();
    rt2.bindBuffers();

    return {
        gl:gl,
        quad:quad,
        rt1:rt1,
        rt2:rt2,
        bindTexture(index=0){
            this.rt2.bindTexture(index);
        },
        bindTextures(){
            this.rt2.bindTextures();
        },
        unbindTextures(){
            this.gl.bindTexture(this.gl.TEXTURE_2D,null);
        },
        update(cb=null){
            let gl = this.gl;
            let rt1 = this.rt1;
            let rt2 = this.rt2;


            // bind the write texture
            rt1.bindFbo();
            // bind previous buffer's texture
            rt2.bindTextures();
            // draw the quad
            if(cb !== null){
                cb(this.quad.shader);
            }
            this.quad.draw();

            rt1.unbindFbo();
            // swap
            var tmp = this.rt2;
            this.rt2 = this.rt1;
            this.rt1 = tmp;
        },
    }
}

/**
 * A basic, pingpong setup. Generates it's own textures at the moment.
 * @param gl {WebGLRenderingContext} a webgl context
 * @param simulation {Number} the shader for the manipulating the texture data
 * @param width {Number} width for the textures
 * @param height {Number} height for the textures
 * @returns {{gl: *, flag: number, rt1: ({gl, drawTexture, fbo, bindFbo, unbindFbo, bindTexture, unbindTexture}|{gl: *, drawTexture: *, fbo: *, bindFbo: bindFbo, unbindFbo: unbindFbo, bind: bind, unbind: unbind}), rt2: ({gl, drawTexture, fbo, bindFbo, unbindFbo, bindTexture, unbindTexture}|{gl: *, drawTexture: *, fbo: *, bindFbo: bindFbo, unbindFbo: unbindFbo, bind: bind, unbind: unbind}), quad: ({vao, shader, buffer, type, hasTexture, gl, draw}|{vao: ({gl, vao, ext, attributes, setAttributeLocation, enableAttributes, addAttribute, getAttribute, enableAttribute, disableAttribute, setData, point, bind, unbind}|*), shader: ({gl, program, uniforms, attributes, bind, setMatrixUniform, setTextureUniform, uniform}|*), buffer: *, hasTexture: boolean, gl: *, draw: draw}), update: update, getOutput: getOutput}}
 */
export function createPingpongBuffer(gl,simulation,{width=128,height=128,texture=null,uniformMap=[]}={}){

    let obj = {};

    simulation = simulation !== undefined ? simulation : (function () {
        console.error("Attempt made to create a Pingpong buffer without a source shader");
    })();

    let rt1 = null;
    let rt2 = null;

    if(texture !== null){
        rt1 = createFBO(gl,{
            width:width,
            height:height,
            floatingPoint:true,
            texture:texture
        });

        rt2 = createFBO(gl,{
            width:width,
            height:height,
            floatingPoint:true,
            texture:texture
        });
    }else{
        rt1 = createFBO(gl,{
            width:width,
            height:height,
            floatingPoint:true
        });

        rt2 = createFBO(gl,{
            width:width,
            height:height,
            floatingPoint:true
        });
    }

    let quad = createQuad(gl,{
        withTexture:true,
        fragmentShader:simulation,
        uniforms:uniformMap
    });
    return {
        gl:gl,
        flag:0,
        rt1:rt1,
        rt2:rt2,
        quad:quad,
        update(cb=null){
            let gl = this.gl;
            let rt1 = this.rt1;
            let rt2 = this.rt2;


            // bind the write texture
            rt1.bindFbo();
            // bind previous buffer's texture
            rt2.bindTexture();

            if(cb !== null){
                this.quad.drawWithCallback(cb);
            }else{
                // draw the quad
                this.quad.draw();
            }

            // ubind both textures
            rt2.unbindTexture()
            rt1.unbindFbo();
            // swap
            var tmp = this.rt2;
            this.rt2 = this.rt1;
            this.rt1 = tmp;
        },

        //return the current output from the simulation
        getOutput(){
            return this.rt2.drawTexture
        }
    }
}




