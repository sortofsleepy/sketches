import {logError} from '../utils'
/**
 * A stand alone function for creating data based textures with TypedArrays.
 * Usable on it's own, but recommended that you use the {@link createTexture2d}
 * function
 * @param gl {WebGLRenderingContext} a WebGLRenderingContext
 * @param data {TypedArray} a TypedArray of data you want to write onto the texture
 * @param options {Object} a map of options for the texture creation. Needs the following keys
 * - width
 * - height
 * - internalFormat (gl.RGBA, etc)
 * - format (in WebGL 1, this should be the same as internalFormat, may change in WebGL2)
 * - type (gl.FLOAT, etc)
 * @returns {*}
 */
export function createDataTexture(gl,data,options){
    let texture = gl.createTexture();

    gl.bindTexture(TEXTURE_2D,texture);
    gl.texImage2D(
        TEXTURE_2D,
        0,
        options.internalFormat,
        options.width,
        options.height,
        0,
        options.format,
        options.type,
        data
    );

    // set min and mag filters
    gl.texParameteri(TEXTURE_2D,MAG_FILTER,options.magFilter);
    gl.texParameteri(TEXTURE_2D,MIN_FILTER,options.minFilter)

    //set wrapping
    gl.texParameteri(TEXTURE_2D,WRAP_S,options.wrapS)
    gl.texParameteri(TEXTURE_2D,WRAP_T,options.wrapT)

    // generate mipmaps if necessary
    if(options.generateMipMaps){
        gl.generateMipmap(TEXTURE_2D);
    }

    gl.bindTexture(TEXTURE_2D,null);

    return texture;
}

export function createCubemap(gl,images){
    const targets = [
        gl.TEXTURE_CUBE_MAP_POSITIVE_X, gl.TEXTURE_CUBE_MAP_NEGATIVE_X,
        gl.TEXTURE_CUBE_MAP_POSITIVE_Y, gl.TEXTURE_CUBE_MAP_NEGATIVE_Y,
        gl.TEXTURE_CUBE_MAP_POSITIVE_Z, gl.TEXTURE_CUBE_MAP_NEGATIVE_Z
    ];
    let texture = gl.createTexture();
    gl.bindTexture(gl.TEXTURE_CUBE_MAP, this.texture);
    for (let j = 0; j < 6; j++) {
        gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, false);
        if(images[j].shape) {
            gl.texImage2D(targets[j], 0, gl.RGBA, images[j].shape.width, images[j].shape.height, 0, gl.RGBA, gl.FLOAT, images[j].data);
        } else {
            gl.texImage2D(targets[j], 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, images[j]);
        }
        gl.texParameteri(gl.TEXTURE_CUBE_MAP, gl.TEXTURE_WRAP_S, this.wrapS);
        gl.texParameteri(gl.TEXTURE_CUBE_MAP, gl.TEXTURE_WRAP_T, this.wrapT);
        gl.texParameteri(gl.TEXTURE_CUBE_MAP, gl.TEXTURE_MAG_FILTER, this.magFilter);
        gl.texParameteri(gl.TEXTURE_CUBE_MAP, gl.TEXTURE_MIN_FILTER, this.minFilter);
    }
    gl.bindTexture(gl.TEXTURE_CUBE_MAP, null);
    return texture;
}

/**
 * Create an image based texture. Usable on it's own, but recommended that you use the {@link createTexture2d}
 * function
 * @param gl {WebGLRenderingContext} a WebGLRenderingContext
 * @param image {Image} and image object
 * @param options {Object} a map of options for the texture creation
 * @returns {*}
 */
export function createImageTexture(gl,image,options){
    let texture = gl.createTexture();
    gl.bindTexture(TEXTURE_2D,texture);

    // set the image
    gl.texImage2D(TEXTURE_2D,0,options.format,options.format,options.type,image);

    // set min and mag filters
    gl.texParameteri(TEXTURE_2D,MAG_FILTER,options.magFilter);
    gl.texParameteri(TEXTURE_2D,MIN_FILTER,options.minFilter)

    //set wrapping
    gl.texParameteri(TEXTURE_2D,WRAP_S,options.wrapS)
    gl.texParameteri(TEXTURE_2D,WRAP_T,options.wrapT)

    // generate mipmaps if necessary
    if(options.generateMipMaps){
        gl.generateMipmap(TEXTURE_2D);
    }

    gl.bindTexture(TEXTURE_2D,null);

    return texture;
}

export function createTextureCube(gl,images={},{textureOptions}={}){
    let texture = null;

    // NOTES
    // 1. in WebGL 1 , internalFormat and format ought to be the same value.
    // 2. UNSIGNED_BYTE corresponds to a Uint8Array, float corresponds to a Float32Array
    let textureSettings = {
        format:RGBA,
        internalFormat:RGBA,
        type:UNSIGNED_BYTE,
        wrapS:CLAMP_TO_EDGE,
        wrapT:CLAMP_TO_EDGE,
        minFilter:LINEAR,
        magFilter:LINEAR,
        generateMipMaps:false
    }


    if(textureOptions !== undefined){
        Object.assign(textureSettings,textureOptions);
    }

    // check to ensure we have at least 4 images
    if(images.keys() < 4){
        logError("Unable to create Texture cube without 4 images");
        return;
    }

    if(images.hasOwnProperty("px") &&
        images.hasOwnProperty("nx") &&
        images.hasOwnProperty("py") &&
        images.hasOwnProperty("ny") &&
        images.hasOwnProperty("pz") &&
        images.hasOwnProperty("nz")){
        createCubemap(gl,)
    }else{
        logError("Unable to continue constructing cubemap, it doesn't appear that you've set up the correct properties.")
        return;
    }


}

/**
 * Ensures that the specified width/height for the texture doesn't exceed the max for the
 * current card
 * @param gl {WebGLRenderingContext} a WebGL context
 * @param width {Number} the width
 * @param height {Number} the height
 * @returns {boolean}
 */
export function checkTextureSize(gl,width,height){
    var maxTextureSize = gl.getParameter(gl.MAX_TEXTURE_SIZE)
    if(width < 0 || width > maxTextureSize || height < 0 || height  > maxTextureSize) {
        logError('Invalid texture shape specified',true);
        return false;
    }else{
        return true;
    }
}


/**
 * Simple function for creating a basic texture
 * @param gl {WebGLRenderingContext} a WebGL context
 * @param data {Object} the initial texture data to use. Can be a TypedArray or an image.
 * @param textureOptions {Object} any options for how to process the resulting texture
 * @param width {Number} The width of the texture
 * @param height {Number} The height of the texture
 * @param randomInit {Boolean} a flag indicating whether or not we want random information written to the texture.
 * Useful for things like GPU ping-pong. False by default.
 * @returns {*}
 */
export function createTexture2d(gl,{data,textureOptions,width,height,randomInit=false}={}){
    let texture = null;

    // NOTES
    // 1. in WebGL 1 , internalFormat and format ought to be the same value. TODO does this change in WebGL2?
    // 2. UNSIGNED_BYTE corresponds to a Uint8Array, float corresponds to a Float32Array
    let textureSettings = {
        format:RGBA,
        internalFormat:RGBA,
        type:UNSIGNED_BYTE,
        wrapS:CLAMP_TO_EDGE,
        wrapT:CLAMP_TO_EDGE,
        minFilter:LINEAR,
        magFilter:LINEAR,
        generateMipMaps:false
    }


    if(textureOptions !== undefined){
        Object.assign(textureSettings,textureOptions);
    }

    // if we have data, process it as such, otherwise generate a blank texture of random data
    if(data === undefined){
        width = width || 128;
        height = height || 128;

        let data = null;

        // if textureOptions isn't undefined, check to see if we've defined the "type" key.
        // if that is set to the floating point constant, make sure to use a Float32Array,
        // otherwise default to Uint8Array.
        // If the parameter isn't defined, default to Uint8Array
        /*
         if(textureOptions !== undefined){
         if(textureOptions.hasOwnProperty('type')){
         if(textureOptions.type === FLOAT){
         data = new Float32Array(width * height * 4);
         }else{
         data = new Uint8Array(width * height * 4);
         }
         }
         }else{
         data = new Uint8Array(width * height * 4);
         }
         */
        //simplify the above a bit, leaving it for testing.
        if(textureSettings.type === FLOAT){
            data = new Float32Array(width * height * 4);
        }else{
            data = new Uint8Array(width * height * 4);
        }

        // if we just need a smattering of random data, apply that here if the flag is set
        if(randomInit){
            for(var i = 0; i < (width * height * 4);i += 4){
                data[i] = Math.random();
                data[i + 1] = Math.random();
                data[i + 2] = Math.random();
                data[i + 3] = 1.0;
            }
        }

        textureSettings["width"] = width;
        textureSettings["height"] = height;
        texture = createDataTexture(gl,data,textureSettings);

        // if we have data
    }else{
        textureSettings["width"] = width || 128;
        textureSettings["height"] = height || 128;

        // if it's an image, build an image texture
        if(data instanceof Image){
            texture = createImageTexture(gl,data,textureSettings);
        }

        // if it's a float 32 array we, build a data texture.
        if(data instanceof Float32Array){
            if(textureSettings.type !== FLOAT){
                textureSettings.type = FLOAT;
            }
            texture = createDataTexture(gl,data,textureSettings);
        }

        // if it's a float 32 array we, build a data texture.
        if(data instanceof Uint8Array){
            if(textureSettings.type !== FLOAT){
                textureSettings.type = FLOAT;
            }
            texture = createDataTexture(gl,data,textureSettings);
        }

        if(data instanceof Array){
            if(textureSettings.type !== FLOAT){
                textureSettings.type = FLOAT;
            }
            texture = createDataTexture(gl,new Float32Array(data),textureSettings);
        }


    }

    return {
        gl:gl,
        texture:texture,
        settings:textureSettings,
        getTexture(){
            return this.texture;
        },

        /**
         * Resizes a texture
         * @param w the new width
         * @param h the new height
         */
        resize(w,h){
            let options = this.settings;
            if(checkTextureSize(this.gl,w,h)){
                this.bind();
                gl.texImage2D(
                    TEXTURE_2D,
                    0,
                    options.internalFormat,
                    options.width,
                    options.height,
                    0,
                    options.format,
                    options.type,
                    null
                );
                this.unbind(0)
            }
        },
        bind(index=0){
            let gl = this.gl;
            gl.activeTexture(TEXTURE0 + index);
            gl.bindTexture(gl.TEXTURE_2D,this.texture);

            this.isBound = true;
        },

        unbind(){
            gl.bindTexture(gl.TEXTURE_2D,null);
        }
    }
}
