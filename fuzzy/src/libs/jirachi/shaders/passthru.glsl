uniform mat4 modelViewMatrix;
uniform mat4 projectionMatrix;


varying vec3 vPosition;
varying vec3 vColor;
attribute vec3 color;
attribute vec3 position;
void main(){
    vColor = color;
    vPosition = position;
    gl_Position = projectionMatrix * modelViewMatrix * vec4(position,1.);
}