#pragma once
#include "cinder/gl/Fbo.h"
#include "cinder/gl/GlslProg.h"
#include "cinder/gl/VboMesh.h"
#include "cinder/gl/Batch.h"
#include "cinder/app/App.h"
using namespace ci;
using namespace std;

class KScopeMaterial {

	ci::gl::BatchRef mBatch;
	ci::gl::GlslProgRef mShader;
	ci::gl::VboMeshRef mMesh;

public:
	KScopeMaterial() {
		
	}

	void setup() {

	}

	void draw() {
		mBatch->draw();
	}
};