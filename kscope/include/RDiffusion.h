#include "cinder/app/App.h"
#include "cinder/app/RendererGl.h"
#include "cinder/Camera.h"
#include "cinder/gl/gl.h"
#include "cinder/ImageIo.h"
#include "cinder/Utilities.h"
#include "cinder/params/Params.h"
#include "Resources.h"

#define SIZE 512

using namespace ci;
using namespace ci::app;
using namespace std;

class RDiffusion {
  public:

	void	setup() ;
	void	update() ;
	void	draw() ;
	void setInput(ci::gl::TextureRef mTexture);
  private:
	void	resetFBOs();
	
	params::InterfaceGlRef	mParams;
		
	int					mCurrentFBO, mOtherFBO;
	gl::FboRef			mFBOs[2];
	gl::GlslProgRef		mRDShader;
	gl::TextureRef		mTexture;
	
	vec2			mMouse;
	bool			mMousePressed;
	
	float			mReactionU;
	float			mReactionV;
	float			mReactionK;
	float			mReactionF;
	

	static const int		FBO_WIDTH = SIZE, FBO_HEIGHT = SIZE;
};


void RDiffusion::setInput(gl::TextureRef mTexture) {
	this->mTexture = mTexture;
	resetFBOs();
}


void RDiffusion::setup()
{
	mReactionU = 0.25f;
	mReactionV = 0.04f;
	mReactionK = 0.047f;
	mReactionF = 0.1f;

	mMousePressed = false;
	
	mCurrentFBO = 0;
	mOtherFBO = 1;
	mFBOs[0] = gl::Fbo::create( FBO_WIDTH, FBO_HEIGHT, gl::Fbo::Format().colorTexture().disableDepth() );
	mFBOs[1] = gl::Fbo::create( FBO_WIDTH, FBO_HEIGHT, gl::Fbo::Format().colorTexture().disableDepth()  );
	
	mRDShader = gl::GlslProg::create( app::loadAsset("diffusion.vert"),app::loadAsset("diffusion.frag") );
	//mTexture = gl::Texture::create( loadImage(  loadAsset("image.jpg") ),
		//						    gl::Texture::Format().wrap(GL_REPEAT).magFilter(GL_LINEAR).minFilter(GL_LINEAR) );
	gl::getStockShader( gl::ShaderDef().texture() )->bind();
	
	//resetFBOs();
}

void RDiffusion::update()
{	

	if (!mTexture) {
		return;
	}

	const int ITERATIONS = 25;
	// normally setMatricesWindow flips the projection vertically so that the upper left corner is 0,0
	// but we don't want to do that when we are rendering the FBOs onto each other, so the last param is false
	gl::setMatricesWindow( mFBOs[0]->getSize() );
	gl::viewport( mFBOs[0]->getSize() );
	for( int i = 0; i < ITERATIONS; i++ ) {
		mCurrentFBO = ( mCurrentFBO + 1 ) % 2;
		mOtherFBO   = ( mCurrentFBO + 1 ) % 2;
		
		gl::ScopedFramebuffer fboBind( mFBOs[ mCurrentFBO ] );
		
		{
			gl::ScopedTextureBind tex( mFBOs[ mOtherFBO ]->getColorTexture(), 0 );
			gl::ScopedTextureBind texSrcBind( mTexture, 1 );
			gl::ScopedGlslProg shaderBind( mRDShader );
			
			mRDShader->uniform( "tex", 0 );
			mRDShader->uniform( "texSrc", 1 );
			mRDShader->uniform( "width", (float) FBO_WIDTH );
			mRDShader->uniform( "ru", mReactionU );
			mRDShader->uniform( "rv", mReactionV );
			mRDShader->uniform( "k", mReactionK );
			mRDShader->uniform( "f", mReactionF );
			gl::drawSolidRect( mFBOs[ mCurrentFBO ]->getBounds() );
		}
		
		if( mMousePressed ){
			gl::ScopedGlslProg shaderBind( gl::getStockShader( gl::ShaderDef().color() ) );
			gl::ScopedColor col( Color::white() );
			RectMapping windowToFBO( getWindowBounds(), mFBOs[mCurrentFBO]->getBounds() );
			gl::drawSolidCircle( windowToFBO.map( mMouse ), 25.0f, 32 );
		}
	}
}

void RDiffusion::draw()
{
	gl::clear();
	gl::setMatricesWindow( getWindowSize() );
	gl::viewport( getWindowSize() );
	{
		gl::ScopedTextureBind bind( mFBOs[mCurrentFBO]->getColorTexture() );
		gl::drawSolidRect( getWindowBounds() );
	}
	
}

void RDiffusion::resetFBOs()
{
	gl::setMatricesWindow( mFBOs[0]->getSize() );
	gl::viewport( mFBOs[0]->getSize() );
	gl::ScopedTextureBind texBind( mTexture );
	for( int i = 0; i < 2; i++ ){
		gl::ScopedFramebuffer fboBind( mFBOs[i] );
		gl::drawSolidRect( mFBOs[i]->getBounds() );
	}
}

