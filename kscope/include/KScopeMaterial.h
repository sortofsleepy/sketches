#pragma once

#include "cinder/gl/Fbo.h"
#include "cinder/gl/GlslProg.h"
#include "cinder/gl/VboMesh.h"
#include "cinder/gl/Batch.h"
#include "cinder/app/App.h"
using namespace ci;
using namespace std;

class KScopeMaterial {

	ci::gl::BatchRef mBatch;
	ci::gl::GlslProgRef mShader;
	ci::gl::VboMeshRef mMesh;

public:
	KScopeMaterial() {
		mMesh = gl::VboMesh::create(geom::Rect().rect(Rectf(0, 0, app::getWindowWidth(), app::getWindowHeight())));
		gl::GlslProg::Format fmt;
		fmt.vertex(app::loadAsset("kscope.vert"));
		fmt.fragment(app::loadAsset("kscope.frag"));

		mShader = gl::GlslProg::create(fmt);

		mBatch = gl::Batch::create(mMesh, mShader);
	}

	void draw() {
		mBatch->draw();
	}
};