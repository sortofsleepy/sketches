#pragma once

#include <vector>
#include "cinder/app/App.h"
#include "cinder/app/RendererGl.h"
#include "cinder/gl/gl.h"
#include "TrianglePiece.h"
#include "cinder/Rand.h"
#include "cinder/Timeline.h"
#include "cinder/Log.h"
using namespace ci;
using namespace std;
using namespace ci::app;

static const int MIRROR_DUR = 5;	// Duration of the mirror/kaleidoscope animation
static const int STILL_DUR = 5;		// Duration of the still image

class KScopeMesh {
	Anim<float> mMirrorRot;
	int mPhase;
	float mSampleSize;			// Size of the image sample to grab for the kaleidoscope		   
	Anim<vec2> mSamplePt;
	vector<TrianglePiece> mTriPieces;
	bool mPiecesIn;
	gl::TextureRef mMirrorTexture;			// texture for the mirror
	vec2 newPos;
	float angle = 0.0;
	float angle2 = 0.0;

	int count = 0;

public:
	KScopeMesh() {
		mSampleSize = randInt(100, 300);
	}

	void setTexture(ci::gl::TextureRef mMirrorTexture) {
		this->mMirrorTexture = mMirrorTexture;
	}

	void draw() {
		auto vec = &mTriPieces;
		gl::ScopedModelMatrix scopedMat;
		gl::translate(getWindowCenter());
		
		angle += 0.02;
		angle2 += 20.0;

		//gl::rotate(mMirrorRot);
		gl::rotate(angle);
		for (int i = 0; i < vec->size(); i++) {
			(*vec)[i].draw();
		}
	}

	void updateMirrors() {
		if (!mMirrorTexture)
			return;

		auto vec = &mTriPieces;
		vec2 mSamplePt1(-0.5, -(sin(M_PI / 3) / 3));
		vec2 mSamplePt2(mSamplePt1.x + 1, mSamplePt1.y);
		vec2 mSamplePt3(mSamplePt1.x + (cos(M_PI / 3)), mSamplePt1.y + (sin(M_PI / 3)));

		mat3 mtrx(1.0f);
		mtrx = glm::translate(mtrx, mSamplePt.value());
		mtrx = glm::scale(mtrx, vec2(mSampleSize));
		mtrx = glm::rotate(mtrx, float((getElapsedFrames() * 4) / 2 * M_PI));

		mSamplePt1 = vec2(mtrx * vec3(mSamplePt1, 1.0));
		mSamplePt2 = vec2(mtrx * vec3(mSamplePt2, 1.0));
		mSamplePt3 = vec2(mtrx * vec3(mSamplePt3, 1.0));

		mSamplePt1 /= mMirrorTexture->getSize();
		mSamplePt2 /= mMirrorTexture->getSize();
		mSamplePt3 /= mMirrorTexture->getSize();

		CI_LOG_I(mSamplePt1, mSamplePt2, mSamplePt3);


		// loop through all the pieces and pass along the current texture and it's coordinates
		for (int i = 0; i < vec->size(); i++) {
			(*vec)[i].update(mMirrorTexture, mSamplePt1, mSamplePt2, mSamplePt3);
		}


		resetSample();
	}
	void changePhase(int newPhase) {
		mPhase = newPhase;

		switch (mPhase) {
			// Mirror Mode
		case 0: {
			// transition all of the mirror pieces in
			transitionMirrorIn();
			resetSample();

			//mMirrorRot = randFloat(M_PI, M_PI * 2);
			float newRot = mMirrorRot + randFloat(M_PI, M_PI / 4);
			//timeline().apply(&mMirrorRot, newRot, MIRROR_DUR, EaseInOutQuad());
			//mPiecesIn = false;
		}
				break;
				// Still Image Mode
		case 1:
			// transition all of the mirror pieces out
			for (vector<TrianglePiece>::iterator piece = mTriPieces.begin(); piece != mTriPieces.end(); ++piece) {
				(*piece).setTransitionOut(.25);
			}
			break;
		}
	}
	void transitionMirrorIn() {
		auto vec = &mTriPieces;
		for (int i = 0; i < vec->size(); i++) {
			float delay = randFloat(0.1f, 0.5f);
			(*vec)[i].reset(delay, mMirrorTexture);
		}
	
	}


	void resetSample() {

	


		if (count < 1050) {

			newPos = vec2(angle2);
			count++;
			
		}
		else {
			mSampleSize = randInt(100, 300);

			mSamplePt.value() = vec2(0);

			angle2 = 0.0;
			count = 0.0;
		}


		timeline().apply(&mSamplePt, newPos, MIRROR_DUR - 1, EaseInOutQuad());

		/*
		mSampleSize = randInt(100, 300);
		mSamplePt.value().y = randFloat(0, getWindowWidth() - mSampleSize);
		mSamplePt.value().x = randFloat(0, getWindowHeight() - mSampleSize);
*/

	
		/*
			// reset sample pos
		mSampleSize = randInt(100, 300);
		mSamplePt.value().y = randFloat(0, getWindowWidth() - mSampleSize);
		mSamplePt.value().x = randFloat(0, getWindowHeight() - mSampleSize);

		
		int count = 0;
		// Try to find a good sample location thats within the window's frame.
		// Give up if we try and settle after a bunch of times, no big deal.
		do {
			newPos.x = randFloat(0, getWindowWidth() - mSampleSize / 2);
			newPos.y = randFloat(0, getWindowHeight() - mSampleSize / 2);
			count++;
		} while (count < 150 && ((mSamplePt.value().x - newPos.x) <100 || (mSamplePt.value().y - newPos.y) < 100));
		//timeline().apply(&mSamplePt, newPos, MIRROR_DUR - 1, EaseInOutQuad()).delay(1000000.0);
	*/
	}

	void setup() {
		buildGrid();

		auto delayOffset = 0;

		changePhase(0);
		// This defines the length of time that we're in each phase
		//timeline().add([&] { changePhase(0); }, timeline().getCurrentTime() + delayOffset);
		//timeline().add([&] { changePhase(1); }, timeline().getCurrentTime() + delayOffset + MIRROR_DUR);
	}

	void buildGrid() {
		const int r = 1;
		const float tri_scale = (float)randInt(120, 400);

		// delete any previous pieces and clear the old vector
		mTriPieces.clear();

		vec2 pt1(0.0, 0.0);
		vec2 pt2(r, 0.0);
		vec2 pt3((cos(M_PI / 3) * r), (sin(M_PI / 3) * r));

		const float tri_width = distance(pt1, pt2) * tri_scale;
		const float tri_height = std::sqrt((tri_width*tri_width) - ((tri_width / 2) * (tri_width / 2)));

		const int amtX = ceil((((getWindowWidth() * 2) - .5) / (1.5*(tri_width))) + 0.5f);
		const float w = ((amtX*1.5) + .5) * tri_width;
		const float xOffset = -(w - getWindowWidth()) / 2;

		const int amtY = ceil((getWindowHeight() * 2) / (tri_height)+0.5f);
		const float yOffset = -((amtY*(tri_height)-getWindowHeight()) / 2);

		// creates a series of hexagons composed of 6 triangles each
		for (int i = 0; i < amtX; i++) {
			float startX = ((tri_width) * 1.5 * i);
			startX += xOffset;
			for (int j = 0; j < amtY; j++) {
				float startY = (i % 2 == 0) ? (tri_height * 2 * j) - (tri_height) : tri_height * 2 * j;
				startY += yOffset;

				for (int k = 0; k < 6; k++) {
					// because every other pieces is a mirror of the one next to it, every other has to be reversed on the x scale
					int scaleX = (k % 2 == 0) ? 1 : -1;
					vec2 scale(scaleX * tri_scale, tri_scale);

					vec2 start(startX, startY);

					TrianglePiece tri = TrianglePiece(vec2(startX, startY), pt1, pt2, pt3, M_PI / 3 * k, scale);
					mTriPieces.push_back(tri);
				}
			}
		}
	}
};