#include "cinder/app/App.h"
#include "cinder/app/RendererGl.h"
#include "cinder/gl/gl.h"
#include "KScopeMesh.h"

using namespace ci;
using namespace ci::app;
using namespace std;

class kscopeApp : public App {
  public:
	void setup() override;
	void mouseDown( MouseEvent event ) override;
	void update() override;
	void draw() override;
	
	KScopeMesh mesh;
	ci::gl::TextureRef mTex;
};


void kscopeApp::setup()
{
	gl::Texture::Format fmt;
	fmt.wrap(GL_CLAMP_TO_EDGE);
	fmt.minFilter(GL_NEAREST);
	fmt.magFilter(GL_NEAREST);
	
	mTex = gl::Texture::create(loadImage(app::loadAsset("image.jpg")),fmt);


	mesh.setup();
	mesh.setTexture(mTex);
}

void kscopeApp::mouseDown( MouseEvent event )
{
}

void kscopeApp::update()
{
	mesh.updateMirrors();
}

void kscopeApp::draw()
{
	gl::clear( Color( 0, 0, 0 ) ); 
	gl::viewport(app::getWindowSize());
	gl::setMatricesWindow(app::getWindowSize());
	mesh.draw();
}





CINDER_APP( kscopeApp, RendererGl )
