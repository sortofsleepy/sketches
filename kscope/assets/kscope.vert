#version 330

uniform mat4 ciModelViewProjection;

in vec3 ciPosition;

void main(){
	gl_Position = ciModelViewProjectioin * vec4(ciPosition,1.);
}