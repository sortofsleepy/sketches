var createBuffer = require('gl-buffer')
var createShell  = require('gl-now')
var createFBO    = require('gl-fbo')
var createVAO    = require('gl-vao')
var createShader = require("gl-shader");
var glslify = require('glslify');
var ndarray = require('ndarray')
var fill    = require('ndarray-fill')
var createCamera = require("perspective-camera")
var createOrbit = require("orbit-controls")
var mat4 = require("gl-matrix").mat4;
var rotate = require("gl-mat4").rotate
var translate = require("gl-mat4").translate


import Pingpong from "./Pingpong"
var raf = require("raf")


var rotationMat = mat4.create();
var modelMat = mat4.create();
var particleVertices
var screenVertices
var nextState
var prevState
var shaders

var t = 0
var time = 0;
var width = window.innerWidth;
var height = window.innerHeight
// ============== BUILD CORE COMPONENTS ===================
const gl = require('webgl-context')({
    width:window.innerWidth,
    height:window.innerHeight
})
document.body.appendChild(gl.canvas);

const camera = createCamera({
    fov: 51,
    position: [0, 0, 1000.5],
    near: 0.0001,
    far: 10000,
    viewport:[0,0,gl.canvas.width,gl.canvas.height]
})

const controls = createOrbit({
    element: gl.canvas,
    distanceBounds: [0.5, 100],
    distance: 0.5
});

window.addEventListener("resize",function(){
    gl.canvas.width = window.innerWidth;
    gl.canvas.height = window.innerHeight;
    camera.viewport = [0,0,gl.canvas.width,gl.canvas.height]
})

///////////////


let lvert = glslify('./shaders/logic.vert');
let lfrag = glslify('./shaders/logic.frag');

let rvert = glslify('./shaders/render.vert');
let rfrag = glslify('./shaders/render.frag');


shaders = {
    logic:createShader(gl,lvert,lfrag),
    render:createShader(gl,rvert,rfrag)
}

var buffer = new Pingpong(gl,lfrag);
buffer.setUniforms({
    uTime:t
})

screenVertices = createVAO(gl, [{
    type: gl.FLOAT
    , size: 2
    , buffer: createBuffer(gl, new Float32Array([
        -1, -1,  +1, -1,  -1, +1,
        -1, +1,  +1, -1,  +1, +1,
    ]))
}])

var index = new Float32Array(512 * 512 * 2)
var i = 0
for (var x = 0; x < 512; x++)
    for (var y = 0; y < 512; y++) {
        index[i++] = x / 512
        index[i++] = y / 512
    }

particleVertices = createVAO(gl, [{
    type: gl.FLOAT
    , size: 2
    , buffer: createBuffer(gl, index)
}])
var xRot = 0;
var yRot = 0;
var zRot = 0;
var lastTime = 0;
/////////
//translate(modelMat,modelMat,[(window.innerHeight / 2) * -1,0,0])
raf(function tick(dt){
    gl.clear(gl.COLOR_BUFFER_BIT);
    gl.clearColor(0,0,0,1.0);
    gl.viewport(0,0,gl.canvas.width,gl.canvas.height);
    t += 1.0;
    time += (90 * dt) / 1000.0;
    time *= 0.00005;

    rotate(modelMat,modelMat,degToRad(time),[0,0,1]);

    time += Math.min(30, dt) / 1000
    buffer.update();
    buffer.updateUniforms("uTime",t);
    gl.viewport(0, 0, width,height)

    var shader = shaders.render
    shader.bind()
    shader.uniforms.uState = buffer.getOutput().bind(0)
    shader.uniforms.projectionMatrix = camera.projection;
    shader.uniforms.viewMatrix = camera.view;
    shader.uniforms.modelMatrix = modelMat;

    shader.uniforms.uScreen = [width,height]

    particleVertices.bind()

    // Additive blending!
    gl.enable(gl.BLEND)
    gl.blendFunc(gl.ONE, gl.ONE)
    gl.drawArrays(gl.POINTS, 0, 512 * 512)
    gl.disable(gl.BLEND)

    buffer.swap();
    raf(tick);
});

function updateTime(){
    var timeNow = new Date().getTime();
    if (lastTime != 0) {
        var elapsed = timeNow - lastTime;

        xRot += (90 * elapsed) / 1000.0;
        yRot += (90 * elapsed) / 1000.0;
        zRot += (90 * elapsed) / 1000.0;
    }
    lastTime = timeNow;
}

function degToRad(degrees) {
    return degrees * Math.PI / 180;
}