#ifdef GL_ES
precision mediump float;
#endif

attribute vec2 aIndex;

uniform mat4 projectionMatrix;
uniform mat4 viewMatrix;
uniform mat4 modelMatrix;
uniform mat4 rotMat;
uniform vec2 uScreen;
uniform sampler2D uState;

varying vec2 vIndex;

void main() {
  vIndex = aIndex;
  gl_PointSize = 1.0;
    vec4 displacement = texture2D(uState, aIndex);
    gl_Position = projectionMatrix * viewMatrix * modelMatrix  * vec4(displacement.xy, displacement.z, 1.0);
  //gl_Position = vec4(texture2D(uState, aIndex).xy / uScreen, 1.0, 1.0);
}
