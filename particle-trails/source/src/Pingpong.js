const createFBO = require('gl-fbo')
const ndarray = require("ndarray");
const fill = require("ndarray-fill");
const createVAO = require("gl-vao");
const createBuffer = require("gl-buffer");
const createShader = require("gl-shader");
const glslify = require("glslify")

class Pingpong {
    /**
     * Constructor
     * @param gl a WebGLRenderingContext
     * @param simulation the GLSL code for the simulation
     * @param width a width ofr the fbos
     * @param height a height for the fbos
     */
    constructor(gl=null,simulation=null,width=512,height=512){

        //build two FBOs
        this.rt1 = createFBO(gl, 512, 512, { 'float': true })
        this.rt2 = createFBO(gl, 512, 512, { 'float': true })

        //set the vertex shader, which is just a simple passthru shader
        var vert = [
            "attribute vec3 position;",
            "void main() {",
                "gl_Position = vec4(position.xyz, 1.0);",
            "}"
        ].join("");

        //build the simulation shader.
        this.shader = createShader(gl,vert,simulation);

        //reference to the WebGLRenderingContext
        this.gl = gl;

        //simulation uniforms
        this.uniforms = null;

        //build the drawable rect to write new data onto the fbo
        this.rect = createVAO(gl, [{
            type: gl.FLOAT
            , size: 2
            , buffer: createBuffer(gl, new Float32Array([
                -1, -1,  +1, -1,  -1, +1,
                -1, +1,  +1, -1,  +1, +1,
            ]))
        }])
    }

    /**
     * sets any uniforms that might need to be passed to the shader
     */
    setUniforms(uniforms){
        this.uniforms = {};
        for(var i in uniforms){
            this.uniforms[i] = uniforms;
        }
    }

    /**
     * Updates a uniform for the simulation
     * @param key the key for the uniform
     * @param value the value for the uniform.
     */
    updateUniforms(key,value){
        this.uniforms[key] = value;

    }

    update(){
        let gl = this.gl;
        this.rt1.bind()
        gl.disable(gl.DEPTH_TEST)
        gl.viewport(0, 0, 512, 512)

        var shader = this.shader;
        shader.bind()
        shader.uniforms.uState = this.rt2.color[0].bind(0)

        if(this.uniforms !== null){

            for(var a in this.uniforms){
                shader.uniforms[a] = this.uniforms[a];
            }
        }

        this.rect.bind()
        gl.drawArrays(gl.TRIANGLES, 0, 6)

        this.unbind();

        this.output = this.rt2.color[0];
    }

    swap(){
        var tmp = this.rt2
        this.rt2 = this.rt1
        this.rt1 = tmp
    }

    getOutput(){
        return this.output;
    }


    unbind(){
        let gl = this.gl;
        gl.bindFramebuffer(gl.FRAMEBUFFER, null)
        gl.disable(gl.DEPTH_TEST)
    }

    setData(data=null){
        let gl = this.gl;

        if(data === null){
            data = ndarray(new Float32Array(512 * 512 * 4), [512, 512, 4])
            fill(data, function(x, y, ch) {
                if (ch > 2) return 1
                return (Math.random() - 0.5) * 800.6125
            })
        }


        this.rt1.color[0].setPixels(data)
        this.rt2.color[0].setPixels(data)


    }
}

export default Pingpong;