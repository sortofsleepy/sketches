
class GLUtils {
    constructor(){}
    /**
     * Generates a pass-thru vertex shader
     * @returns {string}
     * @private
     */
    static createPassShader(){
        if(!window.hasOwnProperty("THREE")){
            return [
                'attribute vec4 position;',
                'uniform mat4 projectionMatrix;',
                'uniform mat4 viewMatrix;',
                'uniform mat4 modelMatrix;',
                'void main() {',
                'gl_Position = projectionMatrix * viewMatrix * modelMatrix * position;',
                '}'
            ].join("")
        }
    }
    
}

export default GLUtils;