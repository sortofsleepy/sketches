#include "Particles.h"
using namespace ci;
using namespace std;


Particles::Particles(const int mNumParticles):mNumParticles(mNumParticles){

	
	
	loadShaders();


}

void Particles::loadShaders() {
	// These are the names of our out going vertices. GlslProg needs to
	// know which attributes should be captured by Transform FeedBack.
	std::vector<std::string> feedbackVaryings({
		"Position",
		"Metadata"
	});

	gl::GlslProg::Format updateFormat;
	updateFormat.vertex(app::loadAsset("shaders/particleUpdate.glsl"))
		.feedbackFormat(GL_SEPARATE_ATTRIBS)
		.feedbackVaryings(feedbackVaryings);

	try {
		mUpdateGlsl = gl::GlslProg::create(updateFormat);
	}
	catch (ci::gl::GlslProgLinkExc &e) {
		CI_LOG_I(e.what());
	}

	gl::GlslProg::Format renderFormat;
	renderFormat
		.vertex(app::loadAsset("shaders/particle.vert"))
		.fragment(app::loadAsset("shaders/particle.frag"));

	mRenderGlsl = gl::GlslProg::create(renderFormat);
}
void Particles::draw() {
	// We iterate our index so that we'll be using 
	
	// Notice that this vao holds the buffers we've just
	// written to with Transform Feedback. It will show
	// the most recent positions
	gl::ScopedVao scopeVao(mVaos[mIterationIndex & 1]);
	gl::ScopedGlslProg scopeGlsl(mRenderGlsl);

	gl::setDefaultShaderVars();

	gl::pointSize(14.0f);
	gl::drawArrays(GL_POINTS, 0, mNumParticles);
}

void Particles::update() {
	
	gl::ScopedGlslProg	scopeGlsl(mUpdateGlsl);
	gl::ScopedState		scopeState(GL_RASTERIZER_DISCARD, true);

	mUpdateGlsl->uniform("time", (float)app::getElapsedSeconds());
	
	// Bind the vao that has the original vbo attached,
	// these buffers will be used to read from.
	gl::ScopedVao scopedVao(mVaos[mIterationIndex & 1]);

	// We iterate our index so that we'll be using the
	// opposing buffers to capture the data
	mIterationIndex++;

	// bind buffers
	gl::bindBufferBase(GL_TRANSFORM_FEEDBACK_BUFFER, 0, mPosition[mIterationIndex & 1]);
	gl::bindBufferBase(GL_TRANSFORM_FEEDBACK_BUFFER, 1, mMetaData[mIterationIndex & 1]);

	// Begin Transform feedback with the correct primitive,
	// In this case, we want GL_POINTS, because each vertex
	// exists by itself
	gl::beginTransformFeedback(GL_POINTS);
	
	// process verts
	gl::drawArrays(GL_POINTS, 0, mNumParticles);

	// After that we issue an endTransformFeedback command
	// to tell OpenGL that we're finished capturing vertices
	gl::endTransformFeedback();

}

void Particles::setup() {

	// ================= BUILD DATA =================== //

	// std::array<vec3,1000> positions;
	vector<ci::vec3> positions(mNumParticles);
	vector<ci::vec4>metadata(mNumParticles);

	const float azimuth = 256.0f * M_PI / mNumParticles;
	const float inclination = M_PI / mNumParticles;
	const float radius = 40.0f;
	
	for (int i = 0; i < mNumParticles; ++i) {

		float x = radius * sin(inclination * i) * cos(azimuth * i);
		float y = radius * cos(inclination * i);
		float z = radius * sin(inclination * i) * sin(azimuth * i);
		
		
		positions[i] = vec3(x, y, z);
	
		// order should be phi,theta,phiSpeed,thetaSpeed
		metadata[i] = (vec4(M_PI * randFloat() * 2, M_PI * randFloat() * 2, randFloat(-0.5, 0.5), randFloat(-0.5, 0.5)));
	}

	


	// ================= WRITE DATA TO BUFFERS =================== //

	for (int i = 0; i < 2; i++) {
		mVaos[i] = gl::Vao::create();
		gl::ScopedVao scopeVao(mVaos[i]);
		{
			// buffer the positions
			mPosition[i] = gl::Vbo::create(GL_ARRAY_BUFFER, positions.size() * sizeof(vec3), positions.data(), GL_DYNAMIC_DRAW);
			{
				// bind and explain the vbo to your vao so that it knows how to distribute vertices to your shaders.
				gl::ScopedBuffer sccopeBuffer(mPosition[i]);
				gl::vertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (const GLvoid*)0);
				gl::enableVertexAttribArray(0);
			}
	
			// buffer the metadata
			mMetaData[i] = gl::Vbo::create(GL_ARRAY_BUFFER, metadata.size() * sizeof(vec4), metadata.data(), GL_DYNAMIC_DRAW);
			{
				// bind and explain the vbo to your vao so that it knows how to distribute vertices to your shaders.
				gl::ScopedBuffer sccopeBuffer(mMetaData[i]);
				gl::vertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, 0, (const GLvoid*)0);
				gl::enableVertexAttribArray(1);
			}
		}
	}

}