#include "Trails.h"
#include "cinder/FileWatcher.h"
#include "cinder/Log.h"
#include "cinder/app/App.h"

using namespace ci;
using namespace ci::app;
using namespace std;

Trails::Trails() :minLength(2), maxLength(10) {}

Trails::Trails(ci::gl::VboRef mParticlePositions):minLength(10),maxLength(50) {
	mParticleBuffer = mParticlePositions;
	
}

Trails& Trails::setupCompute() {
	vector<vec3> triangles;
	vector<vec4> lengths;
	uint32_t segmentCount = 0;
	uint32_t particleCount = mParticleBuffer->getSize() / sizeof(vec3);

	
	
	for (size_t i = 0; i < particleCount; ++i) {
		size_t n = randInt(minLength,maxLength);
		lengths.push_back(vec4(0, n, segmentCount, 0));
		for (size_t j = 0; j < n; ++j) {
			triangles.push_back(vec3(segmentCount + j, 1, 0));
			triangles.push_back(vec3(segmentCount + j, -1, 0));
			triangles.push_back(vec3(segmentCount + j, -1, 1));
			triangles.push_back(vec3(segmentCount + j, 1, 0));
			triangles.push_back(vec3(segmentCount + j, -1, 1));
			triangles.push_back(vec3(segmentCount + j, 1, 1));
		}
		segmentCount += n;
	}

	vector<vec4> zeroes(segmentCount, vec4(0.0f));
	mPositionBuffer = gl::Vbo::create(GL_ARRAY_BUFFER, zeroes, GL_STATIC_DRAW);
	mNormalBuffer = gl::Vbo::create(GL_ARRAY_BUFFER, zeroes, GL_STATIC_DRAW);
	mTangentBuffer = gl::Vbo::create(GL_ARRAY_BUFFER, zeroes, GL_STATIC_DRAW);
	mLengthBuffer = gl::Vbo::create(GL_ARRAY_BUFFER, lengths, GL_STATIC_DRAW);

	mRender = gl::GlslProg::create(loadAsset("shaders/trails/trails.vert"), loadAsset("shaders/trails/trails.frag"));

	mBatch = gl::Batch::create(gl::VboMesh::create(triangles.size(), GL_TRIANGLE_STRIP, { { geom::BufferLayout({ geom::AttribInfo(geom::POSITION, 3, 0, 0) }),
								gl::Vbo::create(GL_ARRAY_BUFFER, sizeof(vec3) * triangles.size(), triangles.data(), GL_STATIC_DRAW) } }),
		mRender);

	return *this;
}
Trails& Trails::loadCompute() {
	mUpdate = gl::GlslProg::create(gl::GlslProg::Format().compute(app::loadAsset("shaders/trails/trails.comp")));
	return *this;
}

void Trails::updateCompute(float timeStep) {

	if (!mParticleBuffer) {
		CI_LOG_I("No particle buffer was set");
		return;
	}
	
	gl::ScopedGlslProg glsl(mUpdate);

	mUpdate->uniform("uTimestep", timeStep);

	gl::bindBufferBase(GL_SHADER_STORAGE_BUFFER, 0, mParticleBuffer);
	gl::bindBufferBase(GL_SHADER_STORAGE_BUFFER, 1, mPositionBuffer);
	gl::bindBufferBase(GL_SHADER_STORAGE_BUFFER, 2, mNormalBuffer);
	gl::bindBufferBase(GL_SHADER_STORAGE_BUFFER, 3, mTangentBuffer);
	gl::bindBufferBase(GL_SHADER_STORAGE_BUFFER, 4, mLengthBuffer);

	gl::dispatchCompute(mParticleBuffer->getSize());//* mThreadGroupSize );
	gl::memoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT | GL_COMMAND_BARRIER_BIT);
}
void Trails::drawCompute() {
	gl::ScopedDebugGroup scopedDebug("Trails::Render");
	gl::ScopedModelMatrix scopedModel;

	gl::bindBufferBase(GL_SHADER_STORAGE_BUFFER, 0, mPositionBuffer);
	gl::bindBufferBase(GL_SHADER_STORAGE_BUFFER, 1, mNormalBuffer);
	gl::bindBufferBase(GL_SHADER_STORAGE_BUFFER, 2, mTangentBuffer);
	gl::bindBufferBase(GL_SHADER_STORAGE_BUFFER, 3, mParticleBuffer);

	gl::multModelMatrix(mTransform);
	mBatch->getGlslProg()->uniform("time", (float)app::getElapsedSeconds());
	mBatch->getGlslProg()->uniform("resolution", vec2(app::getWindowWidth(),app::getWindowHeight()));
	mBatch->draw();
	

}



void Trails::setupWatcher() {
	// setup and watch particle render and update shader

	FileWatcher::instance().watch({
		fs::path("shaders/trails/trails.vert"),
		fs::path("shaders/trails/trails.frag"),
		fs::path("shaders/trails/trails.comp")

		},

		[=](const WatchEvent &event) {

		try {
			mUpdate= gl::GlslProg::create(gl::GlslProg::Format().compute(loadAsset("shaders/trails/trails.comp")));
			mRender = gl::GlslProg::create(loadAsset("shaders/trails/trails.vert"), loadAsset("shaders/trails/trails.frag"));
		}
		catch (const ci::Exception &exc) {
			CI_LOG_E("particle.comp\n" << exc.what());
		}

	});
}

void Trails::cleanupWatcher() {
	FileWatcher::instance().unwatch({
		fs::path("shaders/trails/trails.vert"),
		fs::path("shaders/trails/trails.frag"),
		fs::path("shaders/trails/trails.comp")

	});
}




void Trails::cleanup() {}