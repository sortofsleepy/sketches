#include "cinder/app/App.h"
#include "cinder/app/RendererGl.h"
#include "cinder/gl/gl.h"
#include "cinder/Rand.h"
#include "ParticleCompute.h"
#include "ChisaiCamera.h"
#include "Trails.h"
#include "Composer.h"
#include "Background.h"

using namespace ci;
using namespace ci::app;
using namespace std;

class TrailwindsApp : public App {
  public:
	void setup() override;
	void update() override;
	void draw() override;
	void drawTrails();
	void buildBackground();
	//! Renders scene into it's FBO
	void renderScene();
	void initPost();
	void runPost();

	//! renders the FINAL scene complete with reversed angles. 
	void renderCompleteScene();
	~TrailwindsApp();

	// render image out 
	ci::gl::BatchRef mRenderBatch;
	ci::gl::GlslProgRef mRenderShader;
	ci::gl::FboRef mSceneFbo;

	ChisaiCamera camera;
	Trails trails;
	ParticleCompute computeSystem;

	// ============ POST-PROCESSING STUFF ============== //
	Composer composer;
	PostProcessRef edge, blurW, blurH,fxaa;

	// ============= BACKGROUND STUFF ============= //
	ci::gl::TextureRef mSpring, mSummer, mWinter;
	Background bg;
	int mBackgroundIndex = 0;

};

TrailwindsApp::~TrailwindsApp() {
	computeSystem.cleanupWatcher();
	trails.cleanupWatcher();
}

void TrailwindsApp::setup()
{
	
	gl::GlslProg::Format fmt;
	fmt.vertex(app::loadAsset("shaders/final.vert"));
	fmt.fragment(app::loadAsset("shaders/final.frag"));

	mRenderShader =gl::GlslProg::create(fmt);
	mRenderBatch = gl::Batch::create(geom::Rect(Rectf(0, 0, app::getWindowWidth(),app::getWindowHeight())), mRenderShader);

	computeSystem.setupWatcher();

	trails = Trails(computeSystem.getParticleData());
	trails.setupCompute().loadCompute();
	trails.setupWatcher();

	mSceneFbo = gl::Fbo::create(app::getWindowWidth(), app::getWindowHeight());





	buildBackground();

	// init post-processing
	initPost();
}

void TrailwindsApp::update()
{
	computeSystem.update();
	trails.updateCompute(app::getElapsedSeconds() / app::getElapsedFrames());
}


void TrailwindsApp::buildBackground() {

	gl::Texture::Format fmt;
	fmt.setWrap(GL_CLAMP_TO_EDGE, GL_CLAMP_TO_EDGE);
	fmt.minFilter(GL_NEAREST);
	fmt.magFilter(GL_NEAREST);

	mSpring = gl::Texture::create(loadImage(loadAsset("images/spring.jpg")),fmt);
	mSummer = gl::Texture::create(loadImage(loadAsset("images/summer.jpg")),fmt);
	mWinter = gl::Texture::create(loadImage(loadAsset("images/winter.jpg")),fmt);

	// set initial background images
	bg.setTextures(mSpring, mSummer);
}
void TrailwindsApp::initPost() {

	gl::Texture2d::Format fmt;
	fmt.setMinFilter(GL_LINEAR);
	fmt.setMagFilter(GL_LINEAR);
	fmt.setInternalFormat(GL_RGB8);

	edge = PostProcessRef(new PostProcessLayer(app::loadAsset("shaders/post/edge.glsl"),fmt));
	fxaa = PostProcessRef(new PostProcessLayer(app::loadAsset("shaders/post/fxaa.frag"), fmt));
	blurW = PostProcessRef(new PostProcessLayer(app::loadAsset("shaders/post/blur.glsl"), fmt));
	blurH = PostProcessRef(new PostProcessLayer(app::loadAsset("shaders/post/blur.glsl"), fmt));

	// init composer
	composer = Composer(mSceneFbo->getColorTexture());
	composer.addLayer(edge);
	composer.addLayer(blurW);
	composer.addLayer(blurH);
	composer.addLayer(fxaa);

}

void TrailwindsApp::runPost() {

	/*
		blurW.uniform("sample_offset", vec2(1.0f / app::getWindowWidth(), 0.0f));
	blurW.uniform("attenuation", 1.5f);
	blurH.uniform("sample_offset", vec2( 0.0f, 1.0f / app::getWindowHeight()));
	blurH.uniform("attenuation", 1.5f);*/

	//uniform vec3 edgeColor;
	edge->update([=](gl::GlslProgRef shader)->void {
		shader->uniform("edgeColor", vec3(1.0,0.0,0.0));
	});

	blurW->update([=](gl::GlslProgRef shader)->void {
		shader->uniform("sample_offset", vec2(1.0f / app::getWindowWidth(), 0.0f));
		shader->uniform("attenuation", 1.2f);
	});

	blurH->update([=](gl::GlslProgRef shader)->void {
		shader->uniform("sample_offset", vec2(0.0f, 1.0f / app::getWindowHeight()));
		shader->uniform("attenuation", 1.2f);
	});


	//uniform vec4		uExtents;
	fxaa->update([this](gl::GlslProgRef shader)->void{
		auto bounds = fxaa->getBounds();
		const float w = (float)bounds.getWidth();
		const float h = (float)bounds.getHeight();
		shader->uniform("uExtents", vec4(1.0f / w, 1.0f / h, w, h));
	});
}

void TrailwindsApp::renderScene() {
	gl::ScopedFramebuffer fbo(mSceneFbo);
	gl::ScopedMatrices mats();


	gl::clear(ColorA(0, 0, 0, 0));

	camera.useCamera();

	//bg.draw();

	

	trails.drawCompute();
	



}

void TrailwindsApp::drawTrails() {

	gl::pushMatrices();
	gl::setMatricesWindow(app::getWindowSize());
	gl::viewport(app::getWindowSize());

	

	gl::ScopedTextureBind tex0(composer.getComposite(), 0);
	mRenderShader->uniform("uTex0", 0);
	mRenderBatch->draw();



	gl::popMatrices();


}



void TrailwindsApp::draw()
{
	gl::clear( Color( 0, 0, 0 ) ); 
	
	renderScene();

	runPost();
	drawTrails();
	

	
}

CINDER_APP(TrailwindsApp, RendererGl, [=](App::Settings * settings) {
	settings->setWindowSize(1024, 768);
})
