
uniform sampler2D uTex0;


in vec2 vUv;

out vec4 glFragColor;

void main(){
	vec2 uv = vUv;
	vec2 reverseUv = vec2(1.0 - vUv.s, 1.0 - vUv.t);
	

	vec4 tex = texture(uTex0,uv);
	vec4 rTex = texture(uTex0,reverseUv);
	glFragColor = (tex * rTex);
}