precision highp float;
in vec2 vTextureCoord;
uniform sampler2D textureCurr;
uniform sampler2D textureNext;

uniform float waveFront;
uniform float waveLength;
uniform vec3 startPosition;
uniform float time;

in float vDiffuse;
in vec3 vPosition;
in vec3 vCenter;

const float PI = 3.141592657;
uniform vec2 resolution;
out vec4 glFragColor;

#include "../noise.glsl"

void main(){

	float distToStartPoint = distance(vCenter, startPosition);
	const float posOffset = 0.2;
	float distNoise = snoise(vPosition * posOffset + time);
	distToStartPoint += distNoise * 0.5; 
	float distToWaveFront = distance(distToStartPoint, waveFront);

	bool hasFlipped = false;
	if(distToStartPoint < waveFront) {
		hasFlipped = true;
	} 

	if(!hasFlipped) {
		glFragColor = texture(textureCurr, vTextureCoord);	
	} else {
		glFragColor = texture(textureNext, vTextureCoord);
	}
     glFragColor.rgb *= vDiffuse;

}