precision highp float;
in vec3 ciPosition;
in vec2 ciTexCoord0;
in vec3 ciNormal;
in vec3 aCenter;

uniform mat4 ciModelViewProjection;
uniform float waveFront;
uniform float waveLength;
uniform vec3 startPosition;
uniform float time;

out vec2 vTextureCoord;
out float vDiffuse;
out vec3 vPosition;
out vec3 vCenter;

const float PI = 3.141592657;
#include "../noise.glsl"

mat4 rotationMatrix(vec3 axis, float angle)
{
    axis = normalize(axis);
    float s = sin(angle);
    float c = cos(angle);
    float oc = 1.0 - c;
    
    return mat4(oc * axis.x * axis.x + c,           oc * axis.x * axis.y - axis.z * s,  oc * axis.z * axis.x + axis.y * s,  0.0,
                oc * axis.x * axis.y + axis.z * s,  oc * axis.y * axis.y + c,           oc * axis.y * axis.z - axis.x * s,  0.0,
                oc * axis.z * axis.x - axis.y * s,  oc * axis.y * axis.z + axis.x * s,  oc * axis.z * axis.z + c,           0.0,
                0.0,                                0.0,                                0.0,                                1.0);
}


vec3 rotate(vec3 v, vec3 axis, float angle) {
	mat4 m = rotationMatrix(axis, angle);
	return (m * vec4(v, 1.0)).xyz;
}


float exponentialOut(float t) {
    return t == 1.0 ? t : 1.0 - pow(2.0, -10.0 * t);
}
void main() {
	vec3 relativePos = ciPosition - aCenter;

	vec3 axis = cross(startPosition, ciNormal);
	float distToStartPoint = distance(aCenter, startPosition);
	const float posOffset = 0.2;
	float distNoise = snoise(ciPosition * posOffset + time);
	distToStartPoint += distNoise * 0.5; 


	float distToWaveFront = distance(distToStartPoint, waveFront);
	float angle = 0.0;
	if(distToWaveFront < waveLength) {
		angle = (1.0 - exponentialOut(distToWaveFront/waveLength)) * PI;
	}


	relativePos = rotate(relativePos, axis, angle);

	vec3 finalPosition = aCenter + relativePos;

	// increase radius here so we don't have to add more vertices. 
	finalPosition *= vec3(30.0);

	gl_Position = ciModelViewProjection * vec4(finalPosition, 1.0);
	//gl_Position = ciModelViewProjection * vec4(ciPosition,1.);

	vTextureCoord = ciTexCoord0;

    vec3 N = normalize(ciPosition);
    N = rotate(N, axis, angle);
    vec3 L = normalize(aCenter);
    
    vDiffuse = max(dot(N, L), 0.9);
    // vDiffuse = dot(N, L) * .5 + .5;

    vPosition = ciPosition;
    vCenter = aCenter;
}