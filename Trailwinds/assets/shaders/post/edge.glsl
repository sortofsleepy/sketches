// great sobel edge detection by @jmk with some modifications.
// https://www.shadertoy.com/view/Mdf3zr




uniform sampler2D uTex0;
uniform vec2 resolution;
uniform vec3 edgeColor;
out vec4 glFragColor;



float lookup(vec2 p, float dx, float dy, float d)
{
    vec2 uv = (p.xy + vec2(dx * d, dy * d)) / resolution.xy;
    vec4 c = texture(uTex0, uv.xy);
	
	// return as luma
    return 0.2126*c.r + 0.7152*c.g + 0.0722*c.b;
}

void main( void )
{
    float d = 0.5 + 1.5; // kernel offset
    vec2 p = gl_FragCoord.xy;
    
	// simple sobel edge detection
    float gx = 0.0;
    gx += -1.0 * lookup(p, -1.0, -1.0,d);
    gx += -2.0 * lookup(p, -1.0,  0.0,d);
    gx += -1.0 * lookup(p, -1.0,  1.0,d);
    gx +=  1.0 * lookup(p,  1.0, -1.0,d);
    gx +=  2.0 * lookup(p,  1.0,  0.0,d);
    gx +=  1.0 * lookup(p,  1.0,  1.0,d);
    
    float gy = 0.0;
    gy += -1.0 * lookup(p, -1.0, -1.0,d);
    gy += -2.0 * lookup(p,  0.0, -1.0,d);
    gy += -1.0 * lookup(p,  1.0, -1.0,d);
    gy +=  1.0 * lookup(p, -1.0,  1.0,d);
    gy +=  2.0 * lookup(p,  0.0,  1.0,d);
    gy +=  1.0 * lookup(p,  1.0,  1.0,d);
    
	// hack: use g^2 to conceal noise in the video
    float gx2 = gy*gy;
    
    float g = pow(gx2,0.7);
    float g2 = g;
    
    
    vec4 edges =  vec4(g , g, g2, 1.0);
    
	// tint the edge a color
    edges *= vec4(edgeColor,1.0);
    
	// apply edge shade
    vec4 col = texture(uTex0, p / resolution.xy);
    col += edges;

    
   
    
    glFragColor = col;
}