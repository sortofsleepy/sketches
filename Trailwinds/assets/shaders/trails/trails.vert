
#version 430

uniform mat4 ciModelView;
uniform mat4 ciProjectionMatrix;
uniform mat4 ciModelViewProjection;

layout(std430, binding = 0) buffer PositionBuffer
{
	readonly vec4 uPosition[];
};

layout(std430, binding = 1) buffer NormalBuffer
{
	readonly vec4 uNormal[];
};

layout(std430, binding = 2) buffer TangentBuffer
{
	readonly vec4 uTangent[];
};
struct Particle
{
	// current position 
	vec4 position;

	// velocity
	vec4 vel;

	vec4 osc;

	// any additional metadata
	vec4 meta;
};

layout( std140, binding = 3 ) buffer Part
{
    Particle particle[];
};


in vec3 ciPosition;
out vec3 vPosition;
out vec3 vNormal;
out vec2 vUv;
out float vAlpha;

void main(){
	int index = int( ciPosition.x );

	vec3 normal = normalize( mix( uNormal[index].xyz, uNormal[index+1].xyz, ciPosition.z ) );
	vec3 tangent = normalize( mix( uTangent[index].xyz, uTangent[index+1].xyz, ciPosition.z ) );
	vec3 position = mix( uPosition[index].xyz, uPosition[index+1].xyz, ciPosition.z );
	float life =  mix( uTangent[index].w, uTangent[index+1].w, ciPosition.z );
	float i = mix( uPosition[index].w, uPosition[index+1].w, ciPosition.z );


	position += ciPosition.y * tangent;
	vPosition = ( ciModelView * vec4( position, 1.0 ) ).xyz;
	vUv = vec2( i, ciPosition.y * 0.5f + 0.5f );
	vNormal = normal;
	vAlpha = life;
	
	gl_Position = ciModelViewProjection * vec4( position, 1.0 );

}