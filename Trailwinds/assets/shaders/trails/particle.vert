#version 430
#extension GL_ARB_shader_storage_buffer_object : require

in vec3 ciPosition;
in int particleId;
in float time;


struct Particle
{
	// current position 
	vec4 pos;
};

layout( std140, binding = 0 ) buffer Part
{
    Particle particles[];
};

uniform mat4 ciModelViewProjection;
uniform mat4 ciModelView;
uniform mat3 ciNormalMatrix;

in vec3 ciNormal;



void main(){	
	vec3 finalPos = ciPosition + particles[particleId].pos.xyz;
	gl_Position = ciModelViewProjection * vec4( finalPos, 1 );

}