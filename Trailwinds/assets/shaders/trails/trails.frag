
uniform float time;
uniform vec2 resolution;

out vec4 oColor;
in vec3 vNormal;
in vec3 vPosition;
in vec2 vUv;
in float vAlpha;

void main()
{
	vec2 uv = gl_FragCoord.xy / resolution.xy;
	vec3 col = 0.5 + 0.5 * sin(time+uv.xyx * vUv.xyx +vec3(0,2,4));

	col += col;

	vec4 fin = vec4(col.yz,0.0,1.0);
	oColor = mix(vec4(col,1.),fin,0.6);
	oColor.a = vAlpha;

	
}