#version 430 core


uniform float time;

struct Particle
{
	// current position 
	vec4 position;

	// velocity
	vec4 vel;

	vec4 osc;

	// any additional metadata
	vec4 meta;
};

layout( std140, binding = 0 ) buffer Part
{
    Particle particles[];
};

#define TWO_PI 3.14149 * 3.14149

//layout( local_size_variable ) in;
layout( local_size_x = 128, local_size_y = 1, local_size_z = 1 ) in;

#include "../noise.glsl"

float map(float value, float inMin, float inMax, float outMin, float outMax) {
  return outMin + (outMax - outMin) * (value - inMin) / (inMax - inMin);
}

void main()
{
	uint gid = gl_GlobalInvocationID.x;	// The .y and .z are both 1 in this case.

	// =========== SETUP ================ //

	// get ref to particle for easier use
	Particle p = particles[gid];
	

	// ============ DRAW ================ // 
	// note x direction is reversed for some reason - not sure why. 
	// so negative value will move forward towards right, positive value moves left

	vec3 curl = curlNoise(p.vel.xyz);

	if(p.meta.x <= 0.0){
		if(p.position.x < 50.0){
			float dx = 1.0 - p.meta.x;
			p.meta.x += dx;

		}
	}
	float speed = p.meta.y;
	float waveHeight = p.osc.x;
	float amplitude = p.osc.w;
	float startY = p.osc.y;
	float startX = p.osc.z;

	float y = map(sin(amplitude),-1,1,0,waveHeight);
   
	p.position.x -= 0.1 + (speed); 
	p.position.y = startY + y;
	p.position.z += curl.z / 4.0;


	// re-assign cause things don't operate by reference. 
	//p.osc.x += sin(1.0 / 60.0);

	if(p.osc.x >= 10.0){
		p.osc.w = 0.2;
	}else{
		p.osc.w += 0.02;
	}

	
	if(p.position.x < p.osc.z * -1){
		p.meta.x -= 0.01;

		if(p.meta.x <= 0.0){
			p.position.x = p.osc.z + 50.0;
		}
	}
	
	

	// re-assign cause things don't operate by reference. 
	particles[gid] = p;
}

/*
	if(p.position.x < ((p.osc.z * -1) - 40.0)){
		p.meta.x = 0.0;
		p.position.x = p.osc.z;

	}
	*/
