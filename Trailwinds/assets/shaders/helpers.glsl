
vec3 getVolumeSwirlingForce( vec3 position, vec3 normal, vec3 up, float dist, float separation, vec3 curlSmall, vec3 curlLarge )
{		
	vec3 cros = cross( normal, up );
	vec3 force = vec3( 0.0f );

	// attraction
	force += 0.0005f * normal * smoothstep( 0.0f, separation, dist ) * smoothstep( separation + 15.0, separation, dist );
	// short dist repulsion
	force -= 0.00125f * normal * smoothstep( separation * 0.285f, 0.0, dist );

	// swirling
	float swirlingZone = smoothstep( separation * 1.42f, separation * 0.285f, dist ) * smoothstep( separation * 0.14f, separation * 0.285f, dist );
	force += 0.00025f * normalize( /*curlSmall * 0.125f*/ + cros + normal * 0.3 ) * swirlingZone;
	//force += 0.00005f * normalize( curlSmall ) * swirlingZone;

	return 1.25f * force;
}

vec3 getVolumeOrbitExitForce( vec3 position, vec3 normal, float dist, float exitDist, vec3 curlLarge )
{		
	return -0.00175f * normalize( 2.0 * curlLarge + normal ) // repulsion
			* clamp( vec3( 0.04f * dist, 0.01f * dist, 2.0f * dist ), vec3(-3), vec3(3) ) // slight limit to xz plane
			* smoothstep( exitDist, exitDist + 4.0f, dist ); // only happen far enough
}

vec3 getAmbientCurlForce( vec3 position, vec3 normal, float dist )
{		
	return vec3( 0.0f );
}

vec3 getWindyCurlForce( vec3 position, vec3 normal, float dist )
{		
	return vec3( 0.0f );
}


vec3 rotateAxisAngle(vec3 p, vec3 axis, float angle){
  float c = cos(angle);
  float s = sin(angle);
  float t = 1.0 - c;
  vec3 a = normalize(axis);
  mat3 m = mat3(
    t * a.x * a.x + c,
    t * a.x * a.y + a.z * s,
    t * a.x * a.z - a.y * s,
    t * a.y * a.x - a.z * s,
    t * a.y * a.y + c,
    t * a.y * a.z + a.x * s,
    t * a.z * a.x + a.y * s,
    t * a.z * a.y - a.x * s,
    t * a.z * a.z + c
  );
  return m * p;
}

vec3 rotate( vec3 position, vec3 axis, float angle )
{ 
#if 1
	float halfAngle = angle * 0.5f;
	vec4 quat = vec4( axis * sin( halfAngle ), cos( halfAngle ) );
	return position + 2.0f * cross( quat.xyz, cross( quat.xyz, position ) + quat.w * position );
#else
	return rotateAxisAngle( position, axis, angle );
#endif
}


vec3 parallelFrameTransport( vec3 p0, vec3 p1, vec3 p2, vec3 normal )
{
	vec3 tangent = normalize( p1 - p0 );
	vec3 tangentNext = normalize( p2 - p1 );
	vec3 bitangent = cross( tangent, tangentNext );
	float bitangentLength = length( bitangent );
	if( bitangentLength < 0.001f ) {
		return normal;
	}
	else {
		float angle = acos( dot( tangent, tangentNext ) );
		return rotate( normal, bitangent / bitangentLength, angle );
	}	
}


mat4 rotationMatrix(vec3 axis, float angle)
{
    axis = normalize(axis);
    float s = sin(angle);
    float c = cos(angle);
    float oc = 1.0 - c;
    
    return mat4(oc * axis.x * axis.x + c,           oc * axis.x * axis.y - axis.z * s,  oc * axis.z * axis.x + axis.y * s,  0.0,
                oc * axis.x * axis.y + axis.z * s,  oc * axis.y * axis.y + c,           oc * axis.y * axis.z - axis.x * s,  0.0,
                oc * axis.z * axis.x - axis.y * s,  oc * axis.y * axis.z + axis.x * s,  oc * axis.z * axis.z + c,           0.0,
                0.0,                                0.0,                                0.0,                                1.0);
}


/*
vec3 rotate(vec3 v, vec3 axis, float angle) {
	mat4 m = rotationMatrix(axis, angle);
	return (m * vec4(v, 1.0)).xyz;
}

*/
float exponentialOut(float t) {
    return t == 1.0 ? t : 1.0 - pow(2.0, -10.0 * t);
}