#pragma once


#include "cinder/Camera.h"
#include "cinder/CameraUi.h"
#include "cinder/app/App.h"
#include "cinder/gl/gl.h"

using namespace ci;
using namespace app;

class ChisaiCamera {
	ci::CameraPersp mCam;
	ci::CameraUi mCamUi;

	ci::vec3 eye;
	ci::vec3 target;

	float zoom;
public:
	ChisaiCamera(float fov=60.0f, float aspect=app::getWindowAspectRatio(), float min=0.1, float max=10000.0,float zoom=-40) {
		eye = ci::vec3(0, 0, zoom);
		target = ci::vec3(0, 0, 0);

		mCam = CameraPersp(getWindowWidth(), getWindowHeight(), 60.0, 0.1, 10000.0);
		mCam.lookAt(eye, target);
		mCamUi = CameraUi(&mCam, getWindow(), -1);
	
		app::getWindow()->getSignalMouseDown().connect([=](MouseEvent e)->void{
			mCamUi.mouseDown(e);
		});

		app::getWindow()->getSignalMouseDrag().connect([=](MouseEvent e)->void{
			mCamUi.mouseDrag(e);
		});


	
	}

	void setEye(ci::vec3 eye) {
		this->eye = eye;
		mCam.lookAt(eye, target);
	}

	void setTarget(ci::vec3 target) {
		mCam.lookAt(eye,target);
		this->target = target;
	}

	ci::mat4 getViewMatrix() {
		return mCam.getViewMatrix();
	}

	ci::mat4 getInverseViewMatrix() {
		return mCam.getInverseViewMatrix();
	}

	void useCamera() {
		gl::setMatrices(mCam);
	}


};