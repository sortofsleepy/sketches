#pragma once

#include "cinder/gl/GlslProg.h"
#include "cinder/gl/Fbo.h"
#include "cinder/Vector.h"
#include "cinder/gl/Texture.h"
#include "cinder/Log.h"
#include <vector>
using namespace ci;
using namespace std;

/**
	An experiment to make up for the lack of compute shaders on the Web. 
	Should be compilable with Emscripten. 
*/

struct ComputeAttribute {
	ci::gl::TextureRef tex1;
	ci::gl::TextureRef tex2;
};
class ComputeProcess {

	// ping-pong'n buffers
	ci::gl::FboRef mFbos[2];
	
	//! Shader to use for the update process
	ci::gl::GlslProgRef mShader;

	//! Number of attributes this process will deal with 
	int mNumAttributes;

	//! The width / height of each texture. 
	float mHeight, mWidth;

	int mCurrentFlag, mOtherFlag;

	std::vector <ComputeAttribute> mAttributes;

	int mCurrentWidth, mCurrentHeight;

	//! The current lookup position of the current set of attributes.
	ci::vec2 mLookupPosition;
public:
	ComputeProcess(int numAttributes = 1, float width = 128, float height = 128) :
		mNumAttributes(numAttributes),
		mWidth(width), mHeight(height),
		mCurrentFlag(0), mOtherFlag(1),
		mCurrentWidth(0),
		mCurrentHeight(0)
	{
	


	}

	//! Add a block of data we want to compute with in the update shader. 
	//! For simplicity, it's assumed all data in a ComputeProcess is in a vec4. 
	void addAttribute(std::vector<ci::vec4> mData) {
		gl::Texture::Format fmt;
		fmt.setInternalFormat(GL_RGBA32F);
		fmt.setWrap(GL_CLAMP_TO_EDGE, GL_CLAMP_TO_EDGE);
		fmt.minFilter(GL_NEAREST);
		fmt.magFilter(GL_NEAREST);

		// build a surface for the attribute.
		ci::Surface16uRef surf = ci::Surface16u::create(mWidth,mHeight, true, ci::SurfaceChannelOrder::RGBA);

		// loop through and apply each pieces of data 
		int count = 0;
		for (int i = 0; i < mWidth; i++) {
			for (int j = 0; j < mHeight; ++j) {
				if (count < mData.size()) {
					auto pix = mData[count];
					surf->setPixel(vec2(j,i), ColorA(pix.x, pix.y, pix.z, pix.w));
					count++;
				}
			}
		}
		
		ComputeAttribute * attrib = new ComputeAttribute();
		attrib->tex1 = gl::Texture::create(*surf.get(), fmt);
		attrib->tex2 = gl::Texture::create(*surf.get(), fmt);

		mAttributes.push_back(*attrib);

		free(attrib);
	}

	void compile() {
		
		gl::Fbo::Format fmt,fmt2;
		
		for (int i = 0; i < mAttributes.size(); ++i) {
			fmt.attachment(GL_COLOR_ATTACHMENT0 + i, mAttributes[i].tex1);
			fmt.attachment(GL_COLOR_ATTACHMENT0 + i, mAttributes[i].tex2);
		}

		mFbos[0] = gl::Fbo::create(mWidth, mHeight, fmt);
		mFbos[1] = gl::Fbo::create(mWidth, mHeight, fmt2);

	}

	void update() {

		// send current lookup
		mShader->uniform("uLookup", mLookupPosition);

		// now we update lookup 
		if (mCurrentWidth < mWidth) {
			mCurrentWidth += 1;
		}
		else {
			if (mCurrentHeight < mHeight) {
				mCurrentHeight += 1;
				mCurrentWidth = 0;
			}
			else {
				mCurrentHeight = 0;
				mCurrentWidth = 0;
			}

		}

		mLookupPosition.x = mCurrentWidth;
		mLookupPosition.y = mCurrentHeight;



		// swap flags for ping-pong
		std::swap(mCurrentFlag, mOtherFlag);
	}
};