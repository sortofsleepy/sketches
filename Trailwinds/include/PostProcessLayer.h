#pragma once

#include "cinder/gl/Fbo.h"
#include "cinder/app/App.h"
#include "cinder/gl/GlslProg.h"
#include "cinder/gl/gl.h"
#include "cinder/Log.h"
#include <memory>

using namespace ci;
using namespace std;

#define STRINGIFY(A) #A


typedef std::shared_ptr<class PostProcessLayer> PostProcessRef;

class PostProcessLayer {


	//! FBO to render the scene. 
	ci::gl::FboRef mFbo;

	//! Shader to run the post-process effect on
	ci::gl::GlslProgRef mShader;

	//! input texture to run post process effect on.
	ci::gl::TextureRef mInput;

	//! Batch to draw rect
	ci::gl::BatchRef mBatch;

	//! Fullscreen vertex shader
	std::string vertex = STRINGIFY(
		uniform mat4 ciModelViewProjection;
		in vec3 ciPosition;
		in vec2 ciTexCoord0;

		out vec2 vUv;

		void main() {
			vUv = ciTexCoord0;
			gl_Position = ciModelViewProjection * vec4(ciPosition, 1.);
		}
	);

public:
	PostProcessLayer(
		//std::string fragment,
		ci::DataSourceRef fragment,
		ci::gl::Texture::Format fmt = ci::gl::Texture::Format(),
		int width = app::getWindowWidth(),
		int height = app::getWindowHeight()) {


		gl::Texture::Format _fmt = fmt;
		
		gl::Fbo::Format fboFmt;
		fboFmt.setColorTextureFormat(_fmt);

		mFbo = gl::Fbo::create(width, height, fboFmt);

		gl::GlslProg::Format _sfmt;
		_sfmt.vertex(vertex);
		_sfmt.fragment(fragment);

		mShader = gl::GlslProg::create(_sfmt);

		mBatch = gl::Batch::create(geom::Rect(Rectf(0, 0, 1, 1)), mShader);
	}

	ci::gl::TextureRef getTexture() {
		return mFbo->getColorTexture();
	}

	ci::Area getBounds() {
		return mFbo->getBounds();
	}

	void onresize() {

	}

	void setInput(ci::gl::TextureRef mInput) {
		this->mInput = mInput;
	}

	//! Updates the effect. Pass in an optional lambda to run so you can set uniforms,etc.
	void update(std::function<void(ci::gl::GlslProgRef)> setup=nullptr) {
		gl::ScopedFramebuffer fbo(mFbo);
		gl::ScopedGlslProg shd(mShader);
		gl::ScopedTextureBind tex(mInput, 0);
		gl::clear(ColorA(0, 0, 0, 0));
		gl::setMatricesWindow(mFbo->getSize(), false);


		//set input texture. 
		mShader->uniform("uTex0", 0);

		// set resolution 
		mShader->uniform("resolution", vec2(mFbo->getWidth(),mFbo->getHeight()));

		if (setup != nullptr) {
			setup(mShader);
		}
		gl::ScopedModelMatrix modelScope;
		gl::scale(mFbo->getWidth(), mFbo->getHeight(), 1.0f);

		
		mBatch->draw();
	}
};