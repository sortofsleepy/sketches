//
// Created by sortofsleepy on 9/23/2018.
//

#ifndef PARTICLES_PARTICLESYSTEM_H
#define PARTICLES_PARTICLESYSTEM_H

#include "cinder/Vector.h"
#include "cinder/gl/gl.h"
#include "cinder/Utilities.h"
#include "cinder/gl/Ssbo.h"
#include "cinder/gl/Vbo.h"
#include "cinder/app/App.h"
#include "cinder/Log.h"
#include "cinder/Rand.h"
#include "cinder/gl/Texture.h"
#include "cinder/FileWatcher.h"
#include "cinder/Rand.h"

using namespace ci;
using namespace ci::app;
using namespace std;

// note - to keep memory in alignment, each item needs to be at least
// 4 bytes, so make everything vec4 instead of vec3
#pragma pack( push, 1 )
struct Particle {

	// current position 
	ci::vec4 position;

	// velocity
	ci::vec4 vel;

	// controls oscilation parameters
	ci::vec4 osc;

	// any additional metadata
	ci::vec4 meta;

};
#pragma pack( pop )

class ParticleCompute {
	//gl::SsboRef mParticleBuffer;
	gl::VboRef mIdsVbo;
	gl::VaoRef mAttributes;

	ci::gl::VaoRef vao;

	gl::GlslProgRef mUpdateShader, mRenderShader;
	vector<Particle> particles;

	ci::gl::VboMeshRef mMesh;
	ci::gl::BatchRef mBatch;

	ci::vec2 resolution;

	int mNumParticles;

	ci::vec3 mBounds;

	gl::GlslProg::Format uFmt, rFmt;

	ci::Rand mRnd;
	ci::gl::VboRef mParticleData;
public:
	explicit ParticleCompute(int numParticles = 1000) {
		vao = gl::Vao::create();
		resolution = app::getWindowSize();
		mNumParticles = numParticles;

		vec2 lifetimeRange = vec2(1.0f, 3.0f);


		for (int i = 0; i < numParticles; ++i) {
			Particle p;
			float startY = randFloat(-20.0, 20.0);
			float waveHeight = randInt(-40, 40.0);
			float amplitude = randFloat() + 1.0;
			float lifetime = mRnd.nextFloat(lifetimeRange.x,lifetimeRange.y);

			p.position = vec4(50.0 + randInt(-10,10),startY,randFloat(),1.0);
			p.vel = vec4(randVec3(), 1.0);

			// x = waveHeight 
			// y = starting y origin 
			// z = starting x origin
			// w = starting waveHeight
			p.osc = vec4(waveHeight,p.position.y, 50.0 + randInt(-10, 10),randFloat());
			
			// x  = particle life
			// y = particle speed
			// z particleLifetime
			p.meta = vec4(1.0,randFloat(),lifetime,0.0);

			particles.emplace_back(p);
		}

		// init shaders
		uFmt.compute(loadAsset("shaders/trails/particlecompute.glsl"));
		rFmt.vertex(loadAsset("shaders/trails/particle.vert"));
		rFmt.fragment(loadAsset("shaders/trails/particle.frag"));

		try {
			mUpdateShader = gl::GlslProg::
				create(gl::GlslProg::Format().compute(loadAsset("shaders/trails/particlecompute.glsl")).attribLocation("OffsetValue", 0));
		}
		catch (ci::Exception e) {
			CI_LOG_E(e.what());
		}

		try {
			// build rendering shader. 
			mRenderShader = gl::GlslProg::create(rFmt);
		}
		catch (ci::Exception e) {
			CI_LOG_E(e.what());
		}


		// load particles into ssbo
		//mParticleBuffer = gl::Ssbo::create(particles.size() * sizeof(Particle), particles.data(), GL_STATIC_DRAW);

		mParticleData = gl::Vbo::create(GL_ARRAY_BUFFER, sizeof(Particle) * particles.size(),particles.data(), GL_DYNAMIC_DRAW);

		// generate geometry
		generateGeometry();


	}

	void setBounds(ci::vec3 mBounds) {
		this->mBounds = mBounds;
	}

	ci::gl::VboRef getParticleData() {
		return mParticleData;
	}

	void generateGeometry() {
		//auto geo = geom::Icosphere().subdivisions(0.5);
		auto geo = geom::Teapot();

		mMesh = gl::VboMesh::create(geo);

		int numParticles = particles.size();

		// generate ids so we can look up particles easily when rendering.
		std::vector<GLuint> ids(numParticles);
		GLuint currId = 0;
		ids.push_back(0);
		std::generate(ids.begin(), ids.end(), [&currId]() -> GLuint { return currId++; });

		mIdsVbo = gl::Vbo::create<GLuint>(GL_ARRAY_BUFFER, ids, GL_STATIC_DRAW);

		// generate random rotation offsets
		std::vector<float> rotationOffsets(numParticles);
		std::generate(rotationOffsets.begin(), rotationOffsets.end(), []() -> GLuint {
			return randFloat() + 1.0;
		});

		gl::VboRef rotations = gl::Vbo::create<float>(GL_ARRAY_BUFFER, rotationOffsets, GL_STATIC_DRAW);

		vao->bind();
		rotations->bind();
		gl::vertexAttribPointer(0, 1, GL_FLOAT, GL_FALSE, sizeof(float) * rotationOffsets.size(), 0);
		gl::enableVertexAttribArray(0);
		rotations->unbind();
		vao->unbind();

		geom::BufferLayout instanceDataLayout;
		instanceDataLayout.append(geom::Attrib::CUSTOM_0, 1, 0, 0, 1 /* per instance */);
		mMesh->appendVbo(instanceDataLayout, mIdsVbo);

		geom::BufferLayout rotationLayout;
		rotationLayout.append(geom::Attrib::CUSTOM_1, 1, 0, 0, 1 /* per instance */);
		mMesh->appendVbo(rotationLayout, rotations);


		mBatch = gl::Batch::create(mMesh, mRenderShader, {
			{geom::CUSTOM_0,"particleId"},
			{geom::CUSTOM_1,"rotationOffset"}
			});


	}

	void update() {

		gl::ScopedVao v(vao);
		gl::ScopedGlslProg updateS(mUpdateShader);

		//mParticleBuffer->bindBase(0);
		gl::bindBufferBase(GL_SHADER_STORAGE_BUFFER, 0, mParticleData);


		//mUpdateShader->uniform("maxDistance", 130.0f);
		mUpdateShader->uniform("time", (float)app::getElapsedSeconds());

		// may need to adjust work group size, just dispatch everything for now. 
		
		gl::dispatchCompute(mParticleData->getSize() / sizeof(Particle) / 1, 1, 1);
		//gl::memoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);
		gl::memoryBarrier(GL_VERTEX_ATTRIB_ARRAY_BARRIER_BIT);


	}


	void draw() {

		gl::enableDepthRead();
		gl::enableDepthWrite();


		//gl::ScopedBuffer scopedParticleSsbo(mParticleBuffer);
		mBatch->getGlslProg()->uniform("time", (float)app::getElapsedSeconds());
	
		mBatch->drawInstanced(particles.size());
	}

	void setupWatcher() {
		// setup and watch particle render and update shader

		FileWatcher::instance().watch({
			fs::path("shaders/trails/particle.vert"), 
			fs::path("shaders/trails/particle.frag"),
			fs::path("shaders/trails/particlecompute.glsl")
			
			},
			
			[=](const WatchEvent &event) {

			try {

				mUpdateShader = gl::GlslProg::create(gl::GlslProg::Format().compute(loadAsset("shaders/trails/particlecompute.glsl")));
				
				mRenderShader = gl::GlslProg::create(rFmt);
			}
			catch (const ci::Exception &exc) {
				CI_LOG_E("particle.comp\n" << exc.what());
			}

		});
	}

	void cleanupWatcher() {
		FileWatcher::instance().unwatch({ 
			
		fs::path("shaders/trails/particle.vert"),
			fs::path("shaders/trails/particle.frag"),
			fs::path("shaders/trails/particlecompute.glsl")


		});
	}
};

#endif //PARTICLES_PARTICLESYSTEM_H
