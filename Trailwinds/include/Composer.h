#pragma once

#include <vector>
#include "PostProcessLayer.h"

using namespace std;

class Composer {

	std::vector<PostProcessRef> mLayers;
	ci::gl::TextureRef mInput;
public:
	Composer() = default;
	Composer(ci::gl::TextureRef mInput) {
		this->mInput = mInput;
	}


	Composer& addLayer(PostProcessRef mTex) {
		
		// add new layer
		mLayers.push_back(mTex);

		// re-compile stack 
		compile();

		// return class so we can chain
		return *this;
	}

	ci::Area getBounds() {
		return mInput->getBounds();
	}

	void compile() {

		for (int i = 0; i < mLayers.size(); ++i) {
			auto layer = mLayers[i];
			if (i == 0) {
				layer->setInput(mInput);
			}
			else {
				layer->setInput(mLayers[i-1]->getTexture());
			}
		}

	}

	ci::gl::TextureRef getComposite() {
		return mLayers[mLayers.size() - 1]->getTexture();
	}
};