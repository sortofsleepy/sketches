#pragma once

#include "cinder/gl/Vbo.h"
#include "cinder/gl/GlslProg.h"
#include "cinder/Rand.h"
#include "cinder/gl/VboMesh.h"
#include "cinder/app/App.h"
#include "cinder/gl/gl.h"
#include "cinder/gl/Batch.h"

/**
	Ribbon / Trail class - code is largely based on ideas and research by 
	@simongeilfus
*/
class Trails {

	ci::gl::VboRef mParticleBuffer;
	ci::gl::VboRef mPositionBuffer;
	ci::gl::VboRef mNormalBuffer;
	ci::gl::VboRef mTangentBuffer;
	ci::gl::VboRef mLengthBuffer;

	float minLength, maxLength;

	ci::gl::GlslProgRef mUpdate,mRender;
	ci::gl::VboMeshRef mMesh;
	ci::gl::BatchRef mBatch;

	ci::mat4 mTransform;


public:
	Trails();
	Trails(ci::gl::VboRef mParticlePositions);
	
	// load compute shader for compute version
	Trails& loadCompute();
	void updateCompute(float timeStep);
	Trails& setupCompute();
	void drawCompute();

	// load web compatible update shader
	void loadWebCompatible();
	void updateWeb(float timeStep);

	void cleanup();
	void draw();

	void setupWatcher();
	void cleanupWatcher();

};