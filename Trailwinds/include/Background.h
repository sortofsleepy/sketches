#pragma once

/**
	Code and shaders ported from Yi-Wen Li's excellent experiment
	http://yiwenl.github.io/Sketches/exps/11

	Looking to make own additions to this as well. 
*/

#include "cinder/gl/Batch.h"
#include "cinder/gl/VboMesh.h"
#include "cinder/gl/GlslProg.h"
#include "cinder/app/App.h"
#include "cinder/Log.h"
#include "cinder/gl/gl.h"
#include "cinder/gl/Texture.h"
#include <vector>

using namespace ci;
using namespace std;
class Background {

	ci::gl::VboMeshRef mMesh;
	ci::gl::BatchRef mBatch;
	ci::gl::GlslProgRef mShader;

	float radius;
	float waveFront;
	float waveLength;

	ci::gl::TextureRef mCurrent, mNext;
public:
	Background(float radius=5.5):radius(radius) {
		buildMesh();

		waveFront = radius * 2 + 3;
		waveLength = 1.0;

	}

	void setTextures(ci::gl::TextureRef current, ci::gl::TextureRef next) {
		mCurrent = current;
		mNext = next;
	}

	void draw() {
		gl::ScopedTextureBind tex(mCurrent,0);
		gl::ScopedTextureBind tex2(mNext, 1);
	
		gl::enableDepth();
		gl::enableDepthRead();
		gl::enableDepthWrite();


		mBatch->getGlslProg()->uniform("resolution", app::getWindowSize());
		mBatch->getGlslProg()->uniform("startPosition", vec3(0, radius, 0));
		mBatch->getGlslProg()->uniform("textureCurr", 0);
		mBatch->getGlslProg()->uniform("textureNext", 1);
		mBatch->getGlslProg()->uniform("waveFront", waveFront);
		mBatch->getGlslProg()->uniform("waveLength", waveLength);
		mBatch->getGlslProg()->uniform("time", (float)app::getElapsedSeconds());

		gl::pushMatrices();
		gl::scale(vec3(4));
		mBatch->draw();

		gl::popMatrices();
	}

	void buildMesh() {
		std::vector<ci::vec3> positions;
		std::vector<ci::vec2> uvs;
		std::vector<ci::vec3> normals;
		std::vector<ci::vec3> centers;
		std::vector<uint32_t> indices;

		int count = 0;
		float num = 60.0;
		float uvGap = 1 / num;

		auto getPosition = [=](float i, float j)->vec3 {
			vec3 pos;
			float ry = i / num * M_PI * 2.0;
			float rx = j / num * M_PI - M_PI / 2;

			pos.y = sin(rx) * radius;
			auto r = cos(rx) * radius;
			pos.x = cos(ry)  * r;
			pos.z = sin(ry) * r;

			return pos;
		};

		auto getNormal = [](vec3 p0, vec3 p1, vec3 p2) {
			auto pp0 = p0;
			auto pp1 = p1;
			auto pp2 = p2;

			vec3 v0, v1, n;

			v0 = pp1 - pp0;
			v1 = pp2 - pp0;

			vec3 normal = glm::cross(v1, v0);

			normal = glm::normalize(normal);

			return normal;
		};

		auto getCenter = [](ci::vec3 p0, ci::vec3 p1) {

			return vec3(
				(p0.x - p1.x) / 2.0f,
				(p0.y + p1.y) / 2.0f,
				(p0.z + p1.z) / 2.0f
			);
		};

	

		for (int j = 0; j < num; ++j) {
			for (int i = 0; i < num; ++i) {

				ci::vec3 v0 = getPosition(i, j);
				ci::vec3 v1 = getPosition(i + 1, j);
				ci::vec3 v2 = getPosition(i + 1, j + 1);
				ci::vec3 v3 = getPosition(i, j + 1);
				ci::vec3 n = getNormal(v0, v1, v3);
				ci::vec3 c = getCenter(v0, v2);

				positions.push_back(v0);
				positions.push_back(v1);
				positions.push_back(v2);
				positions.push_back(v3);

				normals.push_back(n);
				normals.push_back(n);
				normals.push_back(n);
				normals.push_back(n);

				centers.push_back(c);
				centers.push_back(c);
				centers.push_back(c);
				centers.push_back(c);
				
				uvs.push_back(vec2(i / num, j / num));
				uvs.push_back(vec2(i / num + uvGap, j / num));
				uvs.push_back(vec2(i / num + uvGap, j / num + uvGap));
				uvs.push_back(vec2(i / num, j / num + uvGap));


				indices.push_back(count * 4 + 0);
				indices.push_back(count * 4 + 1);
				indices.push_back(count * 4 + 2);
				indices.push_back(count * 4 + 0);
				indices.push_back(count * 4 + 2);
				indices.push_back(count * 4 + 3);


		
				count++;

			}
		}


		gl::VboMesh::Layout layout;
		layout.attrib(geom::POSITION, 3);
		layout.attrib(geom::NORMAL, 3);
		layout.attrib(geom::TEX_COORD_0, 2);
		layout.attrib(geom::CUSTOM_0, 3);

		mMesh = gl::VboMesh::create(positions.size(), GL_TRIANGLES, { layout }, indices.size(), GL_UNSIGNED_INT);
		mMesh->bufferAttrib(geom::POSITION, sizeof(vec3) * positions.size(), positions.data());
		mMesh->bufferAttrib(geom::NORMAL, sizeof(vec3) * normals.size(), normals.data());
		mMesh->bufferAttrib(geom::TEX_COORD_0, sizeof(vec2) * uvs.size(), uvs.data());
		mMesh->bufferIndices(sizeof(uint32_t) * indices.size(), indices.data());
		mMesh->bufferAttrib(geom::CUSTOM_0, sizeof(vec3) * centers.size(), centers.data());

		gl::GlslProg::Format fmt;
		fmt.vertex(app::loadAsset("shaders/background/bgvert.glsl"));
		fmt.fragment(app::loadAsset("shaders/background/bgfrag.glsl"));

		try {
			mShader = gl::GlslProg::create(fmt);
		}
		catch (ci::Exception &e) {
			CI_LOG_E("Issue loading shader background shader " << e.what());
		}

		mBatch = gl::Batch::create(mMesh, mShader, {
			{geom::CUSTOM_0, "aCenter"}
		});
		
	}
};