#include "cinder/app/App.h"
#include "cinder/app/RendererGl.h"
#include "cinder/gl/gl.h"
#include "Settings.h"
#include "mocha/renderer/DeferredRenderer.h"
#include "mocha/renderer/Object3D.h"
#include "mocha/core/Camera.h"
#include "Icosahedron.h"
#include "ParticleSystem.h"
#include "LightSystem.h"
#include "Crystal.h"
#include <random>
#include <iterator>
#include <algorithm>
#include "mocha/post/PostProcess.h"
#include "mocha/post/PostProcessEffect.h"


using namespace ci;
using namespace ci::app;
using namespace std;

class DeferredRendererApp : public App {
public:
	void setup() override;
	void update() override;
	void draw() override;
	void setupPostProcess();

	//! setup the whole scene
	void setupScene();

	// setup gui
	mocha::Settings settings;

	// main camera
	mocha::CameraRef mCam;

	// main 3D objects
	mocha::Object3DRef mFloor, mTraceObject;


	// deferred renderer.
	mocha::DeferredRendererRef renderer;

	// main particle system
	ParticleSystemRef system;

	// lighting particle system
	LightSystem lsystem;

	// crystal structure
	CrystalMesh crystal;

	// composites everything together.
	mocha::post::PostProcess post;
	mocha::post::PostEffectRef fxaa;

};

void DeferredRendererApp::setup()
{

	mCam = mocha::Camera::create();
	mCam->setZoom(-10.0);
	mCam->lookAt(vec3(-2.221f, 2.0f, 15.859f), vec3(0.0f, 2.0f, 0.0f));

	mCam->enableUI();

	renderer = mocha::DeferredRenderer::create();
	renderer->setCamera(mCam);
	setupScene();
	setupPostProcess();
}


void DeferredRendererApp::setupPostProcess() {

	gl::GlslProg::Format fxaaFmt;
	fxaaFmt.fragment(app::loadAsset("shaders/fxaa.glsl"));

	fxaa = mocha::post::PostProcessEffect::create(fxaaFmt);

	// add post processing stages 
	post.addStage(fxaa);

	// set up all the inputs into each stage of the pipeline
	post.compile(renderer->getOutput());
	
}


void DeferredRendererApp::setupScene() {
	const gl::VboMeshRef plane = gl::VboMesh::create(geom::Plane()
		.axes(vec3(0.0f, 1.0f, 0.0f), vec3(0.0f, 0.0f, 1.0f))
		.normal(vec3(0.0f, 1.0f, 0.0f))
		.size(vec2(20.0f)));

	mFloor = mocha::Object3D::create(gl::VboMesh::create(geom::Plane()
		.axes(vec3(0.0f, 1.0f, 0.0f), vec3(0.0f, 0.0f, 1.0f))
		.normal(vec3(0.0f, 1.0f, 0.0f))
		.size(vec2(20.0f))));


	// ============= SETUP PARTICLE SYSTEM ============= //      

	crystal = CrystalMesh();
	crystal.genrateCrystal(3);

	auto pos = crystal.getRandomPoints();
	auto positions = pos;
	//auto positions = ico.getStartingPosition();

	system = ParticleSystem::create(positions);
	//system->setup(renderer);

	// ============ SETUP LIGHTING SYSTEM ============ //
	lsystem = LightSystem();
	lsystem.addToRenderer(renderer);


	// =============== SETUP OTHER OBJECTS =============== //


	renderer->addToScene(crystal.getMesh());
	//renderer->addToScene(system->getObject());
}



void DeferredRendererApp::update()
{

	lsystem.update();
	renderer->update();

	//uniform vec4		uExtents;
	fxaa->update([this](gl::GlslProgRef shader)->void {
		auto bounds = fxaa->getBounds();
		const float w = (float)bounds.getWidth();
		const float h = (float)bounds.getHeight();
		shader->uniform("uExtents", vec4(1.0f / w, 1.0f / h, w, h));
	});
}

void DeferredRendererApp::draw()
{
	gl::clear(Color(0, 0, 0));
	mCam->useCamera();
	
	post.draw();


}

CINDER_APP(DeferredRendererApp, RendererGl, [](App::Settings *settings) {
	settings->setWindowSize(1024, 768);
})
