#include "Icosahedron.h"
#include "cinder/Log.h"
#include "cinder/Rand.h"

using namespace ci;
using namespace std;

Icosahedron::Icosahedron(float scale) :mScale(scale){
	setup();
}

void Icosahedron::setup() {

	for (size_t i = 0; i < 60; ++i) {
		auto pos = *reinterpret_cast<const vec3*>(&sPositions[sIndices[i] * 3]);

		pos *= vec3(mScale);

		positions.emplace_back(pos);
		texcoords.emplace_back(*reinterpret_cast<const vec2*>(&sTexCoords[i * 2]));
		indices.push_back((uint32_t)i);
	}

	// calculate the face normal for each triangle
	size_t numTriangles = indices.size() / 3;
	normals.resize(60);
	for (size_t i = 0; i < numTriangles; ++i) {
		const uint32_t index0 = (indices)[i * 3 + 0];
		const uint32_t index1 = (indices)[i * 3 + 1];
		const uint32_t index2 = (indices)[i * 3 + 2];

		const vec3 &v0 = (positions)[index0];
		const vec3 &v1 = (positions)[index1];
		const vec3 &v2 = (positions)[index2];

		vec3 e0 = v1 - v0;
		vec3 e1 = v2 - v0;

		(normals)[index0] = (normals)[index1] = (normals)[index2] = normalize(cross(e0, e1));
	}


	gl::VboMesh::Layout layout;
	layout.attrib(geom::POSITION, 3);
	layout.attrib(geom::TEX_COORD_0, 2);
	layout.attrib(geom::NORMAL, 3);

	mMesh = gl::VboMesh::create(positions.size(), GL_TRIANGLES, { layout }, indices.size(), GL_UNSIGNED_INT);
	mMesh->bufferAttrib(geom::POSITION, sizeof(vec3) * positions.size(), positions.data());
	mMesh->bufferAttrib(geom::NORMAL, sizeof(vec3) * normals.size(), normals.data());
	mMesh->bufferAttrib(geom::TEX_COORD_0, sizeof(vec2) * texcoords.size(), texcoords.data());
	mMesh->bufferIndices(sizeof(uint32_t) * indices.size(), indices.data());
}

std::vector<ci::vec3> Icosahedron::getStartingPosition() {

	int facesLength = indices.size() / 3;

	vector <ci::vec3> startPositions;

	for (int i = 0; i < positions.size(); ++i) {

		int faceIdx = floor(randFloat() * facesLength);
		uint32_t facea = indices[faceIdx];
		uint32_t faceb = indices[faceIdx + 1]; 
		uint32_t facec = indices[faceIdx + 2];

		vec3 vertex1 = positions[facea];
		vec3 vertex2 = positions[randFloat() > 0.5 ? faceb : facec];

		vec3 point = vertex2 - vertex1;
		point *= vec3(randFloat());
		point += vertex1;

		startPositions.push_back(point);
	}

	return startPositions;
}


#undef PHI	// take the reciprocal of phi, to obtain an icosahedron that fits a unit cube
#define PHI (1.0f / ((1.0f + math<float>::sqrt(5.0f)) / 2.0f))
float Icosahedron::sPositions[12 * 3] = {
	-PHI, 1.0f, 0.0f,    PHI, 1.0f, 0.0f,   -PHI,-1.0f, 0.0f,    PHI,-1.0f, 0.0f,
	0.0f, -PHI, 1.0f,   0.0f,  PHI, 1.0f,   0.0f, -PHI,-1.0f,   0.0f,  PHI,-1.0f,
	1.0f, 0.0f, -PHI,   1.0f, 0.0f,  PHI,  -1.0f, 0.0f, -PHI,  -1.0f, 0.0f,  PHI };
float Icosahedron::sTexCoords[60 * 2] = {
	0.0f, 0.0f, 0.0f, 1.0f, 1.0f, 0.0f,
	0.0f, 0.0f, 0.0f, 1.0f, 1.0f, 0.0f,
	0.0f, 0.0f, 0.0f, 1.0f, 1.0f, 0.0f,
	0.0f, 0.0f, 0.0f, 1.0f, 1.0f, 0.0f,
	0.0f, 0.0f, 0.0f, 1.0f, 1.0f, 0.0f,
	1.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f,
	1.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f,
	1.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f,
	1.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f,
	1.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f,
	1.0f, 1.0f, 0.0f, 1.0f, 1.0f, 0.0f,
	1.0f, 1.0f, 0.0f, 1.0f, 1.0f, 0.0f,
	1.0f, 1.0f, 0.0f, 1.0f, 1.0f, 0.0f,
	1.0f, 1.0f, 0.0f, 1.0f, 1.0f, 0.0f,
	1.0f, 1.0f, 0.0f, 1.0f, 1.0f, 0.0f,
	1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f,
	1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f,
	1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f,
	1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f,
	1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f };
uint32_t Icosahedron::sIndices[60] = {
	0,11, 5, 0, 5, 1, 0, 1, 7, 0, 7,10, 0,10,11,
	5,11, 4, 1, 5, 9, 7, 1, 8,10, 7, 6,11,10, 2,
	3, 9, 4, 3, 4, 2, 3, 2, 6, 3, 6, 8, 3, 8, 9,
	4, 9, 5, 2, 4,11, 6, 2,10, 8, 6, 7, 9, 8, 1 };