in vec4 position;
in vec4 velocity;
in vec4 originPosition;

uniform float timer;

out vec4 oPosition;
out vec4 oVelocity;

#include "noise.glsl"

vec3 snoiseVec3( vec3 x ){

	float s  = snoise(vec3( x ));
	float s1 = snoise(vec3( x.y - 19.1 , x.z + 33.4 , x.x + 47.2 ));
	float s2 = snoise(vec3( x.z + 74.2 , x.x - 124.5 , x.y + 99.4 ));
	vec3 c = vec3( s , s1 , s2 );
	return c;

}


vec3 curlNoise( vec3 p ){

	const float e = .1;
	vec3 dx = vec3( e   , 0.0 , 0.0 );
	vec3 dy = vec3( 0.0 , e   , 0.0 );
	vec3 dz = vec3( 0.0 , 0.0 , e   );

	vec3 p_x0 = snoiseVec3( p - dx );
	vec3 p_x1 = snoiseVec3( p + dx );
	vec3 p_y0 = snoiseVec3( p - dy );
	vec3 p_y1 = snoiseVec3( p + dy );
	vec3 p_z0 = snoiseVec3( p - dz );
	vec3 p_z1 = snoiseVec3( p + dz );

	float x = p_y1.z - p_y0.z - p_z1.y + p_z0.y;
	float y = p_z1.x - p_z0.x - p_x1.z + p_x0.z;
	float z = p_x1.y - p_x0.y - p_y1.x + p_y0.x;

	const float divisor = 1.0 / ( 2.0 * e );
	return normalize( vec3( x , y , z ) * divisor );

}

float rand(vec2 co){
			    return fract(sin(dot(co.xy ,vec2(12.9898,78.233))) * 43758.5453);
			}
void main(){

	oPosition = position;
	oVelocity = velocity;

	
	if ( rand( oVelocity.xy + timer ) > 0.99 || oPosition.a <= 0.0 ) {

		oPosition = originPosition;
		oPosition.a = 1.0;
	}else{

		float x = oPosition.x + timer;
		float y = oPosition.y + timer;
		float z = oPosition.z + timer;

		

		oPosition.x += sin( y * 0.033 ) * cos( z * 0.037 ) * 0.4;
		oPosition.y += sin( x * 0.035 ) * cos( x * 0.035 ) * 0.4;
		oPosition.z += sin( x * 0.037 ) * cos( y * 0.033 ) * 0.4;

	
		oPosition.a -= 0.00001;
	}

}

/*
oPosition.xyz = curlNoise(oPosition.xyz);
		float x = oPosition.x + timer;
		float y = oPosition.y + timer;
		float z = oPosition.z + timer;

		oPosition.x += sin( y * 0.033 ) * cos( z * 0.037 ) * 0.4;
		oPosition.y += sin( x * 0.035 ) * cos( x * 0.035 ) * 0.4;
		oPosition.z += sin( x * 0.037 ) * cos( y * 0.033 ) * 0.4;

		*/