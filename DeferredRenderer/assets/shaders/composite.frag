
uniform sampler2D mainScene;
in vec2 vUv;

out vec4 glFragColor;
void main(){

	vec4 main = texture(mainScene,vUv);
	
	glFragColor = main;

}