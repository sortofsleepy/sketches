#version 330 

uniform mat4 ciModelViewProjection;

in vec4 ciPosition;

out float life;
void main(){

	life = ciPosition.a + 0.5;
	gl_PointSize = 2.0;
	gl_Position = ciModelViewProjection * ciPosition;
}
