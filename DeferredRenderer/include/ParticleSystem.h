#pragma once

#include "mocha/gpu/TransformFeedback.h"
#include <vector>
#include "cinder/Vector.h"
#include "cinder/Rand.h"

using namespace ci;
using namespace std;          
using namespace mocha;


typedef std::shared_ptr<class ParticleSystem>ParticleSystemRef;
class ParticleSystem {


	vector<ci::vec4> positions;
	vector<ci::vec4> velocities;
	vector<ci::vec4> sphereData;
	vector<ci::vec4> originData;

	ci::gl::VboMeshRef mMesh;
	ci::gl::BatchRef mBatch;

	mocha::gpu::TFBufferRef buffer;
	mocha::Object3DRef mObject;
	ci::gl::GlslProgRef mShader;

	int numInstances;

public:
	static ParticleSystemRef create(std::vector<ci::vec3> &startingPositions) {
		auto system = new ParticleSystem(startingPositions);
		return ParticleSystemRef(system);
	}
	ParticleSystem(std::vector<ci::vec3> &startingPositions) {
		for (int i = 0; i < startingPositions.size(); ++i) {
			
			auto position = vec4(startingPositions[i], 1.0);

			// build spherical data
			auto phi = randFloat();
			auto phiSpeed = randFloat();
			auto theta = randFloat();
			auto thetaSpeed = randFloat();

			positions.push_back(position);
			originData.push_back(position);
			velocities.push_back(vec4(randVec3(), 1.0));
		}

		numInstances = startingPositions.size();
		
	
	}

	~ParticleSystem() {
	}

	void setup(mocha::DeferredRendererRef renderer) {

		// create a transform feedback system
		buffer = mocha::gpu::TFBuffer::create();

		// set the number of items to run transform feedback over
		buffer->setNumPoints(positions.size());
	
		// load the update shader
		buffer->loadShader("shaders/particleUpdate.glsl", {
			"oPosition",
			"oVelocity"
		});
		
		// set buffers to run transform feedback process over
		// Remember index is assigned based on order added.
		buffer->bufferAttrib(positions);
		buffer->bufferAttrib(velocities);
		buffer->bufferAttrib(originData);

		// setup data onto vaos
		buffer->vertexAttribPointer(0, 4);
		buffer->vertexAttribPointer(1, 4);
		buffer->vertexAttribPointer(2, 4);

		// setup layout for the mesh we want to render. We do it like this because we'll be utilizing VBOs from the transform feedback system 

		geom::BufferLayout _buffer;
		_buffer.append(geom::POSITION, 3, 0, 0);

		// build the mesh. We just need to render the position
		gl::VboMeshRef mMesh = gl::VboMesh::create(positions.size(), GL_POINTS, { {_buffer,buffer->getVbo(0)} });
		
		// setup the shader. We use the default vertex shader but will use custom fragment.
		gl::GlslProg::Format shader;
		shader.fragment(app::loadAsset("shaders/particleRender.frag"));

		// build deferred engine object
		mObject = mocha::Object3D::create(mMesh,shader);

		// use custom render function instead of default render
		mObject->setRenderFunction(std::bind(&ParticleSystem::draw,this,std::placeholders::_1));
	
		// add particle system to scene
		renderer->addToScene(mObject);

	}

	mocha::Object3DRef getObject() { return mObject; }


	void draw(mocha::Object3D * obj) {
		gl::ScopedState state(GL_PROGRAM_POINT_SIZE, true);
		gl::ScopedMatrices mats;
		
		gl::translate(vec3(0, 1, 0));

		buffer->update([=](gl::GlslProgRef shader)->void{
			shader->uniform("timer", (float)app::getElapsedSeconds() * 0.00005f);
		});


		obj->uniform("pointSize", 4.0f);
		obj->getBatch()->draw();

		
	}
};