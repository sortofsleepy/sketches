#pragma once
#include <string>
#include "CinderImGui.h"
#include "cinder/Vector.h"

namespace mocha {



	class Setting {
	public:
		Setting() = default;
		virtual void render() {};
		void setName(std::string name) {
			this->name = name;
		}

		std::string getName() {
			return name;
		}
	protected:
		std::string name = "";
	};



	// ======= DIFFERENT GUI TYPES ============== //

	template<typename T>
	class SliderItem : public Setting {

	public:
		SliderItem() = default;
		SliderItem(T value) {
			this->value = value;
		}

		SliderItem& setMin(T min);
		SliderItem& setMax(T max);

		T& getValue() {
			return value;
		}
		T& getMin() {
			return min;
		}


		T& getMax() {
			return max;
		}

		void render();
	private:
		T value;
		T min;
		T max;
	};


	// template class decs 
	template class SliderItem<float>;
	template class SliderItem<glm::vec3>;
}