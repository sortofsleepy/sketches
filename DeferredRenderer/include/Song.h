#pragma once


#ifdef CINDER_EMSCRIPTEN



#else

#include "cinder/audio/Voice.h"
#include "cinder/audio/audio.h"
#endif

class Song {


public:
	Song();
	void loadSong(std::string path);
};