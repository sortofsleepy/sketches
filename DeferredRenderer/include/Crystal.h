#pragma once

#include "cinder/Rand.h"
#include "cinder/gl/gl.h"
#include "cinder/GeomIo.h"
#include <vector>
#include "mocha/core/math.h"
#include "mocha/renderer/Object3D.h"

using namespace ci;
using namespace std;

class CrystalMesh {

	vector<ci::vec3> mVertices;
	vector<uint32_t> mIndices;
	ci::gl::VboMeshRef mMesh;
	ci::gl::BatchRef mBatch;
	ci::gl::GlslProgRef mShader;

	
	mocha::Object3DRef object;

public:
	CrystalMesh() {
	
		mShader = gl::getStockShader(gl::ShaderDef().color());

	}

	vector<ci::vec3> getPositions() {
		return mVertices;
	}

	void genrateCrystal(int scale = 1) {

		vec3 up = vec3(0, 1, 0);
		float min = randFloat(1, 5);
		float max = randFloat(1, 10);
		float minOffset = randFloat(1, 2);
		float maxOffset = randFloat(1, 2);

		int numElements = randInt(5,40);

		// holds final shape + transforms
		geom::SourceMods shape;

		for (int i = 0; i < numElements; ++i) {


			AxisAlignedBox bounds;
			geom::SourceMods g = randShape();

			auto sx = randFloat(0.1, 1);
			auto sy = randFloat(0.1, 1);
			auto sz = randFloat(min, max);

			// grab bounding box
			g = g >> geom::Bounds(&bounds);

			// scale mesh
			vec3 scale = vec3(sx, sy, sz);

			// shorten extremely long pieces.
			if (scale.z > 2) {
				scale *= 0.5;
			}

			g = g >> geom::Transform(glm::scale(scale));
		
			// calculate offset (may just need to set manually)
			float offset = randFloat(minOffset, maxOffset);
			g = g >> geom::Transform(glm::translate(vec3(0, 0, bounds.getMax().z * offset)));


			// rotate element
			float d = std::min(sx, sy);
			vec3 p = vec3(randFloat(-d, d), randFloat(-d, d), randFloat(-d, d));

			g = g >> geom::Transform(glm::lookAt(vec3(0), p, up));


			shape = shape & g;

		}


		mMesh = gl::VboMesh::create(shape);
		mVertices.clear();



		// there isn't an easy way to extract attributes from built in geom sources, so unmesh things. 
		auto mappedPosAttrib = mMesh->mapAttrib3f(geom::Attrib::POSITION, false);
		for (int i = 0; i < mMesh->getNumVertices(); i++) {
			vec3 &pos = *mappedPosAttrib;
			mVertices.push_back(pos);
			++mappedPosAttrib;
		}
		mappedPosAttrib.unmap();


		// grab index data.
		gl::VboRef idxVBO = mMesh->getIndexVbo();
	
		int idxSize = sizeof(uint32_t) * mMesh->getNumIndices();

		void * indices = idxVBO->map(GL_READ_ONLY);

		unsigned short * idxArray = static_cast<unsigned short*>(indices);

		mIndices.clear();

		for (int i = 0; i < mMesh->getNumIndices(); ++i) {
			auto val = mocha::math::unsignedShortToInt(idxArray[i]);
			mIndices.push_back(val);
		}

		idxVBO->unmap();


		//object = mocha::Object3D::create(gl::VboMesh::create(geom::Teapot()));
		object = mocha::Object3D::create(mMesh);

		// TODO get rid of this cause we don't need it.
		//mBatch = gl::Batch::create(mMesh, mShader);

		object->setRenderFunction(std::bind(&CrystalMesh::draw, this,std::placeholders::_1));
	
	}

	//! Return random points based on position of crystal. 
	vector<ci::vec3> getRandomPoints() {
	
		// we want to store random points on the generated face.
		auto faces = mocha::math::indicesToFaces(mIndices);


		for (int i = 0; i < mVertices.size(); ++i) {
			auto face = faces[randFloat() * faces.size()];


			vec3 vertex1 = mVertices[face.x];
			vec3 vertex2;
			if (randFloat() > 0.5) {
				vertex2 = mVertices[face.y];
			}
			else {
				vertex2 = mVertices[face.z];
			}

			vec3 point = vertex2 - vertex1;
			point *= randVec3();
			point += vertex1;
			
			mVertices[i] = point;
		}

		return mVertices;
	}

	//! Draws the Object3D
	void draw(mocha::Object3D * obj) {
		gl::ScopedMatrices mats;
		gl::scale(vec3(2));
		obj->getBatch()->draw();
	}

	//! Returns the Object3D reference
	mocha::Object3DRef getMesh() {
		return object;
	}

	//! Generates a random shape
	geom::SourceMods randShape() {
		// generate base geometry 
		int geometryID = randInt(0, 2);

		geom::SourceMods baseShape;

		switch (geometryID) {
		case 0:
			baseShape = geom::Cube();
			break;

		case 1:
			baseShape = geom::Icosahedron();
			break;

		case 2:
			baseShape = geom::Cylinder();
			break;
		}

		return baseShape;
	}
};