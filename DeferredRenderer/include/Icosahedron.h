#ifndef Icosahedron_hpp
#define Icosahedron_hpp

#include "cinder/gl/gl.h"
#include "cinder/gl/VboMesh.h"
#include "cinder/gl/Batch.h"
#include "mocha/renderer/Object3D.h"

class Icosahedron {
protected:
	ci::gl::VboMeshRef mMesh;

	mutable std::vector<ci::vec3> positions, normals, colors;
	mutable std::vector<ci::vec2> texcoords;
	mutable std::vector<uint32_t> indices;

	static float sPositions[12 * 3];
	static float sTexCoords[60 * 2];
	static uint32_t sIndices[60];

	float mScale;


public:
	Icosahedron(float scale = 1.0f);
	void setup();

	mocha::Object3DRef getMesh() { return mocha::Object3D::create(mMesh); }
	//ci::gl::VboMeshRef getMesh() { return mMesh; }
	std::vector<ci::vec3> getStartingPosition();
};

#endif