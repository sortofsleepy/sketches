#include "GuiTypes.h"
#include "cinder/Log.h"
namespace mocha {



	// ======= DIFFERENT GUI TYPES ============== //

	template<typename T>
	SliderItem<T>& SliderItem<T>::setMin(T value) {
		this->min = value;
		return *this;
	}

	template<typename T>
	SliderItem<T>& SliderItem<T>::setMax(T value) {
		this->max = value;
		return *this;
	}


	template<typename T>
	void SliderItem<T>::render() {
		if (std::is_same<T, float>::value) {	
			auto v = dynamic_cast<SliderItem<float>&>(*this);
			ImGui::SliderFloat(name.c_str(), &v.getValue(), v.getMin(), v.getMax());
		}
	}



}
