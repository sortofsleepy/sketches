#include "mocha/post/PostProcess.h"

using namespace ci;                                                                                                                                 
namespace mocha {
	namespace post {
		PostProcessEffect::PostProcessEffect(ci::gl::GlslProg::Format shader) {

			if (shader.getVertex() == "") {
				shader.vertex(vertex);
			}

			mShader = gl::GlslProg::create(shader);


			mBatch = gl::Batch::create(geom::Rect(Rectf(0, 0, 1, 1)), mShader);
			resizeFbo(app::getWindowWidth(), app::getWindowHeight());
		}

		void PostProcessEffect::resizeFbo(int width, int height) {
			ci::gl::Fbo::Format fmt;
			ci::gl::Texture::Format tfmt;

			tfmt.setWrap(GL_CLAMP_TO_EDGE, GL_CLAMP_TO_EDGE);
			tfmt.minFilter(GL_NEAREST);
			tfmt.magFilter(GL_NEAREST);
			tfmt.loadTopDown(true);
			fmt.setColorTextureFormat(tfmt);
			mFbo = ci::gl::Fbo::create(width, height, fmt);
		}

		void PostProcessEffect::update(std::function<void(ci::gl::GlslProgRef)> mfunc) {
			// if input isn't set - don't run update function
			if (mInput == nullptr) {
				throw std::exception("PostProcessEffect error - no input image");
				return;
			}

		

			gl::ScopedFramebuffer fbo(mFbo);
			gl::ScopedGlslProg shd(mShader);
			gl::ScopedTextureBind tex(mInput, 0);
			gl::clear(ColorA(0, 0, 0, 0));
			gl::setMatricesWindow(mFbo->getSize(), false);

			//set input texture. 
			mShader->uniform("uTex0", 0);

			// set resolution 
			mShader->uniform("resolution", vec2(mFbo->getWidth(), mFbo->getHeight()));

			// if user passes in lambda - run function and pass back shader
			if (mfunc != nullptr) {
				mfunc(mShader);
			}

			gl::ScopedModelMatrix modelScope;
			gl::scale(mFbo->getWidth(), mFbo->getHeight(), 1.0f);

			mBatch->draw();

		}

		void PostProcessEffect::setInput(ci::gl::TextureRef Input) {
			mInput = Input;
		}
	
} }