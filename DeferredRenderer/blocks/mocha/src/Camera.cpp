#include "mocha/core/Camera.h"

using namespace ci;
using namespace std;

namespace mocha {
	Camera::Camera(float fov, float aspect, float near, float far):
	eye(vec3(0,0,50)),
	target(vec3(0,0,0)) {
		mCam = CameraPersp(app::getWindowWidth(), app::getWindowHeight(), fov, near, far).calcFraming(Sphere(vec3(0), 200));
		mCam.lookAt(eye, target);
		
	
		mCam.setAspectRatio(aspect);


	}

	void Camera::setFov(float fov) {
		mCam.setFov(fov);
	}

	void Camera::lookAt(ci::vec3 eye, ci::vec3 target) {
		mCam.lookAt(eye, target);
	}

	void Camera::enableUI() {
		mCamUi = CameraUi(&mCam, app::getWindow(), -1);
	}

	void Camera::setZoom(float zoom) {
		auto currentEye = mCam.getEyePoint();
		currentEye.z = zoom;
		mCam.lookAt(currentEye, target);
	}

	void Camera::useCamera() {
		gl::setMatrices(mCam);
	}
}