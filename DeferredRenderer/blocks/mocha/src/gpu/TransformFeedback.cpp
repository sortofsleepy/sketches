//
//  TFBuffer.cpp
//  VolParticles
//
//  Created by Joseph Chow on 7/14/17.
//
//

#include "mocha/gpu/TransformFeedback.h"
#include "cinder/Log.h"
using namespace ci;
using namespace std;

namespace mocha {
	namespace gpu {

		TFBuffer::TFBuffer() :
			mIterationsPerFrame(1),
			numPoints(100),
			mode(GL_POINTS),
			updateShaderSet(false) {
			mVaos[0] = gl::Vao::create();
			mVaos[1] = gl::Vao::create();
		}
		void TFBuffer::setNumPoints(int numPoints) {
			this->numPoints = numPoints;
		}

		void TFBuffer::setFeedbackMode(GLenum mode) {
			this->mode = mode;
		}
		std::array<ci::gl::VboRef, 2> TFBuffer::getAttributeSet(int index) {
			return mVbos[index];
		}

		void TFBuffer::loadShader(std::string updatepath, std::vector<std::string> feedbackVaryings, GLenum feedbackType, std::function<ci::gl::GlslProg::Format(ci::gl::GlslProg::Format)> fn) {
			gl::GlslProg::Format fmt;

			// set vertex shader. Transform feedback discards fragment stage entirely so we don't need to setup
			// a fragment shader
			fmt.vertex(app::loadAsset(updatepath));

			// set seperate attribs by default.
			fmt.feedbackFormat(feedbackType);

			// We also send the names of the attributes to capture
			fmt.feedbackVaryings(feedbackVaryings);

			// do any additional custom format setup
			if (fn != nullptr) {
				fmt = fn(fmt);
			}

			fmt.version(330);

			// build the shader
			try {
				mUpdateShader = gl::GlslProg::create(fmt);
			}
			catch (gl::GlslProgCompileExc &e) {
				CI_LOG_E("Error compiling Transform feedback shader - error is : " << e.what());
			}

			// set flag indicating shader is built.
			updateShaderSet = true;

			this->feedbackType = feedbackType;

		}

		void TFBuffer::vertexAttribPointerInterleaved(std::function<void()> fn) {
			for (int i = 0; i < 2; ++i) {
				gl::ScopedVao vao(mVaos[i]);
				gl::ScopedBuffer vbo(mVbos[0][i]);

				fn();

			}
		}

		void TFBuffer::setIterationsPerFrame(int mIterationsPerFrame) {
			this->mIterationsPerFrame = mIterationsPerFrame;
		}


		void TFBuffer::update(std::function<void(ci::gl::GlslProgRef)> fn) {

			gl::ScopedGlslProg prog(mUpdateShader);
			gl::ScopedState rasterizer(GL_RASTERIZER_DISCARD, true);	// turn off fragment stage


			if (fn != nullptr) {
				fn(mUpdateShader);
			}


			for (int i = mIterationsPerFrame; i != 0; --i) {


				// Bind the source data (Attributes refer to specific buffers).
				gl::ScopedVao source(mVaos[mSourceIndex]);

				for (int a = 0; a < mVbos.size(); ++a) {
					gl::bindBufferBase(GL_TRANSFORM_FEEDBACK_BUFFER, a, mVbos[a][mDestinationIndex]);
				}


				gl::beginTransformFeedback(mode);

				// Draw source into destination, performing our vertex transformations.
				gl::drawArrays(mode, 0, numPoints);

				gl::endTransformFeedback();

				// Swap source and destination for next loop
				std::swap(mSourceIndex, mDestinationIndex);
			}
		}

		void TFBuffer::vertexAttribPointer(int bufferIndex, int size, GLenum data_type, GLboolean normalized, int stride, const GLvoid * pointer) {

			auto bufferSet = mVbos[bufferIndex];

			for (int i = 0; i < 2; ++i) {
				gl::ScopedVao vao(mVaos[i]);
				gl::ScopedBuffer vbo(bufferSet[i]);

				if (data_type == GL_FLOAT) {
					gl::vertexAttribPointer(bufferIndex, size, data_type, normalized, stride, pointer);
				}
				else if (data_type == GL_INT) {
					gl::vertexAttribIPointer(bufferIndex, size, data_type, stride, pointer);
				}

				gl::enableVertexAttribArray(bufferIndex);
			}

		}

	}
}