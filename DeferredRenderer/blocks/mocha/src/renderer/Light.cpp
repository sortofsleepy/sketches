
#include "mocha/renderer/Light.h"
using namespace ci;

namespace mocha {
	Light::Light() :mColorAmbient(Colorf::black()),
		mColorDiffuse(Colorf::white()), mColorSpecular(Colorf::white()),
		mIntensity(1.0f), mPosition(vec3(0.0f,0.0f,0.0f)), mRadius(1.0f),
		mVolume(10.0f) {}

	Light& Light::colorDiffuse(ci::ColorA mColorDiffuse) {

		this->mColorDiffuse = mColorDiffuse;

		return *this;
	}

	Light& Light::colorAmbient(ci::ColorA mColorAmbient) {

		this->mColorAmbient = mColorAmbient;

		return *this;
	}

	Light& Light::intensity(float mIntensity) {
		this->mIntensity = mIntensity;

		return *this;
	}

	Light& Light::position(ci::vec3 position) {
		this->mPosition = position;

		return *this;
	}

	Light& Light::radius(float mRadius) {
		this->mRadius = mRadius;
		return *this;
	}

	Light& Light::volume(float mVolume) {
		this->mVolume = mVolume;
		return *this;
	}
}