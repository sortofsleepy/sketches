
#include "mocha/renderer/DeferredRenderer.h"
#include "cinder/Log.h"
using namespace ci;
using namespace std;


namespace mocha {

	DeferredRenderer::DeferredRenderer() :mGenerateShadows(false), mNear(0.1), mFar(1000.0) {
	


		// add default light. 
		LightRef light = Light::create();
		light->colorDiffuse(ColorAf(0.95f, 1.0f, 0.92f, 1.0f))
			.intensity(0.8f).position(vec3(0.0f, 10.0f, 0.0f))
			.radius(0.1f).volume(15.0f);

		addLight(
			light
		);


		// add light cube to render lighting
		mLightCube = gl::Batch::create(
			gl::VboMesh::create(geom::Cube().size(vec3(2.0f))), 
			gl::GlslProg::create(gl::GlslProg::Format()
				.vertex(deferred::passThruVertex)
				.fragment(deferred::lBuffer))
		);
	

	}
	void DeferredRenderer::resize(int width,
		int height,
		int shadowMapSize) {

		viewWidth = width;
		viewHeight = height;
	
	}

	void DeferredRenderer::addLight(LightRef light) {
		mLightSources.push_back(light);

		// TODO only make these if debugging. 
		mLightObjects.push_back(gl::Batch::create(
			gl::VboMesh::create(geom::Icosahedron()),
			gl::getStockShader(gl::ShaderDef().color())
		));
	}

	void DeferredRenderer::generateBuffers() {
	
		// Color texture format
		const gl::Texture2d::Format colorTextureFormat = gl::Texture2d::Format()
			.internalFormat(GL_RGBA8)
			.magFilter(GL_NEAREST)
			.minFilter(GL_NEAREST)
			.wrap(GL_CLAMP_TO_EDGE);

		// Data texture format
		const gl::Texture2d::Format dataTextureFormat = gl::Texture2d::Format()
			.internalFormat(GL_RGBA16F)
			.magFilter(GL_NEAREST)
			.minFilter(GL_NEAREST)
			.wrap(GL_CLAMP_TO_EDGE);

		// Create FBO for the the geometry buffer (G-buffer). This G-buffer uses
		// three color attachments to store position, normal, emissive (light), and
		// color (albedo) data.

		const int32_t h = viewHeight;
		const int32_t w = viewWidth;

		try {
			gl::Fbo::Format fboFormat;
			mTextureFboGBuffer[0] = gl::Texture2d::create(w, h, colorTextureFormat);
			mTextureFboGBuffer[1] = gl::Texture2d::create(w, h, dataTextureFormat);
			mTextureFboGBuffer[2] = gl::Texture2d::create(w, h, dataTextureFormat);
			mTextureFboGBuffer[3] = gl::Texture2d::create(w, h, dataTextureFormat);
			for (size_t i = 0; i < 4; ++i) {
				fboFormat.attachment(GL_COLOR_ATTACHMENT0 + (GLenum)i, mTextureFboGBuffer[i]);
			}
			fboFormat.depthTexture();
			mGBuffer = gl::Fbo::create(w, h, fboFormat);
			const gl::ScopedFramebuffer scopedFramebuffer(mGBuffer);
			const gl::ScopedViewport scopedViewport(ivec2(0), mGBuffer->getSize());
			gl::clear();
		}
		catch (const std::exception& e) {
			app::console() << "mFboGBuffer failed: " << e.what() << std::endl;
		}

		// Create FBO for the light buffer (L-buffer). The L-buffer reads the
		// G-buffer textures to render the scene inside light volumes.
		try {
			mLBuffer = gl::Fbo::create(w, h, gl::Fbo::Format().colorTexture());
			const gl::ScopedFramebuffer scopedFramebuffer(mLBuffer);
			const gl::ScopedViewport scopedViewport(ivec2(0), mLBuffer->getSize());
			gl::clear();
		}
		catch (const std::exception& e) {
			app::console() << "mLBuffer failed: " << e.what() << std::endl;
		}
	}

	void DeferredRenderer::setupShadowmap(GLsizei sz, vec3 eye, vec3 target) {
		mShadowBuffer = gl::Fbo::create(sz, sz, gl::Fbo::Format().depthTexture());
		const gl::ScopedFramebuffer scopedFramebuffer(mShadowBuffer);
		const gl::ScopedViewport scopedViewport(ivec2(0), mShadowBuffer->getSize());
		gl::clear();
		mShadowBuffer->getDepthTexture()->setCompareMode(GL_COMPARE_REF_TO_TEXTURE);

		// Set up shadow camera
		mShadowCamera.setPerspective(120.0f, mShadowBuffer->getAspectRatio(), mCamera->getNear(), mCamera->getFar());
		mShadowCamera.lookAt(eye, target);

		mGenerateShadows = true;

	}
	void DeferredRenderer::setCamera(float fov, float aspect, float near, float far, vec3 eye, vec3 target) {
		
		/*
		const vec2 windowSize = app::toPixels(app::getWindowSize());
		mCamera = CameraPersp(windowSize.x, windowSize.y, fov, near, far);
		mCamera.lookAt(eye, target);
		mCamUi = CameraUi(&mCamera, app::getWindow(), -1);
		mNear = near;
		mFar = far;
		mFov = fov;*/

	}

	void DeferredRenderer::draw() {
		// ================= FINAL RENDER ======================== //
		const gl::ScopedViewport scopedViewport(ivec2(0), app::toPixels(app::getWindowSize()));
		const gl::ScopedMatrices scopedMatrices;
		gl::disableDepthRead();
		gl::disableDepthWrite();
		gl::clear();

		gl::setMatricesWindow(app::getWindowSize());


		//gl::drawSolidRect(app::getWindowBounds());
		//gl::draw(mGBuffer->getColorTexture(), app::getWindowBounds());
		gl::draw(mLBuffer->getColorTexture(), app::getWindowBounds());

	}

	void DeferredRenderer::setCamera(mocha::CameraRef camera) {
		mCamera = camera;
		//setCamera(camera.getFov(), camera.getAspect(), camera.getNear(), camera.getFar());
	}

	void DeferredRenderer::addToScene(mocha::Object3DRef itm) {
		mObjects.push_back(itm);
	}

	void DeferredRenderer::update() {

		gl::color(ColorAf::white());

		// this is important - otherwise nothing gets rendered.
		gl::disableAlphaBlending();
		{
			// Bind the G-buffer FBO and draw to all attachments
			const gl::ScopedFramebuffer scopedFrameBuffer(mGBuffer);
			{
				const static GLenum buffers[] = {
					GL_COLOR_ATTACHMENT0,
					GL_COLOR_ATTACHMENT1,
					GL_COLOR_ATTACHMENT2,
					GL_COLOR_ATTACHMENT3
				};
				gl::drawBuffers(4, buffers);
			}
			const gl::ScopedViewport scopedViewport(ivec2(0), mGBuffer->getSize());
			const gl::ScopedMatrices scopedMatrices;
			gl::enableDepthRead();
			gl::enableDepthWrite();
			gl::clear();
			mCamera->useCamera();

			// Draw floor
			for (int i = 0; i < mObjects.size(); ++i) {
				mObjects[i]->draw();
			}

			// render teh lighting
			for (int i = 0; i < mLightObjects.size(); ++i) {
		
				gl::pushMatrices();
				gl::translate(mLightSources[i]->getPosition());
			//	mLightObjects[i]->draw();
				gl::popMatrices();
			}
		}
		// ========= BUILD LIGHTING  ================== //
		{
			const gl::ScopedFramebuffer scopedFrameBuffer(mLBuffer);
			const gl::ScopedViewport scopedViewport(ivec2(0), mLBuffer->getSize());
			gl::clear();
			const gl::ScopedMatrices scopedMatrices;
			const gl::ScopedBlendAdditive scopedAdditiveBlend;
			const gl::ScopedFaceCulling scopedFaceCulling(true, GL_FRONT);
			mCamera->useCamera();
			gl::enableDepthRead();
			gl::disableDepthWrite();

			// Bind G-buffer textures and shadow map
			const gl::ScopedTextureBind scopedTextureBind0(mTextureFboGBuffer[0], 0);
			const gl::ScopedTextureBind scopedTextureBind1(mTextureFboGBuffer[1], 1);
			const gl::ScopedTextureBind scopedTextureBind2(mTextureFboGBuffer[2], 2);
			//const gl::ScopedTextureBind scopedTextureBind3(mShadowBuffer->getDepthTexture(), 3);

			mLightCube->getGlslProg()->uniform("uSamplerAlbedo", 0);
			mLightCube->getGlslProg()->uniform("uSamplerNormalEmissive", 1);
			mLightCube->getGlslProg()->uniform("uSamplerPosition", 2);
			mLightCube->getGlslProg()->uniform("uSamplerShadowMap", 3);

			mLightCube->getGlslProg()->uniform("uShadowEnabled", mGenerateShadows);
			mLightCube->getGlslProg()->uniform("uShadowMatrix",
				mShadowCamera.getProjectionMatrix() * mShadowCamera.getViewMatrix());
			mLightCube->getGlslProg()->uniform("uViewMatrixInverse", mCamera->getInverseViewMatrix());

	
			for ( LightRef light : mLightSources) {
				mLightCube->getGlslProg()->uniform("uLightColorAmbient", light->getAmbient());
				mLightCube->getGlslProg()->uniform("uLightColorDiffuse", light->getDiffuse());
				mLightCube->getGlslProg()->uniform("uLightColorSpecular", light->getSpecular());
				mLightCube->getGlslProg()->uniform("uLightPosition",
					vec3((mCamera->getViewMatrix() * vec4(light->getPosition(), 1.0))));
				mLightCube->getGlslProg()->uniform("uLightIntensity", light->getIntensity());
				mLightCube->getGlslProg()->uniform("uLightRadius",light->getVolume());
				mLightCube->getGlslProg()->uniform("uWindowSize", vec2(app::getWindowSize()));

				gl::ScopedModelMatrix scopedModelMatrix;
				gl::translate(light->getPosition());
				gl::scale(vec3(light->getVolume()));
				mLightCube->draw();
			}

			// re-enable alpha blend for parts that need it.
			gl::enableAlphaBlending();
		
		}
		
			
		

		
	}
}