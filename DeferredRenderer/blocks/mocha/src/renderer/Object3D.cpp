#include "mocha/renderer/Object3D.h"
#include "cinder/Log.h"
using namespace ci;
using namespace std;
namespace mocha {

	Object3D::Object3D() : uEmissive(0.0),
		position(vec3()),
		isMeshSet(false),
		renderFunction(nullptr){}
	
	Object3D::Object3D(ci::gl::VboMeshRef mesh, ci::gl::GlslProg::Format shader) :
		uEmissive(0.0),
		position(vec3()),
		isMeshSet(true)
	{

		// combine the user content with the default vertex shader for geometry in the renderer

		if (shader.getVertex() == "") {
			shader.vertex(mocha::deferred::getDefaultGBufferVertex());
		}
		else {
			std::string vertexShader = mocha::string::loadShader(mocha::deferred::gBufferVertex, shader.getVertex(), true);
			shader.vertex(vertexShader);
		}

		if (shader.getFragment() == "") {
			shader.fragment(mocha::deferred::getDefaultGBufferFragment());
		}
		else {
			std::string joined = mocha::string::loadShader(mocha::deferred::gBufferFragment, shader.getFragment());

			shader.fragment(joined);
		}

		shader.version(330);

		mShader = gl::GlslProg::create(shader);

		// build the geometry
		mBatch = gl::Batch::create(mesh, mShader);
	}

	void Object3D::setShader(ci::gl::GlslProg::Format shader) {
		// combine the user content with the default vertex shader for geometry in the renderer.
		std::string vertexShader = mocha::string::loadShader(mocha::deferred::gBufferVertex, shader.getVertex(), true);
		shader.vertex(vertexShader);

		if (shader.getFragment() == "") {
			shader.fragment(mocha::deferred::getDefaultGBufferFragment());
		}
		else {
			shader.fragment(mocha::string::loadShader(mocha::deferred::gBufferFragment, shader.getFragment()));
		}

		shader.version(330);
		mShader = gl::GlslProg::create(shader);

		isMeshSet = false;

	}

	void Object3D::draw(){
		if (isMeshSet) {
			if (renderFunction != nullptr) {
				// set emissive value directly, no need to do it in render function
				mBatch->getGlslProg()->uniform("uEmissive", uEmissive);
				renderFunction(this);
			}
			else {
				gl::pushMatrices();

				gl::translate(position);
				mBatch->getGlslProg()->uniform("uEmissive", uEmissive);
				mBatch->draw();

				gl::popMatrices();
			}
		}
		else {
			if (renderFunction != nullptr) {
				renderFunction(this);
			}
		}
	}
}