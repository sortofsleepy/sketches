#pragma once
#include "cinder/gl/Vbo.h"
#include "cinder/gl/Fbo.h"
#include "cinder/app/App.h"
#include "cinder/gl/gl.h"
#include "cinder/Log.h"

namespace mocha {namespace utils {

	// helper to  quickly setup an FBO with a Texture format.
	static ci::gl::FboRef createFbo(ci::vec2 dimensions=ci::vec2(ci::app::getWindowWidth(),ci::app::getWindowHeight()),ci::gl::Texture::Format tfmt = ci::gl::Texture::Format()) {
		ci::gl::Fbo::Format fmt;
		fmt.setColorTextureFormat(tfmt);
		return ci::gl::Fbo::create(dimensions.x,dimensions.y, fmt);
	}

	//! Helper to quickly set built in projection and view matrices to fit the window.
	static void windowView() {
		ci::gl::setMatricesWindow(ci::app::getWindowSize());
		ci::gl::viewport(ci::app::getWindowSize());
	}

	//! Loads a shader. Pass in the path to each possible component of the shader. The base
	//! path is assumed to be in your assets folder.
	//! At a minimum you need a vertex shader.
	static ci::gl::GlslProgRef loadShader(std::string vertex,std::string fragment="",std::string compute="", std::string geometry="") {
		ci::gl::GlslProg::Format fmt;
		fmt.vertex(ci::app::loadAsset(vertex));
		
		if (fragment != "") {
			fmt.fragment(ci::app::loadAsset(fragment));
		}

		if (compute != "") {
			fmt.compute(ci::app::loadAsset(compute));
		}

		if (geometry != "") {
			fmt.geometry(ci::app::loadAsset(geometry));
		}

		try {
			return ci::gl::GlslProg::create(fmt);
		}
		catch (ci::Exception &e) {
			CI_LOG_E(e.what());
		}
	}
} }