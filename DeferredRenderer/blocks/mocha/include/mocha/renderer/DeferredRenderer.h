#pragma once


#include <memory>
#include <string>
#include <vector>
#include "cinder/gl/Fbo.h"
#include "cinder/gl/gl.h"
#include "cinder/app/App.h"
#include "cinder/Camera.h"
#include "cinder/Utilities.h"
#include "cinder/ImageIo.h"
#include "cinder/CameraUi.h"
#include "cinder/app/App.h"
#include "mocha/core/Camera.h"
#include "mocha/renderer/Object3D.h"
#include "mocha/renderer/Light.h"
#include "mocha/renderer/RendererDefaults.h"

namespace mocha {
	typedef std::shared_ptr<class DeferredRenderer>DeferredRendererRef;


	/*
		A basic Deferred renderer largely based on Cinder sample by 
		 Stephen Schieberl and Michael Latzoni
	*/
	class DeferredRenderer {


	public:
		DeferredRenderer();

		static DeferredRendererRef create(
			int width = ci::app::getWindowWidth(),
			int height = ci::app::getWindowHeight(),
			int shadowMapSize = 2048) {

			DeferredRenderer * render = new DeferredRenderer;

			// set dimensions.
			render->setHeight(height);
			render->setWidth(width);
			//render->setupShadowmap(shadowMapSize);

			// generate buffers
			render->generateBuffers();
		
			return DeferredRendererRef(render);

		}
		void setHeight(int val) { viewHeight = val; }
		void setWidth(int val) { viewWidth = val; }
		void setScene(ci::gl::FboRef mScene) { mSceneFbo = mScene; }
		void setupShadowmap(GLsizei sz = 2048, ci::vec3 eye = ci::vec3(0.0f, 7.0f, 0.0f), ci::vec3 target = ci::vec3(0.0f));
		void setCamera(float fov, float aspect, float near, float far, ci::vec3 eye = ci::vec3(-2.221f, 2.0f, 15.859f), ci::vec3 target = ci::vec3(0.0f, 2.0f, 0.0f));
		void setCamera(mocha::CameraRef cam);
		
		//! updates the renderer. 
		void update();

		//! Generates the necessary Fbos
		void generateBuffers();
		
		//! Also adds a piece of geometry to the scene
		void addToScene(mocha::Object3DRef itm);

		void addLight(LightRef light);

		//! draws the final result. 
		void draw();

		//! Return the output from the renderer
		ci::gl::TextureRef getOutput() { return mLBuffer->getColorTexture(); }

		//! Resizes the FBOs used in the renderer
		void resize(int width = ci::app::getWindowWidth(),
			int height = ci::app::getWindowHeight(),
			int shadowMapSize = 2048);
	protected:

		//! all the objects to be rendered in the scene.
		std::vector<mocha::Object3DRef> mObjects;

		//! All the lights to be rendered in the scene. 
		std::vector<ci::gl::BatchRef> mLightObjects;
		
		//! All the lighting information for the scene. 
		std::vector<LightRef> mLightSources;

		//! Whether or not the renderer should have shadows. 
		bool mGenerateShadows;

		//! General camera settings
		float mNear, mFar, mFov;
		ci::CameraPersp mShadowCamera;
		mocha::CameraRef mCamera;

		int viewWidth, viewHeight;
		ci::gl::FboRef mGBuffer, mLBuffer, mShadowBuffer, mSceneFbo;
		ci::gl::GlslProgRef mGShader, mLShader, mSceneShader;
		
		//! All the textures used to hold geometry information.
		ci::gl::TextureRef mTextureFboGBuffer[4];

		//! Default light information
		LightRef mainLight;

		//! Helps to render all the lighting. 
		ci::gl::BatchRef mLightCube;

	};
}