#pragma once

#include "mocha/renderer/strings.h"
#include "cinder/gl/Batch.h"
#include "cinder/gl/VboMesh.h"
#include "cinder/gl/GlslProg.h"
#include <string>
#include <memory>
#include "mocha/renderer/RendererDefaults.h"
#include "mocha/renderer/strings.h"
#include "cinder/app/App.h"
#include "cinder/gl/gl.h"
namespace mocha {

	typedef std::shared_ptr<class Object3D> Object3DRef;

	class Object3D {

	public:
		Object3D();
		Object3D(ci::gl::VboMeshRef mesh, ci::gl::GlslProg::Format shader);
		void setShader(ci::gl::GlslProg::Format shader);

		void draw();

		template<typename T>
		void uniform(std::string name, T val) { mBatch->getGlslProg()->uniform(name, val); }

		static Object3DRef create(ci::gl::VboMeshRef mesh, ci::gl::GlslProg::Format shader = ci::gl::GlslProg::Format())
		{
			return Object3DRef(new Object3D(mesh, shader));
			//return Object3DRef( new Object3D() );
		}


		static Object3DRef create()
		{
			return Object3DRef(new Object3D());
		}

		ci::gl::BatchRef getBatch() { return mBatch;  }
		void setPosition(ci::vec3 position) { this->position = position; }
		ci::vec3 getPosition() { return position; }
		void setEmissive(float emissiveValue){ uEmissive = emissiveValue; }

		ci::mat4 getModelMatrix() { return modelMatrix; }
		void setRenderFunction(std::function<void(mocha::Object3D*)> renderFunction) {
			this->renderFunction = renderFunction;
		}

	
	protected:

		//! A rendering function to use when needing to do custom rendering. Passes Object3D back 
		//! to callback function.
		std::function<void(mocha::Object3D*)> renderFunction;

		//! Helps us determine if we've set a mesh or if we need to render something ourselves
		bool isMeshSet;

		//! position for the geometry
		ci::vec3 position;
		
		//! emissive value for the object in the renderer
		float uEmissive;

		//! Batch to render geometry
		ci::gl::BatchRef mBatch;

		//! Shader to render geometry
		ci::gl::GlslProgRef mShader;

		//! Matrix to manipulate things via CPU 
		ci::mat4 modelMatrix;

		
	};
}