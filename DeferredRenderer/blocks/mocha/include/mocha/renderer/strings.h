#pragma once

#include <string>
#include <vector>
#include <sstream>
#include "cinder/Log.h"
#define STRINGIFY(A) #A


namespace mocha { namespace string {
	typedef struct {
		size_t start, end;
	}StringSearchResult;


	inline static std::string getContentBetween(std::string copy,std::string delim1, std::string delim2) {
		unsigned first = copy.find(delim1);
		unsigned last = copy.find(delim2);
		return copy.substr(first + 1, last - (first + 1));
	}

	


	//! Shader injection for joining core and user code.
	//! Base code vector should follow the following format \n
	//! index 0 - core uniforms and varyings 
	//! index 1 - core main code
	//! Note that it is assumed user will output core variables like gl_Position and fragment output
	//! Returns joined source. 
	inline std::string loadShader(std::vector<std::string> baseCode, std::string userCode,bool isVertexShader=false) {
		std::istringstream content(userCode);
		std::string line;
		std::string uniformVarings = "";
		std::string mainContent = "";
		std::string finalShader = "";


		// ================ GRAB UNIFORM / VARYINGS FROM USER CONTENT ================== //
		std::vector<size_t> found; 
		std::vector<StringSearchResult> results;

		results.resize(4);
		found.resize(4);

		int numTries = 0;

		// parse uniforms and varyings and remove them from user content while storing onto final compiled content.
		while (getline(content, line)) {
		

			found[0] = line.find("out");
			found[1] = line.find("uniform");
			found[2] = line.find("in");
			found[3] = line.find("main");
		
			for (int i = 0; i < found.size(); ++i) {
				if (found[i] != std::string::npos) {
					
					uniformVarings += line;

					StringSearchResult result;
				

					result.end = userCode.find("\n", found[i]);
					result.start = userCode.rfind("\n", found[i]);

					results.push_back(result);
				}

			}

			if (results.size() > 0 && numTries >100) {
				break;
			}

			if (numTries > 100 && results.size() < 1) {
				break;
			}

			numTries += 1;
		}

		// parse results so we erase additional uniforms and varyings from original user code
		//https://stackoverflow.com/questions/8247102/how-to-remove-a-line-from-a-string-with-large-content-in-c
		if (results.size() > 0) {
			for (StringSearchResult & result : results) {
			
				auto start = result.start != std::string::npos ? result.start : 0;
				auto end = result.end != std::string::npos ? result.end : 0;

				userCode.erase(start, end - start);

			}
		}

		//================== GRAB USER MAIN FUNCTION ==================== //
		
		// TODO maybe just leave this out for now or figure out how to determine if statement later.
		//mainContent = getContentBetween(userCode, "void main(){", "}");

		mainContent = userCode;
	

		//================== COMPILE ==================== //

		// add core uniforms / varyings
		finalShader += baseCode[0];

		// add user uniform / varyings 
		finalShader += "\n" + uniformVarings;

		// add core main content 
		finalShader += "\n void main(){" + baseCode[1] + "\n";

		// add user main content 
		// If we're dealing with a vertex shader, it's likely user hasn't set gl_Position so set it if that is the case
		if (isVertexShader) {
			finalShader += "\n" + mainContent + " \n gl_Position = ciModelViewProjection * ciPosition;\n}";
		}
		else {
			finalShader += "\n" + mainContent + "}";
		}
		
		//CI_LOG_I(finalShader);


		
		return finalShader;

	}



} }