#pragma once

#include "cinder/Color.h"
#include <memory>

namespace mocha {

	typedef std::shared_ptr<class Light> LightRef;

	class Light {

	public:
		Light();

		static LightRef create() {
			return LightRef(new Light());
		}
	
		Light& colorDiffuse(ci::ColorA mColorDiffuse);
		Light& colorAmbient(ci::ColorA mColorAmbient);

		Light& intensity(float mIntensity);
		Light& position(ci::vec3 position);
		Light& radius(float mRadius);
		Light& volume(float mVolume);

		ci::ColorA getAmbient() { return mColorAmbient; }
		ci::ColorA getDiffuse() { return mColorDiffuse; }
		ci::ColorA getSpecular() { return mColorSpecular; }
		float getIntensity() {
			return mIntensity;
		}

		float getRadius() { return mRadius;  }
		float getVolume() { return mVolume; }
		ci::vec3 getPosition() { return mPosition;  }
		void setPosition(ci::vec3 position) { mPosition = position; }
	protected:
		ci::ColorA mColorAmbient,mColorDiffuse,mColorSpecular;
		float mIntensity,mRadius,mVolume;
		ci::vec3 mPosition;

	};
}