#pragma once

#include "CinderImGui.h"
#include <string>
#include <vector>
#include "cinder/Log.h"
#include "GuiTypes.h"

namespace mocha {


// =================== // 

	class Settings {
	public:
		Settings() {
			ImGui::initialize();
		}

	

		~Settings() {
			// TODO cleanup gui items.
		}

		void render() {

			for (auto &itm : mSettings) {
				itm->render();
			}
		}

		void addItem(Setting * gui) {

			if (gui->getName() == "") {
				gui->setName("Gui Item " + mSettings.size() + 1);
			}
			mSettings.push_back(gui);
		}


	protected:
		std::vector<Setting*> mSettings;


	};
}