#pragma once

#include "cinder/gl/GlslProg.h"
#include "cinder/gl/Texture.h"
#include "cinder/gl/Fbo.h"
#include "cinder/app/App.h"
#include "cinder/gl/gl.h"
#include "cinder/Log.h"
#include <memory>

#define STRINGIFY(A) #A
namespace mocha { namespace post {

	typedef std::shared_ptr<class PostProcessEffect> PostEffectRef;
	
	//! Describes a basic post processing effect. Pass in the shader that managed the effect.
	class PostProcessEffect {
	public:
		PostProcessEffect() = default;

	
		PostProcessEffect(ci::gl::GlslProg::Format shader);

		//! Resizes the FBO used to handle post processing. Defaults to window width / height.
		void resizeFbo(int width = ci::app::getWindowWidth(), int height = ci::app::getWindowHeight());

		//! Create new Post process effect. 
		static PostEffectRef create(ci::gl::GlslProg::Format shader = ci::gl::GlslProg::Format()) {
			return PostEffectRef(new PostProcessEffect(shader));
		}

		//! Set input to process for this effect
		void setInput(ci::gl::TextureRef Input);

		//! Return the procssing bounds 
		ci::Area getBounds() { return mFbo->getBounds(); }

		//! Update the effect. Pass in optional lambda if you need to customize things like shader uniforms
		void update(std::function<void(ci::gl::GlslProgRef)> mfunc = nullptr);

		//! Returns the processed input
		ci::gl::TextureRef getTexture() { return mFbo->getColorTexture(); }

	protected:
		ci::gl::FboRef mFbo;
		ci::gl::TextureRef mInput;
		ci::gl::GlslProgRef mShader;

		ci::gl::BatchRef mBatch;
	

		//! Main vertex shader - gets used if no vertex shader is supplied to the effect.
		//! Assumes we're rendering onto a full-screen quad.
		std::string vertex = STRINGIFY(
			uniform mat4 ciModelViewProjection;
			in vec3 ciPosition;
			in vec2 ciTexCoord0;

			out vec2 vUv;

			void main() {
				vUv = ciTexCoord0;
				gl_Position = ciModelViewProjection * vec4(ciPosition, 1.);
			}
		);
	};
} }