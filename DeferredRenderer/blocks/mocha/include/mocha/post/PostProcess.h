#pragma once

#include "cinder/gl/gl.h"
#include "cinder/app/App.h"
#include "cinder/gl/Fbo.h"
#include "mocha/post/PostProcessEffect.h"
#include <vector>

namespace mocha { namespace post {

	//! A Post processing compositor. 
	class PostProcess {

	public:
		PostProcess() {

			// resize all post process FBOs when the window resizes.
			ci::app::getWindow()->getSignalResize().connect([this] {
				for (PostEffectRef stage : stages) {
					stage->resizeFbo();
				}
			});

		}

		PostProcess& addStage(PostEffectRef stage) { 
			stages.push_back(stage);
			return *this;
		}

		//! Sets up each stage of the pipeline with the correct input image.
		void compile(ci::gl::TextureRef mInput) {

			if (stages.size() > 1) {
				for (int i = 0; i < stages.size(); ++i) {
					if (i == 0) {
						stages[i]->setInput(mInput);
					}
					else {
						stages[i]->setInput(stages[i - 1]->getTexture());
					}
				}
			}
			else {
				stages[0]->setInput(mInput);
			}
		}

		//! runs through the update stage of each effect.
		/*void update() {
			for (int i = 0; i < stages.size(); ++i) {
				stages[i]->update();
			}
		}*/

		//! Returns the last stage in the pipeline so you can render to screen.
		ci::gl::TextureRef getTexture() { 
		
			auto idx = stages.size() - 1;
			return stages[idx]->getTexture();
		}

		void draw() {
			ci::gl::setMatricesWindow(ci::app::getWindowSize());
			ci::gl::viewport(ci::app::getWindowSize());

			// TODO just make this geometry w/ shader.
			ci::gl::draw(getTexture(), ci::app::getWindowBounds());
			//ci::gl::drawSolidRect(ci::app::getWindowBounds());
		}
	private:
		std::vector<PostEffectRef> stages;
	};
}}