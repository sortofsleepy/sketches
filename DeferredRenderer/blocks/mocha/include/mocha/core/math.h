#pragma once

#include "cinder/Matrix44.h"
#include "cinder/Vector.h"
#include <vector>


using namespace std;
using namespace ci;
namespace mocha {
	namespace math {

		//! Returns a point on a sphere based on a radius, theta and phi.
		static vec3 pointOnSphere(float radius, float theta, float phi) {
			int r = radius;
			float x = cos(theta) * sin(phi) * r;
			float y = sin(theta) * sin(phi) * r;
			float z = cos(phi) * r;

			return vec3(x, y, z);
		}


		//! Converts an unsigned short value to an integer.
		static double unsignedShortToInt(unsigned short value) {
			auto val = (long)value;
			return (double)val;
		}


		//! Groups a set of unrolled indices into groups of 3 via a vec3
		static vector<ci::vec3> indicesToFaces(std::vector<uint32_t> indices) {

			vector<ci::vec3> faces;
			for (int i = 0; i < indices.size(); i += 3) {


				float idx1 = indices[i];
				float idx2 = indices[i + 1];
				float idx3 = indices[i + 2];

				faces.push_back(vec3(idx1, idx2, idx2));

			}



			return faces;
		}

		// adapted from three.js
		static ci::mat4 rotationFromEuler(ci::mat4 euler) {

			float x = euler[3][0], y = euler[3][1], z = euler[3][2];
			float a = cos(x), b = sin(x);
			float c = cos(y), d = sin(y);
			float e = cos(z), f = sin(z);

			auto ae = a * e, af = a * f, be = b * e, bf = b * f;

			euler[0][0] = c * e;
			euler[0][1] = -c * f;
			euler[0][2] = d;

			euler[1][0] = af + be * d;
			euler[1][1] = ae - bf * d;
			euler[1][2] = -b * c;

			euler[2][0] = bf - ae * d;
			euler[2][1] = be + af * d;
			euler[2][2] = a * c;

			euler[3][0] = 0;
			euler[3][1] = 0;
			euler[3][2] = 0;

			euler[0][3] = 0;
			euler[1][3] = 0;
			euler[2][3] = 0;
			euler[3][3] = 0;

			return euler;
		}
	}
}