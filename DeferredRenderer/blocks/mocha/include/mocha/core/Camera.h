#pragma once
#include "cinder/gl/gl.h"
#include "cinder/CameraUi.h"
#include "cinder/Timeline.h"
#include "cinder/Camera.h"
#include "cinder/app/App.h"

#include <memory>

namespace mocha {

	typedef std::shared_ptr<class Camera> CameraRef;

	class Camera {
	public:
		Camera(float fov = 60.0f, float aspect = ci::app::getWindowAspectRatio(), float near = 0.1f, float far = 1000.0f);


		static CameraRef create(float fov = 60.0f, float aspect = ci::app::getWindowAspectRatio(), float near = 0.1f, float far = 1000.0f){
			return CameraRef(new Camera(fov, aspect, near, far));
		}

		void setZoom(float zoom);
		void enableUI();
		void useCamera();

		void setFov(float fov = 60.0);
		void lookAt(ci::vec3 eye, ci::vec3 target = ci::vec3(0));
		

		//! Sets camera to perspective camera based on window. 
		static void useWindowCamera() {
			ci::gl::setMatricesWindow(ci::app::getWindowSize());
			ci::gl::viewport(ci::app::getWindowSize());
		}

		float getFov() { return fov; }
		float getAspect() { return mCam.getAspectRatio(); }
		float getNear() { return mCam.getNearClip(); }
		float getFar() { return mCam.getFarClip();  }

		ci::mat4 getViewMatrix() { return mCam.getViewMatrix(); }
		ci::mat4 getInverseViewMatrix() { return mCam.getInverseViewMatrix(); }
		ci::mat4 getProjectionMatrix() { return mCam.getProjectionMatrix(); }
	private:
		ci::CameraPersp mCam;
		ci::CameraUi mCamUi;
		float fov;
		ci::vec3 position, target, eye, up;
	};
}